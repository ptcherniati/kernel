package org.inra.ecoinfo.filecomp.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = FileType.NAME_ENTITY_JPA,
       indexes = @Index(name = "filetype_code_idx", columnList = "code"))
public class FileType implements Serializable, Comparable<FileType> {

    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    public static final String NAME_ENTITY_JPA = "filetype";
    /**
     * The Constant NAME_ATTRIBUTS_DESCRIPTION @link(String).
     */
    public static final String NAME_ATTRIBUTS_DESCRIPTION = "description";
    /**
     * The Constant NAME_ATTRIBUTS_NAME @link(String).
     */
    public static final String NAME_ATTRIBUTS_NAME = "name";
    /**
     * The Constant NAME_ATTRIBUTS_ID @link(String).
     */
    public static final String ID_JPA = "ft_id";

    private static final long serialVersionUID = 1L;
    /**
     * The code @link(String).
     */
    @Column(nullable = false, unique = true)
    @NaturalId
    private String code;
    /**
     * The name @link(String).
     */
    @Column(nullable = false, unique = true, name = FileType.NAME_ATTRIBUTS_NAME)
    private String name;
    /**
     * The description @link(String).
     */
    @Column(columnDefinition = "TEXT", name = FileType.NAME_ATTRIBUTS_DESCRIPTION)
    private String description = "";
    /**
     * The id @link(Long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = FileType.ID_JPA)
    protected Long id;

    /**
     *
     */
    public FileType() {
        super();
    }

    /**
     *
     * @param name
     */
    public FileType(String name) {
        super();
        setName(name);
    }

    /**
     *
     * @param name
     * @param description
     */
    public FileType(String name, String description) {
        super();
        setName(name);
        setDescription(description);
    }

    @Override
    public int compareTo(FileType o) {
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            return 0;
        }
        return getCode() == null ? -1 : getCode().compareTo(o.getCode());
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FileType)) {
            return false;
        }
        FileType toCompare = (FileType) obj;
        return Objects.equals(code, toCompare.code);
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
        this.code = Utils.createCodeFromString(name);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (code == null ? 0 : code.hashCode());
        result = prime * result + (description == null ? 0 : description.hashCode());
        return result;
    }
}
