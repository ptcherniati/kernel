package org.inra.ecoinfo.filecomp.config.impl;

import org.inra.ecoinfo.config.IModuleConfiguration;

/**
 *
 * @author ptcherniati
 */
public interface IFileCompConfiguration extends IModuleConfiguration {

    /**
     *
     */
    String ID_BEAN = "fileCompConfiguration";

    /**
     *
     * @param folder
     */
    void setFolder(String folder);

    /**
     *
     * @return
     */
    String getRepositoryFiles();

}
