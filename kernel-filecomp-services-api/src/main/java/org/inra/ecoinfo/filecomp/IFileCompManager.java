package org.inra.ecoinfo.filecomp;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.filecomp.entity.FileType;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IFileCompManager extends Serializable {

    /**
     * The id bean @link(String).
     */
    String ID_BEAN = "fileCompManager";

    /**
     *
     * @param fileCode
     * @return
     * @throws BusinessException
     */
    byte[] doDownload(final String fileCode) throws BusinessException;

    /**
     * Do download.
     *
     * @link(String)
     * @link(String) the suffix name file
     *
     * @return the byte[]
     */
    Utilisateur getUtilisateur();

    /**
     *
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    List<String> retrieveListDatesPossible() throws BusinessException;

    /**
     *
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    List<FileType> retrieveListTypesPossible() throws BusinessException;

    /**
     *
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException @throws PersistenceException
     */
    List<String> retrieveListUploadersPossible() throws BusinessException;

    /**
     *
     * @param code
     * @return
     */
    Optional<FileComp> getFileCompByCode(String code);

    /**
     *
     * @param code
     * @return
     */
    Optional<FileComp> getFileCompById(Long code);

    /**
     *
     * @param originalFileName
     * @return
     */
    String createUniqueIdForFile(String originalFileName);

    /**
     *
     * @param nodeIds
     * @param intervalsDate
     * @return
     */
    List<FileComp> getFileCompFromPathesAndIntervalsDate(List<Long> nodeIds, List<IntervalDate> intervalsDate);

    /**
     *
     * @param compositeGroup
     * @return
     */
    Set<Long> getAssociatesNodesIds(ICompositeGroup compositeGroup);

    /**
     *
     * @param fileTypeCode
     * @return
     */
    Optional<FileType> getFileTypeFromFileTypeCode(String fileTypeCode);

    /**
     *
     * @param fileComp
     * @param descriptionsList
     * @return
     * @throws BusinessException
     */
    FileComp saveFile(FileComp fileComp, List<String> descriptionsList) throws BusinessException;

    /**
     *
     * @param fileComp
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    void removeFile(FileComp fileComp) throws BusinessException;

    /**
     *
     * @param fileComp
     * @param descriptionsList
     * @return
     * @throws BusinessException
     */
    FileComp updateFile(FileComp fileComp, List<String> descriptionsList) throws BusinessException;
    

    /**
     *
     * @return
     */
    List<FileComp> getAllFilesComp();

    /**
     *
     * @return
     */
    List<FileComp> getFileCompForApplication();
}
