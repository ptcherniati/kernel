package org.inra.ecoinfo.filecomp.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = FileComp.NAME_ENTITY_JPA,
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {FileComp.FILE_CODE})},
        indexes = {
            @Index(name = "file_code_idx", columnList = FileComp.FILE_CODE)
            ,
           @Index(name = "file_type_idx", columnList = FileType.ID_JPA)
            ,
           @Index(name = "file_modify_user_idx", columnList = FileComp.LAST_MODIFY_USER)
            ,
           @Index(name = "file_create_user_idx", columnList = FileComp.CREATE_USER)
        })
@NamedEntityGraph(name = FileComp.COMPLETE_GRAPH,
        attributeNodes = {
            @NamedAttributeNode("id")
            ,@NamedAttributeNode("fileName")
            ,@NamedAttributeNode("code")
            ,@NamedAttributeNode("description")
            ,@NamedAttributeNode("groups")
            ,@NamedAttributeNode("dateFinPeriode")
            ,@NamedAttributeNode("mandatory")
            ,@NamedAttributeNode("lastModifyUser")
            ,@NamedAttributeNode("size")
            ,@NamedAttributeNode("createUser")
            ,@NamedAttributeNode("groupFileComps")
            ,@NamedAttributeNode("dateDebutPeriode")
            ,@NamedAttributeNode("lastModifyDate")
            ,@NamedAttributeNode("fileType")
            ,@NamedAttributeNode("createDate")
        })
public class FileComp implements Comparable<FileComp>, ICompositeGroup, Serializable {

    /**
     *
     */
    public static final String COMPLETE_GRAPH = "graph.org.inra.ecoinfo.filecomp.entity.FileComp";

    /**
     *
     */
    public static final String FILE_PATTERN = "file_%s";

    /**
     *
     */
    public static final String UPLOAD_PATH_PATTERN = "%s/%s/%s";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "insertion_filecomp_ifcs";

    /**
     *
     */
    public static final String ENTITY_DESCRIPTION_COLUM_NAME = "ifsc_description";

    /**
     *
     */
    public static final String FILE_NAME = "ifcs_name";

    /**
     *
     */
    public static final String FILE_CODE = "ifcs_code";

    /**
     *
     */
    public static final String CREATE_USER = "ifcs_create_user";

    /**
     *
     */
    public static final String CREATE_DATE = "ifcs_create_date";

    /**
     *
     */
    public static final String LAST_MODIFY_USER = "ifcs_last_modify_user";

    /**
     *
     */
    public static final String LAST_MODIFY_DATE = "ifcs_last_modify_date";
    /**
     * The Constant DATE_DEBUT_ID @link(String).
     */
    public static final String DATE_DEBUT_ID = "ifcs_date_debut_periode";
    /**
     * The Constant DATE_FIN_ID @link(String).
     */
    public static final String DATE_FIN_ID = "ifcs_date_fin_periode";
    /**
     * The Constant SIZE @link(String).
     */
    public static final String SIZE = "ivfc_size";
    /**
     * The Constant KEY_ID @link(String).
     */
    public static final String KEY_ID = "ifcs_id";
    /**
     *
     */
    public static final String CODE_PATTERN = "%s@%s";
    /**
     * Default serial
     */
    private static final long serialVersionUID = 1L;
    private static final int MB = 1_048_576;
    private static final int KB = 1_024;
    private static final String BYTES = "bytes";
    /**
     * The Constant BATCH_SIZE @link(int).
     */
    static final int BATCH_SIZE = 14;

    /**
     * The data @link(byte[]).
     *
     * @param fileTypeCode
     * @param fileCompRepository
     * @param originalFilename
     * @return
     */
    public static String getAbsoluteFilePath(String fileTypeCode, String originalFilename, String fileCompRepository) {
        return String.format(UPLOAD_PATH_PATTERN, fileCompRepository, fileTypeCode, originalFilename);

    }

    @Transient
    Group _ownGroup;

    @Column(nullable = false, unique = false, name = FileComp.FILE_NAME)
    private String fileName;
    @NaturalId
    @Column(nullable = false, unique = true, name = FileComp.FILE_CODE)
    private String code;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, optional = false)
    @JoinColumn(referencedColumnName = FileType.ID_JPA, nullable = false, name = FileType.ID_JPA)
    private FileType fileType;
    /**
     * The create date @link(Date).
     */
    @Column(unique = false, name = FileComp.CREATE_DATE)
    @CreationTimestamp
    private LocalDateTime createDate;
    /**
     * The create user @link(Utilisateur).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, optional = false)
    @JoinColumn(referencedColumnName = Utilisateur.UTILISATEUR_NAME_ID, nullable = false, name = FileComp.CREATE_USER)
    private Utilisateur createUser;
    @Column(unique = false, name = FileComp.LAST_MODIFY_DATE)
    @UpdateTimestamp
    private LocalDateTime lastModifyDate;
    /**
     * The create user @link(Utilisateur).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, optional = false)
    @JoinColumn(referencedColumnName = Utilisateur.UTILISATEUR_NAME_ID, nullable = false, name = FileComp.LAST_MODIFY_USER)
    private Utilisateur lastModifyUser;
    /**
     * The date debut periode @link(Date).
     */
    @Column(nullable = true, unique = false, name = FileComp.DATE_DEBUT_ID)
    private LocalDate dateDebutPeriode;
    /**
     * The date fin periode @link(Date).
     */
    @Column(nullable = true, unique = false, name = FileComp.DATE_FIN_ID)
    private LocalDate dateFinPeriode;
    /**
     * The size @link(int).
     */
    @Column(name = FileComp.SIZE)
    private int size = 0;
    @Column(columnDefinition = "TEXT", name = ENTITY_DESCRIPTION_COLUM_NAME, nullable = false)
    private String description;
    private Boolean mandatory = Boolean.FALSE;
    private Boolean forApplication = Boolean.FALSE;
    /**
     * The data @link(byte[]).
     */
    @Type(type = "org.hibernate.type.MaterializedBlobType")
    @Column(nullable = false)
    @Basic(fetch = FetchType.LAZY)
    private byte[] data;
    
    
    private String contentType = "application/octet-stream";
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = GroupFileComp.TABLE_NAME,
            joinColumns = {
                @JoinColumn(name = GroupFileComp.FILE_COMP_FIELD_NAME, nullable = false, updatable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = GroupFileComp.GROUP_FIELD_NAME)}
    )
    Set<Group> groups = new HashSet();
    @OneToMany(targetEntity = GroupFileComp.class, mappedBy = "fileComp")
    @Column(insertable = false, updatable = false)
    Set<GroupFileComp> groupFileComps = new HashSet();
    /**
     *
     */
    @Id
    @Column(name = FileComp.KEY_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected Long id;

    /**
     *
     * @return
     */
    public String getContentType() {
        return contentType;
    }

    /**
     *
     * @param contentType
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     *
     * @return
     */
    public byte[] getData() {
        return Optional.ofNullable(data)
                .map(data -> Arrays.copyOf(data, data.length))
                .orElse(null);
    }

    /**
     *
     * @param d
     */
    public void setData(byte[] d) {
        this.data = Optional.ofNullable(d)
                .map(data -> Arrays.copyOf(data, data.length))
                .orElse(new byte[0]);
    }

    /**
     *
     */
    @PostLoad
    public void initGroups() {
        getAllGroups().addAll(groupFileComps.stream()
                .map(gu -> gu.getGroup())
                .collect(Collectors.toSet()));
        getOwnGroup(WhichTree.TREEDATASET);
    }

    /**
     *
     * @param mandatory
     */
    public void setMandatory(Boolean mandatory) {
        this.mandatory = Optional.ofNullable(mandatory).orElse(Boolean.FALSE);
    }

    /**
     *
     * @return
     */
    public Boolean getMandatory() {
        return Optional.ofNullable(mandatory).orElse(Boolean.FALSE);
    }

    /**
     *
     * @return
     */
    public Boolean getForApplication() {
        return Optional.ofNullable(forApplication).orElse(Boolean.FALSE);
    }

    /**
     *
     * @param forApplication
     */
    public void setForApplication(Boolean forApplication) {
        this.forApplication = Optional.ofNullable(forApplication).orElse(Boolean.FALSE);
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(FileComp o) {
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            return 0;
        } else if (getDateDebutPeriode() == null) {
            return -1;
        } else if (getDateDebutPeriode().compareTo(o.getDateDebutPeriode()) != 0) {
            return getDateDebutPeriode().compareTo(o.getDateDebutPeriode());
        } else if (getDateFinPeriode() == null) {
            return -1;
        } else {
            return getDateFinPeriode().compareTo(o.getDateFinPeriode());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */

    /**
     *
     * @param obj
     * @return
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FileComp)) {
            return false;
        }
        FileComp toCompare = (FileComp) obj;
        return Objects.equals(code, toCompare.code);
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the creates the user.
     *
     * @return the creates the user
     */
    public Utilisateur getCreateUser() {
        return createUser;
    }

    /**
     *
     * @param createUser
     */
    public void setCreateUser(Utilisateur createUser) {
        this.createUser = createUser;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     *
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     *
     * @return
     */
    public String getFileSizeAffichage() {
        String fileSize = "";
        if (size < FileComp.KB) {
            fileSize = String.format("%s %s", size, FileComp.BYTES);
        } else if (size < FileComp.MB) {
            fileSize = String.format("%d %s", Math.round((double) size / FileComp.KB), "KB");
        } else {
            fileSize = String.format("%d %s", Math.round((double) size / FileComp.MB), "MB");
        }
        return fileSize;
    }

    /**
     *
     * @return
     */
    public FileType getFileType() {
        return fileType;
    }

    /**
     *
     * @param fileType
     */
    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Gets the interval date.
     *
     * @return the interval date
     * @throws BadExpectedValueException the bad expected value exception
     */
    @Transient
    public IntervalDate getIntervalDate() throws BadExpectedValueException {
        return new IntervalDate(getDateDebutPeriode().atStartOfDay(), getDateFinPeriode().atStartOfDay(), DateUtil.DD_MM_YYYY);
    }

    /**
     *
     * @return
     */
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getLastModifyDate() {
        return lastModifyDate;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateDebutPeriode() {
        return dateDebutPeriode;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateFinPeriode() {
        return dateFinPeriode;
    }

    /**
     *
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     *
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (code == null ? 0 : code.hashCode());
        result = prime * result + (dateDebutPeriode == null ? 0 : dateDebutPeriode.hashCode());
        result = prime * result + (dateFinPeriode == null ? 0 : dateFinPeriode.hashCode());
        result = prime * result + (createDate == null ? 0 : createDate.hashCode());
        result = prime * result + (createUser == null ? 0 : createUser.hashCode());
        result = prime * result + (description == null ? 0 : description.hashCode());
        return result;
    }

    /**
     *
     * @param fileCompRepository
     * @return
     */
    public String getAbsoluteFilePath(String fileCompRepository) {
        return String.format(UPLOAD_PATH_PATTERN, fileCompRepository, fileType.getCode(), getCode());

    }

    /**
     *
     * @return
     */
    public Utilisateur getLastModifyUser() {
        return lastModifyUser;
    }

    /**
     *
     * @param lastModifyDate
     */
    public void setLastModifyDate(LocalDateTime lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    /**
     *
     * @param lastModifyUser
     */
    public void setLastModifyUser(Utilisateur lastModifyUser) {
        this.lastModifyUser = lastModifyUser;
    }

    /**
     *
     * @param dateDebutPeriode
     */
    public void setDateDebutPeriode(LocalDate dateDebutPeriode) {
        this.dateDebutPeriode = dateDebutPeriode;
    }

    /**
     *
     * @param dateFinPeriode
     */
    public void setDateFinPeriode(LocalDate dateFinPeriode) {
        this.dateFinPeriode = dateFinPeriode;
    }

    /**
     *
     * @param createDate
     */
    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    /**
     *
     * @return
     */
    @Override
    public String getGroupName() {
        return String.format(FILE_PATTERN, code);
    }

    /**
     *
     * @param groups
     */
    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    /**
     *
     * @return
     */
    @Override
    public Set<Group> getAllGroups() {
        return groups;
    }

    /**
     *
     * @param groups
     */
    public void setAllGroups(List<Group> groups) {
        this.groups = groups.stream().collect(Collectors.toSet());
    }

    /**
     *
     * @param whichTree
     * @return
     */
    @Override
    public Group getOwnGroup(WhichTree whichTree) {
        if (_ownGroup == null) {
            _ownGroup = getAllGroups().stream().filter(u -> u.getGroupName().equals(getGroupName()) && u.getWhichTree().equals(whichTree)).findFirst().orElse(_ownGroup);
        }
        return _ownGroup;
    }

    /**
     *
     * @param group
     * @return
     */
    @Override
    public boolean testIsMutableGroup(Group group) {
        return true;
    }
}
