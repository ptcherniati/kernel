/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.filecomp.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.mga.business.composite.groups.Group;

/**
 *
 * @author tcherniatinsky
 */
@Entity
@Table(name = GroupFileComp.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {GroupFileComp.FILE_COMP_FIELD_NAME, GroupFileComp.GROUP_FIELD_NAME}))
public class GroupFileComp implements Serializable{

    /**
     *
     */
    public static final String TABLE_NAME = "Group_FileComp";

    /**
     *
     */
    public static final String FILE_COMP_FIELD_NAME = "file_comp_id";

    /**
     *
     */
    public static final String GROUP_FIELD_NAME = "group_id";

    /**
     *
     */
    @Id
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = FILE_COMP_FIELD_NAME, referencedColumnName = FileComp.KEY_ID, nullable = false)
    protected FileComp fileComp;

    /**
     *
     */
    @Id
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = GROUP_FIELD_NAME, referencedColumnName = Group.ID_JPA, nullable = false)
    protected Group group;

    /**
     *
     */
    public GroupFileComp() {
    }

    /**
     *
     * @return
     */
    public FileComp getFileComp() {
        return fileComp;
    }

    /**
     *
     * @param fileComp
     */
    public void setUser(FileComp fileComp) {
        this.fileComp = fileComp;
    }

    /**
     *
     * @return
     */
    public Group getGroup() {
        return group;
    }

    /**
     *
     * @param group
     */
    public void setGroup(Group group) {
        this.group = group;
    }

}
