package org.inra.ecoinfo.refdata;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import org.inra.ecoinfo.refdata.config.Metadata;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IMetadataManager.
 */
public interface IMetadataManager extends Serializable {

    /**
     * The Constant ID_BEAN @link(String).
     */
    String ID_BEAN = "metadataManager";

    /**
     * Builds the model grid metadata.
     *
     * @param refdataName the refdata name
     * @return the model grid metadata
     * @throws BusinessException the business exception
     * @link(String) the refdata name
     */
    @SuppressWarnings("rawtypes")
    ModelGridMetadata buildModelGridMetadata(String refdataName) throws BusinessException;

    /**
     * Retrieve all sites.
     *
     * @return tous les sites
     */
    List<Site> retrieveAllSites();

    /**
     * Retrieve restricted metadatas.
     *
     * @return the list
     * @throws BusinessException the business exception
     */
    List<Metadata> retrieveRestrictedMetadatas() throws BusinessException;

    /**
     * Save and delete model grid metadata.
     *
     * @param modelGridMetadata the model grid metadata
     * @param linesModelGridMetadataEdited the lines model grid metadata edited
     * @param nameRefdata the name refdata
     * @throws BusinessException the business exception
     * @link(ModelGridMetadata) the model grid metadata
     * @link(Set<LineModelGridMetadata>) the lines model grid metadata edited
     * @link(String) the name refdata
     */
    @SuppressWarnings("rawtypes")
    void saveAndDeleteModelGridMetadata(ModelGridMetadata modelGridMetadata, Set<LineModelGridMetadata> linesModelGridMetadataEdited, String nameRefdata) throws BusinessException;

    /**
     * Save model grid metadata.
     *
     * @param modelGridMetadata the model grid metadata
     * @param linesModelGridMetadataEdited the lines model grid metadata edited
     * @param nameRefdata the name refdata
     * @throws BusinessException the business exception
     * @link(ModelGridMetadata) the model grid metadata
     * @link(Set<LineModelGridMetadata>) the lines model grid metadata edited
     * @link(String) the name refdata
     */
    @SuppressWarnings("rawtypes")
    void saveModelGridMetadata(ModelGridMetadata modelGridMetadata, Set<LineModelGridMetadata> linesModelGridMetadataEdited, String nameRefdata) throws BusinessException;

    /**
     * Upload metadatas.
     *
     * @param file the file
     * @param metadataCode the metadata code
     * @param originalFilename the original filename
     * @param login the login
     * @throws BusinessException the business exception
     * @link(File) the file
     * @link(String) the metadata code
     * @link(String) the original filename
     * @link(String) the login
     */
    void uploadMetadatas(File file, String metadataCode, String originalFilename, String login) throws BusinessException;
}
