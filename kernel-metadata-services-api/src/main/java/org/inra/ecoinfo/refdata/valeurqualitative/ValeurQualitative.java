package org.inra.ecoinfo.refdata.valeurqualitative;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.NaturalId;

/**
 * The Class ValeurQualitative.
 */
@Entity
@Table(name = ValeurQualitative.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {ValeurQualitative.ATTRIBUTE_JPA_VALUE, ValeurQualitative.ATTRIBUTE_JPA_CODE, ValeurQualitative.ATTRIBUTE_JPA_TYPE_LIST}))
public class ValeurQualitative implements Serializable, IValeurQualitative {


    static final String VALEUR_QUALITATIVE = "valeur_qualitative";

    /**
     * Default serial version UID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The valeur @link(String).
     */
    @Column(nullable = false, name = IValeurQualitative.ATTRIBUTE_JPA_VALUE)
    @NaturalId
    private String valeur;
    /**
     * The code @link(String).
     */
    @Column(nullable = false, name = IValeurQualitative.ATTRIBUTE_JPA_CODE)
    @NaturalId
    private String code;

    /**
     *
     */
    @Column(name = ATTRIBUTE_JPA_TYPE_LIST)
    @NaturalId
    protected String typeListe = VALEUR_QUALITATIVE;
    /**
     * The id @link(Long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = IValeurQualitative.ID_JPA)
    protected Long id;

    /**
     * Instantiates a new valeur qualitative.
     */
    public ValeurQualitative() {
        super();
    }

    /**
     * Instantiates a new valeur qualitative.
     *
     * @param code the nom
     * @param value the value
     * @link(String) the nom
     * @link(String) the value
     */
    public ValeurQualitative(final String code, final String value) {
        super();
        this.code = code;
        this.valeur = value;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param typeListe
     */
    public void setTypeListe(String typeListe) {
        this.typeListe = typeListe;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the code.
     *
     * @return the code
     * @see
     * org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#getCode()
     */
    @Override
    public String getCode() {
        return code;
    }

    /**
     * Gets the id.
     *
     * @return the id
     * @see
     * org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#getId()
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Gets the valeur.
     *
     * @return the valeur
     * @see
     * org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#getValeur()
     */
    @Override
    public String getValeur() {
        return valeur;
    }

    /**
     * Sets the valeur.
     *
     * @param valeur the new valeur
     * @see
     * org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#setValeur(java.lang.String)
     */
    @Override
    public void setValeur(final String valeur) {
        this.valeur = valeur;
    }
}
