package org.inra.ecoinfo.refdata.variable;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;

/**
 * The Class Variable.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = Variable.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = Variable.ID_JPA)
public class Variable extends Nodeable implements Serializable {

    /**
     * The Constant PERSISTENT_NAME_ID @link(String).
     */
    public static final String PERSISTENT_NAME_ID = "id";
    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "var_id";
    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    public static final String NAME_ENTITY_JPA = "Variable";
    /**
     * The Constant ATTRIBUTE_JPA__DEFINITION @link(String).
     */
    public static final String ATTRIBUTE_JPA_DEFINITION = "definition";
    /**
     * The Constant RESULTSET_MAPPING_JPA @link(String).
     */
    public static final String RESULTSET_MAPPING_JPA = "variables";
    /**
     * The Constant VARIABLE_NAME_ID @link(String).
     */
    public static final String VARIABLE_NAME_ID = "id";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The affichage @link(String).
     */
    @Column(nullable = true)
    private String affichage;
    /**
     * The is qualitative @link(Boolean).
     */
    @Column(name = "isQualitative", columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean isQualitative = false;
    /**
     * The definition @link(String).
     */
    @Column(columnDefinition = "TEXT", name = Variable.ATTRIBUTE_JPA_DEFINITION)
    private String definition = "";

    /**
     * Instantiates a new variable.
     */
    public Variable() {
        super();
    }

    /**
     * Instantiates a new variable.
     *
     * @param id the id
     * @link(Long) the id
     */
    public Variable(final Long id) {
        super.setId(id);
    }

    /**
     * Instantiates a new variable.
     *
     * @param code
     * @param definition the definition
     * @param affichage the affichage
     * @param isQualitative the is qualitative
     * @link(String) the nom
     * @link(String) the definition
     * @link(String) the affichage
     * @link(Boolean) the is qualitative
     */
    public Variable(final String code, final String definition, final String affichage, final Boolean isQualitative) {
        super(code);
        setDefinition(definition);
        this.affichage = affichage;
        this.isQualitative = isQualitative;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(getCode());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Variable other = (Variable) obj;
        return Objects.equals(getCode(), other.getCode());
    }

    /**
     * Clone only attributes.
     *
     * @return the variable
     */
    public Variable clone() {
        Variable variable = null;
        try {
            variable = (Variable) super.clone();
            variable.setId(getId());
            variable.setCode(code);
            variable.setAffichage(affichage);
            variable.setOrder(order);
            variable.setIsQualitative(isQualitative);
        } catch (CloneNotSupportedException cnse) {
            LOGGER.error(cnse.getMessage(), cnse);
        }
        return variable;
    }

    /**
     * Gets the affichage.
     *
     * @return the affichage
     */
    public String getAffichage() {
        return affichage;
    }

    /**
     * Sets the affichage.
     *
     * @param affichage the new affichage
     */
    public void setAffichage(final String affichage) {
        this.affichage = affichage;
    }

    /**
     * Gets the definition.
     *
     * @return the definition
     */
    public String getDefinition() {
        return definition == null ? "" : definition;
    }

    /**
     * Sets the definition.
     *
     * @param definition the new definition
     */
    public void setDefinition(final String definition) {
        this.definition = definition;
    }

    /**
     * Gets the checks if is qualitative.
     *
     * @return the checks if is qualitative
     */
    public Boolean getIsQualitative() {
        return isQualitative;
    }

    /**
     * Sets the checks if is qualitative.
     *
     * @param isQualitative the new checks if is qualitative
     */
    public void setIsQualitative(final Boolean isQualitative) {
        this.isQualitative = isQualitative;
    }

    /**
     *
     * @return
     */
    @Override
    public String getName() {
        return this.code;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) Variable.class;
    }

    @Override
    public int compareTo(INodeable o) {
        return code.compareTo(o.getCode());
    }
}
