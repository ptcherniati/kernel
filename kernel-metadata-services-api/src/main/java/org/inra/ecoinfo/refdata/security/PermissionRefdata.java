package org.inra.ecoinfo.refdata.security;

import java.io.Serializable;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class PermissionRefdata.
 */
public class PermissionRefdata implements Serializable{

    /**
     * The administration @link(Boolean).
     */
    private Boolean administration = false;
    /**
     * The code @link(String).
     */
    private String code;
    /**
     * The delete @link(Boolean).
     */
    private Boolean delete = false;
    /**
     * The download @link(Boolean).
     */
    private Boolean download = false;
    /**
     * The edition @link(Boolean).
     */
    private Boolean edition = false;
    /**
     * The local resource @link(String).
     */
    private String localResource;
    /**
     * The resource @link(String).
     */
    private String resource;

    /**
     * Instantiates a new permission refdata.
     */
    public PermissionRefdata() {
    }

    /**
     * Instantiates a new permission refdata.
     *
     * @param resource the resource
     * @param localizedResource the localized resource
     * @param administration the administration
     * @param edition the edition
     * @param delete the delete
     * @param download the download
     * @link(String) the resource
     * @link(String) the localized resource
     * @link(Boolean) the administration
     * @link(Boolean) the edition
     * @link(Boolean) the delete
     * @link(Boolean) the download
     */
    public PermissionRefdata(final String resource, final String localizedResource,
            final Boolean administration, final Boolean edition, final Boolean delete,
            final Boolean download) {
        super();
        setResource(resource);
        this.localResource = localizedResource;
        this.administration = administration;
        this.edition = edition;
        this.delete = delete;
        this.download = download;
    }

    /**
     * Gets the administration.
     *
     * @return the administration
     */
    public Boolean getAdministration() {
        return administration;
    }

    /**
     * Sets the administration.
     *
     * @param administration the new administration
     */
    public void setAdministration(final Boolean administration) {
        this.administration = administration;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the delete.
     *
     * @return the delete
     */
    public Boolean getDelete() {
        return delete;
    }

    /**
     * Sets the delete.
     *
     * @param delete the new delete
     */
    public void setDelete(final Boolean delete) {
        this.delete = delete;
    }

    /**
     * Gets the download.
     *
     * @return the download
     */
    public Boolean getDownload() {
        return download;
    }

    /**
     * Sets the download.
     *
     * @param download the new download
     */
    public void setDownload(final Boolean download) {
        this.download = download;
    }

    /**
     * Gets the edition.
     *
     * @return the edition
     */
    public Boolean getEdition() {
        return edition;
    }

    /**
     * Sets the edition.
     *
     * @param edition the new edition
     */
    public void setEdition(final Boolean edition) {
        this.edition = edition;
    }

    /**
     * Gets the local resource.
     *
     * @return the local resource
     */
    public String getLocalResource() {
        return localResource;
    }

    /**
     * Gets the resource.
     *
     * @return the resource
     */
    public String getResource() {
        return resource;
    }

    /**
     * Sets the resource.
     *
     * @param resource the new resource
     */
    public void setResource(final String resource) {
        this.resource = resource;
        this.code = Utils.createCodeFromString(resource);
    }
}
