package org.inra.ecoinfo.refdata;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class LineModelGridMetadata.
 */
public class LineModelGridMetadata implements Serializable {

    /**
     * The columns model grid metadatas @link(List<ColumnModelGridMetadata>).
     */
    private final List<ColumnModelGridMetadata> columnsModelGridMetadatas = new LinkedList<>();
    /**
     * The is new @link(Boolean).
     */
    private Boolean isNew = false;
    /**
     * The to delete @link(Boolean).
     */
    private Boolean toDelete = false;

    /**
     * Gets the column model grid metadata at.
     *
     * @param index the index
     * @return the column model grid metadata at
     * @link(Integer) the index
     */
    public ColumnModelGridMetadata getColumnModelGridMetadataAt(final Integer index) {
        return columnsModelGridMetadatas.get(index);
    }

    /**
     * Gets the columns model grid metadatas.
     *
     * @return the columns model grid metadatas
     */
    public List<ColumnModelGridMetadata> getColumnsModelGridMetadatas() {
        return columnsModelGridMetadatas;
    }

    /**
     * Gets the checks if is new.
     *
     * @return the checks if is new
     */
    public Boolean getIsNew() {
        return isNew;
    }

    /**
     * Sets the checks if is new.
     *
     * @param isNew the new checks if is new
     */
    public void setIsNew(final Boolean isNew) {
        this.isNew = isNew;
    }

    /**
     * Gets the checks if is valid.
     *
     * @return the checks if is valid
     */
    public Boolean getIsValid() {
        return columnsModelGridMetadatas.stream().noneMatch((columnModelGridMetadata) -> (!columnModelGridMetadata.getIsValid()));
    }

    /**
     * Gets the key path.
     *
     * @return the key path
     */
    public String getKeyPath() {
        String keyPath = "";
        for (final ColumnModelGridMetadata column : getColumnsModelGridMetadatas()) {
            if (column.getIsNKey()) {
                final String key = column.getValue();
                keyPath = keyPath.concat(Utils.SEPARATOR_URL).concat(key == null ? "" : key);
            }
        }
        return keyPath;
    }

    /**
     * Gets the to delete.
     *
     * @return the to delete
     */
    public Boolean getToDelete() {
        return toDelete;
    }

    /**
     * Sets the to delete.
     *
     * @param toDelete the new to delete
     */
    public void setToDelete(final Boolean toDelete) {
        this.toDelete = toDelete;
    }
}
