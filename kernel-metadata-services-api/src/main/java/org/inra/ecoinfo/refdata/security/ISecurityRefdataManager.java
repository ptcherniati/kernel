package org.inra.ecoinfo.refdata.security;

import java.util.List;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface ISecurityRefdataManager.
 */
public interface ISecurityRefdataManager extends IPolicyManager {

    /**
     * The Constant PRIVILEGE_TYPE_REFDATA @link(String).
     */
    String PRIVILEGE_TYPE_REFDATA = "refdata";
    /**
     * The Constant ROLE_ADMINISTRATION @link(String).
     */
    String activity_ADMINISTRATION = "administration";
    /**
     * The Constant ROLE_DELETE @link(String).
     */
    String activity_DELETE = "delete";
    /**
     * The Constant ROLE_DOWNLOAD @link(String).
     */
    String activity_DOWNLOAD = "download";
    /**
     * The Constant ROLE_EDITION @link(String).
     */
    String activity_EDITION = "edition";

    /**
     * Gets the restricted permissions by user id.
     *
     * @param id the id
     * @return the restricted permissions by user id
     * @throws BusinessException the business exception
     * @link(Long) the id
     */
    List<PermissionRefdata> getRestrictedPermissionsByUserId(Long id) throws BusinessException;

    /**
     * Update permissions user.
     *
     * @param utilisateurId the utilisateur id
     * @param permissions the permissions
     * @throws BusinessException the business exception
     * @link(Long) the utilisateur id
     * @link(List<PermissionRefdata>) the permissions
     */
    void updatePermissionsUser(Long utilisateurId, List<PermissionRefdata> permissions) throws BusinessException;
}
