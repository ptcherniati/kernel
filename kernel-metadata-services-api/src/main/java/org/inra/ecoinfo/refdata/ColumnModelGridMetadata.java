package org.inra.ecoinfo.refdata;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class ColumnModelGridMetadata.
 */
public class ColumnModelGridMetadata implements Serializable{


    /**
     * The Constant NULL_KEY @link(String).
     */
    public static final String NULL_KEY = "nullKeyForListOfValuesPossibles";
    /**
     * The Constant NULL_MAP_POSSIBLES @link(Map<String,String[]>).
     */
    public static final Map<String, String[]> NULL_MAP_POSSIBLES = null;
    /**
     * The Constant NULL_VALUE_POSSIBLES @link(String[]).
     */
    public static final String[] NULL_VALUE_POSSIBLES = null;
    /**
     * The auto complete @link(AutoComplete).
     */
    private AutoComplete autoComplete = null;
    /**
     * The header @link(String).
     */
    private String header;
    /**
     * The is long string @link(Boolean).
     */
    private Boolean isLongString = false;
    /**
     * The is n key @link(Boolean).
     */
    private Boolean isNKey = false;
    /**
     * The mandatory @link(Boolean).
     */
    private Boolean mandatory = false;
    /**
     * The parent @link(ColumnModelGridMetadata).
     */
    private ColumnModelGridMetadata parent;
    /**
     * The ref autocomplete @link(boolean).
     */
    private boolean refAutocomplete = false;
    /**
     * The ref by @link(ColumnModelGridMetadata).
     */
    private ColumnModelGridMetadata refBy;
    /**
     * The refs @link(List<ColumnModelGridMetadata>).
     */
    private List<ColumnModelGridMetadata> refs;
    /**
     * The value @link(String).
     */
    private String value;
    /**
     * The values possibles @link(Map<String,String[]>).
     */
    private Map<String, String[]> valuesPossibles;
     
    /**
     * Instantiates a new column model grid metadata.
     *
     * @param value the value
     * @param valuesPossibles the values possibles
     * @param header the header
     * @param isNKey the is n key
     * @param isLongString the is long string
     * @param bln2
     * @param mandatory the mandatory
     * @link(Object) the value
     * @link(Map<String,String[]>) the values possibles
     * @link(String) the header
     * @link(Boolean) the is n key
     * @link(Boolean) the is long string
     * @link(Boolean) the mandatory
     */
    public ColumnModelGridMetadata(final Object value, final Map<String, String[]> valuesPossibles, final String header, final Boolean isNKey, final Boolean isLongString, final Boolean mandatory) {
        this(value, header);
        setValuesPossibles(valuesPossibles);
        this.isNKey = isNKey;
        this.isLongString = isLongString;
        this.mandatory = mandatory;
    }

    /**
     * Instantiates a new column model grid metadata.
     *
     * @param value the value
     * @param string
     * @param header the header
     * @link(Object) the value
     * @link(String) the header
     */
    public ColumnModelGridMetadata(final Object value, final String header) {
        super();
        String stringValue = null;
        if (value != null) {
            stringValue = value.toString();
        }
        this.value = stringValue;
        this.header = header;
        this.isNKey = false;
        this.isLongString = false;
    }

    /**
     * Instantiates a new column model grid metadata.
     *
     * @param value the value
     * @param valuesPossibles the values possibles
     * @param header the header
     * @param isNKey the is n key
     * @param isLongString the is long string
     * @param bln2
     * @param mandatory the mandatory
     * @link(Object) the value
     * @link(String[]) the values possibles
     * @link(String) the header
     * @link(Boolean) the is n key
     * @link(Boolean) the is long string
     * @link(Boolean) the mandatory
     */
    public ColumnModelGridMetadata(final Object value, final String[] valuesPossibles, final String header, final Boolean isNKey, final Boolean isLongString, final Boolean mandatory) {
        this(value, header);
        Map<String, String[]> map = new HashMap<>();
        if (valuesPossibles == null) {
            map = null;
        } else {
            map.put(null, valuesPossibles);
        }
        setValuesPossibles(map);
        this.isNKey = isNKey;
        this.isLongString = isLongString;
        this.mandatory = mandatory;
    }

    /**
     * Gets the auto complete.
     *
     * @return the auto complete
     */
    public AutoComplete getAutoComplete() {
        return autoComplete;
    }

    /**
     * Gets the header.
     *
     * @return the header
     */
    public String getHeader() {
        return header;
    }

    /**
     * Sets the header.
     *
     * @param header the new header
     */
    public void setHeader(final String header) {
        this.header = header;
    }

    /**
     * Gets the checks if is list values.
     *
     * @return the checks if is list values
     */
    public Boolean getIsListValues() {
        return valuesPossibles != null;
    }

    /**
     * Gets the checks if is long string.
     *
     * @return the checks if is long string
     */
    public Boolean getIsLongString() {
        return isLongString;
    }

    /**
     * Sets the checks if is long string.
     *
     * @param isLongString the new checks if is long string
     */
    public void setIsLongString(final Boolean isLongString) {
        this.isLongString = isLongString;
    }

    /**
     * Gets the checks if is n key.
     *
     * @return the checks if is n key
     */
    public Boolean getIsNKey() {
        return isNKey;
    }

    /**
     * Sets the checks if is n key.
     *
     * @param isNKey the new checks if is n key
     */
    public void setIsNKey(final Boolean isNKey) {
        this.isNKey = isNKey;
        this.mandatory = true;
    }

    /**
     * Gets the checks if is valid.
     *
     * @return the checks if is valid
     */
    public Boolean getIsValid() {
        return !(mandatory && Strings.isNullOrEmpty(value));
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        String key = null;
        if (refBy != null) {
            key = refBy.getValue();
            if (refBy.getKey() != null) {
                key = refBy.getKey().concat(Utils.SEPARATOR_URL).concat(key);
            }
        }
        return key;
    }

    /**
     * Gets the list of values possibles.
     *
     * @return the list of values possibles
     */
    public String[] getListOfValuesPossibles() {
        if (valuesPossibles != null && (getKey() == null || getKey().equals(ColumnModelGridMetadata.NULL_KEY))) {
            return valuesPossibles.isEmpty() ? null : valuesPossibles.values().iterator().next();
        }
        return valuesPossibles == null ? new String[]{} : valuesPossibles.get(getKey());
    }

    /**
     * Gets the mandatory.
     *
     * @return the mandatory
     */
    public Boolean getMandatory() {
        return mandatory;
    }

    /**
     * Sets the mandatory.
     *
     * @param mandatory the new mandatory
     */
    public void setMandatory(final Boolean mandatory) {
        this.mandatory = mandatory;
    }

    /**
     * Gets the parent.
     *
     * @return the parent
     */
    public ColumnModelGridMetadata getParent() {
        return parent;
    }

    /**
     * Sets the parent.
     *
     * @param parent the new parent
     */
    public void setParent(final ColumnModelGridMetadata parent) {
        this.parent = parent;
    }

    /**
     * Gets the ref autocomplete.
     *
     * @return the ref autocomplete
     */
    public boolean getRefAutocomplete() {
        return refAutocomplete;
    }

    /**
     * Sets the ref autocomplete.
     *
     * @param refAutocomplete the new ref autocomplete
     */
    public void setRefAutocomplete(final boolean refAutocomplete) {
        this.refAutocomplete = refAutocomplete;
    }

    /**
     * Gets the ref by.
     *
     * @return the ref by
     */
    public ColumnModelGridMetadata getRefBy() {
        return refBy;
    }

    /**
     * Sets the ref by.
     *
     * @param refBy the new ref by
     */
    public void setRefBy(final ColumnModelGridMetadata refBy) {
        this.refBy = refBy;
    }

    /**
     * Gets the ref by value.
     *
     * @return the ref by value
     */
    public String getRefByValue() {
        return refBy == null ? null : refBy.getValue();
    }

    /**
     * Gets the refs.
     *
     * @return the refs
     */
    public List<ColumnModelGridMetadata> getRefs() {
        return refs;
    }

    /**
     * Sets the refs.
     *
     * @param refs the new refs
     */
    public void setRefs(final List<ColumnModelGridMetadata> refs) {
        if (this.refs != null) {
            this.refs.stream().forEach((columnModelGridMetadata) -> {
                columnModelGridMetadata.refBy = null;
            });
        }
        this.refs = refs;
        if (refs == null) {
            return;
        }
        this.refs.stream().forEach((columnModelGridMetadata) -> {
            columnModelGridMetadata.refBy = this;
        });
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final String value) {
        this.value = value;
        if (refs != null) {
            refs.stream().forEach((ref) -> {
                ref.updateValue();
            });
        }
    }

    /**
     * Gets the values possibles.
     *
     * @return the values possibles
     */
    public Map<String, String[]> getValuesPossibles() {
        return valuesPossibles;
    }

    /**
     * Sets the values possibles.
     *
     * @param valuesPossibles the values possibles
     * @link(Map<String,String[]>) the values possibles
     */
    public void setValuesPossibles(final Map<String, String[]> valuesPossibles) {
        this.valuesPossibles = valuesPossibles;
        updateValue();
    }

    /**
     * Sets the auto complete.
     *
     * @param possibilities the possibilities
     * @param refBy the ref by
     * @link(Map<String,List<String>>) the possibilities
     * @link(ColumnModelGridMetadata) the ref by
     */
    public void setAutoComplete(final Map<String, List<String>> possibilities, final ColumnModelGridMetadata refBy) {
        this.autoComplete = new AutoComplete(possibilities, refBy);
    }

    /**
     * Update value.
     */
    private void updateValue() {
        if (valuesPossibles == null) {
            return;
        }
        getListOfValuesPossibles();
        /*if (listOfValuesPossibles == null) {
            return;
        }
        for (String string : getListOfValuesPossibles()) {
            if (string.equals(value)) {
                return;
            }
        }*/
    }

    /**
     * The Class AutoComplete.
     */
    public static class AutoComplete {

        /**
         * The possibilities @link(Map<String,List<String>>).
         */
        private Map<String, List<String>> possibilities;
        /**
         * The ref by @link(ColumnModelGridMetadata).
         */
        private ColumnModelGridMetadata refBy;
        /**
         * The return list @link(List<String>).
         */
        private List<String> returnList = new ArrayList<>();

        /**
         * Instantiates a new auto complete.
         *
         * @param possibilities the possibilities
         * @param refBy the ref by
         * @link(Map<String,List<String>>) the possibilities
         * @link(ColumnModelGridMetadata) the ref by
         */
        public AutoComplete(final Map<String, List<String>> possibilities, final ColumnModelGridMetadata refBy) {
            super();
            this.possibilities = possibilities;
            setRefBy(refBy);
        }

        /**
         * Gets the list of possibilities by ref.
         *
         * @return the list of possibilities by ref
         */
        public List<String> getListOfPossibilitiesByRef() {
            return this.refBy == null ? possibilities.get("") : possibilities.get(this.refBy.value);
        }

        /**
         * Gets the possibilities.
         *
         * @return the possibilities
         */
        public Map<String, List<String>> getPossibilities() {
            return possibilities;
        }

        /**
         * Gets the ref by.
         *
         * @return the ref by
         */
        public ColumnModelGridMetadata getRefBy() {
            return refBy;
        }

        /**
         * Gets the return list.
         *
         * @return the return list
         */
        public List<String> getReturnList() {
            return returnList;
        }

        /**
         * Sets the possibilities.
         *
         * @param possibilities the possibilities
         * @link(Map<String,List<String>>) the possibilities
         */
        public void setPossibilities(final Map<String, List<String>> possibilities) {
            this.possibilities = possibilities;
        }

        /**
         * Sets the ref by.
         *
         * @param refBy the new ref by
         */
        public void setRefBy(final ColumnModelGridMetadata refBy) {
            this.refBy = refBy;
            refBy.refAutocomplete = true;
        }

        /**
         * Sets the return list.
         *
         * @param returnList the new return list
         */
        public void setReturnList(final List<String> returnList) {
            this.returnList = returnList;
        }

        /**
         * Suggest list.
         *
         * @param object the object
         * @return the list
         * @link(Object) the object
         */
        public List<String> suggestList(final Object object) {
            final List<String> listOfPossibilitiesByRef = getListOfPossibilitiesByRef();
            if (listOfPossibilitiesByRef == null) {
                return new LinkedList<>();
            }
            final String name = (String) object;
            returnList.clear();
            /* ________________Logic to filter data_____________ */
            listOfPossibilitiesByRef.stream().filter((listOfPossibilitiesByRef1) -> (listOfPossibilitiesByRef1.startsWith(name))).forEach((listOfPossibilitiesByRef1) -> {
                returnList.add(listOfPossibilitiesByRef1);
            });
            return returnList;
        }
    }
}
