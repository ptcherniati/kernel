package org.inra.ecoinfo.refdata.datatypevariableunite;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 * The Class DatatypeVariableUnite.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = DatatypeVariableUnite.NAME_ENTITY_JPA, 
       uniqueConstraints = @UniqueConstraint(columnNames = {Variable.ID_JPA, DataType.ID_JPA, Unite.ID_JPA}),
       indexes = {
           @Index(name = "dvu_uni_idx", columnList = Unite.ID_JPA),
           @Index(name = "dvu_var_idx", columnList = Variable.ID_JPA),
           @Index(name = "dvu_dty_idx", columnList = DataType.ID_JPA)})
@PrimaryKeyJoinColumn(name = "DatatypeVariableUnite_ID")
public class DatatypeVariableUnite extends Nodeable implements Serializable {



    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "vdt_id";
    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    public static final String NAME_ENTITY_JPA = "datatype_unite_variable_vdt";
    /**
     * Default serial version UID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The datatype @link(DataType).
     */
    @XmlElement(name = "datatype")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = DataType.ID_JPA, referencedColumnName = DataType.ID_JPA, nullable = false)
    private DataType datatype;
    /**
     * The unite @link(Unite).
     */
    @XmlElement(name = "unite")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = Unite.ID_JPA, referencedColumnName = Unite.ID_JPA, nullable = false)
    private Unite unite;
    /**
     * The variable @link(Variable).
     */
    @XmlElement(name = "variable")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = Variable.ID_JPA, referencedColumnName = Variable.ID_JPA, nullable = false)
    private Variable variable;

    /**
     * Instantiates a new datatype variable unite.
     */
    public DatatypeVariableUnite() {
        super();
    }

    /**
     * Instantiates a new datatype variable unite.
     *
     * @param datatype the datatype
     * @param unite the unite
     * @param variable the variable
     * @link(DataType) the datatype
     * @link(Unite) the unite
     * @link(Variable) the variable
     */
    public DatatypeVariableUnite(final DataType datatype, final Unite unite, final Variable variable) {
        super();
        this.datatype = datatype;
        this.unite = unite;
        this.variable = variable;
        setCode(String.format("%s_%s_%s", datatype.getCode(), variable.getCode(), unite.getCode()));
    }

    /**
     * Gets the datatype.
     *
     * @return the datatype
     */
    public DataType getDatatype() {
        return datatype;
    }

    /**
     * Sets the datatype.
     *
     * @param datatype the new datatype
     */
    public void setDatatype(final DataType datatype) {
        this.datatype = datatype;
    }

    /**
     * Gets the unite.
     *
     * @return the unite
     */
    public Unite getUnite() {
        return unite;
    }

    /**
     * Sets the unite.
     *
     * @param unite the new unite
     */
    public void setUnite(final Unite unite) {
        this.unite = unite;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public Variable getVariable() {
        return variable;
    }

    /**
     * Sets the variable.
     *
     * @param variable the new variable
     */
    public void setVariable(final Variable variable) {
        this.variable = variable;
    }
    
    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) DatatypeVariableUnite.class;
    }

    /**
     *
     * @return
     */
    @Override
    public String getName() {
       return this.variable.getName();
    }
}
