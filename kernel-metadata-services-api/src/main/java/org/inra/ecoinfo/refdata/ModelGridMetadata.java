package org.inra.ecoinfo.refdata;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class ModelGridMetadata.
 *
 * @param <T> the generic type
 */
public class ModelGridMetadata<T> {

    /**
     * The Constant SEPARATOR_CSV @link(String).
     */
    private static final String SEPARATOR_CSV = ";";
    /**
     * The Constant STRING_EMPTY @link(String).
     */
    private static final String STRING_EMPTY = "";
    /**
     * The headers columns @link(List<String>).
     */
    private final List<String> headersColumns = new LinkedList<>();
    private final List<Column> columns = new LinkedList<>();
    /**
     * The lines model grid metadatas @link(List<LineModelGridMetadata>).
     */
    private final List<LineModelGridMetadata> linesModelGridMetadatas = new LinkedList<>();
    /**
     * The name header dataset descriptor @link(String).
     */
    private final String nameHeaderDatasetDescriptor;
    /**
     * The recorder @link(IMetadataRecorder<T>).
     */
    private final IMetadataRecorder<T> recorder;
    private LineModelGridMetadata selectedLine;
    /**
     * Instantiates a new model grid metadata.
     *
     * @param nameHeaderDatasetDescriptor the name header dataset descriptor
     * @param imr
     * @param recorder the recorder
     * @link(String) the name header dataset descriptor
     * @link(IMetadataRecorder<T>) the recorder
     */
    public ModelGridMetadata(final String nameHeaderDatasetDescriptor, final IMetadataRecorder<T> recorder) {
        super();
        this.nameHeaderDatasetDescriptor = nameHeaderDatasetDescriptor;
        this.recorder = recorder;
    }

    /**
     *
     * @return
     */
    public LineModelGridMetadata getSelectedLine() {
        return selectedLine;
    }

    /**
     *
     * @param selectedLine
     */
    public void setSelectedLine(LineModelGridMetadata selectedLine) {
        this.selectedLine = selectedLine;
    }


    /**
     * Builds the empty line model grid metadata.
     *
     * @return the line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public LineModelGridMetadata buildEmptyLineModelGridMetadata() throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = getNewLineModelGridMetadata(null);
        lineModelGridMetadata.setIsNew(true);
        return lineModelGridMetadata;
    }

    /**
     * Format as csv.
     *
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public String formatAsCSV() throws IOException {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final PrintStream out = new PrintStream(bos, true, Utils.ENCODING_UTF8);
        Integer index = 0;
        for (final String header : headersColumns) {
            out.print(header.concat(index < headersColumns.size() - 1 ? ModelGridMetadata.SEPARATOR_CSV : ModelGridMetadata.STRING_EMPTY));
            index++;
        }
        out.println();
        parseLines(new LinkedList<>(linesModelGridMetadatas), out);
        bos.flush();
        return bos.toString(Utils.ENCODING_UTF8);
    }

    /**
     * Format as csv.
     *
     * @param lines the lines
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(List<LineModelGridMetadata>) the lines
     */
    public String formatAsCSV(final List<LineModelGridMetadata> lines) throws IOException {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final PrintStream out = new PrintStream(bos, true, Utils.ENCODING_UTF8);
        parseLines(lines, out);
        bos.flush();
        return bos.toString(Utils.ENCODING_UTF8);
    }

    /**
     * Format as csv.
     *
     * @param linesModelGridMetadataEdited the lines model grid metadata edited
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(Set<LineModelGridMetadata>) the lines model grid metadata edited
     */
    public String formatAsCSV(final Set<LineModelGridMetadata> linesModelGridMetadataEdited) throws IOException {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final PrintStream out = new PrintStream(bos, true, Utils.ENCODING_UTF8);
        Integer index = 0;
        for (final String header : headersColumns) {
            out.print(header.concat(index < headersColumns.size() - 1 ? ModelGridMetadata.SEPARATOR_CSV : ModelGridMetadata.STRING_EMPTY));
            index++;
        }
        out.println();
        parseLines(new LinkedList<>(linesModelGridMetadataEdited), out);
        bos.flush();
        return bos.toString(Utils.ENCODING_UTF8);
    }

    /**
     * Gets the headers columns.
     *
     * @return the headers columns
     */
    public List<String> getHeadersColumns() {
        return headersColumns;
    }

    /**
     * Gets the line model grid metadata at.
     *
     * @param index the index
     * @return the line model grid metadata at
     * @link(Integer) the index
     */
    public LineModelGridMetadata getLineModelGridMetadataAt(final Integer index) {
        return linesModelGridMetadatas.get(index);
    }

    /**
     * Gets the lines model grid metadatas.
     *
     * @return the lines model grid metadatas
     */
    public List<LineModelGridMetadata> getLinesModelGridMetadatas() {
        return linesModelGridMetadatas;
    }

    /**
     * Gets the name header dataset descriptor.
     *
     * @return the name header dataset descriptor
     */
    public String getNameHeaderDatasetDescriptor() {
        return nameHeaderDatasetDescriptor;
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param element the element
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(T) the element
     */
    public LineModelGridMetadata getNewLineModelGridMetadata(final T element) throws BusinessException {
        return recorder.getNewLineModelGridMetadata(element);
    }

    /**
     *
     * @return
     */
    public List<Column> getColumns() {
        return columns;
    }

    /**
     * Parses the lines.
     *
     * @param lines the lines
     * @param out the out
     * @link(List<LineModelGridMetadata>) the lines
     * @link(PrintStream) the out
     */
    private void parseLines(final List<LineModelGridMetadata> lines, final PrintStream out) {
        Integer index;
        for (final LineModelGridMetadata lineModelGridMetadata : lines) {
            index = 0;
            final Integer countColumns = lineModelGridMetadata.getColumnsModelGridMetadatas().size();
            for (final ColumnModelGridMetadata columnModelGridMetadata : lineModelGridMetadata.getColumnsModelGridMetadatas()) {
                out.print(columnModelGridMetadata.getValue() == null ? index < countColumns - 1 ? ModelGridMetadata.SEPARATOR_CSV : ModelGridMetadata.STRING_EMPTY : columnModelGridMetadata.getValue().concat(
                        index < countColumns - 1 ? ModelGridMetadata.SEPARATOR_CSV : ModelGridMetadata.STRING_EMPTY));
                index++;
            }
            out.println();
        }
    }

    /**
     *
     * @param column
     * @param lineIndex
     * @param columnIndex
     * @return
     */
    protected boolean isValidColum(Column column, int lineIndex, int columnIndex) {
        LineModelGridMetadata lineMedatada = getLineModelGridMetadataAt(lineIndex);
        if (lineMedatada == null) {
            return false;
        }
        ColumnModelGridMetadata columnMedatada = lineMedatada.getColumnModelGridMetadataAt(columnIndex);
        if (columnMedatada == null) {
            return false;
        }

        return false;
    }
}
