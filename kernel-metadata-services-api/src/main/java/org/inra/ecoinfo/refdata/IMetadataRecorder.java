/**
 * OREILacs project - see LICENCE.txt for use created: 8 avr. 2009 14:42:53
 */
package org.inra.ecoinfo.refdata;

import java.io.File;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IMetadataRecorder.
 *
 * @param <T> the generic type
 * @author "Antoine Schellenberger"
 */
public interface IMetadataRecorder<T> {

    /**
     * Builds the model grid.
     *
     * @return the model grid metadata
     * @throws BusinessException the business exception
     */
    @SuppressWarnings("rawtypes")
    ModelGridMetadata buildModelGrid() throws BusinessException;

    /**
     * Delete process bytes.
     *
     * @param data the data
     * @throws BusinessException the business exception
     * @link(byte[]) the data
     */
    void deleteProcessBytes(byte[] data) throws BusinessException;

    /**
     * Gets the new line model grid metadata.
     *
     * @param element the element
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(T) the element
     */
    LineModelGridMetadata getNewLineModelGridMetadata(T element) throws BusinessException;

    /**
     * Process bytes.
     *
     * @param data the data
     * @throws BusinessException the business exception
     * @link(byte[]) the data
     */
    void processBytes(byte[] data) throws BusinessException;

    /**
     * Teste la présence de mise à jour et en cas d'absence insère les données.
     *
     * @param file the file
     * @throws BusinessException the business exception
     * @link(File) the file
     */
    void processFile(File file) throws BusinessException;

    /**
     * Insère les métadonnées sans tester l'existence de mise à jour.
     *
     * @param file the file
     * @throws BusinessException the business exception
     * @link(File) the file
     */
    void processRecordOnly(File file) throws BusinessException;
}
