/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 15:08:27
 */
package org.inra.ecoinfo.refdata.unite;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.NaturalId;

/**
 * The Class Unite.
 *
 * @author "Antoine Schellenberger"
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = Unite.NAME_ENTITY_JPA)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Unite implements Serializable {

    /**
     * The Constant PERSISTENT_NAME_ID @link(String).
     */
    public static final String PERSISTENT_NAME_ID = "id";
    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "uni_id";
    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    public static final String NAME_ENTITY_JPA = "unite";
    /**
     * The Constant ATTRIBUTE_JPA_NAME @link(String).
     */
    public static final String ATTRIBUTE_JPA_NAME = "name";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_CODE = "code";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The code @link(String).
     */
    @XmlElement(name = "codeUnite")
    @Column(nullable = false, unique = true, name = Unite.ATTRIBUTE_JPA_CODE)
    @NaturalId
    private String code;
    /**
     * The name @link(String).
     */
    @XmlElement(name = "nameUnite")
    @Column(nullable = false, name = Unite.ATTRIBUTE_JPA_NAME)
    private String name;
    /**
     * The id @link(Long).
     */
    @XmlAttribute
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    protected Long id;

    /**
     * Instantiates a new unite.
     */
    public Unite() {
        super();
    }

    /**
     * Instantiates a new unite.
     *
     * @param code the code
     * @param string1
     * @param name the name
     * @link(String) the code
     * @link(String) the name
     */
    public Unite(final String code, final String name) {
        super();
        this.code = code;
        this.name = name;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }
}
