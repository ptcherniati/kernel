package org.inra.ecoinfo.refdata.config;

import java.util.List;

/**
 * The Interface IMetadataConfiguration.
 */
public interface IMetadataConfiguration {

    /**
     * Adds the metadata.
     *
     * @param metadata the metadata
     */
    void addMetadata(Metadata metadata);

    /**
     * Gets the metadatas.
     *
     * @return the metadatas
     */
    List<Metadata> getMetadatas();

    /**
     * Sets the metadatas.
     *
     * @param metadatas the new metadatas
     */
    void setMetadatas(List<Metadata> metadatas);
}
