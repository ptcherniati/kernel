package org.inra.ecoinfo.refdata.theme;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;

/**
 * Représente un theme scientifique.
 *
 * @author "Antoine Schellenberger"
 */
@Entity
@Table(name = Theme.NAME_ENTITY_JPA)
public class Theme extends Nodeable implements Serializable {

    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "the_id";
    /**
     * The Constant NAME_ATTRIBUTS_DESCRIPTION @link(String).
     */
    public static final String NAME_ATTRIBUTS_DESCRIPTION = "description";
    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    public static final String NAME_ENTITY_JPA = "theme";
    /**
     * The Constant THEME_NAME_ID @link(String).
     */
    public static final String THEME_NAME_ID = "id";
    /**
     * The description @link(String).
     */
    @Column(columnDefinition = "TEXT", name = Theme.NAME_ATTRIBUTS_DESCRIPTION)
    private String description = "";

    /**
     * Instantiates a new theme.
     */
    public Theme() {
    }

    /**
     * Instantiates a new theme.
     *
     * @param code the name
     * @link(String) the name
     */
    public Theme(final String code) {
        super();
        this.setCode(code);
    }

    /**
     * Instantiates a new theme.
     *
     * @param code the nom
     * @param description the description
     * @link(String) the nom
     * @link(String) the description
     */
    public Theme(final String code, final String description) {
        setCode(code);
        setDescription(description);
    }

    /**
     * Clone only attributes.
     *
     * @return the theme
     */
    public Theme clone() {
        Theme theme = null;
        try {
            theme = (Theme) super.clone();
            theme.setId(getId());
            theme.setCode(code);
            theme.setDescription(description);
            theme.setOrder(order);
        } catch (CloneNotSupportedException cnse) {
            LOGGER.error(cnse.getMessage(), cnse);
        }
        return theme;
    }

    /**
     * Compare to.
     *
     * @param o the o
     * @return the int
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final INodeable o) {
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            return 0;
        }
        return getCode() == null ? -1 : getCode().compareTo(o.getCode());
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Theme other = (Theme) obj;
        return Objects.equals(getCode(), other.getCode());
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description == null ? "" : description;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    @Override
    public String getName() {
        return code;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (getCode() == null ? 0 : getCode().hashCode());
        result = prime * result + (description == null ? 0 : description.hashCode());
        return result;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) Theme.class;
    }

    /**
     * To string.
     *
     * @return the string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.code;
    }
}
