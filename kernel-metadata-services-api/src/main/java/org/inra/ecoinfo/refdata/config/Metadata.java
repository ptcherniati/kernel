/**
 * OREILacs project - see LICENCE.txt for use created: 1 juil. 2009 16:33:41
 */
package org.inra.ecoinfo.refdata.config;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class Metadata.
 *  * record a Metadata tag from the configuration.xml file.
 *
 * @author "Antoine Schellenberger"
 */
public class Metadata implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    private static final String KEY = "key";
    /**
     * The daos @link(List<String>).
     */
    private List<String> daos = new LinkedList<>();
    /**
     * The final metadata @link(boolean).
     */
    private boolean finalMetadata = false;
    /**
     * The internationalized names @link(Map<String,String>).
     */
    private Map<String, String> internationalizedNames = new HashMap<>();
    private Map<String, String> internationalizedDescriptions = new HashMap<>();
    private Long order;

    /**
     * The recorder @link(String).
     */
    private String recorder;

    /**
     * Instantiates a new metadata.
     */
    public Metadata() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getOrder() {
        return order;
    }

    /**
     *
     * @param order
     */
    public void setOrder(String order) {
        this.order = Long.parseLong(order);
    }

    /**
     *
     * @param order
     */
    public void setOrder(Long order) {
        this.order = order;
    }

    /**
     *
     * @return
     */
    public boolean isFinalMetadata() {
        return finalMetadata;
    }

    /**
     *
     * @return
     */
    public Map<String, String> getInternationalizedDescriptions() {
        return internationalizedDescriptions;
    }

    /**
     * Adds the dao.
     *
     * @param dao the dao
     * @link(StringBuffer) the dao
     */
    public void addDao(final StringBuilder dao) {
        daos.add(dao.toString());
    }

    /**
     * Adds the internationalized name.
     *
     * @param language the language
     * @param value the value
     * @link(String) the language
     * @link(String) the value
     */
    public void addInternationalizedName(final String language, final String value) {
        internationalizedNames.put(language == null ? Localization.getDefaultLocalisation() : language, value);
    }

    /**
     *
     * @param language
     * @param value
     */
    public void addInternationalizedDescription(final String language, final String value) {
        internationalizedDescriptions.put(language == null ? Localization.getDefaultLocalisation() : language, value);
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        final String defaultName = internationalizedNames.getOrDefault(KEY, internationalizedNames.get(Localization.getDefaultLocalisation()));
        return Utils.createCodeFromString(defaultName);
    }

    /**
     * Gets the daos.
     *
     * @return the daos
     */
    public List<String> getDaos() {
        return daos;
    }

    /**
     * Sets the daos.
     *
     * @param daos the new daos
     */
    public void setDaos(final List<String> daos) {
        this.daos = daos;
    }

    /**
     * Gets the final metadata.
     *
     * @return the final metadata
     */
    public boolean getFinalMetadata() {
        return finalMetadata;
    }

    /**
     * Sets the final metadata.
     *
     * @param finalMetadata the new final metadata
     */
    public void setFinalMetadata(final String finalMetadata) {
        this.finalMetadata = Boolean.parseBoolean(finalMetadata);
    }

    /**
     * Gets the internationalized names.
     *
     * @return the internationalized names
     */
    public Map<String, String> getInternationalizedNames() {
        return internationalizedNames;
    }

    /**
     * Sets the internationalized names.
     *
     * @param internationalizedNames the internationalized names
     * @link(Map<String,String>) the internationalized names
     */
    public void setInternationalizedNames(final Map<String, String> internationalizedNames) {
        this.internationalizedNames = internationalizedNames;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return internationalizedNames.containsKey(KEY) ? internationalizedNames.get(KEY) : internationalizedNames.get(Localization.getDefaultLocalisation());
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return internationalizedDescriptions.containsKey(KEY) ? internationalizedDescriptions.get(KEY) : internationalizedDescriptions.get(Localization.getDefaultLocalisation());
    }

    /**
     * Gets the recorder.
     *
     * @return the recorder
     */
    public String getRecorder() {
        return recorder;
    }

    /**
     * Sets the recorder.
     *
     * @param recorder the new recorder
     */
    public void setRecorder(final String recorder) {
        this.recorder = recorder;
    }
}
