package org.inra.ecoinfo.refdata.site;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import static org.inra.ecoinfo.mga.business.composite.Nodeable.LOGGER;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;

/**
 *
 * @author tcherniatinsky
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@PrimaryKeyJoinColumn(name = Site.RECURENT_NAME_ID)
@Table(name = Site.NAME_ENTITY_JPA)
public class Site extends Nodeable implements Cloneable, Serializable {

 
    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    public static final String NAME_ENTITY_JPA = "Site";
    /**
     * The Constant ENTITY_FIELD_DESCRIPTION @link(String).
     */
    public static final String ENTITY_FIELD_DESCRIPTION = "description";
    
  
    /** The Constant RECURENT_NAME_COLUMN_PARENT_ID @link(String). */
    public static final String RECURENT_NAME_COLUMN_PARENT_ID = "parent";

    /**
     *
     */
    public static final String RECURENT_NAME_ID = "site_id";
    
    /**
     *
     */
    public static final String RECURENT_NAME_PARENT_ID = "parent_site_id";
    
    /** The Constant ENTITY_FIELD_NAME @link(String). */
    public static final String ENTITY_FIELD_NAME = "name";
    
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     * @param code
     * @param parent
     * @return
     */
    public static final String getPathForSiteCodeAndParent(String code, Site parent) {
        return Optional.ofNullable(parent)
                .map(p->p.getPath())
                .map(codeParent->new StringBuilder(codeParent).append(PatternConfigurator.ANCESTOR_SEPARATOR).append(code).toString())
                .orElse(code);
    }
    /** The nom @link(String). */
    @Column(name = Site.ENTITY_FIELD_NAME, nullable = false, unique = false)
    protected String name = "";
    
      /** The parent @link(T). */
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = true)
    @JoinColumn(name = Site.RECURENT_NAME_PARENT_ID, referencedColumnName = Site.RECURENT_NAME_ID, nullable = true)
    protected Site parent;
    
    /**
     * The description @link(String).
     */
    @Column(name = Site.ENTITY_FIELD_DESCRIPTION, nullable = true, columnDefinition = "TEXT")
    private String description;
    /**
     * Instantiates a new site.
     */
    public Site() {
        super();
    }

    /**
     * Instantiates a new site.
     *
     * @param name
     * @param description the description
     * @param parent
     * @link(String) the nom
     * @link(String) the description
     */
    public Site(final String name, final String description, Site parent) {
        super();
        this.code = getPathForSiteCodeAndParent(name, parent);
        this.description = description;
        this.parent= parent;
        this.name = name;
    }

    /**
     * Clone.
     *
     * @return the site
     * @throws CloneNotSupportedException the clone not supported exception
     * @see java.lang.Object#clone()
     */
    @Override
    public Site clone() {
        Site site = null;
        try {
            site = (Site) super.clone();
            site.setId(getId());
            site.setCode(code);
            site.setDescription(description);
            site.setOrder(order);
            site.setParent(parent==null?null:parent.clone());
            site.setPath();
        } catch (CloneNotSupportedException cnse) {
            LOGGER.error(cnse.getMessage(), cnse);
        }
        return site;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see org.inra.ecoinfo.refdata.AbstractRecurentObject#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (getCode() == null ? 0 : getCode().hashCode());
        result = prime * result + (parent == null ? 0 : parent.hashCode());
        return result;
    }

    /**
     * To string.
     *
     * @return the string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.name;
    }
    
    /**
     *
     * @param <T>
     * @return
     */

    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) Site.class;
    }

    /**
     *
     * @return
     */
    public Site getParent() {
        return parent;
    }

    /**
     *
     * @param parent
     */
    public void setParent(Site parent) {
         if (parent == null) {
            return;
        }
        this.parent = parent;
        setPath();
    }
    
    /**
     *
     */
    public void setPath() {
        setCode(getDisplayPath());
    }
    
    /**
     *
     * @return
     */
    public String getPath() {
        return getCode();
    }
    
    
    /**
     *
     * @return
     */
    public String getDisplayPath() {
        return Optional.ofNullable(parent)
                .map(p->p.getDisplayPath())
                .map(pa->new StringBuilder(pa).append(PatternConfigurator.ANCESTOR_SEPARATOR).append(name))
                .map(sb->sb.toString())
                .orElse(name);
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String getName() {
         return name ;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Site other = (Site) obj;
        return Objects.equals(getCode(), other.getCode());
    }
}
