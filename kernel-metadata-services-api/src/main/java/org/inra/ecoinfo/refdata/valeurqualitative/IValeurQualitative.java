package org.inra.ecoinfo.refdata.valeurqualitative;

/**
 * The Interface IValeurQualitative.
 */
public interface IValeurQualitative {

    /**
     * The Constant ID_JPA @link(String).
     */
    String ID_JPA = "vq_id";
    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    String NAME_ENTITY_JPA = "valeur_qualitative";
    /**
     * The Constant ATTRIBUTE_JPA_VALUE @link(String).
     */
    String ATTRIBUTE_JPA_VALUE = "valeur";

    /**
     *
     */
    String ATTRIBUTE_JPA_NAME = "nom";
    /**
     * The Constant ATTRIBUTE_JPA_CODE @link(String).
     */
    String ATTRIBUTE_JPA_CODE = "code";

    /**
     *
     */
    String ATTRIBUTE_JPA_TYPE_LIST = "typelist";

    /**
     * Gets the code.
     *
     * @return the code
     */
    String getCode();

    /**
     * Gets the id.
     *
     * @return the id
     */
    Long getId();

    /**
     * Gets the valeur.
     *
     * @return the valeur
     */
    String getValeur();

    /**
     * Sets the valeur.
     *
     * @param valeur the new valeur
     */
    void setValeur(String valeur);
}
