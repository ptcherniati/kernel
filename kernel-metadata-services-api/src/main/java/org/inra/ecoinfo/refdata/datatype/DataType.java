package org.inra.ecoinfo.refdata.datatype;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class DataType.
 *
 * @author "Antoine Schellenberger"
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = DataType.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = DataType.ID_JPA)
public class DataType extends Nodeable implements Serializable, Cloneable {

    /**
     * The Constant DATATYPE_NAME_ID @link(String).
     */
    public static final String DATATYPE_NAME_ID = "id";
    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "dty_id";
    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    public static final String NAME_ENTITY_JPA = "datatype";
    /**
     * The Constant NAME_ATTRIBUTS_DESCRIPTION @link(String).
     */
    public static final String NAME_ATTRIBUTS_DESCRIPTION = "description";
    /**
     * The Constant NAME_ATTRIBUTS_NAME @link(String).
     */
    public static final String NAME_ATTRIBUTS_NAME = "name";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    @XmlElement(name = "description")
    @Column(columnDefinition = "TEXT", name = DataType.NAME_ATTRIBUTS_DESCRIPTION)
    private String description = "";
    /**
     * The name @link(String).
     */
    @XmlElement(name = "name")
    @Column(name = DataType.NAME_ATTRIBUTS_NAME, nullable = false, unique = true)
    private String name;

    /**
     * Instantiates a new data type.
     */
    public DataType() {
    }

    /**
     * Instantiates a new data type.
     *
     * @param name the name
     * @link(String) the name
     */
    public DataType(final String name) {
        this.setName(name);
    }

    /**
     * Instantiates a new data type.
     *
     * @param name the name
     * @param description the description
     * @link(String) the name
     * @link(String) the description
     */
    public DataType(final String name, final String description) {
        this(name);
        setDescription(description);
    }

    /**
     * Clone.
     *
     * @return the data type
     * @throws CloneNotSupportedException the clone not supported exception
     * @see java.lang.Object#clone()
     */
    @Override
    public DataType clone() {
        DataType dataType = null;
        try {
            dataType = (DataType) super.clone();
            dataType.setId(getId());
            dataType.setCode(getCode());
            dataType.setDescription(description);
            dataType.setOrder(order);
        } catch (CloneNotSupportedException cnse) {
            LOGGER.error(cnse.getMessage(), cnse);
        }
        return dataType;
    }

    /**
     * Compare to.
     *
     * @param o the o
     * @return the int
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final INodeable o) {
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            return 0;
        } else {
            return getCode() == null ? -1 : getCode().compareTo(o.getCode());
        }
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataType other = (DataType) obj;
        return Objects.equals(getCode(), other.getCode());
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description == null ? "" : description;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
        setCode(Utils.createCodeFromString(name));
    }
    
    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (getCode() == null ? 0 : getCode().hashCode());
        result = prime * result + (description == null ? 0 : description.hashCode());
        return result;
    }

    /**
     * To string.
     *
     * @return the string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.code;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) DataType.class;
    }
}
