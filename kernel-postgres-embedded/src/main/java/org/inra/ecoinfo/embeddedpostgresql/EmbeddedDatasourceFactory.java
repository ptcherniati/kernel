/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.embeddedpostgresql;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class EmbeddedDatasourceFactory {

    public static final Logger LOGGER = LoggerFactory.getLogger(EmbeddedDatasourceFactory.class);
    public static final String CREATE_USER = "CREATE USER %s with password '%s'";
    public static final String CREATE_DATABASE = "CREATE DATABASE %s owner %s;";
    public static final String MESSAGE = "Initializing the database %s on port %s with user %s and password %s";

    protected DataSource dataSource;
    protected String driver = "postgresql";
    protected EmbeddedPostgres embeddedPostgres;
    protected String database = "monsoeretest";
    protected String userName = "monsoereusertest";
    protected String password = "mst001";
    protected final Map<String, String> databaseProperties = new HashMap();
    protected int port = 15432;

    public EmbeddedDatasourceFactory() throws IOException, SQLException {
    }

    public DataSource getDataSource() throws IOException, SQLException {
        if (dataSource == null) {
            this.embeddedPostgres = EmbeddedPostgres
                    .builder()
                    .setPort(port)
                    .start();
            dataSource = embeddedPostgres.getPostgresDatabase();
            LOGGER.debug(String.format(MESSAGE, "postgres", port, "postgres", null));
            Connection connection = dataSource.getConnection();
            connection.createStatement().execute(String.format(CREATE_USER, userName, password));
            connection.createStatement().execute(String.format(CREATE_DATABASE, database, userName));
            dataSource = embeddedPostgres.getDatabase(userName, database, databaseProperties);
            LOGGER.debug(String.format(MESSAGE, database, port, userName, password));
        }
        return dataSource;
    }

    public void setDatabase(String database) {
        this.database = database == null || database.startsWith("$") ? this.database : database;
    }

    public void setUserName(String userName) {
        this.userName = userName == null || userName.startsWith("$") ? this.userName : userName;
    }

    public void setPassword(String password) {
        this.password = password == null || password.startsWith("$") ? this.password : password;
    }

    public void setPort(String port) {
        this.port = port == null || !port.matches("[0-9]*") ? this.port : Integer.valueOf(port);
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public void setUrl(String url) {
        Pattern pattern = Pattern.compile("jdbc:(?<driver>.*)://localhost:+(?<port>[0-9]*)/(?<database>.*)");
        Matcher matcher = pattern.matcher(url);
        if (matcher.matches()) {
            String regDriver = matcher.group("driver");
            driver = regDriver == null ? driver : regDriver;
            String regPort = matcher.group("port");
            port = regPort == null ? port : Integer.valueOf(regPort);
            String regDatabase = matcher.group("database");
            database = regDatabase == null ? database : regDatabase;
        }
    }
    public void addDatabaseProperty(String keyProperty, String valueProperty){
        databaseProperties.put(keyProperty, valueProperty);
    }
}
