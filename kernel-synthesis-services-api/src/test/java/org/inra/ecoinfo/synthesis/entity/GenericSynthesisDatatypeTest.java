/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis.entity;

import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class GenericSynthesisDatatypeTest {
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    GenericSynthesisDatatype instance;

    /**
     *
     */
    public GenericSynthesisDatatypeTest() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        instance=new GenericSynthesisDatatype();
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdNodesList method, of class GenericSynthesisDatatype.
     */
    @Test
    public void testGetIdNodesList() {
        System.out.println("getIdNodesList");
        String idNodes = "25,32 ,14 ,45,51, 7 ";
        instance.setIdNodes(idNodes);
        List<Long> result = instance.getIdNodesList();
        assertTrue(result.containsAll(Arrays.asList(new Long[]{25L,32L,14L,45L,51L,7L})));
    }
    
}
