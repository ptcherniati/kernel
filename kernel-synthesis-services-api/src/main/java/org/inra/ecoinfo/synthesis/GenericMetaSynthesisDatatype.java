package org.inra.ecoinfo.synthesis;

import org.inra.ecoinfo.synthesis.entity.DefaultSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 * The Class MetaSynthesisDatatype.
 *
 * @param <V>
 * @param <D>
 */
public class GenericMetaSynthesisDatatype<V extends GenericSynthesisValue, D extends GenericSynthesisDatatype> extends MetaSynthesisDatatype<GenericSynthesisValue, DefaultSynthesisDatatype> {

    /**
     *
     * @param prefix
     */
    public GenericMetaSynthesisDatatype(String prefix) {
        setPrefix(prefix);
    }

}
