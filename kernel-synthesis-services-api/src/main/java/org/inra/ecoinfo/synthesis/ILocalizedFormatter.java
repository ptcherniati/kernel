/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis;

import java.io.Serializable;
import java.util.Locale;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author tcherniatinsky
 * @param <T>
 */
public interface ILocalizedFormatter<T> extends Serializable{

    /**
     *
     * @param toFormat
     * @param locale
     * @param arguments
     * @return
     */
    String format(T toFormat, Locale locale, Object... arguments);

    /**
     *
     * @param localizationManager
     */
    void setLocalizationManager(ILocalizationManager localizationManager);

}
