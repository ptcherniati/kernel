package org.inra.ecoinfo.synthesis;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The Class ValueSynthesisVO.
 */
public class ValueSynthesis implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The date @link(Date).
     */
    private LocalDateTime date;
    /**
     * The site @link(String).
     */
    private String site;
    /**
     * The value @link(Float).
     */
    private Float value;
    /**
     * The variable @link(String).
     */
    private String variable;

    /**
     * Instantiates a new value synthesis vo.
     *
     * @param date the date
     * @param site the site
     * @param variable the variable
     * @param f
     * @param value the value
     * @link(Date) the date
     * @link(String) the site
     * @link(String) the variable
     * @link(Float) the value
     */
    public ValueSynthesis(final LocalDateTime date, final String site, final String variable,
            final Float value) {
        super();
        this.date = date;
        this.site = site;
        this.variable = variable;
        this.value = value;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(final LocalDateTime date) {
        this.date = date;
    }

    /**
     * Gets the site.
     *
     * @return the site
     */
    public String getSite() {
        return site;
    }

    /**
     * Sets the site.
     *
     * @param site the new site
     */
    public void setSite(final String site) {
        this.site = site;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Float getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final Float value) {
        this.value = value;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public String getVariable() {
        return variable;
    }

    /**
     * Sets the variable.
     *
     * @param variable the new variable
     */
    public void setVariable(final String variable) {
        this.variable = variable;
    }
}
