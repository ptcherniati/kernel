package org.inra.ecoinfo.synthesis.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 * The Class GenericSynthesisDatatype.
 */
@MappedSuperclass
public class GenericSynthesisDatatype implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The description @link(String).
     */
    @Transient
    private String description;
    /**
     * The id @link(Long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected Long id;

    /**
     *
     */
    @Column(nullable = false)
    protected String idNodes;
    /**
     * The max date @link(Date).
     */
    @Column(nullable = false)
    protected LocalDateTime maxDate;
    /**
     * The min date @link(Date).
     */
    @Column(nullable = false)
    protected LocalDateTime minDate;
    /**
     * The site @link(String).
     */
    @Column(nullable = false)
    protected String site;

    /**
     *
     */
    public GenericSynthesisDatatype() {
    }

    /**
     *
     * @param minDate
     * @param maxDate
     * @param site
     * @param string1
     * @param idNode
     */
    public GenericSynthesisDatatype(LocalDateTime minDate, LocalDateTime maxDate, String site, String idNodes) {
        this.maxDate = maxDate;
        this.minDate = minDate;
        this.site = site;
        this.idNodes = idNodes;
    }

    /**
     *
     * @param maxDate
     * @param minDate
     * @param string
     * @param site
     */
    public GenericSynthesisDatatype(LocalDateTime maxDate, LocalDateTime minDate, String site) {
        this.minDate = minDate;
        this.maxDate = maxDate;
        this.site = site;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getMaxDate() {
        return maxDate;
    }

    /**
     *
     * @param maxDate
     */
    public void setMaxDate(LocalDateTime maxDate) {
        this.maxDate = maxDate;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getMinDate() {
        return minDate;
    }

    /**
     *
     * @param minDate
     */
    public void setMinDate(LocalDateTime minDate) {
        this.minDate = minDate;
    }

    /**
     * Gets the site.
     *
     * @return the site
     */
    public String getSite() {
        return site;
    }

    /**
     * Sets the site.
     *
     * @param site the new site
     */
    public void setSite(final String site) {
        this.site = site;
    }

    /**
     *
     * @return
     */
    public String getIdNodes() {
        return idNodes;
    }

    /**
     *
     * @param idNodes
     */
    public void setIdNodes(String idNodes) {
        this.idNodes = idNodes;
    }

    /**
     *
     * @return
     */
    public Stream<Long> getIdNodesStream() {
        return Optional.ofNullable(idNodes)
                .map(in -> {
                    return Arrays
                            .<String>stream(in.split(","))
                            .<String>map(String::trim)
                            .map(Long::parseLong);
                })
                .orElse(Stream.empty());
    }

    /**
     *
     * @return
     */
    public List<Long> getIdNodesList() {
        return getIdNodesStream()
                .collect(Collectors.toList());
    }
}
