/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis;

import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.stream.Stream;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author tcherniatinsky
 * @param <V>
 * @param <D>
 */
public abstract class AbstractSynthesis<V extends GenericSynthesisValue, D extends GenericSynthesisDatatype> extends AbstractJPADAO implements IJPASynthesisValueDAO<V, D> {

    /**
     *
     */
    protected Class<V> synthesisValueClass;

    /**
     *
     */
    protected Class<D> synthesisDatatypeClass;

    /**
     *
     */
    public AbstractSynthesis() {
        this.synthesisValueClass = getClassOfParameter(0);
        this.synthesisDatatypeClass = getClassOfParameter(1);
    }

    /**
     *
     * @param <T>
     * @param parameter
     * @return
     */
    protected <T> Class<T> getClassOfParameter(int parameter) {
        try {
            return (Class<T>) ((ParameterizedType) getClass()
                    .getGenericSuperclass()).getActualTypeArguments()[parameter];
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            return null;
        }

    }

    /**
     *
     * @return
     */
    @Override
    public Stream<D> getSynthesisDatatype() {

        Root<V> sv = builder.createQuery(synthesisDatatypeClass).from(synthesisValueClass);
        String tableName = sv.getModel().getName();
        String sql = "select site, min(date), max(date), string_agg(distinct(cnp.branch_node_id)::::text, ',') idnode\n"
                + "from %s sv\n"
                + "join composite_node_data_set nd on nd.branch_node_id=sv.idnode\n"
                + "join composite_node_data_set cnp on nd.id_parent_node=cnp.branch_node_id\n"
                + "group by site";
        sql = String.format(sql, tableName);
        final org.hibernate.query.Query query = (org.hibernate.query.Query) entityManager.createNativeQuery(String.format(sql, tableName));
        return query
                .stream()
                .map(
                        sd -> {
                            try {
                                final String site = (String) ((Object[]) sd)[0];
                                final LocalDateTime minDate = ((Timestamp) ((Object[]) sd)[1]).toLocalDateTime();
                                final LocalDateTime maxdate = ((Timestamp) ((Object[]) sd)[2]).toLocalDateTime();
                                final String idNodes = (String) ((Object[]) sd)[3];
                                Constructor<D> constructor = synthesisDatatypeClass.getConstructor(String.class, LocalDateTime.class, LocalDateTime.class, String.class);
                                return constructor.newInstance(site, minDate, maxdate, idNodes);
                            } catch (Exception e) {
                                return null;
                            }
                        })
                .filter(sd->sd!=null);
//        CriteriaQuery<D> query = builder.createQuery(synthesisDatatypeClass);
//        Root<V> sv = query.from(synthesisValueClass);
//        Root<NodeDataSet> nodeVariable = query.from(NodeDataSet.class);
//        Join<NodeDataSet, AbstractBranchNode> nodeLocalization = nodeVariable.join(NodeDataSet_.parent);
//        final Expression<String> literal = builder.literal(",");
//        final Expression<String> idnodes = builder.function("string_agg", String.class, nodeLocalization.get(NodeDataSet_.id), literal);
//        final Path<String> site = sv.get(GenericSynthesisValue_.site);
//        final Expression<LocalDateTime> minDate = builder.least(sv.get(GenericSynthesisValue_.date));
//        final Expression<LocalDateTime> maxDate = builder.greatest(sv.get(GenericSynthesisValue_.date));
//        query
//                .select(builder.construct(synthesisDatatypeClass, site, minDate, maxDate, idnodes)
//                )
//                .where(builder.equal(nodeVariable.get(NodeDataSet_.id), sv.get(GenericSynthesisValue_.idNode)))
//                .groupBy(
//                        sv.get(GenericSynthesisValue_.site)
//                );
//        return getResultListToStream(query);
    }

}
