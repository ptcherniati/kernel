package org.inra.ecoinfo.synthesis;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The Class DatePlotVO.
 */
public class DatePlot implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The date @link(Date).
     */
    private LocalDateTime date;
    /**
     * The value @link(Float).
     */
    private Float value;

    /**
     * Instantiates a new date plot vo.
     */
    public DatePlot() {
        super();
    }

    /**
     * Instantiates a new date plot vo.
     *
     * @param ldt
     * @param date the date
     * @link(Date) the date
     */
    public DatePlot(final LocalDateTime date) {
        super();
        this.date = date;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(final LocalDateTime date) {
        this.date = date;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Float getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final Float value) {
        this.value = value;
    }
}
