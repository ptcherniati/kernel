/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis;

import java.util.stream.Stream;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author tcherniatinsky
 * @param <V>
 * @param <D>
 */
public interface IJPASynthesisValueDAO<V extends GenericSynthesisValue, D extends GenericSynthesisDatatype> {

    /**
     *
     * @return
     */
    Stream<V> getSynthesisValue();

    /**
     *
     * @return
     */
    Stream<D> getSynthesisDatatype();
}
