package org.inra.ecoinfo.synthesis;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * The Interface ISynthesisRegister.
 */
public interface ISynthesisRegister extends Serializable{

    /**
     * Gets the base package.
     *
     * @return the base package
     */
    String getBasePackage();

    /**
     * Gets the datatypes names.
     *
     * @return the datatypes names
     */
    Set<String> getDatatypesNames();

    /**
     * Retrieve metadata synthesis datatype.
     *
     * @param datatypeName the datatype name
     * @return the meta synthesis datatype
     * @link(String) the datatype name
     */
    MetaSynthesisDatatype retrieveMetadataSynthesisDatatype(String datatypeName);

    /**
     *
     * @param datatypesName
     */
    void addGenericMetaSynthesisDatatypes(List<String> datatypesName);
}
