/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author tcherniatinsky
 */
public class SynthesisUtils {

    /**
     *
     * @param date
     * @param datesmap
     * @return
     */
    public static final boolean isBetween(LocalDateTime date, Map<Group, List<LocalDate>> datesmap) {
        return datesmap.values().stream()
                .anyMatch(dates -> {
                    LocalDate startDate = dates.get(0) == null ? DateUtil.MIN_LOCAL_DATE : dates.get(0);
                    LocalDate endDate = dates.get(1) == null ? DateUtil.MAX_LOCAL_DATE : dates.get(1);
                    return !(date.isAfter(endDate.atStartOfDay()) || date.isBefore(startDate.atStartOfDay()));
                });
    }
}
