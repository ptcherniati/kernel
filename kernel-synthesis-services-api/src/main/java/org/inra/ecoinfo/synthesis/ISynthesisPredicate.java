/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis;

import java.util.List;
import java.util.Optional;
import java.util.SortedMap;
import java.util.function.Function;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

/**
 *
 * @author tcherniatinsky
 */
public interface ISynthesisPredicate extends Function<INode, List<INode>> {

    /**
     *
     * @param synthesisDatatypesByNodeId
     * @param pathes
     * @return
     */
    ISynthesisPredicate getInstance(SortedMap<Long, GenericSynthesisDatatype> synthesisDatatypesByNodeId);

    /**
     *
     * @param id
     * @param path
     * @return
     */
    Optional<GenericSynthesisDatatype> getSynthesisDatatype(Long id);

    /**
     *
     * @param nodePath
     * @return
     */
    Optional<String> getSynthesisPath(String nodePath);
}
