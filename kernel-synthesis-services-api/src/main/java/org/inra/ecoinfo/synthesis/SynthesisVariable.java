package org.inra.ecoinfo.synthesis;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class SynthesisVariableVO.
 */
public class SynthesisVariable implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The dates plots @link(List<DatePlotVO>).
     */
    private List<DatePlot> datesPlots = new LinkedList<>();
    /**
     * The variable @link(String).
     */
    private String variable;

    /**
     * Instantiates a new synthesis variable vo.
     */
    public SynthesisVariable() {
        super();
    }

    /**
     * Instantiates a new synthesis variable vo.
     *
     * @param string
     * @param variable the variable
     * @link(String) the variable
     */
    public SynthesisVariable(final String variable) {
        super();
        this.variable = variable;
    }

    /**
     * Gets the dates plots.
     *
     * @return the dates plots
     */
    public List<DatePlot> getDatesPlots() {
        return datesPlots;
    }

    /**
     * Sets the dates plots.
     *
     * @param datesPlots the new dates plots
     */
    public void setDatesPlots(final List<DatePlot> datesPlots) {
        this.datesPlots = datesPlots;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public String getVariable() {
        return variable;
    }

    /**
     * Sets the variable.
     *
     * @param variable the new variable
     */
    public void setVariable(final String variable) {
        this.variable = variable;
    }
}
