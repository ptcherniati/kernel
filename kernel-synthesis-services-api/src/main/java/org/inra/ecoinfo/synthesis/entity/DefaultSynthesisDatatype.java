package org.inra.ecoinfo.synthesis.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * The Class GenericSynthesisDatatype.
 */
@Entity(name = "GenericSynthesisDatatype")
public class DefaultSynthesisDatatype extends GenericSynthesisDatatype {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @Column(nullable = false)
    protected String datatypeName;

    /**
     *
     * @param site
     * @param minDate
     * @param maxDate
     * @param string2
     */
    public DefaultSynthesisDatatype(String datatypeName, String site, LocalDateTime minDate, LocalDateTime maxDate, String idNodes) {
        super(minDate, maxDate, site, idNodes);
        this.datatypeName = datatypeName;
    }

    /**
     *
     */
    public DefaultSynthesisDatatype() {
        super();
    }

    /**
     *
     * @return
     */
    public String getDatatypeName() {
        return datatypeName;
    }
}