package org.inra.ecoinfo.synthesis;

import java.io.Serializable;
import java.time.LocalDateTime;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

/**
 * The Class SynthesisDatatypeVO.
 */
public class SynthesisDatatype implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The datatype @link(String).
     */
    private String datatype;
    /**
     * The date max @link(Date).
     */
    private LocalDateTime dateMax;
    /**
     * The date min @link(Date).
     */
    private LocalDateTime dateMin;
    /**
     * The site @link(String).
     */
    private String site;

    /**
     * Instantiates a new synthesis datatype vo.
     */
    public SynthesisDatatype() {
        super();
    }

    /**
     * Instantiates a new synthesis datatype vo.
     *
     * @param synthesisDatatype the synthesis datatype
     * @param string
     * @param datatype the datatype
     * @link(GenericSynthesisDatatype) the synthesis datatype
     * @link(String) the datatype
     */
    public SynthesisDatatype(final GenericSynthesisDatatype synthesisDatatype, final String datatype) {
        this.site = synthesisDatatype.getSite();
        this.datatype = datatype;
        this.dateMin = synthesisDatatype.getMinDate();
        this.dateMax = synthesisDatatype.getMaxDate();
    }

    /**
     * Instantiates a new synthesis datatype vo.
     *
     * @param site the site
     * @param datatype the datatype
     * @param dateMin the date min
     * @param ldt1
     * @param dateMax the date max
     * @link(String) the site
     * @link(String) the datatype
     * @link(Date) the date min
     * @link(Date) the date max
     */
    public SynthesisDatatype(final String site, final String datatype, final LocalDateTime dateMin,
            final LocalDateTime dateMax) {
        super();
        this.site = site;
        this.datatype = datatype;
        this.dateMin = dateMin;
        this.dateMax = dateMax;
    }

    /**
     * Gets the datatype.
     *
     * @return the datatype
     */
    public String getDatatype() {
        return datatype;
    }

    /**
     * Sets the datatype.
     *
     * @param datatype the new datatype
     */
    public void setDatatype(final String datatype) {
        this.datatype = datatype;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getDateMax() {
        return dateMax;
    }

    /**
     *
     * @param dateMax
     */
    public void setDateMax(LocalDateTime dateMax) {
        this.dateMax = dateMax;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getDateMin() {
        return dateMin;
    }

    /**
     *
     * @param dateMin
     */
    public void setDateMin(LocalDateTime dateMin) {
        this.dateMin = dateMin;
    }

    /**
     * Gets the site.
     *
     * @return the site
     */
    public String getSite() {
        return site;
    }

    /**
     * Sets the site.
     *
     * @param site the new site
     */
    public void setSite(final String site) {
        this.site = site;
    }
}
