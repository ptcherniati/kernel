package org.inra.ecoinfo.synthesis;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface ISynthesisManager.
 */
public interface ISynthesisManager extends Serializable{

    /**
     * Builds the synthesis.
     *
     * @throws BusinessException the business exception
     */
    void buildSynthesis() throws BusinessException;

    /**
     * Gets the all availables variables synthesis.
     *
     * @param contextName the site name
     * @param nodeDatasetContextId
     * @param datatypeName the datatype name
     * @param realNodeContextId
     * @link(String) the site name
     * @link(String)
     * @return the
     * java.util.Set<org.inra.ecoinfo.mga.business.composite.INodeable> the
     * datatype name
     */
    List<NodeDataSet> getAllRestrictedAvailablesVariablesSynthesisNodes(String currentDatatype,  Long nodeDatasetContextId);

    /**
     * Gets the date by site and variable.
     *
     * @param datasetNodeLeafContextId
     * @param contextName the site name
     * @param variableName the variable name
     * @param datatypeName the datatype name
     * @return the date by site and variable
     * @throws BusinessException the business exception
     * @link(String) the site name
     * @link(String) the variable name
     * @link(String) the datatype name
     */
    IntervalDate getDateByContextAndVariable(Long datasetNodeLeafContextId, String datatypeName) throws BusinessException;

    /**
     * Retrieve all synthesis datatype.
     *
     * @return the list
     * @throws BusinessException the business exception
     */
    List<SynthesisDatatype> retrieveAllSynthesisDatatype() throws BusinessException;

    /**
     * Retrieve values synthesis.
     *
     * @param contextName the site name
     * @param synthesisValueClass
     * @param datatype the datatype
     * @param variable the variable
     * @return the list
     * @link(String) the site name
     * @link(String) the datatype
     * @link(String) the variable
     */
    List<GenericSynthesisValue> retrieveValuesSynthesis(Long nodeDatasetVariableId, Class<GenericSynthesisValue> synthesisValueClass);

    /**
     *
     * @param variableNodeId
     * @param activity
     * @return
     */
    boolean hasRoleForNodeIdAndActivity(Long variableNodeId, Activities activity);

    /**
     *
     * @param nodePath
     * @return
     */
    Optional<String> getSynthesisPath(String nodePath);

    /**
     *
     * @param nodePath
     * @return
     * @throws java.lang.ClassNotFoundException
     */
    Optional<GenericSynthesisDatatype> getSynthesisDatatypeForDatatypeAndNode(final String datatypeName, long datasetDcontextNodeId) throws ClassNotFoundException;

    /**
     *
     * @param context
     * @param dateDebut
     * @param dateFin
     * @param node
     * @param synthesisValueClass
     * @param activities
     * @return
     * @throws BusinessException
     */
    Stream<GenericSynthesisValue> getSynthesisValuesByVariableNodeableAndSiteByDatesInterval(LocalDateTime dateDebut, LocalDateTime dateFin, NodeDataSet node, Class<GenericSynthesisValue> synthesisValueClass, List<Activities> activities) throws BusinessException;

    /**
     *
     * @param datatypeName
     * @return
     */
    boolean isGenericdatatype(final String datatypeName);
}
