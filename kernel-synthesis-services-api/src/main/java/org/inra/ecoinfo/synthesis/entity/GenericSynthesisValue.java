/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 13:30:55
 */
package org.inra.ecoinfo.synthesis.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * The Class GenericSynthesisValue.
 *
 * @author "Antoine Schellenberger"
 */
@MappedSuperclass
public abstract class GenericSynthesisValue implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The id @link(Long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected Long id;
    
    /**
     *
     */
    @Column(nullable = false)
    protected Long idNode;
    /**
     * The date @link(Date).
     */
    @Column(nullable = false)
    protected LocalDateTime date;
    /**
     * The site @link(String).
     */
    @Column(nullable = false)
    protected String site;
    /**
     * The value string @link(String).
     */
    @Column(nullable = true)
    protected String valueString;
    /**
     * The value float @link(Float).
     */
    @Column(nullable = true)
    protected Float valueFloat;
    /**
     * The variable @link(String).
     */
    @Column(nullable = false)
    protected String variable;
    /**
     * The is mean @link(Boolean).
     */
    @Column(nullable = false)
    protected Boolean isMean;

    /**
     * Instantiates a new generic synthesis value.
     */
    public GenericSynthesisValue() {
        super();
    }

    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param valueFloat
     * @param valueString
     * @param l
     * @param idNode
     */
    public GenericSynthesisValue(LocalDateTime date, String site, String variable, Float valueFloat, String valueString, Long idNode) {
        super();
        this.idNode = idNode;
        this.date = date;
        this.site = site;
        this.valueString = valueString;
        this.valueFloat = valueFloat;
        this.variable = variable;
    }

    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param valueFloat
     * @param valueString
     * @param idNode
     * @param bln
     * @param isMean
     */
    public GenericSynthesisValue(LocalDateTime date, String site, String variable, Float valueFloat, String valueString, Long idNode, Boolean isMean) {
        this.idNode = idNode;
        this.date = date;
        this.site = site;
        this.valueString = valueString;
        this.valueFloat = valueFloat;
        this.variable = variable;
        this.isMean = isMean;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(final LocalDateTime date) {
        this.date = date;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the checks if is mean.
     *
     * @return the checks if is mean
     */
    public Boolean getIsMean() {
        return isMean;
    }

    /**
     * Sets the checks if is mean.
     *
     * @param isMean the new checks if is mean
     */
    public void setIsMean(final Boolean isMean) {
        this.isMean = isMean;
    }

    /**
     * Gets the site.
     *
     * @return the site
     */
    public String getSite() {
        return site;
    }

    /**
     * Sets the site.
     *
     * @param site the new site
     */
    public void setSite(final String site) {
        this.site = site;
    }

    /**
     * Gets the value float.
     *
     * @return the value float
     */
    public Float getValueFloat() {
        return valueFloat;
    }

    /**
     * Sets the value float.
     *
     * @param valueFloat the new value float
     */
    public void setValueFloat(final Float valueFloat) {
        this.valueFloat = valueFloat;
    }

    /**
     * Gets the value string.
     *
     * @return the value string
     */
    public String getValueString() {
        return valueString;
    }

    /**
     * Sets the value string.
     *
     * @param value the new value string
     */
    public void setValueString(final String value) {
        this.valueString = value;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public String getVariable() {
        return variable;
    }

    /**
     * Sets the variable.
     *
     * @param variable the new variable
     */
    public void setVariable(final String variable) {
        this.variable = variable;
    }

    /**
     *
     * @return
     */
    public Long getIdNode() {
        return idNode;
    }

    /**
     *
     * @param idNode
     */
    public void setIdNode(Long idNode) {
        this.idNode = idNode;
    }
}
