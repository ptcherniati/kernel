package org.inra.ecoinfo.synthesis;

import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 * The Class MetaSynthesisDatatype.
 *
 * @param <V>
 * @param <D>
 */
public class MetaSynthesisDatatype<V extends GenericSynthesisValue, D extends GenericSynthesisDatatype> {

    private IJPASynthesisValueDAO<V, D> synthesisValueDAO;
    /**
     * The prefix @link(String).
     */
    private String prefix;
    private String synthesisChartTrenderVariableBuilder;

    /**
     *
     * @param synthesisValueDAO
     */
    public void setSynthesisValueDAO(IJPASynthesisValueDAO<V, D> synthesisValueDAO) {
        this.synthesisValueDAO = synthesisValueDAO;
    }

    /**
     * Gets the prefix.
     *
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Sets the prefix.
     *
     * @param prefix the new prefix
     */
    public void setPrefix(final String prefix) {
        this.prefix = prefix;
    }

    /**
     *
     * @return
     */
    public IJPASynthesisValueDAO<V, D> getSynthesisValueDAO() {
        return this.synthesisValueDAO;
    }

    /**
     *
     * @return
     */
    public String getSynthesisChartTrenderVariableBuilder() {
        return synthesisChartTrenderVariableBuilder;
    }

    /**
     *
     * @param synthesisChartTrenderVariableBuilder
     */
    public void setSynthesisChartTrenderVariableBuilder(String synthesisChartTrenderVariableBuilder) {
        this.synthesisChartTrenderVariableBuilder = synthesisChartTrenderVariableBuilder;
    }
}
