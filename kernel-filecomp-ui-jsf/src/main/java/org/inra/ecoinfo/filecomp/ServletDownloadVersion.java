package org.inra.ecoinfo.filecomp;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.utils.StreamUtils;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Servlet implementation class ServletRetrieveVariableGraphic.
 */
@WebServlet(value = "/downloadFileComp", name = "servlet download file comp")
public class ServletDownloadVersion extends HttpServlet {

    /**
     * The Constant PARAMETER_GET_REQUEST @link(String).
     */
    private static final String PARAMETER_GET_REQUEST_CODE = "code";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new servlet download version.
     *
     * @see HttpServlet#HttpServlet()
     */
    public ServletDownloadVersion() {
        super();
    }

    /**
     * Do get.
     *
     * @param request
     * @link(HttpServletRequest) the request
     * @param response
     * @link(HttpServletResponse) the response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred. @see
     * HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final ServletContext servletContext = this.getServletContext();
        final WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        final IFileCompManager fileCompManager = (IFileCompManager) applicationContext.getBean(IFileCompManager.ID_BEAN);
        applicationContext.getBean(IFileCompConfiguration.ID_BEAN);
        final JpaTransactionManager transactionManager = (JpaTransactionManager) applicationContext.getBean("transactionManager");
        final TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));
        final String code = request.getParameter(ServletDownloadVersion.PARAMETER_GET_REQUEST_CODE);
        try {
            final byte[] datas = fileCompManager.doDownload(code);
            final FileComp fileComp = fileCompManager.getFileCompByCode(code)
                    .orElseThrow(() -> new BusinessException("can't get file comp"));

            response.setContentType("application/octet-stream");
            response.setContentLength(datas.length);
            response.setHeader("Content-Disposition", String.format("attachment; filename=%s", fileComp.getFileName()));
            response.getOutputStream().write(datas);
            response.getOutputStream().flush();
            StreamUtils.closeStream(response.getOutputStream());
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in ServletDownloadVersion", e);
        } finally {
            transactionManager.commit(status);
        }
    }

    /**
     * Do post.
     *
     * @param request
     * @link(HttpServletRequest) the request
     * @param response
     * @link(HttpServletResponse) the response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred. @see
     * HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
