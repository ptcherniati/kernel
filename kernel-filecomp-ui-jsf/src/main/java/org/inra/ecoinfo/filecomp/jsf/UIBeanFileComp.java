package org.inra.ecoinfo.filecomp.jsf;

import com.google.common.base.Strings;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.TimeZone;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.ValueHolder;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.filecomp.entity.FileType;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.managedbean.UIBeanActionTree;
import org.inra.ecoinfo.mga.managedbean.UserVO;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.tree.Tree;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 * The Class UIBeanDataset.
 */
@ManagedBean(name = "uiFileComp")
@ViewScoped
public class UIBeanFileComp implements Serializable {

    private static final String DATE_FILTER = "date";
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String BUNDLE_PATH = "org/inra/ecoinfo/filecomp/jsf/fileCompManagementPanel";

    private static final Logger LOGGER = LoggerFactory.getLogger("uiFileComp");

    private static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";

    private static final String PROPERTY_MSG_NO_FILE = "PROPERTY_MSG_NO_FILE";

    /**
     *
     */
    public static final String PARAM_MSG_FILE_SELECTED = "PARAM_MSG_FILE_SELECTED";

    /**
     *
     */
    public static final String PARAM_MSG_NO_FILE_SELECTED = "PARAM_MSG_NO_FILE_SELECTED";

    private static final String EMPTY_STRING = "";
    private static final String PARAM_ALERT_BAD_INTERVAL_DATE = "PARAM_ALERT_BAD_INTERVAL_DATE";
    /**
     * The tree length @link(int).
     */

    private List<ItemFileVO> currentSelections;
    private ItemFile _currentSelection;

    private boolean myFiles = false;

    private DataTable selectedFilecompDetailsDatatable;
    /**
     * The tree selection @link(UITree).
     */

    private Tree treeSelection;
    /**
     * The validation successful.
     */
    private String validationSuccessfull;

    private String fileCode;

    private final List<String> notVariableFields;

    private boolean isFileModify;

    private final List<String> locales = new LinkedList<>();
    /**
     * The dataset manager @link(IDatasetManager).
     */
    @ManagedProperty(value = "#{fileCompManager}")
    protected IFileCompManager fileCompManager;
    /**
     * The notifications manager @link(INotificationsManager).
     */
    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;
    /**
     * The transaction manager @link(JpaTransactionManager).
     */
    @ManagedProperty(value = "#{coreConfiguration}")
    protected ICoreConfiguration coreConfiguration;
    /**
     *
     */
    @ManagedProperty(value = "#{fileCompConfiguration}")
    protected IFileCompConfiguration filecompConfiguration;
    /**
     *
     */
    @ManagedProperty(value = "#{transactionManager}")
    protected JpaTransactionManager transactionManager;
    /**
     *
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;
    /**
     *
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;

    /**
     *
     */
    @ManagedProperty(value = "#{uiBeanActionTree}")
    protected UIBeanActionTree uiBeanActionTree;
    private List<ItemFileVO> filteredSelections;

    /**
     * Instantiates a new uI bean file comp.
     */
    public UIBeanFileComp() {
        notVariableFields = new LinkedList<>();

        Localization.getLocalisations().stream().forEach((locale) -> {
            locales.add(String.format("PROPERTY_MSG_LOCALE_%s", locale.toUpperCase()));
        });
    }

    /**
     *
     * @param event
     * @throws IOException
     */
    public void fileUpload(FileUploadEvent event) throws IOException {
        UploadedFile fileUploaded = event.getFile();
        getCurrentSelection().setData(fileUploaded.getContents());
        getCurrentSelection().setSize(Long.toString(fileUploaded.getSize()));
        getCurrentSelection().setOriginalFileName(fileUploaded.getFileName());
        getCurrentSelection().setModify(true);
        getCurrentSelection().setContentType(fileUploaded.getContentType());
    }

    /**
     *
     * @param event
     * @throws IOException
     */
    public void fileUploadModify(FileUploadEvent event) throws IOException {
        isFileModify = true;
        fileUpload(event);
    }

    /**
     *
     * @return
     */
    public Map<String, String> getDescriptions() {
        if (getCurrentSelection().getDescriptions().isEmpty()) {
            getCurrentSelection().getDescriptions().put(Localization.getDefaultLocalisation(), "");
            Localization.getLocalisations().stream().filter((locale) -> !(locale.equals(Localization.getDefaultLocalisation()))).forEach((locale) -> {
                getCurrentSelection().getDescriptions().put(locale, "");
            });
        }
        return getCurrentSelection().getDescriptions();
    }

    /**
     *
     * @return @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public String getModifiyFileButtonValue() throws BusinessException {
        String localizedString = localizationManager.getMessage("org.inra.ecoinfo.filecomp.jsf.fileCompManagementPanel", "PARAM_MSG_MODIFY_FILE");
        return localizedString.replaceAll("\\{0\\}", getCurrentSelection().getOriginalFileName());
    }

    /**
     *
     * @return
     */
    public ItemFile getCurrentSelection() {
        if (_currentSelection == null) {
            try {
                _currentSelection = new ItemFile(
                        policyManager.getCurrentUser().getLanguage(), getMapFileTypes());
            } catch (BusinessException ex) {
                LOGGER.debug("can't get current selection", ex);
            }
        }
        return _currentSelection;
    }

    /**
     *
     * @param currentSelection
     */
    public void setCurrentSelection(ItemFile currentSelection) {
        this._currentSelection = currentSelection;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public List<ItemFileVO> getCurrentSelections() throws BusinessException {
        if (currentSelections == null) {
            setCurrentSelections(getFiles());
        }
        return currentSelections;
    }

    /**
     *
     * @param currentSelections
     */
    public void setCurrentSelections(List<ItemFileVO> currentSelections) {
        this.currentSelections = currentSelections;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public List<ItemFileVO> getFilteredSelections() throws BusinessException {
        return filteredSelections;
    }

    /**
     *
     * @param fileteredSelections
     * @throws BusinessException
     */
    public void setFilteredSelections(List<ItemFileVO> fileteredSelections) throws BusinessException {
        this.filteredSelections = fileteredSelections;
    }

    /**
     *
     * @return
     */
    public ItemFile getFile() {
        return this.getCurrentSelection();
    }

    /**
     *
     * @return
     */
    public String getFileCode() {
        return fileCode;
    }

    /**
     *
     * @param code
     */
    public void setFileCode(String code) {
        this.fileCode = code;
    }

    /**
     *
     * @return
     */
    public ItemFile getFileModify() {
        return this.getCurrentSelection();
    }

    /**
     *
     * @param event
     */
    public void updateAssociate(CloseEvent event) {
        setCurrentSelections(null);
    }

    /**
     *
     * @param uiBeanActionTree
     */
    public void setUiBeanActionTree(UIBeanActionTree uiBeanActionTree) {
        this.uiBeanActionTree = uiBeanActionTree;
    }

    /**
     *
     * @return @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public List<FileType> getListTypesPossible2() throws BusinessException {
        List<FileType> listTypesPossible = fileCompManager.retrieveListTypesPossible();
        return listTypesPossible;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public List<ItemFileVO> getFiles() throws BusinessException {
        Map<String, FileType> fileTypes = getMapFileTypes();
        if (CollectionUtils.isEmpty(currentSelections)) {
            List<FileComp> filesComp = fileCompManager.getAllFilesComp();
            currentSelections = filesComp.stream()
                    .map((fileComp) -> buildItemFileVO(fileComp, fileTypes))
                    .collect(Collectors.toList());
        }
        return currentSelections;
    }

    private Map<String, FileType> getMapFileTypes() throws BusinessException {
        Map<String, FileType> fileTypes = fileCompManager.retrieveListTypesPossible().stream()
                .collect(Collectors.toMap(f -> f.getCode(), f -> f));
        return fileTypes;
    }

    private ItemFileVO buildItemFileVO(final FileComp fileComp, Map<String, FileType> fileTypes) {
        final ItemFile itemFile = new ItemFile(fileComp, getPropertiesForDescriptions(), fileTypes, policyManager.getCurrentUser().getLanguage());
        boolean isOwner = itemFile.getFileComp().getCreateUser().equals(policyManager.getCurrentUser());
        boolean canAdmin = isOwner || policyManager.isRoot();
        itemFile.setActionDownload(canAdmin && hasActivity("depot,administration", "*"));
        itemFile.setActionAssociate(canAdmin && hasActivity("publication,administration", "*"));
        itemFile.setActionDelete(canAdmin && hasActivity("suppression,administration", "*"));
        itemFile.setActionModify(canAdmin && hasActivity("publication,administration", "*"));
        itemFile.setHasAssociates(hasAssociate(fileComp));
        return new ItemFileVO(itemFile);

    }

    private boolean hasActivity(String activity, String resourcePath) {
        return policyManager.isRoot() || policyManager.checkCurrentUserActivity(0, activity, resourcePath);
    }

    /**
     *
     * @param compositeGroup
     * @return
     */
    public boolean hasAssociate(ICompositeGroup compositeGroup) {
        return fileCompManager.getAssociatesNodesIds(compositeGroup).size() > 0;
    }

    /**
     *
     * @return
     */
    public boolean getFormValid() {
        return getFile() != null && getDescriptions() != null && !Strings.isNullOrEmpty(getDescriptions().values().stream().findFirst().orElse(null));
    }

    /**
     *
     * @return
     */
    public Boolean getIsFileUpload() {
        return Optional.ofNullable(getCurrentSelection()).map(i -> i.getFileComp()).map(i -> i.getSize() > 0).orElse(false);
    }

    /**
     *
     * @return
     */
    public boolean getIsValidForm() {
        return getCurrentSelection().getDescriptions() == null || Strings.isNullOrEmpty(getCurrentSelection().getDescriptions().get(Localization.getDefaultLocalisation()));
    }

    /**
     *
     * @return @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public List<String> getListDatesPossible() throws BusinessException {
        if (getCurrentSelections() == null) {
            return new LinkedList<>();
        }
        List<String> collect = getCurrentSelections().stream().map(i -> i.getUploadDate()).distinct().collect(Collectors.toList());
        if (collect.size() > 1) {
            collect.add(0, UIBeanFileComp.EMPTY_STRING);
        }
        return collect;
    }

    /**
     *
     * @return @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public List<String> getListTypesPossible() throws BusinessException {
        List<String> collect = getCurrentSelections().stream().map(i -> i.itemFile.getFileType()).distinct().collect(Collectors.toList());
        return collect;
    }

    /**
     *
     * @return @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public List<String> getListModifiables() throws BusinessException {
        List<String> collect = getCurrentSelections().stream().map(i -> i.getWritable()).distinct().collect(Collectors.toList());
        if (collect.size() > 1) {
            collect.add(0, UIBeanFileComp.EMPTY_STRING);
        }
        return collect;
    }

    /**
     *
     * @return @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public List<String> getListUploadersPossible() throws BusinessException {
        List<String> collect = getCurrentSelections().stream().map(i -> i.itemFile.getCreateUserName()).distinct().collect(Collectors.toList());
        if (collect.size() > 1) {
            collect.add(0, UIBeanFileComp.EMPTY_STRING);
        }
        return collect;
    }

    /**
     *
     * @return
     */
    public String getLocale() {
        return FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
    }

    /**
     *
     * @return
     */
    public List<String> getLocales() {
        return locales.stream()
                .map(locale -> localizationManager.getMessage(BUNDLE_PATH, locale))
                .collect(Collectors.toList());
    }

    /**
     * Gets the selected dataset details datatable.
     *
     * @return the selected dataset details datatable
     */
    public DataTable getSelectedFilecompDetailsDatatable() {
        return selectedFilecompDetailsDatatable;
    }

    /**
     *
     * @param selectedFilecompDetailsDatatable
     */
    public void setSelectedFilecompDetailsDatatable(final DataTable selectedFilecompDetailsDatatable) {
        this.selectedFilecompDetailsDatatable = selectedFilecompDetailsDatatable;
    }

    /**
     * Gets the time zone.
     *
     * @return the time zone
     */
    public TimeZone getTimeZone() {
        return TimeZone.getDefault();
    }

    /**
     * Gets the tree selection.
     *
     * @return the tree selection
     */
    public Tree getTreeSelection() {
        return treeSelection;
    }

    /**
     * Sets the tree selection.
     *
     * @param treeSelection the new tree selection
     */
    public void setTreeSelection(final Tree treeSelection) {
        this.treeSelection = treeSelection;
    }

    /**
     *
     * @return
     */
    public String getValidationSuccessfull() {
        return validationSuccessfull;
    }

    /**
     *
     */
    public void clearCurrentSelection() {
        _currentSelection = null;
    }

    /**
     *
     * @param validationSuccessfull
     */
    public void setValidationSuccessfull(String validationSuccessfull) {
        this.validationSuccessfull = validationSuccessfull;
    }

    /**
     *
     * @throws BusinessException
     */
    public void handleFileUpload() throws BusinessException {
        if (!testIfFilePresent()) {
            throw new BusinessException("null file");
        }
        final List<String> descriptionsList = Localization.getLocalisations().stream()
                .map((locale) -> getCurrentSelection().getDescriptions().get(locale))
                .collect(Collectors.toList());
        FileComp fileComp = fileCompManager.saveFile(getCurrentSelection().getFileComp(), descriptionsList);
        getCurrentSelections().add(buildItemFileVO(fileComp, getMapFileTypes()));
        setCurrentSelections(null);
    }

    /**
     *
     * @throws BusinessException
     */
    public void handleFileUploadModify() throws BusinessException {
        if (!testIfFilePresent()) {
            throw new BusinessException("null file");
        }
        final List<String> descriptionsList = Localization.getLocalisations().stream()
                .map((locale) -> getCurrentSelection().getDescriptions().get(locale))
                .collect(Collectors.toList());
        try {
            fileCompManager.updateFile(getCurrentSelection().getFileComp(), descriptionsList);
        } catch (Exception e) {
            throw new BusinessException("can't save file", e);
        }
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public String navigate() {
        return "fileComp";
    }

    /**
     *
     */
    public void removeFile() {
        try {
            fileCompManager.removeFile(getCurrentSelection().getFileComp());
            currentSelections.stream()
                    .filter(itemVo -> itemVo.itemFile.equals(getCurrentSelection()))
                    .findFirst()
                    .ifPresent(itemFile -> currentSelections.remove(itemFile));
            setCurrentSelections(null);
        } catch (BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("error", e);
        }
    }

    /**
     *
     * @param coreConfiguration
     */
    public void setCoreConfiguration(ICoreConfiguration coreConfiguration) {
        this.coreConfiguration = coreConfiguration;
    }

    /**
     *
     * @param filecompConfiguration
     */
    public void setFilecompConfiguration(IFileCompConfiguration filecompConfiguration) {
        this.filecompConfiguration = filecompConfiguration;
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    /**
     *
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param transactionManager
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     *
     * @return
     */
    public List<String> getLocalizations() {
        return Localization.getLocalisations();
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param facesContext
     * @param uiComponent
     * @param value
     * @throws ValidatorException
     */
    public void validatePastDateModify(FacesContext facesContext, UIComponent uiComponent, Object value) {
        LocalDate localToValid = (LocalDate) ((ValueHolder) uiComponent.findComponent("futureDateModify")).getValue();
        if (value == null || value.equals("") || localToValid == null) {
            return;
        }
        try {
            IntervalDate intervalDate = new IntervalDate(((LocalDate) value).atStartOfDay(), localToValid.atStartOfDay(), DateUtil.DD_MM_YYYY);
        } catch (BadExpectedValueException e) {
            String message = MessageFormat.format(localizationManager.getMessage(BUNDLE_PATH, PARAM_ALERT_BAD_INTERVAL_DATE),
                    DateUtil.getUTCDateTextFromLocalDateTime(((LocalDate) value).atStartOfDay(), DateUtil.DD_MM_YYYY),
                    DateUtil.getUTCDateTextFromLocalDateTime(localToValid.atStartOfDay(), DateUtil.DD_MM_YYYY)
            );
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message), e);

        }
    }

    /**
     *
     * @param facesContext
     * @param uiComponent
     * @param value
     * @throws ValidatorException
     */
    public void validateFuturDateModify(FacesContext facesContext, UIComponent uiComponent, Object value) {
        LocalDate localFromValid = (LocalDate) ((ValueHolder) uiComponent.findComponent("postDateModify")).getValue();
        if (value == null || value.equals("") || localFromValid == null) {
            return;
        }
        try {
            IntervalDate intervalDate = new IntervalDate(localFromValid.atStartOfDay(), ((LocalDate) value).atStartOfDay(), DateUtil.DD_MM_YYYY_FILE);
        } catch (BadExpectedValueException e) {
            String message = MessageFormat.format(localizationManager.getMessage(BUNDLE_PATH, PARAM_ALERT_BAD_INTERVAL_DATE),
                    DateUtil.getUTCDateTextFromLocalDateTime(localFromValid.atStartOfDay(), DateUtil.DD_MM_YYYY),
                    DateUtil.getUTCDateTextFromLocalDateTime(((LocalDate) value).atStartOfDay(), DateUtil.DD_MM_YYYY)
            );
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message), e);

        }
    }

    /**
     *
     * @param facesContext
     * @param uiComponent
     * @param value
     * @throws ValidatorException
     */
    public void validatePastDate(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value == null || value.equals("") || getCurrentSelection().getDateFin() == null) {
            return;
        }
        try {
            IntervalDate intervalDate = null;
            tryGetIntervaleDate(intervalDate, value);
        } catch (BadExpectedValueException e) {
            String message = MessageFormat.format(localizationManager.getMessage(BUNDLE_PATH, PARAM_ALERT_BAD_INTERVAL_DATE),
                    DateUtil.getUTCDateTextFromLocalDateTime(((LocalDate) value).atStartOfDay(), DateUtil.DD_MM_YYYY),
                    DateUtil.getUTCDateTextFromLocalDateTime(getCurrentSelection().getDateFin().atStartOfDay(), DateUtil.DD_MM_YYYY)
            );
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message), e);

        }
    }

    /**
     *
     * @param facesContext
     * @param uiComponent
     * @param value
     * @throws ValidatorException
     */
    public void validateFuturDate(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value == null || value.equals("") || getCurrentSelection().getDateDebut() == null) {
            return;
        }
        try {
            IntervalDate intervalDate = new IntervalDate(getCurrentSelection().getDateDebut().atStartOfDay(), ((LocalDate) value).atStartOfDay(), DateUtil.DD_MM_YYYY_FILE);
        } catch (BadExpectedValueException e) {
            String message = MessageFormat.format(localizationManager.getMessage(BUNDLE_PATH, PARAM_ALERT_BAD_INTERVAL_DATE),
                    DateUtil.getUTCDateTextFromLocalDateTime(getCurrentSelection().getDateDebut().atStartOfDay(), DateUtil.DD_MM_YYYY),
                    DateUtil.getUTCDateTextFromLocalDateTime(((LocalDate) value).atStartOfDay(), DateUtil.DD_MM_YYYY)
            );
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message), e);

        }
    }

    /**
     *
     * @return
     */
    public boolean getMyFiles() {
        return myFiles;
    }

    /**
     *
     * @param myFiles
     */
    public void setMyFiles(boolean myFiles) {
        this.myFiles = myFiles;
    }

    private boolean testIfFilePresent() {
        FacesMessage msg;
        if (getCurrentSelection().getFileComp().getSize() == 0) {
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, UIBeanFileComp.PROPERTY_MSG_ERROR, UIBeanFileComp.PROPERTY_MSG_NO_FILE);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            FacesContext.getCurrentInstance().validationFailed();
            UIBeanFileComp.LOGGER.error("No file selected");
            return false;
        };
        return true;
    }

    private void tryGetIntervaleDate(IntervalDate intervalDate, Object value) throws BadExpectedValueException {
        IntervalDate localIntervalDate = intervalDate;
        localIntervalDate = new IntervalDate(((LocalDate) value).atStartOfDay(), getCurrentSelection().getDateDebut().atStartOfDay(), DateUtil.DD_MM_YYYY_FILE);
    }

    /**
     *
     * @return
     */
    public Map<String, Properties> getPropertiesForDescriptions() {
        Map<String, Properties> descriptionProperties = new HashMap<>();
        Localization.getLocalisations().stream().filter((locale) -> !(Localization.getDefaultLocalisation().equals(locale))).forEach((locale) -> {
            descriptionProperties.put(locale, localizationManager.newProperties(FileComp.NAME_ENTITY_JPA, FileComp.ENTITY_DESCRIPTION_COLUM_NAME, new Locale(locale)));
        });
        return descriptionProperties;
    }

//    class FileUploader {
//
//        String fileName;
//        byte[] fileData;
//        long fileSize;
//        UploadedFile file;
//
//        public FileUploader(FileUploadEvent event) {
//            UploadedFile fileUpload = event.getFile();
//            fileName = fileUpload.getFileName();
//            file = event.getFile();
//            fileData = file == null ? new byte[0] : fileUpload.getContents();
//            fileSize = file.getSize();
//        }
//    }
    /**
     *
     */
    public class ItemFileVO extends UserVO {

        private ItemFile itemFile;

        private ItemFileVO(ItemFile itemFile) {
            super(itemFile.getFileComp().getOwnGroup(WhichTree.TREEDATASET), WhichTree.TREEDATASET);
            this.itemFile = itemFile;
        }

        /**
         *
         * @return
         */
        public ItemFile getItemFile() {
            return itemFile;
        }

        /**
         *
         * @return
         */
        @Override
        public String getLogin() {
            return String.format("file_%s", itemFile.getCode());
        }

        /**
         *
         * @return
         */
        @Override
        public String getNom() {
            return itemFile.getOriginalFileName();
        }

        /**
         *
         * @return
         */
        @Override
        public String getPrenom() {
            return itemFile.getDescription();
        }

        /**
         *
         * @return
         */
        public String getUploadDate() {
            if (itemFile.getFileComp() == null || itemFile.getFileComp().getCreateDate() == null) {
                return "";
            }
            return DateUtil.getUTCDateTextFromLocalDateTime(itemFile.getFileComp().getCreateDate(), DateUtil.DD_MM_YYYY);
        }

        /**
         *
         * @return
         */
        public String getWritable() {
            return itemFile.getCreateUserName().equals(policyManager.getCurrentUserLogin()) ? "modifiable" : "unmodifiable";
        }

        /**
         *
         * @return
         */
        public String getCompleteName() {
            return String.format("%s > %s", itemFile.getShortDescription(), itemFile.getOriginalFileName());
        }

        /**
         *
         * @return
         */
        @Override
        public IUser getUser() {
            return null;
        }

        /**
         *
         * @return
         */
        @Override
        public Group getGroup() {
            return itemFile.getFileComp().getOwnGroup(WhichTree.TREEDATASET);
        }

        /**
         *
         * @param uploadFile
         * @return
         * @throws BusinessException
         */
        public StreamedContent upload(FileComp uploadFile) throws BusinessException {
            byte[] data = fileCompManager.getFileCompById(uploadFile.getId())
                    .map(fileComp -> fileComp.getData())
                    .orElse(new byte[0]);
            ByteArrayInputStream stream = new ByteArrayInputStream(data);
            return new DefaultStreamedContent(stream, uploadFile.getContentType(), uploadFile.getFileName());
        }

        /**
         *
         * @return
         */
        public String getNoUserVOSelected() {
            return PARAM_MSG_NO_FILE_SELECTED;
        }

        /**
         *
         * @return
         */
        public String getUserVOSelected() {
            return PARAM_MSG_FILE_SELECTED;
        }
    }
}
