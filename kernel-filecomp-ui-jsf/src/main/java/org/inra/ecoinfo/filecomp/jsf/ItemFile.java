package org.inra.ecoinfo.filecomp.jsf;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import org.apache.commons.lang.ArrayUtils;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.filecomp.entity.FileType;
import org.inra.ecoinfo.localization.entity.Localization;

/**
 *
 * @author ptcherniati
 */
public class ItemFile implements Serializable, Comparable<ItemFile> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final String locale;
    private FileComp fileComp;
    private Map<String, String> descriptions = new HashMap();
    private Boolean actionModify = false;
    private Boolean actionAssociate = false;
    private Boolean actionDelete = false;
    private Boolean actionDownload = false;
    private boolean isModify = false;
    private boolean hasAssociates = false;
    private Map<String, FileType> fileTypes = new HashMap();

    /**
     *
     * @param locale
     * @param map
     * @param fileTypes
     */
    public ItemFile(String locale, Map<String, FileType> fileTypes) {
        this.fileTypes = fileTypes;
        this.fileComp = new FileComp();
        this.locale = Strings.isNullOrEmpty(locale) ? Localization.getDefaultLocalisation() : locale;
        this.fileComp.setFileType(fileTypes.values().stream().findAny().orElse(null));
        this.descriptions = new HashMap();
    }

    /**
     *
     * @param fileComp
     * @param descriptionProperties
     * @param fileTypes
     * @param string
     * @param locale
     */
    public ItemFile(FileComp fileComp, Map<String, Properties> descriptionProperties, Map<String, FileType> fileTypes, String locale) {
        this.locale = Strings.isNullOrEmpty(locale) ? Localization.getDefaultLocalisation() : locale;
        this.fileComp = fileComp;
        this.fileTypes = fileTypes;
        if (fileComp != null) {
            setDescription(fileComp, descriptionProperties);
        }
    }

    /**
     *
     * @return
     */
    public boolean hasAssociates() {
        return hasAssociates;
    }

    /**
     *
     * @return
     */
    public boolean getHasAssociates() {
        return hasAssociates;
    }

    /**
     *
     * @param hasAssociates
     */
    public void setHasAssociates(boolean hasAssociates) {
        this.hasAssociates = hasAssociates;
    }

    /**
     *
     * @return
     */
    public Boolean getActionAssociate() {
        return true;//actionAssociate;
    }

    /**
     *
     * @param actionAssociate
     */
    public void setActionAssociate(Boolean actionAssociate) {
        this.actionAssociate = actionAssociate;
    }

    /**
     *
     * @return
     */
    public Boolean getActionDelete() {
        return actionDelete;
    }

    /**
     *
     * @param actionDelete
     */
    public void setActionDelete(Boolean actionDelete) {
        this.actionDelete = actionDelete;
    }

    /**
     *
     * @return
     */
    public Boolean getActionDownload() {
        return actionDownload;
    }

    /**
     *
     * @param actionDownload
     */
    public void setActionDownload(Boolean actionDownload) {
        this.actionDownload = actionDownload;
    }

    /**
     *
     * @return
     */
    public Boolean getActionModify() {
        return actionModify;
    }

    /**
     *
     * @param actionModify
     */
    public void setActionModify(Boolean actionModify) {
        this.actionModify = actionModify;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getCreateDate() {
        return Optional.ofNullable(fileComp).map(file -> file.getCreateDate()).orElse(null);
    }

    /**
     *
     * @param createDate
     */
    public void setCreateDate(LocalDateTime createDate) {
        this.fileComp.setCreateDate(createDate);
    }

    /**
     *
     * @return
     */
    public String getCreateUserName() {
        return Optional.ofNullable(fileComp)
                .map(f -> f.getCreateUser())
                .map(u -> u.getNom())
                .orElse("");
    }

    /**
     *
     * @return
     */
    public LocalDate getDateDebut() {
        return Optional.ofNullable(fileComp).map(file -> file.getDateDebutPeriode()).orElse(null);
    }

    /**
     *
     * @param dateDebut
     */
    public void setDateDebut(LocalDate dateDebut) {
        this.fileComp.setDateDebutPeriode(dateDebut);
    }

    /**
     *
     * @return
     */
    public LocalDate getDateFin() {
        return Optional.ofNullable(fileComp).map(file -> file.getDateFinPeriode()).orElse(null);
    }

    /**
     *
     * @param dateFin
     */
    public final void setDateFin(LocalDate dateFin) {
        this.fileComp.setDateFinPeriode(dateFin);
    }

    /**
     *
     * @return
     */
    public Map<String, String> getDescriptions() {
        return descriptions;
    }

    /**
     *
     * @param descriptions
     */
    public void setDescriptions(Map<String, String> descriptions) {
        this.descriptions = descriptions;
    }

    /**
     *
     * @return
     */
    public FileComp getFileComp() {
        return fileComp;
    }

    /**
     *
     * @param fileComp
     */
    public void setFileComp(FileComp fileComp) {
        this.fileComp = fileComp;
    }

    /**
     *
     * @return
     */
    public String getOriginalFileName() {
        return Optional.ofNullable(fileComp).map(file -> file.getFileName()).orElse("");
    }

    /**
     *
     * @param originalFileName
     */
    public void setOriginalFileName(String originalFileName) {
        this.fileComp.setFileName(originalFileName);
    }

    /**
     *
     * @return
     */
    public String getFileType() {
        return Optional.ofNullable(fileComp)
                .map(file -> file.getFileType())
                .map(filetype->filetype.getName())
                .orElse("");
    }

    /**
     *
     * @param fileType
     */
    public void setFileType(String fileType) {
        this.fileComp.setFileType(fileTypes.get(fileType));
    }

    /**
     *
     * @return
     */
    public String getSize() {
        return Integer.toString(fileComp.getSize());
    }

    /**
     *
     * @param size
     */
    public void setSize(String size) {
        fileComp.setSize(Integer.parseInt(Optional.ofNullable(size).orElse("0")));
    }

    /**
     *
     * @param fileCompRepository
     * @return
     */
    public String getAbsoluteFilePath(String fileCompRepository) {
        return Optional.ofNullable(fileComp).map(file -> file.getAbsoluteFilePath(fileCompRepository)).orElse("");

    }

    /**
     *
     * @return
     */
    public String getCode() {
        return fileComp.getCode();
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.fileComp.setCode(code);
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        String description = descriptions.get(locale);
        return description == null ? "" : description;
    }

    /**
     *
     * @return
     */
    public String getShortDescription() {
        String description = getDescription();
        if (description.length() <= 120) {
            return description;
        }
        return description.substring(0, 120).concat(" ...");
    }

    /**
     *
     * @return
     */
    public String getLastModifyUserName() {
        return Optional.ofNullable(fileComp)
                .map(f -> f.getLastModifyUser())
                .map(u -> u.getNom())
                .orElse("");
    }

    /**
     *
     * @return
     */
    public LocalDateTime getLastModifyDate() {
        return Optional.ofNullable(fileComp).map(file -> file.getLastModifyDate()).orElse(null);
    }

    /**
     *
     * @param lastModifyDate
     */
    public void setLastModifyDate(LocalDateTime lastModifyDate) {
        this.fileComp.setLastModifyDate(lastModifyDate);
    }

    /**
     *
     * @param code
     */
    public void changeCode(String code) {
        Optional.ofNullable(fileComp).ifPresent(file -> file.setCode(code));
        this.isModify = true;
    }

    /**
     *
     * @return
     */
    public boolean isModify() {
        return isModify;
    }

    /**
     *
     * @param isModify
     */
    public void setModify(boolean isModify) {
        this.isModify = isModify;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(getCode());
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemFile other = (ItemFile) obj;
        return Objects.equals(getCode(), other.getCode());
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(ItemFile o) {
        if (this.equals(o)) {
            return 0;
        }
        if (getFileType().equals(o.getFileType())) {
            return this.getOriginalFileName().compareToIgnoreCase(o.getOriginalFileName());
        }
        return this.getFileType().compareTo(o.getFileType());
    }

    /**
     *
     * @return
     */
    public String getOrder() {
        return fileComp.getFileType().getName().concat(getOriginalFileName());
    }

    /**
     *
     * @return
     */
    public boolean getMandatory() {
        return Optional.ofNullable(fileComp).map(f -> f.getMandatory()).orElse(false);
    }

    /**
     *
     * @param mandatory
     */
    public void setMandatory(boolean mandatory) {
        fileComp.setMandatory(mandatory);
    }

    /**
     *
     * @return
     */
    public boolean getForApplication() {
        return Optional.ofNullable(fileComp).map(f -> f.getForApplication()).orElse(false);
    }

    /**
     *
     * @param mandatory
     */
    public void setForApplication(boolean mandatory) {
        fileComp.setForApplication(mandatory);
    }

    private void setDescription(FileComp fileComp, Map<String, Properties> descriptionProperties) {
        String description = fileComp.getDescription();
        this.descriptions.put(Localization.getDefaultLocalisation(), Strings.isNullOrEmpty(description) ? "" : description);
        Localization.getLocalisations()
                .stream().
                filter((localLocale) -> !(localLocale.equals(Localization.getDefaultLocalisation())))
                .forEach((localLocale) -> {
                    descriptions.put(
                            localLocale, Strings.isNullOrEmpty(description) ? 
                                    "" : 
                                    descriptionProperties.get(localLocale).getProperty(description, description)
                    );
        });
    }

    /**
     *
     * @return
     */
    public String getCompleteName() {
        return String.format("%s > %s", getShortDescription(), getOriginalFileName());
    }

    /**
     *
     * @return
     */
    public byte[] getData() {
        return Optional.ofNullable(fileComp).map(file -> file.getData()).orElse(new byte[0]);
    }

    /**
     *
     * @param data
     */
    public void setData(byte[] data) {
        this.fileComp.setData(Optional.ofNullable(data).orElse(new byte[0]));
    }

    /**
     *
     * @return
     */
    public boolean isLoadedFile() {
        return !ArrayUtils.isEmpty(getData());
    }

    void setContentType(String contentType) {
        fileComp.setContentType(contentType);
    }

    String getContentType() {
        return Optional.ofNullable(fileComp).map(f -> f.getContentType()).orElse("application/octet-stream");
    }
}
