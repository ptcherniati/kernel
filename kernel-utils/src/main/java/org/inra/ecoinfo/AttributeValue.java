package org.inra.ecoinfo;

import javax.persistence.metamodel.SingularAttribute;

/**
 *
 * @author ptchernia
 */
public class AttributeValue {

    /**
     *
     * @param <C>
     * @param <V>
     * @param attribute
     * @param value
     * @return
     */
    public static <C, V> AttributeValue getIntance(SingularAttribute<C, V> attribute, V value) {
        return new AttributeValue(attribute, value);
    }

    /**
     *
     */
    private SingularAttribute attribute;

    /**
     *
     */
    private Object value;

    private <C, V> AttributeValue(SingularAttribute<C, V> attribute, V value) {
        this.attribute = attribute;
        this.value = value;
    }

    /**
     *
     * @return
     */
    public SingularAttribute getAttribute() {
        return attribute;
    }

    /**
     *
     * @param attribute
     */
    public void setAttribute(SingularAttribute attribute) {
        this.attribute = attribute;
    }

    /**
     *
     * @return
     */
    public Object getValue() {
        return value;
    }

    /**
     *
     * @param value
     */
    public void setValue(Object value) {
        this.value = value;
    }
}
