/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.logs;

import ch.qos.logback.core.recovery.ResilientFileOutputStream;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.util.FileSize;
import ch.qos.logback.core.util.FileUtil;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author ptcherniati
 * @param <E>
 */
public class SimpleHeaderRollingFileAppender<E> extends RollingFileAppender<E> {

    /**
     *
     */
    public static final long DEFAULT_BUFFER_SIZE = 8192;
    private static final InheritableThreadLocal<ResilientFileOutputStream> INHERITABLE_THREAD_LOCAL = new InheritableThreadLocal<ResilientFileOutputStream>();

    /**
     *
     * @return
     */
    public static InheritableThreadLocal getLocalThread(){
        return INHERITABLE_THREAD_LOCAL;
    }
    
    /**
     *
     * @param file_name
     * @throws IOException
     */
    @Override
    public void openFile(String file_name) throws IOException {
        lock.lock();
        try {
            File file = new File(file_name);
            boolean result = FileUtil.createMissingParentDirectories(file);
            if (!result) {
                addError("Failed to create parent directories for [" + file.getAbsolutePath() + "]");
            }

            ResilientFileOutputStream resilientFos = new ResilientFileOutputStream(file, append, new FileSize(DEFAULT_BUFFER_SIZE).getSize());
            resilientFos.setContext(context);
            getLocalThread().set(resilientFos);
            setOutputStream(resilientFos);
        } finally {
            lock.unlock();
        }
    }
    
}
