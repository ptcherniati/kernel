/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.logs;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.html.DefaultThrowableRenderer;
import ch.qos.logback.classic.pattern.MDCConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.CoreConstants;
import ch.qos.logback.core.LayoutBase;
import ch.qos.logback.core.html.IThrowableRenderer;
import ch.qos.logback.core.pattern.Converter;
import ch.qos.logback.core.pattern.ConverterUtil;
import ch.qos.logback.core.pattern.parser.Node;
import ch.qos.logback.core.pattern.parser.Parser;
import ch.qos.logback.core.recovery.ResilientFileOutputStream;
import ch.qos.logback.core.spi.ScanException;
import com.opencsv.CSVWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ptcherniati
 */
public class CSSLayout extends LayoutBase<ILoggingEvent> {

    static final String DEFAULT_CONVERSION_PATTERN = "%date%thread%level%logger%mdc%msg";

    protected String pattern;

    IThrowableRenderer<ILoggingEvent> throwableRenderer;


    protected Converter<ILoggingEvent> head;

    /**
     *
     */
    public CSSLayout() {
        pattern = DEFAULT_CONVERSION_PATTERN;
        throwableRenderer = new DefaultThrowableRenderer();
    }

    /**
     * Set the <b>ConversionPattern </b> option.This is the string which
 controls formatting and consists of a mix of literal content and
 conversion specifiers.
     * @param conversionPattern
     */
    public void setPattern(String conversionPattern) {
        pattern = conversionPattern;
    }

    /**
     *
     * @return
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * Parses the pattern and creates the Converter linked list.
     */
    @Override
    public void start() {
        int errorCount = 0;
        if (throwableRenderer == null) {
            addError("ThrowableRender cannot be null.");
            errorCount++;
        }

        try {
            Parser<ILoggingEvent> p = new Parser<ILoggingEvent>(pattern);
            p.setContext(getContext());
            Node t = p.parse();
            this.head = p.compile(t, getEffectiveConverterMap());
            ConverterUtil.startConverters(this.head);
        }
        catch (ScanException ex) {
            addError("Incorrect pattern found", ex);
            errorCount++;
        }

        if (errorCount == 0) {
            super.started = true;
        }
    }

    /**
     * Returns a map where the default converter map is merged with the map
     * contained in the context.
     * @return 
     */
    public Map<String, String> getEffectiveConverterMap() {
        Map<String, String> effectiveMap = new HashMap<String, String>();

        // add the least specific map fist
        Map<String, String> defaultMap = getDefaultConverterMap();
        if (defaultMap != null) {
            effectiveMap.putAll(defaultMap);
        }

        // contextMap is more specific than the default map
        Context context = getContext();
        if (context != null) {
            @SuppressWarnings("unchecked")
            Map<String, String> contextMap = (Map<String, String>) context.getObject(CoreConstants.PATTERN_RULE_REGISTRY);
            if (contextMap != null) {
                effectiveMap.putAll(contextMap);
            }
        }
        return effectiveMap;
    }

    protected Map<String, String> getDefaultConverterMap() {
        return PatternLayout.defaultConverterMap;
    }

    /**
     *
     * @param event
     * @return
     */
    public String doLayout(ILoggingEvent event) {
        StringBuilder buf = new StringBuilder();

        String level = event.getLevel().toString().toLowerCase();
        Converter<ILoggingEvent> c = head;
        List<String> entries = new LinkedList<>();
        while (c != null) {
            entries.add(c.convert(event));
            c = c.getNext();
        }
        writeCSVLine(buf, entries);

        if (event.getThrowableProxy() != null) {
            throwableRenderer.render(buf, event);
        }
        return buf.toString();
    }

    private void writeCSVLine(StringBuilder buf, List<String> entries) {
        Writer stringWriter = new StringWriter();
        CSVWriter csvWriter = new CSVWriter(stringWriter, ';', '"', '\\', ";");
        csvWriter.writeNext(entries.toArray(new String[]{}));
        buf.append(stringWriter.toString());
        buf.append("\n");
    }

    /**
     *
     * @return
     */
    public IThrowableRenderer getThrowableRenderer() {
        return throwableRenderer;
    }

    /**
     *
     * @param throwableRenderer
     */
    public void setThrowableRenderer(IThrowableRenderer<ILoggingEvent> throwableRenderer) {
        this.throwableRenderer = throwableRenderer;
    }

    /**
     *
     * @return
     */
    @Override
    public String getFileHeader() {
        ResilientFileOutputStream resilientFileOutputStream = (ResilientFileOutputStream) SimpleHeaderRollingFileAppender.getLocalThread().get();
        boolean hasNoHeader = resilientFileOutputStream == null || (resilientFileOutputStream.getFile().exists() && resilientFileOutputStream.getFile().length() == 0);
        StringBuilder sbuf = new StringBuilder();
        if (hasNoHeader) {
            Converter c = head;;
            String name;
            while (c != null) {
                name = computeConverterName(c);
                if (name == null) {
                    c = c.getNext();
                    continue;
                }
                sbuf.append(computeConverterName(c));
                sbuf.append(";");
                c = c.getNext();
            }
        }
        return sbuf.toString();
    }

    protected String computeConverterName(Converter c) {
        if (c instanceof MDCConverter) {
            MDCConverter mc = (MDCConverter) c;
            String key = mc.getFirstOption();
            if (key != null) {
                return key;
            } else {
                return "MDC";
            }
        } else {
            String className = c.getClass().getSimpleName();
            int index = className.indexOf("Converter");
            if (index == -1) {
                return className;
            } else {
                return className.substring(0, index);
            }
        }
    }

}
