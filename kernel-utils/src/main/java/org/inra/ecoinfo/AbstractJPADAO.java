package org.inra.ecoinfo;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjuster;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Transient;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import org.hibernate.NaturalIdLoadAccess;
import org.hibernate.Session;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * The Class AbstractJPADAO.
 *
 * @param <T> the generic type
 */
public class AbstractJPADAO<T> implements IDAO<T> {

    /**
     * The Logger LOGGER.
     */
    @Transient
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractJPADAO.class.getName());
    public static final String LOGGER_ERROR = "Method %s -> %s";

    /**
     *
     * don't modify builder
     *      * builder = entityManager.getCriteriaBuilder();</p>
     * Was setting with entityManager
     */
    protected static CriteriaBuilder builder;

    /**
     * either <p><i>before</i>  isnull or <i>"before"  </i> not after  <i>"date"</i></p>
     * nor <p><i>after</i>  isnull or  <i>"date"</i>  not after  <i>"after"</i></p>
     *
     * @param date
     * @param before
     * @param after
     * @return
     */
    public static final Predicate whereDateBetween(Path<? extends TemporalAdjuster> date, Path<? extends TemporalAdjuster> before, Path<? extends TemporalAdjuster> after) {
        Expression<LocalDateTime> toCompare = builder.function("date_part", LocalDateTime.class, builder.literal("epoch"), date);
        Expression<LocalDateTime> from = builder.function("date_part", LocalDateTime.class, builder.literal("epoch"), before);
        Expression<LocalDateTime> to = builder.function("date_part", LocalDateTime.class, builder.literal("epoch"), after);
        return builder.and(
                builder.or(
                        builder.isNull(before),
                        builder.greaterThan(from, toCompare).not()
                ),
                builder.or(
                        builder.isNull(after),
                        builder.greaterThan(toCompare, to).not()
                )
        );
    }
    /**
     * The EntityManager entity manager.
     */
    @PersistenceContext(unitName = "JPAPu")
    protected EntityManager entityManager;
    /**
     * The JpaTransactionManager transaction manager.
     */
    protected org.springframework.orm.jpa.JpaTransactionManager transactionManager;

    /**
     * Begin default transaction.
     *
     * @return the TransactionStatus transaction status
     */
    @Override
    public TransactionStatus beginDefaultTransaction() {
        return transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRED));
    }

    /**
     * Begin new transaction.
     *
     * @return the TransactionStatus transaction status
     */
    @Override
    public TransactionStatus beginNewTransaction() {
        return transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));
    }

    /**
     * Commit transaction.
     *
     * @param status the TransactionStatus status
     */
    @Override
    public void commitTransaction(final TransactionStatus status) {
        if (!status.isCompleted() && !status.isRollbackOnly()) {
            transactionManager.commit(status);
        }
    }

    /**
     * Evict.
     *
     * @param object the Object object
     * @throws PersistenceException the persistence exception
     */
    public void evict(final Object object) throws PersistenceException {
        try {
            getSession().evict(object);
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Flush.
     */
    @Override
    public void flush() {
        entityManager.flush();
        entityManager.clear();
    }

    /**
     * Gets the List<T> all.
     *
     * @param <C>
     * @param clazz the Class clazz
     * @return the List<T> all
     */
    @Override
    public <C> List<C> getAll(final Class<C> clazz) {
        CriteriaQuery<C> query = builder.createQuery(clazz);
        Root<C> all = query.from(clazz);
        query.select(all);
        return getResultList(query);
    }

    /**
     * Gets the List<T> all.
     *
     * @param <C>
     * @param clazz the Class clazz
     * @return the List<T> all
     */
    @Override
    public <C, T extends Comparable<? super T>> List<C> getAllBy(final Class<C> clazz, Function<? super C, ? extends T> comparingFunction) {
        CriteriaQuery<C> query = builder.createQuery(clazz);
        Root<C> all = query.from(clazz);
        query
                .select(all);
        return getResultAsStream(query)
                .sorted(Comparator.comparing(comparingFunction))
                .collect(Collectors.toList());
    }

    /**
     * Gets the List<T> all.
     *
     * @param <C>
     * @param clazz the Class clazz
     * @return the List<T> all
     */
    @Override
    public <C> List<C> getAllBy(final Class<C> clazz, SingularAttribute<C, ?> attribute) {
        CriteriaQuery<C> query = builder.createQuery(clazz);
        Root<C> all = query.from(clazz);
        query
                .select(all)
                .orderBy(builder.asc(all.get(attribute)));
        return getResultList(query);
    }

    /**
     * Gets the int count.
     *
     * @param clazz the Class clazz
     * @return the int count
     */
    @Override
    public Long getCount(final Class<T> clazz) {
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<T> all = query.from(clazz);
        query.select(builder.count(all));
        return getOptional(query)
                .orElseGet(() -> Long.MIN_VALUE);
    }

    /**
     * Gets the JpaTransactionManager transaction manager.
     *
     * @return the JpaTransactionManager transaction manager
     */
    public org.springframework.orm.jpa.JpaTransactionManager getTransactionManager() {
        return transactionManager;
    }

    /**
     * Sets the void transaction manager.
     *
     * @param transactionManager the JpaTransactionManager transaction manager
     */
    public void setTransactionManager(final org.springframework.orm.jpa.JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     * Merge.
     *
     * @param object the T object
     * @return the T t
     * @throws PersistenceException the persistence exception
     */
    @Override
    public T merge(final T object) throws PersistenceException {
        try {
            return entityManager.merge(object);
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Removes the.
     *
     * @param object the T object
     * @throws PersistenceException the persistence exception
     */
    @Override
    public void remove(final T object) throws PersistenceException {
        try {
            entityManager.remove(object);
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Removes the all.
     *
     * @param clazz the Class<?> clazz
     * @throws PersistenceException the persistence exception
     */
    @Override
    public void removeAll(final Class<T> clazz) throws PersistenceException {
        try {

            CriteriaDelete<T> delete = builder.createCriteriaDelete(clazz); 
            Root<T> deleteRoot = delete.from(clazz);
            final Query query = entityManager.createQuery(delete);
            query.executeUpdate();
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param clazz
     * @throws PersistenceException
     */
    public void delete(final Class clazz) throws PersistenceException {

        CriteriaDelete queryDelete = builder.createCriteriaDelete(clazz);
        queryDelete.from(clazz);
        try {
            entityManager.createQuery(queryDelete).executeUpdate();
        } catch (Exception e) {
            throw new PersistenceException("Can't troncate table", e);
        }
    }

    /**
     * Removes the all generic.
     *
     * @param clazz the Class<?> clazz
     * @throws PersistenceException the persistence exception
     */
    public void removeAllGeneric(final Class<?> clazz) throws PersistenceException {
        try {

            CriteriaDelete<?> delete = builder.createCriteriaDelete(clazz);
            final Query query = entityManager.createQuery(delete);
            query.executeUpdate();
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Rollback transaction.
     *
     * @param status the TransactionStatus status
     */
    @Override
    public void rollbackTransaction(final TransactionStatus status) {
        if (!status.isCompleted()) {
            transactionManager.rollback(status);
        }
    }

    /**
     * Save or update.
     *
     * @param object the T object
     * @throws PersistenceException the persistence exception
     */
    @Override
    public void saveOrUpdate(final T object) throws PersistenceException {
        try {
            entityManager.persist(object);
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Save or update generic.
     *
     * @param object the Object object
     * @throws PersistenceException the persistence exception
     */
    public void saveOrUpdateGeneric(final Object object) throws PersistenceException {
        try {
            entityManager.persist(object);
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Builds the query ids.
     *
     * @param sce the List<?> sce
     * @return the String string
     */
    protected String buildQueryIds(final List<?> sce) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sce.size(); i++) {
            sb.append(sce.get(i));
            if (!(i == sce.size() - 1)) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    /**
     *
     * @param query
     * @param hqlQueryText
     * @return
     */
    public String toSql(String query) {
        return entityManager.createQuery(query).unwrap(org.hibernate.query.Query.class).getQueryString();
    }

    /**
     *
     * @param query
     * @param hqlQueryText
     * @return
     */
    public String toSql(CriteriaQuery query) {
        return entityManager.createQuery(query).unwrap(org.hibernate.query.Query.class).getQueryString();
    }

    /**
     * Gets the T by id.
     *
     * @param clazz the Class clazz
     * @param id the long id
     * @return the T by id
     */
    @Override
    public Optional<T> getById(final Class<T> clazz, final long id) {
        return Optional.ofNullable(entityManager.find(clazz, id));
    }

    /**
     *
     * @param <T>
     * @param query
     * @return
     */
    protected <T> T getFirstOrNull(CriteriaQuery<T> query) {
        return getOptional(query)
                .orElse((T) null);
    }

    /**
     *
     * @param <T>
     * @param query
     * @return
     */
    protected <T> Optional<T> getOptional(CriteriaQuery<T> query) {
        return getResultListToStream(query)
                .findFirst()
                .map(u -> (T) u);
    }

    /**
     *
     * @param <T>
     * @param query
     * @return
     */
    protected <T> List<T> getResultList(CriteriaQuery<T> query) {
        try {
            return entityManager.createQuery(query).getResultList();
        } catch (Exception e) {
            LOGGER.error(String.format(LOGGER_ERROR, "getResultList", e.getMessage()), e);
            return new LinkedList();
        }
    }

    /**
     *      * <B>Make a stream from to the database can potentialy open many
     * connexions</B></p>
     *
     * @param <T>
     * @param query
     * @return
     */
    protected <T> Stream<T> getResultAsStream(CriteriaQuery<T> query) {
        try {
            return ((org.hibernate.query.Query) entityManager.createQuery(query)).stream().map(this::testClazz);
        } catch (Exception e) {
            LOGGER.error(String.format(LOGGER_ERROR, "getResultAsStream", e.getMessage()), e);
            return Stream.empty();
        }
    }

    /* 
     * <P> get the result from datatbase then make a stream
     *
     * @param <T>
     * @param query
     * @return
     */
    protected <T> Stream<T> getResultListToStream(CriteriaQuery<T> query) {
        try {
            return getResultList(query).stream();
        } catch (Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            return Stream.empty();
        }
    }

    /**
     *
     * @param <T>
     * @param delete
     * @return
     */
    protected <T> int delete(CriteriaDelete<T> delete) {
        return entityManager.createQuery(delete).executeUpdate();
    }

    /**
     *
     * @param <T>
     * @param delete
     * @return
     */
    protected <T> int update(CriteriaUpdate<T> delete) {
        return entityManager.createQuery(delete).executeUpdate();
    }

    /**
     *
     */
    public void initCriteriaBuilder() {
        if (builder == null) {
            builder = entityManager.getCriteriaBuilder();
        }
    }

    /**
     *
     * @param date
     * @return
     */
    public Expression<LocalDateTime> castToLocalDateTime(Path<? extends TemporalAdjuster> date) {
        return builder.function("CAST", LocalDateTime.class, date);
    }

    /**
     *
     * @param <T>
     * @param queryString
     * @param clazz
     * @return
     */
    public <T> Stream<T> getStreamFromNativeQuery(String queryString, Class<T> clazz) {
        return entityManager.createQuery(queryString, clazz)
                .getResultList()
                .stream()
                .map(this::testClazz);
    }

    private <T> T testClazz(T arg) {
        if (arg instanceof Object[]) {
            return (T) ((Object[]) arg)[0];
        }
        return arg;
    }

    /**
     *
     * @return
     */
    public Session getSession() {
        return ((Session) entityManager.getDelegate());
    }

    /**
     *
     * @param <C>
     * @param <K>
     * @param clazz
     * @param naturalId
     * @return
     */
    public <C extends Serializable, K extends Serializable> Optional<C> getByNaturalId(Class<C> clazz, K naturalId) {
        return getSession()
                .bySimpleNaturalId(clazz)
                .loadOptional(naturalId);
    }

    /**
     *
     * @param <C>
     * @param <K>
     * @param clazz
     * @param attributesValues
     * @return
     */
    public <C, K> Optional<C> getByNaturalId(Class<C> clazz, AttributeValue... attributesValues) {
        NaturalIdLoadAccess<C> naturalIdLoadAccess = getSession().byNaturalId(clazz);
        for (int i = 0; i < attributesValues.length; i++) {
            AttributeValue attributeValue = attributesValues[i];
            naturalIdLoadAccess.using(attributeValue.getAttribute().getName(), attributeValue.getValue());
        }
        return naturalIdLoadAccess
                .loadOptional();
    }

    /**
     *
     * @param <C>
     * @param <K>
     * @param clazz
     * @param naturalIds
     * @return
     */
    public <C extends Serializable, K extends Serializable> List<C> getByNaturalsId(Class<C> clazz, K... naturalIds) {
        return getSession()
                .byMultipleIds(clazz)
                .multiLoad(naturalIds);
    }

    /**
     *
     * @param <C>
     * @param <K>
     * @param clazz
     * @param naturalIds
     * @return
     */
    public <C extends Serializable, K extends Serializable> List<C> getByNaturalsId(Class<C> clazz, List<K> naturalIds) {
        return getSession()
                .byMultipleIds(clazz)
                .multiLoad(naturalIds);
    }
}
