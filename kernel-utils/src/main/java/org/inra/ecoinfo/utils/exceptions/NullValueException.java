/**
 * OREILacs project - see LICENCE.txt for use created: 25 févr. 2009 14:21:47
 */
package org.inra.ecoinfo.utils.exceptions;

/**
 * The Class NullValueException.
 *
 * @author "Antoine Schellenberger"
 */
public class NullValueException extends Exception {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new null value exception.
     */
    public NullValueException() {
        super();
    }

    /**
     * Instantiates a new null value exception.
     *
     * @param string
     * @param message the message
     * @link(String) the message
     */
    public NullValueException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new null value exception.
     *
     * @param message the message
     * @param thrwbl
     * @param cause the cause
     * @link(String) the message
     * @link(Throwable) the cause
     */
    public NullValueException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new null value exception.
     *
     * @param thrwbl
     * @param cause the cause
     * @link(Throwable) the cause
     */
    public NullValueException(final Throwable cause) {
        super(cause);
    }
}
