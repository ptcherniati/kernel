/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.utils;

/**
 *
 * @author tcherniatinsky
 * @param <T>
 */
@FunctionalInterface
public interface IFormatter<T> {

    /**
     * Take a T object  and eventually some args to return a String
     *
     * @param toFormat
     * @param arguments
     * @return the string object formatted
     */
    String format(T toFormat, Object... arguments);
}
