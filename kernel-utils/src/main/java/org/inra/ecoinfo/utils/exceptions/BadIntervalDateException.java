/**
 * OREILacs project - see LICENCE.txt for use created: 25 févr. 2009 14:21:16
 */
package org.inra.ecoinfo.utils.exceptions;

/**
 * The Class BadIntervalDateException.
 *
 * @author "Antoine Schellenberger"
 */
public class BadIntervalDateException extends Exception {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new bad interval date exception.
     *
     * @param string
     * @param message the message
     * @link(String) the message
     */
    public BadIntervalDateException(final String message) {
        super(message);
    }
}
