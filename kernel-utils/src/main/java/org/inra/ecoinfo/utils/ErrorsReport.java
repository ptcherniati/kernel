package org.inra.ecoinfo.utils;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import org.slf4j.LoggerFactory;

/**
 * The Class ErrorsReport.
 */
public class ErrorsReport {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    public static final String BUNDLE_NAME = "org.inra.ecoinfo.utils.messages";
    /**
     * The Constant PROPERTY_MSG_ERROR @link(String).
     */
    public static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";
    /**
     * The Constant PATTERN_DIV_COLOR_RED @link(String).
     */
    private static final String PATTERN_DIV_COLOR_RED = "<p style='color: green'>%s :</p>";
    /**
     * The Constant PATTERN_DIV_INDENT @link(String).
     */
    private static final String PATTERN_DIV_INDENT = "<p style='text-indent: 30px'>- %s </p>";
    /**
     * The Constant PATTERN_TAB_STRING_RETURN @link(String).
     */
    private static final String PATTERN_TAB_STRING_RETURN = "\t - %s %n";
    /**
     * The errors @link(Map<String,List<Exception>>).
     */
    private final Map<String, List<Exception>> errors = new HashMap<>();
    /**
     * The errors messages @link(List<String>).
     */
    private List<String> errorsMessages = new LinkedList<>();
    /**
     * The MS g_ erro r_ forma t_15 @link(String).
     */
    private final String msgErrorFormat15;

    /**
     * Instantiates a new errors report.
     *
     * @param propertyMsgError the property msg error
     * @link(String) the property msg error
     */
    public ErrorsReport(final String propertyMsgError) {
        super();
        msgErrorFormat15 = String.format("%s %%15s", propertyMsgError);
    }

    /**
     * Adds the exception.
     *
     * @param badFormatException the bad format exception
     * @link(Exception) the bad format exception
     */
    public void addException(final Exception badFormatException) {
        UncatchedExceptionLogger.logUncatchedException(badFormatException.getLocalizedMessage(), badFormatException);
        List<Exception> badsFormatsExceptions = errors.get(badFormatException.getClass().getSimpleName());
        if (badsFormatsExceptions == null) {
            badsFormatsExceptions = new LinkedList<>();
            errors.put(badFormatException.getClass().getSimpleName(), badsFormatsExceptions);
        }
        badsFormatsExceptions.add(badFormatException);
        errorsMessages.add(badFormatException.getMessage());
    }

    /**
     * Builds the html messages.
     *
     * @return the string
     */
    public String buildHTMLMessages() {
        final StringBuilder builder = new StringBuilder();
        final Set<String> keys = errors.keySet();
        String key = null;
        for (final Iterator<String> it = keys.iterator(); it.hasNext();) {
            key = it.next();
            builder.append(String.format(String.format(ErrorsReport.PATTERN_DIV_COLOR_RED, msgErrorFormat15), key));
            for (final Exception badFormatException : errors.get(key)) {
                builder.append(String.format(ErrorsReport.PATTERN_DIV_INDENT, badFormatException.getMessage()));
            }
        }
        return builder.toString();
    }

    /**
     * Gets the errors.
     *
     * @return the errors
     */
    public Map<String, List<Exception>> getErrors() {
        return errors;
    }

    /**
     * Gets the errors messages.
     *
     * @return the errors messages
     */
    public List<String> getErrorsMessages() {
        return errorsMessages;
    }

    /**
     * Sets the errors messages.
     *
     * @param errorsMessages the new errors messages
     */
    public void setErrorsMessages(final List<String> errorsMessages) {
        this.errorsMessages = errorsMessages;
    }

    /**
     * Gets the messages.
     *
     * @return the messages
     */
    public String getMessages() {
        final StringBuilder builder = new StringBuilder();
        final Set<String> keys = errors.keySet();
        String key = null;
        for (final Iterator<String> it = keys.iterator(); it.hasNext();) {
            key = it.next();
            builder.append(String.format(msgErrorFormat15, key));
            for (final Exception badFormatException : errors.get(key)) {
                builder.append(String.format(ErrorsReport.PATTERN_TAB_STRING_RETURN, badFormatException.getMessage()));
            }
        }
        return builder.toString();
    }

    /**
     * Checks for errors.
     *
     * @return true, if successful
     */
    public boolean hasErrors() {
        return !errors.keySet().isEmpty();
    }

    /**
     * Write.
     *
     * @param outputStream the output stream
     * @link(OutputStream) the output stream
     */
    public void write(final OutputStream outputStream) {
        PrintStream ps = null;
        try{
            ps = new PrintStream(outputStream, true, StandardCharsets.UTF_8.name());
            final Set<String> keys = errors.keySet();
            String key = null;
            for (final Iterator<String> it = keys.iterator(); it.hasNext();) {
                key = it.next();
                ps.println(String.format(msgErrorFormat15, key));
                for (final Exception badFormatException : errors.get(key)) {
                    ps.println(String.format(ErrorsReport.PATTERN_TAB_STRING_RETURN, badFormatException.getMessage()));
                }
            }
        } catch (UnsupportedEncodingException ex) {
            LoggerFactory.getLogger(getClass()).error(ex.getMessage(), ex);
        } finally {
            StreamUtils.closeStream(ps);
        }
    }
}
