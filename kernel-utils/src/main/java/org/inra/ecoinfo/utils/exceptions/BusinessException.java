package org.inra.ecoinfo.utils.exceptions;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class BusinessException.
 */
public class BusinessException extends Exception {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The context map @link(Map<String,Object>).
     */
    protected final Map<String, Object> contextMap = new HashMap<>();

    /**
     * Instantiates a new business exception.
     */
    public BusinessException() {
        super();
    }

    /**
     * Instantiates a new business exception.
     *
     * @param string
     * @param arg0 the arg0
     * @link(String) the arg0
     */
    public BusinessException(final String arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new business exception.
     *
     * @param arg0 the arg0
     * @param thrwbl
     * @param arg1 the arg1
     * @link(String) the arg0
     * @link(Throwable) the arg1
     */
    public BusinessException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new business exception.
     *
     * @param thrwbl
     * @param arg0 the arg0
     * @link(Throwable) the arg0
     */
    public BusinessException(final Throwable arg0) {
        super(arg0);
    }

    /**
     * Gets the context map.
     *
     * @return the context map
     */
    public Map<String, Object> getContextMap() {
        return contextMap;
    }
}
