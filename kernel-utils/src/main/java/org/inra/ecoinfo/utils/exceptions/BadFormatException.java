/**
 * OREILacs project - see LICENCE.txt for use created: 25 févr. 2009 14:21:16
 */
package org.inra.ecoinfo.utils.exceptions;

/**
 * The Class BadFormatException.
 *
 * @author "Antoine Schellenberger"
 */
public class BadFormatException extends Exception {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The bads formats report @link(BadsFormatsReport).
     */
    private BadsFormatsReport badsFormatsReport;

    /**
     * Instantiates a new bad format exception.
     *
     * @param bfr
     * @param badsFormatsReport the bads formats report
     * @link(BadsFormatsReport) the bads formats report
     */
    public BadFormatException(final BadsFormatsReport badsFormatsReport) {
        this.badsFormatsReport = badsFormatsReport;
    }

    /**
     * Gets the bads formats report.
     *
     * @return the bads formats report
     */
    public BadsFormatsReport getBadsFormatsReport() {
        return badsFormatsReport;
    }

    /**
     * Sets the bads formats report.
     *
     * @param badsFormatsReport the new bads formats report
     */
    public void setBadsFormatsReport(final BadsFormatsReport badsFormatsReport) {
        this.badsFormatsReport = badsFormatsReport;
    }

    /**
     * Gets the message.
     *
     * @return the message
     * @see java.lang.Throwable#getMessage()
     */
    @Override
    public String getMessage() {
        return getBadsFormatsReport().getMessages();
    }
}
