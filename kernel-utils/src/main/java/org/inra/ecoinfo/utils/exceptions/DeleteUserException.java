/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.utils.exceptions;

/**
 *
 * @author arandolph
 */
public class DeleteUserException extends Exception{
     /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Instantiates a new business exception.
     */
    public DeleteUserException() {
        super();
    }

    /**
     * Instantiates a new business exception.
     *
     * @param string
     * @param arg0 the arg0
     * @link(String) the arg0
     */
    public DeleteUserException(final String arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new business exception.
     *
     * @param arg0 the arg0
     * @param thrwbl
     * @param arg1 the arg1
     * @link(String) the arg0
     * @link(Throwable) the arg1
     */
    public DeleteUserException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new business exception.
     *
     * @param thrwbl
     * @param arg0 the arg0
     * @link(Throwable) the arg0
     */
    public DeleteUserException(final Throwable arg0) {
        super(arg0);
    }

}

