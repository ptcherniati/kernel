package org.inra.ecoinfo.utils;

import java.util.Locale;
import javax.faces.bean.ManagedBean;

/**
 * The Class LanguageDetails.
 */
@ManagedBean(name = "languageDetails")
public class LanguageDetails {

    /**
     * The locale @link(String).
     */
    private String locale;

    /**
     * Change language.
     *
     * @return the string
     */
    public String changeLanguage() {
        Locale.setDefault("fr".equals(locale) ? Locale.FRENCH : Locale.ENGLISH);
        return "";
    }

    /**
     * Gets the locale.
     *
     * @return the locale
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Sets the locale.
     *
     * @param locale the new locale
     */
    public void setLocale(final String locale) {
        this.locale = locale;
    }
}
