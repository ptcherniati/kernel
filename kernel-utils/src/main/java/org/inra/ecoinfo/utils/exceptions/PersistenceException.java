package org.inra.ecoinfo.utils.exceptions;

/**
 * The Class PersistenceException.
 */
public class PersistenceException extends Exception {
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Gets the String last cause exception message.
     *
     * @param exception the Exception exception
     * @return the String last cause exception message
     */
    public static String getLastCauseExceptionMessage(final Exception exception) {
        Throwable causeException = null;
        if (exception.getCause() == null) {
            return exception.getMessage();
        } else {
            causeException = retrieveCauseException(exception);
        }
        return causeException.getMessage();
    }

    /**
     * Retrieve cause exception.
     *
     * @param exception the Throwable exception
     * @return the Throwable throwable
     */
    public static Throwable retrieveCauseException(final Throwable exception) {
        if (exception.getCause() == null) {
            return exception;
        } else {
            return retrieveCauseException(exception.getCause());
        }
    }

    /**
     * The line number @link(Integer).
     */
    private Integer lineNumber;

    /**
     * Instantiates a new persistence exception.
     */
    public PersistenceException() {
    }

    /**
     * Instantiates a new persistence exception.
     *
     * @param string
     * @param message the message
     * @link(String) the message
     */
    public PersistenceException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new persistence exception.
     *
     * @param arg0 the arg0
     * @param thrwbl
     * @param arg1 the arg1
     * @link(String) the arg0
     * @link(Throwable) the arg1
     */
    public PersistenceException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new persistence exception.
     *
     * @param thrwbl
     * @param exception the exception
     * @link(Throwable) the exception
     */
    public PersistenceException(final Throwable exception) {
        super(exception);
    }

    /**
     * Gets the line number.
     *
     * @return the line number
     */
    public Integer getLineNumber() {
        return lineNumber;
    }

    /**
     *
     * @param lineNumber
     */
    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }
}
