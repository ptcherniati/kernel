/**
 * OREILacs project - see LICENCE.txt for use created: 23 févr. 2009 16:16:22
 */
package org.inra.ecoinfo.utils;

import com.google.common.base.Strings;

/**
 * The Class Column.
 */
public class Column {

    /**
     * The boolean date for interval.
     */
    private boolean dateForInterval = false;
    /**
     * The String field name.
     */
    private String fieldName;
    /**
     * The boolean flag.
     */
    private boolean flag = false;
    /**
     * The String flag type.
     */
    private String flagType;
    /**
     * The String format type.
     */
    private String formatType;
    /**
     * The boolean inull.
     */
    private boolean inull = true;
    /**
     * The boolean localizable.
     */
    private boolean localizable = false;
    /**
     * The boolean key.
     */
    private boolean key = false;

    /**
     * The String name.
     */
    private String name;
    /**
     * The String ref variable name.
     */
    private String refVariableName;
    /**
     * The String value type.
     */
    private String valueType;
    /**
     * The boolean variable.
     */
    private boolean variable = false;

    /**
     * Instantiates a new Column column.
     */
    public Column() {
        super();
    }

    /**
     * Instantiates a new Column column.
     *
     * @param column the Column column
     */
    public Column(final Column column) {
        super();
        this.name = column.name;
        this.valueType = column.valueType;
        this.formatType = column.formatType;
        this.inull = column.inull;
        this.variable = column.variable;
        this.flag = column.flag;
        this.flagType = column.flagType;
        this.refVariableName = column.refVariableName;
        this.localizable = column.localizable;
        this.fieldName = column.fieldName;
        this.key = column.key;
    }

    /**
     * Instantiates a new Column column.
     *
     * @param column the Column column
     * @param string
     * @param localization the String localization
     */
    public Column(final Column column, final String localization) {
        super();
        this.name = Strings.isNullOrEmpty(localization) ? column.name : String.format("%s_%s",
                column.name, localization);
        this.valueType = column.valueType;
        this.formatType = column.formatType;
        this.inull = column.inull;
        this.dateForInterval = column.dateForInterval;
        this.variable = column.variable;
        this.flag = column.flag;
        this.flagType = column.flagType;
        this.refVariableName = column.refVariableName;
        this.localizable = column.localizable;
        this.fieldName = column.fieldName;
        this.key = column.key;
    }

    /**
     *
     * @return
     */
    public boolean isKey() {
        return key;
    }

    /**
     *
     * @param key
     */
    public void setKey(String key) {
        this.key = Boolean.valueOf(key);
    }

    /**
     * Gets the String field name.
     *
     * @return the String field name
     */
    public String getFieldName() {
        return fieldName == null ? name : fieldName;
    }

    /**
     * Sets the void field name.
     *
     * @param fieldName the String field name
     */
    public void setFieldName(final String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * Gets the String flag type.
     *
     * @return the String flag type
     */
    public String getFlagType() {
        return flagType;
    }

    /**
     * Sets the void flag type.
     *
     * @param flagType the String flag type
     */
    public void setFlagType(final String flagType) {
        this.flagType = flagType;
    }

    /**
     * Gets the String format type.
     *
     * @return the String format type
     */
    public String getFormatType() {
        return formatType;
    }

    /**
     * Sets the void format type.
     *
     * @param formatType the String format type
     */
    public void setFormatType(final String formatType) {
        this.formatType = formatType;
    }

    /**
     * Gets the String name.
     *
     * @return the String name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the void name.
     *
     * @param name the String name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the String ref variable name.
     *
     * @return the String ref variable name
     */
    public String getRefVariableName() {
        return refVariableName;
    }

    /**
     * Sets the void ref variable name.
     *
     * @param refVariableName the String ref variable name
     */
    public void setRefVariableName(final String refVariableName) {
        this.refVariableName = refVariableName;
    }

    /**
     * Gets the String value type.
     *
     * @return the String value type
     */
    public String getValueType() {
        return valueType;
    }

    /**
     * Sets the void value type.
     *
     * @param valueType the String value type
     */
    public void setValueType(final String valueType) {
        this.valueType = valueType;
    }

    /**
     * Checks if is date for interval.
     *
     * @return true, if is date for interval
     */
    public boolean isDateForInterval() {
        return dateForInterval;
    }

    /**
     * Sets the void date for interval.
     *
     * @param isDateForInterval the boolean is date for interval
     */
    public void setDateForInterval(final boolean isDateForInterval) {
        this.dateForInterval = isDateForInterval;
    }

    /**
     * Checks if is flag.
     *
     * @return true, if is flag
     */
    public boolean isFlag() {
        return flag;
    }

    /**
     * Sets the void flag.
     *
     * @param flag the boolean flag
     */
    public void setFlag(final boolean flag) {
        this.flag = flag;
    }

    /**
     * Checks if is inull.
     *
     * @return true, if is inull
     */
    public boolean isInull() {
        return inull;
    }

    /**
     * Sets the void inull.
     *
     * @param inull the boolean inull
     */
    public void setInull(final boolean inull) {
        this.inull = inull;
    }

    /**
     * Checks if is localizable.
     *
     * @return true, if is localizable
     */
    public boolean isLocalizable() {
        return localizable;
    }

    /**
     * Sets the void localizable.
     *
     * @param localizable the boolean localizable
     */
    public void setLocalizable(final boolean localizable) {
        this.localizable = localizable;
    }

    /**
     * Checks if is nullable.
     *
     * @return true, if is nullable
     */
    public boolean isNullable() {
        return inull;
    }

    /**
     * Checks if is variable.
     *
     * @return true, if is variable
     */
    public boolean isVariable() {
        return variable;
    }

    /**
     * Sets the void variable.
     *
     * @param variable the boolean variable
     */
    public void setVariable(final boolean variable) {
        this.variable = variable;
    }

    /**
     * Sets the void date for interval.
     *
     * @param isDateForInterval the String is date for interval
     */
    public void setDateForInterval(final String isDateForInterval) {
        this.dateForInterval = Boolean.parseBoolean(isDateForInterval);
    }

    /**
     * Sets the void flag.
     *
     * @param flag the String flag
     */
    public void setFlag(final String flag) {
        this.flag = Boolean.parseBoolean(flag);
    }

    /**
     * Sets the void localizable.
     *
     * @param localizable the String localizable
     */
    public void setLocalizable(final String localizable) {
        this.localizable = Boolean.parseBoolean(localizable);
    }

    /**
     * Sets the void nullable.
     *
     * @param isNull the String is null
     */
    public void setNullable(final String isNull) {
        this.inull = !Boolean.parseBoolean(isNull);
    }

    /**
     * Sets the void variable.
     *
     * @param isVariable the String is variable
     */
    public void setVariable(final String isVariable) {
        this.variable = Boolean.parseBoolean(isVariable);
    }
}
