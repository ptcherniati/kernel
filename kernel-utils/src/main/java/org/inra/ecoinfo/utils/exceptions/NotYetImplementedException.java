package org.inra.ecoinfo.utils.exceptions;

/**
 * The Class NotYetImplementedException.
 */
public class NotYetImplementedException extends RuntimeException {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new not yet implemented exception.
     */
    public NotYetImplementedException() {
        super();
    }
}
