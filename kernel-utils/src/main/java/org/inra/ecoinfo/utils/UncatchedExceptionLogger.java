package org.inra.ecoinfo.utils;

import javax.persistence.Transient;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UncatchedExceptionLogger.
 */
public class UncatchedExceptionLogger {

    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(UncatchedExceptionLogger.class);

    /**
     *
     * @param message
     * @param e
     */
    public static void log(final String message, final Exception e) {
        logUncatchedException(UncatchedExceptionLogger.LOGGER, message, e);
    }

    /**
     *
     * @param message
     * @param e
     */
    public static void info(final String message, final Exception e) {
        infoUncatchedException(UncatchedExceptionLogger.LOGGER, message, e);
    }

    /**
     * Log uncatched exception.
     *      * This static method may be used for trace uncatched Exceptions
     *
     * @param externalLogger the external LOGGER
     * @param message the message
     * @param e the e
     * @link(Logger) the external LOGGER
     * @link(String) the message
     * @link(Exception) the e {@link Exception}
     *      * typically when you may a catch(Exception e) without specifing the nature
     * of the exception.
     *      * the result is the message of the e {@link Exception} folowed by the {@link StackTraceElement} of th e {@link Exception}
     * @link(Logger) the external LOGGER
     *      * if you have no external LOGGER use
     * org.inra.ecoinfo.utils.UncatchedExceptionLogger
     * #logUncatchedException(java.lang.string, java.lang.Exception)
     * @link(String) the generic message
     *      * example : "uncatched publish version exception"
     * @link(Exception) the e
     */
    public static void logUncatchedException(final Logger externalLogger, final String message, final Exception e) {
        final String stacktrace = ExceptionUtils.getStackTrace(e);
        externalLogger.debug(String.format("stacktrace for uncatched message %s", message), new Throwable(stacktrace, e));
    }

    /**
     *
     * @param externalLogger
     * @param message
     * @param e
     */
    public static void infoUncatchedException(final Logger externalLogger, final String message, final Exception e) {
        final String stacktrace = ExceptionUtils.getMessage(e);
        externalLogger.info(String.format("stacktrace for uncatched message %s", message), new Throwable(stacktrace, e));
    }

    /**
     * Log uncatched exception.
     *      * This static method may be used for trace uncatched Exceptions
     *
     * @param message the message
     * @param e the e
     * @link(String) the message
     * @link(Exception) the e {@link Exception}
     *      * typically when you may a catch(Exception e) without specifing the nature
     * of the exception.
     *      * the result is the message of the e {@link Exception} folowed by the {@link StackTraceElement} of th e {@link Exception}
     * @link(String) the generic message
     *      * example : "uncatched publish version exception"
     * @link(Exception) the e
     */
    public static void logUncatchedException(final String message, final Exception e) {
        logUncatchedException(UncatchedExceptionLogger.LOGGER, message, e);
    }

    private UncatchedExceptionLogger() {
        super();
    }
}
