/**
 * OREILacs project - see LICENCE.txt for use created: 25 févr. 2009 14:22:46
 */
package org.inra.ecoinfo.utils.exceptions;

/**
 * The Class BadValueTypeException.
 *
 * @author "Antoine Schellenberger"
 */
public class BadValueTypeException extends Exception {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new bad value type exception.
     */
    public BadValueTypeException() {
        super();
    }

    /**
     * Instantiates a new bad value type exception.
     *
     * @param message the message
     * @link(String) the message
     */
    public BadValueTypeException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new bad value type exception.
     *
     * @param message the message
     * @param cause the cause
     * @link(String) the message
     * @link(Throwable) the cause
     */
    public BadValueTypeException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new bad value type exception.
     *
     * @param cause the cause
     * @link(Throwable) the cause
     */
    public BadValueTypeException(final Throwable cause) {
        super(cause);
    }
}
