package org.inra.ecoinfo.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The Class ApplicationContextHolder.
 */
public class ApplicationContextHolder implements ApplicationContextAware {

    /**
     * Contexte Spring qui sera injecte par Spring directement.
     */
    private static ApplicationContext context = null;

    /**
     * Methode statique pour récupérer le contexte.
     *
     * @return the contexte Spring qui sera injecte par Spring directement
     */
    public static ApplicationContext getContext() {
        return ApplicationContextHolder.context;
    }

    /**
     * Sets the context.
     *
     * @param ctx the new context
     */
    private static void setContext(final ApplicationContext ctx) {
        ApplicationContextHolder.context = ctx;
    }

    /**
     * Méthode de ApplicationContextAware, qui sera appellée automatiquement par
     * le conteneur.
     *
     * @param ctx the new application context
     */
    @Override
    public void setApplicationContext(final ApplicationContext ctx) {
        ApplicationContextHolder.setContext(ctx);
    }
}
