/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.utils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.hql.internal.ast.ASTQueryTranslatorFactory;
import org.hibernate.hql.spi.QueryTranslator;
import org.hibernate.hql.spi.QueryTranslatorFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**hqlQueryText
 *
 * @author tcherniatinsky
 */
public class HqlToSqlConvertor {

    private static HqlToSqlConvertor instance;

    /**
     *
     * @param hqlQueryText
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public static String toSql(String hqlQueryText) {
        return instance._toSql(hqlQueryText);
    }

    /**
     * The EntityManager entity manager.
     */
    @PersistenceContext(unitName = "JPAPu")
    protected EntityManager entityManager;

    /**
     *
     */
    public HqlToSqlConvertor() {
        super();
        instance = this;
    }

    /**
     *
     * @param entityManager
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    private String _toSql(String hqlQueryText) {
        if (hqlQueryText != null && hqlQueryText.trim().length() > 0) {
            final QueryTranslatorFactory translatorFactory = new ASTQueryTranslatorFactory();
            SessionImplementor hibernateSession = (SessionImplementor) instance.entityManager.getDelegate();
            QueryTranslator translator = translatorFactory.createQueryTranslator("", hqlQueryText, java.util.Collections.EMPTY_MAP, hibernateSession.getFactory(), null);
            translator.compile(java.util.Collections.EMPTY_MAP, false);
            return translator.getSQLString();
        }
        return null;
    }


}
