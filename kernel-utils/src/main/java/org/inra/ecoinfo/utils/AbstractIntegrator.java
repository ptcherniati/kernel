/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.utils;

import java.io.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tcherniatinsky
 * @param <T>
 */
public abstract class AbstractIntegrator<T> {

    /**
     *
     */
    public static final int KO = 1_024;
    /**
     * Embed in zip.
     *
     *
     *
     * @param zipOutputStream the zip output stream
     * @param filesMap the files map
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void embedInZip(final ZipOutputStream zipOutputStream,
            final Map<String, File> filesMap) throws IOException {
        final byte[] buf = new byte[KO];
        for (final Map.Entry<String, File> fileNameEntry : filesMap.entrySet()) {
            String pathInZip = fileNameEntry.getKey();
            final File file = fileNameEntry.getValue();
            String pathExtension = matches(pathInZip);
            if(pathExtension==null){
                String fileExtension = matches(file.getName());
                pathInZip=String.format("%s%s", pathInZip, fileExtension);
            }
            try (InputStream in = new BufferedInputStream(new FileInputStream(file))) {
                zipOutputStream.putNextEntry(new ZipEntry(pathInZip));
                int len;
                while ((len = in.read(buf, 0, KO)) > 0) {
                    zipOutputStream.write(buf, 0, len);
                }
                file.delete();
                zipOutputStream.closeEntry();
            }
        }
    }

    private static String matches(String pathInZip) {
        Pattern p = Pattern.compile("(.*)(\\.[^\\.]*)");
        Matcher matcher = p.matcher(pathInZip);
        if(matcher.matches()){
            //System.out.println(matcher.group(0)+":"+matcher.group(1)+":"+matcher.group(2));
            return matcher.group(2);
        }else{
            //System.out.println(false);
            return null;
        }
    }

    Logger LOGGER = LoggerFactory.getLogger(AbstractIntegrator.class);

    /**
     *
     * @param zipOutputStream
     * @param wrappedByteArrays
     */
    protected void writeFiles(final ZipOutputStream zipOutputStream, List<WrappedByteArray> wrappedByteArrays) {
        try {
            for (Iterator<WrappedByteArray> iterator = wrappedByteArrays.iterator(); iterator.hasNext();) {
                WrappedByteArray wrappedByteArray = iterator.next();
                try {
                    zipOutputStream.putNextEntry(new ZipEntry(wrappedByteArray.path));
                    zipOutputStream.write(wrappedByteArray.getData(), 0, wrappedByteArray.getData().length);
                    zipOutputStream.closeEntry();
                } catch (IOException ex) {
                    LOGGER.error("can't write file ", ex);
                }
            }
            zipOutputStream.flush();
        } catch (IOException ex) {
            LOGGER.error("can't write file ", ex);
        }
    }

    /**
     *
     * @return
     */
    public abstract String getMapEntryKey();

    /**
     *
     * @param zipOutputStream
     * @param collection
     * @throws IOException
     */
    public abstract void embed(ZipOutputStream zipOutputStream, Collection<T> collection) throws IOException;

    /**
     *
     */
    protected class WrappedByteArray {

        byte[] data;
        String path;

        /**
         *
         * @param data
         * @param path
         */
        public WrappedByteArray(byte[] data, String path) {
            this.data = data;
            this.path = path;
        }

        /**
         *
         * @param wrappedByteArray
         */
        public WrappedByteArray(WrappedByteArray wrappedByteArray) {
            this.data = wrappedByteArray.data;
            this.path = wrappedByteArray.path;
        }

        /**
         *
         * @return
         */
        public byte[] getData() {
            return data;
        }

        /**
         *
         * @param data
         */
        public void setData(byte[] data) {
            this.data = data;
        }

        /**
         *
         * @return
         */
        public String getPath() {
            return path;
        }

        /**
         *
         * @param path
         */
        public void setPath(String path) {
            this.path = path;
        }
    }
}
