package org.inra.ecoinfo.utils;

import java.io.Closeable;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class StreamCloser.
 */
public class StreamUtils {

    /**
     * The bundle name.
     */
    private static final String BUNDLE_NAME = "org.inra.ecoinfo.utils.messages";
    /**
     * The resource bundle.
     */
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle(StreamUtils.BUNDLE_NAME, Locale.getDefault());
    /**
     * The Constant PROPERTY_MSG_FAIL_STREAM_CLOSE.
     */
    private static final String PROPERTY_MSG_FAIL_STREAM_CLOSE = "PROPERTY_MSG_FAIL_STREAM_CLOSE";
    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(StreamUtils.class.getName());

    /**
     * Close stream.
     *
     * @param stream the stream
     */
    public static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (final IOException e1) {
                UncatchedExceptionLogger.log("ioexception", e1);
                final String message = StreamUtils.resourceBundle
                        .getString(StreamUtils.PROPERTY_MSG_FAIL_STREAM_CLOSE);
                StreamUtils.LOGGER.error(message, message);
            }
        }
    }

    /**
     *
     * @param <T>
     * @param lhs
     * @param rhs
     * @return
     */
    public static <T> Stream<T> concat(Stream<? extends T> lhs, Stream<? extends T> rhs) {
        return Stream.concat(lhs, rhs);
    }

    /**
     *
     * @param <T>
     * @param lhs
     * @param rhs
     * @return
     */
    public static <T> Stream<T> concat(Stream<? extends T> lhs, T rhs) {
        return Stream.concat(lhs, Stream.of(rhs));
    }
    
    /**
     *
     * @param <T>
     * @param object
     * @return
     */
    public static <T> T print(T object){
        return object;
    }

    private StreamUtils() {
        super();
    }
}
