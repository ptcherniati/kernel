package org.inra.ecoinfo.utils;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Messages.
 */
public final class Messages {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    private static final String BUNDLE_NAME = "org.inra.ecoinfo.utils.messages";
    /**
     * The Constant RESOURCE_BUNDLE @link(ResourceBundle).
     */
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
            .getBundle(Messages.BUNDLE_NAME);

    /**
     * Gets the string.
     *
     * @param key the key
     * @return the string
     * @link(String) the key
     */
    public static String getString(final String key) {
        try {
            return Messages.RESOURCE_BUNDLE.getString(key);
        } catch (final MissingResourceException e) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).info("no key", e);
            return '!' + key + '!';
        }
    }

    /**
     * Instantiates a new messages.
     */
    private Messages() {
    }
}
