/*
 * Copyright 2002-2011 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */
package org.inra.ecoinfo.utils;

import java.lang.reflect.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * Simple utility class for working with the reflection API and handling
 * reflection exceptions.
 *
 *  * Only intended for internal use.
 *
 * @author Juergen Hoeller
 * @author Rob Harrop
 * @author Rod Johnson
 * @author Costin Leau
 * @author Sam Brannen
 * @author Chris Beams
 * @since 1.2.2
 */
public abstract class ReflectionUtils {


    private static final String UNEXPECTED_REFLECTION_EXCEPTION = "Unexpected reflection exception - ";
    private static final String SHOULD_NEVER_GET_HERE = "Should never get here";
    private static final String METHOD_NAME_MUST_NOT_BE_NULL = "Method name must not be null";
    private static final String CLASS_MUST_NOT_BE_NULL = "Class must not be null";
    private static final Pattern CGLIB_RENAMED_METHOD_PATTERN = Pattern.compile("CGLIB\\$(.+)\\$\\d+");
    /**
     * Pre-built FieldFilter that matches all non-static, non-final fields.
     */
    private static FieldFilter COPYABLE_FIELDS = (Field field) -> !(Modifier.isStatic(field.getModifiers()) || Modifier.isFinal(field.getModifiers()));
    /**
     * Pre-built MethodFilter that matches all non-bridge methods.
     */
    private static MethodFilter NON_BRIDGED_METHODS = (Method method) -> !method.isBridge();
    /**
     * Pre-built MethodFilter that matches all non-bridge methods which are not
     * declared on <code>java.lang.Object</code>.
     */
    private static MethodFilter USER_DECLARED_METHODS = (Method method) -> !method.isBridge() && method.getDeclaringClass() != Object.class;

    /**
     * Determine whether the given method explicitly declares the given
     * exception or one of its superclasses, which means that an exception of
     * that type can be propagated as-is within a reflective invocation.
     *
     * @param method the declaring method
     * @param exceptionType the exception to throw
     * @return <code>true</code> if the exception can be thrown as-is;
     * <code>false</code> if it needs to be wrapped
     */
    public static boolean declaresException(Method method, Class<?> exceptionType) {
        assert method != null : "Method must not be null";
        final Class<?>[] declaredExceptions = method.getExceptionTypes();
        for (final Class<?> declaredException : declaredExceptions) {
            if (declaredException.isAssignableFrom(exceptionType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Invoke the given callback on all fields in the target class, going up the
     * class hierarchy to get all declared fields.
     *
     * @param clazz the target class to analyze
     * @param fc the callback to invoke for each field
     */
    public static void doWithFields(Class<?> clazz, FieldCallback fc) {
        ReflectionUtils.doWithFields(clazz, fc, null);
    }

    /**
     * Invoke the given callback on all fields in the target class, going up the
     * class hierarchy to get all declared fields.
     *
     * @param clazz the target class to analyze
     * @param fc the callback to invoke for each field
     * @param ff the filter that determines the fields to apply the callback to
     */
    public static void doWithFields(Class<?> clazz, FieldCallback fc, FieldFilter ff) {
        Class<?> targetClass = clazz;
        do {
            final Field[] fields = targetClass.getDeclaredFields();
            for (final Field field : fields) {
                if (ff != null && !ff.matches(field)) {
                    continue;
                }
                try {
                    fc.doWith(field);
                } catch (final IllegalAccessException ex) {
                    throw new IllegalStateException("Shouldn't be illegal to access field '" + field.getName() + "': " + ex, ex);
                }
            }
            targetClass = targetClass.getSuperclass();
        } while (targetClass != null && targetClass != Object.class);
    }

    /**
     * Perform the given callback operation on all matching methods of the given
     * class and superclasses.
     *      * The same named method occurring on subclass and superclass will appear
     * twice, unless excluded by a {@link MethodFilter}.
     *
     * @param clazz class to start looking at
     * @param mc the callback to invoke for each method
     * @see #doWithMethods(Class, MethodCallback, MethodFilter)
     */
    public static void doWithMethods(Class<?> clazz, MethodCallback mc) {
        ReflectionUtils.doWithMethods(clazz, mc, null);
    }

    /**
     * Perform the given callback operation on all matching methods of the given
     * class and superclasses (or given interface and super-interfaces).
     *      * The same named method occurring on subclass and superclass will appear
     * twice, unless excluded by the specified {@link MethodFilter}.
     *
     * @param clazz class to start looking at
     * @param mc the callback to invoke for each method
     * @param mf the filter that determines the methods to apply the callback to
     */
    public static void doWithMethods(Class<?> clazz, MethodCallback mc, MethodFilter mf) {
        final Method[] methods = clazz.getDeclaredMethods();
        for (final Method method : methods) {
            if (mf != null && !mf.matches(method)) {
                continue;
            }
            try {
                mc.doWith(method);
            } catch (final IllegalAccessException ex) {
                throw new IllegalStateException("Shouldn't be illegal to access method '" + method.getName() + "': " + ex, ex);
            }
        }
        if (clazz.getSuperclass() != null) {
            ReflectionUtils.doWithMethods(clazz.getSuperclass(), mc, mf);
        } else if (clazz.isInterface()) {
            for (final Class<?> superIfc : clazz.getInterfaces()) {
                ReflectionUtils.doWithMethods(superIfc, mc, mf);
            }
        }
    }

    /**
     * Attempt to find a {@link Field field} on the supplied {@link Class} with
     * the supplied <code>name</code>. Searches all superclasses up to
     * {@link Object}.
     *
     * @param clazz the class to introspect
     * @param name the name of the field
     * @return the corresponding Field object, or <code>null</code> if not found
     */
    public static Field findField(Class<?> clazz, String name) {
        return ReflectionUtils.findField(clazz, name, null);
    }

    /**
     * Attempt to find a {@link Field field} on the supplied {@link Class} with
     * the supplied <code>name</code> and/or {@link Class type}. Searches all
     * superclasses up to {@link Object}.
     *
     * @param clazz the class to introspect
     * @param name the name of the field (may be <code>null</code> if type is
     * specified)
     * @param type the type of the field (may be <code>null</code> if name is
     * specified)
     * @return the corresponding Field object, or <code>null</code> if not found
     */
    public static Field findField(Class<?> clazz, String name, Class<?> type) {
        assert clazz != null : ReflectionUtils.CLASS_MUST_NOT_BE_NULL;
        assert name != null || type != null : "Either name or type of the field must be specified";
        Class<?> searchType = clazz;
        while (!Object.class.equals(searchType) && searchType != null) {
            final Field[] fields = searchType.getDeclaredFields();
            for (final Field field : fields) {
                if ((name == null || name.equals(field.getName())) && (type == null || type.equals(field.getType()))) {
                    return field;
                }
            }
            searchType = searchType.getSuperclass();
        }
        return null;
    }

    /**
     * Attempt to find a {@link Method} on the supplied class with the supplied
     * name and no parameters. Searches all superclasses up to
     * <code>Object</code>.
     *      * Returns <code>null</code> if no {@link Method} can be found.
     *
     * @param clazz the class to introspect
     * @param name the name of the method
     * @return the Method object, or <code>null</code> if none found
     */
    public static Method findMethod(Class<?> clazz, String name) {
        return ReflectionUtils.findMethod(clazz, name, new Class[0]);
    }

    /**
     * Attempt to find a {@link Method} on the supplied class with the supplied
     * name and parameter types. Searches all superclasses up to
     * <code>Object</code>.
     *      * Returns <code>null</code> if no {@link Method} can be found.
     *
     * @param clazz the class to introspect
     * @param name the name of the method
     * @param paramTypes the parameter types of the method (may be
     * <code>null</code> to indicate any signature)
     * @return the Method object, or <code>null</code> if none found
     */
    public static Method findMethod(Class<?> clazz, String name, Class<?>... paramTypes) {
        assert clazz != null : ReflectionUtils.CLASS_MUST_NOT_BE_NULL;
        assert name != null : ReflectionUtils.METHOD_NAME_MUST_NOT_BE_NULL;
        Class<?> searchType = clazz;
        while (searchType != null) {
            final Method[] methods = searchType.isInterface() ? searchType.getMethods() : searchType.getDeclaredMethods();
            for (final Method method : methods) {
                if (name.equals(method.getName()) && (paramTypes == null || Arrays.equals(paramTypes, method.getParameterTypes()))) {
                    return method;
                }
            }
            searchType = searchType.getSuperclass();
        }
        return null;
    }

    /**
     * Get all declared methods on the leaf class and all superclasses. Leaf
     * class methods are included first.
     *
     * @param leafClass
     * @return
     */
    public static Method[] getAllDeclaredMethods(Class<?> leafClass) {
        final List<Method> methods = new ArrayList<>(32);
        ReflectionUtils.doWithMethods(leafClass, methods::add);
        return methods.toArray(new Method[methods.size()]);
    }

    /**
     *
     * @return
     */
    public static FieldFilter getCopyableFields() {
        return ReflectionUtils.COPYABLE_FIELDS;
    }

    /**
     *
     * @param copyableFields
     */
    public static void setCopyableFields(FieldFilter copyableFields) {
        ReflectionUtils.COPYABLE_FIELDS = copyableFields;
    }

    /**
     * Get the field represented by the supplied {@link Field field object} on
     * the specified {@link Object target object}. In accordance with
     * {@link Field#get(Object)} semantics, the returned value is automatically
     * wrapped if the underlying field has a primitive type.
     *      * Thrown exceptions are handled via a call to
     * {@link #handleReflectionException(Exception)}.
     *
     * @param field the field to get
     * @param target the target object from which to get the field
     * @return the field's current value
     */
    public static Object getField(Field field, Object target) {
        try {
            return field.get(target);
        } catch (final IllegalAccessException ex) {
            ReflectionUtils.handleReflectionException(ex);
            throw new IllegalStateException(ReflectionUtils.UNEXPECTED_REFLECTION_EXCEPTION + ex.getClass().getName() + ": " + ex.getMessage(), ex);
        }
    }

    /**
     *
     * @return
     */
    public static MethodFilter getNonBrigedMethods() {
        return ReflectionUtils.NON_BRIDGED_METHODS;
    }

    /**
     * Get the unique set of declared methods on the leaf class and all
     * superclasses. Leaf class methods are included first and while traversing
     * the superclass hierarchy any methods found with signatures matching a
     * method already included are filtered out.
     *
     * @param leafClass
     * @return
     */
    public static Method[] getUniqueDeclaredMethods(Class<?> leafClass) {
        final List<Method> methods = new ArrayList<>(32);
        ReflectionUtils.doWithMethods(leafClass, new LocalMethodCallback(methods));
        return methods.toArray(new Method[methods.size()]);
    }

    /**
     *
     * @return
     */
    public static MethodFilter getUserDeclaredMethods() {
        return ReflectionUtils.USER_DECLARED_METHODS;
    }

    /**
     *
     * @param userDeclaredMethods
     */
    public static void setUserDeclaredMethods(MethodFilter userDeclaredMethods) {
        ReflectionUtils.USER_DECLARED_METHODS = userDeclaredMethods;
    }

    /**
     * Handle the given invocation target exception. Should only be called if no
     * checked exception is expected to be thrown by the target method.
     *      * Throws the underlying RuntimeException or Error in case of such a root
     * cause. Throws an IllegalStateException else.
     *
     * @param ex the invocation target exception to handle
     */
    public static void handleInvocationTargetException(InvocationTargetException ex) {
        ReflectionUtils.rethrowRuntimeException(ex.getTargetException());
    }

    /**
     * Handle the given reflection exception. Should only be called if no
     * checked exception is expected to be thrown by the target method.
     *      * Throws the underlying RuntimeException or Error in case of an
     * InvocationTargetException with such a root cause. Throws an
     * IllegalStateException with an appropriate message else.
     *
     * @param ex the reflection exception to handle
     */
    public static void handleReflectionException(Exception ex) {
        if (ex instanceof NoSuchMethodException) {
            throw new IllegalStateException("Method not found: " + ex.getMessage());
        }
        if (ex instanceof IllegalAccessException) {
            throw new IllegalStateException("Could not access method: " + ex.getMessage());
        }
        if (ex instanceof InvocationTargetException) {
            ReflectionUtils.handleInvocationTargetException((InvocationTargetException) ex);
        }
        if (ex instanceof RuntimeException) {
            throw (RuntimeException) ex;
        }
        throw new UndeclaredThrowableException(ex);
    }

    /**
     * Invoke the specified JDBC API {@link Method} against the supplied target
     * object with no arguments.
     *
     * @param method the method to invoke
     * @param target the target object to invoke the method on
     * @return the invocation result, if any
     * @throws SQLException the JDBC API SQLException to rethrow (if any)
     * @see #invokeJdbcMethod(java.lang.reflect.Method, Object, Object[])
     */
    public static Object invokeJdbcMethod(Method method, Object target) throws SQLException {
        return ReflectionUtils.invokeJdbcMethod(method, target, new Object[0]);
    }

    /**
     * Invoke the specified JDBC API {@link Method} against the supplied target
     * object with the supplied arguments.
     *
     * @param method the method to invoke
     * @param target the target object to invoke the method on
     * @param args the invocation arguments (may be <code>null</code>)
     * @return the invocation result, if any
     * @throws SQLException the JDBC API SQLException to rethrow (if any)
     * @see #invokeMethod(java.lang.reflect.Method, Object, Object[])
     */
    public static Object invokeJdbcMethod(Method method, Object target, Object... args) throws SQLException {
        try {
            return method.invoke(target, args);
        } catch (final IllegalAccessException ex) {
            UncatchedExceptionLogger.log("reThrowIllegalAccessException", ex);
            ReflectionUtils.handleReflectionException(ex);
        } catch (final InvocationTargetException ex) {
            if (ex.getTargetException() instanceof SQLException) {
                UncatchedExceptionLogger.log("reThrowSQLException", ex);
                throw (SQLException) ex.getTargetException();
            }
            ReflectionUtils.handleInvocationTargetException(ex);
        }
        throw new IllegalStateException(ReflectionUtils.SHOULD_NEVER_GET_HERE);
    }

    /**
     * Invoke the specified {@link Method} against the supplied target object
     * with no arguments. The target object can be <code>null</code> when
     * invoking a static {@link Method}.
     *      * Thrown exceptions are handled via a call to
     * {@link #handleReflectionException}.
     *
     * @param method the method to invoke
     * @param target the target object to invoke the method on
     * @return the invocation result, if any
     * @see #invokeMethod(java.lang.reflect.Method, Object, Object[])
     */
    public static Object invokeMethod(Method method, Object target) {
        return ReflectionUtils.invokeMethod(method, target, new Object[0]);
    }

    /**
     * Invoke the specified {@link Method} against the supplied target object
     * with the supplied arguments. The target object can be <code>null</code>
     * when invoking a static {@link Method}.
     *      * Thrown exceptions are handled via a call to
     * {@link #handleReflectionException}.
     *
     * @param method the method to invoke
     * @param target the target object to invoke the method on
     * @param args the invocation arguments (may be <code>null</code>)
     * @return the invocation result, if any
     */
    public static Object invokeMethod(Method method, Object target, Object... args) {
        try {
            return method.invoke(target, args);
        } catch (final IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            UncatchedExceptionLogger.log("no methods", ex);
            ReflectionUtils.handleReflectionException(ex);
        }
        throw new IllegalStateException(ReflectionUtils.SHOULD_NEVER_GET_HERE);
    }

    /**
     * Determine whether the given method is a CGLIB 'renamed' method, following
     * the pattern "CGLIB$methodName$0".
     *
     * @param renamedMethod the method to check
     * @return
     * @see net.sf.cglib.proxy.Enhancer#rename
     */
    public static boolean isCglibRenamedMethod(Method renamedMethod) {
        return ReflectionUtils.CGLIB_RENAMED_METHOD_PATTERN.matcher(renamedMethod.getName()).matches();
    }

    /**
     * Determine whether the given method is an "equals" method.
     *
     * @param method
     * @return
     * @see java.lang.Object#equals(Object)
     */
    public static boolean isEqualsMethod(Method method) {
        if (method == null || !"equals".equals(method.getName())) {
            return false;
        }
        final Class<?>[] paramTypes = method.getParameterTypes();
        return paramTypes.length == 1 && paramTypes[0] == Object.class;
    }

    /**
     * Determine whether the given method is a "hashCode" method.
     *
     * @param method
     * @return
     * @see java.lang.Object#hashCode()
     */
    public static boolean isHashCodeMethod(Method method) {
        return method != null && "hashCode".equals(method.getName()) && method.getParameterTypes().length == 0;
    }

    /**
     * Determine whether the given method is originally declared by
     * {@link java.lang.Object}.
     *
     * @param method
     * @return
     */
    public static boolean isObjectMethod(Method method) {
        try {
            Object.class.getDeclaredMethod(method.getName(), method.getParameterTypes());
            return true;
        } catch (final SecurityException | NoSuchMethodException ex) {
            UncatchedExceptionLogger.log("no method", ex);
            return false;
        }
    }

    /**
     * Determine whether the given field is a "public static final" constant.
     *
     * @param field the field to check
     * @return
     */
    public static boolean isPublicStaticFinal(Field field) {
        final int modifiers = field.getModifiers();
        return Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers) && Modifier.isFinal(modifiers);
    }

    /**
     * Determine whether the given method is a "toString" method.
     *
     * @param method
     * @return
     * @see java.lang.Object#toString()
     */
    public static boolean isToStringMethod(Method method) {
        return method != null && "toString".equals(method.getName()) && method.getParameterTypes().length == 0;
    }

    /**
     * Make the given constructor accessible, explicitly setting it accessible
     * if necessary. The <code>setAccessible(true)</code> method is only called
     * when actually necessary, to avoid unnecessary conflicts with a JVM
     * SecurityManager (if active).
     *
     * @param ctor the constructor to make accessible
     * @see java.lang.reflect.Constructor#setAccessible
     */
    public static void makeAccessible(Constructor<?> ctor) {
        if ((!Modifier.isPublic(ctor.getModifiers()) || !Modifier.isPublic(ctor.getDeclaringClass().getModifiers())) && !ctor.isAccessible()) {
            ctor.setAccessible(true);
        }
    }

    /**
     * Make the given field accessible, explicitly setting it accessible if
     * necessary. The <code>setAccessible(true)</code> method is only called
     * when actually necessary, to avoid unnecessary conflicts with a JVM
     * SecurityManager (if active).
     *
     * @param field the field to make accessible
     * @see java.lang.reflect.Field#setAccessible
     */
    public static void makeAccessible(Field field) {
        if ((!Modifier.isPublic(field.getModifiers()) || !Modifier.isPublic(field.getDeclaringClass().getModifiers()) || Modifier.isFinal(field.getModifiers())) && !field.isAccessible()) {
            field.setAccessible(true);
        }
    }

    /**
     * Make the given method accessible, explicitly setting it accessible if
     * necessary. The <code>setAccessible(true)</code> method is only called
     * when actually necessary, to avoid unnecessary conflicts with a JVM
     * SecurityManager (if active).
     *
     * @param method the method to make accessible
     * @see java.lang.reflect.Method#setAccessible
     */
    public static void makeAccessible(Method method) {
        if ((!Modifier.isPublic(method.getModifiers()) || !Modifier.isPublic(method.getDeclaringClass().getModifiers())) && !method.isAccessible()) {
            method.setAccessible(true);
        }
    }

    /**
     * Rethrow the given {@link Throwable exception}, which is presumably the
     * <em>target exception</em> of an {@link InvocationTargetException}. Should
     * only be called if no checked exception is expected to be thrown by the
     * target method.
     *      * Rethrows the underlying exception cast to an {@link Exception} or
     * {@link Error} if appropriate; otherwise, throws an
     * {@link IllegalStateException}.
     *
     * @param ex the exception to rethrow
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public static void rethrowException(Throwable ex) throws BusinessException {
        if (ex instanceof Exception) {
            throw new BusinessException("exception reThrown", ex);
        }
        if (ex instanceof Error) {
            throw (Error) ex;
        }
        throw new UndeclaredThrowableException(ex);
    }

    /**
     * Rethrow the given {@link Throwable exception}, which is presumably the
     * <em>target exception</em> of an {@link InvocationTargetException}. Should
     * only be called if no checked exception is expected to be thrown by the
     * target method.
     *      * Rethrows the underlying exception cast to an {@link RuntimeException} or
     * {@link Error} if appropriate; otherwise, throws an
     * {@link IllegalStateException}.
     *
     * @param ex the exception to rethrow
     * @throws RuntimeException the rethrown exception
     */
    public static void rethrowRuntimeException(Throwable ex) {
        if (ex instanceof RuntimeException) {
            throw (RuntimeException) ex;
        }
        if (ex instanceof Error) {
            throw (Error) ex;
        }
        throw new UndeclaredThrowableException(ex);
    }

    /**
     * Set the field represented by the supplied {@link Field field object} on
     * the specified {@link Object target object} to the specified
     * <code>value</code>. In accordance with {@link Field#set(Object, Object)}
     * semantics, the new value is automatically unwrapped if the underlying
     * field has a primitive type.
     *      * Thrown exceptions are handled via a call to
     * {@link #handleReflectionException(Exception)}.
     *
     * @param field the field to set
     * @param target the target object on which to set the field
     * @param value the value to set; may be <code>null</code>
     */
    public static void setField(Field field, Object target, Object value) {
        try {
            field.set(target, value);
        } catch (final IllegalAccessException ex) {
            ReflectionUtils.handleReflectionException(ex);
            throw new IllegalStateException(ReflectionUtils.UNEXPECTED_REFLECTION_EXCEPTION + ex.getClass().getName() + ": " + ex.getMessage(), ex);
        }
    }

    /**
     *
     * @param nonBridgedMethods
     */
    public static void setNonBridgedMethods(MethodFilter nonBridgedMethods) {
        ReflectionUtils.NON_BRIDGED_METHODS = nonBridgedMethods;
    }

    /**
     * Given the source object and the destination, which must be the same class
     * or a subclass, copy all fields, including inherited fields. Designed to
     * work on objects with public no-arg constructors.
     *
     * @param src
     * @param dest
     * @throws IllegalArgumentException if the arguments are incompatible
     */
    public static void shallowCopyFieldState(final Object src, final Object dest) {
        if (src == null) {
            throw new IllegalArgumentException("Source for field copy cannot be null");
        }
        if (dest == null) {
            throw new IllegalArgumentException("Destination for field copy cannot be null");
        }
        if (!src.getClass().isAssignableFrom(dest.getClass())) {
            throw new IllegalArgumentException("Destination class [" + dest.getClass().getName() + "] must be same or subclass as source class [" + src.getClass().getName() + "]");
        }
        ReflectionUtils.doWithFields(src.getClass(), (Field field) -> {
            ReflectionUtils.makeAccessible(field);
            final Object srcValue = field.get(src);
            field.set(dest, srcValue);
        }, ReflectionUtils.getCopyableFields());
    }

    private ReflectionUtils() {
    }

    private static final class LocalMethodCallback implements MethodCallback {

        private final List<Method> methods;

        private LocalMethodCallback(List<Method> methods) {
            this.methods = methods;
        }

        @Override
        public void doWith(Method method) {
            boolean knownSignature = false;
            Method methodBeingOverriddenWithCovariantReturnType = null;
            for (final Method existingMethod : methods) {
                if (method.getName().equals(existingMethod.getName()) && Arrays.equals(method.getParameterTypes(), existingMethod.getParameterTypes())) {
                    if (existingMethod.getReturnType() != method.getReturnType() && existingMethod.getReturnType().isAssignableFrom(method.getReturnType())) {
                        methodBeingOverriddenWithCovariantReturnType = existingMethod;
                    } else {
                        knownSignature = true;
                    }
                    break;
                }
            }
            if (methodBeingOverriddenWithCovariantReturnType != null) {
                methods.remove(methodBeingOverriddenWithCovariantReturnType);
            }
            if (!knownSignature && !ReflectionUtils.isCglibRenamedMethod(method)) {
                methods.add(method);
            }
        }
    }

    /**
     * Callback interface invoked on each field in the hierarchy.
     */
    public interface FieldCallback {

        /**
         * Perform an operation using the given field.
         *
         * @param field the field to operate on
         * @throws java.lang.IllegalAccessException
         */
        void doWith(Field field) throws IllegalAccessException;
    }

    /**
     * Callback optionally used to filter fields to be operated on by a field
     * callback.
     */
    public interface FieldFilter {

        /**
         * Determine whether the given field matches.
         *
         * @param field the field to check
         * @return
         */
        boolean matches(Field field);
    }

    /**
     * Action to take on each method.
     */
    public interface MethodCallback {

        /**
         * Perform an operation using the given method.
         *
         * @param method the method to operate on
         * @throws java.lang.IllegalAccessException
         */
        void doWith(Method method) throws IllegalAccessException;
    }

    /**
     * Callback optionally used to filter methods to be operated on by a method
     * callback.
     */
    public interface MethodFilter {

        /**
         * Determine whether the given method matches.
         *
         * @param method the method to check
         * @return
         */
        boolean matches(Method method);
    }
}
