package org.inra.ecoinfo.utils;

import java.io.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class FileWithFolderCreator.
 */
public class FileWithFolderCreator {

    /**
     * The Constant PROPERTY_MSG_FILE_NOT_FOUND @link(String).
     */
    private static final String PROPERTY_MSG_FILE_NOT_FOUND = Messages
            .getString("PROPERTY_MSG_FILE_NOT_FOUND");
    /**
     * The Constant PROPERTY_MSG_NO_DELETE_FILE @link(String).
     */
    private static final String PROPERTY_MSG_NO_DELETE_FILE = Messages
            .getString("PROPERTY_MSG_NO_DELETE_FILE");
    /**
     * The Constant PROPERTY_MSG_NO_DELETE_PATH @link(String).
     */
    private static final String PROPERTY_MSG_NO_DELETE_PATH = Messages
            .getString("PROPERTY_MSG_NO_DELETE_PATH");

    /**
     * Creates the file.
     *
     * @param path the path
     * @return the file
     * @link(String) the path
     */
    public static File createFile(final String path) {
        final File returnFile = new File(path);
        if (!returnFile.getParentFile().exists() && returnFile.getParentFile().mkdirs()) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).info(String.format("%s created", returnFile.getParentFile().getName()));
        }
        return returnFile;
    }

    /**
     * Creates the input stream.
     *
     * @param path the path
     * @return the file input stream
     * @throws FileNotFoundException the file not found exception
     * @link(String) the path
     */
    public static FileInputStream createInputStream(final String path) throws FileNotFoundException {
        final File returnFile = new File(path);
        if (!returnFile.getParentFile().exists() && returnFile.getParentFile().mkdirs()) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).info("{0} created", returnFile.getParentFile().getName());
        }
        return new FileInputStream(path);
    }

    /**
     * Creates the output stream.
     *
     * @param path the path
     * @return the file output stream
     * @throws FileNotFoundException the file not found exception
     * @link(String) the path
     */
    public static FileOutputStream createOutputStream(final String path)
            throws FileNotFoundException {
        final File returnFile = new File(path);
        if (!returnFile.getParentFile().exists() && returnFile.getParentFile().mkdirs()) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).info("{0} created", returnFile.getName());
        }
        return new FileOutputStream(path);
    }

    /**
     * Recursif delete.
     *
     * @param path the path
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(File) the path
     */
    public static void recursifDelete(final File path) throws IOException {
        if (!path.exists()) {
            throw new IOException(FileWithFolderCreator.PROPERTY_MSG_FILE_NOT_FOUND
                    + path.getAbsolutePath() + "'");
        }
        if (path.isDirectory()) {
            final File[] children = path.listFiles();
            for (int i = 0; children != null && i < children.length; i++) {
                FileWithFolderCreator.recursifDelete(children[i]);
            }
            if (!path.delete()) {
                throw new IOException(FileWithFolderCreator.PROPERTY_MSG_NO_DELETE_PATH
                        + path.getAbsolutePath() + "'");
            }
        } else if (!path.delete()) {
            throw new IOException(FileWithFolderCreator.PROPERTY_MSG_NO_DELETE_FILE
                    + path.getAbsolutePath() + "'");
        }
    }

    private FileWithFolderCreator() {
        super();
    }
}
