package org.inra.ecoinfo.utils;


import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.util.Locale;
import org.apache.commons.lang.StringUtils;

/**
 * The Class DateUtil.() <this class dispense stativ function for getting {@link
 * SimpleDateFormat} with different patterns  * <p> Be careful in the use of dates with Hibernate. Indeed, when storing a
 * date with Hibern a
 * t
 * e, the date object created is a local date. If we base '14 / 02/2012 0:30 ',
 * the date object returned will be the timestamp '13 / 02/2012 11:30 p.m., with
 * a local one.
 *
 *  * When retrieving the date '14 / 02/2012 0:30 a file, it will have to parse a
 * SimpleDateFormat 'in local. (object timestamp date '13 / 02/2012 11:30 p.m.,
 * with a local one, but stored '14 / 02/2012 0:30 'basis).
 *
 *  * By cons, if you want to display the date retrieved from the database, you
 * must be vigilant during the time change. If it stores the full date, there is
 * no problem Stock if the date in a date field and a time field If the stored
 * date is '30 / 03/2012 'and '02: 00' will be transformed into a '30 / 03/2012
 * 'and '03: 00' and date '30 / 03/2012 'and '03: 00 'will also be transformed
 * into '30 / 03/2012' and '03: 00 '.
 *
 * @author Philippe TCHERNIATINSKY Utilitaire de gestion des dates
 */
public final class DateUtil {

    /**
     *
     */
    public static final LocalDate MIN_LOCAL_DATE = LocalDate.of(1, 1, 1);

    /**
     *
     */
    public static final LocalDateTime MIN_LOCAL_DATE_TIME = MIN_LOCAL_DATE.atStartOfDay();

    /**
     *
     */
    public static final LocalDate MAX_LOCAL_DATE = LocalDate.of(3000, 12, 31);

    /**
     *
     */
    public static final LocalDateTime MAX_LOCAL_DATE_TIME = MIN_LOCAL_DATE.atStartOfDay();

    /**
     *
     */
    public static final ZoneId UTC_ZONEID = ZoneId.ofOffset("", ZoneOffset.UTC);

    /**
     *
     */
    public static final ZoneOffset UTC_OFFSET = ZoneOffset.UTC;

    /**
     * The Constant YYYY_MM_FILE.
     */
    public static final String YYYY_MM_FILE = "yyyy-MM";
    /**
     * The Constant YYYY_MM.
     */
    public static final String YYYY_MM = "yyyy/MM";
    /**
     * The Constant MM_YYYY_FILE.
     */
    public static final String MM_YYYY_FILE = "MM-yyyy";
    /**
     * The Constant MM_YYYY.
     */
    public static final String MM_YYYY = "MM/yyyy";
    /**
     * The Constant HH_MM.
     */
    public static final String HH_MM = "HH:mm";

    /**
     *
     */
    public static final String HH_MM_SS = "HH:mm:ss";
    /**
     * The Constant YYYY_MM_DD_FILE.
     */
    public static final String YYYY_MM_DD_FILE = "yyyy-MM-dd";
    /**
     * The Constant YYYY_MM_DD.
     */
    public static final String YYYY_MM_DD = "yyyy/MM/dd";
    /**
     * The Constant DD_MM_YYYY_FILE.
     */
    public static final String DD_MM_YYYY_FILE = "dd-MM-yyyy";
    /**
     * The Constant YYYY.
     */
    public static final String YYYY = "yyyy";
    /**
     * The Constant DD_MM_YYYY.
     */
    public static final String DD_MM_YYYY = "dd/MM/yyyy";
    /**
     * The Constant DD_MM_YYYY_HH_MM.
     */
    public static final String DD_MM_YYYY_HH_MM = "dd/MM/yyyy HH:mm";
    /**
     * The Constant DD_MM_YYYY_HH_MM.
     */
    public static final String DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy HH:mm:ss";
    /**
     * The Constant GMT.
     */
    public static final String GMT = "GMT";

    /**
     *
     */
    public static final String YYYY_MM_DD_HHMMSS_SSSZ = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    /**
     *
     */
    public static final String YYYY_MM_DD_HHMMSS = "yyyy-MM-dd HH:mm:ss";

    /**
     *
     * @return
     */
    public static final ZoneId getLocaleZoneId() {
        return ZoneId.systemDefault();
    }

    /**
     *
     * @param instant
     * @return
     */
    public static final ZoneId getLocaleOffset(Instant instant) {
        return getLocaleZoneId().getRules().getOffset(instant);
    }

    /**
     *
     * @param formatOfDate
     * @param UTCDate
     * @return
     */
    public static LocalDateTime readLocalDateTimeFromText(String formatOfDate, String UTCDate) throws DateTimeParseException {
        return readLocalDateTimeFromTextWithLocale(formatOfDate, UTCDate, Locale.FRANCE);
    }

    /**
     *
     * @param formatOfDate
     * @param UTCDate
     * @param locale
     * @return
     */
    public static LocalDateTime readLocalDateTimeFromTextWithLocale(String formatOfDate, String UTCDate, Locale locale) throws DateTimeParseException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(formatOfDate, locale);
        LocalDateTime localeDateTime;
        if (isTimePattern(formatOfDate) && isDatePattern(formatOfDate)) {
            localeDateTime = LocalDateTime.parse(UTCDate, dtf);
        } else if (isDatePattern(formatOfDate)) {
            LocalDate localeDate = LocalDate.parse(UTCDate, dtf);
            localeDateTime = localeDate.atStartOfDay();
        } else if (isTimePattern(formatOfDate)) {
            LocalTime localTime = LocalTime.parse(UTCDate, dtf);
            localeDateTime = LocalDateTime.of(LocalDate.ofEpochDay(0), localTime);
        } else {
            localeDateTime = LocalDateTime.parse(UTCDate, dtf);
        }
        return localeDateTime;
    }

    /**
     *
     * @param formatOfDate
     * @param UTCdate
     * @return
     */
    public static LocalDate readLocalDateFromText(String formatOfDate, String UTCdate) throws DateTimeParseException {
        return readLocalDateFromTextWithLocale(formatOfDate, UTCdate, Locale.FRANCE);

    }

    /**
     *
     * @param formatOfDate
     * @param UTCdate
     * @param locale
     * @return
     */
    public static LocalDate readLocalDateFromTextWithLocale(String formatOfDate, String UTCdate, Locale locale) throws DateTimeParseException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(formatOfDate, locale);
        assert isDatePattern(formatOfDate) : formatOfDate + " isn't a date format";
        return LocalDate.parse(UTCdate, dtf);

    }

    /**
     *
     * @param formatOfTime
     * @param UTCTime
     * @return
     */
    public static LocalTime readLocalTimeFromText(String formatOfTime, String UTCTime) throws DateTimeParseException {
        return readLocalTimeFromTextWithLocale(formatOfTime, UTCTime, Locale.FRANCE);
    }

    /**
     *
     * @param formatOfTime
     * @param UTCTime
     * @param locale
     * @return
     */
    public static LocalTime readLocalTimeFromTextWithLocale(String formatOfTime, String UTCTime, Locale locale) throws DateTimeParseException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(formatOfTime, locale);
        assert isTimePattern(formatOfTime) : formatOfTime + " isn't a time format";
        return LocalTime.parse(UTCTime, dtf);
    }

    /**
     *
     * @param formatOfdate
     * @param localDate
     * @param formatOfTime
     * @param localTime
     * @return
     * @throws DateTimeParseException
     */
    public static LocalDateTime readLocalDateTimeFromLocalDateAndLocaltime(String formatOfdate, String localDate, String formatOfTime, String localTime) throws DateTimeParseException {
        LocalDate date = readLocalDateFromText(formatOfdate, localDate);
        LocalTime time = readLocalTimeFromText(formatOfTime, localTime);
        return getLocalDateTimeFromLocalDateAndLocaltime(date, time);
    }

    /**
     *
     * @param formatOfDate
     * @return
     */
    public static boolean isTimePattern(String formatOfDate) {
        return StringUtils.containsAny(formatOfDate, "HmsSAnN");
    }

    /**
     *
     * @param formatOfDate
     * @return
     */
    public static boolean isDatePattern(String formatOfDate) {
        return StringUtils.containsAny(formatOfDate, "uyDMLdQqYwWEecF");
    }

    /**
     *
     * @param localDateTime
     * @param formatOfDate
     * @return
     */
    public static String getUTCDateTextFromLocalDateTime(TemporalAccessor localDateTime, String formatOfDate) throws DateTimeException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(formatOfDate);
        return dtf.format(localDateTime);
    }

    /**
     *
     * @param localDateTime
     * @param formatOfDate
     * @param locale
     * @return
     */
    public static String getUTCDateTextWithLocaleFromLocalDateTime(TemporalAccessor localDateTime, String formatOfDate, Locale locale) throws DateTimeException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(formatOfDate, locale);
        return dtf.format(localDateTime);
    }

    /**
     *
     * @param localDate
     * @param localTime
     * @return
     */
    public static LocalDateTime getLocalDateTimeFromLocalDateAndLocaltime(LocalDate localDate, LocalTime localTime) throws DateTimeException {
        return LocalDateTime.of(localDate, localTime);
    }
}
