package org.inra.ecoinfo.utils;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import org.slf4j.LoggerFactory;

/**
 * The Class InfosReport.
 */
public class InfosReport {

    /**
     * The Constant PATTERN_DIV_COLOR_RED @link(String).
     */
    private static final String PATTERN_DIV_COLOR = "<p style='color: %s'>%s :</p>";
    /**
     * The Constant COLOR_GREEN.
     */
    private static final String COLOR_GREEN = "green";
    /**
     * The Constant PATTERN_DIV_INDENT @link(String).
     */
    private static final String PATTERN_DIV_INDENT = "<p style='text-indent: 30px'>- %s </p>";
    /**
     * The Constant PATTERN_TAB_STRING_RETURN @link(String).
     */
    private static final String PATTERN_TAB_STRING_RETURN = "\t - %s %n";
    /**
     * The infos messages @link(List<String>).
     */
    private Map<String, List<String>> infosMessages = new LinkedHashMap<>();
    /**
     * The MS g_ erro r_ forma t_15 @link(String).
     */
    private final String msgInfoFormat15;
    /**
     * The pattern color.
     */
    private final String patternColor;

    /**
     * Instantiates a new infos report.
     *
     * @param propertyMsgInfo the property msg info
     * @link(String) the property msg info
     */
    public InfosReport(final String propertyMsgInfo) {
        super();
        msgInfoFormat15 = String.format("%s %%15s", propertyMsgInfo);
        this.patternColor = String.format(InfosReport.PATTERN_DIV_COLOR, InfosReport.COLOR_GREEN, "%s");
    }

    /**
     * Instantiates a new infos report.
     *
     * @param propertyMsgInfo the property msg info
     * @param color the color
     */
    public InfosReport(final String propertyMsgInfo, String color) {
        super();
        msgInfoFormat15 = String.format("%s %%15s", propertyMsgInfo);
        this.patternColor = String.format(InfosReport.PATTERN_DIV_COLOR, color, "%s");
    }

    /**
     * Adds the exception.
     *
     * @param type the type
     * @param infos the infos
     * @link(Exception) the bad format exception
     */
    public void addInfos(final String type, final String infos) {
        if (infosMessages.get(type) == null) {
            infosMessages.put(type, new LinkedList<>());
        }
        infosMessages.get(type).add(infos);
    }

    /**
     * Builds the html messages.
     *
     * @return the string
     */
    public String buildHTMLMessages() {
        final StringBuilder builder = new StringBuilder();
        infosMessages.entrySet().stream().map((infoEntry) -> {
            builder.append(String.format(patternColor, infoEntry.getKey()));
            return infoEntry;
        }).forEach((infoEntry) -> {
            infoEntry.getValue().stream().forEach((message) -> {
                builder.append(String.format(InfosReport.PATTERN_DIV_INDENT, message));
            });
        });
        return builder.toString();
    }

    /**
     * Gets the infos messages.
     *
     * @return the infos messages
     */
    public Map<String, List<String>> getinfoMessages() {
        return infosMessages;
    }

    /**
     * Gets the messages.
     *
     * @return the messages
     */
    public String getMessages() {
        final StringBuilder builder = new StringBuilder();
        final Set<String> keys = infosMessages.keySet();
        String key = null;
        for (final Iterator<String> it = keys.iterator(); it.hasNext();) {
            key = it.next();
            builder.append(String.format(msgInfoFormat15, key));
            for (final String message : infosMessages.get(key)) {
                builder.append(String.format(InfosReport.PATTERN_TAB_STRING_RETURN, message));
            }
        }
        return builder.toString();
    }

    /**
     * Checks for infos.
     *
     * @return true, if successful
     */
    public boolean hasinfo() {
        return !infosMessages.isEmpty();
    }

    /**
     * Sets the infos messages.
     *
     * @param infosMessages the new infos messages
     */
    public void setInfoMessages(final Map<String, List<String>> infosMessages) {
        this.infosMessages = infosMessages;
    }

    /**
     * Write.
     *
     * @param outputStream the output stream
     * @link(OutputStream) the output stream
     */
    public void write(final OutputStream outputStream) {
        PrintStream ps = null;
        try {
            ps = new PrintStream(outputStream, true, StandardCharsets.UTF_8.name());
            final Set<String> keys = infosMessages.keySet();
            String key = null;
            for (final Iterator<String> it = keys.iterator(); it.hasNext();) {
                key = it.next();
                ps.println(String.format(msgInfoFormat15, key));
                for (final String message : infosMessages.get(key)) {
                    ps.println(String.format(InfosReport.PATTERN_TAB_STRING_RETURN, message));
                }
            }
        }catch (UnsupportedEncodingException ex) {
            LoggerFactory.getLogger(getClass()).error(ex.getMessage(), ex);
        } finally {
            StreamUtils.closeStream(ps);
        }
    }
}
