/**
 * OREILacs project - see LICENCE.txt for use created: 25 févr. 2009 14:23:25
 */
package org.inra.ecoinfo.utils.exceptions;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import org.inra.ecoinfo.utils.StreamUtils;
import org.slf4j.LoggerFactory;

/**
 * The Class BadsFormatsReport.
 *
 * @author "Antoine Schellenberger"
 */
public class BadsFormatsReport {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.utils.messages";
    /**
     * The errors @link(Map<String,List<Exception>>).
     */
    private final Map<String, List<Exception>> errors = new HashMap<>();
    /**
     * The errors messages @link(List<String>).
     */
    private List<String> errorsMessages = new LinkedList<>();
    /**
     * The pattern error @link(String).
     */
    private final String patternError;

    /**
     * Instantiates a new bads formats report.
     *
     * @param propertyMsgError the property msg error
     * @link(String) the property msg error
     */
    public BadsFormatsReport(final String propertyMsgError) {
        patternError = propertyMsgError;
    }

    /**
     * Adds the exception.
     *
     * @param badFormatException the bad format exception
     * @link(Exception) the bad format exception
     */
    public void addException(final Exception badFormatException) {
         List<Exception> badsFormatsExceptions = errors.get(badFormatException.getClass().getSimpleName());
        if (badsFormatsExceptions == null) {
            badsFormatsExceptions = new LinkedList<>();
            errors.put(badFormatException.getClass().getSimpleName(), badsFormatsExceptions);
        }
        badsFormatsExceptions.add(badFormatException);
        errorsMessages.add(badFormatException.getMessage());
    }

    /**
     * Gets the errors.
     *
     * @return the errors
     */
    public Map<String, List<Exception>> getErrors() {
        return errors;
    }

    /**
     * Gets the errors messages.
     *
     * @return the errors messages
     */
    public List<String> getErrorsMessages() {
        return errorsMessages;
    }

    /**
     * Sets the errors messages.
     *
     * @param errorsMessages the new errors messages
     */
    public void setErrorsMessages(final List<String> errorsMessages) {
        this.errorsMessages = errorsMessages;
    }

    /**
     * Gets the hTML messages.
     *
     * @return the hTML messages
     */
    public String getHTMLMessages() {
        final StringBuilder builder = new StringBuilder();
        final Set<String> keys = errors.keySet();
        String key = null;
        for (final Iterator<String> it = keys.iterator(); it.hasNext();) {
            key = it.next();
            builder.append(String.format("<p style='color: red'>%s :</p>", String.format(patternError, key)));
            for (final Exception badFormatException : errors.get(key)) {
                builder.append(String.format("<p style='text-indent: 30px'>- %s </p>", badFormatException.getMessage()));
            }
        }
        return builder.toString();
    }

    /**
     * Gets the messages.
     *
     * @return the messages
     */
    public String getMessages() {
        final StringBuilder builder = new StringBuilder();
        final Set<String> keys = errors.keySet();
        String key = null;
        for (final Iterator<String> it = keys.iterator(); it.hasNext();) {
            key = it.next();
            builder.append(String.format("%s :", String.format(patternError, key)));
            for (final Exception badFormatException : errors.get(key)) {
                builder.append(String.format("- %s ", badFormatException.getMessage()));
            }
        }
        return builder.toString();
    }

    /**
     * Checks for errors.
     *
     * @return true, if successful
     */
    public boolean hasErrors() {
        return !errors.keySet().isEmpty();
    }

    /**
     * Write.
     *
     * @param outputStream the output stream
     * @link(OutputStream) the output stream
     */
    public void write(final OutputStream outputStream) {
        PrintStream ps = null;
        try {
            ps = new PrintStream(outputStream, true, StandardCharsets.UTF_8.name());
            final Set<String> keys = errors.keySet();
            String key = null;
            for (final Iterator<String> it = keys.iterator(); it.hasNext();) {
                key = it.next();
                ps.println(String.format("%s:", String.format(patternError, key)));
                for (final Exception badFormatException : errors.get(key)) {
                    ps.println(String.format("\t - %s", badFormatException.getMessage()));
                }
            }
        } catch (UnsupportedEncodingException ex) {
            LoggerFactory.getLogger(getClass()).error(ex.getMessage(), ex);
        } finally {
            StreamUtils.closeStream(ps);
        }
    }
}
