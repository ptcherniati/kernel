package org.inra.ecoinfo.config.jpa;

import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class JPAGenericDAO extends AbstractJPADAO<Object> implements IGenericDAO{

    /**
     * The data source @link(DataSource).
     */
    static DataSource dataSource;
    /**
     * The datasource connection @link(DatabaseDataSourceConnection).
     */
    static DatabaseDataSourceConnection datasourceConnection;

    /**
     *
     * @return
     */
    public static DataSource getDataSource() {
        return dataSource;
    }

    /**
     *
     * @param dataSource
     */
    public static void setDataSource(DataSource dataSource) {
        JPAGenericDAO.dataSource = dataSource;
    }

    /**
     *
     * @param nativeScriptSQL
     * @return
     */
    @Override
    public int execute(String nativeScriptSQL) {
        return entityManager.createNativeQuery(nativeScriptSQL).executeUpdate();
    }

    /**
     * Gets the datasource connexion.
     *
     * @return the datasource connexion
     * @throws SQLException the sQL exception
     */
    protected DatabaseDataSourceConnection getDatasourceConnexion() throws SQLException {
        datasourceConnection = new DatabaseDataSourceConnection(dataSource, "public");
        datasourceConnection.getConfig().setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);
        return datasourceConnection;
    }

    /**
     *
     * @param <T>
     * @param nativeScriptSQL
     * @param T
     * @return
     */
    @Override
    public <T> List<T> execute(String nativeScriptSQL, Class T) {
        return entityManager.createNativeQuery(nativeScriptSQL).getResultList();
    }
}
