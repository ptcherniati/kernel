package org.inra.ecoinfo.config.jpa;

import java.util.List;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IGenericDAO extends IDAO<Object> {

    /**
     *
     * @param nativeScriptSQL
     * @return
     */
    int execute(String nativeScriptSQL);

    /**
     *
     * @param <T>
     * @param nativeScriptSQL
     * @param T
     * @return
     */
    <T> List<T> execute(String nativeScriptSQL, Class T);

}
