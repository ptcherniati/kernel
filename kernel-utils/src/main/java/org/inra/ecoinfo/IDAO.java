/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:55:22
 */
package org.inra.ecoinfo;

/**
 * @author "Antoine Schellenberger"
 *
 */

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.TransactionStatus;

/**
 * The Interface IDAO.
 *
 * @param <T> the generic type
 */
public interface IDAO<T> extends Serializable {

    /**
     * Begin default transaction.
     *
     * @return the TransactionStatus transaction status
     */
    TransactionStatus beginDefaultTransaction();

    /**
     * Begin new transaction.
     *
     * @return the TransactionStatus transaction status
     */
    TransactionStatus beginNewTransaction();

    /**
     * Commit transaction.
     *
     * @param status the TransactionStatus status
     */
    void commitTransaction(TransactionStatus status);

    /**
     * Flush.
     */
    void flush();

    /**
     * Gets the List<T> all.
     *
     * @param <C>
     * @param clazz the Class clazz
     * @return the List<T> all
     */
    <C> List<C> getAll(final Class<C> clazz);

    /**
     * Gets the List<T> all.
     *
     * @param <C>
     * @param clazz the Class clazz
     * @return the List<T> all
     */
    <C> List<C> getAllBy(final Class<C> clazz,  SingularAttribute<C, ?> attribute);

    <C, T extends Comparable<? super T>> List<C> getAllBy(final Class<C> clazz, Function<? super C,? extends T> comparingFunction) ;

    /**
     * Gets the T by id.
     *
     * @param clazz the Class clazz
     * @param id the long id
     * @return the T by id
     */
    Optional<T> getById(Class<T> clazz, long id);

    /**
     * Gets the int count.
     *
     * @param clazz the Class clazz
     * @return the int count
     */
    Long getCount(Class<T> clazz);

    /**
     * Merge.
     *
     * @param object the T object
     * @return the T t
     * @throws PersistenceException the persistence exception
     */
    T merge(T object) throws PersistenceException;

    /**
     * Removes the.
     *
     * @param object the T object
     * @throws PersistenceException the persistence exception
     */
    void remove(T object) throws PersistenceException;

    /**
     * Removes the all.
     *
     * @param clazz the Class<?> clazz
     * @throws PersistenceException the persistence exception
     */
    void removeAll(Class<T> clazz) throws PersistenceException;

    /**
     * Rollback transaction.
     *
     * @param status the TransactionStatus status
     */
    void rollbackTransaction(TransactionStatus status);

    /**
     * Save or update.
     *
     * @param object the T object
     * @throws PersistenceException the persistence exception
     */
    void saveOrUpdate(T object) throws PersistenceException;
}
