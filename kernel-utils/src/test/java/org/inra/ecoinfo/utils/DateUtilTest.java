package org.inra.ecoinfo.utils;

import java.time.*;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The Class DateUtilTest.
 */
public class DateUtilTest {

    /**
     *
     */
    public static final int HOURS_1 = 3_600_000;
    /**
     *
     */
    protected static final String DATE_PATTERN = "yyyy-MM-dd";

    /**
     *
     */
    protected static final String TIME_PATTERN = "HH:mm:ss";

    /**
     *
     */
    protected static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     *
     */
    protected static final String DATE = "2010-01-23";
    /**
     *
     */
    protected static final String TIME = "22:15:56";
    /**
     *
     */
    protected static final String DATE_TIME = "2010-01-23T22:15:56";
    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
    private static LocalDateTime ldt;
    private static LocalDate ld;
    private static LocalTime lt;

    /**
     * @throws DateTimeException
     */
    @BeforeClass
    public static void init() throws DateTimeException {
        ldt = LocalDateTime.of(2010, 01, 23, 22, 15, 56);
        ld = LocalDate.of(2010, 01, 23);
        lt = LocalTime.of(22, 15, 56);

    }

    /**
     * @throws Exception
     */
    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of readLocalDateTimeFromText method, of class DateUtil.
     */
    @Test
    public void testReadLocalDateTimeFromText() {
        LocalDateTime result = DateUtil.readLocalDateTimeFromText(DATE_TIME_PATTERN, DATE_TIME);
        assertEquals(ldt, result);
    }

    /**
     * Test of isTimePattern method, of class DateUtil.
     */
    @Test
    public void testIsTimePattern() {
        assertTrue(DateUtil.isTimePattern("HH:mm/ss"));
        assertTrue(DateUtil.isTimePattern("dHd"));
        assertTrue(DateUtil.isTimePattern("dmd"));
        assertTrue(DateUtil.isTimePattern("dsd"));
        assertTrue(DateUtil.isTimePattern("dSd"));
        assertTrue(DateUtil.isTimePattern("dAd"));
        assertTrue(DateUtil.isTimePattern("dnd"));
        assertTrue(DateUtil.isTimePattern("dNd"));
        assertTrue(DateUtil.isTimePattern(ALPHABET));
        assertFalse(DateUtil.isTimePattern(ALPHABET.replaceAll("[HmsSAnN]", "")));
    }

    /**
     * Test of isDatePattern method, of class DateUtil.
     */
    @Test
    public void testIsDatePattern() { //uyDMLdQqYwWEecF
        //uyDMLdQqYwWEecF
        assertTrue(DateUtil.isDatePattern("HuH"));
        assertTrue(DateUtil.isDatePattern("HyH"));
        assertTrue(DateUtil.isDatePattern("HDH"));
        assertTrue(DateUtil.isDatePattern("HMH"));
        assertTrue(DateUtil.isDatePattern("HLH"));
        assertTrue(DateUtil.isDatePattern("HdH"));
        assertTrue(DateUtil.isDatePattern("HQH"));
        assertTrue(DateUtil.isDatePattern("HqH"));
        assertTrue(DateUtil.isDatePattern("HYH"));
        assertTrue(DateUtil.isDatePattern("HwH"));
        assertTrue(DateUtil.isDatePattern("HcH"));
        assertTrue(DateUtil.isDatePattern("HeH"));
        assertTrue(DateUtil.isDatePattern("HEH"));
        assertTrue(DateUtil.isDatePattern("HFH"));
        assertTrue(DateUtil.isDatePattern(ALPHABET));
        assertFalse(DateUtil.isDatePattern(ALPHABET.replaceAll("[uyDMLdQqYwWEecF]", "")));
    }

    /**
     * Test of getUTCDateTextFromLocalDateTime method, of class DateUtil.
     */
    @Test
    public void testGetUTCDateTextFromLocalDateTime() {
        String result = DateUtil.getUTCDateTextFromLocalDateTime(ldt, DATE_TIME_PATTERN);
        assertEquals(DATE_TIME, result);
    }

    /**
     * Test of getLocaleZoneId method, of class DateUtil.
     */
    @Test
    public void testGetLocaleZoneId() {
        assertEquals("Europe/Paris", DateUtil.getLocaleZoneId().toString());
    }

    /**
     * Test of getLocaleOffset method, of class DateUtil.
     */
    @Test
    public void testGetLocaleOffset() {
        ZoneId result = DateUtil.getLocaleOffset(Instant.ofEpochMilli(362326606546056L));
        assertEquals("+02:00", result.toString());
    }

    /**
     * Test of getLocalDateTimeFromLocalDateAndLocaltime method, of class
     * DateUtil.
     */
    @Test
    public void testGetLocalDateTimeFromLocalDateAndLocaltime() {
        LocalDateTime result = DateUtil.getLocalDateTimeFromLocalDateAndLocaltime(ld, lt);
        assertEquals(ldt, result);
    }

    /**
     * Test of readLocalDateFromText method, of class DateUtil.
     */
    @Test
    public void testReadLocalDateFromText() {
        LocalDate result = DateUtil.readLocalDateFromText(DATE_PATTERN, DATE);
        assertEquals(DATE, result.toString());
    }

    /**
     * Test of readLocalTimeFromText method, of class DateUtil.
     */
    @Test
    public void testReadLocalTimeFromText() {
        LocalTime result = DateUtil.readLocalTimeFromText(TIME_PATTERN, TIME);
        assertEquals(TIME, result.toString());
    }
}
