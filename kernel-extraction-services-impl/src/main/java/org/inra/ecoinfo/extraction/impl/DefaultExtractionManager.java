package org.inra.ecoinfo.extraction.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.ExtractionType;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IMotivation;
import org.inra.ecoinfo.extraction.IMotivationFormatter;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.IRequeteDAO;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.config.IExtractorDAO;
import org.inra.ecoinfo.extraction.entity.Requete;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.exception.OverFileSizeException;
import org.inra.ecoinfo.extraction.jpa.JPAExtractionTypeDAO;
import org.inra.ecoinfo.identification.IUsersDeletion;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.jobs.StatusBar;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.StreamUtils;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.DeleteUserException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class DefaultExtractionManager.
 */
public class DefaultExtractionManager extends MO implements IExtractionManager, IUsersDeletion {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.extraction.impl.messages";
    /**
     * The Constant MSG_EXTRACTION_ABORTED @link(String).
     */
    private static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";
    /**
     * The Constant MSG_EXTRACTION_SUCCESS @link(String).
     */
    private static final String MSG_EXTRACTION_SUCCESS = "PROPERTY_MSG_SUCCES_EXTRACT_WITH_TIME";
    /**
     * The Constant MSG_MISSING_DATATYPE @link(String).
     */
    private static final String MSG_MISSING_DATATYPE = "PROPERTY_MSG_BAD_DATATYPE";
    /**
     * The Constant MSG_START_EXTRACTION @link(String).
     */
    private static final String MSG_START_EXTRACTION = "PROPERTY_MSG_EXTRACT_BEGIN";
    /**
     * The Constant PROPERTY_FORMAT_TIME @link(String).
     */
    private static final String PROPERTY_FORMAT_TIME = "PROPERTY_FORMAT_TIME";
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultExtractionManager.class.getName());
    private static final Logger LOGGER_EXTRACTION = LoggerFactory.getLogger("extraction.logger");
    /**
     * The Constant EXTENSION_CSV @link(String).
     */
    protected static final String EXTENSION_CSV = ".csv";
    /**
     * The Constant EXTENSION_ZIP @link(String).
     */
    protected static final String EXTENSION_ZIP = ".zip";
    /**
     * The Constant EXTRACTION @link(String).
     */
    protected static final String EXTRACTION = "extraction";

    /**
     *
     */
    protected static final String MSG_EXTRACTION_TO_BIG_DATA = "MSG_EXTRACTION_TO_BIG_DATA";

    /**
     *
     */
    protected static final String PROPERTY_MSG_FAILED_EXTRACT = "PROPERTY_MSG_FAILED_EXTRACT";

    /**
     *
     */
    protected static final String MSG_EXTRACTION_UNKNOWN_EXCEPTION = "MSG_EXTRACTION_UNKNOWN_EXCEPTION";
    /**
     * The Constant FILE_SEPARATOR @link(String).
     */
    protected static final String FILE_SEPARATOR = System.getProperty("file.separator");
    /**
     * The Constant SEPARATOR_TEXT @link(String).
     */
    protected static final String SEPARATOR_TEXT = "_";

    /**
     * The Constant PROPERTY_MSG_FOR_SUCCESS_EXTRACTION_DELETION @link(String).
     */
    protected static final String PROPERTY_MSG_FOR_SUCCESS_EXTRACTION_DELETION = "PROPERTY_MSG_FOR_SUCCESS_EXTRACTION_DELETION";

    /**
     * The Constant PROPERTY_MSG_FOR_ECHEC_EXTRACTION_DELETION @link(String).
     */
    protected static final String PROPERTY_MSG_FOR_ECHEC_EXTRACTION_DELETION = "PROPERTY_MSG_FOR_ECHEC_EXTRACTION_DELETION";

    /**
     * The configuration @link(Configuration).
     */
    protected ICoreConfiguration configuration;
    /**
     * The datatype dao @link(IDatatypeDAO).
     */
    protected IDatatypeDAO datatypeDAO;
    /**
     * The extraction type dao @link(JPAExtractionTypeDAO).
     */
    protected JPAExtractionTypeDAO extractionTypeDAO;
    /**
     * The extractor dao @link(IExtractorDAO).
     */
    protected IExtractorDAO extractorDAO;
    /**
     * The requete dao @link(IRequeteDAO).
     */
    protected IRequeteDAO requeteDAO;
    /**
     * The utilisateur dao @link(IUtilisateurDAO).
     */
    protected IUtilisateurDAO utilisateurDAO;

    /**
     * Delete requests from ids.
     *      *      *      *      *      *      *
     * @param ids the ids
     *      * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.extraction.IExtractionManager#deleteRequestsFromIds(java.lang.Long[])
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteRequestsFromIds(final Long[] ids) throws BusinessException {
        try {
            if (ids.length > 0) {
                for (final Long id : ids) {
                    final Requete requete = requeteDAO.getById(id)
                            .orElseThrow(() -> new BusinessException("no request"));
                    deleteFile(requete.getNomFichier(), requete.getExtractionType().getCode(), DefaultExtractionManager.EXTENSION_ZIP);
                }
                requeteDAO.deleteRequestsFromIds(ids);
            }
        } catch (final PersistenceException e) {
            throw new BusinessException("no request", e);
        }
    }

    protected void deleteRequestsFromIdsNonTransactional(final Long[] ids) throws BusinessException {
        try {
            if (ids.length > 0) {
                for (final Long id : ids) {
                    final Requete requete = requeteDAO.getById(id)
                            .orElseThrow(() -> new BusinessException("no request"));
                    deleteFile(requete.getNomFichier(), requete.getExtractionType().getCode(), DefaultExtractionManager.EXTENSION_ZIP);
                }
                requeteDAO.deleteRequestsFromIds(ids);
            }
        } catch (final PersistenceException e) {
            throw new BusinessException("no request", e);
        }
    }

    /**
     *
     * @param utilisateur
     * @return
     * @throws DeleteUserException
     */
    @Override
    public InfosReport deleteDependancesUserProfile(Utilisateur utilisateur) throws DeleteUserException {
        InfosReport extractionReport = new InfosReport("extraction Deletion");
        String reportMessage;
        try {
            List<ExtractionType> userExtractionsType = getExtractiontypesResultsAvailables(utilisateur);

            DeleteAllUserExtractionsRequestsAndFiles(userExtractionsType, utilisateur);

            reportMessage = String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_FOR_SUCCESS_EXTRACTION_DELETION), utilisateur.getLogin());
            extractionReport.addInfos("Extraction", reportMessage);

        } catch (BusinessException e) {
            reportMessage = String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_FOR_ECHEC_EXTRACTION_DELETION), utilisateur.getLogin());
            throw new DeleteUserException(reportMessage);
        }

        return extractionReport;
    }

    protected void DeleteAllUserExtractionsRequestsAndFiles(List<ExtractionType> userExtractionsType, Utilisateur utilisateur) throws BusinessException {

        int i = 0;
        int j = 0;
        for (ExtractionType extraction : userExtractionsType) {
            List<Requete> allAssociatedRequests = requeteDAO.getAllByLoginAndExtractionTypeName(utilisateur.getId(), extraction.getId());

            Long[] longRequeteIds = new Long[allAssociatedRequests.size()];
            j = 0;
            for (Requete requete : allAssociatedRequests) {
                longRequeteIds[j] = requete.getId();

                j++;
            }

            deleteRequestsFromIdsNonTransactional(longRequeteIds);
            i++;
        }
    }

    /**
     * Do download.
     *      *      *      *      *      *      *
     * @param requete the requete
     *      * @return the byte[]
     *      * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.extraction.IExtractionManager#doDownload(org.inra.ecoinfo.extraction.RequeteVO)
     */
    @Override
    public byte[] doDownload(final Requete requete) throws BusinessException {
        final String extractionTypeCode = requete.getExtractionType().getCode();
        final String filenameSuffix = requete.getNomFichier();
        return doDownload(extractionTypeCode, filenameSuffix);
    }

    /**
     * Do download.
     *      *      *      *      *      *      *
     * @param codeExtractionType the code extraction type
     * @param suffixNameFile the suffix name file
     *      * @return the byte[]
     *      * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.extraction.IExtractionManager#doDownload(java.lang.String,
     * java.lang.String)
     */
    @Override
    public byte[] doDownload(final String codeExtractionType, final String suffixNameFile) throws BusinessException {
        FileInputStream fis = null;
        if (codeExtractionType == null) {
            throw new IllegalArgumentException("no extraction type");
        }
        final String filename = codeExtractionType.concat(DefaultExtractionManager.SEPARATOR_TEXT).concat(suffixNameFile).concat(DefaultExtractionManager.EXTENSION_ZIP);
        byte[] data = null;
        FileChannel fc = null;
        try {
            fis = genericDoDownload(codeExtractionType, filename);
            fc = fis.getChannel();
            data = new byte[(int) fc.size()];
            final ByteBuffer bb = ByteBuffer.wrap(data);
            fc.read(bb);
            StreamUtils.closeStream(fc);
            StreamUtils.closeStream(fis);
        } catch (final FileNotFoundException e) {
            throw new BusinessException("file not found", e);
        } catch (final IOException e) {
            throw new BusinessException("ioException", e);
        } finally {
            StreamUtils.closeStream(fc);
            StreamUtils.closeStream(fis);
        }
        return data;
    }

    /**
     * Extract.
     *      *      *      *      *      *      *
     * @param parameters the parameters
     * @param code the code
     *      * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.extraction.IExtractionManager#extract(org.inra.ecoinfo.extraction.IParameter,
     * int)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void extract(final IParameter parameters, final int code) throws BusinessException {
        MDC.put("user", policyManager.getCurrentUserLogin());
        try {
            final Utilisateur utilisateur = utilisateurDAO.getByLogin(policyManager.getCurrentUserLogin()).orElseThrow(PersistenceException::new);
            tryExtract(parameters, code, utilisateur);
        } catch (final PersistenceException e) {
            throw new BusinessException("unknown user", e);
        }
    }

    /**
     * Gets the extractiontypes results availables.
     *      *      *      *      *      *      *
     * @param utilisateur the utilisateur
     *      * @return the extractiontypes results availables
     *      * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.extraction.IExtractionManager#getExtractiontypesResultsAvailables(org.inra.ecoinfo.identification.entity.Utilisateur)
     */
    @Override
    public List<ExtractionType> getExtractiontypesResultsAvailables(final Utilisateur utilisateur) throws BusinessException {
        return requeteDAO.getExtractiontypesResultsAvailables(utilisateur.getId());
    }

    /**
     * Gets the requests results by extraction type code.
     *      *      *      *      *      *      *
     * @param extractionTypeCode the extraction type code
     * @param utilisateur the utilisateur
     *      * @return the requests results by extraction type code
     *      * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.extraction.IExtractionManager#getRequestsResultsByExtractionTypeCode(java.lang.String,
     * org.inra.ecoinfo.identification.entity.Utilisateur)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, readOnly = true)
    public List<Requete> getRequestsResultsByExtractionTypeCode(final String extractionTypeCode, final Utilisateur utilisateur) throws BusinessException {
        Optional<ExtractionType> extractionTypeOpt = extractionTypeDAO.getByCode(extractionTypeCode);
        if (!extractionTypeOpt.isPresent()) {
            final String message = String.format(localizationManager.getMessage(DefaultExtractionManager.BUNDLE_SOURCE_PATH, DefaultExtractionManager.MSG_MISSING_DATATYPE), extractionTypeCode);
            LOGGER.error(message, message);
            throw new BusinessException(message);
        }
        return requeteDAO.getAllByLoginAndExtractionTypeName(utilisateur.getId(), extractionTypeOpt.get().getId());
    }

    /**
     * Sets the configuration.
     *      *      *      *      *      *      *
     * @param configuration the new configuration
     */
    public void setConfiguration(final ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the datatype dao.
     *      *      *      *      *      *      *
     * @param datatypeDAO the new datatype dao
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Sets the extracter dao.
     *      *      *      *      *      *      *
     * @param extractorDAO the new extracter dao
     */
    public void setExtractorDAO(final IExtractorDAO extractorDAO) {
        this.extractorDAO = extractorDAO;
    }

    /**
     * Sets the extraction type dao.
     *      *      *      *      *      *      *
     * @param extractionTypeDAO the new extraction type dao
     */
    public void setExtractionTypeDAO(final JPAExtractionTypeDAO extractionTypeDAO) {
        this.extractionTypeDAO = extractionTypeDAO;
    }

    /**
     * Sets the requete dao.
     *      *      *      *      *      *      *
     * @param requeteDAO the new requete dao
     */
    public void setRequeteDAO(final IRequeteDAO requeteDAO) {
        this.requeteDAO = requeteDAO;
    }

    /**
     * Sets the utilisateur dao.
     *      *      *      *      *      *      *
     * @param utilisateurDAO the new utilisateur dao
     */
    public void setUtilisateurDAO(final IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     * Delete file.
     *      *      *      *      *      *      *
     * @param filenameSuffix the filename suffix
     * @param datatypeCode the datatype code
     * @param extension the extension
     * @link(String) the filename suffix
     * @link(String) the datatype code
     * @link(String) the extension
     */
    private void deleteFile(final String filenameSuffix, final String datatypeCode, final String extension) {
        final String fileSeparator = DefaultExtractionManager.FILE_SEPARATOR;
        final String cheminExtraction = configuration.getRepositoryURI().concat(fileSeparator).concat(DefaultExtractionManager.EXTRACTION).concat(fileSeparator);
        final String filename = cheminExtraction.concat(datatypeCode).concat(DefaultExtractionManager.SEPARATOR_TEXT).concat(filenameSuffix).concat(extension);
        final File file = new File(filename);
        if (file.delete()) {
            LOGGER.info("{} deleted", file.getAbsolutePath());
        }
    }

    private FileInputStream genericDoDownload(final String codeExtractionType, final String filename) throws FileNotFoundException {
        FileInputStream fis;
        try {
            fis = new FileInputStream(configuration.getRepositoryURI().concat(DefaultExtractionManager.EXTRACTION).concat(DefaultExtractionManager.FILE_SEPARATOR).concat(filename));
        } catch (final FileNotFoundException e) {
            LOGGER.info("file not found", e);
            fis = new FileInputStream(configuration.getRepositoryURI().concat(DefaultExtractionManager.EXTRACTION).concat(DefaultExtractionManager.FILE_SEPARATOR).concat(codeExtractionType).concat(DefaultExtractionManager.FILE_SEPARATOR)
                    .concat(filename));
        }
        return fis;
    }

    /**
     * Save request results.
     *      *      *      *      *      *      *
     * @param extractionType the extraction type
     * @param comments the comments
     * @param roBuildZipOutputStream the ro build zip output stream
     * @param utilisateur the utilisateur
     *      * @throws PersistenceException the persistence exception
     * @throws BusinessException the business exception
     * @link(ExtractionType) the extraction type
     * @link(String) the comments
     * @link(RObuildZipOutputStream) the ro build zip output stream
     * @link(Utilisateur) the utilisateur
     */
    private void saveRequestResults(ExtractionType extractionType, final String comments, final RObuildZipOutputStream roBuildZipOutputStream, final Utilisateur utilisateur, Instant startDate) throws BusinessException {
        try {
            ExtractionType localExtractionType = extractionType;
            final String extractionTypeCode = localExtractionType.getCode();
            final Instant endDate = Instant.now();
            final Duration processDate = Duration.between(startDate, endDate);
            localExtractionType = extractionTypeDAO.getByCode(extractionTypeCode)
                    .orElseThrow(() -> new BusinessException("can't get ExtractionType"));
            final Requete requete = new Requete();
            requete.setExtractionType(localExtractionType);
            requete.setUtilisateur(utilisateur);
            requete.setCommentaire(comments);
            requete.setDureeExtraction(processDate.toMillis());
            requete.setNomFichier(roBuildZipOutputStream.getFileSuffix());
            requeteDAO.saveOrUpdate(requete);
            final String attachment = DefaultExtractionManager.EXTRACTION.concat(DefaultExtractionManager.FILE_SEPARATOR).concat(extractionTypeCode).concat(DefaultExtractionManager.FILE_SEPARATOR).concat(extractionTypeCode)
                    .concat(DefaultExtractionManager.SEPARATOR_TEXT).concat(roBuildZipOutputStream.getFileSuffix()).concat(DefaultExtractionManager.EXTENSION_ZIP);
            final String processTime = DurationFormatUtils.formatDuration(processDate.toMillis(), localizationManager.getMessage(DefaultExtractionManager.BUNDLE_SOURCE_PATH, DefaultExtractionManager.PROPERTY_FORMAT_TIME));
            sendNotificationWithAttachment(String.format(localizationManager.getMessage(DefaultExtractionManager.BUNDLE_SOURCE_PATH, DefaultExtractionManager.MSG_EXTRACTION_SUCCESS), localExtractionType.getName(), processTime), Notification.INFO,
                    comments, attachment, utilisateur);
        } catch (PersistenceException ex) {
            throw new BusinessException(ex);
        }
    }

    private RObuildZipOutputStream tryBuildOutput(final IParameter parameters, final IOutputBuilder outputBuilder) throws BusinessException {

        MDC.clear();
        try {
            Optional.ofNullable(policyManager)
                    .map(policyManager -> (Utilisateur) policyManager.getCurrentUser())
                    .ifPresent(utilisateur -> {
                        MDC.put("extract.user.name", utilisateur.getNom());
                        MDC.put("extract.user.login", utilisateur.getLogin());
                        MDC.put("extract.user.surname", utilisateur.getPrenom());
                        MDC.put("extract.user.email", utilisateur.getEmail());
                    });
            MDC.put("extract.extractionType", parameters.getExtractionTypeCode());
            MDC.put("extract.dateExtration", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.YYYY_MM_DD_HHMMSS));
            final RObuildZipOutputStream buildOutput = outputBuilder.buildOutput(parameters);
            buildOutput.getZipOutputStream().close();
            LOGGER_EXTRACTION.info(MDC.getCopyOfContextMap().toString());
            return buildOutput;
        } catch (NoExtractionResultException | IOException e) {
            throw new BusinessException("file not found ", e);
        }
    }

    private void tryExtract(final IParameter parameters, final int code, final Utilisateur utilisateur) throws BusinessException {
        Allocator allocator = Allocator.getInstance();
        StatusBar statusBar = null;
        try {
            statusBar = new StatusBar(policyManager.getCurrentUser(), parameters.getExtractionTypeCode());
            parameters.getParameters().put(parameters.getExtractionTypeCode(), statusBar);
            final IExtractor extractor = extractorDAO.getExtractorByCode(parameters.getExtractionTypeCode());
            final IOutputBuilder outputBuilder = extractorDAO.getDisplayByCodeAndOrder(parameters.getExtractionTypeCode(), code);
            final ExtractionType extractionType = extractionTypeDAO.getByCode(parameters.getExtractionTypeCode())
                    .orElseThrow(() -> new PersistenceException("can't get extractionType"));
            sendNotification(String.format(localizationManager.getMessage(DefaultExtractionManager.BUNDLE_SOURCE_PATH, DefaultExtractionManager.MSG_START_EXTRACTION), parameters.getExtractionTypeCode()), Notification.INFO,
                    parameters.getCommentaire(), utilisateur);
            Instant startDate = Instant.now();
            allocate(utilisateur, allocator, extractor, parameters);
            statusBar.setProgress(0);
            extractor.extract(parameters);
            RObuildZipOutputStream rObuildZipOutputStream = tryBuildOutput(parameters, outputBuilder);
            saveRequestResults(extractionType, parameters.getCommentaire(), rObuildZipOutputStream, utilisateur, startDate);
            IMotivation motivation = ((InheritableThreadLocal<IMotivation>) DefaultMotivation.getLocalThread()).get();
            IMotivationFormatter motivationFormatter = extractorDAO.getMotivationFormatter(parameters.getExtractionTypeCode());
            if (motivation != null && motivationFormatter != null) {
                motivationFormatter.formatMotivation(parameters, motivation, localizationManager, (Utilisateur) policyManager.getCurrentUser());
                LoggerFactory.getLogger(IMotivationFormatter.class).info(motivation.getMotivation());
            }
        } catch (OverFileSizeException e) {
            sendToHeavyExtraction(utilisateur);
        } catch (final PersistenceException e) {
            sendNotification(String.format(localizationManager.getMessage(DefaultExtractionManager.BUNDLE_SOURCE_PATH, DefaultExtractionManager.MSG_EXTRACTION_ABORTED)), Notification.ERROR, e.getMessage(), utilisateur);
            throw new BusinessException("can't persist request", e);
        } catch (final NoExtractionResultException e) {
            LOGGER.debug(e.getMessage(), e);
            sendNotification(e.getMessage(), Notification.WARN, e.getMessage(), utilisateur);
        } catch (final BusinessException e) {
            if (e.getCause() != null && e.getCause() instanceof NoExtractionResultException) {
                sendNotification(e.getMessage(), Notification.WARN, e.getCause().getMessage(), utilisateur);
            } else {
                throw new BusinessException(e.getMessage(), e);
            }
            LOGGER.error("businessException", e);
        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException", e);
            sendToHeavyExtraction(utilisateur);
        } catch (Exception e) {
            LOGGER.error("not catched Exception in extraction", e);
            sendUnknownExeption(utilisateur, e);
        } finally {
            StatusBar.getStatusBars().remove(statusBar.getDate());
            if (allocator != null) {
                allocator.free("extract");
            }
        }
    }

    private void sendUnknownExeption(final Utilisateur utilisateur, Exception e) throws BusinessException {
        String message = String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, MSG_EXTRACTION_UNKNOWN_EXCEPTION));
        String info = String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_FAILED_EXTRACT));
        sendNotification(info, Notification.WARN, message, utilisateur);
        UncatchedExceptionLogger.logUncatchedException(info, e);
    }

    private void sendToHeavyExtraction(final Utilisateur utilisateur) throws BusinessException {
        String message = localizationManager.getMessage(BUNDLE_SOURCE_PATH, MSG_EXTRACTION_TO_BIG_DATA);
        String info = localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_FAILED_EXTRACT);
        sendNotification(info, Notification.WARN, message, utilisateur);
    }

    private void allocate(Utilisateur user, Allocator allocator, final IExtractor extractor, final IParameter parameters) throws InterruptedException, BusinessException {
        if (allocator != null) {
            final long extractionSize = extractor.getExtractionSize(parameters);
            if (extractionSize < 0 || extractionSize > 2_500_000_000L) {
                throw new OverFileSizeException(null);
            }
            allocator.allocate("extract", extractionSize);
        }
    }
}
