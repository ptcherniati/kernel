package org.inra.ecoinfo.extraction.serialization;

/**
 * The Interface IIdSerialization.
 */
public interface IExtractionSerialization {

    /**
     *
     * @return
     */
    String getId();
}
