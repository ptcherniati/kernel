/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction.genericfilesrepository.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.Tuple;
import org.inra.ecoinfo.dataset.versioning.IGenericDatatypeDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptchernia
 */
public class GenericDatatypeExtractor implements IExtractor {

    IGenericDatatypeDAO genericDatatypeDAO;
    IVersionFileDAO versionFileDAO;
    IPolicyManager policyManager;

    /**
     *
     */
    protected Extraction extraction;

    @Override
    public void extract(IParameter parameters) throws BusinessException {
        List<VersionFile> versionFiles = Optional.ofNullable(parameters.getParameters())
                .map(p -> getVersionFiles(parameters))
                .orElse(new LinkedList());
        parameters.getParameters()
                .put(VersionFile.class.getSimpleName(), versionFiles);
    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        return 0L;
    }

    /**
     *
     * @param genericDatatypeDAO
     */
    public void setGenericDatatypeDAO(IGenericDatatypeDAO genericDatatypeDAO) {
        this.genericDatatypeDAO = genericDatatypeDAO;
    }

    /**
     *
     * @param parameter
     * @return
     */
    protected List<VersionFile> getVersionFiles(IParameter parameter) {
        Set<NodeDataSet> nodeDataSets = (Set<NodeDataSet>) parameter.getParameters().get(NodeDataSet.class.getSimpleName());
        List<IntervalDate> intervalDates = (List<IntervalDate>) parameter.getParameters().get(IntervalDate.class.getSimpleName());
        List<Tuple> dbTuples = genericDatatypeDAO.getNodeDataSetFromNodesAndPeriods(nodeDataSets, intervalDates);
        Map<Long, Map<Group, List<LocalDate>>> rights = policyManager.getOrLoadActivities(policyManager.getCurrentUser(), WhichTree.TREEDATASET)
                .get(Activities.extraction);
        return dbTuples.stream()
                .filter(tuple->policyManager.getCurrentUser().getIsRoot() || rights.containsKey(((NodeDataSet)tuple.get(0)).getId()))
                .filter(tuple->policyManager.getCurrentUser().getIsRoot() || hasDatesRights(((Dataset)tuple.get(1)), rights.get(((NodeDataSet)tuple.get(0)).getId())))
                .map(tuple->((Dataset)tuple.get(1)))
                .map(dataset->dataset.getPublishVersion())
                .map(version->versionFileDAO.getById(version.getId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
            
        }

    /**
     *
     * @param dataset
     * @param rights
     * @return
     */
    protected boolean hasDatesRights(Dataset dataset, Map<Group, List<LocalDate>> rights) {
        return rights.values().stream()
                .anyMatch(listDate->testDates(dataset.getDateDebutPeriode(), dataset.getDateFinPeriode(), listDate.get(0), listDate.get(1)));
    }
    
    /**
     *
     * @param dateDebutPeriode
     * @param dateFinPeriode
     * @param minRightDate
     * @param maxRightDate
     * @return
     */
    protected boolean testDates(LocalDateTime dateDebutPeriode, LocalDateTime dateFinPeriode, LocalDate minRightDate, LocalDate maxRightDate){
        return (dateDebutPeriode.isBefore(maxRightDate.atTime(23, 59, 59)) || dateDebutPeriode.isEqual(maxRightDate.atTime(23, 59, 59)))
                &&
                (dateFinPeriode.isAfter(minRightDate.atStartOfDay()) || dateFinPeriode.isEqual(minRightDate.atStartOfDay()));
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    @Override
    public void setExtraction(Extraction extraction) {
        this.extraction = extraction;
    }

    /**
     *
     * @param versionFileDAO
     */
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

}
