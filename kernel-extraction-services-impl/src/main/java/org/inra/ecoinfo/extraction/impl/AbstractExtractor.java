package org.inra.ecoinfo.extraction.impl;

import java.util.List;
import java.util.Map;
import javax.persistence.Transient;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.IExtractionConfiguration;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.localization.IInternationalizable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AbstractExtractor.
 */
public abstract class AbstractExtractor implements IExtractor, IInternationalizable {

    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractExtractor.class.getName());
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.extraction.impl.messages";
    /**
     * The Constant EXTENSION_CSV @link(String).
     */
    protected static final String EXTENSION_CSV = ".csv";
    /**
     * The Constant EXTENSION_TXT @link(String).
     */
    protected static final String EXTENSION_TXT = ".txt";
    /**
     * The Constant EXTRACTION @link(String).
     */
    protected static final String EXTRACTION = "extraction";
    /**
     * The Constant FILE_SEPARATOR @link(String).
     */
    protected static final String FILE_SEPARATOR = System.getProperty("file.separator");
    /**
     * The Constant FILENAME_REMINDER @link(String).
     */
    protected static final String FILENAME_REMINDER = "recapitulatif_extraction";
    /**
     * The Constant SEPARATOR_TEXT @link(String).
     */
    protected static final String SEPARATOR_TEXT = "_";
    /**
     * The Constant SUFFIX_FILENAME_DEFAULT @link(String).
     */
    protected static final String SUFFIX_FILENAME_DEFAULT = "";
    /**
     * The configuration @link(Configuration).
     */
    protected IExtractionConfiguration configuration;
    /**
     * The localization manager @link(ILocalizationManager).
     */
    protected ILocalizationManager localizationManager;
    /**
     * The security context @link(IPolicyManager).
     */
    protected IPolicyManager policyManager;
    
    /**
     *
     */
    protected Extraction extraction;

    /**
     * Récupère les données en base en fonction des métadonnées de la demande
     * initiale. <br/>
     * En fonction des métadonnées, on peut avoir plusieurs types de requêtes à
     * la base, d'où plusieurs listes de résultats indexé dans une map.
     *
     * @param parameters the parameters
     * @throws BusinessException the business exception
     * @link(IParameter) the parameters
     */
    @Override
    public abstract void extract(IParameter parameters) throws BusinessException;
    /**
     * Gets the localization manager.
     *
     * @return the localization manager
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see
     * org.inra.ecoinfo.localization.IInternationalizable#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final IExtractionConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the security context.
     *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        return 500_000L;
    }

    /**
     * Extract datas.
     *
     * @param requestMetadatasMap the request metadatas map
     * @return the map
     * @throws BusinessException the business exception
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("rawtypes")
    protected abstract Map<String, List> extractDatas(Map<String, Object> requestMetadatasMap) throws BusinessException;

    /**
     * Prepare request metadatas.
     *
     * @param requestMetadatasMap the request metadatas map
     * @throws BusinessException the business exception
     * @link(Map<String,Object>) the request metadatas map
     */
    protected abstract void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException;

    @Override
    public void setExtraction(Extraction extraction) {
        this.extraction = extraction;
    }
}
