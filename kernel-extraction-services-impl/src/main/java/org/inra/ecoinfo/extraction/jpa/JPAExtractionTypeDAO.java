package org.inra.ecoinfo.extraction.jpa;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.extraction.ExtractionType;
import org.inra.ecoinfo.extraction.ExtractionType_;
import org.inra.ecoinfo.extraction.IExtractionTypeDAO;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class JPAExtractionTypeDAO.
 *
 * @author ptcherniati
 */
public class JPAExtractionTypeDAO extends AbstractJPADAO<ExtractionType> implements IExtractionTypeDAO {

    /**
     * Gets the.
     *
     * @param extraction the extraction
     * @return the extraction type
     * @see
     * org.inra.ecoinfo.extraction.IExtractionTypeDAO#get(org.inra.ecoinfo.extraction.config.impl.Extraction)
     */
    @Override
    public Optional<ExtractionType> get(final Extraction extraction) {
        return getByCode(Utils.createCodeFromString(extraction.getName()));
    }

    /**
     * Gets the all extraction type.
     *
     * @return the all extraction type
     * @see
     * org.inra.ecoinfo.extraction.IExtractionTypeDAO#getAllExtractionType()
     */
    @Override
    public List<ExtractionType> getAllExtractionType() {
        return getAll(ExtractionType.class);
    }

    /**
     * Gets the by code.
     *
     * @param code the chlorocode
     * @return the by code
     * @see
     * org.inra.ecoinfo.extraction.IExtractionTypeDAO#getByCode(java.lang.String)
     */
    @Override
    public Optional<ExtractionType> getByCode(final String code) {
        CriteriaQuery<ExtractionType> query = builder.createQuery(ExtractionType.class);
        Root<ExtractionType> et = query.from(ExtractionType.class);
        query.where(builder.equal(et.get(ExtractionType_.code), code));
        query.select(et);
        return getOptional(query);
    }
}
