package org.inra.ecoinfo.extraction;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.Transient;
import org.inra.ecoinfo.extraction.config.IExtractionConfiguration;
import org.inra.ecoinfo.extraction.config.IExtractorDAO;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.impl.DefaultMotivationFormatter;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The Class ConfigurationGeneratedExtractorDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class ConfigurationGeneratedExtractorDAO implements IExtractorDAO, ApplicationContextAware {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.extraction.impl.messages";
    /**
     * The Constant PROPERTY_MSG_UNAVAILABLE_DISPLAY @link(String).
     */
    private static final String PROPERTY_MSG_UNAVAILABLE_DISPLAY = "PROPERTY_MSG_UNAVAILABLE_DISPLAY";
    /**
     * The Constant PROPERTY_MSG_UNAVAILABLE_EXTRACTOR @link(String).
     */
    private static final String PROPERTY_MSG_UNAVAILABLE_EXTRACTOR = "PROPERTY_MSG_UNAVAILABLE_EXTRACTOR";
    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationGeneratedExtractorDAO.class.getName());
    /**
     * The application context @link(ApplicationContext).
     */
    private ApplicationContext applicationContext;
    /**
     * The configuration @link(Configuration).
     */
    protected IExtractionConfiguration configuration;
    /**
     * The display map @link(Map<String,Map<String,IOutputBuilder>>).
     */
    protected Map<String, Map<String, IOutputBuilder>> displayMap = new HashMap<>();
    /**
     * The extracteurs map @link(Map<String,IExtractor>).
     */
    protected Map<String, IExtractor> extracteursMap = new HashMap<>();
    protected Map<String, IMotivationFormatter> motivationFormatterMap = new HashMap<>();
    /**
     * The localization manager @link(ILocalizationManager).
     */
    protected ILocalizationManager localizationManager;

    /**
     * Gets the display by code and order.
     *
     * @param code the code
     * @param order the order
     * @return the display by code and order
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.extraction.impl.IExtractorDAO#getDisplayByCodeAndOrder(java.lang.String,
     * int)
     */
    @Override
    public IOutputBuilder getDisplayByCodeAndOrder(final String code, final int order) throws PersistenceException {
        final Map<String, IOutputBuilder> displayForCode = displayMap.get(code);
        if (displayForCode == null) {
            LOGGER.error(String.format(localizationManager.getMessage(ConfigurationGeneratedExtractorDAO.BUNDLE_SOURCE_PATH, ConfigurationGeneratedExtractorDAO.PROPERTY_MSG_UNAVAILABLE_EXTRACTOR), code));
            throw new PersistenceException(String.format(localizationManager.getMessage(ConfigurationGeneratedExtractorDAO.BUNDLE_SOURCE_PATH, ConfigurationGeneratedExtractorDAO.PROPERTY_MSG_UNAVAILABLE_EXTRACTOR), code));
        } else {
            final IOutputBuilder display = displayForCode.get(String.valueOf(order));
            if (display == null) {
                LOGGER.error(String.format(localizationManager.getMessage(ConfigurationGeneratedExtractorDAO.BUNDLE_SOURCE_PATH, ConfigurationGeneratedExtractorDAO.PROPERTY_MSG_UNAVAILABLE_DISPLAY), order));
                throw new PersistenceException(String.format(localizationManager.getMessage(ConfigurationGeneratedExtractorDAO.BUNDLE_SOURCE_PATH, ConfigurationGeneratedExtractorDAO.PROPERTY_MSG_UNAVAILABLE_DISPLAY), order));
            } else {
                return display;
            }
        }
    }

    /*
     * *
     * 
     * @see org.inra.ore.data.dao.IRecorderDAO#getRecorderByDataTypeName(java.lang .String)
     */
 /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.impl.IExtractorDAO#getExtractorByCode(java.lang.String)
     */
    /**
     *
     * @param code
     * @return
     * @throws PersistenceException
     */
    @Override
    public IExtractor getExtractorByCode(final String code) throws PersistenceException {
        final IExtractor extracteur = extracteursMap.get(code);
        if (extracteur == null) {
            LOGGER.error(String.format(localizationManager.getMessage(ConfigurationGeneratedExtractorDAO.BUNDLE_SOURCE_PATH, ConfigurationGeneratedExtractorDAO.PROPERTY_MSG_UNAVAILABLE_EXTRACTOR), code));
            throw new PersistenceException(String.format(localizationManager.getMessage(ConfigurationGeneratedExtractorDAO.BUNDLE_SOURCE_PATH, ConfigurationGeneratedExtractorDAO.PROPERTY_MSG_UNAVAILABLE_EXTRACTOR), code));
        } else {
            return extracteur;
        }
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public IMotivationFormatter getMotivationFormatter(final String code) {
        return motivationFormatterMap.getOrDefault(code, new DefaultMotivationFormatter());
    }

    /**
     * Inits the.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @SuppressWarnings("unchecked")
    public void init() throws BusinessException {
        for (final Extraction extraction : configuration.getExtractions()) {
            Class<IExtractor> extracterClass = null;
            IExtractor extractorInstance = null;
            initExtractor(extraction);
            initMotivationFormatter(extraction);
            initOutputDisplays(extraction);
        }
    }

    private void initExtractor(final Extraction extraction) throws BusinessException {
        IExtractor extractorInstance = null;
        try {
            extractorInstance = tryBeanName(extraction.getExtractor());
        } catch (final BeansException e2) {
            LOGGER.error("bean exception", e2);
            extractorInstance = tryClassname(extraction.getExtractor());
        }
        extracteursMap.put(extraction.getCode(), extractorInstance);
    }

    private void initMotivationFormatter(final Extraction extraction) throws BusinessException {
        if (extraction.getExtractionFormatter() == null) {
            return;
        }
        IMotivationFormatter motivationFormatter = null;
        try {
            motivationFormatter = tryBeanName(extraction.getExtractionFormatter());
        } catch (final BeansException e2) {
            LOGGER.error("bean exception", e2);
            motivationFormatter = tryClassname(extraction.getExtractionFormatter());
        }
        motivationFormatterMap.put(extraction.getCode(), motivationFormatter);
    }

    /*
     * *
     *
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext (org.springframework.context.ApplicationContext)
     */
 /*
     * *
     *
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext (org.springframework.context.ApplicationContext)
     */
 /*
     * (non-Javadoc)
     *
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    /**
     *
     * @param applicationContext
     */
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext
    ) {
        this.applicationContext = applicationContext;
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final IExtractionConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /*
     * *
     *
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext (org.springframework.context.ApplicationContext)
     */
    private void initOutputDisplays(final Extraction extraction) {
        IOutputBuilder displayInstance = null;
        IOutputBuilder localDisplayInstance = displayInstance;
        Class<IOutputBuilder> displayClass;
        for (final Entry<String, String> entryDAO : extraction.getOutputDisplays().entrySet()) {
            final Integer ordre = Integer.valueOf(entryDAO.getKey());
            try {
                localDisplayInstance = (IOutputBuilder) applicationContext.getBean(entryDAO.getValue());
            } catch (final BeansException e) {
                try {
                    displayClass = (Class<IOutputBuilder>) Class.forName(entryDAO.getValue());
                    localDisplayInstance = displayClass.newInstance();
                } catch (final ClassNotFoundException | InstantiationException | IllegalAccessException e2) {
                    UncatchedExceptionLogger.logUncatchedException(LOGGER, "uncatched exception in init", e2);
                }
                UncatchedExceptionLogger.logUncatchedException(LOGGER, "uncatched exception in init", e);
            }
            displayMap
                    .computeIfAbsent(extraction.getCode(), t -> new HashMap())
                    .put(String.valueOf(ordre), localDisplayInstance);
        }
    }

    private <T> T tryClassname(String classPath) throws BusinessException {
        T extractorInstance = null;
        try {
            extractorInstance = ((Class<T>) Class.forName(classPath)).newInstance();
        } catch (final ClassCastException ex) {
            LOGGER.error("%s n'est pas du bon type",classPath);
            throw new BusinessException(ex);
        } catch (final  InstantiationException | IllegalAccessException ex) {
            LOGGER.error("%s n'a pas de constructeur par défaut et ne peut être instancié", classPath);
            throw new BusinessException(ex);
        } catch (ClassNotFoundException ex) {
            LOGGER.error("%s n'est pas un chemin de classe valide", classPath);
            throw new BusinessException(ex);
        } 
        return extractorInstance;
    }

    private <T> T tryBeanName(String beanName) {
        T beanInstance = null;
        beanInstance = (T) applicationContext.getBean(beanName);
        return beanInstance;
    }

}
