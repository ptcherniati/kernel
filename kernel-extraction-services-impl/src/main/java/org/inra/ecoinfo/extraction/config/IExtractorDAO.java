package org.inra.ecoinfo.extraction.config;

import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IMotivationFormatter;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface IExtractorDAO.
 *
 * @author Philippe TCHERNIATINSKY
 */
public interface IExtractorDAO {

    /**
     * Gets the display by code and order.
     *
     * @param code the code
     * @param order the order
     * @return the display by code and order
     * @throws PersistenceException the persistence exception
     * @link(String) the code
     * @link(int) the order
     */
    IOutputBuilder getDisplayByCodeAndOrder(String code, int order) throws PersistenceException;

    /**
     * Gets the extractor by code.
     *
     * @param code the code
     * @return the extractor by code
     * @throws PersistenceException the persistence exception
     * @link(String) the code
     */
    IExtractor getExtractorByCode(String code) throws PersistenceException;

    /**
     *
     * @param code
     * @return
     */
    IMotivationFormatter getMotivationFormatter(final String code);
}
