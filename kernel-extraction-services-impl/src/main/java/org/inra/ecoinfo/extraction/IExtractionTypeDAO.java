package org.inra.ecoinfo.extraction;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.extraction.config.impl.Extraction;

/**
 * The Interface IExtractionTypeDAO.
 */
public interface IExtractionTypeDAO extends IDAO<ExtractionType> {

    /**
     * Gets the.
     *
     * @param extraction the extraction
     * @return the extraction type
     * @link(Extraction) the extraction
     */
    Optional<ExtractionType> get(Extraction extraction);

    /**
     * Gets the all extraction type.
     *
     * @return the all extraction type
     */
    List<ExtractionType> getAllExtractionType();

    /**
     * Gets the by code.
     *
     * @param extractionType
     * @return the by code
     * @link(String) the chloro pp transp extraction code
     */
    Optional<ExtractionType> getByCode(String extractionType);
}
