/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction.genericfilesrepository.impl;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.Tuple;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.dataset.versioning.IGenericDatatypeDAO;
import org.inra.ecoinfo.extraction.genericfilesrepository.api.IGenericDatatypeManager;
import org.inra.ecoinfo.mga.business.composite.FlatNode;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.viewadapter.ITreeBuilder;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.IntervalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptchernia
 */
public class GenericDatatypeManager extends MO implements IGenericDatatypeManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenericDatatypeManager.class);

    /**
     *
     */
    public final static String EXTRACT_CODE = "extraction_de_fichiers_bruts";
    private static final int DATYPE_GENERIC_CONFIGURATION = AbstractMgaIOConfigurator.GENERIC_DATATYPE_CONFIGURATION;
    IGenericDatatypeDAO genericDatatypeDAO;
    ITreeBuilder treeBuilder;
    IMgaIOConfigurator configuration;

    /**
     *
     * @param genericDatatypeDAO
     */
    public void setGenericDatatypeDAO(IGenericDatatypeDAO genericDatatypeDAO) {
        this.genericDatatypeDAO = genericDatatypeDAO;
    }

    /**
     *
     * @param periods
     * @return
     */
    @Override
    public Map<DataType, ITreeNode<FlatNode>> getDatatype(List<IntervalDate> periods, final Integer codeConfiguration) {
        List<Tuple> nodedatasetTuples = genericDatatypeDAO.getDatatype(periods);
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> rights = policyManager.getOrLoadActivities(policyManager.getCurrentUser(), WhichTree.TREEDATASET);
        Map<DataType, List<INode>> collectingDataset = new HashMap();
        nodedatasetTuples.stream()
                .forEach(
                        t -> collectingDataset.computeIfAbsent(t.get(3, DataType.class), k -> new LinkedList()).add((t.get(0, NodeDataSet.class)))
                );
        return collectingDataset.entrySet().stream()
                .collect(
                        Collectors.toMap(e -> e.getKey(), e -> buildTree(e.getValue(), rights, codeConfiguration)));
    }

    private ITreeNode<FlatNode> buildTree(List<INode> nodes, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> selectUserActivities, final Integer codeConfiguration) {
        IMgaIOConfiguration configuration = this.configuration.getConfiguration(codeConfiguration).orElse(this.configuration.getConfiguration(DATYPE_GENERIC_CONFIGURATION).get());
        ITreeNode buildSkeletonTree = treeBuilder.getApplicationCacheManager().buildSkeletonTree(configuration, nodes.stream(), false);
        Set<Long> nodesWithActivity = new HashSet();
        return treeBuilder.buildFlatTree(policyManager.getCurrentUser().getOwnGroup(WhichTree.TREEDATASET), buildSkeletonTree, selectUserActivities, null, "", nodesWithActivity, configuration);
    }

    /**
     *
     * @param treeBuilder
     */
    public void setTreeBuilder(ITreeBuilder treeBuilder) {
        this.treeBuilder = treeBuilder;
    }

    /**
     *
     * @param configuration
     */
    public void setConfiguration(IMgaIOConfigurator configuration) {
        this.configuration = configuration;
    }
}
