package org.inra.ecoinfo.extraction.deserialization;

/**
 * The Interface IIdSerialization.
 */
public interface IExtractionDeserialization {

    /**
     *
     * @return
     */
    String getId();
}
