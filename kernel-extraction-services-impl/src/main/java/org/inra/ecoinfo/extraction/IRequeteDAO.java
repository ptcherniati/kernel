package org.inra.ecoinfo.extraction;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.extraction.entity.Requete;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface IRequeteDAO.
 */
public interface IRequeteDAO extends IDAO<Requete> {

    /**
     * Delete requests from ids.
     *
     * @param ids the ids
     * @throws PersistenceException the persistence exception
     * @link(Long[]) the ids
     */
    void deleteRequestsFromIds(Long[] ids) throws PersistenceException;

    /**
     * Gets the all by login and extraction type name.
     *
     * @param utilisateurId the utilisateur id
     * @param extractionTypeId the extraction type id
     * @return the all by login and extraction type name
     * @link(Long) the utilisateur id
     * @link(Long) the extraction type id
     */
    List<Requete> getAllByLoginAndExtractionTypeName(Long utilisateurId, Long extractionTypeId);

    /**
     * Gets the by id.
     *
     * @param id the id
     * @return the by id
     * @link(Long) the id
     */
    Optional<Requete> getById(Long id);

    /**
     * Gets the extractiontypes results availables.
     *
     * @param utilisateurId the utilisateur id
     * @return the extractiontypes results availables
     * @link(Long) the utilisateur id
     */
    List<ExtractionType> getExtractiontypesResultsAvailables(Long utilisateurId);
}
