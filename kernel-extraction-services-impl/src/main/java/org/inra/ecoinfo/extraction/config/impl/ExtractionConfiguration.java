package org.inra.ecoinfo.extraction.config.impl;

import java.util.LinkedList;
import java.util.List;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.AbstractConfiguration;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.ExtractionType;
import org.inra.ecoinfo.extraction.IExtractionTypeDAO;
import org.inra.ecoinfo.extraction.config.IExtractionConfiguration;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class ExtractionConfiguration.
 */
public class ExtractionConfiguration extends AbstractConfiguration implements IExtractionConfiguration {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/extractionConfiguration.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "extractionConfiguration";

    private static final String REPOSITORY_PATTERN = "%s/extraction/.";
    /**
     * The extraction type dao.
     */
    private IExtractionTypeDAO extractionTypeDAO;
    /**
     * The extractions.
     */
    private List<Extraction> extractions = new LinkedList<>();
    private ICoreConfiguration coreConfiguration;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.config.IExtractionConfiguration#addExtraction(org.inra.ecoinfo.extraction.config.impl.Extraction)
     */
    /**
     *
     * @param extraction
     */
    @Override
    public void addExtraction(Extraction extraction) {
        if (!this.extractions.contains(extraction)) {
            extractions.add(extraction);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#createConfig(org.apache.commons.digester.Digester)
     */
    @Override
    public void createConfig(Digester digester) {
        digester.addObjectCreate("configuration/module/extractionConfiguration/extraction", Extraction.class);
        digester.addSetNext("configuration/module/extractionConfiguration/extraction", "addExtraction");
        digester.addCallMethod("configuration/module/extractionConfiguration/extraction/name", "addInternationalizedName", 2, newStringClassArray());
        digester.addCallParam("configuration/module/extractionConfiguration/extraction/name", 0, "language");
        digester.addObjectCreate("configuration/module/extractionConfiguration/extraction/name", String.class);
        digester.addCallMethod("configuration/module/extractionConfiguration/extraction/name", "concat", 0);
        digester.addCallParam("configuration/module/extractionConfiguration/extraction/name", 1, false);
        digester.addCallMethod("configuration/module/extractionConfiguration/extraction/description", "addInternationalizedDescription", 2, newStringClassArray());
        digester.addCallParam("configuration/module/extractionConfiguration/extraction/description", 0, "language");
        digester.addObjectCreate("configuration/module/extractionConfiguration/extraction/description", String.class);
        digester.addCallMethod("configuration/module/extractionConfiguration/extraction/description", "concat", 0);
        digester.addCallParam("configuration/module/extractionConfiguration/extraction/description", 1, false);
        digester.addCallMethod("configuration/module/extractionConfiguration/extraction/extractor", "setExtractor", 0);
        digester.addCallMethod("configuration/module/extractionConfiguration/extraction/outputDisplay", "addOutputDisplay", 2, newIntegerClassArray());
        digester.addCallParam("configuration/module/extractionConfiguration/extraction/outputDisplay", 0, "number");
        digester.addObjectCreate("configuration/module/extractionConfiguration/extraction/outputDisplay", StringBuilder.class);
        digester.addCallMethod("configuration/module/extractionConfiguration/extraction/outputDisplay/class", "append", 0);
        digester.addCallParam("configuration/module/extractionConfiguration/extraction/outputDisplay", 1, true);
        digester.addCallMethod("configuration/module/extractionConfiguration/extraction/extractionFormatter", "setExtractionFormatter", 0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.config.IExtractionConfiguration#getExtractions()
     */
    /**
     *
     * @return
     */
    @Override
    public List<Extraction> getExtractions() {
        return extractions;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.config.IExtractionConfiguration#setExtractions(java.util.List)
     */
    /**
     *
     * @param extractions
     */
    @Override
    public void setExtractions(List<Extraction> extractions) {
        this.extractions = extractions;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getModuleName()
     */
    @Override
    public String getModuleName() {
        return ExtractionConfiguration.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getSchemaPath()
     */
    @Override
    public String getSchemaPath() {
        return ExtractionConfiguration.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#isAutoLoad()
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     * Post config.
     *
     * @throws BusinessException the business exception
     */
    @Override
    public void postConfig() throws BusinessException {
        updateExtractionType();
        createRepositoryIfNeeded();
    }

    /**
     * Sets the extraction type dao.
     *
     * @param extractionTypeDAO the new extraction type dao
     */
    public void setExtractionTypeDAO(IExtractionTypeDAO extractionTypeDAO) {
        this.extractionTypeDAO = extractionTypeDAO;
    }

    /**
     *
     * @param coreConfiguration
     */
    public void setCoreConfiguration(ICoreConfiguration coreConfiguration) {
        this.coreConfiguration = coreConfiguration;
    }

    private void createRepositoryIfNeeded() {
        String repositoryFiles = String.format(REPOSITORY_PATTERN, coreConfiguration.getRepositoryURI());
        FileWithFolderCreator.createFile(repositoryFiles + "/.");
    }

    private void updateExtractionType() throws BusinessException {
        for (final Extraction extraction : getExtractions()) {
            if (!extractionTypeDAO.get(extraction).isPresent()) {
                try {
                    final ExtractionType extractionType = new ExtractionType(extraction);
                    extractionTypeDAO.saveOrUpdate(extractionType);
                } catch (final PersistenceException e) {
                    throw new BusinessException(e.getMessage(), e);
                }
            }
        }
    }
}
