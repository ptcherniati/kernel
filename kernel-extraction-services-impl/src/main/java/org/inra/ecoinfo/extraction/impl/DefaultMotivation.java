/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction.impl;

import org.inra.ecoinfo.extraction.IMotivation;

/**
 *
 * @author ptcherniati
 */
public class DefaultMotivation implements IMotivation{
    private static final InheritableThreadLocal<IMotivation> INHERITABLE_THREAD_LOCAL = new InheritableThreadLocal<IMotivation>();

    /**
     *
     * @return
     */
    public static InheritableThreadLocal getLocalThread(){
        return INHERITABLE_THREAD_LOCAL;
    }

    /**
     *
     */
    public String motivation;

    /**
     *
     */
    public DefaultMotivation() {
    }

    /**
     *
     * @param string
     */
    public DefaultMotivation(String motivation) {
        this.motivation = motivation;
    }

    /**
     *
     * @return
     */
    public String getMotivation() {
        return motivation;
    }

    /**
     *
     * @param motivation
     */
    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "DefaultMotivation{" + motivation+ '}';
    }
    
}
