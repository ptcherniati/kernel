/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction.genericfilesrepository.impl;

import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;

/**
 *
 * @author ptchernia
 */
public class GenericDatatypeParameter  extends DefaultParameter implements IParameter  {

    /**
     *
     * @param map
     * @param parameters
     */
    public GenericDatatypeParameter(Map<String, Object> parameters) {
        this.setParameters(parameters);
    }

    @Override
    public String getExtractionTypeCode() {
        return GenericDatatypeManager.EXTRACT_CODE;
    }
    
}
