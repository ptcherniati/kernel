package org.inra.ecoinfo.extraction.serialization.impl;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IRequeteDAO;
import org.inra.ecoinfo.extraction.entity.Requete;
import org.inra.ecoinfo.extraction.serialization.IExtractionSerialization;
import org.inra.ecoinfo.serialization.AbstractSerialization;
import org.inra.ecoinfo.serialization.impl.AbstractActivitySerialization;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MailAdminSerialization.
 */
public class ExtractionSerialization extends AbstractSerialization implements IExtractionSerialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/extractionSerialization.xsd";

    /**
     *
     */
    public static final String EXTRACTION_CSV = "extraction/extraction.csv";

    /**
     *
     */
    public static final String LINE_PROPERTY_CSV_FILE = ";%s;%s";

    /**
     *
     */
    public static final String FILE_NAME_PATTERN = "%s.csv";

    /**
     *
     */
    public static final String EXTRACTION_ZIP_ENTRY_PATTERN = "extraction/%1$s/%1$s_%2$s.zip";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "extractionSerialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractActivitySerialization.class.getName());
    private String id;
    /**
     * The mail admin.
     */
    private IRequeteDAO requeteDAO;
    private IExtractionManager extractionManager;

    /**
     *
     * @param extractionManager
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param requeteDAO
     */
    public void setRequeteDAO(IRequeteDAO requeteDAO) {
        this.requeteDAO = requeteDAO;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#createConfig(org.apache.commons.digester.Digester)
     */
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("serialization/module/extractionSerialization/id", "setId", 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#getModuleName()
     */
    /**
     *
     * @return
     */
    @Override
    public String getModuleName() {
        return ExtractionSerialization.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.AbstractSerialization#getSchemaPath()
     */
    /**
     *
     * @return
     */
    @Override
    public String getSchemaPath() {
        return ExtractionSerialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#isAutoLoad()
     */
    /**
     *
     * @return
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param outZip
     * @throws BusinessException
     */
    @Override
    public void serialize(ZipOutputStream outZip) throws BusinessException {
        List<Requete> requetes = new LinkedList<>();
        try {
            requetes = requeteDAO.getAll(Requete.class);
            List<String> lines = requetes.stream()
                    .map(requete -> writeLineEntry(requete, outZip))
                    .collect(Collectors.toList());
            writeExtractionInformations(outZip, lines);
        } catch (IOException ex) {
            LOGGER.error("cant'write activities", ex);
        }
    }

    private void writeExtractionInformations(ZipOutputStream outZip, List<String> lines) throws IOException {
        outZip.putNextEntry(new ZipEntry(EXTRACTION_CSV));
        lines.stream()
                .filter(line->line!=null)
                .forEach(line -> {
                    try {
                        outZip.write(line.getBytes());
                    } catch (IOException ex) {
                        LOGGER.error(String.format("can't write extraction line %s",line), ex);
                    }
                });
        outZip.closeEntry();
    }

    /**
     *
     * @param requete
     * @param outZip
     * @return 
     * @throws IOException
     */
    public String writeLineEntry(Requete requete, ZipOutputStream outZip) {
        String nomFichier = requete.getNomFichier();
        //"login;resource_path;role;activity_type;debut;fin"
        String line = String.format(LINE_CSV_FILE,
                requete.getId(),
                requete.getUtilisateur().getLogin(),
                requete.getExtractionType().getCode(), nomFichier,
                DateUtil.getUTCDateTextFromLocalDateTime(requete.getDate(), DateUtil.DD_MM_YYYY_HH_MM),
                requete.getDureeExtraction(),
                requete.getCommentaire()
        );
        try {
            byte[] data = extractionManager.doDownload(requete);
            writeEntry(outZip, requete.getExtractionType().getCode(), nomFichier, data);

            return line;
        } catch (BusinessException | IOException ex) {
            LOGGER.error(String.format("Can't get file data for extraction type %s and file name %s", requete.getExtractionType().getCode(), nomFichier), ex);
            return null;
        }
    }

    /**
     *
     * @param outZip
     * @param extractionType
     * @param fileName
     * @param extractionCode
     * @param data
     * @throws IOException
     */
    public void writeEntry(ZipOutputStream outZip, String extractionType, String fileName, byte[] data) throws IOException {
        ZipEntry extractionFile = new ZipEntry(String.format(EXTRACTION_ZIP_ENTRY_PATTERN, extractionType, fileName));
        outZip.putNextEntry(extractionFile);
        outZip.write(data);
        outZip.closeEntry();
    }
}
