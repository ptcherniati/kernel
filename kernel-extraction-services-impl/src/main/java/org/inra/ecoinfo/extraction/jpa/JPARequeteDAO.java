/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.extraction.jpa;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.extraction.ExtractionType;
import org.inra.ecoinfo.extraction.ExtractionType_;
import org.inra.ecoinfo.extraction.IRequeteDAO;
import org.inra.ecoinfo.extraction.entity.Requete;
import org.inra.ecoinfo.extraction.entity.Requete_;
import org.inra.ecoinfo.identification.entity.Utilisateur_;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class JPARequeteDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class JPARequeteDAO extends AbstractJPADAO<Requete> implements IRequeteDAO {

    /**
     * Delete requests from ids.
     *
     * @param ids the ids
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.extraction.IRequeteDAO#deleteRequestsFromIds(java.lang.Long[])
     */
    @Override
    public void deleteRequestsFromIds(final Long[] ids) throws PersistenceException {
        try {

            CriteriaDelete<Requete> delete = builder.createCriteriaDelete(Requete.class);
            Root<Requete> rq = delete.from(Requete.class);
            delete.where(rq.<Long>get(Requete_.id).in(Arrays.asList(ids)));
            delete(delete);
        } catch (final Exception e) {
            UncatchedExceptionLogger.logUncatchedException(LOGGER, "uncatched exception in deleteRequestsFromIds", e);
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Gets the all by login and extraction type name.
     *
     * @param utilisateurId the utilisateur id
     * @param extractionTypeId the extraction type id
     * @return the all by login and extraction type name
     * @see
     * org.inra.ecoinfo.extraction.IRequeteDAO#getAllByLoginAndExtractionTypeName(java.lang.Long,
     * java.lang.Long)
     */
    @Override
    public List<Requete> getAllByLoginAndExtractionTypeName(final Long utilisateurId, final Long extractionTypeId) {
        CriteriaQuery<Requete> query = builder.createQuery(Requete.class);
        Root<Requete> rq = query.from(Requete.class);
        query.where(
                builder.equal(rq.join(Requete_.utilisateur).get(Utilisateur_.id), utilisateurId),
                builder.equal(rq.join(Requete_.extractionType).get(ExtractionType_.id), extractionTypeId)
        );
        query
                .select(rq)
                .orderBy(
                        builder.asc(rq.get(Requete_.EXTRACTION_TYPE)), 
                        builder.desc(rq.get(Requete_.date)));
        return getResultList(query);
    }

    /**
     * Gets the by id.
     *
     * @param id the id
     * @return the by id
     * @see org.inra.ecoinfo.extraction.IRequeteDAO#getById(java.lang.Long)
     */
    @Override
    public Optional<Requete> getById(final Long id) {
        return getById(Requete.class, id);
    }

    /**
     * Gets the extractiontypes results availables.
     *
     * @param utilisateurId the utilisateur id
     * @return the extractiontypes results availables
     * @see
     * org.inra.ecoinfo.extraction.IRequeteDAO#getExtractiontypesResultsAvailables(java.lang.Long)
     */
    @Override
    public List<ExtractionType> getExtractiontypesResultsAvailables(final Long utilisateurId) {
        CriteriaQuery<ExtractionType> query = builder.createQuery(ExtractionType.class);
        Root<Requete> rq = query.from(Requete.class);
        query.where(
                builder.equal(rq.join(Requete_.utilisateur).get(Utilisateur_.id), utilisateurId)
        );
        query
                .distinct(true)
                .select(rq.join(Requete_.extractionType)
                );
        return getResultList(query);
    }
}
