/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction.genericfilesrepository.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.extraction.genericfilesrepository.api.IGenericDatatypeManager;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author tcherniatinsky
 */
public class GenericDatatypeExtractionIntegrator extends AbstractIntegrator<VersionFile> {

    
    private static final String GENERIC_DATATYPE_BASE = "/raw_files";
    private static final String TMPTEMPORARY_VERSION_FILES = GENERIC_DATATYPE_BASE+"/%s/%s_%s_%s.%s";

    /**
     *
     */
    protected IGenericDatatypeManager genericDatatypeManager;

    /**
     *
     * @return
     */
    @Override
    public String getMapEntryKey() {
        return VersionFile.class.getSimpleName();
    }

    /**
     *
     * @param zipOutputStream
     * @param versionFiles
     * @throws IOException
     */
    @Override
    public void embed(ZipOutputStream zipOutputStream, Collection<VersionFile> versionFiles) throws IOException {
        Set<VersionFile> files = new HashSet();
        files.addAll(Optional.ofNullable(versionFiles).orElse(new HashSet()));
        if (!CollectionUtils.isEmpty(files)) {
            List<WrappedByteArray> wrappedByteArrays = files.stream()
                    .map(versionFile -> new WrappedByteArray(versionFile.getData(), getPathesForVersion(versionFile)))
                    .collect(Collectors.toList());
            writeFiles(zipOutputStream, wrappedByteArrays);
        }
    }

    private String getPathesForVersion(VersionFile versionFile) {
        String path = Optional.ofNullable(versionFile)
                .map(v->v.getDataset())
                .map(d->d.getRealNode())
                .map(n->n.getPath().replaceAll(",", "/"))
                .orElse("nodatatype");
        return String.format(TMPTEMPORARY_VERSION_FILES,  
                path,
                versionFile.getFileName(), 
                DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateDebutPeriode(), DateUtil.DD_MM_YYYY_FILE),  
                DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateFinPeriode(), DateUtil.DD_MM_YYYY_FILE), 
                versionFile.getExtention());
    }

    /**
     *
     * @param genericDatatypeManager
     */
    public void setGenericDatatypeManager(IGenericDatatypeManager genericDatatypeManager) {
        this.genericDatatypeManager = genericDatatypeManager;
    }
}
