package org.inra.ecoinfo.extraction.impl;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Class DefaultParameter.
 */
public class DefaultParameter {

    /**
     * The commentaire @link(String).
     */
    private String commentaire = "";
    /**
     * The files maps @link(List<Map<String,File>>).
     */
    private final List<Map<String, File>> filesMaps = new LinkedList<>();
    /**
     * The results @link(Map<String,Map<String,List>>).
     */
    @SuppressWarnings("rawtypes")
    private final Map<String, Map<String, List>> results = new HashMap<>();
    /**
     * The parameters @link(Map<String,Object>).
     */
    Map<String, Object> parameters = new HashMap<>();

    /**
     * Instantiates a new default parameter.
     */
    public DefaultParameter() {
        super();
    }

    /**
     * Gets the commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets the commentaire.
     *
     * @param commentaire the new commentaire
     */
    public void setCommentaire(final String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     * Gets the files maps.
     *
     * @return the files maps
     */
    public List<Map<String, File>> getFilesMaps() {
        return filesMaps;
    }

    /**
     * Gets the parameters.
     *
     * @return the parameters
     */
    public Map<String, Object> getParameters() {
        return parameters;
    }

    /**
     * Sets the parameters.
     *
     * @param parameters the parameters
     * @link(Map<String,Object>) the parameters
     */
    public void setParameters(final Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    /**
     * Gets the results.
     *
     * @return the results
     */
    @SuppressWarnings("rawtypes")
    public Map<String, Map<String, List>> getResults() {
        return results;
    }
}
