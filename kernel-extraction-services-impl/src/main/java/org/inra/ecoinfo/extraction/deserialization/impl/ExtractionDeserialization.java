package org.inra.ecoinfo.extraction.deserialization.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.deserialization.AbstractDeserialization;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IExtractionTypeDAO;
import org.inra.ecoinfo.extraction.IRequeteDAO;
import org.inra.ecoinfo.extraction.deserialization.IExtractionDeserialization;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;

/**
 * The Class MailAdminDeserialization.
 */
public class ExtractionDeserialization extends AbstractDeserialization implements IExtractionDeserialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/extractionDeserialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "extractionDeserialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(ExtractionDeserialization.class.getName());

    /**
     *
     */
    public static final String EXTRACTION_ENTRY = "extraction";

    /**
     *
     */
    public static final String HEADER_CSV_FILE = "id;active;email;emploi;isroot;language;login;nom;password;poste;prenom";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    /**
     *
     */
    public static final String START_FILE_NAME_PATTERN = String.format("%s%s", EXTRACTION_ENTRY, File.separator);

    /**
     *
     */
    public static final String PATTERN_CONCAT_WORDS = "%s%s";

    /**
     *
     */
    public static final String EXTRACTION_CSV = String.format(PATTERN_CONCAT_WORDS, START_FILE_NAME_PATTERN, "extraction.csv");
    private String id;
    /**
     * The mail admin.
     */
    private IUtilisateurDAO utilisateurDAO;
    private ICoreConfiguration configuration;

    /**
     *
     */
    protected IExtractionManager extractionManager;
    private IPolicyManager policyManager;

    /**
     *
     */
    protected IRequeteDAO requeteDAO;

    /**
     *
     */
    protected IExtractionTypeDAO extractionTypeDAO;

    /**
     *
     * @param requeteDAO
     */
    public void setRequeteDAO(IRequeteDAO requeteDAO) {
        this.requeteDAO = requeteDAO;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param extractionManager
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     *
     * @param configuration
     */
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }
    
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        super.createConfig(digester);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#getModuleName()
     */
    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.AbstractDeserialization#getSchemaPath()
     */
    @Override
    public String getSchemaPath() {
        return SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleDeserialization#isAutoLoad()
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void deSerialize(File inZip) throws BusinessException {
        TransactionStatus tr = utilisateurDAO.beginDefaultTransaction();
        try {
            init(policyManager);
            List<String> lines = readCSV(inZip);
            for (String line : lines) {
                writeFile(line, inZip);
            }
            utilisateurDAO.commitTransaction(tr);

        } catch (PersistenceException ex) {
            utilisateurDAO.rollbackTransaction(tr);
            throw new BusinessException("no root user", ex);
        } catch (IOException ex) {
            LOGGER.error("can't write requete ", ex);
            utilisateurDAO.rollbackTransaction(tr);
        }

    }

    private void writeFile(String attachment, File inZip) throws FileNotFoundException, IOException {
        String outFileName = String.format(PATTERN_CONCAT_WORDS, configuration.getRepositoryURI(), attachment);
        try (
                final FileInputStream fis = new FileInputStream(inZip);
                final ZipInputStream zis = new ZipInputStream(fis);
                final FileOutputStream outStream = FileWithFolderCreator.createOutputStream(outFileName);) {
            ZipEntry ze = zis.getNextEntry();
            byte[] buffer = new byte[1_024];
            while (ze != null) {
                String fileName = ze.getName();
                if (fileName.equals(attachment)) {
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        outStream.write(buffer, 0, len);
                    }
                    outStream.close();
                    break;
                }
                ze = zis.getNextEntry();
            }
        } catch(Exception e) {
            throw new IOException(String.format("cant' write attachment %s in %s", attachment, inZip.getName()), e);
        }
    }

    /**
     *
     * @param inZip
     * @return
     * @throws BusinessException
     */
    public List<String> readCSV(File inZip) throws BusinessException {
        List<String> lines = new LinkedList();
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(inZip), StandardCharsets.UTF_8);) {
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                String entryName = zipEntry.getName();
                if (entryName.equals(EXTRACTION_CSV)) {
                    Scanner sc = new Scanner(zis, StandardCharsets.UTF_8.name());
                    String line;
                    int i = 0;
                    while (sc.hasNextLine()) {
                        i++;
                        line = sc.nextLine();
                        if (i == 1) {
                            continue;
                        }
                        lines.add(line);
                    }
                    zis.close();
                    break;
                }
                zipEntry = zis.getNextEntry();
            }
            return lines;
        } catch (IOException ex) {
            throw new BusinessException(String.format("can't write file %s", inZip.getAbsolutePath()), ex);
        }
    }

    private void init(IPolicyManager policyManager) throws BusinessException, PersistenceException {
        Utilisateur rootUtilisateur = utilisateurDAO.getAdmins().get(0);
        policyManager.setCurrentUser(rootUtilisateur);
    }
}
