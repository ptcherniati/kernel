package org.inra.ecoinfo.extraction.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.zip.ZipOutputStream;
import javax.persistence.Transient;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.config.IExtractionConfiguration;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.IInternationalizable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.StreamUtils;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * The Class AbstractOutputBuilder.
 */
public abstract class AbstractOutputBuilder implements IOutputBuilder, IInternationalizable {

    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractOutputBuilder.class.getName());
    /**
     * /**
     * The Constant EXTENSION_CSV @link(String).
     */
    protected static final String EXTENSION_CSV = ".csv";
    /**
     * The Constant EXTENSION_TXT @link(String).
     */
    protected static final String EXTENSION_TXT = ".txt";
    /**
     * The Constant EXTRACTION @link(String).
     */
    protected static final String EXTRACTION = "extraction";
    /**
     * The Constant FILE_SEPARATOR @link(String).
     */
    protected static final String FILE_SEPARATOR = System.getProperty("file.separator");
    /**
     * The Constant FILENAME_REMINDER @link(String).
     */
    protected static final String FILENAME_REMINDER = "recapitulatif_extraction";
    /**
     * The Constant MAP_INDEX_0 @link(String).
     */
    protected static final String MAP_INDEX_0 = "0";
    /**
     * The Constant SEPARATOR_TEXT @link(String).
     */
    protected static final String SEPARATOR_TEXT = "_";
    /**
     * The Constant SUFFIX_FILENAME_DEFAULT @link(String).
     */
    protected static final String SUFFIX_FILENAME_CSV = ".csv";
    protected static final String SUFFIX_FILENAME_TXT = ".txt";
    protected static final String SUFFIX_FILENAME_ZIP = ".zip";
    private static final int MILLIARD = 1_000_000_000;
    /**
     * The Constant EXTENSION_ZIP.
     */
    static final String EXTENSION_ZIP = ".zip";
    /**
     * The configuration @link(Configuration).
     */
    protected ICoreConfiguration configuration;

    /**
     *
     */
    protected IExtractionConfiguration extractionConfiguration;
    /**
     * The datatype dao @link(IDatatypeDAO).
     */
    protected IDatatypeDAO datatypeDAO;
    /**
     * The localization manager @link(ILocalizationManager).
     */
    protected ILocalizationManager localizationManager;
    /**
     * The security context @link(IPolicyManager).
     */
    protected IPolicyManager policyManager;
    List<AbstractIntegrator> integrators = new LinkedList();

    /**
     * Builds the output.*      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *
     * @param parameters the parameters
     * @param cstResultExtractionCode the cst result extraction code
     *      * @return the map
     *      * @throws BusinessException the business exception
     * @return 
     * @link(IParameter) the parameters
     * @link(String) the cst result extraction code
     */
    @SuppressWarnings("rawtypes")
    public Map<String, File> buildOutput(final IParameter parameters, final String cstResultExtractionCode) throws BusinessException {
            Optional.ofNullable(policyManager)
                    .map(policyManager -> (Utilisateur) policyManager.getCurrentUser())
                    .ifPresent(utilisateur -> {
                        MDC.put("extract.user.name", utilisateur.getNom());
                        MDC.put("extract.user.login", utilisateur.getLogin());
                        MDC.put("extract.user.surname", utilisateur.getPrenom());
                        MDC.put("extract.user.email", utilisateur.getEmail());
                    });
        assert parameters.getResults() != null : "Empty results ";
        final Map<String, List> results = parameters.getResults().get(cstResultExtractionCode);
        if (results != null && results.get(AbstractOutputBuilder.MAP_INDEX_0).isEmpty()) {
            throw new NoExtractionResultException(localizationManager.getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, NoExtractionResultException.ERROR));
        }
        final String resultsHeader = buildHeader(parameters.getParameters());
        return buildBody(resultsHeader, results, parameters.getParameters());
    }

    /**
     * Gets the localization manager.
     *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *
     * @return the localization manager
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     * Sets the localization manager.
     *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *
     * @param localizationManager the new localization manager
     *      * @see
     * org.inra.ecoinfo.localization.IInternationalizable#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the configuration.
     *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *
     * @param configuration the new configuration
     */
    public void setConfiguration(final ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param extractionConfiguration
     */
    public void setExtractionConfiguration(IExtractionConfiguration extractionConfiguration) {
        this.extractionConfiguration = extractionConfiguration;
    }

    /**
     * Sets the datatype dao.
     *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *
     * @param datatypeDAO the new datatype dao
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Sets the security context.
     *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Builds the body.
     *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *
     * @param headers the headers
     * @param resultsDatasMap the results datas map
     * @param requestMetadatasMap the request metadatas map
     *      * @return the map
     *      * @throws BusinessException the business exception
     * @link(String) the headers
     * @link(Map<String,List>) the results datas map
     * @link(Map<String,Object>) the request metadatas map
     */
    @SuppressWarnings("rawtypes")
    protected abstract Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws org.inra.ecoinfo.utils.exceptions.BusinessException;

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    protected abstract String buildHeader(Map<String, Object> requestMetadatasMap) throws org.inra.ecoinfo.utils.exceptions.BusinessException;

    /**
     * Builds the output file.
     *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *
     * @param filename the filename
     * @param extension the extension
     *      * @return the file
     * @link(String) the filename
     * @link(String) the extension
     */
    protected File buildOutputFile(final String filename, final String extension) {
        final String extractionPath = configuration.getRepositoryURI().concat(AbstractOutputBuilder.FILE_SEPARATOR).concat(AbstractOutputBuilder.EXTRACTION).concat(AbstractOutputBuilder.FILE_SEPARATOR);
        final String fileRandomSuffix = Long.toString(Instant.now().toEpochMilli()).concat("-").concat(Double.toString(Math.random() * 1_000_000).substring(0, 6));
        return new File(extractionPath.concat(filename).concat(AbstractOutputBuilder.SEPARATOR_TEXT).concat(AbstractOutputBuilder.SEPARATOR_TEXT).concat(fileRandomSuffix).concat(extension));
    }

    /**
     * Builds the output print stream map.
     *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *
     * @param filesMap the files map
     *      * @return the map
     *      * @throws BusinessException the business exception
     * @link(Map<String,File>) the files map
     */
    protected Map<String, PrintStream> buildOutputPrintStreamMap(final Map<String, File> filesMap) throws BusinessException {
        final Map<String, PrintStream> outputPrintStreamMap = new HashMap<>();
        try {
            for (final Entry<String, File> filenameEntry : filesMap.entrySet()) {
                outputPrintStreamMap.put(filenameEntry.getKey(), new PrintStream(filesMap.get(filenameEntry.getKey()), Utils.ENCODING_ISO_8859_15));
            }
        } catch (final FileNotFoundException e) {
            LOGGER.error("FileNotFoundException", e);
            throw new org.inra.ecoinfo.utils.exceptions.BusinessException("FileNotFoundException", e);
        } catch (final UnsupportedEncodingException e) {
            LOGGER.error("UnsupportedEncodingException", e);
            throw new org.inra.ecoinfo.utils.exceptions.BusinessException("UnsupportedEncodingException", e);
        }
        return outputPrintStreamMap;
    }

    /**
     * Builds the outputs files.
     *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *
     * @param filesNames the files names
     * @param suffix the suffix
     *      * @return the map
     * @link(Set<String>) the files names
     * @link(String) the suffix
     */
    protected Map<String, File> buildOutputsFiles(final Set<String> filesNames, final String suffix) {
        final Map<String, File> filesMaps = new TreeMap<>();
        final String extractionPath = configuration.getRepositoryURI().concat(AbstractOutputBuilder.FILE_SEPARATOR).concat(AbstractOutputBuilder.EXTRACTION).concat(AbstractOutputBuilder.FILE_SEPARATOR);
        filesNames.stream().forEach((filename) -> {
            String fileRandomSuffix = Long.toString(Instant.now().toEpochMilli()).concat("-").concat(Double.toString(Math.random() * 1_000_000).substring(0, 6));
            fileRandomSuffix = fileRandomSuffix.replaceAll("\\.$", "0");
            final File resultFile = FileWithFolderCreator.createFile(extractionPath.concat(filename).concat(AbstractOutputBuilder.SEPARATOR_TEXT).concat(suffix).concat(AbstractOutputBuilder.SEPARATOR_TEXT).concat(fileRandomSuffix)
                    .concat(AbstractOutputBuilder.EXTENSION_CSV));
            filesMaps.put(filename, resultFile);
        });
        return filesMaps;
    }

    /**
     * Close streams.
     *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *
     * @param outputPrintStreamMap the output print stream map
     * @link(Map<String,PrintStream>) the output print stream map
     */
    protected void closeStreams(final Map<String, PrintStream> outputPrintStreamMap) {
        outputPrintStreamMap.values().stream().map((printStream) -> {
            printStream.flush();
            return printStream;
        }).forEach((printStream) -> {
            StreamUtils.closeStream(printStream);
        });
    }

    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        try {
            RObuildZipOutputStream rObuildZipOutputStream = buildZipOutputStream(parameters.getExtractionTypeCode());
            final Optional<List<AbstractIntegrator>> opt = Optional.ofNullable(integrators);
            if (opt.isPresent()) {
                for (AbstractIntegrator integrator : opt.get()) {
                    Collection collection = (Collection) parameters.getParameters().getOrDefault(integrator.getMapEntryKey(), new LinkedList());
                    integrator.embed(rObuildZipOutputStream.getZipOutputStream(), collection);
                };
            }
            return rObuildZipOutputStream;
        } catch (FileNotFoundException e1) {
            throw new BusinessException("FileNotFoundException", e1);
        } catch (IOException e) {
            throw new BusinessException("IOException", e);
        }
    }

    /**
     * Builds the zip output stream.
     *
     *
     *
     * @param datatypeCode
     * @link(String) the datatype code
     *
     * @return the r obuild zip output stream
     *
     * @throws FileNotFoundException
     *             the file not found exception
     * @throws BusinessException the business exception
     * @link(String) the datatype code
     */
    protected RObuildZipOutputStream buildZipOutputStream(final String datatypeCode) throws BusinessException, FileNotFoundException {
        final String extractionPath = this.configuration.getRepositoryURI().concat(FILE_SEPARATOR).concat(EXTRACTION).concat(FILE_SEPARATOR);
        final Instant now = Instant.now();
        final String fileRandomSuffix = Long.toString(now.toEpochMilli()).concat("-").concat(Double.toString(Math.random() * MILLIARD).substring(0, 6));
        final File resultFile = FileWithFolderCreator.createFile(extractionPath.concat(datatypeCode).concat(FILE_SEPARATOR).concat(datatypeCode).concat(SEPARATOR_TEXT).concat(fileRandomSuffix).concat(EXTENSION_ZIP));
        final ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(resultFile)));
        final RObuildZipOutputStream roBuildZipOutputStream = new RObuildZipOutputStream(zipOutputStream, fileRandomSuffix, LocalDateTime.ofInstant(now, DateUtil.getLocaleZoneId()));
        return roBuildZipOutputStream;
    }

    /**
     *
     * @param integrators
     */
    public void setIntegrators(List<AbstractIntegrator> integrators) {
        this.integrators = integrators;
    }
}
