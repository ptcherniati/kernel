/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction.impl;

import java.time.LocalDateTime;
import java.util.Objects;
import org.inra.ecoinfo.extraction.IMotivation;
import org.inra.ecoinfo.extraction.IMotivationFormatter;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 *
 * @author ptcherniati
 */
public class DefaultMotivationFormatter implements IMotivationFormatter {
    private static final Logger LOGGER = LoggerFactory.getLogger(IMotivationFormatter.class);

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
    }

    /**
     *
     * @param parameters
     * @param motivation
     * @param localizationManager
     * @param user
     */
    @Override
    public void formatMotivation(IParameter parameters, IMotivation motivation, ILocalizationManager localizationManager, Utilisateur user) {
        MDC.put("date", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.DD_MM_YYYY_HH_MM_SS));
        MDC.put("login", user.getLogin());
        MDC.put("name", user.getNom());
        MDC.put("surname", user.getPrenom());
        MDC.put("email", user.getEmail());
        MDC.put("motivation", motivation.getMotivation());
        MDC.put("extractionType", parameters.getExtractionTypeCode());
        MDC.put("comment", parameters.getCommentaire());
        parameters.getParameters().forEach((k, v) -> {
            MDC.put(k, Objects.toString(v));
        });
    }
}
