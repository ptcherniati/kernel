package org.inra.ecoinfo.extraction;

import com.Ostermiller.util.BadDelimiterException;
import com.Ostermiller.util.CSVParser;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.inra.ecoinfo.AbstractTestFixture;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.applicationContext;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.coreConfiguration;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class AbstractExtractionFixture extends AbstractTestFixture {

    /**
     * The Constant START_INDEX.
     */
    public static final String START_INDEX = "start";

    /**
     * The Constant END_INDEX.
     */
    public static final String END_INDEX = "end";


    /**
     * The Constant SEPARATOR.
     */
    protected static final char SEPARATOR = ';';

    /**
     *
     */
    public AbstractExtractionFixture() {
        super();
    }

    /**
     *
     * @param filePathsList
     * @return
     * @throws IOException
     */
    public String checkExtractData(String filePathsList) throws IOException {
        ZipFile zipFile;
        try {
            final String attachement = getAttachement();
            if(attachement==null){
                throw new BusinessException(getMessage());
            }
            zipFile = getExtractZip(attachement);
        } catch (IOException | BusinessException | PersistenceException e1) {
            return "erreur dans la lecture du fichier d'extraction " + e1.getMessage();
        }
        ZipEntry entry;
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        int orderOfentry = 0;
        List<ErrorsReport> errorsReports = new LinkedList<>();
        String[] filepaths = filePathsList.split(" *, *");
        List<File> filesToCompare = new LinkedList<>();
        for (String fileName : filepaths) {
            File file = new File(fileName);
            filesToCompare.add(file);
        }
        while (entries.hasMoreElements()) {
            entry = entries.nextElement();
            if (entry.getName().startsWith("recapitulatif_extraction")) {
                continue;
            }
            File file = filesToCompare.get(orderOfentry);
            orderOfentry++;
            String fileEncoding;
            CSVParser parser;
            try {
                fileEncoding = Utils.detectStreamEncoding(new BufferedInputStream(zipFile.getInputStream(entry)));
                parser = new CSVParser(new InputStreamReader(zipFile.getInputStream(entry), fileEncoding), SEPARATOR);
            } catch (BadDelimiterException | IOException e) {
                return "erreurs dans la lecture du fichier d'extraction " + e.getMessage();
            }
            ErrorsReport errorsReport;
            if (filesToCompare.isEmpty()) {
                errorsReport = new ErrorsReport();
                errorsReport.addErrorMessage(String.format("Le fichier d'extraction %s n'est pas attendu", entry.getName()));
                errorsReports.add(errorsReport);
                continue;
            } else {
                errorsReport = new ErrorsReport(entry.getName(), file.getName());
            }
            CSVParser parserControl = new CSVParser(new InputStreamReader(new FileInputStream(file.getAbsolutePath()), fileEncoding), SEPARATOR);
            String[] values = parser.getLine();
            String[] expectedValues = null;
            int lineNumber = 0;
            while (values != null) {
                lineNumber++;
                expectedValues = parserControl.getLine();
                if (expectedValues == null) {
                    errorsReport.addErrorMessage(String.format("Le fichier d'extraction contient plus de lignes (%d) que le fichier de contrôle (%d)", parser.getLastLineNumber(), parserControl.getLastLineNumber()));
                    break;
                }
                String value;
                String expectedValue;
                int i;
                for (i = 0; i < values.length; i++) {
                    if (i >= expectedValues.length) {
                        errorsReport.addErrorMessage(String.format("Ligne %d, le fichier d'extraction contient plus de valeurs (%d) que le fichier de contrôle (%d)", lineNumber, values.length, expectedValues.length));
                        break;
                    }
                    value = values[i];
                    expectedValue = expectedValues[i];
                    if(expectedValue.startsWith("//")){
                        continue;
                    }
                    if (!value.equals(expectedValue)) {                        
                        errorsReport.addErrorMessage(String.format("ligne %d colonne %d, la valeur attendue %s est différente de la valeur retournée %s", lineNumber, i + 1, expectedValue, value));
                    }
                }
                if (i < values.length) {
                    errorsReport.addErrorMessage(String.format("Ligne %d, le fichier d'extraction contient moins de valeurs (%d) que le fichier de contrôle (%d)", lineNumber, values.length, expectedValues.length));
                }
                values = parser.getLine();
            }
            if (errorsReport.hasErrors()) {
                errorsReports.add(errorsReport);
            }
        }
        if(zipFile.size()<filepaths.length){
            throw new IOException(String.format("Il y a %d fichier dans le zip alors que %d fichiers sont attendus", zipFile.size(),filepaths.length));
        }
        zipFile.close();
        if (!errorsReports.isEmpty()) {
            String error = "";
            for (ErrorsReport errorsReport : errorsReports) {
                error = error.concat(errorsReport.getErrorsMessages()).concat(ErrorsReport.NEW_LINE);
            }
            return error;
        }
        return "true";
    }

    /**
     * Gets the extract zip.
     *
     * @return the extract zip
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private ZipFile getExtractZip(String attachement) throws IOException {
        final String repositoryURI = ((ICoreConfiguration) coreConfiguration).getRepositoryURI();
        return new ZipFile(repositoryURI.concat(System.getProperty("file.separator").concat(attachement)));
    }

    private String getAttachement() throws BusinessException, org.inra.ecoinfo.utils.exceptions.PersistenceException {
        return ((INotificationsManager) applicationContext.getBean("notificationsManager")).getAllUserNotifications(getUtilisateurConnected()).get(0).getAttachment();
    }

    private String getMessage() throws BusinessException, org.inra.ecoinfo.utils.exceptions.PersistenceException {
        final Notification notification = ((INotificationsManager) applicationContext.getBean("notificationsManager")).getAllUserNotifications(getUtilisateurConnected()).get(0);
        return notification==null?"no notification for error":notification.getBody();
    }

    /**
     * Builds the new map period.
     *
     * @param dateStart
     * @param dateEnd
     * @return the map
     */
    protected final List<Map<String, String>> buildNewMapPeriod(String dateStart, String dateEnd) {
        List<Map<String, String>> period = new LinkedList<>();
        final Map<String, String> firstMap = new HashMap<>();
        firstMap.put(START_INDEX, dateStart);
        firstMap.put(END_INDEX, dateEnd);
        period.add(firstMap);
        return period;
    }

    /**
     * Builds the new map period.
     *
     * @return the map
     */
    protected final List<Map<String, String>> buildNewMapPeriod() {
        return buildNewMapPeriod("", "");
    }

    class ErrorsReport {

        /**
         * The Constant NEW_LINE.
         */
        static final String NEW_LINE = "\n";
        /**
         * The Constant TIRET.
         */
        private static final String TIRET = "-";
        /**
         * The Constant ERROR_MESSAGE.
         */
        private static final String ERROR_MESSAGE = "Le fichier d'extraction %s n'est pas conforme au fichier de contrôle %S";
        /**
         * The errors messages.
         */
        private String errorsMessages;
        /**
         * The errors.
         */
        int errors = 0;

        /**
         * Instantiates a new errors report.
         */
        public ErrorsReport() {
            super();
            errorsMessages = "";
        }

        /**
         * Instantiates a new errors report.
         *
         * @param extractionFileName the extraction file name
         * @param controlefileName the controlefile name
         */
        public ErrorsReport(String extractionFileName, String controlefileName) {
            super();
            errorsMessages = String.format(ErrorsReport.ERROR_MESSAGE, extractionFileName, controlefileName);
        }

        /**
         * Adds the error message.
         *
         * @param errorMessage the error message
         */
        public void addErrorMessage(String errorMessage) {
            errorsMessages = errorsMessages.concat(ErrorsReport.TIRET).concat(errorMessage).concat(ErrorsReport.NEW_LINE);
            errors++;
        }

        /**
         * Gets the count errors.
         *
         * @return the count errors
         */
        public int getCountErrors() {
            return errors;
        }

        /**
         * Gets the errors messages.
         *
         * @return the errors messages
         */
        public String getErrorsMessages() {
            return errorsMessages;
        }

        /**
         * Checks for errors.
         *
         * @return true, if successful
         */
        public boolean hasErrors() {
            return errors > 0;
        }
    }
}
