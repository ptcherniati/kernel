package org.inra.ecoinfo.extraction.config.impl;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.dbunit.DatabaseUnitException;
import org.inra.ecoinfo.config.ConfigurationException;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class ExtractionConfigurationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
public class ExtractionConfigurationTest {

    private static final String REPOSITORY_PATTERN = "%s/extraction";

    /**
     * The extraction configuration.
     */
    @Autowired
    ExtractionConfiguration extractionConfiguration;
    @Autowired
    ICoreConfiguration coreConfiguration;

    /**
     * Sets the extraction configuration.
     *
     * @param extractionConfiguration the new extraction configuration
     */
    public void setExtractionConfiguration(ExtractionConfiguration extractionConfiguration) {
        this.extractionConfiguration = extractionConfiguration;
    }

    /**
     * Test.
     *
     * @throws URISyntaxException the uRI syntax exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws IllegalAccessException the illegal access exception
     * @throws ConfigurationException the configuration exception
     * @throws PersistenceException the persistence exception
     * @throws DatabaseUnitException the database unit exception
     * @throws SQLException the sQL exception
     * @throws ParserConfigurationException the parser configuration exception
     */
    @Test
    public void test() throws URISyntaxException, IOException, IllegalAccessException, ConfigurationException, PersistenceException, DatabaseUnitException, SQLException, ParserConfigurationException {
        Assert.assertTrue("la liste des extractions est vide", extractionConfiguration.getExtractions().size() > 0);
        Assert.assertTrue(new File(String.format(REPOSITORY_PATTERN, coreConfiguration.getRepositoryURI())).exists());
    }

    /**
     *
     * @param coreConfiguration
     */
    public void setCoreConfiguration(ICoreConfiguration coreConfiguration) {
        this.coreConfiguration = coreConfiguration;
    }
}
