package org.inra.ecoinfo.refdata.datatype;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * The Interface IDatatypeDAO.
 */
public interface IDatatypeDAO extends IDAO<DataType> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    List<DataType> getAll();

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     * @link(String) the code
     */
    Optional<DataType> getByCode(String code);
}
