package org.inra.ecoinfo.refdata.serialization.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.IMetadataRecorder;
import org.inra.ecoinfo.refdata.IMetadataRecorderDAO;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.config.IMetadataConfiguration;
import org.inra.ecoinfo.refdata.serialization.IMetadataSerialization;
import org.inra.ecoinfo.serialization.AbstractSerialization;
import org.inra.ecoinfo.serialization.impl.AbstractActivitySerialization;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MailAdminSerialization.
 */
public class MetadataSerialization extends AbstractSerialization implements IMetadataSerialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/metadataSerialization.xsd";

    /**
     *
     */
    public static final String METADATA_ENTRY = "metadata/metadata.csv";

    /**
     *
     */
    public static final String LINE_PROPERTY_CSV_FILE = ";%s;%s";

    /**
     *
     */
    public static final String METADATA_ZIP_ENTRY_PATTERN = "metadata/%s/%s.csv";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "metadataSerialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractActivitySerialization.class.getName());
    private String id;
    /**
     * The mail admin.
     */
    protected IUtilisateurDAO utilisateurDAO;
    private IMetadataConfiguration metadataConfiguration;
    private IMetadataRecorderDAO metadataRecorderDAO;

    /**
     *
     */
    protected IMetadataManager metadataManager;

    /**
     *
     * @param metadataManager
     */
    public void setMetadataManager(IMetadataManager metadataManager) {
        this.metadataManager = metadataManager;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     *
     * @param metadataConfiguration
     */
    public void setMetadataConfiguration(IMetadataConfiguration metadataConfiguration) {
        this.metadataConfiguration = metadataConfiguration;
    }

    /**
     *
     * @param metadataRecorderDAO
     */
    public void setMetadataRecorderDAO(IMetadataRecorderDAO metadataRecorderDAO) {
        this.metadataRecorderDAO = metadataRecorderDAO;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#createConfig(org.apache.commons.digester.Digester)
     */
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("serialization/module/metadataSerialization/id", "setId", 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#getModuleName()
     */
    /**
     *
     * @return
     */
    @Override
    public String getModuleName() {
        return MetadataSerialization.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.AbstractSerialization#getSchemaPath()
     */
    /**
     *
     * @return
     */
    @Override
    public String getSchemaPath() {
        return MetadataSerialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#isAutoLoad()
     */
    /**
     *
     * @return
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param outZip
     * @throws BusinessException
     */
    @Override
    public void serialize(ZipOutputStream outZip) throws BusinessException {
        List<String> lines = metadataConfiguration.getMetadatas().stream()
                .map((metadata) -> {
                    final String metadataPath = metadata.getCode();
                    final String metadataCode = metadataPath.replaceAll(".*/", "");
                    IMetadataRecorder recorder = null;
                    try {
                        recorder = metadataRecorderDAO.getMetadataRecorderByMetadataName(metadataPath);
                    } catch (PersistenceException e) {
                        LOGGER.info(String.format("no recorder for metadata %s", metadataPath), e);
                        return null;
                    }
                    String line = (String) null;
                    try {
                        final ModelGridMetadata buildModelGrid = recorder.buildModelGrid();
                        String fileContent = buildModelGrid.formatAsCSV();
                        line = writeEntry(outZip, metadataPath, metadataCode, fileContent);
                    } catch (Exception e) {
                        LOGGER.error(String.format("Can't write metadata information %s", metadata.getCode()));
                    }
                    return line; //To change body of generated lambdas, choose Tools | Templates.
                })
                .filter(line -> line != null)
                .collect(Collectors.toList());

        ZipEntry entry = new ZipEntry(METADATA_ENTRY);
        try {
            outZip.putNextEntry(entry);
            lines.stream()
                    .filter(line->line!=null)
                    .forEach(line -> {
                        try {
                            outZip.write(line.getBytes());
                        } catch (IOException ex) {
                            LOGGER.error(String.format("Can't metadata write information %s", line));
                        }
                    });
            outZip.closeEntry();
        } catch (IOException e) {
            LOGGER.error("Can't write metadata information ");
        }
    }

    /**
     *
     * @param outZip
     * @param metadataCode
     * @param fileContent
     * @return 
     * @throws IOException
     */
    public String writeEntry(ZipOutputStream outZip, String metadataPath, String metadataCode, String fileContent) {
        String entryName = null;
        String line = null;
        try {
            entryName = String.format(METADATA_ZIP_ENTRY_PATTERN, metadataPath, metadataCode);
            ZipEntry metadataFile = new ZipEntry(entryName);
            outZip.putNextEntry(metadataFile);
            outZip.write(fileContent.getBytes(StandardCharsets.UTF_8));
            outZip.closeEntry();
            line = String.format("%n%s;%s", metadataPath, metadataCode);
        } catch (IOException ex) {
            LOGGER.error(String.format("can't zip metadata file §S", entryName));
        }
        return line;
    }
}
