package org.inra.ecoinfo.refdata.theme;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * The Interface IThemeDAO.
 */
public interface IThemeDAO extends IDAO<Theme> {

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     * @link(String) the code
     */
    Optional<Theme> getByCode(String code);

    /**
     *
     * @return
     */
    List<Theme> getAll();
}
