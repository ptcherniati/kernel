package org.inra.ecoinfo.refdata;

/**
 * OREILacs project - see LICENCE.txt for use created: 30 juil. 2009 14:49:53
 */
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface IMetadataRepositoryDAO.
 *
 * @author "Antoine Schellenberger"
 */
public interface IMetadataRepositoryDAO {

    /**
     * Permet d'ajouter une nouvelle ressource de métadonnées dans la zone de
     * stockage.
     *
     * @param datas the datas
     * @param filename the filename
     * @throws PersistenceException the persistence exception
     * @link(byte[]) the datas
     * @link(String) the filename
     */
    void addMetadata(byte[] datas, String filename) throws PersistenceException;
}
