/**
 * OREILacs project - see LICENCE.txt for use created: 12 août 2009 11:48:00
 */
package org.inra.ecoinfo.refdata;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.localization.IInternationalizable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.refdata.config.IMetadataConfiguration;
import org.inra.ecoinfo.refdata.config.Metadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The Class ConfigurationGeneratedMetadataRecorderDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class ConfigurationGeneratedMetadataRecorderDAO extends AbstractJPADAO<Object> implements IMetadataRecorderDAO, ApplicationContextAware, IInternationalizable {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.refdata.impl.messages";
    /**
     * The Constant PROPERTY_MSG_NO_RECORDER @link(String).
     */
    private static final String PROPERTY_MSG_NO_RECORDER = "PROPERTY_MSG_NO_RECORDER";


    /**
     * The Constant SET @link(String).
     */
    private static final String SET = "set";
    private final List<String> defaultProperties = Stream.of("localizationManager", "mgaServiceBuilder", "policyManager", "treeApplicationCacheManager").collect(Collectors.toList());
    /**
     * The application context @link(ApplicationContext).
     */
    private ApplicationContext applicationContext;
    /**
     * The localization manager @link(ILocalizationManager).
     */
    private ILocalizationManager localizationManager;

    /**
     * The metadata recorders map @link(Map<String,IMetadataRecorder>).
     *
     */
    @SuppressWarnings("rawtypes")
    private final Map<String, IMetadataRecorder> metadataRecordersMap = new HashMap<>();
    /**
     * The configuration @link(Configuration).
     */
    protected IMetadataConfiguration configuration;

    /**
     *
     */
    public ConfigurationGeneratedMetadataRecorderDAO() {
        super();
    }

    /*
     * *
     *
     * @see org.inra.ore.data.dao.IMetadataRecorderDAO#getMetadataRecorderByMetadataName (java.lang.String)
     */
 /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.refdata.IMetadataRecorderDAO#getMetadataRecorderByMetadataName(java.lang.String)
     */
    /**
     *
     * @param metadataCode
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("rawtypes")
    @Override
    public IMetadataRecorder getMetadataRecorderByMetadataName(final String metadataCode) throws PersistenceException {
        final IMetadataRecorder metadataRecorder = metadataRecordersMap.get(metadataCode);
        if (metadataRecorder == null) {
            final String message = localizationManager.getMessage(ConfigurationGeneratedMetadataRecorderDAO.BUNDLE_SOURCE_PATH, ConfigurationGeneratedMetadataRecorderDAO.PROPERTY_MSG_NO_RECORDER);
            throw new PersistenceException(String.format(message, metadataCode));
        } else {
            return metadataRecorder;
        }
    }

    /**
     * Inits the.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @SuppressWarnings("rawtypes")
    public void init() throws BusinessException {
        for (final Metadata metadata : configuration.getMetadatas()) {
            try {
                final Class<?> metadataRecorderClass = Class.forName(metadata.getRecorder());
                final Object metadataRecorderInstance = metadataRecorderClass.newInstance();
                for (final String daoClass : metadata.getDaos()) {
                    final Object springDAO = applicationContext.getBean(daoClass);
                    final Method daoSetterMethod = getMethod(metadataRecorderClass, daoClass);
                    daoSetterMethod.invoke(metadataRecorderInstance, springDAO);
                }
                final IMetadataRecorder metadataRecorder = (IMetadataRecorder) metadataRecorderInstance;
                for (String p : defaultProperties) {
                    addBean(metadataRecorderClass, metadataRecorderInstance, p);
                }
                metadataRecordersMap.put(metadata.getCode(), metadataRecorder);
            } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException ex) {
                throw new BusinessException(ex);
            }
        }
    }

    /**
     * Sets the application context.
     *
     * @param applicationContext the new application context
     * @see
     * org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final IMetadataConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see
     * org.inra.ecoinfo.localization.IInternationalizable#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Retrieve method by name.
     *
     * @param name the name
     * @param clazz the clazz
     * @return the method
     * @link(String) the name
     * @link(Class<?>) the clazz
     */
    private Method retrieveMethodByName(final String name, final Class<?> clazz) {
        for (final Method method : clazz.getMethods()) {
            if (method.getName().equals(name)) {
                return method;
            }
        }
        return null;
    }

    /**
     * Adds the bean.
     *
     * @param metadataRecorderClass the metadata recorder class
     * @param metadataRecorderInstance the metadata recorder instance
     * @param beanName the bean name
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     * @link(Class<?>) the metadata recorder class
     * @link(Object) the metadata recorder instance
     * @link(String) the bean name
     */
    protected void addBean(final Class<?> metadataRecorderClass, final Object metadataRecorderInstance, final String beanName) throws IllegalAccessException, InvocationTargetException {
        final Object springDAO = applicationContext.getBean(beanName);
        final Method daoSetterMethod = getMethod(metadataRecorderClass, beanName);
        daoSetterMethod.invoke(metadataRecorderInstance, springDAO);
    }

    /**
     * Gets the method.
     *
     * @param metadataRecorderClass the metadata recorder class
     * @param daoClass the dao class
     * @return the method
     * @link(Class<?>) the metadata recorder class
     * @link(String) the dao class
     */
    protected Method getMethod(final Class<?> metadataRecorderClass, final String daoClass) {
        final StringBuilder stringBuilder = new StringBuilder(daoClass);
        final String daoName = stringBuilder.replace(0, 1, stringBuilder.substring(0, 1).toUpperCase()).toString();
        return retrieveMethodByName(ConfigurationGeneratedMetadataRecorderDAO.SET + daoName, metadataRecorderClass);
    }
}
