package org.inra.ecoinfo.refdata.serialization;

/**
 * The Interface IIdSerialization.
 */
public interface IMetadataSerialization {

    /**
     *
     * @return
     */
    String getId();
}
