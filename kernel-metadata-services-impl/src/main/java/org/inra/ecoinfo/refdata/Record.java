/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 16:38:02
 */
package org.inra.ecoinfo.refdata;

import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.utils.Column;

/**
 * The Class Record.
 *
 * @author "Antoine Schellenberger"
 */
public class Record {

    /**
     * The columns @link(List<Column>).
     */
    private List<Column> columns = new LinkedList<>();

    /**
     * Gets the columns.
     *
     * @return the columns
     */
    public List<Column> getColumns() {
        return columns;
    }

    /**
     * Sets the columns.
     *
     * @param columns the new columns
     */
    public void setColumns(final List<Column> columns) {
        this.columns = columns;
    }
}
