/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.refdata.theme;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<Theme> {

    /**
     * The properties description en @link(Properties).
     */
    protected Properties propertiesDescriptionEN;
    /**
     * The properties nom fr @link(Properties).
     */
    protected Properties propertiesNomFR;
    /**
     * The properties nom en @link(Properties).
     */
    protected Properties propertiesNomEN;
    /**
     * The theme dao @link(IThemeDAO).
     */
    protected IThemeDAO themeDAO;

    /**
     * Delete record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final String code = values[0];
                themeDAO.remove(themeDAO.getByCode(code).orElseThrow(() -> new BusinessException("can't get theme")));
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param theme the theme
     * @return the new line model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final Theme theme) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(theme == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : theme.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(theme == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomFR.getProperty(theme.getName(),theme.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(theme == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomEN.getProperty(theme.getName(),theme.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(theme == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : theme.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(theme == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesDescriptionEN.getProperty(theme.getDescription(),theme.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }

    /**
     * Process record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(Theme.class));
                final String nom = tokenizerValues.nextToken();
                final String description = tokenizerValues.nextToken();
                Optional<Theme> dbTheme = themeDAO.getByCode(Utils.createCodeFromString(nom));
                if (!dbTheme.isPresent()) {
                    final Theme theme = new Theme(nom, description);
                    themeDAO.saveOrUpdate(theme);
                } else {
                    dbTheme.get().setDescription(description);
                }
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the theme dao.
     *
     * @param themeDAO the new theme dao
     */
    public void setThemeDAO(final IThemeDAO themeDAO) {
        this.themeDAO = themeDAO;
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @throws BusinessException the persistence exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Theme> getAllElements() throws BusinessException {
        final List<Theme> all = themeDAO.getAll();
        Collections.sort(all, (Theme o1, Theme o2) -> {
            if(o1==null || o2 ==null){
                return o1==o2?0:-1;
            }
            return o1.getCode().compareTo(o2.getCode());
        });
        return all;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<Theme> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(Nodeable.getLocalisationEntite(Theme.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE);
        propertiesNomEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(Theme.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(Theme.class), "description", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
}
