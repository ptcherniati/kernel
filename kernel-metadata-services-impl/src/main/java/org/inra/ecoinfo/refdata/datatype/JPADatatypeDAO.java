/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.refdata.datatype;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;

/**
 * The Class JPADatatypeDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class JPADatatypeDAO extends AbstractJPADAO<DataType> implements IDatatypeDAO {

    /**
     * Gets the all.
     *
     * @return the all
     * @see org.inra.ecoinfo.refdata.datatype.IDatatypeDAO#getAll()
     */
    @Override
    public List<DataType> getAll() {
        return getAllBy(DataType.class, DataType::getCode);
    }

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     * @see
     * org.inra.ecoinfo.refdata.datatype.IDatatypeDAO#getByCode(java.lang.String)
     */
    @Override
    public Optional<DataType> getByCode(final String code) {
        CriteriaQuery<DataType> query = builder.createQuery(DataType.class);
        Root<DataType> dty = query.from(DataType.class);
        query.where(builder.equal(dty.get(Nodeable_.code), code));
        query.select(dty);
        return getOptional(query);
    }
}
