package org.inra.ecoinfo.refdata.config.impl;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.PersistenceException;
import org.inra.ecoinfo.mga.business.IMgaBuilder;
import org.inra.ecoinfo.mga.business.MgaBuilder;
import org.inra.ecoinfo.mga.business.MgaServiceBuilder;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yahiaoui
 */
public class RefdatasInitialiser implements Serializable {

    /**
     *
     */
    protected MgaServiceBuilder mgaServiceBuilder;

    /**
     *
     */
    protected RefdatasInitialiser refdatasInitialiser;

    /**
     *
     * @param refdatasInitialiser
     */
    public void setRefdatasInitialiser(RefdatasInitialiser refdatasInitialiser) {
        this.refdatasInitialiser = refdatasInitialiser;
    }

    /**
     *
     * @param mgaServiceBuilder
     */
    public void setMgaServiceBuilder(MgaServiceBuilder mgaServiceBuilder) {
        this.mgaServiceBuilder = mgaServiceBuilder;
    }

    /**
     *
     * @param pathes
     */
    public void createRefdataNodes(Stream<String> pathes) {

        try {
            IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
            IMgaRecorder mgaRecorder = mgaServiceBuilder.getRecorder();
            final Optional<IMgaIOConfiguration> configuration = configurator.getConfiguration(AbstractMgaIOConfigurator.REFDATA_CONFIGURATION_RIGHTS);
            Class<Nodeable> leafType = configuration.isPresent() ? configuration.get().getLeafType() : null;
            if (!mgaRecorder.emptyQueryResult(builder -> MgaServiceBuilder.buildQuery(builder, NodeDataSet.class, leafType))) {

                IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
                Stream<Refdata> nodeables = mgaRecorder.getNodeables(Refdata.class);

                Map<String, INodeable> entities = nodeables
                        .collect(Collectors.toMap(
                                n -> MgaBuilder.getUniqueCode(Refdata.class, PatternConfigurator.ANCESTOR_SEPARATOR, n.getCode()),
                                n -> n));

                Stream<INode> leavesForPathes = mgaBuilder.buildLeavesForPathes(
                        pathes,
                        configurator.getConfiguration(1).map(conf -> conf.getEntryType()).orElse(new Class[0]),
                        entities,
                        WhichTree.TREEREFDATA, null
                );
                leavesForPathes.forEach(node -> mgaRecorder.persist(node));

            }
        } catch (PersistenceException ex) {
            LoggerFactory.getLogger(RefdatasInitialiser.class).error(ex.getMessage(), ex);
        }
    }

}
