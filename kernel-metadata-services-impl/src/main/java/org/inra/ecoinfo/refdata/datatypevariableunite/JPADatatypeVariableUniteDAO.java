package org.inra.ecoinfo.refdata.datatypevariableunite;

import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.unite.Unite_;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 * The Class JPADatatypeVariableUniteDAO.
 */
public class JPADatatypeVariableUniteDAO extends AbstractJPADAO<DatatypeVariableUnite> implements IDatatypeVariableUniteDAO {

    /**
     * Gets the by datatype.
     *
     * @param datatypeCode the datatype code
     * @return the by datatype
     * @see
     * org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO#getByDatatype(java.lang.String)
     */
    @Override
    public List<DatatypeVariableUnite> getByDatatype(final String datatypeCode) {
        CriteriaQuery<DatatypeVariableUnite> query = builder.createQuery(DatatypeVariableUnite.class);
        Root<DatatypeVariableUnite> dvu = query.from(DatatypeVariableUnite.class);
        query.where(
                builder.equal(dvu.join(DatatypeVariableUnite_.datatype).get(Nodeable_.code), datatypeCode)
        );
        query.orderBy(builder.asc(dvu.join(DatatypeVariableUnite_.variable).get(Nodeable_.code)));
        query.select(dvu);
        return getResultList(query);
    }

    /**
     * Gets the by n key.
     *
     * @param datatypeCode the datatype code
     * @param variableCode the variable code
     * @param uniteCode the unite code
     * @return the by n key
     * @see
     * org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO#getByNKey(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public Optional<DatatypeVariableUnite> getByNKey(final String datatypeCode, final String variableCode, final String uniteCode) {
        CriteriaQuery<DatatypeVariableUnite> query = builder.createQuery(DatatypeVariableUnite.class);
        Root<DatatypeVariableUnite> dvu = query.from(DatatypeVariableUnite.class);
        query.where(
                builder.equal(dvu.join(DatatypeVariableUnite_.datatype).get(Nodeable_.code), datatypeCode),
                builder.equal(dvu.join(DatatypeVariableUnite_.variable).get(Nodeable_.code), variableCode),
                builder.equal(dvu.join(DatatypeVariableUnite_.unite).get(Unite_.code), uniteCode)
        );
        query.orderBy(builder.asc(dvu.join(DatatypeVariableUnite_.variable).get(Nodeable_.code)));
        query.select(dvu);
        return getOptional(query);
    }

    /**
     * Gets the unite.
     *
     * @param datatypeCode
     * @param variableCode
     * @return the unite
     * @see
     * org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO#getUnite(java.lang.String,
     * java.lang.String)
     */
    @Override
    public Optional<Unite> getUnite(final String datatypeCode, final String variableCode) {
        CriteriaQuery<Unite> query = builder.createQuery(Unite.class);
        Root<DatatypeVariableUnite> dvu = query.from(DatatypeVariableUnite.class);
        query.where(
                builder.equal(dvu.join(DatatypeVariableUnite_.datatype).get(Nodeable_.code), datatypeCode),
                builder.equal(dvu.join(DatatypeVariableUnite_.variable).get(Nodeable_.code), variableCode)
        );
        query.orderBy(builder.asc(dvu.join(DatatypeVariableUnite_.variable).get(Nodeable_.code)));
        query.select(dvu.join(DatatypeVariableUnite_.unite));
        return getOptional(query);
    }

    /**
     *
     * @param realNode
     * @return
     */
    @Override
    public RealNode mergeRealNode(RealNode realNode) {
        return entityManager.merge(realNode);
    }

    /**
     *      * get a map of realnode variable sorted by variable where parent node
     * datatype is realNode</p>
     *
     * @param realNode
     * @return
     */
    @Override
    public Map<Variable, RealNode> getRealNodesVariables(RealNode realNode) {

        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> rn = query.from(RealNode.class);
        final Join<RealNode, Nodeable> nodeable = rn.join(RealNode_.nodeable);
        Root<DatatypeVariableUnite> dvu = query.from(DatatypeVariableUnite.class);
        dvu.join(DatatypeVariableUnite_.variable, JoinType.LEFT);
        query.where(
                builder.equal(rn.join(RealNode_.parent), realNode),
                builder.equal(nodeable.get(Nodeable_.id), dvu.get(DatatypeVariableUnite_.id))
        );
        query.select(rn);
        final List<RealNode> resultList = getResultList(query);
        return Maps.uniqueIndex(resultList, c -> ((DatatypeVariableUnite) c.getNodeable()).getVariable());
    }
}
