/**
 * OREILacs project - see LICENCE.txt for use created: 12 août 2009 11:40:44
 */
package org.inra.ecoinfo.refdata;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface IMetadataRecorderDAO.
 *
 * @author "Antoine Schellenberger"
 */
public interface IMetadataRecorderDAO extends IDAO<Object> {

    /**
     * Gets the metadata recorder by metadata name.
     *
     * @param metadataName the metadata name
     * @return the metadata recorder by metadata name
     * @throws PersistenceException the persistence exception
     * @link(String) the metadata name
     */
    @SuppressWarnings("rawtypes")
    IMetadataRecorder getMetadataRecorderByMetadataName(String metadataName) throws PersistenceException;
}
