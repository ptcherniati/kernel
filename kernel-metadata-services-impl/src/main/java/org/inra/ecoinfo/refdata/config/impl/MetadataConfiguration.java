package org.inra.ecoinfo.refdata.config.impl;

import com.google.common.base.Strings;
import java.io.IOException;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.AbstractConfiguration;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.refdata.config.IMetadataConfiguration;
import org.inra.ecoinfo.refdata.config.Metadata;
import org.inra.ecoinfo.refdata.refdata.IRefdataDAO;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MetadataConfiguration.
 */
public class MetadataConfiguration extends AbstractConfiguration implements IMetadataConfiguration {

    private static long order = 0L;

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/metadataConfiguration.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "metadataConfiguration";
    private static final String REPOSITORY_PATTERN = "%s/metadata/.";
    private static final Logger LOGGER = LoggerFactory.getLogger(MetadataConfiguration.class);
    private static final String KEY = "key";
    /**
     * The metadatas.
     */
    private List<Metadata> metadatas = new LinkedList<>();
    private ICoreConfiguration coreConfiguration;
    private ILocalizationManager localizationManager;
    private IRefdataDAO refdataDAO;
    private RefdatasInitialiser refdatasInitialiser;

    /**
     *
     * @param refdatasInitialiser
     */
    public void setRefdatasInitialiser(RefdatasInitialiser refdatasInitialiser) {
        this.refdatasInitialiser = refdatasInitialiser;
    }

    /**
     *
     * @param refdataDAO
     */
    public void setRefdataDAO(IRefdataDAO refdataDAO) {
        this.refdataDAO = refdataDAO;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.refdata.config.IMetadataConfiguration#addMetadata(org .inra.ecoinfo.refdata.config.impl.Metadata)
     */
    /**
     *
     * @param metadata
     */
    @Override
    public void addMetadata(Metadata metadata) {
        if (!metadatas.contains(metadata)) {
            metadatas.add(metadata);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#createConfig(org.apache. commons.digester.Digester)
     */

    /**
     *
     * @param digester
     */

    @Override
    public void createConfig(Digester digester) {
        digester.addObjectCreate("configuration/module/metadataConfiguration/metadata", Metadata.class);
        digester.addSetNext("configuration/module/metadataConfiguration/metadata", "addMetadata");
        digester.addCallMethod("configuration/module/metadataConfiguration/metadata/name", "addInternationalizedName", 2, newStringClassArray());
        digester.addCallParam("configuration/module/metadataConfiguration/metadata/name", 0, "language");
        digester.addObjectCreate("configuration/module/metadataConfiguration/metadata/name", String.class);
        digester.addCallMethod("configuration/module/metadataConfiguration/metadata/name", "concat", 0);
        digester.addCallParam("configuration/module/metadataConfiguration/metadata/name", 1, false);
        digester.addCallMethod("configuration/module/metadataConfiguration/metadata/description", "addInternationalizedDescription", 2, newStringClassArray());
        digester.addCallParam("configuration/module/metadataConfiguration/metadata/description", 0, "language");
        digester.addObjectCreate("configuration/module/metadataConfiguration/metadata/description", String.class);
        digester.addCallMethod("configuration/module/metadataConfiguration/metadata/description", "concat", 0);
        digester.addCallParam("configuration/module/metadataConfiguration/metadata/description", 1, false);
        digester.addCallMethod("configuration/module/metadataConfiguration/metadata/order", "setOrder", 0);
        digester.addCallMethod("configuration/module/metadataConfiguration/metadata/finalMetadata", "setFinalMetadata", 0);
        digester.addCallMethod("configuration/module/metadataConfiguration/metadata/recorder", "setRecorder", 0);
        digester.addObjectCreate("configuration/module/metadataConfiguration/metadata/spring-dao", StringBuilder.class);
        digester.addCallMethod("configuration/module/metadataConfiguration/metadata/spring-dao/class", "append", 0);
        digester.addSetNext("configuration/module/metadataConfiguration/metadata/spring-dao", "addDao");
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.refdata.config.IMetadataConfiguration#getMetadatas()
     */
    /**
     *
     * @return
     */
    @Override
    public List<Metadata> getMetadatas() {
        return metadatas;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.refdata.config.IMetadataConfiguration#setMetadatas(java .util.List)
     */
    /**
     *
     * @param metadatas
     */
    @Override
    public void setMetadatas(List<Metadata> metadatas) {
        this.metadatas = metadatas;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getModuleName()
     */

    /**
     *
     * @return
     */

    @Override
    public String getModuleName() {
        return MetadataConfiguration.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getSchemaPath()
     */

    /**
     *
     * @return
     */

    @Override
    public String getSchemaPath() {
        return MetadataConfiguration.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#isAutoLoad()
     */

    /**
     *
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractConfiguration#postConfig()
     */

    /**
     *
     * @throws BusinessException
     */

    @Override
    public void postConfig() throws BusinessException {
        try {
            createRepositoryIfNeeded();
            Stream<String> pathes = updateRefdataAndReturnPathes().collect(Collectors.toSet()).stream();
            refdatasInitialiser.createRefdataNodes(pathes);
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @return @throws PersistenceException
     */
    protected Stream<String> updateRefdataAndReturnPathes() throws PersistenceException {
        return getMetadatas().stream()
                .map(metadata -> persistMetadataFromMetadata(metadata))
                .filter(t -> t != null);
    }

    /**
     *
     * @param metadata
     * @return
     */
    protected String persistMetadataFromMetadata(Metadata metadata) {
        Deque<String> codes = splitAnscestors(metadata.getCode());
        Deque<String> names = splitAnscestors(metadata.getName());
        Map<String, Deque<String>> internationalizedNames = metadata.getInternationalizedNames().entrySet()
                .stream()
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> splitAnscestors(entry.getValue())));
        Map<String, String> internationalizedDescriptions = metadata.getInternationalizedDescriptions();
        final String metadataCode = persistMetadataFromCodesAndNames(codes, names, internationalizedNames, internationalizedDescriptions, metadata.getOrder(), false) ? metadata.getCode() : null;
        return metadataCode;
    }

    /**
     *
     * @param anscestorPath
     * @return
     */
    protected LinkedList splitAnscestors(String anscestorPath) {
        return (LinkedList) Arrays.asList(anscestorPath.split(PatternConfigurator.ANCESTOR_SEPARATOR))
                .stream()
                .collect(Collectors.toCollection(() -> new LinkedList()));
    }

    /**
     *
     * @param codes
     * @param names
     * @param internationalizedNames
     * @param hasBeenPersist
     * @param internationalizedDescriptions
     * @param ord
     * @return
     */
    protected boolean persistMetadataFromCodesAndNames(Deque<String> codes, Deque<String> names, Map<String, Deque<String>> internationalizedNames,
            Map<String, String> internationalizedDescriptions, Long ord, boolean hasBeenPersist) {
        if (CollectionUtils.isEmpty(codes)) {
            return hasBeenPersist;
        }
        String code = getCodeAndPollLast(codes);
        String name = names.pollLast();
        Map<String, String> internationalizedName = internationalizedNames.entrySet().stream()
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue().pollLast()));
        Optional<Refdata> refdataEntityOpt = refdataDAO.getByCode(code);
        name = internationalizedName.containsKey(KEY) ? internationalizedName.get(KEY) : name;
        name = name == null ? code : name;
        try {
            refdataEntityOpt = refdataDAO.getByCode(code);
            Refdata refdata;
            if (!refdataEntityOpt.isPresent()) {
                refdata = new Refdata(name);
                refdata.setCode(code);
                ord = setOrder(ord, refdata);
                saveInternationalizedNames(internationalizedName, refdata.getName());
                hasBeenPersist = true;
                if (!internationalizedDescriptions.isEmpty()) {
                    final String description = internationalizedDescriptions.getOrDefault(KEY, internationalizedDescriptions.get(Localization.getDefaultLocalisation()));
                    refdata.setDescription(description);
                    saveDescription(internationalizedDescriptions, description);
                }
                refdataDAO.saveOrUpdate(refdata);
            } else {
                refdata = refdataEntityOpt.get();
                ord = setOrder(ord, refdata);
                saveInternationalizedNames(internationalizedName, refdata.getName());
                if (Strings.isNullOrEmpty(refdata.getDescription()) && !internationalizedDescriptions.isEmpty()) {
                    final String description = internationalizedDescriptions.getOrDefault(KEY, internationalizedDescriptions.get(Localization.getDefaultLocalisation()));
                    refdata.setDescription(description);
                }
                saveDescription(internationalizedDescriptions, refdata.getDescription());
            }
        } catch (PersistenceException e) {
            LOGGER.error("can't persist refdatas", e);
        }
        return persistMetadataFromCodesAndNames(codes, names, internationalizedNames, internationalizedDescriptions, ord, hasBeenPersist);
    }

    private void saveDescription(Map<String, String> internationalizedDescriptions, String defaultDescription) throws PersistenceException {
        if (defaultDescription != null) {
            saveInternationalizedDescriptions(internationalizedDescriptions, defaultDescription);
            internationalizedDescriptions.clear();
        }
    }

    private Long setOrder(Long ord, final Refdata refdata) {
        if (ord == null) {
            ord = order;
            order = ord++;
        } else {
            ++order;
        }
        refdata.setOrder(ord);
        return ord;
    }

    /**
     *
     * @param coreConfiguration
     */
    public void setCoreConfiguration(ICoreConfiguration coreConfiguration) {
        this.coreConfiguration = coreConfiguration;
    }

    private void createRepositoryIfNeeded() throws IOException {
        String repositoryFiles = String.format(REPOSITORY_PATTERN, coreConfiguration.getRepositoryURI());
        FileWithFolderCreator.createFile(repositoryFiles + "/.");
    }

    private String getCodeAndPollLast(Deque<String> codes) {
        String path = codes.stream()
                .collect(Collectors.joining(PatternConfigurator.ANCESTOR_SEPARATOR));
        codes.pollLast();
        return path;
    }

    private void saveInternationalizedDescriptions(Map<String, String> internationalizedDescriptions, String code) {
        String defaultKey = internationalizedDescriptions.containsKey(KEY) ? KEY : Localization.getDefaultLocalisation();
        for (Map.Entry<String, String> entry : internationalizedDescriptions.entrySet()) {
            try {
                String language = entry.getKey();
                if (defaultKey.equals(language)) {
                    continue;
                }
                String internationalizedString = entry.getValue();
                localizationManager.createorUpdateLocalization(language, Nodeable.getLocalisationEntite(Refdata.class), Refdata.NAME_ATTRIBUTS_DESCRIPTION, code, internationalizedString);
            } catch (BusinessException ex) {
                LOGGER.error("can't register internationalized description", ex);
            }
        }
    }

    private void saveInternationalizedNames(Map<String, String> internationalizedNames, String code) {
        String defaultKey = internationalizedNames.containsKey(KEY) ? KEY : Localization.getDefaultLocalisation();
        for (Map.Entry<String, String> entry : internationalizedNames.entrySet()) {
            try {
                String language = entry.getKey();
                if (defaultKey.equals(language)) {
                    continue;
                }
                String internationalizedString = entry.getValue();
                localizationManager.createorUpdateLocalization(language, Nodeable.getLocalisationEntite(Refdata.class), Nodeable.ENTITE_COLUMN_NAME, code, internationalizedString);
            } catch (BusinessException ex) {
                LOGGER.error("can't register internationalized description", ex);
            }
        }
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }
}
