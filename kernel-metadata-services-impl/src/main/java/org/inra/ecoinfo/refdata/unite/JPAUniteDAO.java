/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.refdata.unite;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * The Class JPAUniteDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class JPAUniteDAO extends AbstractJPADAO<Unite> implements IUniteDAO {

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     * @see org.inra.ecoinfo.refdata.unite.IUniteDAO#getByCode(java.lang.String)
     */
    @Override
    public Optional<Unite> getByCode(final String code) {
        CriteriaQuery<Unite> query = builder.createQuery(Unite.class);
        Root<Unite> uni = query.from(Unite.class);
        query.where(
                builder.equal(uni.get(Unite_.code), code)
        );
        query.select(uni);
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Unite> getAll() {
        return getAll(Unite.class);
    }
}
