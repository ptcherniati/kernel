/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.refdata.serialization.impl;

import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.serialization.impl.AbstractActivitySerialization;

/**
 *
 * @author tcherniatinsky
 */
public class MetadataActivitiesSerialization extends AbstractActivitySerialization{

    /**
     *
     */
    public static final String PRIVILEGES_ENTRY = "activities_metadatas.csv";
    private static final String MODULE_NAME = "metadataActivitySerialization";

    /**
     *
     * @return
     */
    @Override
    public String getActivityEntry() {
        return  PRIVILEGES_ENTRY;
    }

    /**
     *
     * @return
     */
    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

    /**
     *
     * @return
     */
    @Override
    public int getConfigurationNumber() {
       return AbstractMgaIOConfigurator.REFDATA_CONFIGURATION_RIGHTS;
    }

    /**
     *
     * @return
     */
    @Override
    public WhichTree getWhichTree() {
        return WhichTree.TREEREFDATA;
    }
    
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        super.createConfig(digester);
    }
    
}
