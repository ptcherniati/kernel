package org.inra.ecoinfo.refdata.impl;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.Transient;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.IMetadataRecorder;
import org.inra.ecoinfo.refdata.IMetadataRecorderDAO;
import org.inra.ecoinfo.refdata.IMetadataRepositoryDAO;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.config.IMetadataConfiguration;
import org.inra.ecoinfo.refdata.config.Metadata;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class LocalMetadataManager.
 *
 * @author "Antoine Schellenberger"
 */
public class LocalMetadataManager extends MO implements IMetadataManager {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.refdata.impl.messages";
    /**
     * The Constant DATE_FORMAT @link(String).
     */
    protected static final String DATE_FORMAT = "mm:ss";
    /**
     * The Constant PROPERTY_MSG_BAD_METATADATA_FILE_FORMAT_INSERTION_FAILURE.
     * @link(String).
     */
    protected static final String PROPERTY_MSG_BAD_METATADATA_FILE_FORMAT_INSERTION_FAILURE = "PROPERTY_MSG_BAD_METATADATA_FILE_FORMAT_INSERTION_FAILURE";
    /**
     * The Constant PROPERTY_MSG_DELETE_ERROR_REFDATA @link(String).
     */
    protected static final String PROPERTY_MSG_DELETE_ERROR_REFDATA = "PROPERTY_MSG_DELETE_ERROR_REFDATA";
    /**
     * The Constant
     * PROPERTY_MSG_METADATA_FILE_CONTAINS_UPDATE_INSERTION_ABORTED.
     * @link(String).
     */
    protected static final String PROPERTY_MSG_METADATA_FILE_CONTAINS_UPDATE_INSERTION_ABORTED = "PROPERTY_MSG_METADATA_FILE_CONTAINS_UPDATE_INSERTION_ABORTED";
    /**
     * The Constant PROPERTY_MSG_METADATA_INSERTION_FAILURE @link(String).
     */
    protected static final String PROPERTY_MSG_METADATA_INSERTION_FAILURE = "PROPERTY_MSG_METADATA_INSERTION_FAILURE";
    /**
     * The Constant PROPERTY_MSG_METADATA_INSERTION_SUCCESSED @link(String).
     */
    protected static final String PROPERTY_MSG_METADATA_INSERTION_SUCCESSED = "PROPERTY_MSG_METADATA_INSERTION_SUCCESSED";
    /**
     * The Constant PROPERTY_MSG_ROLLBACK_TRANSACTION @link(String).
     */
    protected static final String PROPERTY_MSG_ROLLBACK_TRANSACTION = "PROPERTY_MSG_ROLLBACK_TRANSACTION";
    /**
     * The Constant PROPERTY_MSG_SUCCESS_DELETE_REFDATA @link(String).
     */
    protected static final String PROPERTY_MSG_SUCCESS_DELETE_REFDATA = "PROPERTY_MSG_SUCCESS_DELETE_REFDATA ";
    /**
     * The Constant PROPERTY_MSG_SUCCESS_SAVE_REFDATA @link(String).
     */
    protected static final String PROPERTY_MSG_SUCCESS_SAVE_REFDATA = "PROPERTY_MSG_SUCCESS_SAVE_REFDATA";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_RIGHTS_ON_DATA = "PROPERTY_MSG_NO_RIGHTS_ON_DATA";

    /**
     *
     */
    protected static final String MSG_NO_RIGHTS_FOR_READ = "PROPERTY_MSG_NO_RIGHTS_FOR_READ";

    /**
     *
     */
    protected static final String MSG_NO_RIGHTS_FOR_DELETE = "MSG_NO_RIGHTS_FOR_DELETE";

    /**
     *
     */
    protected static final String MSG_NO_RIGHTS_FOR_DOWNLOAD = "PROPERTY_MSG_NO_RIGHTS_FOR_DOWNLOAD";

    /**
     *
     */
    protected static final String MSG_NO_RIGHTS_FOR_UPLOAD = "PROPERTY_MSG_NO_RIGHTS_FOR_UPLOAD";

    /**
     *
     */
    protected static final String MSG_NULL_METADATA = "PROPERTY_MSG_NULL_METADATA";
    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    protected static final Logger LOGGER = LoggerFactory.getLogger(LocalMetadataManager.class.getName());

    /**
     *
     */
    public static Logger METADATA_LOGGER = LoggerFactory.getLogger("metadata.logger");
    /**
     * The configuration @link(Configuration).
     */
    protected IMetadataConfiguration configuration;
    /**
     * The datatype dao @link(IDatatypeDAO).
     */
    protected IDatatypeDAO datatypeDAO;
    /**
     * The metadata recorder dao @link(IMetadataRecorderDAO).
     */
    protected IMetadataRecorderDAO metadataRecorderDAO;
    /**
     * The metadata repository dao @link(IMetadataRepositoryDAO).
     */
    protected IMetadataRepositoryDAO metadataRepositoryDAO;
    /**
     * The site dao @link(ISiteDAO).
     */
    protected ISiteDAO siteDAO;

    /**
     * Builds the model grid metadata.
     *
     * @param refdataName the refdata name
     * @return the model grid metadata
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.IMetadataManager#buildModelGridMetadata(java.lang.String)
     */
    @SuppressWarnings("rawtypes")
    @Override
    @Transactional(readOnly = true)
    public ModelGridMetadata buildModelGridMetadata(final String refdataName) throws BusinessException {
        testIsNullRefdata(refdataName);
        testHasRights(refdataName, new Activities[]{Activities.administration, Activities.depot, Activities.publication, Activities.telechargement, Activities.edition, Activities.suppression}, WhichTree.TREEREFDATA, LocalMetadataManager.PROPERTY_MSG_NO_RIGHTS_ON_DATA);
        IMetadataRecorder metadataRecorder;
        metadataRecorder = getMetadataRecorder(refdataName, (Utilisateur) policyManager.getCurrentUser());
        return metadataRecorder.buildModelGrid();
    }

    /**
     * Retrieve all sites.
     *
     * @return the list
     * @see org.inra.ecoinfo.refdata.IMetadataManager#retrieveAllSites()
     */
    @Override
    public List<Site> retrieveAllSites() {
        return siteDAO.getAll();
    }

    /**
     * Retrieve restricted metadatas.
     *
     * @return the list
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.IMetadataManager#retrieveRestrictedMetadatas()
     */
    @Override
    public List<Metadata> retrieveRestrictedMetadatas() throws BusinessException {

        final List<Metadata> metadatas = configuration.getMetadatas();

        if (policyManager.isRoot()) {
            return metadatas;
        }

        Set<String> restrictOnlyToActivitydNodeNameForCurrentUser
                = policyManager.getOnlyNamesOfNodesWhichHaveAtLeastOneActivity(policyManager.getCurrentUser(), WhichTree.TREEREFDATA);

        final List<Metadata> restrictedMetadatas = new LinkedList<>();

        metadatas.stream().filter((metadata) -> (restrictOnlyToActivitydNodeNameForCurrentUser.contains(metadata.getName()))).forEach((metadata) -> {
            restrictedMetadatas.add(metadata);
        });

        return restrictedMetadatas;
    }

    /**
     * Save and delete model grid metadata.
     *
     * @param modelGridMetadata the model grid metadata
     * @param editedLines the edited lines
     * @param nameRefdata the name refdata
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.IMetadataManager#saveAndDeleteModelGridMetadata(org.inra.ecoinfo.refdata.ModelGridMetadata,
     * java.util.Set, java.lang.String)
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void saveAndDeleteModelGridMetadata(final ModelGridMetadata modelGridMetadata, final Set<LineModelGridMetadata> editedLines, final String nameRefdata) throws BusinessException {
        testIsNullRefdata(nameRefdata);
        TransactionStatus tr = metadataRecorderDAO.beginDefaultTransaction();
        final Utilisateur utilisateur = (Utilisateur) policyManager.getCurrentUser();
        final Set<LineModelGridMetadata> linesToEdit = getLinesToEdit(editedLines);
        final List<LineModelGridMetadata> linesToDelete = getLinesToDelete(modelGridMetadata);
        if (!linesToDelete.isEmpty()) {
            testHasRights(nameRefdata, Activities.suppression, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_DELETE);
        }
        if (!linesToEdit.isEmpty()) {
            testHasRights(nameRefdata, Activities.publication, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_UPLOAD);
        }
        MDC.put("metadata.metadataname", nameRefdata);
        Optional.ofNullable(policyManager.getCurrentUser())
                .ifPresent((user) -> {
                    MDC.put("metadata.user.login", user.getLogin());
                    MDC.put("metadata.user.email", user.getEmail());
                    MDC.put("metadata.user.name", user.getNom());
                    MDC.put("metadata.user.surname", user.getPrenom());
                });
        IMetadataRecorder metadataRecorder = getMetadataRecorder(nameRefdata, utilisateur);
        saveModel(modelGridMetadata, nameRefdata, linesToEdit, utilisateur);
        boolean hasNewLine = linesToEdit.stream().anyMatch(l -> l.getIsNew());
        boolean isNew = linesToEdit.stream().allMatch(l -> l.getIsNew());
        String type = "update";
        type = isNew ? "create" : (hasNewLine ? "create and update" : "update");
        MDC.put("metadata.type", type);
        MDC.put("metadata.date", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.YYYY_MM_DD_HHMMSS));
        MDC.put("metadata.metadataname", nameRefdata);
        MDC.put("metadata.lineToEdit", linesToEdit
                .stream()
                .map(line -> line.getKeyPath())
                .collect(Collectors.joining(",")));
        METADATA_LOGGER.info(String.format("%s a updaté  la donnée de référence %s", MDC.get("metadata.user.login"), MDC.get("metadata.metadataname")));

        if (linesToDelete.isEmpty()) {
            updateRepositoryFile(nameRefdata, metadataRecorder);
            return;
        }
        try {
            metadataRecorder.deleteProcessBytes(modelGridMetadata.formatAsCSV(linesToDelete).getBytes(StandardCharsets.UTF_8));
            MDC.put("metadata.type", "delete");
            MDC.put("metadata.lineToEdit", null);
            MDC.put("metadata.lineToDelete",
                    linesToDelete
                            .stream()
                            .map(line -> line.getKeyPath())
                            .collect(Collectors.joining(",")));
            tryFlush(nameRefdata, tr, utilisateur);
        } catch (final BusinessException | IOException e) {
            final String message = String.format(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_DELETE_ERROR_REFDATA), nameRefdata);
            LOGGER.error(message, e);
            MDC.put("metadata.error", e.getMessage());
            sendNotification(message, Notification.ERROR, null, utilisateur);
            metadataRecorderDAO.rollbackTransaction(tr);
            throw new BusinessException(message);
        } finally {
            METADATA_LOGGER.info(String.format("%s a supprimé  la donnée de référence %s", MDC.get("metadata.user.login"), MDC.get("metadata.metadataname")));
        }
        if (!linesToEdit.isEmpty()) {
            sendNotification(String.format(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_SUCCESS_DELETE_REFDATA), nameRefdata), Notification.INFO, null, utilisateur);
        }
        updateRepositoryFile(nameRefdata, metadataRecorder);
    }

    /**
     *
     * @param nameRefdata
     * @param metadataRecorder
     * @throws BusinessException
     */
    protected void updateRepositoryFile(final String nameRefdata, IMetadataRecorder metadataRecorder) throws BusinessException {
        try {
            metadataRepositoryDAO.addMetadata(metadataRecorder.buildModelGrid().formatAsCSV().getBytes(StandardCharsets.UTF_8), nameRefdata);
        } catch (PersistenceException | IOException e) {
            LOGGER.error("metadata modifications saved in db but not saved on repository", e);
        }
    }

    /**
     *
     * @param modelGridMetadata
     * @return
     */
    protected List<LineModelGridMetadata> getLinesToDelete(final ModelGridMetadata modelGridMetadata) {
        final List<LineModelGridMetadata> linesToDelete = new LinkedList<>();
        modelGridMetadata.getLinesModelGridMetadatas().stream().filter((line) -> (((LineModelGridMetadata) line).getToDelete())).forEach((line) -> {
            linesToDelete.add((LineModelGridMetadata) line);
        });
        return linesToDelete;
    }

    /**
     *
     * @param editedLines
     * @return
     */
    protected Set<LineModelGridMetadata> getLinesToEdit(final Set<LineModelGridMetadata> editedLines) {
        final Set<LineModelGridMetadata> linesToEdit = new HashSet<>();
        editedLines.stream().filter((lineModelGridMetadata) -> (!lineModelGridMetadata.getToDelete())).forEach((lineModelGridMetadata) -> {
            linesToEdit.add(lineModelGridMetadata);
        });
        return linesToEdit;
    }

    /**
     *
     * @param modelGridMetadata
     * @param nameRefdata
     * @param linesToEdit
     * @param utilisateur
     * @throws BusinessException
     */
    protected void saveModel(final ModelGridMetadata modelGridMetadata, final String nameRefdata, final Set<LineModelGridMetadata> linesToEdit, final Utilisateur utilisateur) throws BusinessException {
        try {
            if (!linesToEdit.isEmpty()) {
                saveModelGridMetadata(modelGridMetadata, linesToEdit, nameRefdata);
            }
        } catch (final BusinessException e1) {
            sendNotification(String.format(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_METADATA_INSERTION_FAILURE), nameRefdata), Notification.ERROR, e1.getMessage(), utilisateur);
            throw new BusinessException(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_METADATA_INSERTION_FAILURE), e1);
        } catch (final Exception e1) {
            sendNotification(String.format(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_METADATA_INSERTION_FAILURE), nameRefdata), Notification.ERROR,
                    localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_ROLLBACK_TRANSACTION), utilisateur);
            throw new BusinessException(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_METADATA_INSERTION_FAILURE), e1);
        }
    }

    /**
     *
     * @param nameRefdata
     * @param utilisateur
     * @return
     * @throws BusinessException
     */
    protected IMetadataRecorder getMetadataRecorder(final String nameRefdata, final Utilisateur utilisateur) throws BusinessException {
        IMetadataRecorder metadataRecorder = null;
        String refdataCode = Utils.createCodeFromString(nameRefdata);
        try {
            metadataRecorder = metadataRecorderDAO.getMetadataRecorderByMetadataName(refdataCode);
        } catch (final PersistenceException e2) {
            sendNotification(String.format(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_METADATA_INSERTION_FAILURE), nameRefdata),
                    Notification.ERROR,
                    String.format("%s%n%s", e2.getMessage(),
                            localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_ROLLBACK_TRANSACTION))), utilisateur);
            throw new BusinessException(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_METADATA_INSERTION_FAILURE), e2);
        }
        return metadataRecorder;
    }

    /**
     * Save model grid metadata.
     *
     * @param modelGridMetadata the model grid metadata
     * @param editedLines the edited lines
     * @param nameRefdata the name refdata
     * @throws BusinessException the business exception
     * @see
     *
     * register data from database to repository file
     * org.inra.ecoinfo.refdata.IMetadataManager#saveModelGridMetadata(org.inra.ecoinfo.refdata.ModelGridMetadata,
     * java.util.Set, java.lang.String)
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveModelGridMetadata(final ModelGridMetadata modelGridMetadata, final Set<LineModelGridMetadata> editedLines, final String nameRefdata) throws BusinessException {
        testIsNullRefdata(nameRefdata);
        testHasRights(nameRefdata, Activities.publication, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_UPLOAD);
        try {
            final IMetadataRecorder metadataRecorder = getMetadataRecorder(nameRefdata, (Utilisateur) policyManager.getCurrentUser());
            metadataRecorder.processBytes(modelGridMetadata.formatAsCSV(editedLines).getBytes(StandardCharsets.UTF_8));
        } catch (final IOException e) {
            throw new BusinessException("io exception", e);
        } finally {
            METADATA_LOGGER.info(String.format("%s a updaté  la donnée de référence %s", MDC.get("metadata.user.login"), MDC.get("metadata.metadataname")));
        }
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final IMetadataConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the datatype dao.
     *
     * @param datatypeDAO the new datatype dao
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Sets the metadata recorder dao.
     *
     * @param metadataRecorderDAO the new metadata recorder dao
     */
    public void setMetadataRecorderDAO(final IMetadataRecorderDAO metadataRecorderDAO) {
        this.metadataRecorderDAO = metadataRecorderDAO;
    }

    /**
     * Sets the metadata repository dao.
     *
     * @param metadataRepositoryDAO the new metadata repository dao
     */
    public void setMetadataRepositoryDAO(final IMetadataRepositoryDAO metadataRepositoryDAO) {
        this.metadataRepositoryDAO = metadataRepositoryDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param nameRefdata
     * @param tr
     * @param utilisateur
     * @throws BusinessException
     */
    protected void tryFlush(final String nameRefdata, final TransactionStatus tr, final Utilisateur utilisateur) throws BusinessException {
        try {
            tr.flush();
        } catch (final Exception e) {
            metadataRecorderDAO.rollbackTransaction(tr);
            throw new BusinessException(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_DELETE_ERROR_REFDATA), e);
        }
    }

    /**
     * Upload metadatas.
     *
     * @param file the file
     * @param metadataName
     * @param originalFileName
     * @param login the login
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.IMetadataManager#uploadMetadatas(java.io.File,
     * java.lang.String, java.lang.String, java.lang.String)
     */
    @SuppressWarnings("rawtypes")
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void uploadMetadatas(final File file, final String metadataName, final String originalFileName, final String login) throws BusinessException {
        testIsNullRefdata(metadataName);
        testHasRights(metadataName, Activities.publication, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_DOWNLOAD);
        IMetadataRecorder metadataRecorder;
        Properties refdataProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(Refdata.class), Nodeable.ENTITE_COLUMN_NAME);
        try {
            metadataRecorder = getMetadataRecorder(metadataName, (Utilisateur) policyManager.getCurrentUser());
            final Instant startTime = Instant.now();
            Optional.ofNullable(policyManager.getCurrentUser())
                    .ifPresent((user) -> {
                        MDC.put("metadata.user.login", user.getLogin());
                        MDC.put("metadata.user.email", user.getEmail());
                        MDC.put("metadata.user.name", user.getNom());
                        MDC.put("metadata.user.surname", user.getPrenom());
                    });
            MDC.put("metadata.type", "upload");
            MDC.put("metadata.date", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.YYYY_MM_DD_HHMMSS));
            MDC.put("metadata.metadataname", metadataName);
            metadataRecorder.processFile(file);
            final Instant endTime = Instant.now();

            try {
                metadataRepositoryDAO.addMetadata(metadataRecorder.buildModelGrid().formatAsCSV().getBytes(StandardCharsets.UTF_8), metadataName);
            } catch (IOException | PersistenceException ex) {
                MDC.put("metadata.error", ex.getMessage());
                LOGGER.debug(String.format("can't register file %s", originalFileName), ex);
            }
            String formatMessage = localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_METADATA_INSERTION_SUCCESSED);
            final Duration duration = Duration.between(startTime, endTime);
            String formatDuration = DurationFormatUtils.formatDuration(duration.toMillis(), "mm:ss");
            String internationalizedCode = internationalizedCode(metadataName, refdataProperties);
            final String message = String.format(formatMessage, internationalizedCode, formatDuration);
            sendNotificationToUser(message, Notification.INFO, null, login);
            LOGGER.debug(message);
        } catch (final BusinessException e) {
            final String message = String.format(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_METADATA_INSERTION_FAILURE), originalFileName);
            MDC.put("metadata.error", message);
            sendNotificationToUser(message, Notification.ERROR, e.getMessage(), login);
            UncatchedExceptionLogger.logUncatchedException(LOGGER, "uncatched exception in uploadMetadatas", e);
            throw new BusinessException(message, e);
        } finally {
            METADATA_LOGGER.info(String.format("%s a updaté  la donnée de référence %s", MDC.get("metadata.user.login"), MDC.get("metadata.metadataname")));
        }
    }

    /**
     *
     * @param refdataCode
     * @throws BusinessException
     */
    protected void testIsNullRefdata(final String refdataCode) throws BusinessException {
        IMetadataRecorder recorder;
        try {
            recorder = metadataRecorderDAO.getMetadataRecorderByMetadataName(refdataCode);
        } catch (PersistenceException e) {
            String message = localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.MSG_NULL_METADATA);
            notificationsManager.addNotification(buildNotification(Notification.WARN, message, null, null), policyManager.getCurrentUserLogin());
            throw new BusinessException(message, e);
        }
        if (recorder == null) {
            String message = localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.MSG_NULL_METADATA);
            notificationsManager.addNotification(buildNotification(Notification.WARN, message, null, null), policyManager.getCurrentUserLogin());
            throw new BusinessException(message);
        }
    }
// ADMINISTRATION, DELETE, DOWNLOAD, EDITION

    /**
     *
     * @param metadataCode
     * @param activity
     * @param whichTree
     * @param keyMessage
     * @throws BusinessException
     */
    protected void testHasRights(String metadataCode, Activities activity, WhichTree whichTree, String keyMessage) throws BusinessException {

        if (!policyManager.isRoot() && !policyManager.hasActivity(policyManager.getCurrentUser(), activity.name(), whichTree)) {

            String message = String.format(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, keyMessage), metadataCode);
            notificationsManager.addNotification(buildNotification(Notification.INFO, message, null, null), policyManager.getCurrentUserLogin());
            throw new BusinessException(message);
        }

    }

    /**
     *
     * @param metadataCode
     * @param activities
     * @param whichTree
     * @param keyMessage
     * @throws BusinessException
     */
    protected void testHasRights(String metadataCode, Activities[] activities, WhichTree whichTree, String keyMessage) throws BusinessException {

        for (Activities activity : activities) {
            if (policyManager.isRoot() || policyManager.hasActivity(policyManager.getCurrentUser(), activity.name(), whichTree)) {
                return;
            }
        }
        String message = String.format(localizationManager.getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, keyMessage), metadataCode);
        notificationsManager.addNotification(buildNotification(Notification.INFO, message, null, null), policyManager.getCurrentUserLogin());
        throw new BusinessException(message);
    }

    /**
     *
     * @return
     */
    public IPolicyManager getPolicyManager() {
        return policyManager;
    }

    /**
     *
     * @param policyManager
     */
    @Override
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param metadataName
     * @param refdataProperties
     * @return
     */
    protected String internationalizedCode(String metadataName, Properties refdataProperties) {
        return Optional.ofNullable(metadataName)
                .map(name -> Utils.createCodeFromString(name))
                .map(code -> code.split(Utils.SEPARATOR_URL))
                .map(array -> internationalizeArray(array, refdataProperties))
                .map(array -> Stream.of(array).collect(Collectors.joining(Utils.SEPARATOR_URL)))
                .orElse("");
    }

    /**
     *
     * @param array
     * @param refdataProperties
     * @return
     */
    protected String[] internationalizeArray(String[] array, Properties refdataProperties) {
        for (int i = 0; i < array.length; i++) {
            array[i] = refdataProperties.getProperty(array[i], array[i]);
        }
        return array;
    }

}
