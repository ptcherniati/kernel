/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.refdata.variable;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * The Interface IVariableDAO.
 *
 * @author "Antoine Schellenberger"
 */
public interface IVariableDAO extends IDAO<Variable> {

    /**
     * Gets the by code.
     *
     * @param nom the nom
     * @return the by code
     * @link(String) the nom
     */
    Optional<Variable> getByCode(String nom);

    /**
     *
     * @return
     */
    List<Variable> getAll();
}
