/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.refdata.unite;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * The Interface IUniteDAO.
 *
 * @author "Antoine Schellenberger"
 */
public interface IUniteDAO extends IDAO<Unite> {

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     * @link(String) the code
     */
    Optional<Unite> getByCode(String code);

    /**
     *
     * @return
     */
    List<Unite> getAll();
}
