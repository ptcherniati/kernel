/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.refdata.theme;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * The Class JPAThemeDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class JPAThemeDAO extends AbstractJPADAO<Theme> implements IThemeDAO {

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     * @see org.inra.ecoinfo.refdata.theme.IThemeDAO#getByCode(java.lang.String)
     */
    @Override
    public Optional<Theme> getByCode(final String code) {
        CriteriaQuery<Theme> query = builder.createQuery(Theme.class);
        Root<Theme> the = query.from(Theme.class);
        query
                .select(the)
                .where(builder.equal(the.get(Theme_.code), code));
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Theme> getAll() {
        return getAllBy(Theme.class, Theme::getCode);
    }
}
