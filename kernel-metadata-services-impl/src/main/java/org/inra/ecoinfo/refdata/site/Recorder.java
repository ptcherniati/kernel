/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.refdata.site;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.assertj.core.util.Strings;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 *  *
 * @author "Guillaume Enrico"
 */
public class Recorder extends AbstractCSVMetadataRecorder<Site> {

    /**
     * The Constant PROPERTY_CST_NAME.
     */
    private static final String PROPERTY_CST_NAME = "nom";
    /**
     * The Constant PROPERTY_CST_DESCRIPTION.
     */
    private static final String PROPERTY_CST_DESCRIPTION = "description";
    /**
     * The Constant PROPERTY_CST_PARENT_PLACE.
     */
    private static final String PROPERTY_CST_PARENT_PLACE = "parent";
    private static final String BUNDLE_PATH = "org.inra.ecoinfo.refdata.site.messages";
    private static final String PROPERTY_MSG_INVALID_SITE = "PROPERTY_MSG_INVALID_SITE";
    /**
     * The site mon soere dao.
     */
    protected ISiteDAO siteDAO;

    /**
     *
     */
    protected IMgaRecorder mgaRecorder;
    /**
     * The properties nom en.
     */
    private Properties propertiesNomFR;
    /**
     * The properties nom en.
     */
    private Properties propertiesNomEN;
    /**
     * The properties description en.
     */
    private Properties propertiesDescriptionEN;
    private String[] namesSitesPossible;

    /**
     *
     * @param mgaRecorder
     */
    public void setMgaRecorder(IMgaRecorder mgaRecorder) {
        this.mgaRecorder = mgaRecorder;
    }

    /**
     * Creates the or update site.
     *      *
     * @param parent the parent
     * @param nom the nom
     * @param description the description
     * @param dbSite the db site
     * @param dbTypeSite the db type site
     * @throws BusinessException the persistence exception
     */
    private void createOrUpdateSite(final Site parent, final String nom, final String description, final Site dbSite)
            throws BusinessException {
        if (dbSite == null) {
            createSite(parent, nom, description);
        } else {
            updateSite(parent, nom, description, dbSite);
        }
    }

    /**
     * Creates the site.
     *      *
     * @param parent the parent
     * @param nom the nom
     * @param description the description
     * @param dbTypeSite the db type site
     * @throws BusinessException the persistence exception
     */
    private void createSite(final Site parent, final String nom, final String description) throws BusinessException {

        final Site site = new Site(nom, description, parent);
        try {
            siteDAO.saveOrUpdate(site);
        } catch (PersistenceException ex) {
            throw new BusinessException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util
     * .CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values;
            values = parser.getLine();
            while (values != null) {
                final String code = Utils.createCodeFromString(values[0]);
                final String pathParent = Utils.createCodeFromString(values[5]);
                Site parent = siteDAO.getByPath(pathParent).orElse(null);
                final Site site = siteDAO.getByCodeAndParent(code, parent)
                        .orElseThrow(() -> new BusinessException("can't get Site"));
                mgaRecorder.remove(site);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Site> getAllElements() throws BusinessException {
        return siteDAO.getAll();
    }

    /**
     * Gets the description site en.
     *      *
     * @param site the site
     * @return the description site en
     */
    private String getDescriptionSiteEN(final Site site) {
        String siteDescription = AbstractCSVMetadataRecorder.EMPTY_STRING;
        if (site != null && site.getDescription() != null) {
            siteDescription = propertiesDescriptionEN.getProperty(site.getDescription(), site.getDescription());
        }
        return siteDescription;
    }

    /**
     * Gets the description site fr.
     *      *
     * @param site the site
     * @return the description site fr
     */
    private String getDescriptionSiteFR(final Site site) {
        String siteDescription = AbstractCSVMetadataRecorder.EMPTY_STRING;
        if (site != null) {
            siteDescription = site.getDescription();
        }
        return siteDescription;
    }

    /**
     * Gets the names sites possibles.
     *      *
     * @return the names sites possibles
     * @throws BusinessException the persistence exception
     */
    private String[] getNamesSitesPossibles() {
        final List<Site> sites = siteDAO.getAll();
        final String[] namesSitesPossibles = new String[sites.size() + 1];
        namesSitesPossibles[0] = AbstractCSVMetadataRecorder.EMPTY_STRING;
        int index = 1;
        for (final Site site : sites) {
            namesSitesPossibles[index++] = site.getPath();
        }
        return namesSitesPossibles;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final Site site)
            throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(site == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : site.getName(),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, Recorder.PROPERTY_CST_NAME,
                        true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(getNomSiteFR(site),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, Recorder.PROPERTY_CST_NAME,
                        false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(getNomSiteEN(site),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, Recorder.PROPERTY_CST_NAME,
                        false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(getDescriptionSiteFR(site),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES,
                        Recorder.PROPERTY_CST_DESCRIPTION, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(getDescriptionSiteEN(site),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES,
                        Recorder.PROPERTY_CST_DESCRIPTION, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(getSiteParent(site), namesSitesPossible,
                        Recorder.PROPERTY_CST_PARENT_PLACE, true, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Gets the nom site en.
     *      *
     * @param site the site
     * @return the nom site en
     */
    private String getNomSiteEN(final Site site) {
        String siteNom = AbstractCSVMetadataRecorder.EMPTY_STRING;
        if (site != null) {
            siteNom = propertiesNomEN.getProperty(site.getName());
        }
        return siteNom;
    }

    /**
     * Gets the nom site fr.
     *      *
     * @param site the site
     * @return the nom site fr
     */
    private String getNomSiteFR(final Site site) {
        String siteNom = AbstractCSVMetadataRecorder.EMPTY_STRING;
        if (site != null) {
            siteNom = propertiesNomFR.getProperty(site.getName());
        }
        return siteNom;
    }

    /**
     * Gets the properties description en.
     *      *
     * @return the properties description en
     */
    public Properties getPropertiesDescriptionEN() {
        return propertiesDescriptionEN;
    }

    /**
     * Gets the properties nom en.
     *      *
     * @return the properties nom en
     */
    public Properties getPropertiesNomEN() {
        return propertiesNomEN;
    }

    /**
     * Gets the site mon soere dao.
     *      *
     * @return the site mon soere dao
     */
    public ISiteDAO getSiteDAO() {
        return siteDAO;
    }

    /**
     * Gets the site parent.
     *      *
     * @param site the site
     * @return the site parent
     */
    private String getSiteParent(final Site site) {
        String siteParent = AbstractCSVMetadataRecorder.EMPTY_STRING;
        if (site != null && site.getParent() != null) {
            siteParent = site.getParent().getPath();
        }
        return siteParent;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<Site> initModelGridMetadata() {
        namesSitesPossible = getNamesSitesPossibles();
        propertiesNomFR = localizationManager.newProperties(Nodeable.getLocalisationEntite(Site.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE);
        propertiesNomEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(Site.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(Site.class), "description", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     * Persist site.
     *      *
     * @param errorsReport the errors report
     * @param typeSiteCode the type site code
     * @param nomPathParent the nom path parent
     * @param nom the nom
     * @param description the description
     * @param lineNumber the line number
     * @throws BusinessException the persistence exception
     */
    private void persistSite(final AbstractCSVMetadataRecorder.ErrorsReport errorsReport, final String nomPathParent, final String nom, final String description,
            final int lineNumber) throws BusinessException {
        Optional<Site> parentOpt = siteDAO.getByPath(nomPathParent);
        if (!parentOpt.isPresent() && !Strings.isNullOrEmpty(nomPathParent)) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_PATH, PROPERTY_MSG_INVALID_SITE), lineNumber, 7, nomPathParent));
            return;
        }
        final String path = parentOpt
                .map(parent -> parent.getPath())
                .map(p -> p.concat(PatternConfigurator.ANCESTOR_SEPARATOR).concat(Utils.createCodeFromString(nom)))
                .orElse(Utils.createCodeFromString(nom));
        Site dbSite = siteDAO.getByPath(path).orElse(null);
        if (!errorsReport.hasErrors()) {
            createOrUpdateSite(parentOpt.orElse(null), nom, description, dbSite);
        }
    }

    /**
     * Process record.
     *      *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final AbstractCSVMetadataRecorder.ErrorsReport errorsReport = new AbstractCSVMetadataRecorder.ErrorsReport();
        try {
            skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values;
            int lineNumber = 0;
            Boolean loopEnd = false;
            while (!loopEnd) {
                loopEnd = !loopEnd;
                values = parser.getLine();
                if (values != null) {
                    final AbstractCSVMetadataRecorder.TokenizerValues tokenizerValues = new AbstractCSVMetadataRecorder.TokenizerValues(values, Nodeable.getLocalisationEntite(Site.class));
                    lineNumber++;
                    final String nom = tokenizerValues.nextToken();
                    final String description = tokenizerValues.nextToken();
                    final String nomPathParent = tokenizerValues.nextToken();
                    persistSite(errorsReport, nomPathParent, nom, description,
                            lineNumber);
                    loopEnd = false;
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | BusinessException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the properties description en.
     *      *
     * @param propertiesDescriptionEN the new properties description en
     */
    public void setPropertiesDescriptionEN(final Properties propertiesDescriptionEN) {
        this.propertiesDescriptionEN = propertiesDescriptionEN;
    }

    /**
     * Sets the properties nom en.
     *      *
     * @param propertiesNomEN the new properties nom en
     */
    public void setPropertiesNomEN(final Properties propertiesNomEN) {
        this.propertiesNomEN = propertiesNomEN;
    }

    /**
     * Sets the site mon soere dao.
     *      *
     * @param siteDAO the new site mon soere dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     * Update site.
     *      *
     * @param parent the parent
     * @param nom the nom
     * @param description the description
     * @param dbSite the db site
     * @param dbTypeSite the db type site
     * @throws BusinessException the persistence exception
     */
    private void updateSite(final Site parent, final String nom, final String description,
            Site dbSite) throws BusinessException {
        dbSite.setParent(parent);
        dbSite.setName(nom);
        dbSite.setDescription(description);
        try {
            siteDAO.saveOrUpdate(dbSite);
        } catch (PersistenceException ex) {
            throw new BusinessException(ex);
        }
        siteDAO.flush();
    }
}
