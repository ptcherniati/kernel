package org.inra.ecoinfo.refdata.impl;

/**
 * OREILacs project - see LICENCE.txt for use created: 30 juil. 2009 14:52:02
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.refdata.IMetadataRepositoryDAO;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.StreamUtils;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class FSMetadataRepositoryDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class FSMetadataRepositoryDAO implements IMetadataRepositoryDAO {

    private static final String PATH_PATTERN = "%s/%s/%s.csv";
    /**
     * The Constant METADATA @link(String).
     */
    private static final String METADATA = "metadata";
    /**
     * The configuration @link(Configuration).
     */
    private ICoreConfiguration configuration;

    /**
     * Adds the metadata.
     *
     * @param datas the datas
     * @param filename the filename
     * @throws PersistenceException the persistence exception
     * @see org.inra.ecoinfo.refdata.IMetadataRepositoryDAO#addMetadata(byte[],
     * java.lang.String)
     */
    @Override
    public void addMetadata(final byte[] datas, final String filename) throws PersistenceException {
        FileOutputStream fos = null;
        try {
            final String metadatasURI = String.format(PATH_PATTERN, configuration.getRepositoryURI(), FSMetadataRepositoryDAO.METADATA, filename);
            final File metadataFile = FileWithFolderCreator.createFile(metadatasURI);
            fos = new FileOutputStream(metadataFile);
            fos.write(datas);
            StreamUtils.closeStream(fos);
        } catch (final FileNotFoundException e) {
            throw new PersistenceException("file not found", e);
        } catch (final IOException e) {
            throw new PersistenceException("io exception", e);
        } finally {
            StreamUtils.closeStream(fos);
        }
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final ICoreConfiguration configuration) {
        this.configuration = configuration;
    }
}
