package org.inra.ecoinfo.refdata.valeurqualitative;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * The Class JPAValeurQualitativeDAO.
 */
public class JPAValeurQualitativeDAO extends AbstractJPADAO<IValeurQualitative> implements IValeurQualitativeDAO {

    /**
     * Gets the by code.
     *
     * @param <T> the generic type
     * @param code the code
     * @return the by code
     * @see
     * org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitativeDAO#getByCode(java.lang.String)
     */
    @Override
    public <T extends IValeurQualitative> List<T> getByCode(final String code) {
        CriteriaQuery<T> query = builder.createQuery((Class<T>) ValeurQualitative.class);
        Root<T> vq = query.from((Class<T>) ValeurQualitative.class);
        query
                .select(vq)
                .where(
                        builder.equal(vq.get((SingularAttribute<T, String>) ValeurQualitative_.code), code)
                );
        return getResultList(query);
    }

    /**
     * Gets the by code and value.
     *
     * @param <T> the generic type
     * @param code the code
     * @param value the value
     * @return the by code and value
     * @see
     * org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitativeDAO#getByCodeAndValue(java.lang.String,
     * java.lang.String)
     */
    @Override
    public <T extends IValeurQualitative> Optional<T> getByCodeAndValue(final String code, final String value) {
        CriteriaQuery<T> query = builder.createQuery((Class<T>) ValeurQualitative.class);
        Root<T> vq = query.from((Class<T>) ValeurQualitative.class);
        query
                .select(vq)
                .where(
                        builder.equal(vq.get((SingularAttribute<T, String>) ValeurQualitative_.code), code),
                        builder.equal(vq.get((SingularAttribute<T, String>) ValeurQualitative_.valeur), value)
                );
        return getOptional(query);
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends IValeurQualitative> List<T> getAll() {
            CriteriaQuery<T> query = builder.createQuery((Class<T>) ValeurQualitative.class);
            Root<T> vq = query.from((Class<T>) ValeurQualitative.class);
            query.select(vq);
            return getResultList(query);
    }
}
