/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.refdata.site;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * The Interface ISiteDAO.
 *
 * @author "Antoine Schellenberger"
 */
public interface ISiteDAO extends IDAO<Site> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    List<Site> getAll();

    /**
     * Gets the by code.
     *
     * @param code the code
     * @param parent
     * @return the by code
     * @link(String) the code
     */
    Optional<Site> getByCodeAndParent(String code, Site parent);

    /**
     * Gets the by path code.
     *
     * @param pathCode the path code
     * @return the by path code
     * @link(String) the path code
     */
    Optional<Site> getByPath(String pathCode);
}
