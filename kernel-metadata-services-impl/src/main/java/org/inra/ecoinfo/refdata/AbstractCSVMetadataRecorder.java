package org.inra.ecoinfo.refdata;

import com.Ostermiller.util.BadDelimiterException;
import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.DateTimeException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.Transient;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.localization.IInternationalizable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mga.business.IMgaBuilder;
import org.inra.ecoinfo.mga.business.IMgaServiceBuilder;
import org.inra.ecoinfo.mga.business.MgaBuilder;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.caching.ITreeApplicationCacheManager;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.StreamUtils;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NullValueException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class AbstractCSVMetadataRecorder.
 *
 * @param <T> the generic type
 * @author "Antoine Schellenberger"
 */
public abstract class AbstractCSVMetadataRecorder<T> implements IMetadataRecorder<T>, IInternationalizable {

    /**
     * The Constant COMMA @link(String).
     */
    public static final String COMMA = ",";
    /**
     * The Constant DATE_TYPE @link(String).
     */
    public static final String DATE_TYPE = "date";
    /**
     * The Constant TIME_TYPE @link(String).
     */
    public static final String TIME_TYPE = "time";
    /**
     * The Constant DOT @link(String).
     */
    public static final String DOT = ".";
    /**
     * The Constant FLOAT_TYPE @link(String).
     */
    public static final String FLOAT_TYPE = "float";
    /**
     * The Constant INTEGER_TYPE @link(String).
     */
    public static final String DOUBLE_TYPE = "double";

    /**
     *
     */
    public static final String INTEGER_TYPE = "integer";
    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractCSVMetadataRecorder.class);
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.refdata.impl.messages";
    /**
     * The Constant DATASET_DESCRIPTOR_XML @link(String).
     */
    private static final String DATASET_DESCRIPTOR_XML = "dataset-descriptor.xml";
    /**
     * The Constant PREFIX @link(String).
     */
    private static final String PREFIX = "prefix";
    /**
     * The Constant PROPERTY_MSG_BAD_HEADER_COLUMN @link(String).
     */
    private static final String PROPERTY_MSG_BAD_HEADER_COLUMN = "PROPERTY_MSG_BAD_HEADER_COLUMN";
    /**
     * The Constant PROPERTY_MSG_CHECKING_FORMAT_FILE @link(String).
     */
    private static final String PROPERTY_MSG_CHECKING_FORMAT_FILE = "PROPERTY_MSG_CHECKING_FORMAT_FILE";
    /**
     * The Constant PROPERTY_MSG_DATE_VALUE_EXPECTED @link(String).
     */
    private static final String PROPERTY_MSG_DATE_VALUE_EXPECTED = "PROPERTY_MSG_DATE_VALUE_EXPECTED";
    /**
     * The Constant PROPERTY_MSG_TIME_VALUE_EXPECTED @link(String).
     */
    private static final String PROPERTY_MSG_TIME_VALUE_EXPECTED = "PROPERTY_MSG_TIME_VALUE_EXPECTED";
    /**
     * The Constant PROPERTY_MSG_ERROR @link(String).
     */
    private static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";
    /**
     * The Constant PROPERTY_MSG_ERROR_UNEXPECTED_NUMBER_OF_COLUMNS.
     * @link(String).
     */
    private static final String PROPERTY_MSG_ERROR_UNEXPECTED_NUMBER_OF_COLUMNS = "PROPERTY_MSG_ERROR_UNEXPECTED_NUMBER_OF_COLUMNS";
    /**
     * The Constant PROPERTY_MSG_FILE_FORMAT_OK @link(String).
     */
    private static final String PROPERTY_MSG_FILE_FORMAT_OK = "PROPERTY_MSG_FILE_FORMAT_OK";
    /**
     * The Constant PROPERTY_MSG_LINES_MISSING @link(String).
     */
    private static final String PROPERTY_MSG_LINES_MISSING = "PROPERTY_MSG_LINES_MISSING";
    /**
     * The Constant PROPERTY_MSG_MISSING_FILE @link(String).
     */
    private static final String PROPERTY_MSG_MISSING_FILE = "PROPERTY_MSG_MISSING_FILE";
    /**
     * The Constant PROPERTY_MSG_MISSING_FLOAT @link(String).
     */
    private static final String PROPERTY_MSG_MISSING_DOUBLE = "PROPERTY_MSG_MISSING_DOUBLE";
    private static final String PROPERTY_MSG_MISSING_FLOAT = "PROPERTY_MSG_MISSING_FLOAT";
    /**
     * The Constant PROPERTY_MSG_MISSING_INTEGER @link(String).
     */
    private static final String PROPERTY_MSG_MISSING_INTEGER = "PROPERTY_MSG_MISSING_INTEGER";
    /**
     * The Constant PROPERTY_MSG_MISSING_VALUE @link(String).
     */
    private static final String PROPERTY_MSG_MISSING_VALUE = "PROPERTY_MSG_MISSING_VALUE";
    /**
     * The Constant SUFFIX @link(String).
     */
    private static final String SUFFIX = "suffix";
    /**
     * The Constant EMPTY_STRING @link(String).
     */
    protected static final String EMPTY_STRING = "";
    /**
     * The Constant PROPERTY_CST_CVS_SEPARATOR @link(char).
     */
    protected static final char PROPERTY_CST_CVS_SEPARATOR = ';';
    /**
     * The Constant PROPERTY_MSG_EMPTY_FILE @link(String).
     */
    protected static final String PROPERTY_MSG_EMPTY_FILE = "PROPERTY_MSG__EMPTY_FILE";
    /**
     * The dataset descriptor @link(DatasetDescriptor).
     */
    protected DatasetDescriptor datasetDescriptor;
    /**
     * The localization manager @link(ILocalizationManager).
     */
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    protected IMgaServiceBuilder mgaServiceBuilder;

    /**
     *
     */
    protected IPolicyManager policyManager;

    /**
     *
     */
    protected ITreeApplicationCacheManager treeApplicationCacheManager;

    /**
     *
     */
    protected Class<T> entityClass = null;

    /**
     *
     */
    protected Boolean hasGenericNodeable = null;

    /**
     * Instantiates a new cSV metadata recorder.
     */
    public AbstractCSVMetadataRecorder() {
        super();
        try {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(this.getClass().getResourceAsStream(AbstractCSVMetadataRecorder.DATASET_DESCRIPTOR_XML));
        } catch (final IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Builds the model grid.
     *
     * @return the model grid metadata
     * @throws BusinessException the business exception
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#buildModelGrid()
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ModelGridMetadata<T> buildModelGrid() throws BusinessException {
        final ModelGridMetadata<T> modelGridMetadata = initModelGridMetadata();
        final List<T> allElements = getAllElements();
        if (CollectionUtils.isEmpty(allElements)) {
            return modelGridMetadata;
        }
        for (final T element : allElements) {
            final LineModelGridMetadata lineModelGridMetadata = getNewLineModelGridMetadata(element);
            modelGridMetadata.getLinesModelGridMetadatas().add(lineModelGridMetadata);
        }
        return modelGridMetadata;
    }

    /**
     * Delete process bytes.
     *
     * @param data the data
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.IMetadataRecorder#deleteProcessBytes(byte[])
     */
    @Override
    public void deleteProcessBytes(final byte[] data) throws BusinessException {
        FileOutputStream fos = null;
        try {
            final String fileEncoding = Utils.detectStreamEncoding(data);
            final CSVParser parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(data), fileEncoding), AbstractCSVMetadataRecorder.PROPERTY_CST_CVS_SEPARATOR);
            final File file = File.createTempFile(AbstractCSVMetadataRecorder.PREFIX, AbstractCSVMetadataRecorder.SUFFIX);
            fos = new FileOutputStream(file);
            fos.write(data, 0, data.length);
            fos.flush();
            StreamUtils.closeStream(fos);
            deleteRecord(parser, file, fileEncoding);
        } catch (final BadDelimiterException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } finally {
            StreamUtils.closeStream(fos);
        }
    }

    /**
     * Delete record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     *      * use <i>values[i]</i> to access field</p>
     *      * don't use tokenizer because it register localization</p>
     */
    public abstract void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException;

    /**
     * Process bytes.
     *
     * @param data the data
     * @throws BusinessException the business exception
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#processBytes(byte[])
     */
    @Override
    public void processBytes(final byte[] data) throws BusinessException {
        try {
            final String fileEncoding = Utils.detectStreamEncoding(data);
            final CSVParser parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(data), fileEncoding), AbstractCSVMetadataRecorder.PROPERTY_CST_CVS_SEPARATOR);
            final File file = File.createTempFile(AbstractCSVMetadataRecorder.PREFIX, AbstractCSVMetadataRecorder.SUFFIX);
            try (FileOutputStream fos = new FileOutputStream(file);) {
                fos.write(data, 0, data.length);
                fos.flush();
                StreamUtils.closeStream(fos);
                processRecord(parser, file, fileEncoding);
            } catch (Exception ex) {
                throw new BusinessException(ex.getMessage(), ex);
            }
        } catch (final BadDelimiterException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Process file.
     *
     * @param file the file
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#processFile(java.io.File)
     */
    @Override
    public void processFile(final File file) throws BusinessException {
        try (
                final FileInputStream fis = new FileInputStream(file);
                final BufferedInputStream bufferedInputStream = new BufferedInputStream(fis);) {
            testFormat(file);
            final String fileEncoding = Utils.detectStreamEncoding(bufferedInputStream);
            try (final FileInputStream fis2 = new FileInputStream(file);
                    final InputStreamReader inputStreamReader = new InputStreamReader(fis2, fileEncoding);) {
                final CSVParser parser = new CSVParser(inputStreamReader, AbstractCSVMetadataRecorder.PROPERTY_CST_CVS_SEPARATOR);
                processRecord(parser, file, fileEncoding);
            } catch (final Exception e) {
                throw new BusinessException(e.getMessage(), e);
            }
        } catch (final BadDelimiterException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Process record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     *      * use <i>new TokenizerValues(values)</i> to get a tokeniser</p>
     *      * use <i>new TokenizerValues(values,
     * Nodeable.getLocalisationEntite(MyEntity.class));</i> to get a tokeniser
     * for an entity</p>
     *      * use <i>new TokenizerValues(values, "name of the table");</i> to get a
     * tokeniser for a localized entity</p>
     */
    public abstract void processRecord(CSVParser parser, File file, String encoding) throws BusinessException;

    /**
     * Process record only.
     *
     * @param file the file
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.IMetadataRecorder#processRecordOnly(java.io.File)
     */
    @Override
    public void processRecordOnly(final File file) throws BusinessException {
        try (
                final FileInputStream fis = new FileInputStream(file);
                final BufferedInputStream bufferedInputStream = new BufferedInputStream(fis);) {
            final String fileEncoding = Utils.detectStreamEncoding(bufferedInputStream);
            try (final FileInputStream fis2 = new FileInputStream(file);
                    final InputStreamReader inputStreamReader = new InputStreamReader(fis2, fileEncoding);) {
                final CSVParser parser = new CSVParser(inputStreamReader, AbstractCSVMetadataRecorder.PROPERTY_CST_CVS_SEPARATOR);
                processRecord(parser, file, fileEncoding);
            } catch (final IOException e) {
                throw new BusinessException(e.getMessage(), e);
            }
        } catch (final BadDelimiterException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see
     * org.inra.ecoinfo.localization.IInternationalizable#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Cette méthode est appelée depuis testFormat() de
     * AbstractCSVMetadataRecorder.
     *
     * @param badsFormatsReport the bads formats report
     * @param parser the parser
     * @return le nombre de ligne analysée
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(BadsFormatsReport) the bads formats report
     * @link(CSVParser) the parser
     */
    public int testHeader(final BadsFormatsReport badsFormatsReport, final CSVParser parser) throws IOException {
        int lineNumber = 0;
        final String[] values = parser.getLine();
        if (values == null) {
            badsFormatsReport.addException(new BusinessException(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_EMPTY_FILE)));
            return 0;
        } else if (getDatasetDescriptor().getUndefinedColumn() > 0) {
            if (values.length < getDatasetDescriptor().getUndefinedColumn()) {
                badsFormatsReport.addException(new BusinessException(String.format(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_ERROR_UNEXPECTED_NUMBER_OF_COLUMNS),
                        values.length, getDatasetDescriptor().getColumns().size())));
            }
        } else if (values.length != getDatasetDescriptor().getColumns().size()) {
            badsFormatsReport.addException(new BusinessException(String.format(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_ERROR_UNEXPECTED_NUMBER_OF_COLUMNS),
                    values.length, getDatasetDescriptor().getColumns().size())));
        }
        lineNumber++;
        for (int index = 0; index < values.length; index++) {
            if (index > getDatasetDescriptor().getColumns().size() - 1) {
                break;
            }
            final String value = values[index];
            final Column column = getDatasetDescriptor().getColumns().get(index);
            final String codifiedExpectedColumnName = Utils.createCodeFromString(column.getName());
            final String codifiedFileColumnName = Utils.createCodeFromString(value);
            if (!codifiedExpectedColumnName.equals(codifiedFileColumnName)) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_BAD_HEADER_COLUMN), index + 1, value,
                        column.getName().trim().toLowerCase())));
            }
        }
        return lineNumber;
    }

    /**
     * Check date value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkDateValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.DATE_TYPE.equalsIgnoreCase(column.getValueType().trim()) && value.length() > 0) {
            try {
                DateUtil.readLocalDateTimeFromText(Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY : column.getFormatType(), value);
            } catch (final DateTimeException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_DATE_VALUE_EXPECTED),
                        column.getFormatType(), lineNumber, i + 1, column.getName(), value), e));
            }
        }
    }

    /**
     * Check float value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkFloatValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.FLOAT_TYPE.equals(column.getValueType()) && value.length() > 0) {
            try {
                Float.parseFloat(value.replaceAll(AbstractCSVMetadataRecorder.COMMA, AbstractCSVMetadataRecorder.DOT));
            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_MISSING_FLOAT), lineNumber, i + 1,
                        column.getName(), value), e));
            }
        }
    }

    /**
     * Check float value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkDoubleValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.DOUBLE_TYPE.equals(column.getValueType()) && value.length() > 0) {
            try {
                Double.parseDouble(value.replaceAll(AbstractCSVMetadataRecorder.COMMA, AbstractCSVMetadataRecorder.DOT));
            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_MISSING_DOUBLE), lineNumber, i + 1,
                        column.getName(), value), e));
            }
        }
    }

    /**
     * Check integer value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkIntegerValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.INTEGER_TYPE.equals(column.getValueType()) && value.length() > 0) {
            try {
                Integer.parseInt(value);
            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_MISSING_INTEGER), lineNumber, i + 1,
                        column.getName(), value), e));
            }
        }
    }

    private void checkLineValues(final BadsFormatsReport badsFormatsReport, int lineNumber, String[] values) {
        for (int index = 0; index < values.length; index++) {
            if (index > getDatasetDescriptor().getColumns().size() - 1) {
                break;
            }
            final String value = values[index];
            final Column column = getDatasetDescriptor().getColumns().get(index);
            checkValue(badsFormatsReport, lineNumber, index, value, column);
        }
    }

    /**
     * Check nullable value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkNullableValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        if (!column.isNullable() && StringUtils.isEmpty(value)) {
            badsFormatsReport.addException(new NullValueException(String.format(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_MISSING_VALUE), lineNumber, i + 1,
                    column.getName())));
        }
    }

    /**
     * Check time value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkTimeValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.TIME_TYPE.equalsIgnoreCase(column.getValueType().trim()) && value.length() > 0) {
            try {
                DateUtil.readLocalDateTimeFromText(Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY : column.getFormatType(), value);
            } catch (final DateTimeException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_TIME_VALUE_EXPECTED),
                        column.getFormatType(), lineNumber, i + 1, column.getName(), value), e));
            }
        }
    }

    /**
     * Check value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        checkNullableValue(badsFormatsReport, lineNumber, i, value, column);
        checkValueByType(badsFormatsReport, lineNumber, i, value, column);
    }

    /**
     * Check value by type.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkValueByType(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        if (column.getValueType() != null) {
            checkDateValue(badsFormatsReport, lineNumber, i, value, column);
            checkTimeValue(badsFormatsReport, lineNumber, i, value, column);
            checkFloatValue(badsFormatsReport, lineNumber, i, value, column);
            checkDoubleValue(badsFormatsReport, lineNumber, i, value, column);
            checkIntegerValue(badsFormatsReport, lineNumber, i, value, column);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    protected abstract List<T> getAllElements() throws BusinessException;

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     */
    protected ModelGridMetadata<T> initModelGridMetadata() {
        final ModelGridMetadata<T> modelGridMetadata = new ModelGridMetadata<>(getDatasetDescriptor().getName(), this);
        getDatasetDescriptor().getColumns().stream().map((column) -> {
            modelGridMetadata.getHeadersColumns().add(column.getName());
            return column;
        }).forEach((column) -> {
            modelGridMetadata.getColumns().add(column);
        });
        return modelGridMetadata;
    }

    /**
     * Skip header.
     *
     * @param parser the parser
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(CSVParser) the parser
     */
    protected void skipHeader(final CSVParser parser) throws IOException {
        parser.getLine();
    }

    /**
     *
     * @param badsFormatsReport
     * @param parser
     * @param lineNumber
     * @return
     * @throws IOException
     */
    protected int testAllLinesFormat(final BadsFormatsReport badsFormatsReport, final CSVParser parser, int lineNumber) throws IOException {
        int returnLineNumber = lineNumber;
        String[] values = parser.getLine();
        while (values != null) {
            returnLineNumber++;
            checkLineValues(badsFormatsReport, returnLineNumber, values);
            values = parser.getLine();
        }
        return returnLineNumber;
    }

    /**
     * Test format.
     *
     * @param file the file
     * @throws BusinessException the business exception
     * @link(File) the file
     */
    protected void testFormat(final File file) throws BusinessException {
        try (
                final FileInputStream fis = new FileInputStream(file);
                final BufferedInputStream bufferedInputStream = new BufferedInputStream(fis);) {
            final BadsFormatsReport badsFormatsReport = new BadsFormatsReport(localizationManager.getMessage(BadsFormatsReport.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_ERROR));
            LOGGER.debug(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_CHECKING_FORMAT_FILE));
            final String encoding = Utils.detectStreamEncoding(bufferedInputStream);
            try (final FileInputStream fis2 = new FileInputStream(file);
                    final InputStreamReader inputStreamReader = new InputStreamReader(fis2, encoding);) {
                final CSVParser parser = new CSVParser(inputStreamReader, AbstractCSVMetadataRecorder.PROPERTY_CST_CVS_SEPARATOR);
                if (datasetDescriptor == null) {
                    datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(getClass().getResource(AbstractCSVMetadataRecorder.DATASET_DESCRIPTOR_XML).openStream());
                }
                final int lineNumber = testHeader(badsFormatsReport, parser);
                testAllLinesFormat(badsFormatsReport, parser, lineNumber);
                if (badsFormatsReport.hasErrors()) {
                    LOGGER.error(badsFormatsReport.getMessages());
                    throw new BusinessException(badsFormatsReport.getMessages(), new BadFormatException(badsFormatsReport));
                }
                LOGGER.debug(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_FILE_FORMAT_OK));
            } catch (final IOException e) {
                throw new BusinessException(e.getMessage(), e);
            }
        } catch (final FileNotFoundException e) {
            LOGGER.error(String.format(localizationManager.getMessage(AbstractCSVMetadataRecorder.BUNDLE_SOURCE_PATH, AbstractCSVMetadataRecorder.PROPERTY_MSG_MISSING_FILE), AbstractCSVMetadataRecorder.DATASET_DESCRIPTOR_XML));
            throw new BusinessException(e.getMessage(), e);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param file
     * @param configurator
     * @param codeConfiguration
     * @param skipHeader
     * @return
     */
    protected Stream<String> buildOrderedPathes(final File file, IMgaIOConfigurator configurator, Integer codeConfiguration, boolean skipHeader) {
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        return mgaBuilder.buildOrderedPaths(file.getAbsolutePath(),
                configurator.getConfiguration(codeConfiguration).map(code -> code.getEntryOrder()).orElse(new Integer[0]),
                PatternConfigurator.SPLITER, skipHeader);
    }

    /**
     *      * from a list af ordered pathes build a list of INode leaves</p>
     *
     * @param buildOrderedPaths
     * @param codeConfiguration
     * @return
     * @throws javax.persistence.PersistenceException
     */
    protected Stream<INode> buildLeaves(Stream<String> buildOrderedPaths, Integer codeConfiguration) throws javax.persistence.PersistenceException {
        IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        IMgaRecorder mgaRecorder = mgaServiceBuilder.getRecorder();
        List<INodeable> stickyNodeables = null;
        final Optional<IMgaIOConfiguration> configuration = configurator.getConfiguration(codeConfiguration);
        if (configuration.map(conf -> conf.getStickyLeafType() != null).orElse(false)) {
            stickyNodeables = mgaRecorder.getNodeables(configuration.get().getStickyLeafType())
                    .collect(Collectors.toList());
        }
        Map<String, INodeable> entities = mgaRecorder.getNodeables()
                .collect(Collectors.toMap(nodeable -> MgaBuilder.getUniqueCode(nodeable, PatternConfigurator.ANCESTOR_SEPARATOR), n -> n));
        return mgaBuilder.buildLeavesForPathes(
                buildOrderedPaths,
                configuration.map(conf -> conf.getEntryType()).orElse(new Class[0]),
                entities,
                WhichTree.TREEDATASET,
                stickyNodeables);
    }

    /**
     *
     * @param listChild
     * @param parser
     */
    protected void persistNodes(Stream<INode> listChild, final CSVParser parser) {
        IMgaRecorder mgaRecorder = mgaServiceBuilder.getRecorder();
        listChild.forEach((node) -> {
            mgaRecorder.persist(node);
            if (parser.getLastLineNumber() % 50 == 0) {
                mgaRecorder.getEntityManager().flush();
                mgaRecorder.getEntityManager().clear();
            }
        });
    }

    /**
     *
     * @param mgaServiceBuilder
     */
    public void setMgaServiceBuilder(IMgaServiceBuilder mgaServiceBuilder) {
        this.mgaServiceBuilder = mgaServiceBuilder;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param treeApplicationCacheManager
     */
    public void setTreeApplicationCacheManager(ITreeApplicationCacheManager treeApplicationCacheManager) {
        this.treeApplicationCacheManager = treeApplicationCacheManager;
    }


    protected DatasetDescriptor getDatasetDescriptor() {
        return datasetDescriptor;
    }

    /**
     *      * Lets link child columns (<i>columnsChildren</i>) to their parent
     * (<i>columnParent</i>) : a=>b; a=>c; a=>d ...
     *
     * @param columnParent the parent column
     * @param columnsChildren the list children columns to be linked
     */
    protected void setColumnRef(ColumnModelGridMetadata columnParent, List<ColumnModelGridMetadata> columnsChildren) {
        columnParent.setRefs(columnsChildren);
        for (int i = 0; i < columnsChildren.size(); i++) {
            String childValue = columnsChildren.get(i).getValue();
            columnsChildren.get(i).setRefBy(columnParent);
            columnsChildren.get(i).setValue(childValue != null ? childValue : "");
        }
    }

    /**
     *      * For cascaded menus, this method binds a set of child columns starting
     * with their parent. For example, for four columns a (parent) and b, c, d
     * (children) such that a => b => c => d
     *
     * @param columnParent the first column
     * @param columnsToBeLinked the following columns
     */
    protected void setColumnRefRecursive(Deque<ColumnModelGridMetadata> columnsToBeLinked) {
        ColumnModelGridMetadata columnParent = columnsToBeLinked.poll();
        columnParent.setRefs(new LinkedList(columnsToBeLinked));
        ColumnModelGridMetadata columnChild = columnsToBeLinked.peek();
        if (columnChild == null) {
            return;
        }
        String childValue = columnChild.getValue();
        columnChild.setRefBy(columnParent);
        columnChild.setValue(childValue != null ? childValue : "");
        if (columnsToBeLinked.isEmpty()) {
            return;
        }
        setColumnRefRecursive(columnsToBeLinked);
    }

    /**
     *      * This feature allows linked menus to create the necessary objects for the
     * bindings in the <i>getNewLineModelGridMetadata</i> step, when creating
     * the
     * <i>ColumnModelGridMetadata</i>.
     *      * The first linked menu expects a String[] as valuesPossibles, the linked
     * menus expect them a Map<String, String[]>. The method populates the
     * Maps(<i>listOfMapOfValuesPossibles</i> parameter) and returns the
     * String[].
     *      * For example, if an animal store has menus to choose the type, species,
     * and animal code, and these three menus are linked. We will provide a
     * mapOfValues = {"dog": {"cocker": ["21", "13"], "chiwawa", [15 "," 19
     * "]}," cat ": {" siamese ": [" 30 "]," Persian ": [" 31 "," 20 "," 15 "]}}
     * This map is built upstream from the contents of the database.
     *      * The result will be String [] {"dog", "cat"} for valuesPossibles of the
     * first column,
     *      * The first element of listOfMapOfValuesPossibles will contain {"dog":
     * ["cocker", "chiwawa"], "cat": ["siamese," Persian "]} for possible values
     * of the second column,
     *      * The second item of listOfMapOfValuesPossibles will contain {"dog /
     * cocker": ["21", "13"], "dog / chihuahua": [15 "," 19 "]," cat /
     * "siamese": ["30"] , "cat / persian": ["31", "20", "15"]} for possible
     * values of the third column
     *
     * @param mapOfValues is a tree representation of linked menus. For example,
     * 3 linked menus (a => b => c) will be represented by a Map <String, Map
     * <String, List <String >>>.
     * @param listOfMapOfValuesPossibles the list of Map <String, String []>
     * for the linked menus in the order of the links (in the example for the
     * menu b, then the c)
     * @param valuesForKey we supply an empty list and it is used in recurrence.
     * @return the String[] for the first menu
     */
    protected String[] readMapOfValuesPossibles(Map<String, ? extends Object> mapOfValues, List<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles, List<String> valuesForKey) {
        for (Entry<String, ? extends Object> entry : mapOfValues.entrySet()) {
            String key = entry.getKey();
            Object childValues = entry.getValue();
            ConcurrentMap<String, String[]> mapOfValuesPossibles = listOfMapOfValuesPossibles.get(0);
            Collection<String> values = null;
            if (childValues instanceof Map) {
                values = ((Map) childValues).keySet();
            } else if (childValues instanceof List) {
                values = (Collection<String>) childValues;
            }
            List<String> newValueForKey = new LinkedList(valuesForKey);
            if (values != null) {
                newValueForKey.add(key);
                mapOfValuesPossibles.put(newValueForKey.stream().collect(Collectors.joining("/")), values.toArray(new String[]{}));
            }
            if (childValues instanceof Map) {
                readMapOfValuesPossibles((Map<String, Object>) childValues, listOfMapOfValuesPossibles.stream().skip(1).collect(Collectors.toList()), newValueForKey);
            }

        }
        return mapOfValues.keySet().toArray(new String[]{});
    }
    /**
     * The Class ErrorsReport.
     */
    public class ErrorsReport {

        /**
         * The Constant NEW_LINE @link(String).
         */
        private static final String NEW_LINE = "\n";
        /**
         * The errors messages @link(String).
         */
        private String errorsMessages = "";

        /**
         * Adds the error message.
         *
         * @param errorMessage the error message
         * @link(String) the error message
         */
        public void addErrorMessage(final String errorMessage) {
            errorsMessages = errorsMessages.concat(AbstractCSVMetadataRecorder.DOT).concat(errorMessage).concat(ErrorsReport.NEW_LINE);
        }

        /**
         *
         * @param errorMessage
         * @param e
         */
        public void addErrorMessage(final String errorMessage, Exception e) {
            UncatchedExceptionLogger.logUncatchedException(e.getLocalizedMessage(), e);
            addErrorMessage(errorMessage);
        }

        /**
         * Gets the errors messages.
         *
         * @return the errors messages
         */
        public String getErrorsMessages() {
            return errorsMessages;
        }

        /**
         * Checks for errors.
         *
         * @return true, if successful
         */
        public boolean hasErrors() {
            return errorsMessages.length() > 0;
        }
    }
    /**
     * The Class TokenizerValues.
     */
    protected class TokenizerValues {

        /**
         * The Constant PROPERTY_SUFFIX_LANGUAGE @link(String).
         */
        private static final String PROPERTY_SUFFIX_LANGUAGE = "_%s$";
        /**
         * The column iterator @link(Iterator<Column>).
         */
        private Iterator<Column> columnIterator;
        /**
         * The entity @link(String).
         */
        private String entity;
        /**
         * The value index @link(int).
         */
        private int valueIndex = 0;
        /**
         * The values @link(String[]).
         */
        private String[] values;

        /**
         * Instantiates a new tokenizer values.
         *
         * @param val the val
         * @link(String[]) the val
         */
        public TokenizerValues(final String[] val) {
            super();
            if (val == null) {
                this.values = new String[0];
            } else {
                this.values = Arrays.copyOf(val, val.length);
            }
        }

        /**
         * Instantiates a new tokenizer values.
         *
         * @param val the val
         * @param entity the entity
         * @link(String[]) the val
         * @link(String) the entity
         */
        public TokenizerValues(final String[] val, final String entity) {
            if (val == null) {
                this.values = new String[0];
            } else {
                this.values = Arrays.copyOf(val, val.length);
            }
            this.entity = entity;
            if (datasetDescriptor != null) {
                columnIterator = getDatasetDescriptor().getColumns().iterator();
            }
        }

        /**
         *
         * @return
         */
        protected boolean initGenericNodeable() {
            Type genericSuperclass = AbstractCSVMetadataRecorder.this.getClass().getGenericSuperclass();
            while (!(genericSuperclass instanceof ParameterizedType)) {
                genericSuperclass = ((Class) genericSuperclass).getGenericSuperclass();
            }
            entityClass = (Class<T>) ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
            return ClassUtils.getAllInterfaces(entityClass).contains(INodeable.class);
        }

        /**
         * Convert to float.
         *
         * @param value the value
         * @return the float
         * @link(String) the value
         */
        private Float convertToFloat(final String value) {
            final String val = value.replaceAll(AbstractCSVMetadataRecorder.COMMA, AbstractCSVMetadataRecorder.DOT);
            if (val.isEmpty()) {
                return null;
            } else {
                return Float.parseFloat(val);
            }
        }

        /**
         * Current token.
         *
         * @return the string
         */
        public String currentToken() {
            final String value = values[valueIndex].trim();
            if (value.isEmpty()) {
                return null;
            } else {
                return value;
            }
        }

        /**
         * Current token float.
         *
         * @return the float
         */
        public Float currentTokenFloat() {
            return convertToFloat(currentToken());
        }

        /**
         * Current token index.
         *
         * @return the int
         */
        public int currentTokenIndex() {
            return valueIndex;
        }

        /**
         *
         * @param defaultString
         * @param localizationString
         * @param colonne
         * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
         */
        public void getLocalizedNextTokens(String defaultString, final String localizationString, final Column colonne) throws BusinessException {
            if (hasGenericNodeable == null && entityClass == null) {
                hasGenericNodeable = initGenericNodeable();
            }
            Localization localization;
            Optional<Localization> localizationOpt = localizationManager.getByNKey(localizationString, hasGenericNodeable ? Nodeable.getLocalisationEntite(entityClass) : entity, colonne.getFieldName().replaceAll(String.format(TokenizerValues.PROPERTY_SUFFIX_LANGUAGE, localizationString), ""), defaultString);
            if (localizationOpt.isPresent()) {
                localization = localizationOpt.get();
                localization.setLocaleString(currentToken() == null ? defaultString : currentToken());
            } else {
                localization = new Localization(
                        localizationString,
                        hasGenericNodeable ? Nodeable.getLocalisationEntite(entityClass) : entity,
                        colonne.getFieldName().replaceAll(String.format(TokenizerValues.PROPERTY_SUFFIX_LANGUAGE, localizationString), ""),
                        defaultString,
                        currentToken() == null ? defaultString : currentToken());
            }
            valueIndex++;
            localizationManager.saveLocalization(localization);
            if (hasGenericNodeable != null && hasGenericNodeable) {
                getLocalizedNextTokens(localization, colonne);
            }

        }
        public String nextToken(boolean codifieLocalization) throws BusinessException {
            String result;
            if (columnIterator == null) {
                result = currentToken();
                valueIndex++;
            } else {
                result = nextTokens(codifieLocalization);
            }
            return result;
        }

        /**
         * Next token.
         *
         * @return the string
         * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
         */
        public String nextToken() throws BusinessException {
            String result;
            if (columnIterator == null) {
                result = currentToken();
                valueIndex++;
            } else {
                result = nextTokens(false);
            }
            return result;
        }

        /**
         * Next token float.
         *
         * @return the float
         */
        public Float nextTokenFloat() {
            if (columnIterator != null && columnIterator.hasNext()) {
                columnIterator.next();
            }
            return convertToFloat(values[valueIndex++].trim());
        }

        /**
         * Next tokens.
         *
         * @return the string
         * @throws PersistenceException the persistence exception
         */
        private String nextTokens(boolean codifieLocalization) throws BusinessException {
            String defaultString = currentToken();
            for (final String localizationString : Localization.getLocalisations()) {
                Column colonne = columnIterator.next();
                if (!colonne.isLocalizable()) {
                    valueIndex++;
                    break;
                }
                if (defaultString == null) {
                    defaultString = "";
                }
                final boolean isDefaultColumn = !colonne.isKey() && Localization.getDefaultLocalisation().equals(localizationString);
                if (isDefaultColumn) {
                    valueIndex++;
                } else {
                    if (colonne.isKey() && colonne.getName().endsWith("_key")) {
                        valueIndex++;
                        colonne = columnIterator.next();
                    }
                    getLocalizedNextTokens(codifieLocalization?Utils.createCodeFromString(defaultString):defaultString, localizationString, colonne);
                }
            }
            return defaultString;
        }

        private void getLocalizedNextTokens(Localization localization, Column colonne) throws BusinessException {
            if (hasGenericNodeable == null && entityClass == null) {
                hasGenericNodeable = initGenericNodeable();
            }
            String type = Nodeable.getLocalisationEntite(entityClass);
            String column=localization.getColonne();
            if("nom".equals(column)){
                column = Nodeable.ENTITE_COLUMN_NAME;
            }
            Optional<Localization> localizationOpt = localizationManager.getByNKey(localization.getLocalization(), type, column, localization.getDefaultString());
            if (localizationOpt.isPresent()) {
                localizationOpt.get().setLocaleString(localization.getLocaleString());
            } else {
                Localization dbLocalization = new Localization(
                        localization.getLocalization(),
                        type,
                        column,
                        localization.getDefaultString(),
                        localization.getLocaleString());
                localizationManager.saveLocalization(dbLocalization);
            }
        }
    }
}
