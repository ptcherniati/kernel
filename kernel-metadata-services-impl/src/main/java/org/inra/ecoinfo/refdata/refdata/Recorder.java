/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.refdata.refdata;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<Refdata> {

    /**
     * The properties description en @link(Properties).
     */
    private Properties propertiesDescriptionEN;
    /**
     * The properties nom en @link(Properties).
     */
    private Properties propertiesNomFR;
    private Properties propertiesNomEN;
    /**
     * The refdata dao @link(IRefdataDAO).
     */
    protected IRefdataDAO refdataDAO;

    /**
     * Delete record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                Refdata refdata = refdataDAO.getByCode(values[0])
                        .orElse(
                                refdataDAO.getByCode(Utils.createCodeFromString(values[0])).orElse(null)
                        );
                if (refdata != null) {
                    refdataDAO.remove(refdata);
                } else {
                    throw new BusinessException("can't get refdata");
                }
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param refdata the refdata
     * @return the new line model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final Refdata refdata) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(refdata == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : refdata.getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(refdata == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomFR.getProperty(refdata.getName(), refdata.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(refdata == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomEN.getProperty(refdata.getName(), refdata.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(refdata == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : refdata.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(refdata == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesDescriptionEN.getProperty(refdata.getDescription(), refdata.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }

    /**
     * Process record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(Refdata.class));
                final String code = tokenizerValues.nextToken();
                final String description = tokenizerValues.nextToken();
                persistRefdata(code, description);
                values = parser.getLine();
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the refdata dao.
     *
     * @param refdataDAO the new refdata dao
     */
    public void setRefdataDAO(final IRefdataDAO refdataDAO) {
        this.refdataDAO = refdataDAO;
    }

    /**
     * Creates the or update refdata.
     *
     * @param refdataPath the refdata path
     * @param description the description
     * @param dbRefdata the db refdata
     * @throws PersistenceException the persistence exception
     * @link(String) the refdata path
     * @link(String) the description
     * @link(Refdata) the db refdata
     */
    private PersistenceException createOrUpdateRefdata(final String refdataPath, final String description, final Refdata dbRefdata) {
        if (dbRefdata == null) {
            try {
                createRefdata(refdataPath, description);
            } catch (PersistenceException ex) {
                return ex;
            }
        } else {
            updateRefdata(description, dbRefdata);
        }
        return null;
    }

    /**
     * Creates the refdata.
     *
     * @param refdataNom refdataPath refdata nom
     * @param description the description
     * @throws PersistenceException the persistence exception
     * @link(String) the refdata nom
     * @link(String) the description
     */
    private void createRefdata(final String refdataPath, final String description) throws PersistenceException {
        final Refdata refdata = new Refdata(refdataPath, description);
        refdataDAO.saveOrUpdate(refdata);

    }

    /**
     * Persist refdata.
     *
     * @param refdataPath the refdata path
     * @param description the description
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the type refdata code
     * @link(String) the refdata path
     * @link(String) the description
     */
    private void persistRefdata(final String refdataPath, final String description) throws PersistenceException {
        Refdata dbRefdata = refdataDAO.getByCode(refdataPath)
                .orElse(
                        refdataDAO.getByCode(Utils.createCodeFromString(refdataPath)).orElse(null)
                );
        PersistenceException ex = createOrUpdateRefdata(refdataPath, description, dbRefdata);
        if (ex != null) {
            throw ex;
        }
    }

    /**
     * Update refdata.
     *
     * @param description the description
     * @param dbRefdata the db refdata
     * @link(String) the description
     * @link(Refdata) the db refdata
     */
    private void updateRefdata(final String description, final Refdata dbRefdata) {
        dbRefdata.setDescription(description);
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Refdata> getAllElements() throws BusinessException {
        return refdataDAO.getAll(Refdata.class);
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<Refdata> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(Nodeable.getLocalisationEntite(Refdata.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE);
        propertiesNomEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(Refdata.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(Refdata.class), "description", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
}
