package org.inra.ecoinfo.refdata.deserialization;

/**
 * The Interface IIdSerialization.
 */
public interface IMetadataDeserialization {

    /**
     *
     * @return
     */
    String getId();
}
