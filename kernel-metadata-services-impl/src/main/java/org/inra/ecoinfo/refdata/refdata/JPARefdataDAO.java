/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.refdata.refdata;

import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author yahiaoui
 */
public class JPARefdataDAO extends AbstractJPADAO<Refdata> implements IRefdataDAO {

    @Override
    public Stream<Refdata> getAll() {
        CriteriaQuery<Refdata> query = builder.createQuery(Refdata.class);
        Root<Refdata> refdata = query.from(Refdata.class);
        query.select(refdata);
        return getResultListToStream(query);
    }

    @Override
    public Optional<Refdata> getByCode(String code) {CriteriaQuery<Refdata> query = builder.createQuery(Refdata.class);
            Root<Refdata> ref = query.from(Refdata.class);
            query.where(
                    builder.equal(ref.get(Refdata_.code), code)
            );
            query.select(ref);
            return getOptional(query);
    }

    @Override
    public void remove(Refdata object) throws PersistenceException {
        super.remove(object); 
    }

}
