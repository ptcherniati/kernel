/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.refdata.refdata;

import java.util.Optional;
import java.util.stream.Stream;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author yahiaoui
 */
public interface IRefdataDAO extends IDAO<Refdata>  {
   
    
    /**
     * Gets the all.
     * 
     * @return the all
     */
    Stream<Refdata> getAll();

    /**
     * Gets the by code.
     * 
     * @param code
     *            the code
     * @return the by code
     * @link(String) the code
     */
    Optional<Refdata> getByCode(String code);
}
