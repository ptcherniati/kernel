package org.inra.ecoinfo.refdata.valeurqualitative;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<IValeurQualitative> {

    /**
     * The properties valeur fr @link(Properties).
     */
    private Properties propertiesNomFR;

    /**
     * The properties valeur en @link(Properties).
     */
    private Properties propertiesNomEN;
    /**
     * The properties valeur fr @link(Properties).
     */
    private Properties propertiesValeurFR;

    /**
     * The properties valeur en @link(Properties).
     */
    private Properties propertiesValeurEN;
    /**
     * The valeur qualitative dao @link(IValeurQualitativeDAO).
     */
    protected IValeurQualitativeDAO valeurQualitativeDAO;

    /**
     * Delete record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = parser.getLine();
            while (values != null) {
                final String code = Utils.createCodeFromString(values[0]);
                final String valeur = Utils.createCodeFromString(values[3]);
                valeurQualitativeDAO.remove(valeurQualitativeDAO.getByCodeAndValue(code, valeur).orElseThrow(() -> new BusinessException("can't get unite")));
                if (errorsReport.hasErrors()) {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param valeurQualitative the valeur qualitative
     * @return the new line model grid metadata
     * @throws BusinessException the persistence exception
     * @see
     * org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final IValeurQualitative valeurQualitative) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(valeurQualitative == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : valeurQualitative.getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(valeurQualitative == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomFR.getProperty(valeurQualitative.getCode(), valeurQualitative.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(valeurQualitative == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomEN.getProperty(valeurQualitative.getCode(), valeurQualitative.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(valeurQualitative == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : valeurQualitative.getValeur(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(valeurQualitative == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesValeurFR.getProperty(valeurQualitative.getValeur(), valeurQualitative.getValeur()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(valeurQualitative == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesValeurEN.getProperty(valeurQualitative.getValeur(), valeurQualitative.getValeur()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Process record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            int lineNumber = 0;
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, IValeurQualitative.NAME_ENTITY_JPA);
                lineNumber++;
                final String nom = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String valeur = Utils.createCodeFromString(tokenizerValues.nextToken());
                persistQualitativeVariable(valeur, nom);
                values = parser.getLine();
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | BusinessException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the valeur qualitative dao.
     *
     * @param valeurQualitativeDAO the new valeur qualitative dao
     */
    public void setValeurQualitativeDAO(final IValeurQualitativeDAO valeurQualitativeDAO) {
        this.valeurQualitativeDAO = valeurQualitativeDAO;
    }

    /**
     * Persist qualitative variable.
     *
     * @param valeur the valeur
     * @param nom the nom
     * @throws BusinessException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the valeur
     * @link(String) the nom
     */
    private void persistQualitativeVariable(final String valeur, final String nom) throws BusinessException {
        if (!valeurQualitativeDAO.getByCodeAndValue(Utils.createCodeFromString(nom), valeur).isPresent()) {
            final IValeurQualitative valeurQualitative = new ValeurQualitative(nom, valeur);
            try {
                valeurQualitativeDAO.saveOrUpdate(valeurQualitative);
            } catch (PersistenceException ex) {
                throw new BusinessException(ex);
            }
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<IValeurQualitative> getAllElements() {
        return valeurQualitativeDAO.getAll();
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<IValeurQualitative> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(IValeurQualitative.NAME_ENTITY_JPA, IValeurQualitative.ATTRIBUTE_JPA_NAME, Locale.FRANCE);
        propertiesNomEN = localizationManager.newProperties(IValeurQualitative.NAME_ENTITY_JPA, IValeurQualitative.ATTRIBUTE_JPA_NAME, Locale.ENGLISH);
        propertiesValeurFR = localizationManager.newProperties(IValeurQualitative.NAME_ENTITY_JPA, IValeurQualitative.ATTRIBUTE_JPA_VALUE, Locale.FRANCE);
        propertiesValeurEN = localizationManager.newProperties(IValeurQualitative.NAME_ENTITY_JPA, IValeurQualitative.ATTRIBUTE_JPA_VALUE, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
}
