/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.refdata.deserialization.impl;

import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.deserialization.impl.AbstractActivityDeserialization;

/**
 *
 * @author tcherniatinsky
 */
public class MetadataActivitiesDeserialization extends AbstractActivityDeserialization{

    /**
     *
     */
    public static final String PRIVILEGES_ENTRY = "activities_metadatas.csv";
    private static final String MODULE_NAME = "metadataActivityDeserialization";

    /**
     *
     * @return
     */
    @Override
    public String getActivityEntry() {
        return  PRIVILEGES_ENTRY;
    }

    /**
     *
     * @return
     */
    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }
    
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        super.createConfig(digester);
    }
    
}
