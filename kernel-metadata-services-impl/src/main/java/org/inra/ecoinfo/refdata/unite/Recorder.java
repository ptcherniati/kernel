/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.refdata.unite;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<Unite> {

    /**
     * The properties nom fr @link(Properties).
     */
    private Properties propertiesCodeFR;

    /**
     * The properties nom en @link(Properties).
     */
    private Properties propertiesCodeEN;
    /**
     * The properties nom fr @link(Properties).
     */
    private Properties propertiesNomFR;

    /**
     * The properties nom en @link(Properties).
     */
    private Properties propertiesNomEN;
    /**
     * The unite dao @link(IUniteDAO).
     */
    protected IUniteDAO uniteDAO;

    /**
     * Delete record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final String code = Utils.createCodeFromString(values[0]);
                uniteDAO.remove(uniteDAO.getByCode(code).orElseThrow(() -> new BusinessException("can't get unite")));
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param unite the unite
     * @return the new line model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final Unite unite) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(unite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : unite.getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(unite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesCodeFR.getProperty(unite.getCode(),unite.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(unite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesCodeEN.getProperty(unite.getCode(),unite.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(unite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : unite.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(unite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomFR.getProperty(unite.getName(),unite.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(unite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomEN.getProperty(unite.getName(),unite.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Process record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            int lineNumber = 0;
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Unite.NAME_ENTITY_JPA);
                lineNumber++;
                final String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String nom = tokenizerValues.nextToken();
                Optional<Unite> dbUnite = uniteDAO.getByCode(code);
                if (!dbUnite.isPresent()) {
                    final Unite unite = new Unite(code, nom);
                    uniteDAO.saveOrUpdate(unite);
                } else {
                    dbUnite.get().setName(nom);
                }
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the unite dao.
     *
     * @param uniteDAO the new unite dao
     */
    public void setUniteDAO(final IUniteDAO uniteDAO) {
        this.uniteDAO = uniteDAO;
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Unite> getAllElements() {
        final List<Unite> all = uniteDAO.getAll();
        Collections.sort(all, (Unite o1, Unite o2) -> {
            if (o1 == null || o2 == null) {
                return o1 == o2 ? 0 : -1;
            }
            return o1.getCode().compareTo(o2.getCode());
        });
        return all;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<Unite> initModelGridMetadata() {
        propertiesCodeFR = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, Unite.ATTRIBUTE_JPA_CODE, Locale.FRANCE);
        propertiesCodeEN = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, Unite.ATTRIBUTE_JPA_CODE, Locale.ENGLISH);
        propertiesNomFR = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, Unite.ATTRIBUTE_JPA_NAME, Locale.FRANCE);
        propertiesNomEN = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, Unite.ATTRIBUTE_JPA_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
}
