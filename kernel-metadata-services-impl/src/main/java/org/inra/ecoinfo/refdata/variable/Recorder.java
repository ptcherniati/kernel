/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.refdata.variable;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class Recorder.
 *
 * @author "Philippe"
 */
public class Recorder extends AbstractCSVMetadataRecorder<Variable> {

    /**
     * The properties nom fr @link(Properties).
     */
    private Properties propertiesNomFR;

    /**
     * The properties nom en @link(Properties).
     */
    private Properties propertiesNomEN;
    /**
     * The properties definition en @link(Properties).
     */
    private Properties propertiesDefinitionEN;
    /**
     * The variable dao @link(IVariableDAO).
     */
    protected IVariableDAO variableDAO;

    /**
     * Builds the model grid.
     *
     * @return the model grid metadata
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#buildModelGrid()
     */
    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Transactional(propagation = Propagation.REQUIRED)
    public ModelGridMetadata buildModelGrid() throws BusinessException {
        final ModelGridMetadata modelGridMetadata = initModelGridMetadata();
        final List<Variable> variables = variableDAO.getAll(Variable.class);
        variables.stream().map((variable) -> {
            final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
            lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
            lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable.getDefinition(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
            return lineModelGridMetadata;
        }).forEach((lineModelGridMetadata) -> {
            modelGridMetadata.getLinesModelGridMetadatas().add(lineModelGridMetadata);
        });
        return modelGridMetadata;
    }

    /**
     * Delete record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final String code = Utils.createCodeFromString(values[0]);
                variableDAO.remove(variableDAO.getByCode(code).orElseThrow(() -> new BusinessException("can't get variable")));
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param variable the variable
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see
     * org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final Variable variable) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        final String localizedFRName = propertiesNomFR.getProperty(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getName(), variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getName());
        final String localizedENName = propertiesNomEN.getProperty(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getName(), variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getName());
        final String localizedDefinition = propertiesDefinitionEN.getProperty(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getDefinition(),
                variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getDefinition());
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : localizedFRName, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : localizedENName, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getDefinition(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : localizedDefinition, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? "false" : variable.getIsQualitative().toString(), new String[]{"true", "false"}, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Process record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            processValues(parser);
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     */
    public void setVariableDAO(final IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     * Creates the variable.
     *
     * @param variable the variable
     * @throws BusinessException the persistence exception
     * @link(Variable) the variable
     */
    private void createVariable(final Variable variable) throws BusinessException {
        try {
            variableDAO.saveOrUpdate(variable);
        } catch (PersistenceException ex) {
            throw new BusinessException(ex);
        }
    }

    /**
     * Persist variable.
     *
     * @param nom the nom
     * @param code the code
     * @param definition the definition
     * @param isQualitative the is qualitative
     * @throws BusinessException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the nom
     * @link(String) the code
     * @link(String) the definition
     * @link(Boolean) the is qualitative
     * @link(int) the line number
     */
    private void persistVariable(final String code, final String definition, final Boolean isQualitative) throws BusinessException {
        Optional<Variable> dbVariable = variableDAO.getByCode(code);

        if (!dbVariable.isPresent()) {
            final Variable variable = new Variable(code, definition, code, isQualitative);
            createVariable(variable);
        } else {
            updateDBVariable(dbVariable.get(), code, definition, isQualitative);
        }
    }

    private void processValues(final CSVParser parser) throws IOException, BusinessException, PersistenceException {
        String[] values = parser.getLine();
        int lineNumber = 0;
        while (values != null) {
            lineNumber++;
            final TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(Variable.class));
            final String code = tokenizerValues.nextToken();
            final String definition = tokenizerValues.nextToken();
            final String isQualitativeString = tokenizerValues.nextToken();
            Boolean isQualitative = false;
            if ("true".equalsIgnoreCase(isQualitativeString) || "t".equals(isQualitativeString)) {
                isQualitative = true;
            }
            persistVariable(code, definition, isQualitative);
            values = parser.getLine();
        }
    }

    /**
     * Update db variable.
     *
     * @param variable the variable
     * @param dbVariable the db variable
     * @link(Variable) the variable
     * @link(Variable) the db variable
     */
    private void updateDBVariable(final Variable dbVariable, String code, String definition, boolean isQualitative) {
        dbVariable.setCode(code);
        dbVariable.setDefinition(definition);
        dbVariable.setIsQualitative(isQualitative);
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @throws BusinessException the persistence exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Variable> getAllElements() throws BusinessException {
        final List<Variable> all = variableDAO.getAll();
        Collections.sort(all, (Variable o1, Variable o2) -> {
            if (o1 == null || o2 == null) {
                return o1 == o2 ? 0 : -1;
            }
            return o1.getCode().compareTo(o2.getCode());
        });
        return all;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<Variable> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(Nodeable.getLocalisationEntite(Variable.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE);
        propertiesNomEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(Variable.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        propertiesDefinitionEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(Variable.class), "definition", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
}
