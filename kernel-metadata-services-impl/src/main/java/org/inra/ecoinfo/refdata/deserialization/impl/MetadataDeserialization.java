package org.inra.ecoinfo.refdata.deserialization.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.deserialization.AbstractDeserialization;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.IMetadataRecorder;
import org.inra.ecoinfo.refdata.IMetadataRecorderDAO;
import org.inra.ecoinfo.refdata.config.IMetadataConfiguration;
import org.inra.ecoinfo.refdata.deserialization.IMetadataDeserialization;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;

/**
 * The Class MailAdminDeserialization.
 */
public class MetadataDeserialization extends AbstractDeserialization implements IMetadataDeserialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/metadataDeserialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "metadataDeserialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(MetadataDeserialization.class.getName());

    /**
     *
     */
    public static final String METADATA_ENTRY = "metadata";

    /**
     *
     */
    public static final String HEADER_CSV_FILE = "id;active;email;emploi;isroot;language;login;nom;password;poste;prenom";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    /**
     *
     */
    public static final String START_FILE_NAME_PATTERN = "metadata/";
    private String id;
    /**
     * The mail admin.
     */
    private IUtilisateurDAO utilisateurDAO;
    private IMetadataConfiguration metadataConfiguration;
    private IMetadataRecorderDAO metadataRecorderDAO;
    private ICoreConfiguration configuration;

    /**
     *
     */
    protected IMetadataManager metadataManager;
    private Map<String, File> files = new HashMap();

    /**
     *
     * @param metadataManager
     */
    public void setMetadataManager(IMetadataManager metadataManager) {
        this.metadataManager = metadataManager;
    }

    /**
     *
     * @param configuration
     */
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     *
     * @param metadataConfiguration
     */
    public void setMetadataConfiguration(IMetadataConfiguration metadataConfiguration) {
        this.metadataConfiguration = metadataConfiguration;
    }

    /**
     *
     * @param metadataRecorderDAO
     */
    public void setMetadataRecorderDAO(IMetadataRecorderDAO metadataRecorderDAO) {
        this.metadataRecorderDAO = metadataRecorderDAO;
    }
    
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        super.createConfig(digester);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#getModuleName()
     */

    /**
     *
     * @return
     */

    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractDeserialization#getSchemaPath()
     */

    /**
     *
     * @return
     */

    @Override
    public String getSchemaPath() {
        return SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#isAutoLoad()
     */

    /**
     *
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param inZip
     * @throws BusinessException
     */
    @Override
    public void deSerialize(File inZip) throws BusinessException {
        LOGGER.info("*************************************************************");
        LOGGER.info("*                déserialization metadata                   *");
        LOGGER.info("*************************************************************");
        LOGGER.info("");
        LOGGER.info(getClass().getName());
        LOGGER.info("");
        LOGGER.info("");
        copyMetadataFiles(inZip);
        TransactionStatus tr = utilisateurDAO.beginDefaultTransaction();
        files.forEach((e, f) -> LOGGER.info("pr\u00e9sence du fichier : {} -> {}", e, f == null ? "null" : f.getPath()));
        metadataConfiguration.getMetadatas().stream()
                .forEach(m -> readFiles(m.getCode()));
        utilisateurDAO.commitTransaction(tr);

    }

    private void readFiles(String metadataCode) {
        if (!files.containsKey(metadataCode)) {
            LOGGER.info(String.format("no file %s", metadataCode));
            return;
        }
        final File file = files.get(metadataCode);
        LOGGER.info("upload file {}", file.getName());
        uploadFile(metadataCode, file);
        LOGGER.info(String.format("metadata %s registered", metadataCode));
    }

    private void uploadFile(String metadataCode, File file) {
        try {
            LOGGER.info(String.format("uploading %s %s", metadataCode, file.getName()));
            IMetadataRecorder recorder = metadataRecorderDAO.getMetadataRecorderByMetadataName(metadataCode);
            recorder.processRecordOnly(file);
            utilisateurDAO.flush();
        } catch (BusinessException | PersistenceException ex) {
            LoggerFactory.getLogger(MetadataDeserialization.class.getName()).error(ex.getMessage(), ex);
        }
    }

    private void copyMetadataFiles(File inZip) {
        try (
                ZipInputStream zis = new ZipInputStream(new FileInputStream(inZip));) {
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                String entryName = zipEntry.getName();
                if (entryName.startsWith(START_FILE_NAME_PATTERN) && !entryName.endsWith("/") &&!entryName.equals(START_FILE_NAME_PATTERN)) {
                    copyFile(entryName, zis);
                }
                zipEntry = zis.getNextEntry();
            }
        } catch (IOException ex) {
            LOGGER.error("can't write file %s", inZip.getAbsolutePath(), ex);
        }
    }

    private void copyFile(String entryName, ZipInputStream zis) {

        LOGGER.info(String.format("starting copy metadata %s", entryName));
        byte[] buffer = new byte[1_024];
        final String fileName = configuration.getRepositoryURI() + entryName;
        try (
                FileOutputStream output = FileWithFolderCreator.createOutputStream(fileName);) {
            int len;
            while ((len = zis.read(buffer)) > 0) {
                output.write(buffer, 0, len);
            }
            output.close();
            final File file = FileWithFolderCreator.createFile(fileName);
            files.computeIfAbsent(entryName.replaceAll("(?:metadata)?/(.*)/(.*)\\.csv$", "$1"), k -> file);
        } catch (IOException e) {
            LOGGER.error(String.format("can't write ti file %s", fileName), e);
        }
    }
}
