/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.refdata.datatypevariableunite;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.unite.IUniteDAO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<DatatypeVariableUnite> {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.refdata.datatypevariableunite.messages";
    /**
     * The Constant PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE @link(String).
     */
    private static final String PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE = "PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE";
    /**
     * The Constant PROPERTY_MSG_UNITE_MISSING_IN_DATABASE @link(String).
     */
    private static final String PROPERTY_MSG_UNITE_MISSING_IN_DATABASE = "PROPERTY_MSG_UNITE_MISSING_IN_DATABASE";
    /**
     * The Constant PROPERTY_MSG_VARIABLE_MISSING_IN_DATABASE @link(String).
     */
    private static final String PROPERTY_MSG_VARIABLE_MISSING_IN_DATABASE = "PROPERTY_MSG_VARIABLE_MISSING_IN_DATABASE";
    /**
     * The datatypes possibles @link(Map<String,String[]>).
     */
    private final Map<String, String[]> datatypesPossibles = new ConcurrentHashMap<>();
    /**
     * The unites possibles @link(Map<String,String[]>).
     */
    private final Map<String, String[]> unitesPossibles = new ConcurrentHashMap<>();
    /**
     * The variables possibles @link(Map<String,String[]>).
     */
    private final Map<String, String[]> variablesPossibles = new ConcurrentHashMap<>();
    /**
     * The datatype dao @link(IDatatypeDAO).
     */
    protected IDatatypeDAO datatypeDAO;
    /**
     * The datatype variable unite dao @link(IDatatypeVariableUniteDAO).
     */
    protected IDatatypeVariableUniteDAO datatypeVariableUniteDAO;
    /**
     * The unite dao @link(IUniteDAO).
     */
    protected IUniteDAO uniteDAO;
    /**
     * The variable dao @link(IVariableDAO).
     */
    protected IVariableDAO variableDAO;

    /**
     * Delete record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String uniteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                mgaServiceBuilder.getRecorder().remove(
                        datatypeVariableUniteDAO.getByNKey(datatypeCode, variableCode, uniteCode)
                                .orElseThrow(() -> new BusinessException("no dvu"))
                );
                values = parser.getLine();
            }
            treeApplicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS);
            policyManager.clearTreeFromSession();
        } catch (final IOException e) {
            throw new BusinessException("io exception", e);
        }
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<DatatypeVariableUnite> getAllElements() {
        return datatypeVariableUniteDAO.getAll(DatatypeVariableUnite.class);
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param datatypeVariableUnite the datatype variable unite
     * @return the new line model grid metadata
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @see
     * org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final DatatypeVariableUnite datatypeVariableUnite) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatypeVariableUnite.getDatatype().getName(), datatypesPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatypeVariableUnite.getVariable().getName(), variablesPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatypeVariableUnite.getUnite().getCode(), unitesPossibles, null, true, false, true));
        return lineModelGridMetadata;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<DatatypeVariableUnite> initModelGridMetadata() {
        try {
            updateNamesVariablesPossibles();
            updateNamesUnitesPossibles();
            updateNamesDatatypesPossibles();
        } catch (final PersistenceException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in ModelGridMetadata constructor", e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * Persist datatype variable.
     *
     * @param errorsReport the errors report
     * @param datatypeCode the datatype code
     * @param variableCode the variable code
     * @param uniteCode the unite code
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the datatype code
     * @link(String) the variable code
     * @link(String) the unite code
     */
    private DatatypeVariableUnite persistDatatypeVariable(final ErrorsReport errorsReport, final String datatypeCode, final String variableCode, final String uniteCode) throws BusinessException {
        final DataType dbDatatype = retrieveDBDatatype(errorsReport, datatypeCode);
        final Variable dbVariable = retrieveDBVariable(errorsReport, variableCode);
        final Unite dbUnite = retrieveDBUnite(errorsReport, uniteCode);
        if (dbDatatype != null && dbVariable != null && dbUnite != null && !datatypeVariableUniteDAO.getByNKey(datatypeCode, variableCode, uniteCode).isPresent()) {
            final DatatypeVariableUnite datatypeVariableUnite = new DatatypeVariableUnite(dbDatatype, dbUnite, dbVariable);
            try {
                datatypeVariableUniteDAO.saveOrUpdate(datatypeVariableUnite);
            } catch (PersistenceException ex) {
                throw new BusinessException("can't save dvu", ex);
            }
            return datatypeVariableUnite;
        }

        return null;
    }

    /**
     * Process record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(File) the file
     * @link(String) the encoding
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();

        try {
            skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = parser.getLine();

            Map<DataType, List<DatatypeVariableUnite>> nodeablesDVU = new HashMap();

            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String uniteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final DatatypeVariableUnite nodeable = persistDatatypeVariable(errorsReport,
                        datatypeCode,
                        variableCode,
                        uniteCode
                );
                if (nodeable != null) {
                    if (!nodeablesDVU.containsKey(nodeable.getDatatype())) {
                        nodeablesDVU.put(nodeable.getDatatype(), new LinkedList());
                    }
                    nodeablesDVU.get(nodeable.getDatatype()).add(nodeable);
                }
                if (parser.getLastLineNumber() % 50 == 0) {
                    mgaServiceBuilder.getRecorder().getEntityManager().flush();
                }

                values = parser.getLine();
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

            try {
                IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
                Optional<IMgaIOConfiguration> configuration = configurator.getConfiguration(AbstractMgaIOConfigurator.DATASET_CONFIGURATION);
                Stream<INode> datatypeNodes = mgaServiceBuilder.loadNodesByTypeResource(
                        WhichTree.TREEDATASET, 
                        configuration.map(conf->conf.getLeafType()).orElse(null)
                );
                Map<DataType, List<INode>> datatypeNodesMap = datatypeNodes
                        .filter(n -> nodeablesDVU.containsKey(n.getNodeByNodeableTypeResource(DataType.class).getNodeable()))
                        .collect(Collectors.groupingBy(
                                p -> ((DataType) p.getNodeByNodeableTypeResource(DataType.class).getNodeable()))
                        );
                for (Iterator<Map.Entry<DataType, List<DatatypeVariableUnite>>> iteratorOnDVU = nodeablesDVU.entrySet().iterator(); iteratorOnDVU.hasNext();) {
                    Map.Entry<DataType, List<DatatypeVariableUnite>> dvusEntry = iteratorOnDVU.next();
                    DataType datatype = dvusEntry.getKey();
                    List<DatatypeVariableUnite> dvus = dvusEntry.getValue();
                    List<INode> datatypeNodeList = datatypeNodesMap.get(datatype);
                    for (Iterator<INode> iteratorOnDatatypeNodes = datatypeNodeList.iterator(); iteratorOnDatatypeNodes.hasNext();) {
                        INode datatypeNode = iteratorOnDatatypeNodes.next();
                        for (Iterator<DatatypeVariableUnite> dvuIterator = dvus.iterator(); dvuIterator.hasNext();) {
                            DatatypeVariableUnite dvu = dvuIterator.next();
                            RealNode parentRn = datatypeNode.getRealNode();
                            String path = String.format("%s%s%s", parentRn.getPath(), PatternConfigurator.SPLITER, dvu.getCode());
                            RealNode rn = new RealNode(parentRn, null, dvu, path);
                            mgaServiceBuilder.getRecorder().saveOrUpdate(rn);
                            NodeDataSet nds = new NodeDataSet((NodeDataSet) datatypeNode, null);
                            nds.setRealNode(rn);
                            mgaServiceBuilder.getRecorder().merge(nds);
                        }
                        iteratorOnDatatypeNodes.remove();
                    }
                    mgaServiceBuilder.getRecorder().getEntityManager().flush();
                    iteratorOnDVU.remove();
                }
                policyManager.clearTreeFromSession();

                if (errorsReport.hasErrors()) {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
            } catch (final BusinessException e) {
                throw new BusinessException(e.getMessage(), e);
            }
        } catch (final IOException e) {
            throw new BusinessException("io exception", e);
        }
    }

    /**
     *
     * @param datatypeNode
     * @param nv
     * @return
     */
    public RealNode getOrCreateRealNode(INode datatypeNode, DatatypeVariableUnite nv) {
        return policyManager.getMgaServiceBuilder().getRecorder().getRealNodeByNKey(String.format("%s%s%s", datatypeNode.getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, nv.getCode()))
                .orElse(new RealNode(datatypeNode.getRealNode(), null, nv, String.format("%s%s%s", datatypeNode.getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, nv.getCode())));
    }

    /**
     * Retrieve db datatype.
     *
     * @param errorsReport the errors report
     * @param datatypeCode the datatype code
     * @return the data type
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the datatype code
     */
    private DataType retrieveDBDatatype(final ErrorsReport errorsReport, final String datatypeCode) throws BusinessException {
        Optional<DataType> datatype = datatypeDAO.getByCode(datatypeCode);
        if (!datatype.isPresent()) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE), datatypeCode));
        }
        return datatype.orElseThrow(() -> new BusinessException("no Datatype"));
    }

    /**
     * Retrieve db unite.
     *
     * @param errorsReport the errors report
     * @param uniteCode the unite code
     * @return the unite
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the unite code
     */
    private Unite retrieveDBUnite(final ErrorsReport errorsReport, final String uniteCode) throws BusinessException {
        Optional<Unite> unite = uniteDAO.getByCode(uniteCode);
        if (!unite.isPresent()) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_UNITE_MISSING_IN_DATABASE), uniteCode));
        }
        return unite.orElseThrow(() -> new BusinessException("no unite"));
    }

    /**
     * Retrieve db variable.
     *
     * @param errorsReport the errors report
     * @param variableCode the variable code
     * @return the variable
     * @throws PersistenceException the persistence exception
     * @link(ErrorsReport) the errors report
     * @link(String) the variable code
     */
    private Variable retrieveDBVariable(final ErrorsReport errorsReport, final String variableCode) throws BusinessException {
        Optional<Variable> variable = variableDAO.getByCode(variableCode);
        if (!variable.isPresent()) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_VARIABLE_MISSING_IN_DATABASE), variableCode));
        }
        return variable.orElseThrow(() -> new BusinessException("no no variable"));
    }

    /**
     * Update names datatypes possibles.
     *
     * @throws PersistenceException the persistence exception
     */
    private void updateNamesDatatypesPossibles() throws PersistenceException {
        final List<DataType> datatypes = datatypeDAO.getAll(DataType.class);
        final String[] namesDatatypesPossibles = new String[datatypes.size()];
        int index = 0;
        for (final DataType datatype : datatypes) {
            namesDatatypesPossibles[index++] = datatype.getName();
        }
        datatypesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesDatatypesPossibles);
    }

    /**
     * Update names unites possibles.
     *
     * @throws PersistenceException the persistence exception
     */
    private void updateNamesUnitesPossibles() throws PersistenceException {
        final List<Unite> unites = uniteDAO.getAll(Unite.class);
        final String[] namesUnitesPossibles = new String[unites.size()];
        int index = 0;
        for (final Unite unite : unites) {
            namesUnitesPossibles[index++] = unite.getCode();
        }
        unitesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesUnitesPossibles);
    }

    /**
     * Update names variables possibles.
     *
     * @throws PersistenceException the persistence exception
     */
    private void updateNamesVariablesPossibles() throws PersistenceException {
        final List<Variable> variables = variableDAO.getAll(Variable.class);
        final String[] namesVariablesPossibles = new String[variables.size()];
        int index = 0;
        for (final Variable variable : variables) {
            namesVariablesPossibles[index++] = variable.getName();
        }
        variablesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesVariablesPossibles);
    }

    /**
     * Sets the datatype dao.
     *
     * @param datatypeDAO the new datatype dao
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Sets the datatype variable unite dao.
     *
     * @param datatypeVariableUniteDAO the new datatype variable unite dao
     */
    public void setDatatypeVariableUniteDAO(final IDatatypeVariableUniteDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    /**
     * Sets the unite dao.
     *
     * @param uniteDAO the new unite dao
     */
    public void setUniteDAO(final IUniteDAO uniteDAO) {
        this.uniteDAO = uniteDAO;
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO the new variable dao
     */
    public void setVariableDAO(final IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }
}
