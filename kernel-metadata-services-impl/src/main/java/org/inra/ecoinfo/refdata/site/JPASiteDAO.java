/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.refdata.site;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;

/**
 * The Class JPASiteDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class JPASiteDAO extends AbstractJPADAO<Site> implements ISiteDAO {

    /**
     * Gets the all.
     *
     * @return the all
     * @see org.inra.ecoinfo.refdata.site.ISiteDAO#getAll()
     */
    @Override
    public List<Site> getAll() {
        return getAll(Site.class);
    }

    /**
     * Gets the by code.
     *
     * @param code the code
     * @param parent
     * @return the by code
     * @see org.inra.ecoinfo.refdata.site.ISiteDAO#getByCode(java.lang.String)
     */
    @Override
    public Optional<Site> getByCodeAndParent(final String code, Site parent) {
        CriteriaQuery<Site> query = builder.createQuery(Site.class);
        Root<Site> s = query.from(Site.class);
        query
                .select(s)
                .where(
                        builder.equal(s.get(Nodeable_.code), code),
                        builder.equal(s.join(Site_.parent), parent)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.refdata.site.ISiteMonSoereDAO#getByPath(java.lang.String)
     */
    /**
     *
     * @param path
     * @return
     */
    @Override
    public Optional<Site> getByPath(final String path) {
        if (path != null) {
            return getAll().stream()
                    .filter(s -> path.equalsIgnoreCase(s.getPath()))
                    .findFirst();
        }
        return Optional.empty();
    }
}
