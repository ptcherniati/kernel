/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.refdata.datatype;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 *
 * @author "Antoine Schellenberger"
 */
public class Recorder extends AbstractCSVMetadataRecorder<DataType> {

    /**
     * The properties description en @link(Properties).
     */
    private Properties propertiesDescriptionFR;
    /**
     * The properties nom en @link(Properties).
     */
    private Properties propertiesNomFR;
    /**
     * The datatype dao @link(IDatatypeDAO).
     */
    private Properties propertiesDescriptionEN;
    /**
     * The properties nom en @link(Properties).
     */
    private Properties propertiesNomEN;
    /**
     * The datatype dao @link(IDatatypeDAO).
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     * Delete record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final String code = Utils.createCodeFromString(values[0]);
                datatypeDAO.remove(datatypeDAO.getByCode(code).orElseThrow(() -> new BusinessException("no datatype")));
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Gets the new line model grid metadata.
     *
     * @param datatype the datatype
     * @return the new line model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final DataType datatype) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatype == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatype.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatype == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomFR.getProperty(datatype.getName(), datatype.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatype == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomEN.getProperty(datatype.getName(), datatype.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatype == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatype.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatype == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesDescriptionFR.getProperty(datatype.getDescription(), datatype.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatype == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesDescriptionEN.getProperty(datatype.getDescription(), datatype.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }

    /**
     * Process record.
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser,
     * java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(DataType.class));
                final String nom = tokenizerValues.nextToken();
                final String description = tokenizerValues.nextToken();
                Optional<DataType> datatypeOpt = datatypeDAO.getByCode(Utils.createCodeFromString(nom));
                if (!datatypeOpt.isPresent()) {
                    final DataType datatype = new DataType(nom, description);
                    datatypeDAO.saveOrUpdate(datatype);
                } else {
                    datatypeOpt.get().setDescription(description);
                }
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the datatype dao.
     *
     * @param datatypeDAO the new datatype dao
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Gets the all elements.
     *
     * @return the all elements
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<DataType> getAllElements() {
        final List<DataType> all = datatypeDAO.getAll();
        Collections.sort(all, (DataType o1, DataType o2) -> {
            if (o1 == null || o2 == null) {
                return o1 == o2 ? 0 : -1;
            }
            return o1.getCode().compareTo(o2.getCode());
        });
        return all;
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<DataType> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(DataType.NAME_ENTITY_JPA, Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE);
        propertiesNomEN = localizationManager.newProperties(DataType.NAME_ENTITY_JPA, Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        propertiesDescriptionFR = localizationManager.newProperties(DataType.NAME_ENTITY_JPA, "description", Locale.FRENCH);
        propertiesDescriptionEN = localizationManager.newProperties(DataType.NAME_ENTITY_JPA, "description", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
}
