/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.refdata.variable;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;

/**
 * The Class JPAVariableDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class JPAVariableDAO extends AbstractJPADAO<Variable> implements IVariableDAO {

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     * @see
     * org.inra.ecoinfo.refdata.variable.IVariableDAO#getByCode(java.lang.String)
     */
    @Override
    public Optional<Variable> getByCode(final String code) {
        CriteriaQuery<Variable> query = builder.createQuery(Variable.class);
        Root<Variable> var = query.from(Variable.class);
        query
                .select(var)
                .where(
                        builder.equal(var.get(Nodeable_.code), code)
                );
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Variable> getAll() {
        return getAllBy(Variable.class, Variable::getCode);
    }
}
