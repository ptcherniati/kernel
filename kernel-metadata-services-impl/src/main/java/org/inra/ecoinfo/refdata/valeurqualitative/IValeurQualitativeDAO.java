package org.inra.ecoinfo.refdata.valeurqualitative;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * The Interface IValeurQualitativeDAO.
 */
public interface IValeurQualitativeDAO extends IDAO<IValeurQualitative> {

    /**
     * Gets the by code.
     *
     * @param <T> the generic type
     * @param code the code
     * @return the by code
     * @link(String) the code
     */
    <T extends IValeurQualitative> List<T> getByCode(String code);

    /**
     *
     * @param <T>
     * @return
     */
    <T extends IValeurQualitative> List<T> getAll();

    /**
     * Gets the by code and value.
     *
     * @param <T> the generic type
     * @param code the code
     * @param value the value
     * @return the by code and value
     * @link(String) the code
     * @link(String) the value
     */
    <T extends IValeurQualitative> Optional<T> getByCodeAndValue(String code, String value);
}
