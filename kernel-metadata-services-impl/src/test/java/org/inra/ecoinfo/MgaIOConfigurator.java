package org.inra.ecoinfo;

import org.inra.ecoinfo.MgaIOConfigurator.Projet;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.configurator.AbstractMgaDisplayerConfiguration;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.theme.Theme;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tcherniatinsky
 */
public class MgaIOConfigurator extends AbstractMgaIOConfigurator {

    private static final Integer[] NORMAL_ORDER_0_1_2_3 = new Integer[]{0, 1, 2, 3};
    private static final Integer[] INVERSE_ORDER_3_0_2_1 = new Integer[]{3, 0, 2, 1};
    private static final Integer[] DATASET_ORDER_0 = new Integer[]{0};
    private static final Integer[] EXTRACTION_ORDER_0_1 = new Integer[]{0, 1};
    private static final Class<INodeable>[] entryInstanceConfigZero = new Class[]{
            Projet.class,
            Site.class,
            Theme.class,
            Datatype.class
        };
    private static final Activities[] ACTIVITY_SAPDSE
                = new Activities[]{
                    Activities.synthese,
                    Activities.administration,
                    Activities.publication,
                    Activities.depot,
                    Activities.suppression,
                    Activities.extraction,};
    private static final Class<INodeable>[] ENTRY_INSTANCE_REF = new Class[]{Refdata.class};
    private static final Class<INodeable>[] ENTRY_INSTANCE_PSTD = new Class[]{
            Projet.class,
            Site.class,
            Theme.class,
            Datatype.class
        };
    Activities[] ACTIVITY_ESTA
            = new Activities[]{
                Activities.edition,
                Activities.suppression,
                Activities.telechargement,
                Activities.administration,};

    /**
     *
     */
    public MgaIOConfigurator() {
        super(new AbstractMgaDisplayerConfiguration(){});
    }

    /**
     *
     */
    @Override
    protected void initConfigurations() {
        addConfigDataset();
        addConfigRefdata();
        addConfigSynthesys();
    }

    private void addConfigDataset() {
        boolean includeAncestorConfigZero = true;
        WhichTree whichTreeDataSetZero = WhichTree.TREEDATASET;
        boolean displayColumnNamesZero = false;
        MgaIOConfiguration configDatasetRights
                = new MgaIOConfiguration(
                        DATASET_CONFIGURATION_RIGHTS, 
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3, 
                        entryInstanceConfigZero,
                        ACTIVITY_SAPDSE, 
                        INVERSE_ORDER_3_0_2_1, 
                        includeAncestorConfigZero, 
                        whichTreeDataSetZero, 
                        DatatypeVariableUnite.class, 
                        displayColumnNamesZero);
        MgaIOConfiguration configDataset
                = new MgaIOConfiguration(
                        DATASET_CONFIGURATION, 
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3, 
                        entryInstanceConfigZero,
                        ACTIVITY_SAPDSE, 
                        INVERSE_ORDER_3_0_2_1, 
                        includeAncestorConfigZero, 
                        whichTreeDataSetZero, 
                        null, 
                        displayColumnNamesZero);

        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION_RIGHTS, k -> configDatasetRights);
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION, k -> configDataset);
    }

    /**
     * Configuration One // For RefData Insertion
     *
     */
    protected void addConfigRefdata() {
        boolean includeAncestorConfigOne = false;
        WhichTree whichTreeRefDataOne = WhichTree.TREEREFDATA;
        boolean displayColumnNamesOne = false;
        MgaIOConfiguration configRefdataRights
                = new MgaIOConfiguration(
                        REFDATA_CONFIGURATION_RIGHTS,
                        Refdata.class,
                        DATASET_ORDER_0,
                        ENTRY_INSTANCE_REF,
                        ACTIVITY_ESTA,
                        DATASET_ORDER_0,
                        includeAncestorConfigOne,
                        whichTreeRefDataOne,
                        null,
                        displayColumnNamesOne);
        MgaIOConfiguration configRefdata
                = new MgaIOConfiguration(
                        REFDATA_CONFIGURATION,
                        Refdata.class,
                        DATASET_ORDER_0,
                        ENTRY_INSTANCE_REF,
                        ACTIVITY_ESTA,
                        DATASET_ORDER_0,
                        includeAncestorConfigOne,
                        whichTreeRefDataOne,
                        null,
                        displayColumnNamesOne);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION_RIGHTS, k -> configRefdataRights);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION, k -> configRefdata);
    
    }

    /**
     * Configuration Three
     *
     */
    protected void addConfigSynthesys() {
        boolean includeAncestorConfigThree = false;
        WhichTree whichTreeDataSetThree = WhichTree.TREEDATASET;
        boolean displayColumnNamesThree = false;
        MgaIOConfiguration configSynthese
                = new MgaIOConfiguration(
                        MgaIOConfigurator.SYNTHESIS_CONFIGURATION,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_PSTD,
                        ACTIVITY_SAPDSE,
                        EXTRACTION_ORDER_0_1,
                        includeAncestorConfigThree,
                        whichTreeDataSetThree,
                        null,
                        displayColumnNamesThree);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION, k -> configSynthese);
    }

    /**
     *
     */
    public class MgaIOConfiguration extends AbstractMgaIOConfiguration {

        /**
         *
         * @param codeConfiguration
         * @param codeConfig
         * @param leafType
         * @param entryOrder
         * @param entryTypes
         * @param activities
         * @param sortOrder
         * @param includeAncestor
         * @param whichTree
         * @param stickyLeafType
         * @param displayColumnNames
         */
        public MgaIOConfiguration(Integer codeConfiguration, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfiguration, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

    }

    /**
     *
     */
    public class DatatypeVariableUnite extends Nodeable {

        /**
         *
         * @return
         */
        @Override
        public String getName() {
            return getClass().getSimpleName();
        }

        /**
         *
         * @param <T>
         * @return
         */
        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) getClass();
        }

    }

    /**
     *
     */
    public class Projet extends Nodeable {

        /**
         *
         * @return
         */
        @Override
        public String getName() {
            return getClass().getSimpleName();
        }

        /**
         *
         * @param <T>
         * @return
         */
        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) getClass();
        }

    }


}
