/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.refdata.impl;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationDAO;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.IMetadataRecorder;
import org.inra.ecoinfo.refdata.IMetadataRecorderDAO;
import org.inra.ecoinfo.refdata.IMetadataRepositoryDAO;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.config.IMetadataConfiguration;
import org.inra.ecoinfo.refdata.config.Metadata;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.transaction.TransactionStatus;

/**
 *
 * @author tcherniatinsky
 */
public class LocalMetadataManagerTest {
    private static final String REFDATA_CODE = "refdataname";
    private static final String REFDATA_NAME = "refdataName";

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    LocalMetadataManager instance;
    @Mock(name = "configuration")
    private IMetadataConfiguration configuration;
    @Mock(name = "datatypeDAO")
    private IDatatypeDAO datatypeDAO;
    @Mock(name = "localizationManager")
    private ILocalizationManager localizationManager;
    @Mock(name = "metadataRecorderDAO")
    private IMetadataRecorderDAO metadataRecorderDAO;
    @Mock(name = "notificationDAO")
    private INotificationDAO notificationDAO;
    @Mock(name = "metadataRepositoryDAO")
    private IMetadataRepositoryDAO metadataRepositoryDAO;
    @Mock(name = "notificationsManager")
    private INotificationsManager notificationsManager;
    @Mock(name = "policyManager")
    private IPolicyManager policyManager;
    @Mock(name = "siteDAO")
    private ISiteDAO siteDAO;
    @Mock(name = "metadataRecorder")
    IMetadataRecorder metadataRecorder;
    @Mock(name = "model")
    ModelGridMetadata model;
    @Mock(name = "currentUser")
    Utilisateur currentUser;

    /**
     *
     */
    public LocalMetadataManagerTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new LocalMetadataManager();
        instance.setConfiguration(configuration);
        instance.setDatatypeDAO(datatypeDAO);
        instance.setLocalizationManager(localizationManager);
        instance.setMetadataRecorderDAO(metadataRecorderDAO);
        instance.setMetadataRepositoryDAO(metadataRepositoryDAO);
        instance.setNotificationDAO(notificationDAO);
        instance.setNotificationsManager(notificationsManager);
        instance.setPolicyManager(policyManager);
        instance.setSiteDAO(siteDAO);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of buildModelGridMetadata method, of class LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildModelGridMetadata() throws Exception {
        instance = spy(instance);
        doReturn(currentUser).when(policyManager).getCurrentUser();
        doNothing().when(instance).testIsNullRefdata(REFDATA_NAME);
        doNothing().when(instance).testHasRights(REFDATA_NAME, new Activities[]{Activities.administration, Activities.depot, Activities.publication, Activities.telechargement, Activities.edition, Activities.suppression}, WhichTree.TREEREFDATA, LocalMetadataManager.PROPERTY_MSG_NO_RIGHTS_ON_DATA);
        doReturn(metadataRecorder).when(instance).getMetadataRecorder(REFDATA_NAME, currentUser);
        doReturn(model).when(metadataRecorder).buildModelGrid();
        ModelGridMetadata result = instance.buildModelGridMetadata(REFDATA_NAME);
        verify(instance).testIsNullRefdata(REFDATA_NAME);
        verify(instance).testHasRights(REFDATA_NAME, new Activities[]{Activities.administration, Activities.depot, Activities.publication, Activities.telechargement, Activities.edition, Activities.suppression}, WhichTree.TREEREFDATA, LocalMetadataManager.PROPERTY_MSG_NO_RIGHTS_ON_DATA);
        assertEquals(model, result);

        //with exception
        doThrow(new PersistenceException()).when(metadataRecorderDAO).getMetadataRecorderByMetadataName(REFDATA_CODE);
        try {
            result = instance.buildModelGridMetadata(REFDATA_NAME);
        } catch (BusinessException B) {
            assertEquals("can't build model", B.getMessage());
        }

    }

    /**
     * Test of retrieveAllSites method, of class LocalMetadataManager.
     */
    @Test
    public void testRetrieveAllSites() {
        List<Site> result = instance.retrieveAllSites();
        verify(siteDAO).getAll();
    }

    /**
     * Test of retrieveRestrictedMetadatas method, of class
     * LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    public void testRetrieveRestrictedMetadatas() throws Exception {
        Metadata metadataAuth = mock(Metadata.class, "metadataAuth");
        when(metadataAuth.getName()).thenReturn("metadataAuth");
        Metadata metadataNoneAuth = mock(Metadata.class, "metadataNoneAuth");
        when(metadataNoneAuth.getName()).thenReturn("metadataNoneAuth");
        List<Metadata> metadatas = Stream.of(metadataAuth, metadataNoneAuth).collect(Collectors.toList());
        when(configuration.getMetadatas()).thenReturn(metadatas);
        //root case
        when(policyManager.isRoot()).thenReturn(Boolean.TRUE);
        List<Metadata> result = instance.retrieveRestrictedMetadatas();
        assertEquals(metadatas, result);
        //none root
        when(policyManager.isRoot()).thenReturn(Boolean.FALSE);
        when(policyManager.getCurrentUser()).thenReturn(currentUser);
        Set<String> restrictOnlyToActivitydNodeNameForCurrentUser = mock(Set.class);
        doReturn(restrictOnlyToActivitydNodeNameForCurrentUser).when(policyManager).getOnlyNamesOfNodesWhichHaveAtLeastOneActivity(currentUser, WhichTree.TREEREFDATA);
        when(restrictOnlyToActivitydNodeNameForCurrentUser.contains("metadataAuth")).thenReturn(Boolean.TRUE);
        result = instance.retrieveRestrictedMetadatas();
        assertTrue(result.size() == 1);
        assertTrue(result.contains(metadataAuth));
        assertFalse(result.contains(metadataNoneAuth));
    }

    /**
     * Test of saveAndDeleteModelGridMetadata method, of class
     * LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    public void testSaveAndDeleteModelGridMetadata() throws Exception {
//nominal
                instance = spy(instance);
        TransactionStatus tr = mock(TransactionStatus.class);
        doReturn(tr).when(metadataRecorderDAO).beginDefaultTransaction();
        doReturn(currentUser).when(policyManager).getCurrentUser();
        Set<LineModelGridMetadata> editedLines, linesToEdit;
        List<LineModelGridMetadata> linesToDelete;
        editedLines = mock(Set.class, "editedLines");
        linesToEdit = mock(Set.class, "linesToEdit");
        doReturn(linesToEdit).when(instance).getLinesToEdit(editedLines);
        linesToDelete = mock(List.class, "linesToDelete");
        doReturn(linesToDelete).when(instance).getLinesToDelete(model);
        doNothing().when(instance).testIsNullRefdata(REFDATA_NAME);
        doReturn(metadataRecorder).when(instance).getMetadataRecorder(REFDATA_NAME, currentUser);
        doNothing().when(instance).saveModel(model, REFDATA_NAME, linesToEdit, currentUser);
        doNothing().when(instance).testHasRights(REFDATA_NAME, Activities.suppression, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_DELETE);
        doReturn("delete").when(model).formatAsCSV(linesToDelete);
        doNothing().when(instance).testHasRights(REFDATA_NAME, Activities.publication, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_DOWNLOAD);
        doNothing().when(instance).testHasRights(REFDATA_NAME, Activities.publication, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_UPLOAD);
        doNothing().when(metadataRecorder).deleteProcessBytes("delete".getBytes(StandardCharsets.UTF_8));
        doNothing().when(instance).tryFlush(REFDATA_NAME, tr, currentUser);
        doReturn("%s").when(localizationManager).getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_SUCCESS_DELETE_REFDATA);
        Notification notification = mock(Notification.class);
        doReturn(notification).when(instance).sendNotification(REFDATA_NAME, Notification.INFO, null, currentUser);
        doNothing().when(instance).updateRepositoryFile(REFDATA_NAME, metadataRecorder);
        instance.saveAndDeleteModelGridMetadata(model, editedLines, REFDATA_NAME);
        verify(instance).testIsNullRefdata(REFDATA_NAME);
        verify(metadataRecorderDAO).beginDefaultTransaction();
        verify(policyManager, times(2)).getCurrentUser();
        verify(instance).getLinesToEdit(editedLines);
        verify(instance).getLinesToDelete(model);
        verify(instance).testHasRights(REFDATA_NAME, Activities.suppression, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_DELETE);
        verify(instance).testHasRights(REFDATA_NAME, Activities.publication, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_UPLOAD);
        verify(instance).saveModel(model, REFDATA_NAME, linesToEdit, currentUser);
        verify(metadataRecorder).deleteProcessBytes("delete".getBytes(StandardCharsets.UTF_8));
        verify(instance).tryFlush(REFDATA_NAME, tr, currentUser);
        verify(instance).sendNotification(REFDATA_NAME, Notification.INFO, null, currentUser);
        verify(instance).updateRepositoryFile(REFDATA_NAME, metadataRecorder);
        //empty line to delete line to edit
        when(linesToDelete.isEmpty()).thenReturn(Boolean.TRUE);
        when(linesToEdit.isEmpty()).thenReturn(Boolean.TRUE);
        instance.saveAndDeleteModelGridMetadata(model, editedLines, REFDATA_NAME);
        verify(instance, times(2)).testIsNullRefdata(REFDATA_NAME);
        verify(metadataRecorderDAO, times(2)).beginDefaultTransaction();
        verify(policyManager, times(4)).getCurrentUser();
        verify(instance, times(2)).getLinesToEdit(editedLines);
        verify(instance, times(2)).getLinesToDelete(model);
        verify(instance).testHasRights(REFDATA_NAME, Activities.suppression, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_DELETE);
        verify(instance).testHasRights(REFDATA_NAME, Activities.publication, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_UPLOAD);
        verify(instance, times(2)).saveModel(model, REFDATA_NAME, linesToEdit, currentUser);
        verify(metadataRecorder).deleteProcessBytes("delete".getBytes(StandardCharsets.UTF_8));
        verify(instance).tryFlush(REFDATA_NAME, tr, currentUser);
        verify(instance).sendNotification(REFDATA_NAME, Notification.INFO, null, currentUser);
        verify(instance, times(2)).updateRepositoryFile(REFDATA_NAME, metadataRecorder);

        //throw exception
        when(linesToDelete.isEmpty()).thenReturn(Boolean.FALSE);
        doThrow(new BusinessException()).when(metadataRecorder).deleteProcessBytes("delete".getBytes(StandardCharsets.UTF_8));
        doReturn("%s").when(localizationManager).getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_DELETE_ERROR_REFDATA);
        doNothing().when(metadataRecorderDAO).rollbackTransaction(tr);
        try {
            instance.saveAndDeleteModelGridMetadata(model, editedLines, REFDATA_NAME);
            fail("don't throws exception");
        } catch (Exception e) {
            verify(instance).sendNotification(REFDATA_NAME, Notification.ERROR, null, currentUser);
            verify(metadataRecorderDAO).rollbackTransaction(tr);
            assertEquals(REFDATA_NAME, e.getMessage());
        }
    }

    /**
     * Test of saveModelGridMetadata method, of class LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    public void testSaveModelGridMetadata() throws Exception {
        instance = spy(instance);
        Set<LineModelGridMetadata> editedLines = mock(Set.class, "editedLines");
        doNothing().when(instance).testIsNullRefdata(REFDATA_NAME);
        doNothing().when(instance).testHasRights(REFDATA_NAME, Activities.publication, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_UPLOAD);
        doReturn(metadataRecorder).when(instance).getMetadataRecorder(REFDATA_NAME, currentUser);
        doReturn(currentUser).when(policyManager).getCurrentUser();
        doReturn("lineEdited").when(model).formatAsCSV(editedLines);
        doNothing().when(metadataRecorder).processBytes("lineEdited".getBytes(StandardCharsets.UTF_8));
        instance.saveModelGridMetadata(model, editedLines, REFDATA_NAME);
        verify(instance).testIsNullRefdata(REFDATA_NAME);
        verify(instance).testHasRights(REFDATA_NAME, Activities.publication, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_UPLOAD);
        verify(metadataRecorder).processBytes("lineEdited".getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Test of uploadMetadatas method, of class LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    public void testUploadMetadatas() throws Exception {
        instance = spy(instance);
        File file = mock(File.class);
        doNothing().when(instance).testIsNullRefdata(REFDATA_NAME);
        doNothing().when(instance).testHasRights(REFDATA_NAME, Activities.publication, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_DOWNLOAD);
        doReturn(currentUser).when(policyManager).getCurrentUser();
        doReturn(metadataRecorder).when(instance).getMetadataRecorder(REFDATA_NAME, currentUser);
        doNothing().when(metadataRecorder).processFile(file);
        doReturn("model").when(model).formatAsCSV();
        doNothing().when(metadataRepositoryDAO).addMetadata("model".getBytes(StandardCharsets.UTF_8), REFDATA_NAME);
        Properties refdatasProperties = mock(Properties.class);
        doReturn(refdatasProperties).when(localizationManager).newProperties(Nodeable.getLocalisationEntite(Refdata.class), Nodeable.ENTITE_COLUMN_NAME);
        doReturn("%s, %s").when(localizationManager).getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_METADATA_INSERTION_SUCCESSED);
        doReturn("internationalizedCode").when(instance).internationalizedCode(REFDATA_NAME, refdatasProperties);
        ArgumentCaptor<String> string = ArgumentCaptor.forClass(String.class);
        Notification notification = mock(Notification.class);
        doReturn(notification).when(instance).sendNotificationToUser(string.capture(), eq(Notification.INFO), isNull(String.class), eq("login"));
        doReturn(model).when(metadataRecorder).buildModelGrid();
        doReturn("data").when(model).formatAsCSV();
        instance.uploadMetadatas(file, REFDATA_NAME, "fileName", "login");
        verify(instance).testIsNullRefdata(REFDATA_NAME);
        verify(instance).testHasRights(REFDATA_NAME, Activities.publication, WhichTree.TREEREFDATA, LocalMetadataManager.MSG_NO_RIGHTS_FOR_DOWNLOAD);
        verify(metadataRecorder).processFile(file);
        verify(metadataRepositoryDAO).addMetadata("data".getBytes(StandardCharsets.UTF_8), REFDATA_NAME);
        assertEquals("internationalizedCode, 00:00", string.getValue());

        // with exception
        doThrow(new BusinessException("message")).when(metadataRecorder).processFile(file);
        doReturn("file =%s").when(localizationManager).getMessage(LocalMetadataManager.BUNDLE_SOURCE_PATH, LocalMetadataManager.PROPERTY_MSG_METADATA_INSERTION_FAILURE);
        doReturn(notification).when(instance).sendNotification("file =fileName", Notification.ERROR, "message", currentUser);
        try {
            instance.uploadMetadatas(file, REFDATA_NAME, "fileName", "login");
            fail("no exception thrown");
        } catch (Exception e) {
            assertEquals("file =fileName", e.getMessage());
        }
    }

    /**
     * Test of getPolicyManager method, of class LocalMetadataManager.
     */
    @Test
    public void testGetPolicyManager() {
        assertEquals(policyManager, instance.getPolicyManager());
    }

    /**
     * Test of updateRepositoryFile method, of class LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testUpdateRepositoryFile() throws Exception {
        String nameRefdata = "";
        IMetadataRecorder metadataRecorder = null;
        LocalMetadataManager instance = new LocalMetadataManager();
        instance.updateRepositoryFile(nameRefdata, metadataRecorder);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLinesToDelete method, of class LocalMetadataManager.
     */
    @Test
    @Ignore
    public void testGetLinesToDelete() {
        ModelGridMetadata modelGridMetadata = null;
        LocalMetadataManager instance = new LocalMetadataManager();
        List<LineModelGridMetadata> expResult = null;
        List<LineModelGridMetadata> result = instance.getLinesToDelete(modelGridMetadata);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLinesToEdit method, of class LocalMetadataManager.
     */
    @Test
    @Ignore
    public void testGetLinesToEdit() {
        Set<LineModelGridMetadata> editedLines = null;
        LocalMetadataManager instance = new LocalMetadataManager();
        Set<LineModelGridMetadata> expResult = null;
        Set<LineModelGridMetadata> result = instance.getLinesToEdit(editedLines);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of saveModel method, of class LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testSaveModel() throws Exception {
        ModelGridMetadata modelGridMetadata = null;
        String nameRefdata = "";
        Set<LineModelGridMetadata> linesToEdit = null;
        Utilisateur utilisateur = null;
        LocalMetadataManager instance = new LocalMetadataManager();
        instance.saveModel(modelGridMetadata, nameRefdata, linesToEdit, utilisateur);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMetadataRecorder method, of class LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testGetMetadataRecorder() throws Exception {
        String nameRefdata = "";
        Utilisateur utilisateur = null;
        String refdataCode = "";
        LocalMetadataManager instance = new LocalMetadataManager();
        IMetadataRecorder expResult = null;
        IMetadataRecorder result = instance.getMetadataRecorder(nameRefdata, utilisateur);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of tryFlush method, of class LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testTryFlush() throws Exception {
        String nameRefdata = "";
        TransactionStatus tr = null;
        Utilisateur utilisateur = null;
        LocalMetadataManager instance = new LocalMetadataManager();
        instance.tryFlush(nameRefdata, tr, utilisateur);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of testIsNullRefdata method, of class LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testTestIsNullRefdata() throws Exception {
        String refdataCode = "";
        LocalMetadataManager instance = new LocalMetadataManager();
        instance.testIsNullRefdata(refdataCode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of testHasRights method, of class LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testTestHasRights_4args_1() throws Exception {
        String metadataCode = "";
        Activities activity = null;
        WhichTree whichTree = null;
        String keyMessage = "";
        LocalMetadataManager instance = new LocalMetadataManager();
        instance.testHasRights(metadataCode, activity, whichTree, keyMessage);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of testHasRights method, of class LocalMetadataManager.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testTestHasRights_4args_2() throws Exception {
        String metadataCode = "";
        Activities[] activities = null;
        WhichTree whichTree = null;
        String keyMessage = "";
        LocalMetadataManager instance = new LocalMetadataManager();
        instance.testHasRights(metadataCode, activities, whichTree, keyMessage);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of internationalizedCode method, of class LocalMetadataManager.
     */
    @Test
    @Ignore
    public void testInternationalizedCode() {
        String metadataCode = "";
        Properties refdataProperties = null;
        LocalMetadataManager instance = new LocalMetadataManager();
        String expResult = "";
        String result = instance.internationalizedCode(metadataCode, refdataProperties);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of internationalizeArray method, of class LocalMetadataManager.
     */
    @Test
    @Ignore
    public void testInternationalizeArray() {
        String[] array = null;
        Properties refdataProperties = null;
        LocalMetadataManager instance = new LocalMetadataManager();
        String[] expResult = null;
        String[] result = instance.internationalizeArray(array, refdataProperties);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
