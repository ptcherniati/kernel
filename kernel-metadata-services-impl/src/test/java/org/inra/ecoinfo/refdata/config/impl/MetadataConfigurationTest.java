package org.inra.ecoinfo.refdata.config.impl;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.dbunit.DatabaseUnitException;
import org.inra.ecoinfo.config.ConfigurationException;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class MetadataConfigurationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
public class MetadataConfigurationTest {

    private static final String REPOSITORY_PATTERN = "%s/metadata";

    /**
     * The metadata configuration.
     */
    @Autowired
    MetadataConfiguration metadataConfiguration;
    @Autowired
    ICoreConfiguration coreConfiguration;

    /**
     * Sets the metadata configuration.
     *
     * @param metadataConfiguration the new metadata configuration
     */
    public void setMetadataConfiguration(MetadataConfiguration metadataConfiguration) {
        this.metadataConfiguration = metadataConfiguration;
    }

    /**
     * Test.
     *
     * @throws URISyntaxException the uRI syntax exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws IllegalAccessException the illegal access exception
     * @throws org.inra.ecoinfo.config.ConfigurationException
     * @throws PersistenceException the persistence exception
     * @throws DatabaseUnitException the database unit exception
     * @throws SQLException the sQL exception
     * @throws ParserConfigurationException the parser configuration exception
     */
    @Test
    public void test() throws URISyntaxException, IOException, IllegalAccessException, ConfigurationException, PersistenceException, DatabaseUnitException, SQLException, ParserConfigurationException {
        Assert.assertTrue("la liste des metadonnées est vide", metadataConfiguration.getMetadatas().size() > 0);
        Assert.assertTrue(new File(String.format(REPOSITORY_PATTERN, coreConfiguration.getRepositoryURI())).exists());
    }

    /**
     *
     * @param coreConfiguration
     */
    public void setCoreConfiguration(ICoreConfiguration coreConfiguration) {
        this.coreConfiguration = coreConfiguration;
    }
}
