package org.inra.ecoinfo.refdata;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.notifications.exceptions.DeleteNotificationException;
import org.inra.ecoinfo.notifications.exceptions.UpdateNotificationException;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author ptcherniati
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({DatasetDescriptorBuilder.class})
public class AbstractCSVMetadataRecorderTest {

    private String TMP_FILE;

    @Mock
    DatasetDescriptor datasetDescriptor;
    AbstractCSVMetadataRecorder<MetadataObjectT> recorder;

    /**
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        recorder = new MetadataT();
        Localization.setDefaultLocalisation("fr");
        recorder.setLocalizationManager(new SpringLocalizationManager());
        recorder.localizationManager.setDefaultLanguage("fr");
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void testAbstractCSVMetadataRecorder() throws Exception {
        Assert.assertNotNull(recorder);
        PowerMockito.mockStatic(DatasetDescriptorBuilder.class);
        final String message = "io exception";
        final IOException toBeThrown = new IOException(message);
        PowerMockito.doThrow(toBeThrown).when(DatasetDescriptorBuilder.class);
        DatasetDescriptorBuilder.buildDescriptor(any(InputStream.class));
        recorder = null;
//        final Appender appender = new Mockito().mock(Appender.class);
//        Logger logger = (Logger) AbstractCSVMetadataRecorder.LOGGER;
//        logger.addAppender(appender);
        recorder = new MetadataT();
        Assert.assertNotNull(recorder);
//        when(appender.getName()).thenReturn("MOCK");
//        verify(appender).doAppend(argThat(new ArgumentMatcher() {
//            @Override
//            public boolean matches(final Object argument) {
//                return "io exception".equals(((ILoggingEvent)argument).getMessage());
//            }
//        }));

    }

    /**
     *
     * @throws BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testBuildModelGrid() throws BusinessException, PersistenceException {
        ModelGridMetadata<MetadataObjectT> model = recorder.buildModelGrid();
        Assert.assertTrue(model.getLinesModelGridMetadatas().size() == 2);
        Assert.assertEquals("object 1", model.getLineModelGridMetadataAt(0).getColumnsModelGridMetadatas().get(0).getValue());
        Assert.assertEquals("nom", model.getLineModelGridMetadataAt(0).getColumnsModelGridMetadatas().get(0).getHeader());
        Assert.assertEquals("object 2", model.getLineModelGridMetadataAt(1).getColumnsModelGridMetadatas().get(0).getValue());
        recorder = Mockito.spy(recorder);
        Mockito.when(recorder.getAllElements()).thenThrow(new BusinessException("can't buils model"));
        try {
            model = recorder.buildModelGrid();
            Assert.fail();
        } catch (final BusinessException e) {
            Assert.assertEquals("can't buils model", e.getMessage());
        }
    }

    /**
     *
     * @throws
     * org.inra.ecoinfo.notifications.exceptions.DeleteNotificationException
     * @throws BadFormatException
     * @throws BusinessException
     */
    @Test
    public void testDeleteProcessBytes() throws DeleteNotificationException, BadFormatException, BusinessException {
        final byte[] data = "nom; description".getBytes(StandardCharsets.UTF_8);
        recorder = Mockito.spy(recorder);
        final Answer<String> answer = (InvocationOnMock invocation) -> {
            final Object[] args = invocation.getArguments();
            final CSVParser csvParser = (CSVParser) args[0];
            final String[] line = csvParser.getLine();
            Assert.assertEquals("nom", line[0]);
            Assert.assertEquals("description", line[1]);
            Assert.assertNotNull(args[1]);
            Assert.assertEquals("utf-8", args[2]);
            return null;
        };
        Mockito.doAnswer(answer).when(recorder).deleteRecord(any(CSVParser.class), any(File.class), anyString());
        recorder.deleteProcessBytes(data);
    }

    /**
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Test
    public void testGetAllElements() throws BusinessException {
        final List<MetadataObjectT> elements = recorder.getAllElements();
        Assert.assertEquals("object 1", elements.get(0).getName());
        Assert.assertEquals("object 2", elements.get(1).getName());
    }

    /**
     *
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    @Test
    public void testInitModelGridMetadata() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        recorder.datasetDescriptor = datasetDescriptor;
        final List<Column> columns = new LinkedList<>();
        final Column column1 = new Column();
        column1.setName("name1");
        final Column column2 = new Column();
        column2.setName("name2");
        columns.add(column1);
        columns.add(column2);
        Mockito.when(datasetDescriptor.getColumns()).thenReturn(columns);
        final ModelGridMetadata<MetadataObjectT> model = recorder.initModelGridMetadata();
        Assert.assertTrue(model.getHeadersColumns().size() == 2);
        Assert.assertEquals("name1", model.getHeadersColumns().get(0));
        Assert.assertEquals("name2", model.getHeadersColumns().get(1));
    }

    /**
     *
     * @throws
     * org.inra.ecoinfo.notifications.exceptions.UpdateNotificationException
     * @throws BusinessException
     * @throws org.inra.ecoinfo.utils.exceptions.BadFormatException
     */
    @Test
    public void testProcessBytes() throws UpdateNotificationException, BadFormatException, BusinessException {
        final byte[] data = "nom; description".getBytes(StandardCharsets.UTF_8);
        recorder = Mockito.spy(recorder);
        final Answer<String> answer = (InvocationOnMock invocation) -> {
            final Object[] args = invocation.getArguments();
            final CSVParser csvParser = (CSVParser) args[0];
            final String[] line = csvParser.getLine();
            Assert.assertEquals("nom", line[0]);
            Assert.assertEquals("description", line[1]);
            Assert.assertNotNull(args[1]);
            Assert.assertEquals("utf-8", args[2]);
            return null;
        };
        Mockito.doAnswer(answer).when(recorder).processRecord(any(CSVParser.class), any(File.class), anyString());
        recorder.processBytes(data);
    }

    /**
     *
     * @throws BusinessException
     * @throws BadFormatException
     * @throws IOException
     */
    @Test
    public void testTestFormat() throws BusinessException, BadFormatException, IOException {
        recorder.datasetDescriptor = datasetDescriptor;
        final List<Column> columns = new LinkedList<>();
        final Column column1 = new Column();
        column1.setName("nom");
        final Column column2 = new Column();
        column2.setName("description");
        final Column column3 = new Column();
        column3.setName("integer");
        column3.setValueType("integer");
        final Column column4 = new Column();
        column4.setName("date");
        column4.setValueType("date");
        column4.setFormatType(DateUtil.DD_MM_YYYY);
        final Column column5 = new Column();
        column5.setName("float");
        column5.setValueType("float");
        columns.add(column1);
        columns.add(column2);
        columns.add(column3);
        columns.add(column4);
        columns.add(column5);
        Mockito.when(datasetDescriptor.getColumns()).thenReturn(columns);
        String text = "nom;description;integer;date;float\nchien;animal;1;25/12/1965;10.0\nchaise;objet;13;12/01/2005;23.4";
        TMP_FILE = "tmp.txt";
        File file = new File(TMP_FILE);
        FileUtils.writeStringToFile(file, text);
        recorder.testFormat(file);
        text = "nom;description;integer;date;float\nchien;animal;un;25/12_1965;float\nchaise;objet;13;12_01_2005;23.4";
        file = new File(TMP_FILE);
        FileUtils.writeStringToFile(file, text);
        try {
            recorder.testFormat(file);
            file.delete();
        } catch (final BusinessException e) {
            Assert.assertEquals(
                    "Erreur :- Une valeur entière est attendue à la ligne 2, colonne 3 (integer): la valeur actuelle est \"un\" - Une date de format dd/MM/yyyy est attendue à la ligne 2, colonne 4 (date): la valeur actuelle est \"25/12_1965\" - Une valeur flottante est attendue à la ligne 2, colonne 5 (float): la valeur actuelle est \"float\" - Une date de format dd/MM/yyyy est attendue à la ligne 3, colonne 4 (date): la valeur actuelle est \"12_01_2005\" ",
                    e.getMessage());
        }
        file.delete();

    }

    @Test
    public void setColumnRefForOneRefTest() {
        ColumnModelGridMetadata columnParent = new ColumnModelGridMetadata("chien", new String[]{"chat", "chien", "rat", "belette"}, "animaux", Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);
        ColumnModelGridMetadata columnChild = new ColumnModelGridMetadata("42", new String[]{"10", "21", "35", "42"}, "prix", Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);
        List<ColumnModelGridMetadata> columnsChildren = new LinkedList<>();
        columnsChildren.add(columnChild);
        recorder.setColumnRef(columnParent, columnsChildren);
        Assert.assertEquals(columnChild, columnParent.getRefs().get(0));
        Assert.assertEquals(columnParent, columnChild.getRefBy());
        Assert.assertEquals("42", columnChild.getValue());
    }

    @Test
    public void setColumnRefForMultiRefTest() {
        ColumnModelGridMetadata columnParent = new ColumnModelGridMetadata("chien", new String[]{"chat", "chien", "rat", "belette"}, "animaux", Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);
        ColumnModelGridMetadata columnMiddle = new ColumnModelGridMetadata("bengale", new String[]{"siamois", "gouttière", "bengale", "persan"}, "espèces", Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);
        ColumnModelGridMetadata columnChild = new ColumnModelGridMetadata("42", new String[]{"10", "21", "35", "42"}, "prix", Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);
        List<ColumnModelGridMetadata> columns = new LinkedList();
        columns.add(columnMiddle);
        columns.add(columnChild);
        recorder.setColumnRef(columnParent, columns);
        Assert.assertEquals(columnMiddle, columnParent.getRefs().get(0));
        Assert.assertEquals(columnChild, columnParent.getRefs().get(1));
        Assert.assertEquals(columnParent, columnMiddle.getRefBy());
        Assert.assertEquals(columnParent, columnChild.getRefBy());
        Assert.assertEquals("bengale", columnMiddle.getValue());
        Assert.assertEquals("42", columnChild.getValue());
    }

    @Test
    public void setColumnRefRecursiveTest() {
        ColumnModelGridMetadata columnParent = new ColumnModelGridMetadata("chien", new String[]{"chat", "chien", "rat", "belette"}, "animaux", Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);
        ColumnModelGridMetadata columnMiddle = new ColumnModelGridMetadata("bengale", new String[]{"siamois", "gouttière", "bengale", "persan"}, "espèces", Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);
        ColumnModelGridMetadata columnChild = new ColumnModelGridMetadata("42", new String[]{"10", "21", "35", "42"}, "prix", Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);
        Deque<ColumnModelGridMetadata> columns = new LinkedList();
        columns.add(columnParent);
        columns.add(columnMiddle);
        columns.add(columnChild);
        recorder.setColumnRefRecursive(columns);
        Assert.assertEquals(columnMiddle, columnParent.getRefs().get(0));
        Assert.assertEquals(columnChild, columnParent.getRefs().get(1));
        Assert.assertEquals(columnChild, columnMiddle.getRefs().get(0));
        Assert.assertEquals(columnParent, columnMiddle.getRefBy());
        Assert.assertEquals(columnMiddle, columnChild.getRefBy());
        Assert.assertEquals("bengale", columnMiddle.getValue());
        Assert.assertEquals("42", columnChild.getValue());
    }

    @Test
    public void readMapOfValuesPossiblesTest() {
        Map<String, Object> mapOfValues = new HashMap();
        ConcurrentMap<String, String[]> mapOfValuesPossiblesMiddle = new ConcurrentHashMap();
        ConcurrentMap<String, String[]> mapOfValuesPossiblesChild = new ConcurrentHashMap();
        List<ConcurrentMap<String, String[]>> listOfMapOfVluesPossibles = new LinkedList();
        listOfMapOfVluesPossibles.add(mapOfValuesPossiblesMiddle);
        listOfMapOfVluesPossibles.add(mapOfValuesPossiblesChild);
        List<String> chiwawa = Stream.of(new String[]{"15", "19"}).collect(Collectors.toList()),
                cocker = Stream.of(new String[]{"13", "21"}).collect(Collectors.toList()),
                siamois = Stream.of(new String[]{"30"}).collect(Collectors.toList()),
                persan = Stream.of(new String[]{"31", "20", "15"}).collect(Collectors.toList());
        Map<String, Object> chien = new HashMap();
        chien.put("chiwawa", chiwawa);
        chien.put("cocker", cocker);
        Map<String, Object> chat = new HashMap();
        chat.put("siamois", siamois);
        chat.put("persan", persan);
        mapOfValues.put("chien", chien);
        mapOfValues.put("chat", chat);
        List<String> valuesForKey = new LinkedList<>();
        String[] result = recorder.readMapOfValuesPossibles(mapOfValues, listOfMapOfVluesPossibles, valuesForKey);
        Assert.assertTrue(Stream.of(result).allMatch(r -> "chien".equals(r) || "chat".equals(r)));
        Assert.assertTrue(mapOfValuesPossiblesMiddle.keySet().stream().anyMatch(r -> "chien".equals(r) || "chat".equals(r)));
        Assert.assertTrue(Stream.of(mapOfValuesPossiblesMiddle.get("chien")).allMatch(r -> "cocker".equals(r) || "chiwawa".equals(r)));
        Assert.assertTrue(Stream.of(mapOfValuesPossiblesChild.get("chien/cocker")).allMatch(r -> "21".equals(r) || "13".equals(r)));
        Assert.assertTrue(Stream.of(mapOfValuesPossiblesChild.get("chien/chiwawa")).allMatch(r -> "15".equals(r) || "19".equals(r)));
        Assert.assertTrue(Stream.of(mapOfValuesPossiblesMiddle.get("chat")).allMatch(r -> "siamois".equals(r) || "persan".equals(r)));
        Assert.assertTrue(Stream.of(mapOfValuesPossiblesChild.get("chat/siamois")).allMatch(r -> "30".equals(r)));
        Assert.assertTrue(Stream.of(mapOfValuesPossiblesChild.get("chat/persan")).allMatch(r -> "31".equals(r)||"20".equals(r)||"15".equals(r)));

    }
}
