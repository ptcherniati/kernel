package org.inra.ecoinfo.refdata.config.impl;

import com.sun.faces.config.ConfigurationException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.dbunit.DatabaseUnitException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class ConfigurationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
public class ConfigurationTest {

    /**
     * The metadata configuration.
     */
    @Autowired
    MetadataConfiguration metadataConfiguration;

    /**
     * Test.
     *
     * @throws URISyntaxException the uRI syntax exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws IllegalAccessException the illegal access exception
     * @throws ConfigurationException the configuration exception
     * @throws PersistenceException the persistence exception
     * @throws DatabaseUnitException the database unit exception
     * @throws SQLException the sQL exception
     * @throws ParserConfigurationException the parser configuration exception
     * @throws org.inra.ecoinfo.config.ConfigurationException
     * @throws ConfigurationException the configuration exception
     */
    @Test
    public void test() throws URISyntaxException, IOException, IllegalAccessException, ConfigurationException, PersistenceException, DatabaseUnitException, SQLException, ParserConfigurationException, org.inra.ecoinfo.config.ConfigurationException {
        Assert.assertTrue("la liste des metadonnées est vide", metadataConfiguration.getMetadatas().size() > 0);
    }
}
