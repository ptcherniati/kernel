package org.inra.ecoinfo.refdata;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.Assert;

/**
 *
 * @author ptcherniati
 */
public class MetadataT extends AbstractCSVMetadataRecorder<MetadataObjectT> {

    /**
     *
     */
    public MetadataT() {
        super();
    }

    /**
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        Assert.assertTrue(true);
    }

    /**
     *
     * @param arg0
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(MetadataObjectT arg0) throws BusinessException {
        final LineModelGridMetadata line = new LineModelGridMetadata();
        line.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(arg0.name, "nom"));
        return line;
    }

    /**
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        Assert.assertTrue(true);
        Assert.assertTrue(true);
    }

    /**
     *
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    protected List<MetadataObjectT> getAllElements() throws BusinessException {
        final List<MetadataObjectT> metadataTests = new LinkedList<>();
        metadataTests.add(new MetadataObjectT("object 1"));
        metadataTests.add(new MetadataObjectT("object 2"));
        return metadataTests;
    }

}
