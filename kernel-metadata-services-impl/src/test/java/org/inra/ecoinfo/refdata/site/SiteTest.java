package org.inra.ecoinfo.refdata.site;

import org.inra.ecoinfo.Variable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * The Class SiteTest.
 */
public class SiteTest {

    /**
     * The Constant HESSE.
     */
    private static final String HESSE = "Hesse";
    /**
     * The Constant HESSE_DESC.s
     */
    private static final String HESSE_DESC = "site de Hesse";
    /**
     * The Constant HESSE1.
     */
    private static final String HESSE1 = "Hesse 1";
    /**
     * The Constant HESSE1_DESC.
     */
    private static final String HESSE1_DESC = "site de Hesse 1";
    /**
     * The Constant HESSE2.
     */
    private static final String HESSE2 = "Hesse 2";
    /**
     * The Constant HESSE2_DESC.
     */
    private static final String HESSE2_DESC = "site de Hesse ";
    /**
     * The hesse.
     */
    private static final Site hesse = new Site(SiteTest.HESSE, SiteTest.HESSE_DESC, null);
    /**
     * The hesse1.
     */
    private static final Site hesse1 = new Site(SiteTest.HESSE1, SiteTest.HESSE1_DESC, hesse);
    /**
     * The hesse2.
     */
    private static final Site hesse2 = new Site(SiteTest.HESSE2, SiteTest.HESSE2_DESC, hesse);

    /**
     * Before.
     *
     * @throws Exception the exception
     */
    @BeforeClass
    public static void before() throws Exception {
        SiteTest.hesse1.setParent(SiteTest.hesse);
        SiteTest.hesse2.setParent(SiteTest.hesse);
        SiteTest.hesse1.setId(Long.valueOf(1));
    }

    /**
     * The hesse test.
     */
    private Site hesseTest;
    /**
     * The hesse1 test.
     */
    private Site hesse1Test;

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test clone only attributes.
     */
    @Test
    public final void testClone() {
        hesse1Test = SiteTest.hesse1.clone();
        assertEquals(SiteTest.hesse1.getCode(), hesse1Test.getCode());
        assertEquals(SiteTest.hesse1.getName(), hesse1Test.getName());
        assertEquals(SiteTest.hesse1.getCode(), hesse1Test.getCode());
        assertEquals(SiteTest.hesse1.getDescription(), hesse1Test.getDescription());
        assertEquals(SiteTest.hesse1.getId(), hesse1Test.getId());
    }

    /**
     * Test get children.
     */
    @Test
    public final void testGetChildren() {
        assertEquals(hesse, hesse1.getParent());
        assertEquals(hesse, hesse2.getParent());
    }

    /**
     * Test get nom.
     */
    @Test
    public final void testGetName() {
        hesseTest = new Site(SiteTest.HESSE, SiteTest.HESSE_DESC, null);
        hesse1Test = new Site();
        hesse1Test.setName("Hesse 1");
        hesse1Test.setParent(hesseTest);
        assertEquals(SiteTest.HESSE1, hesse1Test.getName());
        assertEquals("Hesse/Hesse 1", hesse1Test.getDisplayPath());
        assertEquals("Hesse/Hesse 1", hesse1Test.getPath());
        assertEquals("Hesse/Hesse 1", hesse1Test.getCode());
    }

    /**
     * Test site.
     */
    @Test
    public final void testSite() {
        assertEquals(SiteTest.HESSE1, SiteTest.hesse1.getName());
        assertEquals(SiteTest.HESSE1_DESC, SiteTest.hesse1.getDescription());
        assertEquals(SiteTest.hesse, SiteTest.hesse1.getParent());
        assertEquals("Hesse/Hesse 1", SiteTest.hesse1.getPath());
        assertEquals("Hesse/Hesse 1", SiteTest.hesse1.getDisplayPath());
        assertEquals("Hesse", SiteTest.hesse.getPath());
        assertEquals("Hesse", SiteTest.hesse.getDisplayPath());
        assertEquals("Hesse", SiteTest.hesse.getCode());
        assertEquals("Hesse/Hesse 1", SiteTest.hesse1.getCode());
    }

    /**
     * Test site.
     */
    @Test
    public final void testGetLocalisationEntite() {
        assertEquals("site", Nodeable.getLocalisationEntite(Site.class));
        assertEquals("variable", Nodeable.getLocalisationEntite(Variable.class));
        assertEquals("datatype", Nodeable.getLocalisationEntite(DataType.class));
    }
}
