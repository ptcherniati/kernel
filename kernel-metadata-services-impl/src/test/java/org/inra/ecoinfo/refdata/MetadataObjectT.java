package org.inra.ecoinfo.refdata;

/**
 *
 * @author ptcherniati
 */
public class MetadataObjectT {

    String name;

    /**
     *
     * @param string
     * @param name
     */
    public MetadataObjectT(String name) {
        super();
        this.name = name;
    }

    /**
     *
     * @return
     */
    public Object getName() {
        return this.name;
    }
}
