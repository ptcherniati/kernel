package org.inra.ecoinfo.mga.jsf;

import com.google.common.base.Strings;
import java.time.DateTimeException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.utils.DateUtil;
import org.primefaces.validate.ClientValidator;

/**
 * Custom JSF Validator for Email input
 */
@FacesValidator(DateTreeValidator.ID)
public class DateTreeValidator implements Validator, ClientValidator {

    /**
     *
     */
    public static final String ID = "custom.dateTreeValidator";
    private static final String BUNDLE_PATH_INDEX = "org.inra.ecoinfo.jsf.date_converter";
    private static final String PARAM_MSG_ALERT_INVALID_DATE = "PARAM_MSG_ALERT_INVALID_DATE";
    private final List<String> PATTERNS = Arrays.asList(new String[]{"**/**/**", PatternConfigurator.INVALIDATE_DATE_STRING});

    /**
     *
     */
    public DateTreeValidator() {
    }

    /**
     *
     * @param context
     * @param component
     * @param value
     * @throws ValidatorException
     */
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value == null) {
            return;
        }

        if (value instanceof String && Strings.isNullOrEmpty((String) value) || PATTERNS.contains((String) value)) {
                return;
        }
        try {
            DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, (String) value);
        } catch (DateTimeException e) {
            Locale locale = context.getExternalContext().getRequestLocale();
            ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_PATH_INDEX, locale);
            String message = String.format(
                    bundle.getString(PARAM_MSG_ALERT_INVALID_DATE),
                    value
            );
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", message), e);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Map<String, Object> getMetadata() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String getValidatorId() {
        return ID;
    }

}
