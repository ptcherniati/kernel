/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.managedbean;

import java.io.Serializable;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.groups.Group;

/**
 *
 * @author tcherniatinsky
 */
public interface IGroup extends Serializable {

    /**
     *
     * @return
     */
    String getLogin();

    /**
     *
     * @return
     */
    String getNom();

    /**
     *
     * @return
     */
    String getPrenom();

    /**
     *
     * @return
     */
    IUser getUser();

    /**
     *
     * @return
     */
    Group getGroup();

}
