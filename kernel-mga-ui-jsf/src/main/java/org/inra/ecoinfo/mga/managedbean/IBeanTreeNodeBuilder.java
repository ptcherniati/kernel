/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.managedbean;

import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author tcherniatinsky
 */
public interface IBeanTreeNodeBuilder {

    /**
     *
     * @param <T>
     * @param flatNode
     * @return
     */
    <T extends IFlatNode> TreeNode getOrCreateTreeNode(ITreeNode<T> flatNode);
}
