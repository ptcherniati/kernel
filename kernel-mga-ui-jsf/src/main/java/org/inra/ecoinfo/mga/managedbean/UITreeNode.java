/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.managedbean;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author tcherniatinsky
 */
public class UITreeNode extends DefaultTreeNode {

    private final IBeanTreeNodeBuilder outer;
    private boolean childrenFetched;

    /**
     *
     * @param <T>
     * @param flatTreeNode
     * @param outer
     * @param map
     * @param cashMap
     */
    public <T extends IFlatNode> UITreeNode(ITreeNode<T> flatTreeNode, final IBeanTreeNodeBuilder outer, Map<Long, TreeNode> cashMap) {
        super(flatTreeNode.getData() == null ? "root" : flatTreeNode.getData().getTypeResource().toString(),
                flatTreeNode,
                null);
        if (flatTreeNode.getData() == null) {
            cashMap.computeIfAbsent(Long.MIN_VALUE, k -> this);
        } else {
            cashMap.computeIfAbsent(flatTreeNode.getData().getId(), k -> this);
        }
        this.outer = outer;
    }

    /**
     *
     * @return
     */
    public IFlatNode getFlatNode() {
        return getTreeFlatNode().getData();
    }

    /**
     *
     * @return
     */
    protected ITreeNode<IFlatNode> getTreeFlatNode() {
        return (ITreeNode<IFlatNode>) getData();
    }

    /**
     *
     * @return
     */
    @Override
    public List<TreeNode> getChildren() {
        ensureChildrenFetched();
        return super.getChildren();
    }

    /**
     *
     * @return
     */
    @Override
    public int getChildCount() {
        ensureChildrenFetched();
        return super.getChildCount();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isLeaf() {
        ensureChildrenFetched();
        return super.isLeaf();
    }

    private void ensureChildrenFetched() {
        if (!childrenFetched) {
            childrenFetched = true;
            UITreeNode ref = this;
            final List<TreeNode> children = getTreeFlatNode().getChildren().stream()
                    .map((ITreeNode<IFlatNode> f) -> (TreeNode) outer.getOrCreateTreeNode(f))
                    .filter(c -> !c.equals(ref))
                    .collect(Collectors.toList());
            super.getChildren().addAll(children);
        }
    }

}
