/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.managedbean;

import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.WhichTree;

/**
 *
 * @author tcherniatinsky
 */
public class UserVO implements IGroup {
    private static final String BUNDLE_NAME = "org.inra.ecoinfo.treetable.jsf.securityTreePanel";

    /**
     *
     */
    public static final String PARAM_MSG_USER_SELECTED = "PARAM_MSG_USER_SELECTED";

    /**
     *
     */
    public static final String PARAM_MSG_NO_USER_SELECTED = "PARAM_MSG_NO_USER_SELECTED";

    
    /**
     * The utilisateur.
     */
    Group group;
    IUser user;
    ILocalizationEntitiesManager localizationEntitiesManager;

    /**
     *
     * @param localizationEntitiesManager
     */
    public UserVO(ILocalizationEntitiesManager localizationEntitiesManager) {
    }
    /**
     *
     * @param compositeGroup
     * @param whichTree
     */
    public UserVO(ICompositeGroup compositeGroup, WhichTree whichTree) {
        if(compositeGroup instanceof IUser){
            this.user = (IUser)compositeGroup;
            this.group=this.user.getOwnGroup(whichTree);
        }else{
            this.group = (Group) compositeGroup;
        }
    }
    
    /**
     *
     * @return
     */
    public boolean isHasRights() {
        return !group.getActivitiesMap().isEmpty();
    }

    /**
     *
     */
    public void setHasRights() {
        //nothing to do
    }


    /**
     *
     * @return
     */
    public String getEmail() {
        return user==null?"":user.getEmail();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return group.getId();
    }

    /**
     *
     * @return
     */
    public Boolean getIsRoot() {
        return user==null?Boolean.FALSE:user.getIsRoot();
    }

    /**
     *
     * @return
     */
    public String getLanguage() {
        return user==null?"":user.getLanguage();
    }

    /**
     *
     * @return
     */
    @Override
    public String getLogin() {
        return group.getGroupName();
    }

    /**
     *
     * @return
     */
    @Override
    public String getNom() {
        return user==null?group.getGroupName():user.getNom();
    }

    
    /**
     *
     * @return
     */
    public String getPassword() {
        return user==null?"":user.getPassword();
    }

    /**
     *
     * @return
     */
    @Override
    public String getPrenom() {
        return user==null?"":user.getPrenom();
    }

    /**
     *
     * @return
     */
    @Override
    public IUser getUser() {
        return user;
    }

    /**
     *
     * @return
     */
    @Override
    public Group getGroup() {
        return group;
    }

    /**
     *
     * @return
     */
    public String getStyleClass(){
        if(getIsRoot()){
            return "superAdmin";
        }else if(isHasRights()){
            return user==null?"HasRights group":"HasRights user";
        }
        return  user==null?"noRight group":"noRight user";
    }

    /**
     *
     * @return
     */
    public String getNoUserVOSelected(){
        return PARAM_MSG_NO_USER_SELECTED;
    }

    /**
     *
     * @return
     */
    public String getUserVOSelected(){
        return PARAM_MSG_USER_SELECTED;
    }
}
