package org.inra.ecoinfo.mga.managedbean;

/**
 *
 * @author yahiaoui
 */
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Strings;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.FlatNode;
import static org.inra.ecoinfo.mga.business.composite.FlatNode.*;
import org.inra.ecoinfo.mga.business.composite.ICompositeActivity;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.ActivityCondition;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTreatement;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.enums.WhichUser;
import org.inra.ecoinfo.mga.utils.MgaUtils;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tcherniatinsky
 */
@ManagedBean(name = "uiBeanActionTree")
@ViewScoped
public class UIBeanActionTree implements Serializable, IBeanTreeNodeBuilder {

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(UIBeanActionTree.class);
    private static final String BUNDLE_NAME = "org.inra.ecoinfo.treetable.jsf.securityTreePanel";
    private static final String PROPERTY_MSG_REMOVE_ALERT_WITH_ASSOCIATES = "PROPERTY_MSG_REMOVE_ALERT_WITH_ASSOCIATES";
    private static final String PROPERTY_MSG_REMOVE_ALERT = "PROPERTY_MSG_REMOVE_ALERT";
    private final Map<Locale, ResourceBundle> resourceBundles = new HashMap<>();

    /**
     *
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;

    /**
     *
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationEntitiesManager localizationManager;

    /**
     *
     */
    protected final String formatter = PatternConfigurator.dateFormat;

    /* Built Tree*/
    /**
     *
     */
    protected TreeNode currentTreeNode = new DefaultTreeNode(PatternConfigurator.EMPTY_STRING, null, null);

    /* Onselecte TreeNode */
    /**
     *
     */
    protected TreeNode selectedTreeNode = new DefaultTreeNode(PatternConfigurator.EMPTY_STRING, null, null);

    /**
     *
     */
    protected String newval;

    /**
     *
     */
    protected boolean check;

    /**
     *
     */
    protected ICompositeGroup selectedUser;

    /**
     *
     */
    protected NodeExpandEvent currentNodeEvent;

    /**
     *
     */
    protected final Map<Long, TreeNode> cashMap = new HashMap();
    /*
    Dynamic Primefaces Table
     */

    /**
     *
     */
    protected String[] valuesColumns;
    /**
     * The utilisateur.
     */
    IUser utilisateur;

    /**
     *
     */
    protected IUser currentUser;

    /**
     *
     */
    protected final Map<String, List<UserVO>> utilisateursVO = new HashMap();
    /**
     * The current utilisateurVO.
     */
    protected UserVO currentUserVO;

    /**
     *
     */
    protected String viewid = "";
    private String _groupName;
    /**
     *
     * @param code
     * @return
     */
    Map<String, Map<String, String>> internationalizedRefdataCodes = new HashMap<>();
    Map<Long, String> internationalizedNodeName = new HashMap<>();

    /**
     *
     */
    public UIBeanActionTree() {
    }

    /**
     *
     * @param configurationNumber
     * @return
     */
    protected int getConfigurationNumber(String configurationNumber) {
        Integer cN = 0;
        try {
            cN = Integer.valueOf(configurationNumber);
        } catch (NumberFormatException e) {
        }
        return cN;
    }

    /**
     *
     * @param nce
     */
    public void collapseTreeNodeEvent(NodeCollapseEvent nce) {

        nce.getTreeNode().setExpanded(false);
        nce.getTreeNode().getChildren().stream()
                .forEach(child -> collapseTreeNode(child));
    }

    /**
     *
     * @param treeNode
     */
    protected void collapseTreeNode(org.primefaces.model.TreeNode treeNode) {
        treeNode.setExpanded(false);
        treeNode.getChildren().stream()
                .peek(child -> child.setExpanded(false))
                .filter((child) -> (!child.isLeaf()))
                .forEach(child -> collapseTreeNode(child));
    }

    /**
     *
     * @param b
     */
    public void setUpdateTreeNodeValue(boolean b) {

    }

    /**
     *
     * @param nodeId
     * @param jsfCheckBoxId
     * @param privil
     * @return
     */
    public boolean updateTreeNodeValue(String nodeId, String jsfCheckBoxId, String privil) {

        Long idNode;
        try {
            idNode = Long.parseLong(nodeId);
        } catch (NumberFormatException x) {
            return false;
        }
        TreeNode treeNode = findTreeNodeCashingChildrens(idNode).orElse(null);
        Activities activity = Activities.convertToActivity(privil);

        Objects.requireNonNull(treeNode);
        final ITreeNode data = (ITreeNode) treeNode.getData();
        IFlatNode node = (IFlatNode) data.getData();

        Integer state;

        if (node.getOwnTmpState().containsKey(activity)) {
            final Map<Integer, List<LocalDate>> activities = node.getOwnTmpState().get(activity);
            state = new ArrayList<>(activities.keySet()).get(0);
        } else if (node.getOwnRealState().containsKey(activity)) {
            state = node.getOwnRealState().get(activity);
        } else {
            state = UNCHECKED_STATE;
        }

        if (state == CHECKED_STATE) {
            return true;
        }

        if (state == INDETERMINATE_STATE) {
            callScript(jsfCheckBoxId.replace(":" + privil, ":PRIVILEGE_INPUT")
                    + "[" + privil + "(" + state + ", **/**/****,**/**/****)]");
            return false;
        }

        return false;
    }

    /**
     *
     * @param id
     * @return
     */
    protected Optional<TreeNode> findTreeNodeCashingChildrens(Long id) {
        if (cashMap.get(id) == null) {
            return this.policyManager.getTreeNodeById((ITreeNode<IFlatNode>) currentTreeNode.getData(), id)
                    .map(node -> getOrCreateTreeNode(node))
                    .map(Stream::of)
                    .orElse(Stream.empty())
                    .peek(treeNode -> getChildrensTreeNode(treeNode, cashMap))
                    .peek(treeNode -> cashMap.put(id, treeNode))
                    .collect(Collectors.toSet()).stream()
                    .findFirst();
        }
        return Optional.ofNullable(cashMap.get(id));
    }

    /**
     *
     * @param id
     * @return
     */
    protected TreeNode findTreeNode(Long id) {
        return cashMap.getOrDefault(
                id,
                this.policyManager.getTreeNodeById((ITreeNode<IFlatNode>) currentTreeNode.getData(), id)
                        .map(node -> getOrCreateTreeNode(node))
                        .orElse(null)
        );
    }

    /**
     *
     * @param ned
     */
    public void expandTreeNodeEvent(NodeExpandEvent ned) {

        if (cashMap.size() > 1_000) {
            cashMap.clear();
        }
        Objects.requireNonNull(ned.getTreeNode());

        this.currentNodeEvent = ned;
        ned.getTreeNode().setExpanded(true);
        ned.getTreeNode().getChildren().stream().
                forEach(child -> collapseTreeNode(child));

        Long nodeID = (Long) ned.getComponent().getAttributes().get("nodeId");
        findTreeNodeCashingChildrens(nodeID);
    }

    /**
     *
     * @param idTree
     */
    public void resetSelectedTree(String idTree) {

        this.currentTreeNode = new DefaultTreeNode(PatternConfigurator.EMPTY_STRING, null, null);
        this.selectedTreeNode = new DefaultTreeNode(PatternConfigurator.EMPTY_STRING, null, null);
        this.valuesColumns = null;

        if (!Strings.isNullOrEmpty(idTree)) {
            RequestContext.getCurrentInstance().update(idTree);
        }
    }

    /**
     *
     * @param treeNode
     * @param includeCollider
     * @param childrenIds
     * @return
     */
    protected Set<Long> getAllChildrensIds(TreeNode treeNode,
            boolean includeCollider,
            Set<Long> childrenIds) {

        if (treeNode != null) {
            Optional.ofNullable((IFlatNode) ((ITreeNode) treeNode.getData()).getData())
                    .ifPresent((node) -> {
                        childrenIds.add(node.getId());
                        if (includeCollider) {
                            childrenIds.addAll(node.getColliderNodes());
                        }
                    });
            treeNode.getChildren().stream().forEach((childrenNode) -> {
                if (childrenNode.isLeaf()) {
                    childrenIds.add(((IFlatNode) ((ITreeNode) childrenNode.getData()).getData()).getId());
                    if (includeCollider) {
                        childrenIds.addAll(((IFlatNode) ((ITreeNode) childrenNode.getData()).getData()).getColliderNodes());
                    }
                } else {
                    getAllChildrensIds(childrenNode, includeCollider, childrenIds);
                }
            });
        }
        return childrenIds;
    }

    /**
     *
     * @param treeNode
     * @param childrenTreeNodes
     * @return
     */
    protected Map<Long, TreeNode> getChildrensTreeNode(TreeNode treeNode, Map<Long, TreeNode> childrenTreeNodes) {
        treeNode.getChildren().stream()
                .forEach(childrenNode -> childrenTreeNodes.put(((IFlatNode) ((ITreeNode) childrenNode.getData()).getData()).getId(), childrenNode));
        return childrenTreeNodes;
    }

    /**
     *
     * @param treeNode
     * @param activity
     * @param state
     * @param dates
     * @param treatedIds
     */
    protected void setAllChildrensNodesState(TreeNode treeNode,
            Activities activity,
            Integer state, List<LocalDate> dates,
            Set<Long> treatedIds) {

        treeNode.getChildren().stream()
                .peek(childrenNode -> replaceTreeNodeState(activity, state, dates, childrenNode, treatedIds))
                .filter((childrenNode) -> (!childrenNode.isLeaf()))
                .forEach(childrenNode -> setAllChildrensNodesState(childrenNode, activity, state, dates, treatedIds));
    }

    private void replaceTreeNodeState(Activities activity, Integer state, List<LocalDate> dates, TreeNode childrenNode, Set<Long> treatedIds) {
        final IFlatNode flatNode = (IFlatNode) ((ITreeNode) childrenNode.getData()).getData();
        Map<Activities, Map<Integer, List<LocalDate>>> ownTmpState = flatNode.getOwnTmpState();
        ownTmpState.remove(activity);
        ownTmpState
                .computeIfAbsent(activity, k -> new HashMap())
                .put(state, dates);
        treatedIds.add(flatNode.getId());
    }

    /**
     *
     * @param root
     * @param checkedMap
     */
    protected void getCheckedStates(TreeNode root,
            Map<Activities, Map<Long, List<LocalDate>>> checkedMap) {
        if (root.getData() != null && ((ITreeNode) root.getData()).getData() != null) {
            IFlatNode parentNode = ((IFlatNode) ((ITreeNode) root.getData()).getData());
            Map<Activities, Map<Long, List<LocalDate>>> checkedActivitiesParentNode
                    = parentNode.getOwnCheckedActivities();

            checkedActivitiesParentNode.keySet().stream()
                    .forEach((priv) -> {
                        if (checkedMap.containsKey(priv)) {
                            checkedMap.get(priv).putAll(checkedActivitiesParentNode.get(priv));
                        } else {
                            checkedMap.putAll(checkedActivitiesParentNode);
                        }
                    });
        }
        root.getChildren().stream()
                .filter(child -> child.getData() != null && ((ITreeNode) child.getData()).getData() != null)
                .forEach((childrenTree) -> {
                    IFlatNode childNode = ((IFlatNode) ((ITreeNode) childrenTree.getData()).getData());
                    Map<Activities, Map<Long, List<LocalDate>>> checkedActivitiesChildNode = childNode.getOwnCheckedActivities();
                    checkedActivitiesChildNode.entrySet().stream()
                            .forEach(entryActivity -> checkedMap.computeIfAbsent(entryActivity.getKey(), k -> new HashMap()).putAll(entryActivity.getValue()));
                    if (!childrenTree.isLeaf()) {
                        getCheckedStates(childrenTree, checkedMap);
                    }
                });

    }

    /**
     *
     * @param codeConfiguration
     * @param whichUser
     * @param whichTreatement
     * @throws IOException
     */
    public void save(Integer codeConfiguration, String whichUser, String whichTreatement) throws IOException {
        Map<Activities, Map<Long, List<LocalDate>>> checkedMap = new HashMap<>();

        getCheckedStates(currentTreeNode, checkedMap);

        ICompositeActivity role = currentUserVO.getGroup();

        WhichTreatement whichTreatements = WhichTreatement
                .convertToWhichTreatement(whichTreatement);

        if (whichTreatements == WhichTreatement.SAVE) {
            if (role != null) {
                role.setActivitiesMap(checkedMap);
            }
        } else if (whichTreatements == WhichTreatement.UPDATE) {

            List<Activities> activities = this.policyManager
                    .getMgaServiceBuilder()
                    .getMgaIOConfigurator()
                    .getConfiguration(codeConfiguration)
                    .map(conf -> Arrays.asList(conf.getActivities()))
                    .orElseGet(LinkedList::new);

            Set<Long> set = new HashSet();
            getAllChildrensIds(currentTreeNode, true, set);

            if (role != null) {

                for (Activities p : activities) {
                    if (role.getActivitiesMap().containsKey(p)) {
                        role.getActivitiesMap().get(p).keySet().removeAll(set);

                        if (checkedMap.containsKey(p)) {
                            role.getActivitiesMap().get(p).putAll(checkedMap.get(p));
                        }

                        if (role.getActivitiesMap().get(p).keySet().isEmpty()) {
                            role.getActivitiesMap().remove(p);
                        }
                    } else if (checkedMap.containsKey(p)) {
                        role.getActivitiesMap().put(p, checkedMap.get(p));
                    }
                }
                set.clear();
                set = null;
                role.synchronizeActivitiesMap();
            }
        }

        this.policyManager.mergeActivity(role);
        final Map<Long, List<LocalDate>> extraActivity = Optional.ofNullable(role)
                .map(r -> r.getActivitiesMap())
                .map(am -> am.get(Activities.extraction))
                .orElse(new HashMap());

        this.policyManager.persistExtractActivity(currentUserVO.getLogin(), extraActivity);

        this.policyManager.removeActivity(currentUserVO.getGroup(), this.policyManager
                .getMgaServiceBuilder()
                .getMgaIOConfigurator()
                .getConfiguration(codeConfiguration)
                .map(conf -> conf.getWhichTree())
                .orElse(WhichTree.TREEDATASET));

        this.policyManager.removeTreeFromSession(currentUserVO.getGroup(), codeConfiguration);

        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('myDialogSave').show();");
    }

    /**
     * Shared METHOD ( FIXE )
     *
     * @param codeConfiguration
     * @throws java.io.IOException
     */
    public void saveIntermediateState(Integer codeConfiguration) throws IOException {

        Map<String, String> params = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap();

        Map< Activities, Map< Long, Map<String, Object>>> indeterminateMap
                = MgaUtils.jsonToMap(params.get(PatternConfigurator.ID_INTERMEDIATE_STATE),
                        new TypeReference<  Map< Activities, Map< Long, Map<String, Object>>>>() {
                });

        Set<Long> treatedIds = new HashSet();
        indeterminateMap.entrySet().stream()
                .forEach((activityEntry) -> {
                    Activities activity = activityEntry.getKey();
                    Map<Long, Map<String, Object>> stateNode = activityEntry.getValue();
                    stateNode.entrySet().stream()
                            .filter((entryNode) -> !(treatedIds.contains(entryNode.getKey())))
                            .forEach((entryNode) -> {
                                Map<String, Object> get = entryNode.getValue();
                                Long id = entryNode.getKey();
                                List<LocalDate> dates = (List<LocalDate>) get.get("date");
                                Integer state = (Integer) get.get("state");
                                TreeNode treeNodeById = findTreeNode(id);
                                IFlatNode node = (IFlatNode) ((ITreeNode) treeNodeById.getData()).getData();
                                node.getOwnTmpState()
                                        .computeIfAbsent(activity, k -> new HashMap<>())
                                        .clear();
                                node.getOwnTmpState().get(activity).put(state, dates);
                                treatedIds.add(node.getId());
                                if (state != 2 && !treeNodeById.isExpanded()) {
                                    setAllChildrensNodesState(treeNodeById,
                                            activity,
                                            state,
                                            dates,
                                            treatedIds);
                                }
                            });
                });
        treatedIds.clear();
    }

    /**
     *
     * @param param
     */
    protected void callScript(String param) {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("setStateBoolAndDateInputTextUsingParam('" + param + "');");
    }

    /**
     *
     * @return
     */
    public IPolicyManager getPolicyManager() {
        return policyManager;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @return
     */
    public TreeNode getCurrentTreeNode() {
        return currentTreeNode;

    }

    /**
     *
     * @param currentTreeNode
     */
    public void setCurrentTreeNode(TreeNode currentTreeNode) {
        this.currentTreeNode = currentTreeNode;
    }

    /**
     *
     * @return
     */
    public TreeNode getSelectedTreeNode() {
        return selectedTreeNode;
    }

    /**
     *
     * @param selectedTreeNode
     */
    public void setSelectedTreeNode(TreeNode selectedTreeNode) {

        if (selectedTreeNode != null) {
            this.selectedTreeNode = selectedTreeNode;
            updateColumns();
        }
    }

    /**
     *
     * @param activityCondition
     * @return
     */
    protected ActivityCondition getPrivilCondition(String activityCondition) {
        return ActivityCondition.convertToActivityCondition(activityCondition.toUpperCase());
    }

    /**
     *
     * @param userSelection
     * @return
     */
    public ICompositeGroup getUser(String userSelection) {

        if (WhichUser.convertToWhichUser(userSelection.toUpperCase())
                == WhichUser.CURRENTUSER) {
            return this.policyManager.getCurrentUser();
        } else if (WhichUser.convertToWhichUser(userSelection.toUpperCase())
                == WhichUser.SELECTEDUSER) {
            return Optional.ofNullable((ICompositeGroup) getCurrentUserVO().getUser()).orElse(getCurrentUserVO().getGroup());
        }

        return null;
    }

    /**
     *
     * @return
     */
    public IUser getSelectedUser() {
        return currentUserVO.getUser();
    }

    /**
     *
     * @param selectedUser
     */
    public void setSelectedUser(IUser selectedUser) {
        this.selectedUser = selectedUser;
    }

    /**
     *
     * @param configuration
     * @return
     */
    public Activities[] getActivities(long configuration) {

        if (configuration == AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS && currentUserVO != null && "public".equals(currentUserVO.getGroup().getGroupName())) {
            return new Activities[]{Activities.synthese, Activities.extraction};
        }
        return this.policyManager.getMgaServiceBuilder()
                .getMgaIOConfigurator()
                .getConfiguration((int) configuration)
                .map(conf -> conf.getActivities())
                .orElse(new Activities[0]);
    }

    /**
     *
     * @param configurationNumber
     * @param node
     * @return
     */
    public List<String> dynamicColumnsNames(String configurationNumber, ITreeNode node) {
        if (node == null) {
            return this.policyManager.getMgaServiceBuilder()
                    .getMgaIOConfigurator()
                    .getColumnsTreeNames(getConfigurationNumber(
                            configurationNumber.equals("-1")?"5":configurationNumber
                    ));
        }
        ITreeNode leaf = node;
        List<Optional<RealNode>> brancheNodes = new LinkedList<>();
        while (leaf.getChildren().size() > 0) {
            leaf = (ITreeNode) leaf.getChildren().get(0);
        }
        ITreeNode parent = leaf;
        while (parent != null && parent.getData() != null) {
            brancheNodes.add(0, policyManager.getMgaServiceBuilder().getRecorder().getRealNodeById(((FlatNode) parent.getData()).getRealNodeId()));
            parent = parent.getParent();
        }
        return this.policyManager
                .getMgaServiceBuilder()
                .getMgaIOConfigurator()
                .getColumnsTreeNames(getConfigurationNumber(configurationNumber), brancheNodes);
    }

    /**
     * ((FlatNode)
     *
     */
    public void updateColumns() {
        valuesColumns = new String[this.policyManager.
                getMgaServiceBuilder().
                getMgaIOConfigurator().
                getDeepestIndexNode() + 1];

        if (selectedTreeNode == null || selectedTreeNode.getData() == null) {
            return;
        }

        IFlatNode currentNode = (IFlatNode) ((ITreeNode) selectedTreeNode.getData()).getData();

        valuesColumns[selectedTreeNode.getRowKey()
                .split(PatternConfigurator.UNDERSCORE).length - 1] = getLocalName(currentNode);

        TreeNode parentTreeNode = selectedTreeNode.getParent();

        while (parentTreeNode != null && parentTreeNode.getData() != null) {

            IFlatNode parentNode = (IFlatNode) ((ITreeNode) parentTreeNode.getData()).getData();

            if (parentNode == null) {
                break;
            }
            valuesColumns[parentTreeNode.getRowKey()
                    .split(PatternConfigurator.UNDERSCORE).length - 1]
                    = getLocalName(parentNode);

            parentTreeNode = parentTreeNode.getParent();

        }
    }

    /**
     *
     * @param index
     * @return
     */
    public String getColumnName(Integer index) {

        return valuesColumns == null || index >= valuesColumns.length ? null : valuesColumns[index];
    }

    /**
     * *********************** Temporary Dates Save
     *
     * @param codeConfiguration
     * @throws java.io.IOException
     */
    public void saveTemporaryDates(Integer codeConfiguration) throws IOException, DateTimeParseException {
        Map<String, String> params = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap();
        Map< Activities, List<List>> temporaryDates = MgaUtils
                .jsonToMap(params.get(PatternConfigurator.TEMPORARY_DATES),
                        new TypeReference< Map< Activities, List<List>>>() {
                });

        String whichDate = params.get(PatternConfigurator.WHICH_DATES);

        int indexDate = -1;
        if (whichDate.equals(PatternConfigurator.DATE_BEG)) {
            indexDate = 0;
        } else if (whichDate.equals(PatternConfigurator.DATE_END)) {
            indexDate = 1;
        }

        Set<Long> treatedIds = new HashSet();

        for (Activities activity : temporaryDates.keySet()) {

            List<List> activities = Optional.ofNullable(temporaryDates).map(td -> td.get(activity)).orElse(new LinkedList());

            for (List elementsList : activities) {
                Long id = Long.parseLong((String) elementsList.get(0));
                final boolean alreadyTreated = treatedIds.contains(id);
                if (alreadyTreated) {
                    continue;
                }

                LocalDate date = null;
                String stringDate = (String) elementsList.get(1);
                if (!stringDate.isEmpty()) {
                    date = DateUtil.readLocalDateTimeFromText(formatter, stringDate).toLocalDate();
                }
                TreeNode treeNodeById = findTreeNodeCashingChildrens(id).orElse(null);
                setNodeDate(activity, treeNodeById, indexDate, date, treatedIds);

            }
        }
        treatedIds.clear();

    }

    /**
     *
     * @param treeNode
     * @param activity
     * @param indexDate
     * @param date
     * @param treatedIds
     */
    protected void setAllChildrensNodesDate(TreeNode treeNode,
            final Activities activity,
            final int indexDate,
            final LocalDate date, final Set<Long> treatedIds) {

        treeNode.getChildren().stream()
                .forEach(childrenNode -> setNodeDate(activity, childrenNode, indexDate, date, treatedIds));
    }

    /**
     *
     * @param activity
     * @param node
     * @param indexDate
     * @param date
     * @param treatedIds
     */
    protected void setNodeDate(final Activities activity, TreeNode node,
            final int indexDate,
            final LocalDate date, final Set<Long> treatedIds) {
        IFlatNode flatNode = (IFlatNode) ((ITreeNode) node.getData()).getData();
        int state = 0;
        final Map<Integer, List<LocalDate>> activities = flatNode.getOwnTmpState().get(activity);
        if (activities == null) {
            state = flatNode.copyRealToTmpState(activity);
        } else {
            state = (Integer) activities.keySet().toArray()[0];
        }
        flatNode.getOwnTmpState()
                .computeIfAbsent(activity, k -> new HashMap<>())
                .computeIfAbsent(state, k -> Arrays.asList(new LocalDate[]{null, null}))
                .set(indexDate, date);
        treatedIds.add(flatNode.getId());
        if (!node.isLeaf() && (date == null || !date.equals(DateUtil.MIN_LOCAL_DATE))) {
            setAllChildrensNodesDate(node, activity, indexDate, date, treatedIds);
        }
    }

    /**
     * *********************** LOAD Trees
     *
     * @param codeConfiguration
     * @param compositeGroup
     */
    public void loadTreeForUser(Integer codeConfiguration, ICompositeGroup compositeGroup) {

        cashMap.clear();
        if (compositeGroup == null) {
            currentTreeNode = null;
        } else {
            ITreeNode<IFlatNode> root = this.policyManager.getOrLoadTree(compositeGroup, codeConfiguration);
            currentTreeNode = getOrCreateTreeNode(root);
        }

    }

    /**
     *
     * @param codeConfiguration
     * @param whichUser
     * @param activityCondition
     * @param activities
     */
    public void loadTreeForUserAccordingLeafsRestrictions(Integer codeConfiguration,
            String whichUser,
            String activityCondition,
            String activities) {

        ICompositeGroup compositeGroup = getUser(whichUser);
        compositeGroup = compositeGroup != null ? compositeGroup : policyManager.getMgaServiceBuilder().getRecorder().getPublicGroup().get();
        ActivityCondition condition = getPrivilCondition(activityCondition);
        if (policyManager.isRoot()) {
            loadTreeForUser(codeConfiguration, compositeGroup);
            return;
        }
        List<Activities> privs = Activities.convertToListActivities(activities);
        loadTreeOfUserXRestrictedByCurrentUserActivityConditionOnLeafs(codeConfiguration, compositeGroup, condition, privs);

    }

    /**
     * load restricted tree of user whichUser according restriction to current
     * activities "activities" of current user.
     *
     * @param codeConfiguration
     * @param whichUser
     * @param activityCondition "ALL" or "ATLEAST" ;
     * @param activities
     */
    protected void loadTreeForUserAccordingRestrictions(Integer codeConfiguration,
            String whichUser,
            String activityCondition,
            String activities) {

        ICompositeGroup compositeGroup = getUser(whichUser);
        ActivityCondition condition = getPrivilCondition(activityCondition);
        List<Activities> privs = Activities.convertToListActivities(activities);

        loadTreeOfUserXRestrictedByCurrentUserActivityCondition(codeConfiguration, compositeGroup, condition, privs);

    }

    /**
     *
     * @param codeConfiguration
     * @param compositeGroup
     * @param privilsConditions
     * @param activitiesRestriction
     */
    protected void loadTreeOfUserXRestrictedByCurrentUserActivityCondition(
            Integer codeConfiguration,
            ICompositeGroup compositeGroup,
            ActivityCondition privilsConditions,
            List<Activities> activitiesRestriction) {
        cashMap.clear();
        final WhichTree whichTree = this.policyManager.getMgaServiceBuilder().
                getMgaIOConfigurator().getConfiguration(codeConfiguration)
                .map(conf -> conf.getWhichTree())
                .orElse(WhichTree.TREEDATASET);

        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> rolesCurrentUser = this.policyManager.getOrLoadActivities(this.policyManager.getCurrentUser(), whichTree);
        ITreeNode<IFlatNode> restrictTree = this.policyManager.restrictTree(
                this.policyManager.getOrLoadTree(compositeGroup, codeConfiguration),
                rolesCurrentUser,
                privilsConditions,
                activitiesRestriction
        );
        this.currentTreeNode = getOrCreateTreeNode(restrictTree);
    }

    /**
     *
     * @param codeConfiguration
     * @param compositeGroup
     * @param conditionPrivil
     * @param conditions
     */
    protected void loadTreeOfUserXRestrictedByCurrentUserActivityConditionOnLeafs(
            Integer codeConfiguration,
            ICompositeGroup compositeGroup,
            ActivityCondition conditionPrivil,
            List<Activities> conditions) {

        cashMap.clear();
        final ITreeNode<IFlatNode> abstractTree = this.policyManager.getOrLoadTree(compositeGroup, codeConfiguration);

        /* Construir un arbre pour l'utilisateur X puis le filter en fonction
           des droits de l'utilisateur courant et des conditionPrivil + conditions */
        currentTreeNode = getOrCreateTreeNode(this.policyManager.restrictTreeOnLeafsAccordingActivities(abstractTree,
                conditionPrivil,
                conditions
        ));

    }

    /**
     *
     * @return
     */
    public UserVO getCurrentUserVO() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        String actual = facesContext.getViewRoot().getViewId();
        if (!actual.equals(viewid)) {
            currentUserVO = null;
            viewid = actual;
        }
        return currentUserVO;
    }

    /**
     *
     * @param currentUserVO
     */
    public void setCurrentUserVO(UserVO currentUserVO) {
        this.currentUserVO = currentUserVO;
    }

    /**
     *
     * @return
     */
    public IUser getUser() {
        return utilisateur;
    }

    /**
     *
     * @param group
     */
    public void setUser(ICompositeGroup group) {
        //do nothing
    }

    /**
     *
     * @param configurationNumberString
     * @return
     */
    public List<UserVO> getUsersOrRoles(String configurationNumberString) {
        Integer codeConfiguration = getConfigurationNumber(configurationNumberString);
        if (utilisateursVO.get(configurationNumberString) == null) {
            List<ICompositeGroup> retrieveAllUsersOrRoles = this.policyManager.retrieveAllUserOrGroups(codeConfiguration);

            retrieveAllUsersOrRoles.stream()
                    .filter(cg->!"anonymous".equals(cg.getGroupName()))
                    .forEach(user -> addUtilisateurVO(user, configurationNumberString));
        }
        return utilisateursVO.get(configurationNumberString);
    }

    /**
     *
     * @return
     */
    public IUser getCurrentUser() {
        return currentUser;
    }

    /**
     *
     * @param currentUser
     */
    public void setCurrentUser(IUser currentUser) {
        this.currentUser = currentUser;
    }
    /**
     *
     * @param flatNode
     * @return
     */
    public String getLocalName(IFlatNode flatNode) {
        return internationalizedNodeName
                .computeIfAbsent(flatNode.getId(), k->localizationManager.getLocalName(flatNode));
    }

    /**
     *
     * @param treeNode
     * @return
     */
    public String getLocalName(ITreeNode treeNode) {
        return getLocalName((IFlatNode)treeNode.getData());
    }

    /**
     *
     * @param localizationEntitiesManager
     */
    public void setLocalizationManager(ILocalizationEntitiesManager localizationEntitiesManager) {
        this.localizationManager = localizationEntitiesManager;
    }

    /**
     *
     * @param <T>
     * @param flatNode
     * @return
     */
    @Override
    public <T extends IFlatNode> TreeNode getOrCreateTreeNode(ITreeNode<T> flatNode) {
        return new UITreeNode(flatNode, this, cashMap);
    }

    /**
     *
     * @param userOrGroup
     * @param configurationNumberString
     */
    protected void addUtilisateurVO(ICompositeGroup userOrGroup, String configurationNumberString) {
        Integer codeConfiguration = getConfigurationNumber(configurationNumberString);
        WhichTree whichTree = policyManager.getMgaServiceBuilder().getMgaIOConfigurator().getConfiguration(codeConfiguration)
                .map(conf -> conf.getWhichTree())
                .orElse(WhichTree.TREEDATASET);
        final UserVO userVO = new UserVO(userOrGroup, whichTree);
        utilisateursVO
                .computeIfAbsent(configurationNumberString, k -> new LinkedList())
                .add(userVO);
    }

    /**
     *
     * @param groupName
     */
    public void setGroup(String groupName) {
        _groupName = groupName;
    }

    /**
     *
     * @return
     */
    public String getGroup() {
        return _groupName;
    }

    /**
     *
     * @param configurationNumber
     */
    public void handleSaveGroup(String configurationNumber) {
        policyManager.getMgaServiceBuilder().getMgaIOConfigurator().getConfigurations().entrySet()
                .stream()
                .filter(entry -> entry.getKey().toString().equals(configurationNumber))
                .map(entry -> entry.getValue())
                .findAny()
                .map(conf -> policyManager.addGroup(_groupName, conf.getWhichTree(), GroupType.valueOf("SIMPLE_GROUP")))
                .ifPresent(this::sendErrorMessage);
        utilisateursVO.remove(configurationNumber);
    }

    /**
     *
     * @param configurationNumber
     * @param groupName
     */
    public void handleRemoveGroup(String configurationNumber, String groupName) {
        policyManager.getMgaServiceBuilder().getMgaIOConfigurator().getConfigurations().entrySet()
                .stream()
                .filter(entry -> entry.getKey().toString().equals(configurationNumber))
                .map(entry -> entry.getValue())
                .findAny()
                .map(conf -> policyManager.removeGroup(groupName, conf.getWhichTree()))
                .ifPresent(this::sendErrorMessage);
        utilisateursVO.remove(configurationNumber);
    }

    private void sendErrorMessage(BusinessException e) {
        String message = String.format(getMessage(BUNDLE_NAME, e.getMessage()), _groupName);
        RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }

    /**
     *
     * @param user
     * @return
     */
    public String getDeleteAlertForUser(UserVO user) {
        String messsage1 = getMessage(BUNDLE_NAME, PROPERTY_MSG_REMOVE_ALERT_WITH_ASSOCIATES);
        String messsage2 = String.format(getMessage(BUNDLE_NAME, PROPERTY_MSG_REMOVE_ALERT), user.getGroup().getGroupName());
        return new StringBuilder().append(user.isHasRights() ? messsage1 : "").append(messsage2).toString();
    }

    private String getMessage(final String bundleSourcePath, final String key) {
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle bundle = resourceBundles
                .computeIfAbsent(locale, k -> ResourceBundle.getBundle(BUNDLE_NAME, locale));
        String message = null;
        try {
            message = bundle.getString(key);
        } catch (final Exception e) {
            LOGGER.error(String.format("no message for bundle %s and ket %s", bundleSourcePath, key), e);
            message = key;
        }
        return message == null ? key : message;
    }

    /**
     *
     * @param activity
     * @return
     */
    public boolean hasActivity(String activity) {
        return Optional.ofNullable(selectedTreeNode)
                .filter(node -> (node.getData()) instanceof ITreeNode)
                .map(node -> (ITreeNode) node.getData())
                .filter(treenode -> (treenode.getData()) instanceof IFlatNode)
                .map(treenode -> (IFlatNode) treenode.getData())
                .map(flatNode -> flatNode.getActivitiesForAllGroups())
                .filter(a -> Activities.isAnActivity(activity))
                .map(activitiesForAllGroups -> activitiesForAllGroups.get(Activities.convertToActivity(activity)))
                .isPresent();
    }

    /**
     *
     * @param activity
     * @param node
     * @return
     */
    public boolean hasActivityWithoutOwnGroup(Activities activity, ITreeNode node) {
        IFlatNode flatNode = (IFlatNode) node.getData();
        Group ownGroup = flatNode.getOwnGroup();
        boolean present = Optional.ofNullable(flatNode.getActivitiesForAllGroups().get(activity))
                .map(a -> a.keySet())
                .filter(ks
                        -> ks.stream().anyMatch(
                        g -> !g.equals(ownGroup)))
                .isPresent();
        return present;
    }

}
/**
 *
 * @author yahiaoui
 */
