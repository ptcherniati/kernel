package org.inra.ecoinfo.identification.jsf;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UIBeanIdentification.
 */
@ManagedBean(name = "uiAssociateGroups")
@ViewScoped
public class UIBeanAssociateGroup implements Serializable {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.identification.jsf.associategroups";
    private static final String AVAILABLES_GROUPS = "PROPERTY_CST_AVAILABLES_GROUPS";
    private static final String STARTINGS_GROUPS = "PROPERTY_CST_STARTINGS_GROUPS";
    private static final String TREEDATASET = "PROPERTY_CST_TREEDATASET";
    private static final String TREEREFDATA = "PROPERTY_CST_TREEREFDATA";
    private static final String MODIFY_GROUPS = "PROPERTY_CST_MODIFY_GROUPS";
    private static final Logger LOGGER = LoggerFactory.getLogger(UIBeanAssociateGroup.class);
    /**
     * The Constant BUNDLE_NAME.
     */
    private final Map<Locale, ResourceBundle> resourceBundles = new HashMap<>();

    private DualListModel<Group> groups;
    List<WhichTree> whichTrees = new LinkedList();
    private Map<WhichTree, DualListModel> userGroupsList = new HashMap();

    private IUser selectedUser;

    /**
     *
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;

    /**
     *
     * @param gu
     */
    public void addGroup(GroupUserVO gu) {
        policyManager.addGroupToUser(gu.getUser(), gu.getGroup());
        DualListModel dualListModel = userGroupsList
                .computeIfAbsent(gu.getGroup().getWhichTree(), k -> new DualListModel());
        dualListModel.getSource().remove(gu);
        dualListModel.getTarget().add(gu);

    }

    ;
    private void removeGroup(GroupUserVO gu) {
        policyManager.removeGroupOfUser(gu.getUser(), gu.getGroup());
        DualListModel dualListModel = userGroupsList
                .computeIfAbsent(gu.getGroup().getWhichTree(), k -> new DualListModel());
        dualListModel.getSource().add(gu);
        dualListModel.getTarget().remove(gu);

    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @return
     */
    public List<WhichTree> getWhichTrees() {
        return whichTrees;
    }

    /**
     *
     * @param whichTrees
     */
    public void setWhichTrees(List<WhichTree> whichTrees) {
        this.whichTrees = whichTrees;
    }

    /**
     *
     * @param whichTree
     * @param selectedUser
     */
    public void init(String whichTree, IUser selectedUser) {
        if (selectedUser.equals(this.selectedUser) && !userGroupsList.isEmpty()) {
            return;
        }
        resetGroups();
        this.selectedUser = selectedUser;
        setWhichTrees(whichTree);
        addGroups(selectedUser);
    }

    /**
     *
     */
    public void resetGroups() {
        userGroupsList.clear();
    }

    /**
     *
     * @return
     */
    public DualListModel<Group> getGroups() {
        return groups;
    }

    /**
     *
     * @param groups
     */
    public void setGroups(DualListModel<Group> groups) {
        this.groups = groups;
    }

    /**
     *
     * @param whichTrees
     * @return
     */
    public List<WhichTree> setWhichTrees(String whichTrees) {
        this.setWhichTrees(Arrays.stream(whichTrees.split(","))
                .map(w -> WhichTree.convertToWhichTree(w))
                .collect(Collectors.toList()));
        return this.whichTrees;
    }

    private void addGroups(IUser user) {
        Map<Group, Boolean> allPossiblesGroupForUser = policyManager.getAllPossiblesGroupUser(user);
        for (Map.Entry<Group, Boolean> groupEntry : allPossiblesGroupForUser.entrySet()) {
            Group group = groupEntry.getKey();
            Boolean isAssociate = groupEntry.getValue();
            DualListModel dualListModel = userGroupsList
                    .computeIfAbsent(group.getWhichTree(), k -> new DualListModel());
            if (isAssociate || group.getGroupName().equals("public")) {
                dualListModel.getTarget().add(new GroupUserVO(user, group, true));
            } else {
                dualListModel.getSource().add(new GroupUserVO(user, group, false));
            }
        }
    }

    /**
     *
     * @param whichTree
     * @return
     * @throws BusinessException
     */
    public DualListModel groupsList(WhichTree whichTree) throws BusinessException {
        return userGroupsList
                .getOrDefault(whichTree, new DualListModel(new LinkedList(), new LinkedList()));
    }

    /**
     *
     * @param d
     */
    public void setGroupsList(DualListModel d) {

    }

    /**
     *
     * @param event
     */
    public void onTabChange(TabChangeEvent event) {
        //setWhichTree((WhichTree) event.getData());
    }

    /**
     *
     * @param event
     */
    public void onTransfer(TransferEvent event) {
        List<GroupUserVO> items = (List<GroupUserVO>) event.getItems();
        boolean add = event.isAdd();
        items.forEach(add
                ? gu -> addGroup(gu)
                : gu -> removeGroup(gu));
    }

    /**
     *
     * @param whichTree
     * @return
     */
    public String availables(String whichTree) {
        return String.format(getMessage(BUNDLE_NAME, AVAILABLES_GROUPS), getFullWichTtreName(whichTree));
    }

    /**
     *
     * @param whichTree
     * @return
     */
    public String modifyGroups(String whichTree) {
        return String.format(getMessage(BUNDLE_NAME, MODIFY_GROUPS), getFullWichTtreName(whichTree));
    }

    /**
     *
     * @param whichTree
     * @return
     */
    public String starting(String whichTree) {
        return selectedUser == null ? "" : String.format(getMessage(BUNDLE_NAME, STARTINGS_GROUPS), selectedUser.getNom(), getFullWichTtreName(whichTree));
    }

    /**
     *
     * @param whichTree
     * @return
     */
    public String getFullWichTtreName(String whichTree) {
        return getMessage(BUNDLE_NAME, "TREEDATASET".equals(whichTree) ? TREEDATASET : TREEREFDATA);
    }

    private String getMessage(final String bundleSourcePath, final String key) {
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle bundle = resourceBundles
                .computeIfAbsent(locale, k -> ResourceBundle.getBundle(BUNDLE_NAME, locale));
        String message = null;
        try {
            message = bundle.getString(key);
        } catch (final Exception e) {
            LOGGER.error(String.format("no message for bundle %s and ket %s", bundleSourcePath, key), e);
            message = key;
        }
        return message == null ? key : message;
    }

    /**
     *
     * @return
     */
    public IUser getSelectedUser() {
        return selectedUser;
    }
}
