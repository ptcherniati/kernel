/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.identification.jsf;

import java.util.List;
import java.util.Optional;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

/**
 *
 * @author tcherniatinsky
 */
@FacesConverter(value = "userGroupVOConverter")
public class UserGroupVOConverter implements Converter {

    /**
     *
     * @param arg0
     * @param arg1
     * @param arg2
     * @return
     */
    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
        String[] keys = arg2.split(":");
        Optional<GroupUserVO> ret = Optional.empty();
        if (arg1 instanceof PickList) {
            Object dualList = ((PickList) arg1).getValue();
            return getGroupUser(((DualListModel) dualList).getSource(), keys)
                    .orElse(getGroupUser(((DualListModel) dualList).getTarget(), keys)
                            .orElse(null));
        }
        return ret;
    }

    private Optional getGroupUser(List<Object> list, String[] keys) {
        return list.stream()
                .filter(gu -> matchesKeys((GroupUserVO) gu, keys[0], keys[1]))
                .map(k -> (GroupUserVO) k)
                .findFirst();
    }

    private boolean matchesKeys(GroupUserVO gu, String groupName, String login) {
        return gu.getGroupName().equals(groupName) && gu.getUser().getLogin().equals(login);
    }

    /**
     *
     * @param arg0
     * @param arg1
     * @param arg2
     * @return
     */
    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
        String str = "";
        if (arg2 instanceof GroupUserVO) {
            str = ((GroupUserVO) arg2).getKey();
        }
        return str;
    }
}
