/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.identification.jsf;

import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.groups.Group;

/**
 *
 * @author tcherniatinsky
 */
public class GroupUserVO {

    Group group;
    IUser user;
    Boolean isAssociate;

    GroupUserVO(IUser user, Group group, Boolean isAssociate) {
        this.group = group;
        this.user = user;
        this.isAssociate = isAssociate;
    }

    /**
     *
     * @return
     */
    public String getGroupName() {
        return getGroup().getGroupName();
    }

    /**
     *
     * @return
     */
    public Group getGroup() {
        return group;
    }

    /**
     *
     * @param group
     */
    public void setGroup(Group group) {
        this.group = group;
    }

    /**
     *
     * @return
     */
    public IUser getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(IUser user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    public Boolean getIsAssociate() {
        return isAssociate;
    }

    /**
     *
     * @param isAssociate
     */
    public void setIsAssociate(Boolean isAssociate) {
        this.isAssociate = isAssociate;
    }

    /**
     *
     * @return
     */
    public Boolean isDisabled() {
        return getGroupName().equals(user.getLogin())
                || getGroupName().equals("public");
    }

    /**
     *
     * @return
     */
    public String getKey() {
        return group.getGroupName() + ":" + user.getLogin();
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "group=" + group + ", user=" + user + ", isAssociate=" + isAssociate;
    }
}
