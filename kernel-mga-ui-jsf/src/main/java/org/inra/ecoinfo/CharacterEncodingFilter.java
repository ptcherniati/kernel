package org.inra.ecoinfo;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tcherniatinsky
 */
@WebFilter(servletNames = {"Faces Servlet", "Resteasy", "Push Servlet"}, asyncSupported = true, value = "/*", initParams
        = @WebInitParam(name = CharacterEncodingFilter.ENCODING_INIT_PARAM,
                value = CharacterEncodingFilter.DEFAULT_ENCODING))
public class CharacterEncodingFilter implements Filter {
    public static final Logger LOGGER = LoggerFactory.getLogger(CharacterEncodingFilter.class);
    public static final String LOGGER_ERROR = "Method %s -> %s";

    /**
     *
     */
    public static final String ENCODING_INIT_PARAM = "encoding";

    /**
     *
     */
    public static final String DEFAULT_ENCODING = "UTF-8";

    private String encoding = DEFAULT_ENCODING;

    /**
     *
     * @param request
     * @param response
     * @param chain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doFilter(ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws ServletException, IOException {
        request.setCharacterEncoding(encoding);
        response.setCharacterEncoding(encoding);
        try {
            chain.doFilter(request, response);
        } catch (Exception e) {
            //LOGGER.error(String.format(LOGGER_ERROR, "doFilter", e.getMessage()), e);
        }
    }

    /**
     *
     */
    @Override
    public void destroy() {
        //nothing to do
    }

    /**
     *
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig config) throws ServletException {
        encoding = config.getInitParameter(ENCODING_INIT_PARAM);
    }
}
