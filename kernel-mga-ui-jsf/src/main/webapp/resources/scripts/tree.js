

var UNDERSCORE = "_";
var COLON = ":";
var INPUT = "input";
var INDETERMINATE_DATE_STRING = "**/**/****";

var INDETERMINATE_DATE = "01/01/0001"
var BEG_INPUT = "beg_input";
var END_INPUT = "end_input";

var ID_NODE = "idnode";

var BEG = "beg";
var END = "end";

var UNDERSCORE_BEG = UNDERSCORE + BEG;
var UNDERSCORE_END = UNDERSCORE + END;

var TO_UPDATE_CHEKS = "toUpdateCheks";
var SPLITTER = "_._";
var SPLITTER_VALUE = "_v_";

var FORMAT_ID_OF_NODE = "format_id_of_node";

var UNDERSCORE_BEG_INPUT = UNDERSCORE + BEG_INPUT;
var UNDERSCORE_END_INPUT = UNDERSCORE + END_INPUT;

var UNDERSCORE_INPUT = UNDERSCORE + INPUT;

var ID_INTERMEDIATE_STATE = "idCheckIntermediateState";
var TEMPORARY_DATES = "temporaryDates";
var WHICH_DATES = "whichDates";

/* ALL existing Activities */
var activities = new Array("synthese",
        "administration",
        "publication",
        "depot",
        "suppression",
        "extraction",
        "edition",
        "telechargement",
        "associate");

$(document).ready(function () { });

document.onreadystatechange = function (e) {};
window.onload = function () {};
window.onunload = function () {};

window.onbeforeunload = function (e) {
    rcClearTrees();
};

function diplayCheckBoxs(treeID, codeConfig) {
}


function cleanAllCheckBoxs(idTree) {}



function treate(boolCheckID, state) {

    var stateBoolCheckBox = state === true ? 1 : 0;

    var map = {};

    var intermediateStates = "";

    var lastSplit = boolCheckID.lastIndexOf(":");
    var idRef = boolCheckID.substring(lastSplit,
            boolCheckID.length).replace(UNDERSCORE_INPUT, "");

    var activity = idRef.replace(COLON, "");

    var idParent = getParentID(boolCheckID, idRef + UNDERSCORE_INPUT);
    var regexSameLevelIds = getRegexSameLevelIds(idParent, idRef + UNDERSCORE_INPUT);
    var regexChildsIds = getRegexChildsIds(boolCheckID, idRef + UNDERSCORE_INPUT);

    // Level i+1 of Parent
    var lengthUnderParent = idParent.toString().split(UNDERSCORE).length;

    var indeterminate = false;
    var checked = false;
    var unchecked = false;
    var conflict = false;

    //var $updateBool = $(':input[id="'+boolCheckID+'"]');

    if (stateBoolCheckBox === 0) {

        unchekBoolean(boolCheckID);

        var dates = [null, null];

        if (!map.hasOwnProperty(activity)) {
            map[activity] = {};
        }

        var idNodeJsf = boolCheckID.replace(activity + UNDERSCORE_INPUT, ID_NODE);
        var valueIdNode = $('[id="' + idNodeJsf + '"]').prop('value');

        map[activity][valueIdNode] = {};
        map[activity][valueIdNode]['date'] = dates;
        map[activity][valueIdNode]['state'] = 0;

        intermediateStates = intermediateStates
                + boolCheckID
                + SPLITTER_VALUE
                + 0
                + SPLITTER;

        $('input[id*="' + regexChildsIds + '"] ').each(function () {

            var id = $(this).prop('id');

            if (id.toString().endsWith(idRef + UNDERSCORE_INPUT)) {

                var actualStateId = getStateBoolean(id);

                if (actualStateId !== 0) {

                    var idNodeJsf = id.replace(activity + UNDERSCORE_INPUT, ID_NODE);
                    var valueIdNode = $('[id="' + idNodeJsf + '"]').prop('value');

                    map[activity][valueIdNode] = {};
                    map[activity][valueIdNode]['date'] = dates;
                    map[activity][valueIdNode]['state'] = 0;

                    intermediateStates = intermediateStates
                            + id
                            + SPLITTER_VALUE
                            + 0
                            + SPLITTER;
                    unchekBoolean(id);
                }
            }
        });
    } else
    {
        if (stateBoolCheckBox === 1) {

            chekBoolean(boolCheckID);

            if (!map.hasOwnProperty(activity)) {
                map[activity] = {};
            }

            var idNodeJsf = boolCheckID.replace(activity + UNDERSCORE_INPUT, ID_NODE);
            var valueIdNode = $('[id="' + idNodeJsf + '"]').prop('value');

            var dateBegString = boolCheckID.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG);
            var dateEndString = boolCheckID.replace(UNDERSCORE_INPUT, UNDERSCORE_END);

            var dateBeg = getDateInputText(dateBegString);
            var dateEnd = getDateInputText(dateEndString);

            if (dateBeg.length === 0 || dateBeg === INDETERMINATE_DATE_STRING) {
                dateBeg = null
            }
            ;
            if (dateEnd.length === 0 || dateEnd === INDETERMINATE_DATE_STRING) {
                dateEnd = null
            }
            ;

            var dates = [dateBeg, dateEnd];

            map[activity][valueIdNode] = {};
            map[activity][valueIdNode]['date'] = dates;
            map[activity][valueIdNode]['state'] = 1;

            intermediateStates = intermediateStates +
                    boolCheckID + SPLITTER_VALUE +
                    1 + getIntermediateDate(boolCheckID) +
                    SPLITTER;

            $('input[id*="' + regexChildsIds + '"] ').each(function () {

                var id = $(this).prop('id');

                if (id.toString().endsWith(idRef + UNDERSCORE_INPUT)) {

                    if (getStateBoolean(id) !== stateBoolCheckBox) {

                        var idNodeJsf = id.replace(activity + UNDERSCORE_INPUT, ID_NODE);
                        var valueIdNode = $('[id="' + idNodeJsf + '"]').prop('value');

                        var dateBegString = id.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG);
                        var dateEndString = id.replace(UNDERSCORE_INPUT, UNDERSCORE_END);

                        var dateBeg = getDateInputText(dateBegString);
                        var dateEnd = getDateInputText(dateEndString);

                        if (dateBeg.length === 0 || dateBeg === INDETERMINATE_DATE_STRING) {
                            dateBeg = null
                        }
                        ;
                        if (dateEnd.length === 0 || dateEnd === INDETERMINATE_DATE_STRING) {
                            dateEnd = null
                        }
                        ;

                        var dates = [dateBeg, dateEnd];

                        map[activity][valueIdNode] = {};
                        map[activity][valueIdNode]['date'] = dates;
                        map[activity][valueIdNode]['state'] = 1;

                        intermediateStates = intermediateStates +
                                id + SPLITTER_VALUE +
                                1 + getIntermediateDate(boolCheckID)
                                + SPLITTER;
                        chekBoolean(id);
                    }
                }
            });
        }
    }

    $('input[id*="' + regexSameLevelIds + '"] ').each(function () {
        var idChild = $(this).prop('id');
        var lengthUnderChild = idChild.toString().split(UNDERSCORE).length;
        if (lengthUnderParent === lengthUnderChild - 1)
        {
            if (!idChild.toString().endsWith(idRef + UNDERSCORE_INPUT))
                return;
            var state = getStateBoolean(idChild);
            if (state === 0)
                unchecked = true;
            else if (state === 1)
                checked = true;
            else if (state === 2)
                indeterminate = true;

            if ((checked && unchecked) || (checked && indeterminate) ||
                    (unchecked && indeterminate))
            {
                conflict = true;
                return     false;
            }
        }
    });

    var beforeStateofParent = getStateBoolean(idParent);

    if (conflict) {
        setIndeterminateBoolean(idParent);
    } else if (checked) {
        chekBoolean(idParent);
    } else if (unchecked) {
        unchekBoolean(idParent);

    } else if (indeterminate) {
        setIndeterminateBoolean(idParent);
    }

    var afterStateOfParent = getStateBoolean(idParent);

    if (beforeStateofParent !== afterStateOfParent) {

        var idNodeJsf = idParent.replace(activity + UNDERSCORE_INPUT, ID_NODE);
        var valueIdNodeParent = $('[id="' + idNodeJsf + '"]').prop('value');

        var dateBegString = boolCheckID.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG);
        var dateEndString = boolCheckID.replace(UNDERSCORE_INPUT, UNDERSCORE_END);

        var dateBeg = getDateInputText(dateBegString);
        var dateEnd = getDateInputText(dateEndString);

        if (dateBeg.length === 0 || dateBeg === INDETERMINATE_DATE_STRING) {
            dateBeg = null
        }
        ;
        if (dateEnd.length === 0 || dateEnd === INDETERMINATE_DATE_STRING) {
            dateEnd = null
        }
        ;

        var dates = [dateBeg, dateEnd];

        map[activity][valueIdNodeParent] = {};
        map[activity][valueIdNodeParent]['date'] = dates;
        map[activity][valueIdNodeParent]['state'] = afterStateOfParent;

        intermediateStates = intermediateStates +
                idParent + SPLITTER_VALUE +
                afterStateOfParent + SPLITTER;
    }

    if (idParent !== idRef + UNDERSCORE_INPUT)
        intermediateStates = treateParentBox(idParent, intermediateStates, map, activity);

    if (intermediateStates.length > 0) {

        var arrayOfTmpStates = [];
        var stateCheck;
        stateCheck = new Object();
        stateCheck.name = ID_INTERMEDIATE_STATE;
        stateCheck.value = JSON.stringify(map);
        arrayOfTmpStates.push(stateCheck);
        rcsaveIntermediateState(arrayOfTmpStates);
    }
}

function treateParentBox(boolCheckID, intermediateStates, map, activity) {

    var lastSplit = boolCheckID.lastIndexOf(":");
    var idRef = boolCheckID.substring(lastSplit,
            boolCheckID.length).replace(UNDERSCORE_INPUT, "");
    var idParent = getParentID(boolCheckID, idRef + UNDERSCORE_INPUT);
    var regexSameLevelIds = getRegexSameLevelIds(idParent, idRef + UNDERSCORE_INPUT);

    // Level i+1 par from parent
    var lengthUnderParent = idParent.toString().split(UNDERSCORE).length;

    var indeterminate = false;
    var checked = false;
    var unchecked = false;
    var conflict = false;

    $('input[id*="' + regexSameLevelIds + '"] ').each(function () {
        var idChild = $(this).prop('id');
        var lengthUnderChild = idChild.toString().split(UNDERSCORE).length;
        if (lengthUnderParent === lengthUnderChild - 1)
        {
            if (!idChild.toString().endsWith(idRef + UNDERSCORE_INPUT))
                return;
            var state = getStateBoolean(idChild);
            if (state === 0)
                unchecked = true;
            else if (state === 1)
                checked = true;
            else if (state === 2)
                indeterminate = true;

            if ((checked && unchecked) || (checked && indeterminate) ||
                    (unchecked && indeterminate))
            {
                conflict = true;
                return     false;
            }
        }
    });

    var beforeStateOfParent = getStateBoolean(idParent);

    if (conflict) {
        setIndeterminateBoolean(idParent);
    } else if (checked) {
        chekBoolean(idParent);
    } else if (unchecked) {
        unchekBoolean(idParent);

    } else if (indeterminate) {
        setIndeterminateBoolean(idParent);
    }

    var afterStateOfParent = getStateBoolean(idParent);

    if (beforeStateOfParent !== afterStateOfParent) {

        var idNodeJsf = idParent.replace(activity + UNDERSCORE_INPUT, ID_NODE);
        var valueIdNode = $('[id="' + idNodeJsf + '"]').prop('value');

        var dateBegString = idParent.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG);
        var dateEndString = idParent.replace(UNDERSCORE_INPUT, UNDERSCORE_END);

        var dateBeg = getDateInputText(dateBegString);
        var dateEnd = getDateInputText(dateEndString);

        if (dateBeg.length === 0 || dateBeg === INDETERMINATE_DATE_STRING) {
            dateBeg = null
        }
        ;
        if (dateEnd.length === 0 || dateEnd === INDETERMINATE_DATE_STRING) {
            dateEnd = null
        }
        ;

        var dates = [dateBeg, dateEnd];

        map[activity][valueIdNode] = {};
        map[activity][valueIdNode]['date'] = dates;
        map[activity][valueIdNode]['state'] = afterStateOfParent;


        intermediateStates = intermediateStates +
                idParent + SPLITTER_VALUE
                + afterStateOfParent + SPLITTER;
    }

    if (idParent !== idRef + UNDERSCORE_INPUT) {
        treateParentBox(idParent, intermediateStates, map, activity);
    }

    return intermediateStates;
}

// Calcul parent ID
function getParentID(idChild, idRef) {
    var subIdClean = idChild.toString().replace(idRef, "");
    var lastUndersc = subIdClean.lastIndexOf(UNDERSCORE);
    var subParentId = subIdClean.substring(0, lastUndersc);
    return subParentId + idRef;
}

function getRegexSameLevelIds(parentID, idRef) {
    return parentID.toString().replace(idRef, "") + UNDERSCORE;
}
function getRegexChildsIds(chekID, idRef) {
    return chekID.toString().replace(idRef, "") + UNDERSCORE;
}

// Set STATE using input text
function setStateBoolAndDateInputText(idBoolean, state, dateBeg, dateEnd) {

    if (state === 1) {

        chekBoolean(idBoolean + UNDERSCORE_INPUT);

        if (dateBeg === INDETERMINATE_DATE_STRING)
            setDateInputText(idBoolean + UNDERSCORE_BEG, dateBeg);
        if (dateEnd === INDETERMINATE_DATE_STRING)
            setDateInputText(idBoolean + UNDERSCORE_END, dateEnd);

        // IF NOT UPDATED DATE ON UI CALENDER -- THEN DO IT
        if (dateBeg !== INDETERMINATE_DATE_STRING)
        {
            var dateBegIDString = idBoolean + UNDERSCORE_BEG;
            var dateBegOnUI = getDateInputText(dateBegIDString);
            if (dateBegOnUI !== dateBeg) {
                if (dateBeg !== 'null') {
                    setDateInputText(idBoolean + UNDERSCORE_BEG, dateBeg);
                }
            }
        }
        // IF NOT UPDATED DATE ON UI CALENDER -- THEN DO IT
        if (dateEnd !== INDETERMINATE_DATE_STRING)
        {
            var dateEndIDString = idBoolean + UNDERSCORE_END;
            var dateEndOnUI = getDateInputText(dateEndIDString);
            if (dateEndOnUI !== dateEnd) {
                if (dateEnd !== 'null')
                    setDateInputText(idBoolean + UNDERSCORE_END, dateEnd);
            }
        }
    } else if (state === 2) {

        if (getStateBoolean(idBoolean + UNDERSCORE_INPUT) !== 2)
            setIndeterminateBoolean(idBoolean + UNDERSCORE_INPUT);

        if (dateBeg === INDETERMINATE_DATE_STRING)
            setDateInputText(idBoolean + UNDERSCORE_BEG, dateBeg);

        if (dateEnd === INDETERMINATE_DATE_STRING)
            setDateInputText(idBoolean + UNDERSCORE_END, dateEnd);

    } else if (state === 0) {

        unchekBoolean(idBoolean + UNDERSCORE_INPUT);

        if (dateBeg === INDETERMINATE_DATE_STRING)
            setDateInputText(idBoolean + UNDERSCORE_BEG, dateBeg);

        if (dateEnd === INDETERMINATE_DATE_STRING)
            setDateInputText(idBoolean + UNDERSCORE_END, dateEnd);

    }
}


function setStateBoolAndDateInputTextUsingParam(params) {

    var paramArray = params.split("_&_");
    var loop;
    for (loop = 0; loop < paramArray.length; loop++) {

        var param = paramArray[loop];
        var idNode = param.split("[")[0];
        var activitiesParts = param.split("[")[1].replace("]", "");
        var activitiesPart = activitiesParts.split(";");

        var subLoop;
        for (subLoop = 0; subLoop < activitiesPart.length; subLoop++) {

            var activity = activitiesPart[subLoop].split("(")[0];
            var states = activitiesPart[subLoop].split("(")[1].replace(")", "");

            var realState = parseInt(states.split(",")[0].trim());
            var dateBeg = states.split(",")[1].trim();
            var dateEnd = states.split(",")[2].trim();

            setStateBoolAndDateInputText(
                    idNode.replace("PRIVILEGE_INPUT", activity),
                    realState, dateBeg, dateEnd
                    );
        }
    }
}

// Set Indeterminate STATE
function setStateBool(idBoolean, state) {
    if (state === 1) {
        chekBoolean(idBoolean.replace(UNDERSCORE_INPUT, ""));
    } else if (state === 2) {
        setIndeterminateBoolean(idBoolean.replace(UNDERSCORE_INPUT, ""));
    }
}


function updateTmpStateCheckboxes(idCheckBox) {

}

// Set Indeterminate STATE
function setIndeterminateBoolean(idBoolean) {
    var $bol = $('input[id="' + idBoolean + '"] ');
    $($bol).prop('checked', true);

    setIndeterminateBooleanDiv(idBoolean.replace(UNDERSCORE_INPUT, ""));
    disableDateInputText(idBoolean);
    disableDateInputText(idBoolean);
    setDateInputText(idBoolean.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG), INDETERMINATE_DATE_STRING);
    setDateInputText(idBoolean.replace(UNDERSCORE_INPUT, UNDERSCORE_END), INDETERMINATE_DATE_STRING);
}

// Set Indeterminate STATE DIV
function setIndeterminateBooleanDiv(idBoolean) {
    var $i = $('div[id="' + idBoolean + '"] > div');

    $($i).children('span').removeClass(('ui-icon ui-icon-check'));
    $($i).addClass("ui-state-active");
    $($i).children('span').removeClass(('ui-icon-blank'));
    $($i).children('span').addClass(('ui-icon ui-icon-minusthick'));

}

// Set Check STATE
function chekBoolean(idBoolean) {
    var $bol = $('input[id="' + idBoolean + '"] ');
    $($bol).prop('checked', true);
    checkBooleanDiv(idBoolean.replace(UNDERSCORE_INPUT, ""));
    //enableCalendar(idBoolean);
    enableDateInputText(idBoolean);
    if (getDateInputText(idBoolean.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG), "")
            === INDETERMINATE_DATE)
        setDateInputText(idBoolean.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG), "");
    if (getDateInputText(idBoolean.replace(UNDERSCORE_INPUT, UNDERSCORE_END), "")
            === INDETERMINATE_DATE)
        setDateInputText(idBoolean.replace(UNDERSCORE_INPUT, UNDERSCORE_END), "");
}

// Set Check STATE DIV
function checkBooleanDiv(idBoolean) {
    var $i = $('div[id="' + idBoolean + '"] > div');
    $($i).children('span').remove(('ui-icon ui-icon-minusthick'));
    $($i).addClass("ui-state-active");
    $($i).children('span').removeClass(('ui-icon-blank'));
    $($i).children('span').addClass(('ui-icon ui-icon-check'));
}

// Set UnCheck STATE
function unchekBoolean(idBoolean) {
    var $bol = $('input[id="' + idBoolean + '"] ');
    $($bol).prop('checked', false);
    uncheckBooleanDiv(idBoolean.replace(UNDERSCORE_INPUT, ""));
    disableDateInputText(idBoolean);
    setDateInputText(idBoolean.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG), "");
    setDateInputText(idBoolean.replace(UNDERSCORE_INPUT, UNDERSCORE_END), "");

}
// Set UNCheck STATE DIV
function uncheckBooleanDiv(idBoolean) {
    var $i = $('div[id="' + idBoolean + '"] > div');
    $($i).removeClass("ui-state-active");
    $($i).children('span').removeClass(('ui-icon ui-icon-check'));
    $($i).children('span').removeClass(('ui-icon ui-icon-minusthick'));
}

function getStateBoolean(idBoolean) {
    var $i = $('div[id="' + idBoolean.replace(UNDERSCORE_INPUT, "") + '"] > div');
    if ($($i).children('span').hasClass(('ui-icon-check')))
        return 1;
    else if ($($i).children('span').hasClass(('ui-icon-minusthick')))
        return 2;
    else
        return 0;
}

function getRowKey(idBoolean) {
    return idBoolean.toString().split(":")[idBoolean.split(":").length - 2 ];
}


function updateCheckBoxes(xhr, status, args) {
}



function  updateInputText(idInputText) {
    var map = {};
    var ref = idInputText.split(":")[idInputText.split(":").length - 1 ];
    var idParent = getParentID(idInputText, ":" + ref);

    var currentDateInputText = getDateInputText(idInputText);

    var regxChildrens = idInputText.replace(':' + ref, "_");
    //var regexSameLevelIds  = getRegexSameLevelIds(idParent, ":"+ref);

    $('input[id*="' + regxChildrens + '"] ').each(function () {
        var id = $(this).prop('id');

        if (id.endsWith(ref)) {
            if (getStateBoolean(id.replace(UNDERSCORE_BEG, "").replace(UNDERSCORE_END, "") + UNDERSCORE_INPUT) !== 0)
                setDateInputText(id, currentDateInputText);
        }
    });


    var activity = ref.split(UNDERSCORE)[0];

    var whichDate = ref.split(UNDERSCORE)[1];

    if (!map.hasOwnProperty(activity)) {
        map[activity] = [];
    }

    var idNodeJsf = idInputText.replace(ref, ID_NODE);
    var valueIdNode = $('[id="' + idNodeJsf + '"]').prop('value');

    var arrayValues = [];
    arrayValues.push(valueIdNode);
    arrayValues.push(currentDateInputText);

    map[activity].push(arrayValues);


    if (idParent !== ":" + ref) {
        updateDateParentState(idInputText, map);
    }

    var arrayOfTmpDates = [];
    var tmpDates = new Object();
    var whichDates = new Object();

    tmpDates.name = TEMPORARY_DATES;
    tmpDates.value = JSON.stringify(map);

    whichDates.name = WHICH_DATES;
    whichDates.value = whichDate;

    arrayOfTmpDates.push(tmpDates);
    arrayOfTmpDates.push(whichDates);

    rcSaveTemporaryDates(arrayOfTmpDates);
}


function updateDateParentState(idInputText, map) {

    var ref = idInputText.split(":")[idInputText.split(COLON).length - 1 ];

    var idParent = getParentID(idInputText, COLON + ref);

    if (idParent === COLON + ref)
        return;

    var regexSameLevelIds = getRegexSameLevelIds(idParent, COLON + ref);

    var currentDateInputText = getDateInputText(idInputText);

    var indeterminate = false;

    var lengthUnderParent = idParent.toString().split(UNDERSCORE).length;

    var activity = ref.split(UNDERSCORE)[0];

    $('[id*="' + regexSameLevelIds + '"] ').each(function () {

        var idChild = $(this).prop('id');

        var lengthUnderChild = idChild.toString().split(UNDERSCORE).length;

        if (lengthUnderParent === lengthUnderChild - 1)
        {

            if (!idChild.toString().endsWith(ref))
                return;

            var nearInputDate = getDateInputText(idChild);

            if (nearInputDate === INDETERMINATE_DATE_STRING) {
                indeterminate = true;
                return false;
            }
            if (nearInputDate !== currentDateInputText) {
                indeterminate = true;
                return false;
            }
        }
    });

    var idParentNodeJsf = idParent.replace(ref, ID_NODE);
    var valueIdParentNode = $('[id="' + idParentNodeJsf + '"]').prop('value');

    var arrayValues = [];
    arrayValues.push(valueIdParentNode);

    if (indeterminate) {
        arrayValues.push(INDETERMINATE_DATE);
        map[activity].push(arrayValues);
        setIndeterminateDateInputText(idParent);
    } else {
        arrayValues.push(currentDateInputText);
        map[activity].push(arrayValues);
        setDateInputText(idParent, currentDateInputText);
    }
    if (idParent !== ":" + ref)
        updateDateParentState(idParent, map);

}



function getDateInputText(idInputText) {
    if ($('[id="' + idInputText + '"]').length)
        return $('[id="' + idInputText + '"]').val();
    return "";
}

function setDateInputText(idInputText, date) {
    var $calDate = $('input[id="' + idInputText + '"] ');
    $($calDate).val(date);
}

function enableDateInputText(idInputText) {
    $('[id="' + idInputText.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG) + '"]').attr("disabled", false);
    $('[id="' + idInputText.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG) + '"]').removeClass("ui-state-disabled");
    $('[id="' + idInputText.replace(UNDERSCORE_INPUT, UNDERSCORE_END) + '"]').attr("disabled", false);
    $('[id="' + idInputText.replace(UNDERSCORE_INPUT, UNDERSCORE_END) + '"]').removeClass("ui-state-disabled");
}

function disableDateInputText(idInputText) {
    $('[id="' + idInputText.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG) + '"]').attr("disabled", true);
    $('[id="' + idInputText.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG) + '"]').addClass("ui-state-disabled");
    $('[id="' + idInputText.replace(UNDERSCORE_INPUT, UNDERSCORE_END) + '"]').attr("disabled", true);
    $('[id="' + idInputText.replace(UNDERSCORE_INPUT, UNDERSCORE_END) + '"]').addClass("ui-state-disabled");
}


function setIndeterminateDateInputText(idInputText) {
    var $calDate = $('input[id="' + idInputText + '"] ');
    $($calDate).prop('value', INDETERMINATE_DATE_STRING);
}


function getIntermediateDate(idInputText) {

    var datePriv = "";
    var dateBegString = idInputText.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG);
    var dateEndString = idInputText.replace(UNDERSCORE_INPUT, UNDERSCORE_END);

    var dateBeg = getDateInputText(dateBegString);
    var dateEnd = getDateInputText(dateEndString);

    if (dateBeg.length > 0 & dateEnd.length > 0)
        datePriv = "{" + dateBeg + "_" + dateEnd + "}";

    else if (dateBeg.length > 0 & dateEnd.length === 0)
        datePriv = "{" + dateBeg + "_}";

    else if (dateBeg.length === 0 & dateEnd.length > 0)
        datePriv = "{_" + dateEnd + "}";

    return datePriv;
}


/*
 * Calendar manager Functions
 * Calendar manager Functions
 */

// Set STATE Using Calendar
function setStateBoolAndDateCalendar(idBoolean, state, dateBeg, dateEnd) {

    if (state === 1) {
        if (getStateBoolean(idBoolean) !== 1) {
            chekBoolean(idBoolean + UNDERSCORE_INPUT);
        }

        if (dateBeg === INDETERMINATE_DATE_STRING)
            setDateCalender(idBoolean + UNDERSCORE_BEG_INPUT, dateBeg);
        if (dateEnd === INDETERMINATE_DATE_STRING)
            setDateCalender(idBoolean + UNDERSCORE_END_INPUT, dateEnd);

        // IF NOT UPDATED DATE ON UI CALENDER -- THEN DO IT
        if (dateBeg !== INDETERMINATE_DATE_STRING)
        {
            var dateBegIDString = idBoolean + UNDERSCORE_BEG_INPUT;
            var dateBegOnUI = getDateCalender(dateBegIDString);
            if (dateBegOnUI !== dateBeg) {
                if (dateBeg !== 'null') {
                    setDateCalender(idBoolean + UNDERSCORE_BEG_INPUT, dateBeg);
                }
            }
        }
        // IF NOT UPDATED DATE ON UI CALENDER -- THEN DO IT
        if (dateEnd !== INDETERMINATE_DATE_STRING)
        {
            var dateEndIDString = idBoolean + UNDERSCORE_END_INPUT;
            var dateEndOnUI = getDateCalender(dateEndIDString);
            if (dateEndOnUI !== dateEnd) {
                if (dateEnd !== 'null')
                    setDateCalender(idBoolean + UNDERSCORE_END_INPUT, dateEnd);
            }
        }
    } else if (state === 2) {

        if (getStateBoolean(idBoolean) !== 2)
            setIndeterminateBoolean(idBoolean + UNDERSCORE_INPUT);

        if (dateBeg === INDETERMINATE_DATE_STRING)
            setDateCalender(idBoolean + UNDERSCORE_BEG_INPUT, dateBeg);

        if (dateEnd === INDETERMINATE_DATE_STRING)
            setDateCalender(idBoolean + UNDERSCORE_END_INPUT, dateEnd);

    }
}

function setStateBoolAndDateCalendarUsingParam(params) {

    var paramArray = params.split("_&_");
    var loop;
    for (loop = 0; loop < paramArray.length; loop++) {

        var param = paramArray[loop];
        var idNode = param.split("[")[0];
        var activitiesParts = param.split("[")[1].replace("]", "");
        var activitiesPart = activitiesParts.split(";");

        var subLoop;
        for (subLoop = 0; subLoop < activitiesPart.length; subLoop++) {

            var activity = activitiesPart[subLoop].split("(")[0];
            var states = activitiesPart[subLoop].split("(")[1].replace(")", "");

            var realState = parseInt(states.split(",")[0].trim());
            var dateBeg = states.split(",")[1].trim();
            var dateEnd = states.split(",")[2].trim();

            setStateBoolAndDateCalendar(
                    idNode.replace("PRIVILEGE_INPUT", activity),
                    realState, dateBeg, dateEnd
                    );
        }
    }

}

function getInfoCheckBoxIncludeDatesCalendars(operation, array) {

    $('input[id*="' + operation + '"] ').each(function () {

        var id = $(this).prop('id');
        if (!id.contains(BEG_INPUT) & !id.contains(UNDERSCORE_END_INPUT)) {

            var stateCheck = getStateBoolean(id);

            if (stateCheck === 1 || stateCheck === 2)
            {
                var datePriv = "";
                var dateBegString = id.replace(UNDERSCORE_INPUT, UNDERSCORE_BEG_INPUT);
                var dateEndString = id.replace(UNDERSCORE_INPUT, UNDERSCORE_END_INPUT);

                var dateBeg = getDateCalender(dateBegString);
                var dateEnd = getDateCalender(dateEndString);

                if (dateBeg.length > 0 & dateEnd.length > 0)
                    datePriv = "{" + dateBeg + "_" + dateEnd + "}";

                else if (dateBeg.length > 0 & dateEnd.length === 0)
                    datePriv = "{" + dateBeg + "_}";

                else if (dateBeg.length === 0 & dateEnd.length > 0)
                    datePriv = "{_" + dateEnd + "}";

                var state = new Object();
                state.name = id;
                if (datePriv.length > 0)
                    state.value = stateCheck + SPLITTER + datePriv;
                else
                    state.value = stateCheck;

                array.push(state);
            }
        }
    });
}

function enableCalendar(idCalendar) {
    $('[id="' + idCalendar.replace(INPUT, BEG_INPUT) + '"]').attr("disabled", false);
    $('[id="' + idCalendar.replace(INPUT, END_INPUT) + '"]').attr("disabled", false);
}

function disableCalendar(idCalendar) {
    $('[id="' + idCalendar.replace(INPUT, BEG_INPUT) + '"]').attr("disabled", true);
    $('[id="' + idCalendar.replace(INPUT, END_INPUT) + '"]').attr("disabled", true);
}

function setDateCalender(idCalendar, date) {
    var $calDate = $('input[id="' + idCalendar + '"] ');
    $($calDate).prop('value', date);
}

function setIndeterminateDateCalender(idCalendar) {
    var $calDate = $('input[id="' + idCalendar + '"] ');
    $($calDate).prop('value', INDETERMINATE_DATE_STRING);
}

function getDateCalender(idCalendar) {
    if ($('[id="' + idCalendar + '"]').length)
        return $('[id="' + idCalendar + '"]').val();
    return "";
}

function updateCalenders(idCalendar) {

    var ref = idCalendar.split(":")[idCalendar.split(":").length - 1 ];
    var regxChildrens = idCalendar.replace(':' + ref, "_");
    var idParent = getParentID(idCalendar, ":" + ref);
    var regexSameLevelIds = getRegexSameLevelIds(idParent, ":" + ref);

    var currentDateCalendar = getDateCalender(idCalendar + UNDERSCORE_INPUT);

    $('input[id*="' + regxChildrens + '"] ').each(function () {
        var id = $(this).prop('id');
        if (id.endsWith(ref + UNDERSCORE_INPUT)) {

            if (getStateBoolean(id.replace(BEG_INPUT, "").replace(END_INPUT, "") + INPUT) !== 0)
                setDateCalender(id, currentDateCalendar);
        }
    });

    var indeterminate = false;
    var lengthUnderParent = idParent.toString().split(UNDERSCORE).length;

    $('[id*="' + regexSameLevelIds + '"] ').each(function () {

        var idChild = $(this).prop('id');

        var lengthUnderChild = idChild.toString().split(UNDERSCORE).length;

        if (lengthUnderParent === lengthUnderChild - 1) {

            if (!idChild.toString().endsWith(ref))
                return;

            var nearCalendarDate = getDateCalender(idChild + UNDERSCORE_INPUT);

            if (nearCalendarDate !== currentDateCalendar) {
                indeterminate = true;
                return false;
            }
        }
    });

    if (indeterminate)
        setIndeterminateDateCalender(idParent + UNDERSCORE_INPUT);
    else
        setDateCalender(idParent + UNDERSCORE_INPUT, currentDateCalendar);

    if (idParent !== ":" + ref)
        updateCalendersParentState(idParent);
}

function updateCalendersParentState(idCalendar) {

    var ref = idCalendar.split(":")[idCalendar.split(":").length - 1 ];

    var idParent = getParentID(idCalendar, ":" + ref);
    var regexSameLevelIds = getRegexSameLevelIds(idParent, ":" + ref);

    var currentDateCalendar = getDateCalender(idCalendar + UNDERSCORE_INPUT);

    var indeterminate = false;

    var lengthUnderParent = idParent.toString().split(UNDERSCORE).length;

    $('[id*="' + regexSameLevelIds + '"] ').each(function () {

        var idChild = $(this).prop('id');

        var lengthUnderChild = idChild.toString().split(UNDERSCORE).length;

        if (lengthUnderParent === lengthUnderChild - 1)
        {

            if (!idChild.toString().endsWith(ref))
                return;

            var nearCalendarDate = getDateCalender(idChild + UNDERSCORE_INPUT);

            if (nearCalendarDate !== currentDateCalendar) {
                indeterminate = true;
                return false;
            }
        }
    });

    if (indeterminate)
        setIndeterminateDateCalender(idParent + UNDERSCORE_INPUT);
    else
        setDateCalender(idParent + UNDERSCORE_INPUT, currentDateCalendar);

    if (idParent !== ":" + ref)
        updateCalendersParentState(idParent);
}

