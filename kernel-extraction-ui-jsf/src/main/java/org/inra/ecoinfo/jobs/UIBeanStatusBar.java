/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.jobs;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.stream.Collectors;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.extraction.config.IExtractionConfiguration;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiStatusBar")
@ViewScoped
public class UIBeanStatusBar {

    @ManagedProperty(value = "#{policyManager}")
    IPolicyManager policyManager;
    @ManagedProperty(value = "#{extractionConfiguration}")
    IExtractionConfiguration extractionConfiguration;

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @return
     */
    public Map<LocalDateTime, StatusBar> getStatusBars() {
        return StatusBar.getStatusBars().entrySet()
                .stream()
                .filter(
                        e->policyManager.getCurrentUser().getIsRoot() || e.getValue().getUser().equals(policyManager.getCurrentUser())
                )
                .collect(Collectors.toMap(k->k.getKey(), k->k.getValue()));
    }

    /**
     *
     * @param extractionConfiguration
     */
    public void setExtractionConfiguration(IExtractionConfiguration extractionConfiguration) {
        this.extractionConfiguration = extractionConfiguration;
    }

    /**
     *
     * @param extractionType
     * @return
     */
    public String getExtractionType(String extractionType) {
        return extractionConfiguration.getExtractions().stream()
                .filter(e -> 
                        e.getCode().equals(extractionType))
                .map(e -> 
                        e.getInternationalizedDescriptions().getOrDefault(
                policyManager.getCurrentUser().getLanguage(),
                extractionType)
                )
                .findFirst()
                .orElse(extractionType);
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public final String navigate() {
        return "jobs";
    }

}
