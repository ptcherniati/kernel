package org.inra.ecoinfo.extraction.jsf;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Transient;
import org.inra.ecoinfo.extraction.ExtractionType;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.config.IExtractionConfiguration;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.entity.Requete;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.event.TabChangeEvent;

/**
 * The Class UIBeanExtraction.
 */
@ManagedBean(name = "uiExtraction")
@ViewScoped
public class UIBeanExtraction implements Serializable {


    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The extraction types results availables.
     * @link(List<DecoratorExtractionType>).
     */
    @Transient
    private List<DecoratorExtractionType> extractionTypesResultsAvailables;
    /**
     * The configuration @link(Configuration).
     */
    @ManagedProperty(value = "#{extractionConfiguration}")
    protected IExtractionConfiguration configuration;
    /**
     * The current extraction type @link(DecoratorExtractionType).
     */
    @Transient
    protected DecoratorExtractionType currentExtractionType = new DecoratorExtractionType(new ExtractionType());
    /**
     * The extraction manager @link(IExtractionManager).
     */
    @ManagedProperty(value = "#{extractionManager}")
    protected IExtractionManager extractionManager;
    /**
     * The metadata manager @link(IMetadataManager).
     */
    @ManagedProperty(value = "#{metadataManager}")
    protected IMetadataManager metadataManager;
    /**
     * The request selected @link(Requete).
     */
    protected Requete requestSelected;
    /**
     * The security context @link(IPolicyManager).
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;

    /**
     * Instantiates a new uI bean extraction.
     */
    public UIBeanExtraction() {
    }

    /**
     * Delete all requests.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public String deleteAllRequests() throws BusinessException {
        extractionManager.deleteRequestsFromIds(createIdsRequests());
        currentExtractionType.setRequetes(null);
        currentExtractionType.setDisabled(true);
        return null;
    }

    /**
     * Delete request.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public String deleteRequest() throws BusinessException {
        extractionManager.deleteRequestsFromIds(new Long[]{requestSelected.getId()});
        currentExtractionType.getRequetes().remove(requestSelected);
        if (currentExtractionType.getRequetes().isEmpty()) {
            currentExtractionType.setDisabled(true);
            for (final DecoratorExtractionType extractionType : extractionTypesResultsAvailables) {
                if (extractionType.getInner().getCode().equals(currentExtractionType.getInner().getCode())) {
                    extractionTypesResultsAvailables.remove(extractionType);
                    break;
                }
            }
        }
        return null;
    }

    /**
     * Gets the current extraction type.
     *
     * @return the current extraction type
     */
    public DecoratorExtractionType getCurrentExtractionType() {
        return currentExtractionType;
    }

    /**
     * Sets the current extraction type.
     *
     * @param currentDatatype the new current extraction type
     */
    public void setCurrentExtractionType(final DecoratorExtractionType currentDatatype) {
        this.currentExtractionType = currentDatatype;
    }

    /**
     * Gets the extraction types results availables.
     *
     * @return the extraction types results availables
     * @throws BusinessException the business exception
     */
    public List<DecoratorExtractionType> getExtractionTypesResultsAvailables() throws BusinessException {
        if (extractionTypesResultsAvailables == null) {
            final List<ExtractionType> extractionAvailablesInRequests = extractionManager.getExtractiontypesResultsAvailables((Utilisateur) policyManager.getCurrentUser());
            extractionTypesResultsAvailables = new LinkedList<>();
            extractionAvailablesInRequests.stream().map((extractiontypeConfiguration) -> new DecoratorExtractionType(extractiontypeConfiguration)).forEach((decoratorDataType) -> {
                extractionTypesResultsAvailables.add(decoratorDataType);
            });
        }
        return extractionTypesResultsAvailables;
    }

    /**
     * Gets the checks if is extraction types results availables empty.
     *
     * @return the checks if is extraction types results availables empty
     * @throws BusinessException the business exception
     */
    public Boolean getIsExtractionTypesResultsAvailablesEmpty() throws BusinessException {
        getExtractionTypesResultsAvailables();
        return extractionTypesResultsAvailables == null || extractionTypesResultsAvailables.isEmpty();
    }

    /**
     * Gets the time zone.
     *
     * @return the time zone
     */
    public TimeZone getTimeZone() {
        return TimeZone.getDefault();
    }

    /**
     * Load requests extraction type.
     *
     * @param event
     * @throws BusinessException the business exception
     */
    public void loadRequestsExtractionType(TabChangeEvent event) throws BusinessException {
        currentExtractionType = (DecoratorExtractionType) event.getData();
        currentExtractionType.setRequetes(extractionManager.getRequestsResultsByExtractionTypeCode(currentExtractionType.getInner().getCode(), (Utilisateur) policyManager.getCurrentUser()));
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public String navigate() {
        return "extraction";
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final IExtractionConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the extraction manager.
     *
     * @param extractionManager the new extraction manager
     */
    public void setExtractionManager(final IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     * Sets the metadata manager.
     *
     * @param metadataManager the new metadata manager
     */
    public void setMetadataManager(final IMetadataManager metadataManager) {
        this.metadataManager = metadataManager;
    }

    /**
     * Sets the request selected.
     *
     * @param requestSelected the new request selected
     */
    public void setRequestSelected(final Requete requestSelected) {
        this.requestSelected = requestSelected;
    }

    /**
     * Sets the security context.
     *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Creates the ids requests.
     *
     * @return the long[]
     */
    private Long[] createIdsRequests() {
        final Long[] ids = new Long[currentExtractionType.getRequetes().size()];
        Integer i = 0;
        for (final Requete requete : currentExtractionType.getRequetes()) {
            ids[i++] = requete.getId();
        }
        return ids;
    }

    /**
     * Gets the language.
     *
     * @return the language
     */
    private String getLanguage() {
        return FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
    }

    /**
     * Gets the localized extraction type name.
     *
     * @param extractionTypeName the extraction type name
     * @return the localized extraction type name
     * @link(String) the extraction type name
     */
    protected String getLocalizedExtractionTypeName(final String extractionTypeName) {
        for (final Extraction extraction : configuration.getExtractions()) {
            if (extraction.getName().equals(extractionTypeName)) {
                final String localizedExtractionTypeName = extraction.getInternationalizedNames().get(getLanguage());
                return Strings.isNullOrEmpty(localizedExtractionTypeName) ? extractionTypeName : localizedExtractionTypeName;
            }
        }
        return extractionTypeName;
    }

    /**
     * The Class DecoratorExtractionType.
     */
    public class DecoratorExtractionType{

        /**
         * The disabled @link(Boolean).
         */
        private Boolean disabled = true;
        /**
         * The inner @link(ExtractionType).
         */
        private final ExtractionType inner;
        /**
         * The requetes @link(List<Requete>).
         */
        private List<Requete> requetes;

        /**
         * Instantiates a new decorator extraction type.
         *
         * @param et
         * @param extractionType the extraction type
         * @link(ExtractionType) the extraction type
         */
        public DecoratorExtractionType(final ExtractionType extractionType) {
            super();
            this.inner = extractionType;
        }

        /**
         * Gets the disabled.
         *
         * @return the disabled
         */
        public Boolean getDisabled() {
            return disabled;
        }

        /**
         * Gets the inner.
         *
         * @return the inner
         */
        public ExtractionType getInner() {
            return inner;
        }

        /**
         * Gets the localized name.
         *
         * @return the localized name
         */
        public String getLocalizedName() {
            return getLocalizedExtractionTypeName(inner.getName());
        }

        /**
         * Gets the requetes.
         *
         * @return the requetes
         */
        public List<Requete> getRequetes() {
            return requetes;
        }

        /**
         * Sets the disabled.
         *
         * @param disabled the new disabled
         */
        public void setDisabled(final Boolean disabled) {
            this.disabled = disabled;
        }

        /**
         * Sets the requetes.
         *
         * @param requetes the new requetes
         */
        public void setRequetes(final List<Requete> requetes) {
            this.requetes = requetes;
        }
    }
}
