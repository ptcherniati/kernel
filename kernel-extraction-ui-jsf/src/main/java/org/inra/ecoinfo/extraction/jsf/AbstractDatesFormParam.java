package org.inra.ecoinfo.extraction.jsf;

import java.io.PrintStream;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AbstractDatesFormParam.
 */
public abstract class AbstractDatesFormParam implements IDateFormParameter {

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.utils.vo.messages";
    /**
     * The Constant MSG_FROM.
     */
    protected static final String MSG_FROM = "PROPERTY_MSG_FROM";
    /**
     * The Constant MSG_TO.
     */
    protected static final String MSG_TO = "PROPERTY_MSG_TO";
    /**
     * The Constant DATE_FORMAT.
     */
    protected static final String DATE_FORMAT = "dd/MM/yyyy";
    /**
     * The Constant DATE_FORMAT_WT_YEAR.
     */
    protected static final String DATE_FORMAT_WT_YEAR = "dd/MM";
    /**
     * The Constant INITIAL_SQL_CONDITION.
     */
    protected static final String INITIAL_SQL_CONDITION = "and (";
    /**
     * The Constant SQL_CONDITION.
     */
    protected static final String SQL_CONDITION = "%s between :%s and :%s or ";
    /**
     * The Constant END_PREFIX.
     */
    protected static final String END_PREFIX = "end";
    /**
     * The Constant START_PREFIX.
     */
    protected static final String START_PREFIX = "start";
    /**
     * The Constant START_INDEX.
     */
    public static final String START_INDEX = "start";
    /**
     * The Constant END_INDEX.
     */
    public static final String END_INDEX = "end";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractDatesFormParam.class);
    /**
     * The localization manager.
     */
    protected ILocalizationManager localizationManager;
    List<Period> periods = new ArrayList();
    String dateFormat = DateUtil.DD_MM_YYYY;
    private String PROPERTY_MSG_MISSING_START_DATE="PROPERTY_MSG_MISSING_START_DATE";
    private String PROPERTY_MSG_MISSING_END_DATE="PROPERTY_MSG_MISSING_END_DATE";

    /**
     * Instantiates a new abstract dates form param.
     *
     * @param localizationManager the localization manager
     * @param string
     * @param dateFormat
     */
    public AbstractDatesFormParam(ILocalizationManager localizationManager, String dateFormat) {
        super();
        this.localizationManager = localizationManager;
        this.dateFormat = dateFormat;
    }

    /**
     * Builds the period string.
     *
     * @param currentYear the current year
     * @param periodStart the period start
     * @param periodEnd the period end
     * @return the string
     * @throws DateTimeException the parse exception
     */
    protected String buildPeriodString(int currentYear, String periodStart, String periodEnd)
            throws DateTimeException {
        return String.format(
                "%s %s/%d %s %s/%d",
                getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH,
                        AbstractDatesFormParam.MSG_FROM),
                periodStart,
                currentYear,
                getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH,
                        AbstractDatesFormParam.MSG_TO), periodEnd, currentYear);
    }

    /**
     * Builds the summary.
     *
     * @param printStream the print stream
     * @throws DateTimeException the parse exception
     */
    public abstract void buildSummary(PrintStream printStream) throws DateTimeException;

    /**
     * Custom validate.
     */
    public abstract void customValidate();

    /**
     * Gets the checks if is valid.
     *
     * @return the checks if is valid
     */
    public Boolean getIsValid() {
        customValidate();
        return testValidityPeriods();
    }

    /**
     * Gets the localization manager.
     *
     * @return the localization manager
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     * Gets the pattern date.
     *
     * @return the pattern date
     */
    @Override
    public abstract String getPatternDate();

    /**
     * Gets the periods.
     *
     * @return the periods
     */
    public List<Period> getPeriods() {
        return periods;
    }

    /**
     * Gets the summary html.
     *
     * @return the summary html
     * @throws DateTimeException the parse exception
     */
    public abstract String getSummaryHTML() throws DateTimeException;

    /**
     * Gets the validity messages.
     *
     * @return the validity messages
     */
    public List<String> getValidityMessages() {
        return periods.stream()
                .map(p->p.getErrorsMessages())
                .filter(e->!e.isEmpty())
                .map((e) -> replaceEmtyDates(e))
                .map(e->e.values().stream().collect(Collectors.joining("</li><li>","<ul><li>", "</li></ul>")))
                .collect(Collectors.toList());
    }

    /**
     * Prints the validity messages.
     *
     * @param printStream the print stream
     */
    protected void printValidityMessages(PrintStream printStream) {
        printStream.println("<ul>");
        getValidityMessages().stream().forEach((validityMessage) -> {
            printStream.println(String.format("<li>%s</li>", validityMessage));
        });
        printStream.println("</ul>");
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Test validity periods.
     * @return 
     */
    protected boolean testValidityPeriods() {
        return periods.stream().allMatch(Period::testValidityPeriod);
    }

    /**
     * Adds the period years continuous.
     *
     * @return the string
     */
    public String addPeriod() {
        periods.add(new Period(dateFormat));
        return null;
    }

    private Map<String,String> replaceEmtyDates(Map<String, String> e) {
        e.computeIfPresent(Period.ERROR_MESSAGE_START_DATE, (k,v)->localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_START_DATE));
        e.computeIfPresent(Period.ERROR_MESSAGE_END_DATE, (k,v)->localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_END_DATE));
        return e;
    }
}
