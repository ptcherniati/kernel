/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction.genericfilesrepository.ui;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import javax.faces.context.FacesContext;
import org.inra.ecoinfo.dataset.versioning.entity.GenericDatatype;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.refdata.datatype.DataType;

/**
 *
 * @author tcherniatinsky
 */
public class UIGenericDatatype  implements Serializable{

    Set<DatatypeJSF> availablesDatatype = new HashSet<>();
    Set<DatatypeJSF> selectedDatatype = new HashSet<>();

    /**
     *
     */
    protected ILocalizationManager localizationManager;
    private Properties propertiesDatatypesNames;

    /**
     *
     */
    private Map<String, String> localizedDatatypes = new HashMap();

    /**
     *
     */
    public UIGenericDatatype() {
    }

    /**
     *
     * @param localizedDatatypes
     */
    public void setLocalizedDatatypes(Map<String, String> localizedDatatypes) {
        this.localizedDatatypes = localizedDatatypes;
    }

    /**
     * Gets the date step is valid.
     *
     * @return the date step is valid
     */
    public Boolean getGenericDatataypeIsValid() {
        return !selectedDatatype.isEmpty();
    }

    /**
     * Inits the dates request param.
     *
     * @param localizationManager
     */
    public void initGenericDatatypeRequestParam(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Locale locale = facesContext.getViewRoot().getLocale();
        this.propertiesDatatypesNames  = localizationManager.newProperties("datatype", "name", locale);
    }

    /**
     *
     * @param metadatasMap
     */
    public void addDatatypestoMap(Map<String, Object> metadatasMap) {
        metadatasMap.put(
                GenericDatatype.class.getSimpleName(), 
                selectedDatatype.stream()
                        .distinct()
                        .map(d->d.getDatatype())
                        .collect(Collectors.toSet()));
    }

    /**
     *
     * @return
     */
    public Set<DatatypeJSF> getAvailablesDatatype() {
        return availablesDatatype;
    }

    /**
     *
     * @return
     */
    public Set<DatatypeJSF> getSelectedDatatype() {
        return selectedDatatype;
    }

    /**
     *
     * @param availablesDatatype
     */
    public void setAvailablesDatatype(Set<DataType> availablesDatatype) {
        this.availablesDatatype = availablesDatatype.stream()
                .map(d -> new DatatypeJSF(d))
                .collect(Collectors.toSet());
    }

    /**
     *
     * @param datatypeJSF
     */
    public void selectDatatype(DatatypeJSF datatypeJSF) {
        if (!datatypeJSF.getSelected()) {
            selectedDatatype.add(datatypeJSF);
            datatypeJSF.setSelected(true);
        } else {
            selectedDatatype.remove(datatypeJSF);
            datatypeJSF.setSelected(false);
        }
    }
    
    /**
     *
     */
    public void removeAllDatatype(){
        availablesDatatype.stream().forEach(d->d.setSelected(false));
        selectedDatatype.clear();
    }
    
    /**
     *
     */
    public void addAllDatatype(){
        availablesDatatype.stream().forEach(d->d.setSelected(true));
        selectedDatatype.addAll(availablesDatatype);
    }

    /**
     *
     */
    public class DatatypeJSF {

        /**
         * The selected @link(boolean).
         */
        boolean selected = false;
        DataType datatype;
        String localizedName;

        /**
         *
         * @param dt
         * @param datatype
         */
        public DatatypeJSF(DataType datatype) {
            this.datatype = datatype;
            localizedName = propertiesDatatypesNames.getProperty(datatype.getName());
            localizedName = Strings.isNullOrEmpty(localizedName) ? this.datatype.getName() : localizedName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedName() {
            return localizedName;
        }

        /**
         *
         * @param localizedName
         */
        public void setLocalizedName(String localizedName) {
            this.localizedName = localizedName;
        }

        /**
         *
         * @return
         */
        public boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public DataType getDatatype() {
            return datatype;
        }
    }
}
