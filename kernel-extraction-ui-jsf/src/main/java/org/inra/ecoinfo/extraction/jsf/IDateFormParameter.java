package org.inra.ecoinfo.extraction.jsf;

/**
 * The Interface IDateFormParameter.
 */
public interface IDateFormParameter {

    /**
     *
     * @return
     */
    String getPatternDate();
}
