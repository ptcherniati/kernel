package org.inra.ecoinfo.extraction.jsf;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.faces.component.ValueHolder;
import javax.faces.event.AjaxBehaviorEvent;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 * The Class DatesYearsContinuousFormParamVO.
 */
public class DatesFormParamVO extends AbstractDatesFormParam implements IDateFormParameter {

    /**
     * The Constant LABEL.
     */
    public static final String LABEL = "DatesYearsContinuousFormParam";

    /**
     *
     */
    public static final String PROPERTY_MSG_FROM_TO = "PROPERTY_MSG_FROM_TO";

    /**
     *
     */
    public static final String PROPERTY_MSG_FROM_TO_2 = "PROPERTY_MSG_FROM_TO_2";
    Period selectedPeriod;

    /**
     * Instantiates a new dates years continuous form param vo.
     *
     * @param localizationManager the localization manager
     * @param string
     * @param dateFormat
     */
    public DatesFormParamVO(ILocalizationManager localizationManager, String dateFormat) {
        super(localizationManager, dateFormat);
        addPeriod();
    }

    /**
     *
     * @return
     */
    public Period getSelectedPeriod() {
        return selectedPeriod;
    }

    /**
     *
     * @param selectedPeriod
     */
    public void setSelectedPeriod(Period selectedPeriod) {
        this.selectedPeriod = selectedPeriod;
    }

    /**
     *
     * @param printStream
     * @throws DateTimeException
     */
    @Override
    public void buildSummary(PrintStream printStream) throws DateTimeException {
        periods.stream().forEach((t) -> {
            if (t.testValidityPeriod()) {
                printStream.print(
                        String.format(
                                getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH, PROPERTY_MSG_FROM_TO),
                                t.getBeginDateToString(),
                                t.getEndDateString()
                        ));
            }
        });
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#customValidate()
     */
    /**
     *
     */
    @Override
    public void customValidate() {
        LOGGER.info("unused");
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#getPatternDate()
     */
    /**
     *
     * @return
     */
    @Override
    public String getPatternDate() {
        return AbstractDatesFormParam.DATE_FORMAT;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.acbb.extraction.jsf.IDateFormParameter#getPeriodsFromDateFormParameter()
     */
    @Override
    public List<Period> getPeriods() {
        return Optional.ofNullable(periods)
                .filter(p -> !p.isEmpty())
                .orElse(Stream.of(new Period(dateFormat)).collect(Collectors.toList()));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#getSummaryHTML()
     */
    /**
     *
     * @return @throws DateTimeException
     */
    @Override
    public String getSummaryHTML() throws DateTimeException {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try (final PrintStream printStream = new PrintStream(bos, true, StandardCharsets.ISO_8859_1.name())) {
            if (!getIsValid()) {
                printValidityMessages(printStream);
                return bos.toString(StandardCharsets.ISO_8859_1.name());
            }
            periods.stream().forEach((t) -> {
                printStream.println(String.format(
                        "%s<br/>",
                        String.format(
                                getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH, PROPERTY_MSG_FROM_TO),
                                t.getBeginDateString(),
                                t.getEndDateString()))
                );
            });

        } catch (UnsupportedEncodingException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new DateTimeException(ex.getMessage(), ex);
        }
        return bos.toString();
    }

    /**
     *
     * @param event
     */
    public void changeDateStart(AjaxBehaviorEvent event) {
        LocalDate date = (LocalDate) ((ValueHolder) event.getSource()).getValue();
        selectedPeriod = (Period) event.getComponent().getAttributes().get("period");
        selectedPeriod.setStartDate(date);
    }

    /**
     *
     * @param event
     */
    public void changeDateEnd(AjaxBehaviorEvent event) {
        LocalDate date = (LocalDate) ((ValueHolder) event.getSource()).getValue();
        selectedPeriod = (Period) event.getComponent().getAttributes().get("period");
        selectedPeriod.setLastDate(date);
    }
}
