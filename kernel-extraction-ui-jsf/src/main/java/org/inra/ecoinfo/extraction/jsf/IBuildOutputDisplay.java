package org.inra.ecoinfo.extraction.jsf;

import java.io.IOException;
import org.inra.ecoinfo.extraction.IDisplay;
import org.inra.ecoinfo.extraction.IParameter;

/**
 * The Interface IBuildOutputDisplay.
 *
 * @author Philippe TCHERNIATINSKY
 */
public interface IBuildOutputDisplay extends IDisplay {

    /**
     * Buid output.
     *
     * @param fileName the file name
     * @param parameters the parameters
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(String) the file name
     * @link(IParameter) the parameters
     */
    void buidOutput(String fileName, IParameter parameters) throws IOException;

    /**
     * Builds the request remainders.
     *
     * @param parameters the parameters
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(IParameter) the parameters
     */
    String buildRequestRemainders(IParameter parameters) throws IOException;

    /**
     * Removes the file.
     *
     * @param fileRandomSuffix the file random suffix
     * @link(String) the file random suffix
     */
    void removeFile(String fileRandomSuffix);
}
