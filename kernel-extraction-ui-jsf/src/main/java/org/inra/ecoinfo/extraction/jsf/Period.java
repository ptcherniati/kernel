package org.inra.ecoinfo.extraction.jsf;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Periode.
 */
public class Period extends IntervalDate {

    private static final Logger LOGGER = LoggerFactory.getLogger(Period.class);

    /**
     *
     */
    public static final String LOGGER_ERROR = "Method %s -> %s";

    static final String ERROR_MESSAGE_START_DATE = "ERROR_MESSAGE_START_DATE";
    static final String ERROR_MESSAGE_END_DATE = "ERROR_MESSAGE_END_DATE";
    static final String ERROR_MESSAGE_BAD_INTERVAL = "ERROR_MESSAGE_BAD_INTERVAL";
    private static long counter = 0l;
    private Map<String, String> errorsMessages = new HashMap();
    final long rowKey;

    /**
     * Instantiates a new periode.
     *
     * @param string
     * @param dateFormat
     */
    public Period(String dateFormat) {
        super(dateFormat);
        rowKey = ++counter;
    }

    /**
     *
     * @return
     */
    public long getRowKey() {
        return rowKey;
    }

    /**
     *
     * @return
     */
    public Map<String, String> getErrorsMessages() {
        return Optional.ofNullable(errorsMessages)
                .filter(e -> !e.isEmpty())
                .orElse(getMissingDates());
    }

    /**
     *
     * @return
     */
    public String getBeginDateString() {
        return Optional.ofNullable(beginDate)
                .map(d -> DateUtil.getUTCDateTextFromLocalDateTime(d, DateUtil.DD_MM_YYYY))
                .orElse("");
    }

    /**
     *
     * @param beginDateString
     */
    public void setBeginDateString(String beginDateString) {
        try {
            beginDate = getDate(beginDateString);
            errorsMessages.remove(ERROR_MESSAGE_START_DATE);
        } catch (BadExpectedValueException ex) {
            LOGGER.error(String.format(LOGGER_ERROR, "setBeginDateString", ex.getMessage()), ex);
            errorsMessages.put(ERROR_MESSAGE_START_DATE, ex.getMessage());
            beginDate = null;
            return;
        }
        if (endDate != null) {
            try {
                setDates(beginDate, endDate);
                errorsMessages.remove(ERROR_MESSAGE_END_DATE);
            } catch (BadExpectedValueException ex) {
                LOGGER.error(String.format(LOGGER_ERROR, "setBeginDateString /end", ex.getMessage()), ex);
                errorsMessages.put(ERROR_MESSAGE_BAD_INTERVAL, ex.getMessage());
                beginDate = null;
            }
        }
    }

    /**
     *
     * @param beginDate
     */
    public void setStartDate(LocalDate beginDate) {
        if (beginDate != null) {
            this.beginDate = beginDate.atStartOfDay();
            errorsMessages.remove(ERROR_MESSAGE_START_DATE);
        } else {
            this.beginDate = null;
            return;
        }
        if (endDate != null) {
            try {
                setDates(this.beginDate, endDate);
                errorsMessages.remove(ERROR_MESSAGE_END_DATE);
            } catch (BadExpectedValueException ex) {
                LOGGER.error(String.format(LOGGER_ERROR, "setStartDate", ex.getMessage()), ex);
                errorsMessages.put(ERROR_MESSAGE_BAD_INTERVAL, ex.getMessage());
                beginDate = null;
            }
        }
    }

    /**
     *
     * @return
     */
    public String getEndDateString() {
        return Optional.ofNullable(endDate)
                .map(d -> DateUtil.getUTCDateTextFromLocalDateTime(d, DateUtil.DD_MM_YYYY))
                .orElse("");
    }

    /**
     *
     * @param endDateString
     */
    public void setEndDateString(String endDateString) {
        try {
            endDate = getDate(endDateString);
            errorsMessages.remove(ERROR_MESSAGE_END_DATE);
        } catch (BadExpectedValueException ex) {
            LOGGER.error(String.format(LOGGER_ERROR, "setEndDateString", ex.getMessage()), ex);
            errorsMessages.put(ERROR_MESSAGE_END_DATE, ex.getMessage());
            endDate = null;
            return;
        }
        if (beginDate != null) {
            try {
                setDates(beginDate, endDate);
                errorsMessages.remove(ERROR_MESSAGE_BAD_INTERVAL);
            } catch (BadExpectedValueException ex) {
            LOGGER.error(String.format(LOGGER_ERROR, "setEndDateString /begin", ex.getMessage()), ex);
                errorsMessages.put(ERROR_MESSAGE_BAD_INTERVAL, ex.getMessage());
                endDate = null;
            }
        }
    }

    /**
     *
     * @param endDate
     */
    public void setLastDate(LocalDate endDate) {
        if (endDate != null) {
            this.endDate = endDate.atTime(23, 59, 59);
            errorsMessages.remove(ERROR_MESSAGE_END_DATE);
        } else {
            this.endDate = null;
            return;
        }
        if (beginDate != null) {
            try {
                setDates(beginDate, this.endDate);
                errorsMessages.remove(ERROR_MESSAGE_BAD_INTERVAL);
            } catch (BadExpectedValueException ex) {
            LOGGER.error(String.format(LOGGER_ERROR, "setLastDate", ex.getMessage()), ex);
                errorsMessages.put(ERROR_MESSAGE_BAD_INTERVAL, ex.getMessage());
                this.endDate = null;
            }
        }
    }

    /**
     *
     * @return
     */
    public LocalDate getStartDate() {
        return Optional
                .ofNullable(beginDate)
                .map(d -> d.toLocalDate())
                .orElse(null);
    }

    /**
     *
     * @return
     */
    public LocalDate getLastDate() {
        return Optional
                .ofNullable(endDate)
                .map(d -> d.toLocalDate())
                .orElse(null);
    }

    /**
     *
     * @return
     */
    public boolean testValidityPeriod() {
        return beginDate != null && endDate != null;
    }

    /**
     *
     * @return
     */
    public String getDateStartToString() {
        return beginDate == null ? "" : getBeginDateString();
    }

    /**
     *
     * @return
     */
    public String getDateEndToString() {
        return endDate == null ? "" : getEndDateString();
    }

    private Map<String, String> getMissingDates() {
        Map<String, String> misssingDates = new HashMap();
        if (getBeginDate() == null) {
            misssingDates.put(ERROR_MESSAGE_START_DATE, ERROR_MESSAGE_END_DATE);
        }
        if (getEndDate() == null) {
            misssingDates.put(ERROR_MESSAGE_END_DATE, ERROR_MESSAGE_END_DATE);
        }
        return misssingDates;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
