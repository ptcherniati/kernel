package org.inra.ecoinfo.extraction;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.inra.ecoinfo.utils.StreamUtils;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Servlet implementation class ServletRetrieveVariableGraphic.
 */
@WebServlet(value = "/downloadRequest", name = "servlet download requests")
public class ServletDownloadRequest extends HttpServlet {

    private static final String DD_MM_YYYY_HH_MM_SS_SSS = "dd-MM-yyyy-HH-mm-ss-SSS";
    /**
     * The Constant PARAMETER_REQUEST_CODE_DATATYPE @link(String).
     */
    private static final String PARAMETER_REQUEST_CODE_DATATYPE = "code";
    /**
     * The Constant PARAMETER_REQUEST_DATE_REQUETE @link(String).
     */
    private static final String PARAMETER_REQUEST_DATE_REQUETE = "date";
    /**
     * The Constant PARAMETER_REQUEST_SUFFIX_NAME_FILE_REQUETE @link(String).
     */
    private static final String PARAMETER_REQUEST_SUFFIX_NAME_FILE_REQUETE = "suffix";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new servlet download request.
     *
     * @see HttpServlet#HttpServlet()
     */
    public ServletDownloadRequest() {
        super();
    }

    /**
     * Do get.
     *
     * @param request
     * @link(HttpServletRequest) the request
     * @param response
     * @link(HttpServletResponse) the response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred. @see
     * HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final ServletContext servletContext = this.getServletContext();
        final WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        final IExtractionManager extractionManager = (IExtractionManager) applicationContext.getBean(IExtractionManager.ID_BEAN);
        final String codeDatatype = request.getParameter(ServletDownloadRequest.PARAMETER_REQUEST_CODE_DATATYPE);
        final String suffixNameFile = request.getParameter(ServletDownloadRequest.PARAMETER_REQUEST_SUFFIX_NAME_FILE_REQUETE);
        String date = request.getParameter(ServletDownloadRequest.PARAMETER_REQUEST_DATE_REQUETE);
        try {
            final byte[] datas = extractionManager.doDownload(codeDatatype, suffixNameFile);
            final String nameFileDownload = String.format("extraction_%s_%s", codeDatatype, Utils.createCodeFromString(date));
            response.setContentType("application/zip");
            response.setContentLength(datas.length);
            response.setHeader("Content-Disposition", String.format("attachment; filename=%s", nameFileDownload.replaceAll(",", "_").concat(Utils.EXTENSION_FILE_ZIP)));
            response.getOutputStream().write(datas);
            response.getOutputStream().flush();
            StreamUtils.closeStream(response.getOutputStream());
        } catch (final BusinessException | IOException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in init", e);
        }
    }

    /**
     * Do post.
     *
     * @param request
     * @link(HttpServletRequest) the request
     * @param response
     * @link(HttpServletResponse) the response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred. @see
     * HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
