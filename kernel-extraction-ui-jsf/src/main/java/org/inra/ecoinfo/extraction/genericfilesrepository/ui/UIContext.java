/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction.genericfilesrepository.ui;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.DefaultTreeNode;
import org.inra.ecoinfo.mga.business.composite.FlatNode;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.managedbean.IBeanTreeNodeBuilder;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.managedbean.UITreeNode;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.Utils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.TreeNode;

/**
 *
 * @author tcherniatinsky
 */
public class UIContext implements Serializable, IBeanTreeNodeBuilder {

    Map<DataType, TreeNode> availableNodes;
    ILocalizationManager localizationManager;
    private Properties propertiesDatatypesNames;
    private Map<String, String> localizedDatatypes = new HashMap();
    IPolicyManager policyManager;
    TreeNode[] selectedNodeArray;

    /**
     *
     */
    protected final Map<Long, TreeNode> cashMap = new HashMap();

    /**
     *
     */
    public UIContext() {
    }

    /**
     * Gets the date step is valid.
     *
     * @return the date step is valid
     */
    public Boolean getContextIsValid() {
        return getSelectedNodesDatatypes(null).findFirst().isPresent();
    }

    /**
     * Inits the dates request param.
     *
     * @param localizationManager
     * @param policyManager
     */
    public void initContextRequestParam(ILocalizationManager localizationManager, IPolicyManager policyManager) {
        this.localizationManager = localizationManager;
        this.policyManager = policyManager;
        this.propertiesDatatypesNames = localizationManager.newProperties(
                DataType.NAME_ENTITY_JPA, "name");
        Properties localizedDatatypes = localizationManager.newProperties(Nodeable.getLocalisationEntite(DataType.class), Nodeable.ENTITE_COLUMN_NAME);
        for (Map.Entry<Object, Object> entry : localizedDatatypes.entrySet()) {
            String key = Utils.createCodeFromString((String) entry.getKey());
            String value = (String) entry.getValue();
            this.localizedDatatypes.put(key, value);
        }
    }

    /*
    add the list of node_data_set id
    
     */

    /**
     *
     * @param metadatasMap
     */

    public void addContextToMap(Map<String, Object> metadatasMap) {
        metadatasMap.put(NodeDataSet.class.getSimpleName(),
                getSelectedNodesDatatypes(null)
                        .map(fn -> fn.getLeafId())
                        .collect(Collectors.toSet()));
    }

    /**
     *
     * @param availableNodes
     */
    public void setAvailableNodes(Map<DataType, ITreeNode<FlatNode>> availableNodes) {
        this.availableNodes = Optional.ofNullable(availableNodes).orElse(new HashMap<>()).entrySet().stream()
                .collect(Collectors.toMap(k -> k.getKey(), k -> getOrCreateTreeNode(k.getValue())));
    }

    @Override
    public <T extends IFlatNode> TreeNode getOrCreateTreeNode(ITreeNode<T> flatNode) {
        return new UITreeNode(flatNode, this, cashMap);
    }

    /**
     *
     * @return
     */
    public Map<DataType, TreeNode> getAvailableNodes() {
        return Optional.ofNullable(availableNodes).orElse(new HashMap<>());
    }

    /**
     *
     */
    public void removeAllNodes() {
        setSelectedNodesArray(
                new TreeNode[0]
        );
        getSelectedTreeNodes(false)
                .collect(Collectors.toList()).toArray(new TreeNode[0]);
        RequestContext.getCurrentInstance().update("subContainerWest:contextList");
    }

    /**
     *
     */
    public void addAllNodes() {
        final List<TreeNode> collect = getSelectedTreeNodes(true)
                .map(n -> getBranchNodes(n))
                .flatMap(List::stream)
                .collect(Collectors.toList());
        this.selectedNodeArray = collect.toArray(new TreeNode[collect.size()]);
        RequestContext.getCurrentInstance().update("subContainerWest:contextList");
    }

    private List<TreeNode> getBranchNodes(TreeNode node) {
        List<TreeNode> branchNodes = new LinkedList();
        do {
            branchNodes.add(node);
            node = node.getParent();
        } while (node != null);
        return branchNodes;
    }

    private Stream<FlatNode> getSelectedNodesDatatypes(Boolean isSelected) {
        Stream<FlatNode> flatNodesStream = getSelectedTreeNodes(isSelected)
                .map(node -> (DefaultTreeNode) node.getData())
                .map(node -> (FlatNode) node.getData());
        return flatNodesStream;
    }

    private Stream<TreeNode> getSelectedTreeNodes(Boolean isSelected) {
        return getAvailableNodes().values().stream()
                .map(
                        node -> getAllLeafNodes(node, isSelected)
                )
                .flatMap(List::stream)
                .filter(node -> node.isSelected());
    }

    /**
     *
     * @return
     */
    public Set<String> getSelectedPathes() {
        Set<String> selectedPathes = getSelectedNodesDatatypes(null)
                .map(fn -> fn.getPath())
                .collect(Collectors.toSet());
        return selectedPathes;
    }

    private List<TreeNode> getAllLeafNodes(TreeNode node, Boolean isSelected) {
        if (isSelected != null) {
            node.setSelected(isSelected);
        }
        if (node.isLeaf()) {
            return Stream.of(node).collect(Collectors.toList());
        } else {
            return node.getChildren().stream()
                    .map(child -> getAllLeafNodes(child, isSelected))
                    .flatMap(List::stream)
                    .collect(Collectors.toList());
        }

    }

    /**
     *
     * @return
     */
    public TreeNode[] getSelectedNodesArray() {
        return selectedNodeArray;
    }

    /**
     *
     * @param event
     */
    public void onNodeUnselect(NodeUnselectEvent event) {
        getAllLeafNodes(event.getTreeNode(), false);
    }

    /**
     *
     * @param selectedNode
     */
    public void setSelectedNodesArray(TreeNode[] selectedNode) {
        Stream.of(Optional.ofNullable(selectedNode).orElse(new TreeNode[0]))
                .filter(node -> !node.isExpanded())
                .forEach(node -> getAllLeafNodes(node, node.isSelected()));

        this.selectedNodeArray = selectedNode;
    }
}
