/*
 *
 */
package org.inra.ecoinfo.extraction.genericfilesrepository.ui;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.genericfilesrepository.api.IGenericDatatypeManager;
import org.inra.ecoinfo.extraction.genericfilesrepository.impl.GenericDatatypeParameter;
import org.inra.ecoinfo.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.menu.jsf.UIBeanMenu;
import org.inra.ecoinfo.mga.business.composite.FlatNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 * The Class UIBeanMPS.
 */
@ManagedBean(name = "uiGenericdatatype")
@ViewScoped
public class UIBeanGenericDatatype extends AbstractUIBeanForSteps implements Serializable {

    // TODO
    /**
     * Autre type de données @ManagedProperty
     */
    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The extraction manager.
     */
    @ManagedProperty(value = "#{extractionManager}")
    IExtractionManager extractionManager;
    @ManagedProperty(value = "#{uiMenu}")
    UIBeanMenu uiMenu;

    /**
     * The localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    ILocalizationManager localizationManager;

    /**
     * The MPS dataset manager.
     */
    @ManagedProperty(value = "#{genericDatatypeManager}")
    IGenericDatatypeManager genericDatatypeManager;

    /**
     * The notifications manager.
     */
    @ManagedProperty(value = "#{notificationsManager}")
    INotificationsManager notificationsManager;

    ParametersRequest parametersRequest = new ParametersRequest();

    /**
     * The security context.
     */
    @ManagedProperty(value = "#{policyManager}")
    IPolicyManager policyManager;

    /**
     * The transaction manager.
     */
    @ManagedProperty(value = "#{transactionManager}")
    JpaTransactionManager transactionManager;

    UIDate uiDate;
    UIGenericDatatype uiGenericDatatype;
    UIContext uiContext;
    Map<DataType, ITreeNode<FlatNode>> nodesForDatatype;

    // GETTERS SETTERS - BEANS
    /**
     * Instantiates a new uI bean mps.
     */
    public UIBeanGenericDatatype() {
        super();
    }

    /**
     *
     * @return
     */
    public UIDate getUiDate() {
        return uiDate;
    }

    /**
     *
     * @return
     */
    public UIContext getUiContext() {
        return uiContext;
    }

    /**
     * Extract.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public final String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap();
        // TODO
        uiDate.addDatestoMap(metadatasMap);
        uiGenericDatatype.addDatatypestoMap(metadatasMap);
        uiContext.addContextToMap(metadatasMap);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS,
                this.parametersRequest.getCommentExtraction());
        final GenericDatatypeParameter parameters = new GenericDatatypeParameter(metadatasMap);
        this.extractionManager.extract(parameters, 1);
        return null;
    }

    /**
     * autre type de données
     */
    /**
     * Gets the checks if is step valid.
     *
     * @return the checks if is step valid
     * @see
     * org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#getIsStepValid()
     */
    @Override
    public final boolean getIsStepValid() {
        switch (getStep()) {
            case 1:
                return uiDate.getDateStepIsValid();
            case 2:
                return uiGenericDatatype.getGenericDatataypeIsValid();
            case 3:
                return uiContext.getContextIsValid();
            default:
                return false;
        }
    }

    /**
     * @return the parametersRequest
     */
    public ParametersRequest getParametersRequest() {
        return this.parametersRequest;
    }

    /**
     * Inits the properties.
     */
    @PostConstruct
    public void initProperties() {
        uiDate = new UIDate();
        uiDate.initDatesRequestParam(localizationManager);
        uiGenericDatatype = new UIGenericDatatype();
        uiGenericDatatype.initGenericDatatypeRequestParam(localizationManager);
        uiContext = new UIContext();
        uiContext.initContextRequestParam(localizationManager, policyManager);
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public final String navigate() {
        return "genericDatatype";
    }

    /**
     * Next step.
     *
     * @return the string
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#nextStep()
     */
    @Override
    public final String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 2:
                getGenericDatatype(uiMenu.getConfigurationNumber());
                break;
            case 3:
                Map<DataType, ITreeNode<FlatNode>> collect = uiGenericDatatype.getSelectedDatatype().stream()
                        .map(d -> d.getDatatype())
                        .collect(Collectors.toMap(d -> d, d -> nodesForDatatype.get(d)));

                uiContext.setAvailableNodes(collect);
                break;
            default:
                break;
        }
        return null;
    }

    /**
     * @param extractionManager the extractionManager to set
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     * @param localizationManager the localizationManager to set
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * @param notificationsManager the notificationsManager to set
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * @param parametersRequest the parametersRequest to set
     */
    public void setParametersRequest(ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }

    /**
     * @param policyManager the securityContext to set
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * @param transactionManager the transactionManager to set
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    private void getGenericDatatype(Integer codeConfiguration) {
        final List<IntervalDate> periods = uiDate.datesRequestParam.getPeriods().stream().collect(Collectors.toList());
        nodesForDatatype = genericDatatypeManager.getDatatype(periods, codeConfiguration);
        uiGenericDatatype.setAvailablesDatatype(nodesForDatatype.keySet());
    }

    /**
     *
     * @param genericDatatypeManager
     */
    public void setGenericDatatypeManager(IGenericDatatypeManager genericDatatypeManager) {
        this.genericDatatypeManager = genericDatatypeManager;
    }

    /**
     *
     * @return
     */
    public UIGenericDatatype getUiGenericDatatype() {
        return uiGenericDatatype;
    }

    /**
     *
     * @param uiMenu
     */
    public void setUiMenu(UIBeanMenu uiMenu) {
        this.uiMenu = uiMenu;
    }

    /**
     * The Class ParametersRequest.
     */
    public class ParametersRequest {

        /**
         * The comment extraction @link(String).
         */
        String commentExtraction;
        /**
         * The european format @link(boolean).
         */
        boolean format = true;

        /**
         * Gets the comment extraction.
         *
         * @return the comment extraction
         */
        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        /**
         * Gets the european format.
         *
         * @return the european format
         */
        public boolean getFormat() {
            return this.format;
        }

        /**
         * Gets the form is valid.
         *
         * @return the form is valid
         */
        public boolean getFormIsValid() {
            return uiContext.getContextIsValid();
        }

        /**
         * Sets the comment extraction.
         *
         * @param commentExtraction the new comment extraction
         */
        public final void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         * Sets the european format.
         *
         * @param format
         */
        public final void setFormat(final boolean format) {
            this.format = format;
        }
    }

}
