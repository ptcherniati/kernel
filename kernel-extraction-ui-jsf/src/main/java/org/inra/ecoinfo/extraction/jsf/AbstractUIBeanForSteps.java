package org.inra.ecoinfo.extraction.jsf;

import java.util.Optional;
import java.util.TimeZone;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlPanelGroup;
import org.inra.ecoinfo.extraction.IMotivation;
import org.inra.ecoinfo.extraction.impl.DefaultMotivation;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.wizard.Wizard;
import org.primefaces.event.FlowEvent;

/**
 * The Class AbstractUIBeanForSteps.
 */
@SessionScoped
public abstract class AbstractUIBeanForSteps {

    /**
     *
     */
    public static final TimeZone TIME_ZONE = TimeZone.getTimeZone("UTC");

    /**
     * The Constant TITLE_STEP.
     */
    private static final String TITLE_STEP = " %d : %s";

    
    private boolean enabledMotivation = false;

    /**
     * The security context.
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;
    /**
     * The step.
     */
    private int step = 1;
    private String motivation;

    /**
     * The toggle panel.
     */
    private Wizard wizard;
    private int steps;
    /**
     * The accordion.
     */
    private AccordionPanel accordion;
    /**
     * The panel.
     */
    private HtmlPanelGroup panel;

    /**
     * Instantiates a new abstract ui bean for steps.
     */
    public AbstractUIBeanForSteps() {
        super();
    }

    /**
     *
     * @return
     */
    public IMotivation getStaticMotivation() {
        return ((InheritableThreadLocal<IMotivation>)DefaultMotivation.getLocalThread()).get();
    }

    /**
     *
     * @param motivation
     */
    public void setStaticMotivation(IMotivation motivation) {
        ((InheritableThreadLocal<IMotivation>)DefaultMotivation.getLocalThread()).set(motivation);
    }

    /**
     *
     * @param motivation
     */
    public void setStaticMotivation(String motivation) {
        setStaticMotivation(new DefaultMotivation(motivation));
    }

    /**
     *
     * @param enabledMotivation
     */
    public void setEnabledMotivation(boolean enabledMotivation) {
        if(enabledMotivation){
            setStaticMotivation(new DefaultMotivation());
        }else{
            setStaticMotivation((IMotivation)null);
        }
        this.enabledMotivation = enabledMotivation;
    }

    /**
     *
     * @return
     */
    public boolean isEnabledMotivation() {
        return enabledMotivation;
    }

    /**
     *
     * @return
     */
    public String getMotivation() {
        return motivation;
    }

    /**
     *
     * @param motivation
     */
    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    /**
     * Gets the accordion.
     *
     * @return the accordion
     */
    public AccordionPanel getAccordion() {
        return accordion;
    }

    /**
     * Gets the change step.
     *
     * @return the change step
     */
    public String getChangeStep() {
        final Tab currentAccordion = accordion.findTab(accordion.getActiveIndex());
        final int currentAccordionRow = accordion.getChildren().indexOf(currentAccordion);
        // togglePanel.setActiveItem(((Panel) togglePanel.getChildren().get(currentAccordionRow)));
        step = currentAccordionRow + 1;
        return "";
    }

    /**
     * Gets the current toggle item.
     *
     * @return the current toggle item
     */
    public Tab getCurrentToggleItem() {
        return null;// (Tab) togglePanel.gegetItem(togglePanel.getActiveItem());
    }

    /**
     * Gets the checks if is step valid.
     *
     * @return the checks if is step valid
     */
    public abstract boolean getIsStepValid();

    /**
     * Gets the panel.
     *
     * @return the panel
     */
    public HtmlPanelGroup getPanel() {
        return panel;
    }

    /**
     * Gets the step.
     *
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * Gets the title for step.
     *
     * @return the title for step
     */
    public String getTitleForStep() {
        return Optional.ofNullable(((Tab) getWizard().getChildren().get(step - 1)))
                .map(t -> String.format(AbstractUIBeanForSteps.TITLE_STEP, step, t.getTitle()))
                .orElse("");
    }

    /**
     *
     * @param event
     * @return
     */
    public String onFlowProcess(FlowEvent event) {
        return getWizard().getChildren().get(step).getId();
    }

    /**
     * Gets the toggle panel.
     *
     * @return the toggle panel
     */
    public Wizard getWizard() {
        if (steps == 0) {
            steps = Optional.ofNullable(wizard).map(w -> w.getChildCount()).orElse(steps);
        }
        return wizard;
    }

    /**
     * Next step.
     *
     * @return the string
     */
    public String nextStep() {
        if (step < steps && getWizard().getChildren().get(step) != null) {
            getWizard().setStep(getWizard().getChildren().get(step).getId());
            if (accordion.getChildren().get(step) != null) {
                accordion.setActiveIndex(Integer.toString(step));
            }
            step++;
        }
        return null;
    }

    /**
     * Prev step.
     *
     * @return the string
     */
    public String prevStep() {
        if (getWizard().getChildren().get(step - 2) != null) {
            getWizard().setStep(getWizard().getChildren().get(step - 2).getId());
            if (accordion.getChildren().get(step - 2) != null) {
                accordion.setActiveIndex(Integer.toString(step - 2));
            }
            step--;
        }
        return null;
    }

    /**
     * Sets the accordion.
     *
     * @param accordion the new accordion
     */
    public void setAccordion(AccordionPanel accordion) {
        this.accordion = accordion;
    }

    /**
     * Sets the security context.
     *
     * @param policyManager the new security context
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Sets the panel.
     *
     * @param panel the new panel
     */
    public void setPanel(HtmlPanelGroup panel) {
        this.panel = panel;
    }

    /**
     * Sets the step.
     *
     * @param step the new step
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     * Sets the toggle panel.
     *
     * @param panel the new toggle panel
     */
    public void setWizard(Wizard panel) {
        wizard = panel;
    }

    /**
     *
     * @return
     */
    public TimeZone getTimeZone() {
        return TIME_ZONE;
    }

    /**
     *
     * @return
     */
    public int getSteps() {
        return steps;
    }
}
