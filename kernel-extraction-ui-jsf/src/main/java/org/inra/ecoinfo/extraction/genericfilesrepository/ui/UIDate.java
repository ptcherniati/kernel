/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction.genericfilesrepository.ui;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.inra.ecoinfo.extraction.jsf.AbstractDatesFormParam;
import org.inra.ecoinfo.extraction.jsf.DatesFormParamVO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author tcherniatinsky
 */
public class UIDate  implements Serializable{
    /**
     * The dates request param @link(DatesRequestParamVO).
     */
    DatesFormParamVO datesRequestParam;

    /**
     *
     */
    public UIDate() {
    }

    /**
     * Builds the new map period.
     *
     * @return the map
     */
    protected final Map<String, String> buildNewMapPeriod() {
        final Map<String, String> firstMap = new HashMap<>();
        firstMap.put(AbstractDatesFormParam.START_INDEX, org.apache.commons.lang.StringUtils.EMPTY);
        firstMap.put(AbstractDatesFormParam.END_INDEX, org.apache.commons.lang.StringUtils.EMPTY);
        return firstMap;
    }

    /**
     * Gets the date step is valid.
     *
     * @return the date step is valid
     */
    public Boolean getDateStepIsValid() {
        return datesRequestParam.getIsValid();
    }

    /**
     * Gets the dates request param.
     *
     * @return the dates request param
     */
    public AbstractDatesFormParam getDatesRequestParam() {
        return this.datesRequestParam;
    }

    /**
     *
     * @param datesRequestParam
     */
    public void setDatesRequestParam(DatesFormParamVO datesRequestParam) {
        this.datesRequestParam = datesRequestParam;
    }

    /**
     * Inits the dates request param.
     * @param localizationManager
     */
    public void initDatesRequestParam(ILocalizationManager localizationManager) {
        this.datesRequestParam = new DatesFormParamVO(localizationManager, DateUtil.DD_MM_YYYY);
    }

    /**
     *
     * @param metadatasMap
     */
    public void addDatestoMap(Map<String, Object> metadatasMap) {
        metadatasMap.put(IntervalDate.class.getSimpleName(), getDatesRequestParam().getPeriods());
    }

    /**
     *
     */
    public void addDate(){
        getDatesRequestParam().addPeriod();
    }

    /**
     *
     * @param index
     */
    public void removeDate(int index){
        getDatesRequestParam().getPeriods().remove(index);
    }
}
