package org.inra.ecoinfo.system;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import static junit.framework.Assert.assertNotNull;
import org.inra.ecoinfo.system.config.impl.ISystemConfiguration;
import org.inra.ecoinfo.system.config.impl.SystemConfiguration;
import org.inra.ecoinfo.utils.ParallelizedRunner;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

/**
 * The Class ConfigurationTest.
 */
@RunWith(ParallelizedRunner.class)
public class AllocatorTest {

    private static Allocator allocator;

    /**
     *
     * @throws BusinessException
     */
    @BeforeClass
    public static void before() throws BusinessException {
        allocator = new Allocator();
        ISystemConfiguration systemConfiguration = new SystemConfiguration();
        systemConfiguration.addMemory("publish", "30");
        systemConfiguration.addMemory("extract", "50");
        allocator.setSystemConfiguration(systemConfiguration);
    }

    /**
     *
     * @return
     */
    @Parameters
    public static Collection<String[]> params() {
        return Arrays.asList(new String[]{"publish", "15", "200"}, new String[]{"extract", "25", "150"}, new String[]{"other", "50", "200"});
    }
    private int numberOfThreads = 100;
    private String memoryName;
    private long ratioMemory = 15;

    /**
     *
     * @param memoryName
     * @param ratioMemory
     * @param numberOfThreads
     */
    public AllocatorTest(String memoryName, String ratioMemory, String numberOfThreads) {
        super();
        this.memoryName = memoryName;
        this.ratioMemory = Long.parseLong(ratioMemory);
        this.numberOfThreads = Integer.parseInt(numberOfThreads);
    }

    /**
     *
     */
    @Test
    public void existAllocatorInstance() {
        assertNotNull(allocator);
    }

    /**
     *
     * @throws InterruptedException
     */
    @Test(timeout = 50_000)
    public void allocateTest() throws InterruptedException {
        Allocator.getInstance().allocate(memoryName, 10_000);
        Allocator.getInstance().allocate(memoryName, 10_000);
        Allocator.getInstance().allocate(memoryName, 100_000);
        Allocator.getInstance().free(memoryName);
    }

    /**
     *
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Test(timeout = 200_000)
    public void releaseTest() throws InterruptedException, ExecutionException {
        List<Long> expectedThreadIds = new LinkedList<>();
        for (long i = 1; i <= numberOfThreads; i++) {
            expectedThreadIds.add(Thread.currentThread().getId() + i);
        }
        Callable<Long> task = new CallableThreads(memoryName);
        List<Callable<Long>> tasks = Collections.nCopies(numberOfThreads, task);
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        List<Future<Long>> futures = executorService.invokeAll(tasks);
        List<Long> resultList = new ArrayList<>(futures.size());
        for (Future<Long> future : futures) {
            resultList.add(future.get());
        }
        assertTrue(resultList.size() == numberOfThreads);
    }

    /**
     *
     * @param memoryName
     */
    public void setMemoryName(String memoryName) {
        this.memoryName = memoryName;
    }

    class CallableThreads implements Callable<Long> {

        final long maxMemory = Runtime.getRuntime().maxMemory() / 15;
        private final String memoryName;

        public CallableThreads(String memoryName) {
            super();
            this.memoryName = memoryName;
        }

        @Override
        public Long call() throws InterruptedException {
            long id = Thread.currentThread().getId();
            Allocator.getInstance().allocate(memoryName, maxMemory);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                return null;
            }
            int runingThread = Allocator.getInstance().getRuningRequestsCount(memoryName);
            assertTrue(runingThread >= 1);
            Allocator.getInstance().getWaitingRequestsCount(memoryName);
            Allocator.getInstance().free(memoryName);
            return id;
        }
    }
}
