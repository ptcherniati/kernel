package org.inra.ecoinfo.system.config.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class ConfigurationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
public class SystemConfigurationTest {

    @Autowired
    SystemConfiguration systemConfiguration;

    /**
     * The InternationalisationConfiguration internationalisation configuration.
     */
    /**
     * Test.
     */
    @Test
    public void test() {
        assertNotNull(systemConfiguration);
        assertTrue(systemConfiguration.getMemory("publish") == 30);
        assertTrue(systemConfiguration.getMemory("extract") == 50);
        assertTrue(systemConfiguration.getMemory(SystemConfiguration.DEFAULT_MEMORY_ALLOCATION) == 20);
        assertThat(systemConfiguration.getMemoryNames()).contains("publish", "extract", "defaultMemory");
    }

    /**
     * @param systemConfiguration
     */
    public void setSystemConfiguration(SystemConfiguration systemConfiguration) {
        this.systemConfiguration = systemConfiguration;
    }
}
