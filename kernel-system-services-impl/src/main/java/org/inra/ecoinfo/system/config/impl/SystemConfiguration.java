package org.inra.ecoinfo.system.config.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.AbstractConfiguration;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class systemConfiguration.
 */
public class SystemConfiguration extends AbstractConfiguration implements ISystemConfiguration {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/systemConfiguration.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "systemConfiguration";

    private final Map<String, Float> memories = new HashMap<>();

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#createConfig(org.apache.commons.digester.Digester)
     */
    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("configuration/module/systemConfiguration/memories/memory", "addMemory", 2);
        digester.addCallParam("configuration/module/systemConfiguration/memories/memory", 0, "name");
        digester.addCallParam("configuration/module/systemConfiguration/memories/memory", 1, "memory");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getModuleName()
     */
    @Override
    public String getModuleName() {
        return SystemConfiguration.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getSchemaPath()
     */
    @Override
    public String getSchemaPath() {
        return SystemConfiguration.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#isAutoLoad()
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @param name
     * @param memory
     * @throws BusinessException
     */
    @Override
    public void addMemory(String name, String memory) throws BusinessException {
        float floatMemory = 0;
        try {
            floatMemory = Float.parseFloat(memory);
        } catch (NumberFormatException e) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error("can't parse float {}", memory, e);
        }
        if (memories.isEmpty()) {
            initMemories();
        }
        if (memories.containsKey(name)) {
            memories.put(DEFAULT_MEMORY_ALLOCATION, memories.get(DEFAULT_MEMORY_ALLOCATION) + memories.get(name));
        }
        if (memories.get(DEFAULT_MEMORY_ALLOCATION) < floatMemory) {
            throw new BusinessException("You ask more memory than there are");
        }
        memories.put(DEFAULT_MEMORY_ALLOCATION, memories.get(DEFAULT_MEMORY_ALLOCATION) - floatMemory);
        memories.put(name, floatMemory);
    }

    /**
     *
     * @param name
     * @return
     */
    @Override
public float getMemory(String name) {
    return memories.get(name);
}

    /**
     *
     * @return
     */
    @Override
    public List<String> getMemoryNames() {
        return new LinkedList<>(memories.keySet());
    }
    
    private void initMemories() {
        if (memories.get(DEFAULT_MEMORY_ALLOCATION) == null) {
            memories.put(DEFAULT_MEMORY_ALLOCATION, (float) 100);
        }
        try {
            addMemory(ISystemConfiguration.EXTRACT_MEMORY_ALLOCATION, "50");
            addMemory(ISystemConfiguration.PUBLISH_MEMORY_ALLOCATION, "30");
        } catch (BusinessException e) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error("can't init memory for default values", e);
        }
        
    }
}
