/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.*;
import java.util.Properties;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class TestConnexion {

    private static final String userName = "monsoereusertest";
    private static final String password = "mst001";

    /**
     *
     * @throws SQLException
     */
    @Test
    public void testJDBCConnexion() throws SQLException {
        Properties connectionProps = new Properties();
        connectionProps.put("user", this.userName);
        connectionProps.put("password", this.password);
        Connection conn = DriverManager.getConnection("jdbc:postgresql://147.100.179.39:5437/monsoeretest_kernel", connectionProps);
        Statement stmt = null;
        String query = "select login from utilisateur";
        stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            String login = rs.getString("login");
        }
        Assert.assertTrue(conn.isValid(20_000));
    }
}
