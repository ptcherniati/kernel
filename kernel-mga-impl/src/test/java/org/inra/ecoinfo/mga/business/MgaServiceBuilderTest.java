/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business;

import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * @author tcherniatinsky
 */
public class MgaServiceBuilderTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MgaServiceBuilder instance;
    @Mock
    IMgaBuilder mgaBuilder;
    @Mock
    IMgaIOConfigurator mgaIOConfigurator;
    @Mock
    IMgaRecorder recorder;
    @Mock
    FactoryNode factoryNode;
    /**
     *
     */
    public MgaServiceBuilderTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new MgaServiceBuilder();
        instance.setMgaIOConfigurator(mgaIOConfigurator);
        instance.setMgaBuilder(mgaBuilder);
        instance.setRecorder(recorder);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

}
