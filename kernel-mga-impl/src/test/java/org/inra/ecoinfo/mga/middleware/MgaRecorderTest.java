package org.inra.ecoinfo.mga.middleware;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.IExtractActivity;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
@Ignore
public class MgaRecorderTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }


    @Mock
    ICompositeGroup group;

    /**
     *
     */
    public MgaRecorderTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of persist method, of class MgaRecorder.
     */
    @Test
    public void testpersist_INode() {
        INode node = null;
        MgaRecorder instance = new MgaRecorder();
        instance.persist(node);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of persist method, of class MgaRecorder.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testpersist_INodeable() throws PersistenceException {
        INodeable nodeable = null;
        MgaRecorder instance = new MgaRecorder();
        instance.saveOrUpdate(nodeable);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of persist method, of class MgaRecorder.
     */
    @Test
    public void testPersist() {
        IExtractActivity extractActivity = null;
        MgaRecorder instance = new MgaRecorder();
        instance.persist(extractActivity);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of merge method, of class MgaRecorder.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testMerge_INodeable() throws PersistenceException {
        INodeable nodeable = null;
        MgaRecorder instance = new MgaRecorder();
        instance.merge(nodeable);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of merge method, of class MgaRecorder.
     */
    @Test
    public void testMerge_INode() {
        INode node = null;
        MgaRecorder instance = new MgaRecorder();
        INode expResult = null;
        INode result = instance.merge(node);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of remove method, of class MgaRecorder.
     */
    @Test
    public void testRemove() {
        INode node = null;
        MgaRecorder instance = new MgaRecorder();
        instance.remove(node);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removeExtractActivity method, of class MgaRecorder.
     */
    @Test
    public void testRemoveExtractActivity() {
        String login = "";
        MgaRecorder instance = new MgaRecorder();
        instance.removeExtractActivity(login);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEntityManager method, of class MgaRecorder.
     */
    @Test
    public void testgetEntityManager() {
        MgaRecorder instance = new MgaRecorder();
        EntityManager expResult = null;
        EntityManager result = instance.getEntityManager();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRole method, of class MgaRecorder.
     */
    @Test
    public void testGetRole() {
        String name = "";
        MgaRecorder instance = new MgaRecorder();
        ICompositeActivity expResult = null;
        Stream<Group> result = instance.getGroups(group, WhichTree.TREEDATASET);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of mergeRole method, of class MgaRecorder.
     */
    @Test
    public void testMergeRole() {
        ICompositeActivity role = null;
        MgaRecorder instance = new MgaRecorder();
        instance.mergeActivity(role);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of persistRole method, of class MgaRecorder.
     */
    @Test
    public void testPersistRole() {
        ICompositeActivity role = null;
        MgaRecorder instance = new MgaRecorder();
        instance.persistActivity(role);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of existNodeAccordingTypeResource method, of class MgaRecorder.
     */
    @Test
    public void testExistNodeAccordingTypeResource() {
        WhichTree whichTree = null;
        Class<? extends INodeable> type = null;
        MgaRecorder instance = new MgaRecorder();
        boolean expResult = false;
        boolean result = instance.existNodeAccordingTypeResource(whichTree, type);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNodeables method, of class MgaRecorder.
     */
    @Test
    public void testGetNodeables_0args() {
        MgaRecorder instance = new MgaRecorder();
        List<INodeable> expResult = null;
        Stream<INodeable> result = instance.getNodeables();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNodeables method, of class MgaRecorder.
     */
    @Test
    public void testGetNodeables_TypeResource() {
        Class<? extends INodeable> type = null;
        MgaRecorder instance = new MgaRecorder();
        List<INodeable> expResult = null;
        Stream<? extends INodeable> result = instance.getNodeables(type);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of retrieveAllUsers method, of class MgaRecorder.
     */
    @Test
    public void testretrieveAllUsers() {
        MgaRecorder instance = new MgaRecorder();

    }

    /**
     * Test of setUserDao method, of class MgaRecorder.
     */
    @Test
    public void testSetUserDao() {
        ICompositeGroupDAO userDao = null;
        Integer codeConfiguration = 0;
        MgaRecorder instance = new MgaRecorder();
        instance.setUserDao(userDao, codeConfiguration);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of persist method, of class MgaRecorder.
     */
    @Test
    public void testPersist_INode() {
        INode node = null;
        MgaRecorder instance = new MgaRecorder();
        instance.persist(node);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of persist method, of class MgaRecorder.
     */
    @Test
    public void testPersist_IExtractActivity() {
        IExtractActivity extractActivity = null;
        MgaRecorder instance = new MgaRecorder();
        instance.persist(extractActivity);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of merge method, of class MgaRecorder.
     */
    @Test
    public void testMerge() {
        INode node = null;
        MgaRecorder instance = new MgaRecorder();
        INode expResult = null;
        INode result = instance.merge(node);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEntityManager method, of class MgaRecorder.
     */
    @Test
    public void testGetEntityManager() {
        MgaRecorder instance = new MgaRecorder();
        EntityManager expResult = null;
        EntityManager result = instance.getEntityManager();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGroups method, of class MgaRecorder.
     */
    @Test
    public void testGetGroups() {
        ICompositeGroup compositeGroup = null;
        WhichTree whichTree = null;
        MgaRecorder instance = new MgaRecorder();
        Stream<Group> expResult = null;
        Stream<Group> result = instance.getGroups(compositeGroup, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createGroup method, of class MgaRecorder.
     */
    @Test
    public void testCreateGroup() {
        String groupName = "";
        WhichTree whichTree = null;
        MgaRecorder instance = new MgaRecorder();
        Group expResult = null;
        Group result = instance.createGroup(groupName, whichTree, GroupType.SIMPLE_GROUP);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of mergeActivity method, of class MgaRecorder.
     */
    @Test
    public void testMergeActivity() {
        ICompositeActivity activity = null;
        MgaRecorder instance = new MgaRecorder();
        instance.mergeActivity(activity);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of persistActivity method, of class MgaRecorder.
     */
    @Test
    public void testPersistActivity() {
        ICompositeActivity activity = null;
        MgaRecorder instance = new MgaRecorder();
        instance.persistActivity(activity);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNodes method, of class MgaRecorder.
     */
    @Test
    public void testGetNodes() {
        Function<CriteriaBuilder, CriteriaQuery<INode>> query = null;
        MgaRecorder instance = new MgaRecorder();
        Stream<INode> expResult = null;
        Stream<INode> result = instance.getNodes(query);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of emptyQueryResult method, of class MgaRecorder.
     */
    @Test
    public void testEmptyQueryResult() {
        Function<CriteriaBuilder, CriteriaQuery<INode>> query = null;
        MgaRecorder instance = new MgaRecorder();
        boolean expResult = false;
        boolean result = instance.emptyQueryResult(query);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNodeables method, of class MgaRecorder.
     */
    @Test
    public void testGetNodeables_Class() {
        MgaRecorder instance = new MgaRecorder();
        Stream<INodeable> expResult = null;
        Stream<? extends INodeable> result = instance.getNodeables(null);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNodeByNodeableCodeName method, of class MgaRecorder.
     */
    @Test
    public void testGetNodeByNodeableCodeName() {
        String codeName = "";
        WhichTree whichTree = null;
        MgaRecorder instance = new MgaRecorder();
        Optional<INode> expResult = null;
        Optional<INode> result = instance.getNodeByNodeableCodeName(codeName, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNodesByNodeableCodeName method, of class MgaRecorder.
     */
    @Test
    public void testGetNodesByNodeableCodeName() {
        WhichTree whichTree = null;
        String codeName = "";
        MgaRecorder instance = new MgaRecorder();
        Stream<INode> expResult = null;
        Stream<INode> result = instance.getNodesByNodeableCodeName(whichTree, codeName);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNodesByIds method, of class MgaRecorder.
     */
    @Test
    public void testGetNodesByIds() {
        List<Long> nodeIds = null;
        WhichTree whichTree = null;
        MgaRecorder instance = new MgaRecorder();
        Stream<INode> expResult = null;
        Stream<INode> result = instance.getNodesByIds(nodeIds, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNodeByNodeableCode method, of class MgaRecorder.
     */
    @Test
    public void testGetNodeByNodeableCode() {
        String code = "";
        WhichTree whichTree = null;
        MgaRecorder instance = new MgaRecorder();
        Optional<INode> expResult = null;
        Optional<INode> result = instance.getNodeByNodeableCode(code, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDbNodes method, of class MgaRecorder.
     */
    @Test
    public void testGetDbNodes() {
        MgaRecorder instance = new MgaRecorder();
        Map<String, RealNode> expResult = null;
        Map<String, RealNode> result = instance.getDbNodes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removeStickyLeaves method, of class MgaRecorder.
     */
    @Test
    public void testRemoveStickyLeaves() {
        AbstractBranchNode parent = null;
        WhichTree whichTree = null;
        MgaRecorder instance = new MgaRecorder();
        instance.removeStickyLeaves(parent, whichTree);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of saveOrUpdate method, of class MgaRecorder.
     */
    @Test
    public void testSaveOrUpdate_RealNode() {
        RealNode realNode = null;
        MgaRecorder instance = new MgaRecorder();
        instance.saveOrUpdate(realNode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRealNodeByNKey method, of class MgaRecorder.
     */
    @Test
    public void testGetRealNodeByNKey() {
        String path = "";
        MgaRecorder instance = new MgaRecorder();
        Optional<RealNode> expResult = null;
        Optional<RealNode> result = instance.getRealNodeByNKey(path);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRealNodeById method, of class MgaRecorder.
     */
    @Test
    public void testGetRealNodeById() {
        Long realNodeId = null;
        MgaRecorder instance = new MgaRecorder();
        Optional<RealNode> expResult = null;
        Optional<RealNode> result = instance.getRealNodeById(realNodeId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllRealNodes method, of class MgaRecorder.
     */
    @Test
    public void testGetAllRealNodes() {
        MgaRecorder instance = new MgaRecorder();
        Stream<RealNode> expResult = null;
        Stream<RealNode> result = instance.getAllRealNodes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMissingParentsNodes method, of class MgaRecorder.
     */
    @Test
    public void testGetMissingParentsNodes() {
        String login = "";
        MgaRecorder instance = new MgaRecorder();
        Stream<ExtractActivity> expResult = null;
        Stream<ExtractActivity> result = instance.getMissingParentsNodes(login);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPublicGroup method, of class MgaRecorder.
     */
    @Test
    public void testGetPublicGroup() {
        MgaRecorder instance = new MgaRecorder();
        Optional<Group> expResult = null;
        Optional<Group> result = instance.getPublicGroup();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllRoles method, of class MgaRecorder.
     */
    @Test
    public void testGetAllRoles() {
        IUser user = null;
        WhichTree whichTree = null;
        MgaRecorder instance = new MgaRecorder();
        Stream<Group> expResult = null;
        Stream<Group> result = instance.getAllRoles(user, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSingleRole method, of class MgaRecorder.
     */
    @Test
    public void testGetSingleRole() {
        Group compositeGroup = null;
        WhichTree whichTree = null;
        MgaRecorder instance = new MgaRecorder();
        Stream<Group> expResult = null;
        Stream<Group> result = instance.getSingleRole(compositeGroup, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of saveOrUpdate method, of class MgaRecorder.
     */
    @Test
    public void testSaveOrUpdate_Group() {
        Group group = null;
        MgaRecorder instance = new MgaRecorder();
        instance.saveOrUpdate(group);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGroupByGroupNameAndWhichTree method, of class MgaRecorder.
     */
    @Test
    public void testGetGroupByGroupNameAndWhichTree() {
        String groupName = "";
        WhichTree whichTree = null;
        MgaRecorder instance = new MgaRecorder();
        Optional<Group> expResult = null;
        Optional<Group> result = instance.getGroupByGroupNameAndWhichTree(groupName, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCompositeGroupDAO method, of class MgaRecorder.
     */
    @Test
    public void testGetCompositeGroupDAO() {
        Integer codeConfiguration = 0;
        MgaRecorder instance = new MgaRecorder();
        ICompositeGroupDAO expResult = null;
        ICompositeGroupDAO result = instance.getCompositeGroupDAO(codeConfiguration);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
