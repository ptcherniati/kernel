/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * @author tcherniatinsky
 */
public class MgaIOConfigurationTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public Integer codeConfiguration = 2;
    /**
     *
     */
    public Integer[] entryOrder = new Integer[]{3, codeConfiguration, 1};
    /**
     *
     */
    public Class<INodeable>[] entryInstance = new Class[]{Site.class, Theme.class, Datatype.class};
    /**
     *
     */
    public Integer[] sortOrder = new Integer[]{codeConfiguration, 1, 3};
    /**
     *
     */
    public Activities[] activities = new Activities[]{Activities.administration, Activities.associate};
    /**
     *
     */
    public Class<Datatype> query = Datatype.class;
    /**
     *
     */
    public boolean includeAncestor = true;
    /**
     *
     */
    public WhichTree whichTree = WhichTree.TREEDATASET;
    /**
     *
     */
    public boolean displayColumsNames = true;
    /**
     *
     */
    public Class<VariableMonSoere> stickyLeafInstance = VariableMonSoere.class;
    MgaIOConfiguration instance;

    /**
     *
     */
    public MgaIOConfigurationTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        instance = new MgaIOConfiguration(
                codeConfiguration,
                query,
                entryOrder,
                entryInstance,
                activities,
                sortOrder,
                includeAncestor,
                whichTree,
                stickyLeafInstance,
                displayColumsNames);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getEntryOrder method, of class MgaIOConfiguration.
     */
    @Test
    public void testGetEntryOrder() {
        assertArrayEquals(entryOrder, instance.getEntryOrder());
    }

    /**
     * Test of getSortOrder method, of class MgaIOConfiguration.
     */
    @Test
    public void testGetSortOrder() {
        assertArrayEquals(sortOrder, instance.getSortOrder());
    }

    /**
     * Test of getEntryInstance method, of class MgaIOConfiguration.
     */
    @Test
    public void testGetEntryInstance() {
        assertArrayEquals(entryInstance, instance.getEntryType());
    }

    /**
     * Test of getQuery method, of class MgaIOConfiguration.
     */
    @Test
    public void testGetQuery() {
        assertEquals(query, instance.getLeafType());
    }

    /**
     * Test of getCodeConfig method, of class MgaIOConfiguration.
     */
    @Test
    public void testGetCodeConfig() {
        assertTrue(codeConfiguration == instance.getCodeConfiguration());
    }

    /**
     * Test of getActivities method, of class MgaIOConfiguration.
     */
    @Test
    public void testGetActivities() {
        assertArrayEquals(activities, instance.getActivities());
    }

    /**
     * Test of isIncludeAncestor method, of class MgaIOConfiguration.
     */
    @Test
    public void testIsIncludeAncestor() {
        assertTrue(instance.isIncludeAncestor());
    }

    /**
     * Test of getWhichTree method, of class MgaIOConfiguration.
     */
    @Test
    public void testGetWhichTree() {
        assertEquals(whichTree, instance.getWhichTree());
    }

    /**
     * Test of getStickyLeafInstance method, of class MgaIOConfiguration.
     */
    @Test
    public void testGetStickyLeafInstance() {
        Class<? extends INodeable> result = instance.getStickyLeafType();
        assertEquals(stickyLeafInstance, result);
    }

    /**
     * Test of displayColumnNames method, of class MgaIOConfiguration.
     */
    @Test
    public void testDisplayColumnNames() {
        assertTrue(instance.displayColumnNames());
    }

    /**
     *
     */
    public class MgaIOConfiguration extends AbstractMgaIOConfiguration {

        /**
         * @param codeConfiguration
         * @param codeConfig
         * @param leafType
         * @param entryOrder
         * @param entryTypes
         * @param activities
         * @param sortOrder
         * @param includeAncestor
         * @param whichTree
         * @param stickyLeafType
         * @param displayColumnNames
         */
        public MgaIOConfiguration(Integer codeConfiguration, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfiguration, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

    }

    /**
     *
     */
    public class VariableMonSoere extends Nodeable {

        /**
         *
         * @return
         */
        @Override
        public String getName() {
            return getClass().getSimpleName();
        }

        /**
         *
         * @param <T>
         * @return
         */
        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) getClass();
        }

    }

    /**
     *
     */
    public class Site extends Nodeable {

        /**
         *
         * @return
         */
        @Override
        public String getName() {
            return getClass().getSimpleName();
        }

        /**
         *
         * @param <T>
         * @return
         */
        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) getClass();
        }

    }

    /**
     *
     */
    public class Theme extends Nodeable {

        /**
         *
         * @return
         */
        @Override
        public String getName() {
            return getClass().getSimpleName();
        }

        /**
         *
         * @param <T>
         * @return
         */
        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) getClass();
        }

    }

    /**
     *
     */
    public class Datatype extends Nodeable {

        /**
         *
         * @return
         */
        @Override
        public String getName() {
            return getClass().getSimpleName();
        }

        /**
         *
         * @param <T>
         * @return
         */
        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) getClass();
        }

    }
}
