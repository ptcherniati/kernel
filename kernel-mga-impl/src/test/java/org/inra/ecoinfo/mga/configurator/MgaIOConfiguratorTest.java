/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.configurator;

import java.util.Optional;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
@Ignore
public class MgaIOConfiguratorTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MgaIOConfigurator instance;

    /**
     *
     */
    public MgaIOConfiguratorTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new MgaIOConfigurator();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getConfiguration method, of class MgaIOConfigurator.
     */
    @Test
    public void testGetConfiguration() {
        Optional<IMgaIOConfiguration> result = instance.getConfiguration(0);
        assertEquals(new Integer(0), result.get().getCodeConfiguration());
    }

    /**
     * Test of getDeepestIndexNode method, of class MgaIOConfigurator.
     */
    @Test
    public void testGetDeepestIndexNode() {
        instance.setDeepestIndexNode(4);
        int result = instance.getDeepestIndexNode();
        assertEquals(4, result);
    }

    /**
     * Test of getColumnsTreeNames method, of class MgaIOConfigurator.
     */
    @Test
    public void testGetColumnsTreeNames() {
//        instance.columnsTreeNames.put(1, "b");
//        instance.columnsTreeNames.put(2, "c");
//        instance.columnsTreeNames.put(0, "a");
//        List<String> result = instance.getColumnsTreeNames();
//        assertEquals("a",result.get(0));
//        assertEquals("b",result.get(1));
//        assertEquals("c",result.get(2));
    }

    /**
     * Test of addColumn method, of class MgaIOConfigurator.
     */
    @Test
    public void testAddColumn() {
//        String columnName = "";
//        int index = 0;
//        MgaDisplayerConfiguration displayerConfiguration = mock(MgaDisplayerConfiguration.class);
//        Map<TypeResource, String[]> displayConfiguration = mock(Map.class);
//        when(displayerConfiguration.getDisplayColumnNames()).thenReturn(displayConfiguration);
//        doReturn(new String[]{"1","2","3"}).when(displayConfiguration).get(DataType.class);
//        instance.addColumn("DataType", 2);
//        assertTrue(instance.getColumnsTreeNames().contains("PROPERTY_COLUMN_DATATYPE_NAME"));
//        try {
//            instance.addColumn("Choux", 10);
//            fail();
//        } catch (IllegalArgumentException e) {
//            assertEquals("Illegal typeResource : Choux", e.getMessage());
//        }
    }

    /**
     * Test of clearColumns method, of class MgaIOConfigurator.
     */
    @Test
    public void testClearColumns() {
//        instance.addColumn("DataType", 2);
//        assertFalse(instance.getColumnsTreeNames().isEmpty());
//        instance.clearColumns();
//        assertTrue(instance.getColumnsTreeNames().isEmpty());
    }

    /**
     * Test of getConfigurations method, of class MgaIOConfigurator.
     */
    @Test
    public void testGetConfigurations() {
        assertTrue(instance.getConfigurations().size() == 6);
    }

    class MgaIOConfigurator extends AbstractMgaIOConfigurator {

        public MgaIOConfigurator() {
            super(new AbstractMgaDisplayerConfiguration() {
            });
        }

        @Override
        protected void initConfigurations() {
        }


    }

    class MgaIOConfiguration extends AbstractMgaIOConfiguration {

        public MgaIOConfiguration(Integer codeConfiguration, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfiguration, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

    }
}
