/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business;

import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class FactoryNodeTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    FactoryNode instance;

    /**
     *
     */
    public FactoryNodeTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        instance = new FactoryNode();
        MockitoAnnotations.openMocks(this);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of createTransientNode method, of class FactoryNode.
     */
    @Test
    public void testCreateTransientNode() {
        Nodeable nodeable = mock(Nodeable.class);
        RealNode parentRealNode = new RealNode(null, null, nodeable, "");
        RealNode ancestorRealNode = new RealNode(null, null, nodeable, "");
        RealNode realNode = new RealNode(parentRealNode, ancestorRealNode, nodeable, "");
        AbstractBranchNode parentNode = new NodeDataSet(null, null);
        parentNode.setRealNode(parentRealNode);
        AbstractBranchNode ancestorNode = new NodeDataSet(null, null);
        ancestorNode.setRealNode(ancestorRealNode);
        AbstractBranchNode result = FactoryNode.createTransientNode(WhichTree.TREEDATASET, realNode, parentNode, ancestorNode);
        assertEquals(nodeable, result.getNodeable());
        assertTrue(result instanceof NodeDataSet);
        assertEquals(realNode, result.getRealNode());
        assertEquals(parentNode, result.getParent());
        assertEquals(ancestorNode, result.getAncestor());
        assertEquals(1, result.getIndex());
        assertEquals(ancestorRealNode, result.getAncestor().getRealNode());
        assertEquals(parentRealNode, result.getParent().getRealNode());
        parentNode = new NodeRefData(null, null);
        parentNode.setRealNode(parentRealNode);
        ancestorNode = new NodeRefData(null, null);
        ancestorNode.setRealNode(ancestorRealNode);
        result = FactoryNode.createTransientNode(WhichTree.TREEREFDATA, realNode, parentNode, ancestorNode);
        assertEquals(nodeable, result.getNodeable());
        assertTrue(result instanceof NodeRefData);
    }

}
