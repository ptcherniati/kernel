/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.caching.ITreeSessionCacheManager;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.configurator.MgaIOConfigurationTest;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.ActivityCondition;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.mga.viewadapter.ITreeBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.*;
import static org.junit.Assert.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class PolicyManagerTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    PolicyManager instance;
    @Mock(name = "mgaServiceBuilder")
    IMgaServiceBuilder mgaServiceBuilder;
    @Mock(name = "treeBuilder")
    ITreeBuilder treeBuilder;
    @Mock(name = "mgaIOConfiguration")
    IMgaIOConfiguration mgaIOConfiguration;
    @Mock(name = "mgaIOConfigurator")
    IMgaIOConfigurator mgaIOConfigurator;
    @Mock(name = "recorder")
    IMgaRecorder recorder;
    @Mock(name = "cacheManager")
    ITreeSessionCacheManager cacheManager;
    @Mock(name = "em")
    EntityManager em;
    @Mock(name = "user")
    IUser user;
    @Mock(name = "group")
    Group group;
    @Mock(name = "group1")
    Group group1;
    @Mock(name = "group2")
    Group group2;
    Stream<Group> dbs;
    Map<Activities, Map<Long, List<LocalDate>>> activitiesMap1 = new HashMap<>();
    Map<Activities, Map<Long, List<LocalDate>>> activitiesMap2 = new HashMap<>();
    Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role = new HashMap<>();
    @Mock(name = "treeActivities")
    ITreeNode<IFlatNode> treeActivities;
    @Mock(name = "data")
    IFlatNode data;
    @Mock(name = "data1")
    IFlatNode data1;
    @Mock(name = "data2")
    IFlatNode data2;
    @Mock(name = "child1")
    ITreeNode<IFlatNode> child1;
    @Mock(name = "child2")
    ITreeNode<IFlatNode> child2;
    @Mock(name = "otherChild")
    ITreeNode<IFlatNode> otherChild;
    @Mock(name = "child")
    ITreeNode<IFlatNode> child;
    List<ITreeNode<IFlatNode>> children = new LinkedList();

    /**
     *
     */
    public PolicyManagerTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new PolicyManager();
        instance.settreeBuilder(treeBuilder);
        when(treeBuilder.getMgaServiceBuilder()).thenReturn(mgaServiceBuilder);
        when(mgaServiceBuilder.getRecorder()).thenReturn(recorder);
        when(mgaServiceBuilder.getMgaIOConfigurator()).thenReturn(mgaIOConfigurator);
        when(mgaIOConfigurator.getConfiguration(1)).thenReturn(Optional.ofNullable(mgaIOConfiguration));
        doReturn(WhichTree.TREEDATASET).when(mgaIOConfiguration).getWhichTree();
        when(treeBuilder.getSessionCacheManager()).thenReturn(cacheManager);
        children.add(child1);
        children.add(child2);
        doReturn(children).when(treeActivities).getChildren();
        doReturn(em).when(recorder).getEntityManager();
        doNothing().when(em).flush();
        initDbs();
    }

    /**
     * <ul>
     * <li> administration</li>
     * <ul>
     * <li>2L</li>
     * <ul>
     * <li>group1 -- 2010-03-21 2012-01-16</li>
     * <li>group2 -- 2010-03-21 2012-01-16</li>
     * </ul>
     * <li>3L</li>
     * <ul>
     * <li>group1 -- 2010-03-21 2012-01-16</li>
     * <li>group2 -- 1985-07-07 2001-09-08</li>
     * </ul>
     * <li>4L</li>
     * <ul>
     * <li>group1 -- 1985-07-07 2001-09-08</li>
     * </ul>
     * </ul>
     * </ul>
     * <ul>
     * <li>depot</li>
     * <ul>
     * <li>3</li>
     * <ul>
     * <li>group1 -- 1985-07-07 2001-09-08</li>
     * </ul>
     * </ul>
     * </ul>
     */
    private void initDbs() {
        LinkedList<Group> linkedList = new LinkedList<>();
        linkedList.add(group1);
        linkedList.add(group2);
        dbs = linkedList.stream();
        when(group1.getActivitiesMap()).thenReturn(activitiesMap1);
        when(group2.getActivitiesMap()).thenReturn(activitiesMap2);
        addToActivitiesMap(activitiesMap1, Activities.administration, 2L, LocalDate.of(2010, Month.MARCH, 21), LocalDate.of(2012, Month.JANUARY, 16));
        addToActivitiesMap(activitiesMap1, Activities.administration, 3L, LocalDate.of(2010, Month.MARCH, 21), LocalDate.of(2012, Month.JANUARY, 16));
        addToActivitiesMap(activitiesMap1, Activities.depot, 3L, LocalDate.of(1985, Month.JULY, 7), LocalDate.of(2001, Month.SEPTEMBER, 8));
        addToActivitiesMap(activitiesMap2, Activities.administration, 2L, LocalDate.of(2010, Month.MARCH, 21), LocalDate.of(2012, Month.JANUARY, 16));
        addToActivitiesMap(activitiesMap2, Activities.administration, 3L, LocalDate.of(1985, Month.JULY, 7), LocalDate.of(2001, Month.SEPTEMBER, 8));
        addToActivitiesMap(activitiesMap2, Activities.administration, 4L, LocalDate.of(1985, Month.JULY, 7), LocalDate.of(2001, Month.SEPTEMBER, 8));
    }

    private void addToActivitiesMap(Map<Activities, Map<Long, List<LocalDate>>> activitiesMap, Activities activities, long nodeId, LocalDate from, LocalDate to) {
        List<LocalDate> dates = Arrays.asList(new LocalDate[]{from, to});
        activitiesMap
                .computeIfAbsent(activities, k -> new HashMap<>())
                .computeIfAbsent(nodeId, k -> dates);
        role
                .computeIfAbsent(activities, k -> new HashMap<>())
                .computeIfAbsent(nodeId, k -> new HashMap<>())
                .put(group, dates);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of isActivityOrActivitiesList method, of class PolicyManager.
     */
    @Test
    public void testIsActivityOrActivitiesList() {
        assertFalse(instance.isActivityOrActivitiesList(null));
        assertFalse(instance.isActivityOrActivitiesList(""));
        assertFalse(instance.isActivityOrActivitiesList("priv"));
        assertFalse(instance.isActivityOrActivitiesList("depot,priv"));
        assertTrue(instance.isActivityOrActivitiesList("depot,extraction"));
    }

    /**
     * Test of isActivity method, of class PolicyManager.
     */
    @Test
    public void testIsActivity() {
        assertFalse(instance.isActivity(""));
        assertFalse(instance.isActivity("priv"));
        assertTrue(instance.isActivity("depot"));
    }

    /**
     * Test of settreeBuilder method, of class PolicyManager.
     */
    @Test
    public void testSettreeBuilder() {
        ITreeBuilder treeBuilder = mock(ITreeBuilder.class);
        instance.settreeBuilder(treeBuilder);
        assertEquals(treeBuilder, instance.treeBuilder);
    }

    /**
     * Test of removeTreeFromSession method, of class PolicyManager.
     */
    @Test
    public void testRemoveTreeFromSession() {
        instance.removeTreeFromSession(group, 1);
        verify(treeBuilder).removeTreeFromSession(group, 1);
    }

    /**
     * Test of loads method, of class PolicyManager.
     */
    @Test
    public void testgetOrLoadActivities() {
        instance = spy(instance);
        when(recorder.getGroups(group, WhichTree.TREEDATASET)).thenReturn(dbs);
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> result = instance.getOrLoadActivities(group, WhichTree.TREEDATASET);
        assertTrue(result.size() == 2);
        Map<Long, Map<Group, List<LocalDate>>> admin = result.get(Activities.administration);
        assertTrue(admin.size() == 3);
        assertEquals("[2010-03-21, 2012-01-16]", admin.get(2L).get(group1).toString());
        assertEquals("[2010-03-21, 2012-01-16]", admin.get(2L).get(group2).toString());
        assertEquals("[2010-03-21, 2012-01-16]", admin.get(3L).get(group1).toString());
        assertEquals("[1985-07-07, 2001-09-08]", admin.get(3L).get(group2).toString());
        assertEquals("{group2=[1985-07-07, 2001-09-08]}", admin.get(4L).toString());
        assertEquals("{3={group1=[1985-07-07, 2001-09-08]}}", result.get(Activities.depot).toString());
    }

    /**
     * Test of checkActivity method, of class PolicyManager.
     */
    @Test
    public void testCheckActivity_4args() {
        instance = spy(instance);
        final String activityString = "depot";
        final String expression = "expression";
        boolean result = instance.checkActivity(group, 1, activityString, expression);
        //null user
        assertFalse(result);
        verify(instance, never()).isExpression(expression);
        //is root
        doReturn("login").when(instance).getCurrentUserLogin();
        when(instance.isRoot()).thenReturn(true, Boolean.FALSE);
        assertTrue(instance.checkActivity(group, 1, activityString, expression));
        verify(instance, never()).isExpression(expression);

        //not an expression
        doReturn(false).when(instance).isExpression(expression);
        doReturn(true).when(instance).checkActivityWithNotExpression(activityString, group, 1);
        assertTrue(instance.checkActivity(group, 1, activityString, expression));
        verify(instance, times(1)).isExpression(expression);
        doReturn(false).when(instance).checkActivityWithNotExpression(activityString, group, 1);
        assertFalse(instance.checkActivity(group, 1, activityString, expression));
        verify(instance, times(2)).isExpression(expression);

        //is an axpression
        doReturn(true).when(instance).isExpression(expression);
        doReturn(treeActivities).when(instance).getOrLoadTree(group, 1);
        doReturn(false).when(instance).decodeExpression(group, expression, activityString, child1, WhichTree.TREEDATASET);
        doReturn(false).when(instance).decodeExpression(group, expression, activityString, child2, WhichTree.TREEDATASET);
        assertFalse(instance.checkActivity(group, 1, activityString, expression));
        verify(instance, times(1)).decodeExpression(group, expression, activityString, child1, WhichTree.TREEDATASET);
        verify(instance, times(1)).decodeExpression(group, expression, activityString, child2, WhichTree.TREEDATASET);
        doReturn(true).when(instance).decodeExpression(group, expression, activityString, child2, WhichTree.TREEDATASET);
        assertTrue(instance.checkActivity(group, 1, activityString, expression));
        verify(instance, times(2)).decodeExpression(group, expression, activityString, child1, WhichTree.TREEDATASET);
        verify(instance, times(2)).decodeExpression(group, expression, activityString, child2, WhichTree.TREEDATASET);
        doReturn(true).when(instance).decodeExpression(group, expression, activityString, child1, WhichTree.TREEDATASET);
        doReturn(false).when(instance).decodeExpression(group, expression, activityString, child2, WhichTree.TREEDATASET);
        assertTrue(instance.checkActivity(group, 1, activityString, expression));
        verify(instance, times(3)).decodeExpression(group, expression, activityString, child1, WhichTree.TREEDATASET);
        verify(instance, times(2)).decodeExpression(group, expression, activityString, child2, WhichTree.TREEDATASET);

        //n oData
        doReturn(data).when(treeActivities).getData();
        doReturn(false).when(instance).decodeExpression(group, expression, activityString, treeActivities, WhichTree.TREEDATASET);
        assertFalse(instance.checkActivity(group, 1, activityString, expression));
        verify(instance, times(1)).decodeExpression(group, expression, activityString, treeActivities, WhichTree.TREEDATASET);
        doReturn(true).when(instance).decodeExpression(group, expression, activityString, treeActivities, WhichTree.TREEDATASET);
        assertTrue(instance.checkActivity(group, 1, activityString, expression));
        verify(instance, times(2)).decodeExpression(group, expression, activityString, treeActivities, WhichTree.TREEDATASET);

    }

    /**
     * Test of getOrLoadTree method, of class PolicyManager.
     */
    @Test
    public void testGetOrLoadTree() {
        instance = spy(instance);
        doReturn(role).when(instance).getOrLoadActivities(group, WhichTree.TREEDATASET);
        //null User
        doReturn(treeActivities).when(treeBuilder).getOrLoadTree(Optional.ofNullable(mgaIOConfiguration), role, group);
        ITreeNode<IFlatNode> result = instance.getOrLoadTree(group, 1);
        assertEquals(treeActivities, result);
        verify(treeBuilder, times(1)).getOrLoadTree(Optional.ofNullable(mgaIOConfiguration), role, group);
        //not root user
        instance.setCurrentUser(user);
        result = instance.getOrLoadTree(group, 1);
        assertEquals(treeActivities, result);
        verify(treeBuilder, times(2)).getOrLoadTree(Optional.ofNullable(mgaIOConfiguration), role, group);
        //root user
        doReturn(true).when(user).getIsRoot();
        doReturn(treeActivities).when(treeBuilder).getOrLoadTree(Optional.ofNullable(mgaIOConfiguration), role, group);
        result = instance.getOrLoadTree(group, 1);
        assertEquals(treeActivities, result);
        verify(treeBuilder, times(3)).getOrLoadTree(Optional.ofNullable(mgaIOConfiguration), role, group);
    }

    /**
     * Test of checkActivityWithNotExpression method, of class PolicyManager.
     */
    @Test
    public void testCheckActivityWithNotExpression() {
        final String activityString = "depot";
        final String expression = "expression";
        boolean result;
        instance = spy(instance);
        //null priv
        assertTrue(instance.checkActivityWithNotExpression(null, group, 1));
        //empty priv
        assertTrue(instance.checkActivityWithNotExpression("", group, 1));
        //* priv
        //empty roles
        doReturn(new HashMap()).when(instance).getOrLoadActivities(group, WhichTree.TREEDATASET);
        assertFalse(instance.checkActivityWithNotExpression("  * ", group, 1));
        //NotEmptys 
        doReturn(role).when(instance).getOrLoadActivities(group, WhichTree.TREEDATASET);
        assertTrue(instance.checkActivityWithNotExpression("  * ", group, 1));
        verify(instance, times(2)).getOrLoadActivities(group, WhichTree.TREEDATASET);
        //"not a activity" priv
        assertFalse(instance.checkActivityWithNotExpression("not a activity", group, 1));
        //is a activity priv
        doReturn(false).when(instance).checKActivityWithNotExpresionAndNotGenericsActivities("depot", group, 1);
        assertFalse(instance.checkActivityWithNotExpression("depot", group, 1));
        doReturn(true).when(instance).checKActivityWithNotExpresionAndNotGenericsActivities("depot", group, 1);
        assertTrue(instance.checkActivityWithNotExpression("depot", group, 1));
    }

    /**
     * Test of checKActivityWithNotExpresionAndNotGenericsActivities method, of
     * class PolicyManager.
     */
    @Test
    public void testChecKActivityWithNotExpresionAndNotGenericsActivities() {
        instance = spy(instance);
        doReturn(new HashMap()).when(instance).getOrLoadActivities(group, WhichTree.TREEDATASET);
        assertFalse(instance.checKActivityWithNotExpresionAndNotGenericsActivities("depot", group, 1));
        doReturn(role).when(instance).getOrLoadActivities(group, WhichTree.TREEDATASET);
        assertTrue(instance.checKActivityWithNotExpresionAndNotGenericsActivities("depot", group, 1));
        assertTrue(instance.checKActivityWithNotExpresionAndNotGenericsActivities("administration,depot", group, 1));
        assertTrue(instance.checKActivityWithNotExpresionAndNotGenericsActivities("administration,autre", group, 1));
        assertFalse(instance.checKActivityWithNotExpresionAndNotGenericsActivities("synthese", group, 1));
        assertFalse(instance.checKActivityWithNotExpresionAndNotGenericsActivities("autre", group, 1));
    }

    /**
     * Test of getNodeById method, of class PolicyManager.
     */
    @Test
    public void testGetNodeById() {
        instance = spy(instance);
        //node match
        doReturn(data).when(treeActivities).getData();
        doReturn(3l).when(data).getId();
        IFlatNode result = instance.getNodeById(treeActivities, 3L);
        assertEquals(data, result);
        //node not match and children without data
        doReturn(4l).when(data).getId();
        result = instance.getNodeById(treeActivities, 3L);
        assertEquals(null, result);
        doReturn(null).when(treeActivities).getData();

        //childnode match
        doReturn(data2).when(child2).getData();
        when(data2.getId()).thenReturn(3L);
        result = instance.getNodeById(treeActivities, 3L);
        assertEquals(data2, result);
        //not match child but not leaf child
        when(data2.getId()).thenReturn(4L);
        //isLeaf
        result = instance.getNodeById(treeActivities, 3L);
        assertEquals(null, result);
        verify(instance).getNodeById(child2, 3L);
        //not is leaf and children match for "child"
        doReturn(data).when(instance).getNodeById(child2, 3L);
        result = instance.getNodeById(treeActivities, 3L);
        assertEquals(data, result);
        verify(instance, times(2)).getNodeById(child2, 3L);
        doReturn(null).when(instance).getNodeById(child2, 3L);
        result = instance.getNodeById(treeActivities, 3L);
        assertEquals(null, result);
        verify(instance, times(3)).getNodeById(child2, 3L);
    }

    /**
     * Test of restrictTree method, of class PolicyManager.
     */
    @Test
    public void testRestrictTree() {
        instance = spy(instance);
        ITreeNode tree = mock(ITreeNode.class);
        final List<ITreeNode> children = new LinkedList();
        ITreeNode child1 = mock(ITreeNode.class);
        ITreeNode child2 = mock(ITreeNode.class);
        IFlatNode data1 = mock(IFlatNode.class);
        when(data1.getId()).thenReturn(1l);
        when(child1.getData()).thenReturn(data1);
        IFlatNode data2 = mock(IFlatNode.class);
        when(data2.getId()).thenReturn(2l);
        when(child2.getData()).thenReturn(data2);
        children.add(child1);
        children.add(child2);
        when(tree.getChildren()).thenReturn(children);
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activityUser = new HashMap();
        ActivityCondition conditionPrivilRestriction = null;
        List<Activities> priv = new LinkedList();
        ITreeNode expResult = mock(ITreeNode.class);
        ITreeNode result;
        //root
        when(instance.isRoot()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        result = instance.restrictTree(tree, activityUser, conditionPrivilRestriction, priv);
        assertEquals(tree, result);
        //null condition
        result = instance.restrictTree(tree, activityUser, conditionPrivilRestriction, priv);
        verify(instance).restrictTree(child1, activityUser, null, priv);
        verify(instance).restrictTree(child2, activityUser, null, priv);
        //all condition
        conditionPrivilRestriction = ActivityCondition.ALL;
        final Map<Long, Map<Group, List<LocalDate>>> dateMap = new HashMap();
        dateMap.computeIfAbsent(2L, k -> new HashMap()).put(group, new LinkedList());
        activityUser.put(Activities.depot, dateMap);
        priv.add(Activities.depot);
        result = instance.restrictTree(tree, activityUser, conditionPrivilRestriction, priv);
        assertEquals(tree, result);
        assertFalse(children.contains(child1));
        assertTrue(children.contains(child2));
        verify(instance, times(1)).restrictTree(child1, activityUser, ActivityCondition.ALL, priv);
        verify(instance, times(1)).restrictTree(child2, activityUser, ActivityCondition.ALL, priv);
        //all condition
        conditionPrivilRestriction = ActivityCondition.ATLEAST;
        children.add(child1);
        dateMap.computeIfAbsent(2L, k -> new HashMap()).put(group, new LinkedList());
        activityUser.put(Activities.depot, dateMap);
        priv.add(Activities.depot);
        result = instance.restrictTree(tree, activityUser, conditionPrivilRestriction, priv);
        assertEquals(tree, result);
        assertFalse(children.contains(child1));
        assertTrue(children.contains(child2));
        verify(instance, times(1)).restrictTree(child1, activityUser, ActivityCondition.ATLEAST, priv);
        verify(instance, times(1)).restrictTree(child2, activityUser, ActivityCondition.ATLEAST, priv);
    }

    /**
     * Test of restrictLeavesIfExists method, of class PolicyManager.
     */
    @Test
    public void testRestrictLeavesIfExists() {
        instance = spy(instance);
        when(treeActivities.isLeaf()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        final List<Activities> privs = Arrays.asList(new Activities[]{Activities.depot});
        instance.restrictLeavesIfExists(treeActivities, role, ActivityCondition.ATLEAST, privs);
        doReturn(treeActivities).when(instance).restrictTree(treeActivities, role, ActivityCondition.ATLEAST, privs);
        instance.restrictLeavesIfExists(treeActivities, role, ActivityCondition.ATLEAST, privs);
        verify(instance, times(1)).restrictTree(treeActivities, role, ActivityCondition.ATLEAST, privs);
    }

    /**
     * Test of restrictCaseUsingAtleastOneActivity method, of class
     * PolicyManager.
     */
    @Test
    public void testRestrictCaseUsingAtleastOneActivity() {
        List<Activities> priv = Arrays.asList(new Activities[]{Activities.administration, Activities.depot, Activities.associate});
        Iterator<ITreeNode> iterator = mock(Iterator.class);
        instance.restrictCaseUsingAtleastOneActivity(priv, role, 4L, iterator);
        verify(iterator, times(0)).remove();
        priv = Arrays.asList(new Activities[]{Activities.associate});
        instance.restrictCaseUsingAtleastOneActivity(priv, role, 4L, iterator);
        verify(iterator, times(1)).remove();
    }

    /**
     * Test of restrictCaseUsingAllActivity method, of class PolicyManager.
     */
    @Test
    public void testRestrictCaseUsingAllActivity() {
        List<Activities> priv = Arrays.asList(new Activities[]{Activities.administration, Activities.depot});
        Iterator<ITreeNode> iterator = mock(Iterator.class);
        instance.restrictCaseUsingAllActivity(priv, role, 3L, iterator);
        verify(iterator, times(0)).remove();
        instance.restrictCaseUsingAllActivity(priv, role, 4L, iterator);
        verify(iterator, times(1)).remove();
        priv = Arrays.asList(new Activities[]{Activities.administration, Activities.depot, Activities.associate});
        instance.restrictCaseUsingAllActivity(priv, role, 3L, iterator);
        verify(iterator, times(2)).remove();
        priv = Arrays.asList(new Activities[]{Activities.administration});
        instance.restrictCaseUsingAllActivity(priv, role, 2L, iterator);
        verify(iterator, times(2)).remove();
        priv = Arrays.asList(new Activities[]{Activities.depot});
        instance.restrictCaseUsingAllActivity(priv, role, 3L, iterator);
        verify(iterator, times(2)).remove();
    }

    /**
     * Test of restrictTreeOnLeafsAccordingActivities method, of class
     * PolicyManager.
     */
    @Test
    @Ignore
    public void testRestrictTreeOnLeafsAccordingActivities() {
        instance = spy(instance);
        ITreeNode tree = mock(ITreeNode.class);
        final List<ITreeNode> children = new LinkedList();
        ITreeNode child1 = mock(ITreeNode.class, "child1");
        ITreeNode child2 = mock(ITreeNode.class, "child2");
        ITreeNode child3 = mock(ITreeNode.class, "child3");
        ITreeNode child4 = mock(ITreeNode.class, "child4");
        ITreeNode child5 = mock(ITreeNode.class, "child5");
        IFlatNode data1 = mock(IFlatNode.class, "data1");
        when(data1.getId()).thenReturn(1l);
        when(child1.getData()).thenReturn(data1);
        IFlatNode data2 = mock(IFlatNode.class, "data2");
        when(data2.getId()).thenReturn(2l);
        when(child2.getData()).thenReturn(data2);
        IFlatNode data3 = mock(IFlatNode.class, "data3");
        when(data3.getId()).thenReturn(3l);
        when(child3.getData()).thenReturn(data3);
        IFlatNode data4 = mock(IFlatNode.class, "data4");
        when(data4.getId()).thenReturn(4l);
        when(child4.getData()).thenReturn(data4);
        IFlatNode data5 = mock(IFlatNode.class, "data5");
        when(data5.getId()).thenReturn(5l);
        when(child5.getData()).thenReturn(data5);
        children.add(child1);
        children.add(child2);
        children.add(child3);
        children.add(child4);
        children.add(child5);
        when(tree.getChildren()).thenReturn(children);
        ActivityCondition conditionPrivilRestriction = null;
        List<Activities> priv = new LinkedList();
        priv.add(Activities.depot);
        priv.add(Activities.extraction);
        ITreeNode expResult = mock(ITreeNode.class, "expResult");
        ITreeNode result;
        Map<Activities, Integer> activities1 = new HashMap();
        when(data1.getRealState()).thenReturn(activities1);
        activities1.put(Activities.administration, Integer.MIN_VALUE);
        Map<Activities, Integer> activities2 = new HashMap();
        activities2.put(Activities.administration, Integer.MIN_VALUE);
        activities2.put(Activities.depot, Integer.MIN_VALUE);
        activities2.put(Activities.extraction, Integer.MIN_VALUE);
        when(data2.getRealState()).thenReturn(activities2);
        Map<Activities, Integer> activities3 = new HashMap();
        when(data3.getRealState()).thenReturn(activities2);
        activities3.put(Activities.extraction, Integer.MIN_VALUE);
        activities3.put(Activities.depot, Integer.MIN_VALUE);
        when(data4.getRealState()).thenReturn(activities2);
        when(data5.getRealState()).thenReturn(activities2);
        result = instance.restrictTreeOnLeafsAccordingActivities(tree, conditionPrivilRestriction, priv);
        assertEquals(tree, result);
        //all condition
        conditionPrivilRestriction = ActivityCondition.ALL;
        when(data4.hasAllActivities(priv)).thenReturn(Boolean.TRUE);
        when(data5.hasAtLeastOneActivity(priv)).thenReturn(Boolean.TRUE);
        final Map<Long, List<LocalDate>> dateMap = new HashMap();
        dateMap.put(2l, new LinkedList());
        priv.add(Activities.depot);
        result = instance.restrictTreeOnLeafsAccordingActivities(tree, conditionPrivilRestriction, priv);
        assertEquals(tree, result);
        assertFalse(children.contains(child1));
        assertFalse(children.contains(child2));
        assertTrue(children.contains(child3));
        assertTrue(children.contains(child4));
        assertFalse(children.contains(child5));
        verify(instance, never()).restrictTreeOnLeafsAccordingActivities(child1, ActivityCondition.ALL, priv);
        verify(instance, never()).restrictTreeOnLeafsAccordingActivities(child2, ActivityCondition.ALL, priv);
        verify(instance, times(1)).restrictTreeOnLeafsAccordingActivities(child3, ActivityCondition.ALL, priv);
        verify(instance, times(1)).restrictTreeOnLeafsAccordingActivities(child4, ActivityCondition.ALL, priv);
        verify(instance, never()).restrictTreeOnLeafsAccordingActivities(child5, ActivityCondition.ALL, priv);
        //all condition
        children.clear();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        children.add(child4);
        children.add(child5);
        ITreeNode trueNode = mock(ITreeNode.class);
        List<ITreeNode> trueNodes = Arrays.asList(new ITreeNode[]{trueNode});
        when(child3.getChildren()).thenReturn(trueNodes);
        when(child5.getChildren()).thenReturn(trueNodes);
        conditionPrivilRestriction = ActivityCondition.ATLEAST;
        doReturn(trueNode).when(instance).restrictTreeOnLeafsAccordingActivities(trueNode, conditionPrivilRestriction, priv);
        doReturn(trueNode).when(instance).restrictTreeOnLeafsAccordingActivities(child3, conditionPrivilRestriction, priv);
        doReturn(trueNode).when(instance).restrictTreeOnLeafsAccordingActivities(child5, conditionPrivilRestriction, priv);
        result = instance.restrictTreeOnLeafsAccordingActivities(tree, conditionPrivilRestriction, priv);
        assertEquals(tree, result);
        assertFalse(children.contains(child1));
        assertFalse(children.contains(child2));
        assertTrue(children.contains(child3));
        assertFalse(children.contains(child4));
        assertTrue(children.contains(child5));
        verify(instance, never()).restrictTreeOnLeafsAccordingActivities(child1, ActivityCondition.ATLEAST, priv);
        verify(instance, never()).restrictTreeOnLeafsAccordingActivities(child2, ActivityCondition.ATLEAST, priv);
        verify(instance, times(1)).restrictTreeOnLeafsAccordingActivities(child3, ActivityCondition.ATLEAST, priv);
        verify(instance, never()).restrictTreeOnLeafsAccordingActivities(child4, ActivityCondition.ATLEAST, priv);
        verify(instance, times(1)).restrictTreeOnLeafsAccordingActivities(child5, ActivityCondition.ATLEAST, priv);
    }

    /**
     * Test of getAllTreeNodesIds method, of class PolicyManager.
     */
    @Test
    public void testGetAllTreeNodesIds() {
        instance = spy(instance);
        ITreeNode tree = mock(ITreeNode.class);
        Set<Long> set = mock(Set.class);
        doReturn(set).when(instance).getAllNodesIds(eq(tree), any(Set.class));
        Set<Long> result = instance.getAllTreeNodesIds(tree);
        assertEquals(set, result);
        verify(instance).getAllNodesIds(eq(tree), any(Set.class));
    }

    /**
     * Test of getAllNodesIds method, of class PolicyManager.
     */
    @Test
    public void testGetAllNodesIds() {
        instance = spy(instance);
        ITreeNode<IDataNode> tree = mock(ITreeNode.class);
        final List<ITreeNode<IDataNode>> children = new LinkedList();
        final List<ITreeNode<IDataNode>> children2 = new LinkedList();
        ITreeNode<IDataNode> child1 = mock(ITreeNode.class);
        ITreeNode<IDataNode> child2 = mock(ITreeNode.class);
        ITreeNode<IDataNode> child3 = mock(ITreeNode.class);
        ITreeNode<IDataNode> child4 = mock(ITreeNode.class);
        ITreeNode<IDataNode> child5 = mock(ITreeNode.class);
        children.add(child1);
        children.add(child2);
        children.add(child3);
        children.add(child4);
        children2.add(child5);
        when(child4.getChildren()).thenReturn(children2);
        INode data1 = mock(INode.class);
        when(data1.getId()).thenReturn(1l);
        when(child1.getData()).thenReturn(data1);
        INode data2 = mock(INode.class);
        when(data2.getId()).thenReturn(2l);
        when(child2.getData()).thenReturn(data2);
        INode data3 = mock(INode.class);
        when(data3.getId()).thenReturn(3l);
        when(child3.getData()).thenReturn(data3);
        INode data4 = mock(INode.class);
        when(data4.getId()).thenReturn(4l);
        when(child4.getData()).thenReturn(data4);
        INode data5 = mock(INode.class);
        when(data5.getId()).thenReturn(5l);
        when(child5.getData()).thenReturn(data5);
        when(data1.getId()).thenReturn(1L);
        when(data2.getId()).thenReturn(2L);
        when(data3.getId()).thenReturn(3L);
        when(data4.getId()).thenReturn(4L);
        when(data5.getId()).thenReturn(5L);
        Set<Long> collider2 = new HashSet();
        collider2.add(22l);
        collider2.add(23l);
        Set<Long> collider3 = new HashSet();
        collider2.add(32l);
        collider2.add(33l);
        when(data2.getColliderNodes()).thenReturn(collider2);
        when(data3.getColliderNodes()).thenReturn(collider3);
        when(tree.getChildren()).thenReturn(children);
        when(child3.isLeaf()).thenReturn(Boolean.TRUE);
        Set<Long> setIds = new HashSet();
        Set<Long> expResult = null;
        Set<Long> result = instance.getAllNodesIds(tree, setIds);
        assertTrue(result.containsAll(Arrays.asList(new Long[]{1l, 2l, 3l, 4l, 5l, 22l, 23l, 33l})));
    }

    /**
     * Test of getOnlyNamesOfNodesWhichHaveAtLeastOneActivity method, of class
     * PolicyManager.
     */
    @Test
    public void testGetOnlyNamesOfNodesWhichHaveAtLeastOneActivity() {
        instance = spy(instance);
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles = mock(Map.class);
        doReturn(roles).when(instance).getOrLoadActivities(group, WhichTree.TREEDATASET);
        Set<String> set = mock(Set.class);
        doReturn(Optional.ofNullable(set)).when(instance).getNodeNamesWhichHasActivitiesForActivity(roles, WhichTree.TREEDATASET);
        Set<String> result = instance.getOnlyNamesOfNodesWhichHaveAtLeastOneActivity(group, WhichTree.TREEDATASET);
        assertEquals(set, result);
        doReturn(Optional.empty()).when(instance).getNodeNamesWhichHasActivitiesForActivity(roles, WhichTree.TREEDATASET);
        result = instance.getOnlyNamesOfNodesWhichHaveAtLeastOneActivity(group, WhichTree.TREEDATASET);
        assertNotNull(result);
        assertTrue(result.isEmpty());

    }

    /**
     * Test of releaseContext method, of class PolicyManager.
     */
    @Test
    public void testReleaseContext() {
        instance = spy(instance);
        instance.maactivityUsers.put(WhichTree.TREEDATASET, new HashMap());
        assertTrue(1 == instance.maactivityUsers.size());
        IUser user = mock(IUser.class);
        instance.setCurrentUser(user);
        assertNotNull(instance.getCurrentUser());
        instance.releaseContext();
        assertTrue(instance.maactivityUsers.isEmpty());
        assertNull(instance.getCurrentUser());
        verify(instance).clearTreeFromSession();
    }

    /**
     * Test of hasActivity method, of class PolicyManager.
     */
    @Test
    public void testHasActivity() {
        instance = spy(instance);
        Map<Activities, Map<Long, List<LocalDate>>> loadActivity = new HashMap();
        loadActivity.putIfAbsent(Activities.depot, new HashMap());
        WhichTree whichTree = WhichTree.TREEDATASET;
        doReturn(role).when(instance).getOrLoadActivities(group, WhichTree.TREEDATASET);
        assertTrue(instance.hasActivity(group, "depot", WhichTree.TREEDATASET));
        assertTrue(instance.hasActivity(group, "administration", WhichTree.TREEDATASET));
        assertFalse(instance.hasActivity(group, "synthese", WhichTree.TREEDATASET));
        assertFalse(instance.hasActivity(group, "priv", WhichTree.TREEDATASET));
    }

    /**
     * Test of getNodeByNodeableCode method, of class PolicyManager.
     */
    @Test
    public void testGetNodeByNodeableCode() {
        IMgaRecorder recorder = mock(IMgaRecorder.class);
        when(mgaServiceBuilder.getRecorder()).thenReturn(recorder);
        Optional<INode> result = instance.getNodeByNodeableCode("code", WhichTree.TREEDATASET);
        verify(recorder).getNodeByNodeableCode("code", WhichTree.TREEDATASET);
    }

    /**
     * Test of getNodesByNodeableCode method, of class PolicyManager.
     */
    @Test
    public void testGetNodesByNodeableCode() {
        String code = "code";
        WhichTree whichTree = WhichTree.TREEDATASET;
        INode node = mock(INode.class);
        List<INode> nodes = new LinkedList<>();
        nodes.add(node);
        when(recorder.getNodesByNodeableCodeName(whichTree, code)).thenReturn(nodes.stream());
        Stream<INode> result = instance.getNodesByNodeableCode(code, whichTree);
        assertEquals(node, result.findFirst().orElse(null));
    }

    /**
     * Test of isRoot method, of class PolicyManager.
     */
    @Test
    public void testIsRoot() {
        //nouser
        boolean result = instance.isRoot();
        assertFalse(result);
        //user not root
        IUser user = mock(IUser.class);
        instance.setCurrentUser(user);
        result = instance.isRoot();
        assertFalse(result);
        //root user
        when(user.getIsRoot()).thenReturn(Boolean.TRUE);
        result = instance.isRoot();
        assertTrue(result);
    }

    /**
     * Test of removeActivity method, of class PolicyManager.
     */
    @Test
    public void testRemoveActivity() {
        assertTrue(instance.maactivityUsers.isEmpty());
        instance.maactivityUsers.computeIfAbsent(WhichTree.TREEDATASET, k -> new HashMap()).put(group, role);
        assertEquals(role, instance.maactivityUsers.get(WhichTree.TREEDATASET).get(group));
        instance.removeActivity(group, WhichTree.TREEDATASET);
        assertTrue(instance.maactivityUsers.get(WhichTree.TREEDATASET).isEmpty());
    }

    /**
     * Test of updateActivity method, of class PolicyManager.
     */
    @Test
    public void testUpdateActivity() {
        assertTrue(instance.maactivityUsers.isEmpty());
        instance.updateActivities(group, WhichTree.TREEDATASET, role);
        assertEquals(instance.maactivityUsers.get(WhichTree.TREEDATASET).get(group), role);
    }

    /**
     * Test of getCurrentUserLogin method, of class PolicyManager.
     */
    @Test
    public void testGetCurrentUserLogin() {
        IUser user = mock(IUser.class);
        when(user.getLogin()).thenReturn("login");
        String result = instance.getCurrentUserLogin();
        assertNull(result);
        instance.setCurrentUser(user);
        result = instance.getCurrentUserLogin();
        assertEquals("login", result);
        instance.releaseContext();
        result = instance.getCurrentUserLogin();
        assertNull(result);
    }

    /**
     * Test of mergeActivity method, of class PolicyManager.
     */
    @Test
    public void testMergeActivity() {
        IMgaRecorder recorder = mock(IMgaRecorder.class);
        when(mgaServiceBuilder.getRecorder()).thenReturn(recorder);
        ICompositeActivity r = mock(ICompositeActivity.class);
        instance.mergeActivity(r);
        verify(recorder).mergeActivity(r);
    }

    /**
     * Test of persistActivity method, of class PolicyManager.
     */
    @Test
    public void testPersistActivity() {
        IMgaRecorder recorder = mock(IMgaRecorder.class);
        when(mgaServiceBuilder.getRecorder()).thenReturn(recorder);
        ICompositeActivity r = mock(ICompositeActivity.class);
        instance.persistActivity(r);
        verify(recorder).persistActivity(r);
    }

    /**
     * Test of getMgaServiceBuilder method, of class PolicyManager.
     */
    @Test
    public void testGetMgaServiceBuilder() {
        IMgaServiceBuilder result = instance.getMgaServiceBuilder();
        assertEquals(mgaServiceBuilder, result);
    }

    /**
     * Test of checkCurrentUserActivity method, of class PolicyManager.
     */
    @Test
    public void testCheckCurrentUserActivity() {
        instance = spy(instance);
        String login = "login";
        String priv = "priv";
        String expression = "expression";
        //true
        doReturn(user).when(instance).getCurrentUser();
        doReturn(true).when(instance).checkActivity(user, 4, priv, expression);
        assertTrue(instance.checkCurrentUserActivity(4, priv, expression));
        verify(instance).checkActivity(user, 4, priv, expression);
    }

    /**
     * Test of existActivity method, of class PolicyManager.
     */
    @Test
    public void testExistActivity() {
        instance = spy(instance);
        String priv = "extraction";
        Map<Activities, Map<Long, List<LocalDate>>> role = mock(Map.class);
        boolean result = instance.existActivity(priv, role);
        verify(role).containsKey(Activities.extraction);
        assertFalse(result);
        priv = "extraction,depot";
        doReturn(false).when(role).containsKey(Activities.depot);
        result = instance.existActivity(priv, role);
        verify(role, times(2)).containsKey(Activities.extraction);
        verify(role, times(1)).containsKey(Activities.depot);
        assertFalse(result);
        doReturn(true).when(role).containsKey(Activities.depot);
        result = instance.existActivity(priv, role);
        verify(role, times(3)).containsKey(Activities.extraction);
        verify(role, times(2)).containsKey(Activities.depot);
        assertTrue(result);
    }

    /**
     * Test of existAtLeastOneActivity method, of class PolicyManager.
     */
    @Test
    public void testExistAtLeastOneActivity() {
        String[] privs = new String[]{"extraction", "depot"};
        Map<Activities, Map<Long, List<LocalDate>>> role = mock(Map.class);
        assertFalse(instance.existAtLeastOneActivity(privs, role));
        when(role.containsKey(Activities.extraction)).thenReturn(Boolean.TRUE);
        assertTrue(instance.existAtLeastOneActivity(privs, role));
    }

    /**
     * Test of decodeExpression method, of class PolicyManager.
     */
    @Test
    public void testDecodeExpression() {
        instance = spy(instance);
        ITreeNode treeActivity = mock(ITreeNode.class);
        boolean expResult = false;
        //* *
        assertTrue(instance.decodeExpression(group, "*", "*", treeActivity, WhichTree.TREEDATASET));
        //* not *
        Map<Activities, Map<Long, List<LocalDate>>> roles = new HashMap();
        Map map1 = mock(Map.class);
        Map map2 = mock(Map.class);
        roles.put(Activities.depot, map1);
        roles.put(Activities.extraction, map2);
        doReturn(roles).when(instance).getOrLoadActivities(group, WhichTree.TREEDATASET);
        assertFalse(instance.decodeExpression(group, "*", "role", treeActivity, WhichTree.TREEDATASET));
        assertTrue(instance.decodeExpression(group, "*", "extraction,role", treeActivity, WhichTree.TREEDATASET));
        assertTrue(instance.decodeExpression(group, "*", "role,depot", treeActivity, WhichTree.TREEDATASET));
        ArgumentCaptor<String[]> arraysCaptor = ArgumentCaptor.forClass(String[].class);
        doReturn(false).when(instance).decodeComplexeExpression(any(String[].class), eq(treeActivity), eq("depot"));
        assertFalse(instance.decodeExpression(group, "E->(piegeage_en_montee:datatype,mon_theme:theme,nom_site:site||piegeage_en_montee:datatype)", "depot", treeActivity, WhichTree.TREEDATASET));
        verify(instance).decodeComplexeExpression(arraysCaptor.capture(), eq(treeActivity), eq("depot"));
        String[] array = arraysCaptor.getValue();
        assertTrue(2 == array.length);
        assertEquals("piegeage_en_montee:datatype,mon_theme:theme,nom_site:site", array[0]);
        assertEquals("piegeage_en_montee:datatype", array[1]);
        doReturn(true).when(instance).decodeComplexeExpression(any(String[].class), eq(treeActivity), eq("depot"));
        assertTrue(instance.decodeExpression(group, "E->(piegeage_en_montee:datatype,mon_theme:theme,nom_site:site||piegeage_en_montee:datatype)", "depot", treeActivity, WhichTree.TREEDATASET));
        verify(instance, times(2)).decodeComplexeExpression(arraysCaptor.capture(), eq(treeActivity), eq("depot"));
        array = arraysCaptor.getValue();
        assertTrue(2 == array.length);
        assertEquals("piegeage_en_montee:datatype,mon_theme:theme,nom_site:site", array[0]);
        assertEquals("piegeage_en_montee:datatype", array[1]);
        assertTrue(instance.decodeExpression(group, "E->(nom_theme:theme,nom_site:site||nom_site:site)", "depot", treeActivity, WhichTree.TREEDATASET));
        verify(instance, times(3)).decodeComplexeExpression(arraysCaptor.capture(), eq(treeActivity), eq("depot"));
        array = arraysCaptor.getValue();
        assertTrue(2 == array.length);
        assertEquals("nom_theme:theme,nom_site:site", array[0]);
        assertEquals("nom_site:site", array[1]);
    }

    /**
     * Test of decodeComplexeExpression method, of class PolicyManager.
     */
    @Test
    public void testDecodeComplexeExpression() {
        instance = spy(instance);
        String[] subExpressions = "piegeage_en_montee:datatype->mon_theme:theme->nom_site:site||piegeage_en_montee:datatype".split("\\|\\|");
        ITreeNode treeActivity = mock(ITreeNode.class);
        String activities = "depot,extraction";
        ArgumentCaptor<List> listCaptor = ArgumentCaptor.forClass(List.class);
        doReturn(false).when(instance).decodeSimpleExpression(any(List.class), eq(treeActivity), eq(activities), eq(false));
        assertFalse(instance.decodeComplexeExpression(subExpressions, treeActivity, activities));
        verify(instance, times(2)).decodeSimpleExpression(listCaptor.capture(), eq(treeActivity), eq(activities), eq(false));
        List<List> capture = listCaptor.getAllValues();
        assertEquals("piegeage_en_montee:datatype->mon_theme:theme->nom_site:site", capture.get(0).get(0));
        assertEquals("piegeage_en_montee:datatype", capture.get(1).get(0));
        doReturn(true).when(instance).decodeSimpleExpression(any(List.class), eq(treeActivity), eq(activities), eq(false));
        assertTrue(instance.decodeComplexeExpression(subExpressions, treeActivity, activities));
        verify(instance, times(3)).decodeSimpleExpression(listCaptor.capture(), eq(treeActivity), eq(activities), eq(false));
    }

    /**
     * Test of hasActivityForActivities method, of class PolicyManager.
     */
    @Test
    public void testHasActivityForActivities() {
        String[] activities = "priv".split(",");
        assertFalse(instance.hasActivityForActivities(activities, role));
        activities = "depot,administration,extraction".split(",");
        assertTrue(instance.hasActivityForActivities(activities, role));
        activities = "depot,extraction".split(",");
        assertTrue(instance.hasActivityForActivities(activities, role));
        activities = "synthese,extraction".split(",");
        assertFalse(instance.hasActivityForActivities(activities, role));
    }

    /**
     * Test of decodeSimpleExpression method, of class PolicyManager.
     */
    @Test
    public void testDecodeSimpleExpression() {
        ITreeNode treeActivity = mock(ITreeNode.class, "root");
        List<String> expressions = new ArrayList();
        String activities = "";
        ITreeNode child1 = mock(ITreeNode.class, "child 1");
        ITreeNode child2 = mock(ITreeNode.class, "child 2");
        //empty expression
        assertFalse(instance.decodeSimpleExpression(expressions, treeActivity, activities, false));

        //inDeep and expression.size == 1
        expressions = Arrays.asList("*".split(PatternConfigurator.PATH_SEPARATOR));
        assertFalse(instance.decodeSimpleExpression(expressions, treeActivity, activities, true));

        //usual case...
        instance = spy(instance);
        expressions = Arrays.asList("projet:projet,hesse:site".split(PatternConfigurator.PATH_SEPARATOR));
        activities = "depot,extraction";
        ArgumentCaptor<String> nodeCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> typeCaptor = ArgumentCaptor.forClass(String.class);
        ITreeNode matchTree = mock(ITreeNode.class);
        //with no activity
        doReturn(true).when(instance).testTreeNode("projet", "projet", treeActivity, activities);
        assertFalse(instance.decodeSimpleExpression(expressions, treeActivity, activities, false));
        //with activities on first node
        assertFalse(instance.decodeSimpleExpression(expressions, treeActivity, activities, false));
        List<ITreeNode> children = new LinkedList<>();
        children.add(child1);
        children.add(child2);
        when(treeActivity.getChildren()).thenReturn(children);
        doReturn(false).when(instance).testTreeNode("hesse", "site", child1, activities);
        doReturn(false).when(instance).testTreeNode("hesse", "site", child2, activities);
        assertFalse(instance.decodeSimpleExpression(expressions, treeActivity, activities, false));
        //with activities on all nodes
        doReturn(true).when(instance).testTreeNode("hesse", "site", child2, activities);
        assertTrue(instance.decodeSimpleExpression(expressions, treeActivity, activities, false));
        //with inDeep
        expressions = Arrays.asList("*,hesse:site".split(PatternConfigurator.PATH_SEPARATOR));
        doReturn(null).when(instance).getTreeNodeByNameAndType(treeActivity, "hesse", "site");
        assertFalse(instance.decodeSimpleExpression(expressions, treeActivity, activities, false));
        doReturn(Stream.of(child2).collect(Collectors.toList())).when(instance).getTreeNodeByNameAndType(treeActivity, "hesse", "site");
        assertTrue(instance.decodeSimpleExpression(expressions, treeActivity, activities, false));
    }

    /**
     * Test of testTreeNode method, of class PolicyManager.
     */
    @Test
    public void testTestTreeNode() {
        instance = spy(instance);
        String name = "nom";
        String type = MgaIOConfigurationTest.Site.class.getSimpleName();
        ITreeNode findITreeNode = null;
        IFlatNode data = mock(IFlatNode.class);
        String activities = "priv";
        //null tree
        assertFalse(instance.testTreeNode(name, type, findITreeNode, activities));
        findITreeNode = mock(ITreeNode.class);
        //null data
        assertFalse(instance.testTreeNode(name, type, findITreeNode, activities));
        //bad name
        when(findITreeNode.getData()).thenReturn(data);
        when(data.getCode()).thenReturn(name);
        assertFalse(instance.testTreeNode("badname", type, findITreeNode, activities));
        //bad type
        doReturn(MgaIOConfigurationTest.Datatype.class).when(data).getTypeResource();
        assertFalse(instance.testTreeNode(name, type, findITreeNode, activities));
        //good type and name
        //not given activities and empty activities
        Map<Activities, Map<Group, List<LocalDate>>> privs = mock(Map.class);
        when(data.getActivitiesForAllGroups()).thenReturn(privs);
        doReturn(MgaIOConfigurationTest.Site.class).when(data).getTypeResource();
        when(privs.isEmpty()).thenReturn(Boolean.TRUE);
        assertFalse(instance.testTreeNode(name, type, findITreeNode, activities));
        //not given activities and empty activities
        when(privs.isEmpty()).thenReturn(Boolean.TRUE);
        assertFalse(instance.testTreeNode(name, type, findITreeNode, activities));
        //not given activities and not empty activities
        when(privs.isEmpty()).thenReturn(Boolean.FALSE);
        assertTrue(instance.testTreeNode(name, type, findITreeNode, activities));

        //given activities
        activities = "administration";
        doReturn(true).when(instance).hasAtLeastOneActivity(data, activities);
        assertTrue(instance.testTreeNode(name, type, findITreeNode, activities));
        doReturn(false).when(instance).hasAtLeastOneActivity(data, activities);
        assertFalse(instance.testTreeNode(name, type, findITreeNode, activities));
    }

    /**
     * Test of hasAtLeastOneActivity method, of class PolicyManager.
     */
    @Test
    public void testHasAtLeastOneActivity() {
        String privs = "priv";
        assertFalse(instance.hasAtLeastOneActivity(data, privs));
        doReturn(true).when(data).hasActivity(Activities.depot);
        privs = "depot";
        assertTrue(instance.hasAtLeastOneActivity(data, privs));
        doReturn(false).when(data).hasActivity(Activities.depot);
        assertFalse(instance.hasAtLeastOneActivity(data, privs));
        privs = "administration,depot";
        assertFalse(instance.hasAtLeastOneActivity(data, privs));
        doReturn(true).when(data).hasActivity(Activities.depot);
        assertTrue(instance.hasAtLeastOneActivity(data, privs));
    }

    /**
     * Test of isExpression method, of class PolicyManager.
     */
    @Test
    public void testIsExpression() {
        assertTrue(instance.isExpression("E->(hjkihj:skdj)"));
        assertTrue(instance.isExpression("E->(hjkihj:skdj)"));
        assertTrue(instance.isExpression("E->(*,piegeage_en_montee:datatype)"));
        assertTrue(instance.isExpression("E->(*,piegeage_en_montee:datatype,*)"));
        assertTrue(instance.isExpression("*"));
        assertFalse(instance.isExpression("*5"));
        assertFalse(instance.isExpression("4*"));
        assertFalse(instance.isExpression("E->(dsd):"));
        assertFalse(instance.isExpression("dsd)"));
    }

    /**
     * Test of getTreeNodeByNameAndType method, of class PolicyManager.
     */
    /*@Test
    public void testGetTreeNodeByNameAndType() {
        System.out.println("getTreeNodeByNameAndType");
        instance = spy(instance);
        ITreeNode root = mock(ITreeNode.class, "root");
        Long nodeable = 20L;
        when(root.getData()).thenReturn(data);
        when(data.getCode()).thenReturn("code");
        when(data.getNodeableId()).thenReturn(nodeable);
        String name = "hesse";
        String type = "site";
        //node data not null but null name and type
        ITreeNode result = instance.getTreeNodeByNameAndType(root, name, type);
        assertNull(result);
        //node data not null but null type
        when(data.getCode()).thenReturn(name);
        doReturn(MgaIOConfigurationTest.Datatype.class).when(data).getTypeResource();
        result = instance.getTreeNodeByNameAndType(root, name, type);
        assertNull(result);
        //node data and not nul name nad type
        doReturn(MgaIOConfigurationTest.Site.class).when(data).getTypeResource();
        result = instance.getTreeNodeByNameAndType(root, name, type);
        assertEquals(root, result);
        //null node data
        List<ITreeNode> children = new LinkedList();
        children.add(child1);
        children.add(child2);
        when(root.getData()).thenReturn(null);
        when(root.getChildren()).thenReturn(children);
        when(child1.getData()).thenReturn(data2);
        when(data2.getCode()).thenReturn("hesse");
        when(data2.getNodeableId()).thenReturn(nodeable);
        when(child2.getData()).thenReturn(data);
        doReturn(MgaIOConfigurationTest.Site.class).when(data2).getTypeResource();
        doReturn(MgaIOConfigurationTest.Site.class).when(data).getTypeResource();
        doReturn(MgaIOConfigurationTest.Site.class).when(data1).getTypeResource();
        //child 2 matches
        result = instance.getTreeNodeByNameAndType(root, name, type);
        assertEquals(child2, result);
        when(data.getCode()).thenReturn("code");
        //none child matches
        result = instance.getTreeNodeByNameAndType(root, name, type);
        assertNull(result);
        //firstChild matches for children
        doReturn(otherChild).when(instance).getTreeNodeByNameAndType(child1, name, type);
        result = instance.getTreeNodeByNameAndType(root, name, type);
        assertEquals(otherChild, result);
    }*/
    /**
     * Test of persistExtractActivity method, of class PolicyManager.
     */
    /*@Test
    public void testPersistExtractActivity() {
        System.out.println("persistExtractActivity");
        instance = spy(instance);
        String login = "login";
        IMgaRecorder recorder = mock(IMgaRecorder.class);
        //empty extractrole
        Map<Long, List<LocalDate>> extractActivity = new HashMap();
        List<LocalDate> dates1 = mock(List.class);
        List<LocalDate> dates2 = mock(List.class);
        extractActivity.put(10l, dates1);
        extractActivity.put(20l, dates2);
        final LocalDate date1 = LocalDateTime.ofInstant(Instant.ofEpochMilli(20_000L), DateUtil.UTC_ZONEID).toLocalDate();
        final LocalDate date2 = LocalDateTime.ofInstant(Instant.ofEpochMilli(40_000L), DateUtil.UTC_ZONEID).toLocalDate();
        when(dates1.get(0)).thenReturn(date1);
        when(dates1.get(1)).thenReturn(date2);
        //null extract role
        instance.persistExtractActivity(login, null);
        verify(instance, never()).removeExtractActivity(login);
        //null empty role
        instance.persistExtractActivity(login, new HashMap());
        verify(instance, never()).removeExtractActivity(login);
        //not empty
        List<ExtractActivity> extraExtractActivities = new LinkedList<>();
        ExtractActivity activity1 = new ExtractActivity();
        ExtractActivity activity2 = new ExtractActivity();
        extraExtractActivities.add(activity1);
        extraExtractActivities.add(activity2);
        ArgumentCaptor<Map.Entry> exCaptor = ArgumentCaptor.forClass(Map.Entry.class);
        doReturn(extraExtractActivities.stream()).when(recorder).getMissingParentsNodes(login);
        instance.persistExtractActivity(login, extractActivity);
        verify(recorder).persist(activity1);
        verify(recorder).persist(activity2);
        verify(instance, times(2)).persistExtractActivityEntry(exCaptor.capture(), eq(login));
        List<Map.Entry> results = exCaptor.getAllValues();
        assertTrue(results.size() == 2);
        results = results.stream()
                .sorted((o1, o2) -> Long.compare((Long) o1.getKey(), (Long) o2.getKey()))
                .collect(Collectors.toList());

        verify(instance).removeExtractActivity(login);
        verify(instance).addMissingParentsNodes(login);
        assertEquals(10L, results.get(0).getKey());
        assertEquals(dates1, results.get(0).getValue());
        assertEquals(20L, results.get(1).getKey());
        assertEquals(dates2, results.get(1).getValue());
    }*/
    /**
     * Test of persistExtractActivityEntry method, of class PolicyManager.
     */
    @Test
    public void testPersistExtractActivityEntry() throws DateTimeParseException {
        LocalDate startLocalDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, "23/01/2004 00:30").toLocalDate();
        LocalDate endLocalDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, "23/12/2005 23:30").toLocalDate();
        List<LocalDate> dates = Arrays.asList(new LocalDate[]{startLocalDate, endLocalDate});
        String login = "login";
        Map.Entry<Long, List<LocalDate>> entry = new AbstractMap.SimpleEntry(2L, dates);
        ArgumentCaptor<ExtractActivity> extractActivity = ArgumentCaptor.forClass(ExtractActivity.class);
        //nominal
        instance.persistExtractActivityEntry(entry, login);
        verify(recorder).persist(extractActivity.capture());
        assertNotNull(extractActivity.getValue());
        assertEquals(startLocalDate, extractActivity.getValue().getDateStart());
        assertEquals(endLocalDate, extractActivity.getValue().getDateEnd());
        assertTrue(2L == extractActivity.getValue().getIdNode());
        assertEquals(login, extractActivity.getValue().getLogin());
        //with empty LocalDates
        dates = Arrays.asList(new LocalDate[]{null, null});
        entry = new AbstractMap.SimpleEntry(2L, dates);
        instance.persistExtractActivityEntry(entry, login);
        verify(recorder, times(2)).persist(extractActivity.capture());
        assertNotNull(extractActivity.getValue());
        assertEquals(DateUtil.MIN_LOCAL_DATE, extractActivity.getValue().getDateStart());
        assertEquals(DateUtil.MAX_LOCAL_DATE, extractActivity.getValue().getDateEnd());
        assertTrue(2L == extractActivity.getValue().getIdNode());
        assertEquals(login, extractActivity.getValue().getLogin());
    }

    /**
     * Test of removeExtractActivity method, of class PolicyManager.
     */
    @Test
    public void testRemoveExtractActivity() {
        String login = "login";
        IMgaRecorder recorder = mock(IMgaRecorder.class);
        when(mgaServiceBuilder.getRecorder()).thenReturn(recorder);
        instance.removeExtractActivity(login);
        verify(recorder).removeExtractActivity(login);
    }

    /**
     * Test of getCurrentUser method, of class PolicyManager.
     */
    @Test
    public void testGetCurrentUser() {
        IUser user = mock(IUser.class);
        IUser result = instance.getCurrentUser();
        assertNull(result);
        instance.setCurrentUser(user);
        result = instance.getCurrentUser();
        assertEquals(user, result);
    }

    /**
     * Test of persistNodes method, of class PolicyManager.
     */
    @Test
    public void testPersistNodes() {
        INode node = mock(INode.class);
        List<INode> listChild = new LinkedList();
        listChild.add(node);
        listChild.add(node);
        listChild.add(node);
        instance.persistNodes(listChild.stream());
        verify(recorder, times(3)).persist(node);
    }

    /**
     * Test of addMissingParentsNodes method, of class PolicyManager.
     */
    @Test
    public void testAddMissingParentsNodes() {
        ExtractActivity er1 = mock(ExtractActivity.class);
        ExtractActivity er2 = mock(ExtractActivity.class);
        ExtractActivity er3 = mock(ExtractActivity.class);
        List<ExtractActivity> extractActivities = new LinkedList();
        String login = "login";
        // empty or null list
        when(recorder.getMissingParentsNodes(login)).thenReturn(extractActivities.stream());
        instance.addMissingParentsNodes(login);
        verify(recorder, never()).persist(any(ExtractActivity.class));
        // with extract roles
        extractActivities.add(er1);
        extractActivities.add(er2);
        extractActivities.add(er3);
        when(recorder.getMissingParentsNodes(login)).thenReturn(extractActivities.stream());
        instance.addMissingParentsNodes(login);
        verify(recorder).persist(er1);
        verify(recorder).persist(er2);
        verify(recorder).persist(er3);
    }

    /**
     * Test of clearTreeFromSession method, of class PolicyManager.
     */
    @Test
    public void testClearTreeFromSession() {
        instance.clearTreeFromSession();
        verify(treeBuilder).clearTreeFromSession();
    }

    /**
     * Test of getNodeNamesWhichHasActivitiesForActivity method, of class
     * PolicyManager.
     */
    @Test
    public void testGetNodeNamesWhichHasActivitiesForActivity() {
        Optional<Set<String>> names;
        names = instance.getNodeNamesWhichHasActivitiesForActivity(null, WhichTree.TREEDATASET);
        assertTrue(names.get().isEmpty());
        names = instance.getNodeNamesWhichHasActivitiesForActivity(new HashMap(), WhichTree.TREEDATASET);
        assertTrue(names.get().isEmpty());
        List<INode> nodes = new LinkedList<>();
        INode node1 = mock(INode.class, "node1");
        INode node2 = mock(INode.class, "node2");
        nodes.add(node1);
        when(node1.getName()).thenReturn("name1");
        when(node2.getName()).thenReturn("name2");
        nodes.add(node2);
        ArgumentCaptor<List> listArg = ArgumentCaptor.forClass(List.class);
        doReturn(nodes.stream()).when(recorder).getNodesByIds(listArg.capture(), eq(WhichTree.TREEDATASET));
        names = instance.getNodeNamesWhichHasActivitiesForActivity(role, WhichTree.TREEDATASET);
        final Set<String> nameList = names.get();
        assertTrue(nameList.size() == 2);
        assertTrue(nameList.contains("name1"));
        assertTrue(nameList.contains("name2"));
        List ids = listArg.getValue();
        assertTrue(ids.size() == 4);
        assertTrue(ids.contains(2L));
        assertTrue(ids.contains(3L));
        assertTrue(ids.contains(4L));

    }

    /**
     * Test of getTreeNodeByFilterPredicate method, of class PolicyManager.
     */
    @Test
    public void testGetTreeNodeByFilterPredicate() {
        Predicate<ITreeNode<IFlatNode>> filter = mock(Predicate.class);
        Optional<ITreeNode<IFlatNode>> result;
        //case root matches
        doReturn(true).when(filter).test(treeActivities);
        result = testGetTreeNodeByFilterPredicateDuration(treeActivities, filter);
        assertEquals(treeActivities, result.get());
        //case root is leaf
        doReturn(false).when(filter).test(treeActivities);
        when(treeActivities.isLeaf()).thenReturn(Boolean.TRUE);
        result = testGetTreeNodeByFilterPredicateDuration(treeActivities, filter);
        assertFalse(result.isPresent());
        //case root not is leaf
        when(treeActivities.isLeaf()).thenReturn(Boolean.FALSE);
        List<ITreeNode<IFlatNode>> children = new LinkedList<>();
        children.add(treeActivities);
        children.add(treeActivities);
        children.add(child);
        List<ITreeNode<IFlatNode>> children2 = new LinkedList<>();
        children.add(treeActivities);
        children.add(treeActivities);
        children.add(child1);
        when(child.getChildren()).thenReturn(children2);
        List<ITreeNode<IFlatNode>> children3 = new LinkedList<>();
        children.add(treeActivities);
        children.add(treeActivities);
        children.add(child2);
        when(child1.getChildren()).thenReturn(children3);
        when(filter.test(child2)).thenReturn(true);
        result = testGetTreeNodeByFilterPredicateDuration(treeActivities, filter);
        assertEquals(child2, result.get());
    }

    private Optional<ITreeNode<IFlatNode>> testGetTreeNodeByFilterPredicateDuration(ITreeNode<IFlatNode> root, Predicate<ITreeNode<IFlatNode>> filter) {
        Instant i = Instant.now();
        Optional<ITreeNode<IFlatNode>> treeNodeByFilterPredicate = instance.getTreeNodeByFilterPredicate(treeActivities, filter);
        return treeNodeByFilterPredicate;
    }

    /**
     * Test of getTreeNodeById method, of class PolicyManager.
     */
    @Test
    public void testGetTreeNodeById() {
        Predicate<ITreeNode<IFlatNode>> filter = mock(Predicate.class);
        Optional<ITreeNode<IFlatNode>> result;
        when(treeActivities.getData()).thenReturn(data);
        when(child.getData()).thenReturn(data);
        when(child1.getData()).thenReturn(data);
        when(child2.getData()).thenReturn(data2);
        //case root matches
        doReturn(12L).when(data).getId();
        result = testGetTreeNodeByFilterIDDuration(treeActivities, 12L);
        assertEquals(treeActivities, result.get());
        //case root is leaf
        doReturn(13L).when(data).getId();
        when(treeActivities.isLeaf()).thenReturn(Boolean.TRUE);
        result = testGetTreeNodeByFilterIDDuration(treeActivities, 12L);
        assertFalse(result.isPresent());
        //case root not is leaf
        doReturn(12L).when(data2).getId();
        when(treeActivities.isLeaf()).thenReturn(Boolean.FALSE);
        List<ITreeNode<IFlatNode>> children = new LinkedList<>();
        children.add(treeActivities);
        children.add(treeActivities);
        children.add(child);
        List<ITreeNode<IFlatNode>> children2 = new LinkedList<>();
        children.add(treeActivities);
        children.add(treeActivities);
        children.add(child1);
        when(child.getChildren()).thenReturn(children2);
        List<ITreeNode<IFlatNode>> children3 = new LinkedList<>();
        children.add(treeActivities);
        children.add(treeActivities);
        children.add(child2);
        when(child1.getChildren()).thenReturn(children3);
        when(filter.test(child2)).thenReturn(true);
        result = testGetTreeNodeByFilterIDDuration(treeActivities, 12L);
        assertEquals(child2, result.get());
    }

    private Optional<ITreeNode<IFlatNode>> testGetTreeNodeByFilterIDDuration(ITreeNode<IFlatNode> root, long id) {
        Instant i = Instant.now();
        Optional<ITreeNode<IFlatNode>> treeNodeByFilterId = instance.getTreeNodeById(treeActivities, id);
        return treeNodeByFilterId;
    }

    /**
     * Test of filterTreeByPath method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testFilterTreeByPath() {
        String path = "";
        Predicate<ITreeNode<NodeDataSet>> expResult = null;
        Predicate<ITreeNode<NodeDataSet>> result = PolicyManager.filterTreeByPath(path);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of filterTreeById method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testFilterTreeById() {
        Long id = null;
        Predicate<ITreeNode<NodeDataSet>> expResult = null;
        Predicate<ITreeNode<NodeDataSet>> result = PolicyManager.filterTreeById(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOrLoadActivities method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testGetOrLoadActivities() {
        ICompositeGroup compositeGroup = null;
        WhichTree whichTree = null;
        PolicyManager instance = new PolicyManager();
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> expResult = null;
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> result = instance.getOrLoadActivities(compositeGroup, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGroups method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testGetGroups() {
        ICompositeGroup compositeGroup = null;
        WhichTree whichTree = null;
        PolicyManager instance = new PolicyManager();
        List<Group> expResult = null;
        List<Group> result = instance.getGroups(compositeGroup, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRoles method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testGetRoles() {
        ICompositeGroup compositeGroup = null;
        WhichTree whichTree = null;
        PolicyManager instance = new PolicyManager();
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> expResult = null;
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> result = instance.getRoles(compositeGroup, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of checkActivity method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testCheckActivity() {
        ICompositeGroup compositeGroup = null;
        Integer codeConfiguration = 0;
        String priv = "";
        String expression = "";
        PolicyManager instance = new PolicyManager();
        boolean expResult = false;
        boolean result = instance.checkActivity(compositeGroup, codeConfiguration, priv, expression);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updateActivities method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testUpdateActivities() {
        Group group = null;
        WhichTree whichTree = null;
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role = null;
        PolicyManager instance = new PolicyManager();
        instance.updateActivities(group, whichTree, role);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTreeNodeByNameAndType method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testGetTreeNodeByNameAndType() {
        PolicyManager instance = new PolicyManager();
        ITreeNode<NodeDataSet> expResult = null;
        //ITreeNode<NodeDataSet> result = instance.getTreeNodeByNameAndType(null);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of retrieveAllUsers method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testRetrieveAllUsers() {
        Integer codeConfiguration = 0;
        PolicyManager instance = new PolicyManager();
        List expResult = null;
        List result = instance.retrieveAllUsers(codeConfiguration);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of retrieveAllUserOrGroups method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testRetrieveAllUserOrGroups() {
        Integer codeConfiguration = 0;
        PolicyManager instance = new PolicyManager();
        List expResult = null;
        List result = instance.retrieveAllUserOrGroups(codeConfiguration);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of persistExtractActivity method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testPersistExtractActivity() {
        String login = "";
        Map<Long, List<LocalDate>> extractActivity = null;
        PolicyManager instance = new PolicyManager();
        instance.persistExtractActivity(login, extractActivity);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCurrentUser method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testSetCurrentUser() {
        IUser user = null;
        PolicyManager instance = new PolicyManager();
        instance.setCurrentUser(user);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPublicGroup method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testGetPublicGroup() {
        WhichTree whichTree = null;
        PolicyManager instance = new PolicyManager();
        Optional<Group> expResult = Optional.empty();
        Optional<Group> result = instance.getPublicGroup(whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addGroupEntryToActivitiesMap method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testAddGroupEntryToActivitiesMap() {
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles = null;
        Group group = null;
        PolicyManager instance = new PolicyManager();
        instance.addGroupEntryToActivitiesMap(roles, group);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addActivityEntryToActivitiesMap method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testAddActivityEntryToActivitiesMap() {
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles = null;
        Group compositeGroup = null;
        Map.Entry<Activities, Map<Long, List<LocalDate>>> activityEntry = null;
        PolicyManager instance = new PolicyManager();
        instance.addActivityEntryToActivitiesMap(roles, compositeGroup, activityEntry);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addNodeEntryToActivitiesMap method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testAddNodeEntryToActivitiesMap() {
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles = null;
        Group compositeGroup = null;
        Activities activity = null;
        Map.Entry<Long, List<LocalDate>> nodeEntry = null;
        PolicyManager instance = new PolicyManager();
        instance.addNodeEntryToActivitiesMap(roles, compositeGroup, activity, nodeEntry);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGroupByGroupNameAndWhichTree method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testGetGroupByGroupNameAndWhichTree() {
        String groupName = "";
        WhichTree whichTree = null;
        PolicyManager instance = new PolicyManager();
        Optional<Group> expResult = Optional.empty();
        Optional<Group> result = instance.getGroupByGroupNameAndWhichTree(groupName, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllPossiblesGroupUser method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testGetAllPossiblesGroupUser() {
        PolicyManager instance = new PolicyManager();
        Map<Group, Boolean> expResult = null;
        Map<Group, Boolean> result = instance.getAllPossiblesGroupUser(null);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addGroupToUser method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testAddGroupToUser() {
        IUser user = null;
        Group group = null;
        PolicyManager instance = new PolicyManager();
        Optional expResult = Optional.empty();
        Optional result = instance.addGroupToUser(user, group);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removeGroupOfUser method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testRemoveGroupOfUser() {
        IUser user = null;
        Group group = null;
        PolicyManager instance = new PolicyManager();
        Optional expResult = Optional.empty();
        Optional result = instance.removeGroupOfUser(user, group);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addGroup method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testAddGroup() {
        String groupName = "";
        WhichTree whichTree = null;
        PolicyManager instance = new PolicyManager();
        BusinessException expResult = null;
        BusinessException result = instance.addGroup(groupName, whichTree, GroupType.SIMPLE_GROUP);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removeGroup method, of class PolicyManager.
     */
    @Test
    @Ignore
    public void testRemoveGroup() {
        String groupName = "";
        WhichTree whichTree = null;
        PolicyManager instance = new PolicyManager();
        BusinessException expResult = null;
        BusinessException result = instance.removeGroup(groupName, whichTree);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPublicGroup method, of class PolicyManager.
     */
    /*@Test
    public void testGetPublicGroup() {
        System.out.println("getPublicGroup");
        Optional<Group> result = instance.getPublicGroup(WhichTree.TREEDATASET);
        assertNull(result);
        Group publicGroup = new Group("public", WhichTree.TREEDATASET);
        doReturn(Optional.ofNullable(publicGroup)).when(recorder).getPublicGroup();
        result = instance.getPublicGroup(WhichTree.TREEDATASET);
        assertEquals(publicGroup, result);
    }*/
}
