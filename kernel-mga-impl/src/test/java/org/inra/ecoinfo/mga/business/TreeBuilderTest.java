/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.mga.business.composite.*;
import static org.inra.ecoinfo.mga.business.composite.FlatNode.*;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.caching.ITreeApplicationCacheManager;
import org.inra.ecoinfo.mga.caching.ITreeSessionCacheManager;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * @author tcherniatinsky
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(FlatNode.class)
public class TreeBuilderTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    TreeBuilder instance;
    @Mock(name = "mockSkeletonTree")
    ITreeNode<AbstractBranchNode> mockSkeletonTree;
    @Mock(name = "mockFlatTree")
    ITreeNode<IFlatNode> mockFlatTree;
    @Mock(name = "mockFlatTreeParent")
    ITreeNode<IFlatNode> mockFlatTreeParent;
    @Mock(name = "mockFlatTreeChild")
    ITreeNode<IFlatNode> mockFlatTreeChild;
    @Mock(name = "mockBranchTree")
    ITreeNode<AbstractBranchNode> mockBranchTree;
    @Mock(name = "mockFlatNode")
    IFlatNode mockFlatNode;
    @Mock(name = "mockFlatNodeParent")
    IFlatNode mockFlatNodeParent;
    @Mock(name = "mockFlatNodeChild")
    IFlatNode mockFlatNodeChild;
    @Mock(name = "mockBranchNode")
    AbstractBranchNode mockBranchNode;
    List<ITreeNode<IFlatNode>> children = new LinkedList();
    @Mock(name = "mockActivities")
    Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> mockActivities;
    @Mock(name = "activities")
    Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activities = new HashMap();
    @Mock(name = "leaves")
    Stream<INode> leaves;
    @Mock(name = "group")
    Group group;
    @Mock(name = "treeApplicationCacheManager")
    private ITreeApplicationCacheManager applicationCacheManager;
    @Mock(name = "mgaServiceBuilder")
    private IMgaServiceBuilder mgaServiceBuilder;
    @Mock(name = "sessionCacheManager")
    private ITreeSessionCacheManager sessionCacheManager;
    @Mock(name = "configuration")
    private IMgaIOConfiguration configuration;
    @Mock(name = "configurator")
    private IMgaIOConfigurator configurator;
    /**
     *
     */
    public TreeBuilderTest() {
    }


    /**
     * @throws DateTimeParseException
     */
    @Before
    public void setUp() throws DateTimeParseException {
        MockitoAnnotations.openMocks(this);
        instance = spy(new TreeBuilder());
        instance.setTreeApplicationCacheManager(applicationCacheManager);
        instance.setMgaServiceBuilder(mgaServiceBuilder);
        instance.setSessionCacheManager(sessionCacheManager);
        when(configuration.getCodeConfiguration()).thenReturn(5);
        doReturn(mockFlatNode).when(mockFlatTree).getData();
        doReturn(mockBranchNode).when(mockBranchTree).getData();
        doReturn(leaves).when(leaves).sorted();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * @throws Exception
     */
    @Test
    public void testConstruct() throws Exception {
        FlatNode node = PowerMockito.mock(FlatNode.class);
        PowerMockito.whenNew(FlatNode.class).withAnyArguments().thenReturn(node);
        FlatNode flatNode = new FlatNode(mockBranchNode, group);
    }

    /**
     * Test of setApplicationCacheManager method, of class TreeBuilder.
     */
    @Test
    public void testSetApplicationCacheManager() {
        instance.setTreeApplicationCacheManager(applicationCacheManager);
        assertEquals(applicationCacheManager, instance.getApplicationCacheManager());
    }

    /**
     * Test of setSessionCacheManager method, of class TreeBuilder.
     */
    @Test
    public void testSetSessionCacheManager() {
        instance.setSessionCacheManager(sessionCacheManager);
        assertEquals(sessionCacheManager, instance.getSessionCacheManager());
    }

    /**
     * Test of setMgaServiceBuilder method, of class TreeBuilder.
     */
    @Test
    public void testSetMgaServiceBuilder() {
        instance.setMgaServiceBuilder(mgaServiceBuilder);
        assertEquals(mgaServiceBuilder, instance.getMgaServiceBuilder());
    }

    /**
     * Test of removeTreeFromSession method, of class TreeBuilder.
     */
    @Test
    public void testRemoveTreeFromSession() {
        instance.removeTreeFromSession(group, 1);
        verify(sessionCacheManager).removeFlatTree(group, 1);
    }

    /**
     * Test of clearTreeFromSession method, of class TreeBuilder.
     */
    @Test
    public void testClearTreeFromSession() {
        instance.clearTreeFromSession();
        verify(sessionCacheManager).clearFlatTree();
    }

    /**
     * Test of getOrLoadTree method, of class TreeBuilder.
     */
    @Test
    public void testGetOrLoadTree() {
        when(sessionCacheManager.containKeyFlatTree(group, 5)).thenReturn(Boolean.TRUE, Boolean.FALSE);
        doReturn(mockSkeletonTree).when(sessionCacheManager).getFlatTree(group, 5);
        ITreeNode<IFlatNode> result = instance.getOrLoadTree(Optional.ofNullable(configuration), mockActivities, group);
        assertEquals(mockSkeletonTree, result);
        doReturn(mockSkeletonTree).when(instance).getOrLoadSkeletonTree(configuration);
        Group ownGroup = mock(Group.class);
        when(configuration.getWhichTree()).thenReturn(WhichTree.TREEDATASET);
        when(group.getOwnGroup(WhichTree.TREEDATASET)).thenReturn(group);
        doReturn(mockFlatTree).when(instance).buildFlatTree(eq(group), eq(mockSkeletonTree), eq(mockActivities), isNull(ITreeNode.class), eq(""), any(HashSet.class), any(IMgaIOConfiguration.class));
        doReturn(null).when(instance).wrapStates(eq(mockFlatTree), any(HashSet.class));
        result = instance.getOrLoadTree(Optional.ofNullable(configuration), mockActivities, group);
        verify(instance).getOrLoadSkeletonTree(configuration);
        verify(instance).buildFlatTree(eq(group), eq(mockSkeletonTree), eq(mockActivities), isNull(ITreeNode.class), eq(""), any(HashSet.class), any(IMgaIOConfiguration.class));
        verify(instance).wrapStates(eq(mockFlatTree), any(HashSet.class));
        assertNotNull(result);
        assertEquals(mockFlatTree, result);
        verify(sessionCacheManager).saveFlatTree(group, result, 5);
        verify(sessionCacheManager, times(2)).containKeyFlatTree(group, 5);
    }

    /**
     * Test of getOrLoadSkeletonTree method, of class TreeBuilder.
     */
    @Test
    public void testGetOrLoadSkeletonTree() {
        doReturn(mockSkeletonTree).when(applicationCacheManager).getSkeletonTree(5);
        ITreeNode<AbstractBranchNode> result = instance.getOrLoadSkeletonTree(configuration);
        assertEquals(mockSkeletonTree, result);
        verify(instance).getOrLoadSkeletonTree(configuration);
        doReturn(null).when(applicationCacheManager).getSkeletonTree(5);
        doReturn(leaves).when(mgaServiceBuilder).loadNodes(configuration, true, true);
        doReturn(mockSkeletonTree).when(applicationCacheManager).buildSkeletonTree(configuration, leaves, true);
        result = instance.getOrLoadSkeletonTree(configuration);
        assertEquals(mockSkeletonTree, result);
        verify(instance, times(2)).getOrLoadSkeletonTree(configuration);
        verify(mgaServiceBuilder).loadNodes(configuration, true, true);
        verify(applicationCacheManager).buildSkeletonTree(configuration, leaves, true);
    }

    /**
     * Test of setStateIncludeChildrens method, of class TreeBuilder.
     */
    @Test
    public void testSetStateIncludeChildrens() {
        int state = 0;
        ITreeNode<IFlatNode> child1 = mock(ITreeNode.class);
        ITreeNode<IFlatNode> child2 = mock(ITreeNode.class);
        children.add(child1);
        children.add(child2);
        Map<Activities, Map<Group, List<LocalDate>>> activitiesForAllGroups = mock(Map.class);
        Map<Group, List<LocalDate>> datesForGroups = new HashMap<>();
        Group group1 = mock(Group.class, "group1");
        List<LocalDate> dates = Stream.of(LocalDate.of(2012, 12, 1), LocalDate.of(2012, 12, 2)).collect(Collectors.toList());
        datesForGroups.put(group, dates);
        when(activitiesForAllGroups.get(Activities.associate)).thenReturn(datesForGroups);
        when(mockFlatTree.getChildren()).thenReturn(children);
        Set nodesWithActivity = new HashSet();
        doNothing().when(mockFlatNode).addRealState(Activities.associate, state);
        doNothing().when(instance).setStateIncludeChildrens(child1, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
        doNothing().when(instance).setStateIncludeChildrens(child2, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
        doCallRealMethod().when(instance).setStateIncludeChildrens(mockFlatTree, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
        instance.setStateIncludeChildrens(mockFlatTree, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
        verify(mockFlatNode).addRealState(Activities.associate, state);
        verify(instance).setStateIncludeChildrens(child1, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
        verify(instance).setStateIncludeChildrens(child2, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
        state = 1;
        doNothing().when(instance).setStateIncludeChildrens(child1, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
        doNothing().when(instance).setStateIncludeChildrens(child2, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
        doCallRealMethod().when(instance).setStateIncludeChildrens(mockFlatTree, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
        instance.setStateIncludeChildrens(mockFlatTree, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
        verify(mockFlatNode).addRealState(Activities.associate, state);
        verify(instance).setStateIncludeChildrens(child1, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
        verify(instance).setStateIncludeChildrens(child2, state, Activities.associate, activitiesForAllGroups, nodesWithActivity);
    }

    /**
     * Test of setState method, of class TreeBuilder.
     */
    @Test
    public void testSetState_4args_2() throws DateTimeParseException {
        int state = UNCHECKED_STATE;
        Map<Group, List<LocalDate>> datesForGroup = mock(Map.class, "datesForGroup");
        Map<Activities, Map<Group, List<LocalDate>>> activitiesforAllGroups = mock(Map.class, "activitiesforAllGroups");
        when(mockFlatNode.getActivitiesForAllGroups()).thenReturn(activitiesforAllGroups);
        Map<Activities, Integer> realState = mock(Map.class, "realState");
        when(mockFlatNode.getRealState()).thenReturn(realState);
        // null node
        instance.setState(null, Activities.administration, state, datesForGroup);
        // UNCHECKED_STATE
        doNothing().when(mockFlatNode).addRealState(Activities.administration, state);
        instance.setState(mockFlatNode, Activities.administration, state, datesForGroup);
        verify(realState).remove(Activities.administration);
        verify(activitiesforAllGroups).remove(Activities.administration);
        verify(mockFlatNode, never()).addRealState(Activities.administration, state);
        verify(activitiesforAllGroups, never()).put(Activities.administration, datesForGroup);
        // CHECKED_STATE
        state = CHECKED_STATE;

        doNothing().when(mockFlatNode).addRealState(Activities.administration, state);
        Map<Group, List<LocalDate>> datesMap = new HashMap<>();
        List dates = Arrays.asList(new LocalDate[]{null, null});
        datesMap.put(group, dates);
        doReturn(datesMap).when(mockActivities).get(Activities.administration);
        instance.setState(mockFlatNode, Activities.administration, state, datesForGroup);
        verify(mockFlatNode).addRealState(Activities.administration, state);
        verify(activitiesforAllGroups).put(Activities.administration, datesForGroup);
        verify(realState, times(1)).remove(Activities.administration);
        verify(activitiesforAllGroups, times(1)).remove(Activities.administration);
    }

    /**
     * Test of buildFlatNodeForBranchNode method, of class TreeBuilder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildFlatNodeForBranchNode() throws Exception {
        INodeable nodeable = mock(INodeable.class);
        when(mockBranchNode.getNodeable()).thenReturn(nodeable);
        when(nodeable.getId()).thenReturn(5L);
        when(nodeable.getNodeableType()).thenReturn((Class<INodeable>) INodeable.class);
        Set<Long> checkedActivities = mock(Set.class);
        Set<Long> colliders = mock(Set.class);
        String mockFlatTreePath = "";
        Group ownGroup = mock(Group.class);
        //null branch
        instance.buildFlatNodeForBranchNode(group, null, activities, checkedActivities, mockFlatTree, mockFlatTreePath, mockBranchTree, configuration);
        //anscestor branch
        when(mockBranchNode.isAnAncestor()).thenReturn(true);
        instance.buildFlatNodeForBranchNode(group, mockBranchNode, activities, checkedActivities, mockFlatTree, mockFlatTreePath, mockBranchTree, configuration);
        //not anscestor branch
        when(mockBranchNode.isAnAncestor()).thenReturn(Boolean.FALSE);
        when(mockBranchNode.getColliderNodes()).thenReturn(colliders);
        FlatNode mockFlatNode = mock(FlatNode.class);
        DefaultTreeNode mockFlatTree = mock(DefaultTreeNode.class);
        final LinkedList children = new LinkedList();
        when(mockFlatTree.getChildren()).thenReturn(children);
        instance.buildFlatNodeForBranchNode(group, mockBranchNode, activities, checkedActivities, mockFlatTree, mockFlatTreePath, mockBranchTree, configuration);
        children.contains(mockFlatTree);
    }

    /**
     * Test of setState method, of class TreeBuilder.
     */
    @Test
    public void testSetState_4args() {
        IFlatNode flatNode = null;
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role = null;
        Set<Long> checkedActivities = null;
        // null role
        instance.setState(flatNode, role, checkedActivities);
        //not null role
        role = new HashMap<>();
        Map<Long, Map<Group, List<LocalDate>>> dates1 = mock(Map.class, "dates1");
        Map<Long, Map<Group, List<LocalDate>>> dates2 = mock(Map.class, "dates2");
        role.put(Activities.administration, dates1);
        role.put(Activities.edition, dates2);
        flatNode = mockFlatNode;
        when(flatNode.getId()).thenReturn(3L);
        doNothing().when(instance).addActivityToFlatNode(flatNode, role, checkedActivities, Activities.administration);
        doNothing().when(instance).addActivityToFlatNode(flatNode, role, checkedActivities, Activities.edition);
        when(dates2.containsKey(3L)).thenReturn(true);
        instance.setState(flatNode, role, checkedActivities);
        verify(instance, never()).addActivityToFlatNode(flatNode, role, checkedActivities, Activities.administration);
        verify(instance).addActivityToFlatNode(flatNode, role, checkedActivities, Activities.edition);

    }

    /**
     * Test of addActivityToFlatNode method, of class TreeBuilder.
     */
    @Test
    public void testAddActivityToFlatNode() {
        Set<Long> checkedActivities = mock(Set.class);
        Activities activity = Activities.administration;
        Map priv = mock(Map.class);
        Map<Group, List<LocalDate>> datesmap = new HashMap<>();
        List<LocalDate> dates = mock(List.class);
        datesmap.put(group, dates);
        when(mockActivities.get(Activities.administration)).thenReturn(priv);
        when(mockFlatNode.getId()).thenReturn(3L);
        doReturn(true).when(priv).containsKey(3L);
        doReturn(datesmap).when(priv).get(3L);
        assert mockActivities.get(activity) != null && mockActivities.get(activity).containsKey(mockFlatNode.getId()) : new BusinessException("can only add activity to flatNode with activity");
        instance.addActivityToFlatNode(mockFlatNode, mockActivities, checkedActivities, activity);
        verify(mockFlatNode).addActivity(activity, datesmap);
        verify(checkedActivities).add(3L);
    }

    /**
     * Test of WrapStatesForNode method, of class TreeBuilder.
     */
    @Test
    public void testWrapStatesForNode() {
        List object1 = mock(List.class);
        List object2 = mock(List.class);
        Boolean conflictDateBeg = Boolean.TRUE;
        Boolean conflictDateEnd = Boolean.FALSE;
        Boolean conflictDateBeg2 = Boolean.FALSE;
        Boolean conflictDateEnd2 = Boolean.TRUE;
        Activities priv = Activities.administration;
        ITreeNode<IFlatNode> parentNode1 = mock(ITreeNode.class, "parent1");
        ITreeNode<IFlatNode> parentNode2 = mock(ITreeNode.class, "parent2");
        Map<Activities, Map<Group, List<LocalDate>>> activitiesForAllGroups = mock(Map.class);
        Map<Group, List<LocalDate>> datesForGroups = new HashMap<>();
        Group group1 = mock(Group.class, "group1");
        List<LocalDate> dates = Stream.of(LocalDate.of(2012, 12, 1), LocalDate.of(2012, 12, 2)).collect(Collectors.toList());
        datesForGroups.put(group, dates);
        when(activitiesForAllGroups.get(Activities.administration)).thenReturn(datesForGroups);
        when(mockFlatNode.getActivitiesForAllGroups()).thenReturn(activitiesForAllGroups);
        Set nodesWithActivity = new HashSet();
        instance.wrapStateForNode(mockFlatTree, priv, nodesWithActivity);
        verify(instance, times(1)).setStateIncludeChildrens(mockFlatTree, FlatNode.CHECKED_STATE, priv, activitiesForAllGroups, nodesWithActivity);
        when(mockFlatTree.getParent()).thenReturn(parentNode1);
        instance.wrapStateForNode(mockFlatTree, priv, nodesWithActivity);
        verify(instance, times(2)).setStateIncludeChildrens(mockFlatTree, FlatNode.CHECKED_STATE, priv, activitiesForAllGroups, nodesWithActivity);
        when(parentNode1.getData()).thenReturn(mockFlatNode);
        when(parentNode1.getParent()).thenReturn(parentNode2);
        IFlatNode flatNode2 = mock(IFlatNode.class, "flatNode2");
        when(parentNode2.getData()).thenReturn(flatNode2);

    }

    /**
     * Test of buildFlatTree method, of class TreeBuilder.
     */
    @Test
    public void testBuildFlatTree() {
        String mockFlatTreePath = "";
        ITreeNode<AbstractBranchNode> tree = null;
        List<ITreeNode<AbstractBranchNode>> children = new LinkedList();
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activities = mockActivities;
        ITreeNode<IFlatNode> tinyTree = mockFlatTree;
        Set<Long> checkedActivities = mock(Set.class);
        Group ownGroup = mock(Group.class);
        ITreeNode<IFlatNode> result = instance.buildFlatTree(group, tree, activities, tinyTree, "", checkedActivities, configuration);
        assertNotNull(result);
        tree = mockBranchTree;
        result = instance.buildFlatTree(group, tree, activities, tinyTree, "", checkedActivities, configuration);
        assertNotNull(result);
        verify(instance, never()).buildFlatNodeForBranchNode(group, mockBranchNode, activities, checkedActivities, tinyTree, mockFlatTreePath, tree, configuration);
        ITreeNode child1 = mock(ITreeNode.class, "child1");
        ITreeNode child2 = mock(ITreeNode.class, "child2");
        children.add(child1);
        children.add(child2);
        AbstractBranchNode data1 = mock(AbstractBranchNode.class, "data1");
        AbstractBranchNode data2 = mock(AbstractBranchNode.class, "data2");
        when(child1.getData()).thenReturn(data1);
        when(child2.getData()).thenReturn(data2);
        when(tree.getChildren()).thenReturn(children);
        doNothing().when(instance).buildFlatNodeForBranchNode(group, data1, activities, checkedActivities, tinyTree, mockFlatTreePath, child1, configuration);
        doNothing().when(instance).buildFlatNodeForBranchNode(group, data2, activities, checkedActivities, tinyTree, mockFlatTreePath, child2, configuration);
        result = instance.buildFlatTree(group, tree, activities, tinyTree, mockFlatTreePath, checkedActivities, configuration);
        assertNotNull(result);
        verify(instance).buildFlatNodeForBranchNode(group, data1, activities, checkedActivities, tinyTree, mockFlatTreePath, child1, configuration);
        verify(instance).buildFlatNodeForBranchNode(group, data2, activities, checkedActivities, tinyTree, mockFlatTreePath, child2, configuration);
    }

    /**
     * Test of testConflictDate method, of class TreeBuilder.
     */
    @Test
    public void testTestConflictDate() {
        Map<Group, List<LocalDate>> datesForGroupsForParent = mock(Map.class, "datesForGroupsForParent");
        Map<Group, List<LocalDate>> datesForGroupsForChild = mock(Map.class, "datesForGroupsForChild");
        Group group1 = mock(Group.class, "group1");
        Group group2 = mock(Group.class, "group2");
        Group group3 = mock(Group.class, "group3");
        Group group4 = mock(Group.class, "group4");
        List<LocalDate> dates1 = mock(List.class, "dates1");
        when(datesForGroupsForParent.get(group1)).thenReturn(dates1);
        List<LocalDate> dates2 = mock(List.class, "dates2");
        when(datesForGroupsForParent.get(group2)).thenReturn(dates2);
        List<LocalDate> dates3 = mock(List.class, "dates3");
        when(datesForGroupsForChild.get(group1)).thenReturn(dates3);
        List<LocalDate> dates4 = mock(List.class, "dates4");
        when(datesForGroupsForChild.get(group3)).thenReturn(dates4);
        List<LocalDate> dates5 = mock(List.class, "dates5");
        when(datesForGroupsForChild.get(group4)).thenReturn(dates5);
        Map.Entry<Group, List<LocalDate>> entry1 = mock(Map.Entry.class, "entry1");
        Map.Entry<Group, List<LocalDate>> entry2 = mock(Map.Entry.class, "entry2");
        Set<Group> parentKeySet = Stream.of(group1, group2).collect(Collectors.toSet());
        doReturn(parentKeySet).when(datesForGroupsForParent).keySet();
        Map.Entry<Group, List<LocalDate>> entry3 = mock(Map.Entry.class, "entry3");
        Map.Entry<Group, List<LocalDate>> entry4 = mock(Map.Entry.class, "entry4");
        Map.Entry<Group, List<LocalDate>> entry5 = mock(Map.Entry.class, "entry5");
        Set<Group> childKeySet = Stream.of(group1, group3, group4).collect(Collectors.toSet());
        doReturn(childKeySet).when(datesForGroupsForChild).keySet();
        List<LocalDate> returnDates = mock(List.class, "returnDates");
        doReturn(returnDates).when(instance).mergeGroupDates(anyListOf(LocalDate.class), anyListOf(LocalDate.class));
        Map<Group, List<LocalDate>> result = instance.testConflictDate(datesForGroupsForParent, datesForGroupsForChild);
        //assertEquals(datesForGroupsForParent, result);
        verify(instance).mergeGroupDates(dates3, dates1);
        verify(instance).mergeGroupDates(null, dates2);
        verify(instance).mergeGroupDates(dates4, null);
        verify(instance).mergeGroupDates(dates5, null);
        verify(instance, times(1)).mergeGroupDates(anyList(), anyList());
    }

    /**
     * Test of getTreeNodeByID method, of class TreeBuilder.
     */
    @Test
    public void testGetTreeNodeByID() {
        ITreeNode parent = mock(ITreeNode.class);
        //null parent
        ITreeNode result = instance.getTreeNodeByID(mockFlatTree, 3L);
        assertNull(result);
        //data with bad id
        when(mockFlatTree.getParent()).thenReturn(parent);
        result = instance.getTreeNodeByID(mockFlatTree, 3L);
        assertNull(result);
        // data ith good id
        when(mockFlatNode.getId()).thenReturn(3L);
        when(parent.getData()).thenReturn(mockFlatNode);
        result = instance.getTreeNodeByID(mockFlatTree, 3L);
        assertEquals(mockFlatTree, result);
        //null parent with children not leaf
        List children = new LinkedList();
        ITreeNode child1 = mock(ITreeNode.class);
        IDataNode data1 = mock(IDataNode.class);
        when(child1.getData()).thenReturn(data1);
        ITreeNode child2 = mock(ITreeNode.class);
        IDataNode data2 = mock(IDataNode.class);
        when(child2.getData()).thenReturn(data2);
        children.add(child1);
        children.add(child2);
        when(mockFlatTree.getParent()).thenReturn(null);
        when(mockFlatTree.getChildren()).thenReturn(children);
        result = instance.getTreeNodeByID(mockFlatTree, 3L);
        verify(instance).getTreeNodeByID(child1, 3L);
        verify(instance).getTreeNodeByID(child2, 3L);
        assertNull(result);
        //child 2 as leaf
        when(child2.isLeaf()).thenReturn(true);
        result = instance.getTreeNodeByID(mockFlatTree, 3L);
        assertNull(result);
        verify(instance, times(2)).getTreeNodeByID(child1, 3L);
        verify(instance).getTreeNodeByID(child2, 3L);
        //child 2 not  leaf matching id
        when(child2.isLeaf()).thenReturn(false);
        ITreeNode returnValue = mock(ITreeNode.class);
        doReturn(returnValue).when(instance).getTreeNodeByID(child2, 3L);
        result = instance.getTreeNodeByID(mockFlatTree, 3L);
        assertEquals(returnValue, result);
        verify(instance, times(3)).getTreeNodeByID(child1, 3L);
        verify(instance, times(2)).getTreeNodeByID(child2, 3L);
        //with child matching
        when(data1.getId()).thenReturn(3L);
        result = instance.getTreeNodeByID(mockFlatTree, 3L);
        assertEquals(child1, result);
    }

    /**
     * Test of wrapStates method, of class TreeBuilder.
     */
    @Test
    public void testWrapStates() {
        ITreeNode expResult = null;
        Set<Long> checkedProcess = new HashSet();
        checkedProcess.add(1L);
        checkedProcess.add(10L);
        checkedProcess.add(12L);
        doReturn(mockFlatTree).when(instance).getTreeNodeByID(mockFlatTree, 1L);
        doReturn(mockFlatTree).when(instance).getTreeNodeByID(mockFlatTree, 10L);
        doReturn(mockFlatTree).when(instance).getTreeNodeByID(mockFlatTree, 12L);
        ITreeNode result = instance.wrapStates(mockFlatTree, checkedProcess);
    }

    /**
     *
     */
    @Test
    public void testWrapStatesForNodeWithTreeNode() {
        Map<Activities, Map> mockActivity = new HashMap();
        Map<Activities, Map<Group, List<LocalDate>>> activities = new HashMap();
        activities.put(Activities.administration, new HashMap<>());
        activities.put(Activities.edition, new HashMap<>());
        when(mockFlatTree.getData()).thenReturn(mockFlatNode);
        doReturn(activities).when(mockFlatNode).getActivitiesForAllGroups();
        Set nodesWithActivity = new HashSet();
        instance.warpStatesForNode(mockFlatTree, nodesWithActivity);
        verify(instance).wrapStateForNode(mockFlatTree, Activities.administration, nodesWithActivity);
        verify(instance).wrapStateForNode(mockFlatTree, Activities.edition, nodesWithActivity);
    }

    /**
     * Test of getStateNodeAccordingChildren method, of class TreeBuilder.
     */
    @Test
    public void testGetStateNodeAccordingChildren() {
        ITreeNode<IFlatNode> child1 = mock(ITreeNode.class, "child1");
        IFlatNode flatNode1 = mock(IFlatNode.class, "flatNode1");
        when(child1.getData()).thenReturn(flatNode1);
        Map<Activities, Map<Group, List<LocalDate>>> activities1 = mock(Map.class, "activities1");
        when(flatNode1.getActivitiesForAllGroups()).thenReturn(activities1);
        Map<Group, List<LocalDate>> dates1 = mock(Map.class, "dates1");
        when(activities1.get(Activities.depot)).thenReturn(dates1);
        when(activities1.getOrDefault(eq(Activities.depot), any())).thenReturn(dates1);
        ITreeNode<IFlatNode> child2 = mock(ITreeNode.class, "child2");
        IFlatNode flatNode2 = mock(IFlatNode.class, "flatNode2");
        when(child2.getData()).thenReturn(flatNode2);
        Map<Activities, Map<Group, List<LocalDate>>> activities2 = mock(Map.class, "activities2");
        when(flatNode2.getActivitiesForAllGroups()).thenReturn(activities2);
        Map<Group, List<LocalDate>> dates2 = mock(Map.class, "dates2");
        when(activities2.get(Activities.depot)).thenReturn(dates2);
        when(activities2.getOrDefault(eq(Activities.depot), any())).thenReturn(dates2);
        ITreeNode<IFlatNode> child3 = mock(ITreeNode.class, "child3");
        IFlatNode flatNode3 = mock(IFlatNode.class, "flatNode3");
        when(child3.getData()).thenReturn(flatNode3);
        Map<Activities, Map<Group, List<LocalDate>>> activities3 = mock(Map.class, "activities3");
        when(flatNode3.getActivitiesForAllGroups()).thenReturn(activities3);
        Map<Group, List<LocalDate>> dates3 = mock(Map.class, "dates3");
        Map<Group, List<LocalDate>> datesReturn = mock(Map.class, "datesReturn");
        when(activities3.get(Activities.depot)).thenReturn(dates3);
        when(activities3.getOrDefault(eq(Activities.depot), any())).thenReturn(dates3);
        children.add(child1);
        children.add(child2);
        children.add(child3);
        when(mockFlatTree.getChildren()).thenReturn(children);
        //allChecked
        int state1 = CHECKED_STATE;
        when(flatNode1.getRealState(Activities.depot)).thenReturn(state1);
        int state2 = CHECKED_STATE;
        when(flatNode2.getRealState(Activities.depot)).thenReturn(state2);
        int state3 = CHECKED_STATE;
        when(flatNode3.getRealState(Activities.depot)).thenReturn(state3);
        ArgumentCaptor<Map> dateStartCap = ArgumentCaptor.forClass(Map.class);
        ArgumentCaptor<Map> dateEndCap = ArgumentCaptor.forClass(Map.class);
        doReturn(datesReturn).when(instance).testConflictDate(dateStartCap.capture(), dateEndCap.capture());
        Object result = instance.getStateNodeAccordingChildren(mockFlatTree, Activities.depot, state2, dates2);
        assertEquals(CHECKED_STATE, ((List) result).get(0));
        assertEquals(datesReturn, ((List) result).get(1));
        List<Map> datesStart = dateStartCap.getAllValues();
        assertEquals(dates2, datesStart.get(0));
        assertEquals(datesReturn, datesStart.get(1));
        assertEquals(datesReturn, datesStart.get(2));
        List<Map> datesEnd = dateEndCap.getAllValues();
        assertEquals(dates1, datesEnd.get(0));
        assertEquals(dates2, datesEnd.get(1));
        //all unchecked
        state1 = UNCHECKED_STATE;
        when(flatNode1.getRealState(Activities.depot)).thenReturn(state1);
        state2 = UNCHECKED_STATE;
        when(flatNode2.getRealState(Activities.depot)).thenReturn(state2);
        state3 = UNCHECKED_STATE;
        when(flatNode3.getRealState(Activities.depot)).thenReturn(state3);
        result = instance.getStateNodeAccordingChildren(mockFlatTree, Activities.depot, state2, dates2);
        assertEquals(UNCHECKED_STATE, ((List) result).get(0));
        assertEquals(datesReturn, ((List) result).get(1));
        datesStart = dateStartCap.getAllValues();
        assertEquals(dates2, datesStart.get(3));
        assertEquals(datesReturn, datesStart.get(4));
        assertEquals(datesReturn, datesStart.get(5));
        datesEnd = dateEndCap.getAllValues();
        assertEquals(dates1, datesEnd.get(3));
        assertEquals(dates2, datesEnd.get(4));
        assertEquals(dates3, datesEnd.get(5));
        //date2 indeterminate
        state1 = UNCHECKED_STATE;
        when(flatNode1.getRealState(Activities.depot)).thenReturn(state1);
        state2 = INDETERMINATE_STATE;
        when(flatNode2.getRealState(Activities.depot)).thenReturn(state2);
        state3 = UNCHECKED_STATE;
        when(flatNode3.getRealState(Activities.depot)).thenReturn(state3);
        result = instance.getStateNodeAccordingChildren(mockFlatTree, Activities.depot, state3, dates3);
        assertEquals(INDETERMINATE_STATE, ((List) result).get(0));
        assertEquals(datesReturn, ((List) result).get(1));
        datesStart = dateStartCap.getAllValues();
        assertEquals(dates3, datesStart.get(6));
        assertEquals(datesReturn, datesStart.get(7));
        assertTrue(datesStart.size() == 8);
        datesEnd = dateEndCap.getAllValues();
        assertEquals(dates1, datesEnd.get(6));
        assertEquals(dates2, datesEnd.get(7));
        assertTrue(datesEnd.size() == 8);
    }

    /**
     * Test of addGroupActivityForActivity method, of class TreeBuilder.
     */
    @Test
    public void testAddGroupActivityForActivity() {
        Map<Activities, Map<Group, List<LocalDate>>> activitiesForAllGroups = mock(Map.class, "activitiesForAllGroups");
        when(mockFlatNode.getActivitiesForAllGroups()).thenReturn(activitiesForAllGroups);
        Map.Entry<Activities, Map<Group, List<LocalDate>>> entry1 = mock(Map.Entry.class, "entry1");
        when(entry1.getKey()).thenReturn(Activities.administration);
        Map.Entry<Activities, Map<Group, List<LocalDate>>> entry2 = mock(Map.Entry.class, "entry2");
        when(entry2.getKey()).thenReturn(Activities.depot);
        Map.Entry<Activities, Map<Group, List<LocalDate>>> entry3 = mock(Map.Entry.class, "entry3");
        when(entry3.getKey()).thenReturn(Activities.publication);
        Set<Map.Entry<Activities, Map<Group, List<LocalDate>>>> entrySet = Stream.of(entry1, entry2, entry3).collect(Collectors.toSet());
        when(activitiesForAllGroups.entrySet()).thenReturn(entrySet);
        Map.Entry<Group, List<LocalDate>> entryGroup = mock(Map.Entry.class, "entryGroup");
        List<LocalDate> dates = mock(List.class, "dates");
        when(entryGroup.getKey()).thenReturn(group);
        when(entryGroup.getValue()).thenReturn(dates);
        Map<Group, List<LocalDate>> entry2Value = mock(Map.class);
        when(entry2.getValue()).thenReturn(entry2Value);
        //not same group
        when(mockFlatNode.getOwnGroup()).thenReturn(null);
        Map<Activities, List<LocalDate>> ownActivities = mock(Map.class);
        when(mockFlatNode.getOwnActivities()).thenReturn(ownActivities);
        TreeBuilder.addGroupActivityForActivity(mockFlatNode, Activities.depot, entryGroup);
        verify(entry2Value).put(group, dates);
        verify(ownActivities, never()).put(any(Activities.class), any(List.class));
        //not same group
        when(mockFlatNode.getOwnGroup()).thenReturn(group);
        TreeBuilder.addGroupActivityForActivity(mockFlatNode, Activities.depot, entryGroup);
        verify(ownActivities).put(Activities.depot, dates);
    }

    /**
     * Test of getMgaServiceBuilder method, of class TreeBuilder.
     */
    @Test
    public void testGetMgaServiceBuilder() {
        assertEquals(mgaServiceBuilder, instance.getMgaServiceBuilder());
    }

    /**
     * Test of getApplicationCacheManager method, of class TreeBuilder.
     */
    @Test
    public void testGetApplicationCacheManager() {
        assertEquals(applicationCacheManager, instance.getApplicationCacheManager());
    }

    /**
     * Test of getSessionCacheManager method, of class TreeBuilder.
     */
    @Test
    public void testGetSessionCacheManager() {
        assertEquals(sessionCacheManager, instance.getSessionCacheManager());
    }

    /**
     * Test of setOwnState method, of class TreeBuilder.
     */
    @Test
    public void testSetOwnState() {
        int state = UNCHECKED_STATE;
        List<LocalDate> ownDates = mock(List.class, "ownDates");
        Map<Activities, List<LocalDate>> ownActivities = mock(Map.class, "activitiesforAllGroups");
        when(mockFlatNode.getOwnActivities()).thenReturn(ownActivities);
        Map<Activities, Integer> ownRealState = mock(Map.class, "realState");
        when(mockFlatNode.getOwnRealState()).thenReturn(ownRealState);
        doNothing().when(mockFlatNode).addOwnRealState(Activities.administration, state);
        instance.setOwnState(null, Activities.administration, state, ownDates);
        // UNCHECKED_STATE
        doNothing().when(mockFlatNode).addRealState(Activities.administration, state);
        instance.setOwnState(mockFlatNode, Activities.administration, state, ownDates);
        verify(ownRealState).remove(Activities.administration);
        verify(ownActivities).remove(Activities.administration);
        verify(mockFlatNode, never()).addRealState(Activities.administration, state);
        verify(ownActivities, never()).put(Activities.administration, ownDates);
        // CHECKED_STATE
        state = CHECKED_STATE;

        doNothing().when(mockFlatNode).addRealState(Activities.administration, state);
        Map<Group, List<LocalDate>> datesMap = new HashMap<>();
        List dates = Arrays.asList(new LocalDate[]{null, null});
        datesMap.put(group, dates);
        doReturn(datesMap).when(mockActivities).get(Activities.administration);
        instance.setOwnState(mockFlatNode, Activities.administration, state, ownDates);
        verify(mockFlatNode).addOwnRealState(Activities.administration, state);
        verify(ownActivities).put(Activities.administration, ownDates);
        verify(ownRealState, times(1)).remove(Activities.administration);
        verify(ownActivities, times(1)).remove(Activities.administration);
    }

    /**
     * Test of getOwnStateNodeAccordingChildren method, of class TreeBuilder.
     */
    @Test
    public void testGetOwnStateNodeAccordingChildren() {
        ITreeNode<IFlatNode> child1 = mock(ITreeNode.class, "child1");
        IFlatNode flatNode1 = mock(IFlatNode.class, "flatNode1");
        when(child1.getData()).thenReturn(flatNode1);
        Map<Activities, Integer> ownRealState1 = mock(Map.class, "ownRealState1");
        when(flatNode1.getOwnRealState()).thenReturn(ownRealState1);
        Map<Activities, List<LocalDate>> ownActivities1 = mock(Map.class, "ownActivities1");
        when(flatNode1.getOwnActivities()).thenReturn(ownActivities1);
        List<LocalDate> dates1 = mock(List.class, "dates1");
        when(ownActivities1.get(Activities.depot)).thenReturn(dates1);
        ITreeNode<IFlatNode> child2 = mock(ITreeNode.class, "child2");
        IFlatNode flatNode2 = mock(IFlatNode.class, "flatNode2");
        when(child2.getData()).thenReturn(flatNode2);
        Map<Activities, Integer> ownRealState2 = mock(Map.class, "ownRealState2");
        when(flatNode2.getOwnRealState()).thenReturn(ownRealState2);
        Map<Activities, List<LocalDate>> ownActivities2 = mock(Map.class, "ownActivities2");
        when(flatNode2.getOwnActivities()).thenReturn(ownActivities2);
        List<LocalDate> dates2 = mock(List.class, "dates2");
        when(ownActivities2.get(Activities.depot)).thenReturn(dates2);
        ITreeNode<IFlatNode> child3 = mock(ITreeNode.class, "child3");
        IFlatNode flatNode3 = mock(IFlatNode.class, "flatNode3");
        when(child3.getData()).thenReturn(flatNode3);
        Map<Activities, Integer> ownRealState3 = mock(Map.class, "ownRealState3");
        when(flatNode3.getOwnRealState()).thenReturn(ownRealState3);
        Map<Activities, List<LocalDate>> ownActivities3 = mock(Map.class, "ownActivities3");
        when(flatNode3.getOwnActivities()).thenReturn(ownActivities3);
        List<LocalDate> dates3 = mock(List.class, "dates3");
        List<LocalDate> datesReturn = mock(List.class, "datesReturn");
        when(ownActivities3.get(Activities.depot)).thenReturn(dates3);
        children.add(child1);
        children.add(child2);
        children.add(child3);
        when(mockFlatTree.getChildren()).thenReturn(children);
        //allChecked
        int state1 = CHECKED_STATE;
        when(ownRealState1.getOrDefault(Activities.depot, 0)).thenReturn(state1);
        int state2 = CHECKED_STATE;
        when(ownRealState2.getOrDefault(Activities.depot, 0)).thenReturn(state2);
        int state3 = CHECKED_STATE;
        when(ownRealState3.getOrDefault(Activities.depot, 0)).thenReturn(state3);
        ArgumentCaptor<List> dateStartCap = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<List> dateEndCap = ArgumentCaptor.forClass(List.class);
        doReturn(datesReturn).when(instance).mergeGroupDates(dateStartCap.capture(), dateEndCap.capture());
        Object result = instance.getOwnStateNodeAccordingChildren(mockFlatTree, Activities.depot, state2, dates2);
        assertEquals(CHECKED_STATE, ((List) result).get(0));
        assertEquals(datesReturn, ((List) result).get(1));
        List<List> datesStart = dateStartCap.getAllValues();
        assertEquals(dates2, datesStart.get(0));
        assertEquals(datesReturn, datesStart.get(1));
        assertEquals(datesReturn, datesStart.get(2));
        List<List> datesEnd = dateEndCap.getAllValues();
        assertEquals(dates1, datesEnd.get(0));
        assertEquals(dates2, datesEnd.get(1));
        //all unchecked
        state1 = UNCHECKED_STATE;
        when(ownRealState1.getOrDefault(Activities.depot, 0)).thenReturn(state1);
        state2 = UNCHECKED_STATE;
        when(ownRealState2.getOrDefault(Activities.depot, 0)).thenReturn(state2);
        state3 = UNCHECKED_STATE;
        when(ownRealState3.getOrDefault(Activities.depot, 0)).thenReturn(state3);
        result = instance.getOwnStateNodeAccordingChildren(mockFlatTree, Activities.depot, state2, dates2);
        assertEquals(UNCHECKED_STATE, ((List) result).get(0));
        assertEquals(datesReturn, ((List) result).get(1));
        datesStart = dateStartCap.getAllValues();
        assertEquals(dates2, datesStart.get(3));
        assertEquals(datesReturn, datesStart.get(4));
        assertEquals(datesReturn, datesStart.get(5));
        datesEnd = dateEndCap.getAllValues();
        assertEquals(dates1, datesEnd.get(3));
        assertEquals(dates2, datesEnd.get(4));
        assertEquals(dates3, datesEnd.get(5));
        //date2 indeterminate
        state1 = UNCHECKED_STATE;
        when(ownRealState1.getOrDefault(Activities.depot, 0)).thenReturn(state1);
        state2 = INDETERMINATE_STATE;
        when(ownRealState2.getOrDefault(Activities.depot, 0)).thenReturn(state2);
        state3 = UNCHECKED_STATE;
        when(ownRealState3.getOrDefault(Activities.depot, 0)).thenReturn(state3);
        result = instance.getOwnStateNodeAccordingChildren(mockFlatTree, Activities.depot, state3, dates3);
        assertEquals(INDETERMINATE_STATE, ((List) result).get(0));
        assertEquals(datesReturn, ((List) result).get(1));
        datesStart = dateStartCap.getAllValues();
        assertEquals(dates3, datesStart.get(6));
        assertEquals(datesReturn, datesStart.get(7));
        assertTrue(datesStart.size() == 8);
        datesEnd = dateEndCap.getAllValues();
        assertEquals(dates1, datesEnd.get(6));
        assertEquals(dates2, datesEnd.get(7));
        assertTrue(datesEnd.size() == 8);
    }

    /**
     * Test of mergeGroupDates method, of class TreeBuilder.
     */
    @Test
    public void testMergeGroupDates() {
        List<LocalDate> childDates = mock(List.class);
        LocalDate bd1 = LocalDate.of(1984, Month.MARCH, 11);
        when(childDates.get(0)).thenReturn(bd1);
        LocalDate ed1 = LocalDate.of(1984, Month.MARCH, 12);
        when(childDates.get(1)).thenReturn(ed1);
        List<LocalDate> parentDates = mock(List.class);
        LocalDate bd2 = LocalDate.of(1984, Month.MARCH, 21);
        when(parentDates.get(0)).thenReturn(bd2);
        LocalDate ed2 = LocalDate.of(1984, Month.MARCH, 22);
        when(parentDates.get(1)).thenReturn(ed2);
        doReturn(bd2).when(instance).merge(bd1, bd2);
        doReturn(ed1).when(instance).merge(ed1, ed2);
        List<LocalDate> result = instance.mergeGroupDates(null, null);
        assertEquals(null, result);
        result = instance.mergeGroupDates(childDates, null);
        assertEquals(childDates, result);
        result = instance.mergeGroupDates(null, parentDates);
        assertEquals(parentDates, result);
        result = instance.mergeGroupDates(childDates, parentDates);
        assertEquals(bd2, result.get(0));
        assertEquals(ed1, result.get(1));
    }

    /**
     * Test of merge method, of class TreeBuilder.
     */
    @Test
    public void testMerge() {
        LocalDate d1 = null;
        LocalDate d2 = null;
        LocalDate result = instance.merge(d1, d2);
        assertEquals(null, result);
        d1 = LocalDate.of(1984, Month.MARCH, 21);
        result = instance.merge(d1, d2);
        assertEquals(DateUtil.MIN_LOCAL_DATE, result);
        d2 = LocalDate.of(1984, Month.MARCH, 19);
        result = instance.merge(d1, d2);
        assertEquals(DateUtil.MIN_LOCAL_DATE, result);
        d2 = LocalDate.of(1984, Month.MARCH, 21);
        result = instance.merge(d2, d1);
        assertEquals(d2, result);
    }

    /**
     * Test of getMgaIOConfigurator method, of class TreeBuilder.
     */
    @Test
    public void testGetMgaIOConfigurator() {
        when(applicationCacheManager.getMgaIOConfigurator()).thenReturn(configurator);
        IMgaIOConfigurator result = instance.getMgaIOConfigurator();
        assertEquals(configurator, result);
    }

    /**
     * Test of setState method, of class TreeBuilder.
     */
    @Test
    public void testSetState_3args() {
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role = mock(Map.class);
        when(mockFlatNode.getId()).thenReturn(5l);
        Map<Long, Map<Group, List<LocalDate>>> value1 = mock(Map.class);
        Map<Long, Map<Group, List<LocalDate>>> value2 = mock(Map.class);
        Map<Long, Map<Group, List<LocalDate>>> value3 = mock(Map.class);
        Set<Long> nodesWithActivity = mock(Set.class);
        Map.Entry<Activities, Map<Long, Map<Group, List<LocalDate>>>> entry1 = mock(Map.Entry.class);
        when(entry1.getValue()).thenReturn(value1);
        when(entry1.getKey()).thenReturn(Activities.administration);
        when(value1.containsKey(5l)).thenReturn(Boolean.TRUE);
        doNothing().when(instance).addActivityToFlatNode(mockFlatNode, role, nodesWithActivity, Activities.administration);
        Map.Entry<Activities, Map<Long, Map<Group, List<LocalDate>>>> entry2 = mock(Map.Entry.class);
        when(entry2.getValue()).thenReturn(value2);
        when(entry2.getKey()).thenReturn(Activities.depot);
        when(value2.containsKey(5l)).thenReturn(Boolean.FALSE);
        doNothing().when(instance).addActivityToFlatNode(mockFlatNode, role, nodesWithActivity, Activities.depot);
        Map.Entry<Activities, Map<Long, Map<Group, List<LocalDate>>>> entry3 = mock(Map.Entry.class);
        when(entry3.getValue()).thenReturn(value3);
        when(entry3.getKey()).thenReturn(Activities.publication);
        when(value3.containsKey(5l)).thenReturn(Boolean.TRUE);
        doNothing().when(instance).addActivityToFlatNode(mockFlatNode, role, nodesWithActivity, Activities.publication);
        Set<Map.Entry<Activities, Map<Long, Map<Group, List<LocalDate>>>>> entrySet = Stream.of(entry1, entry2, entry3).collect(Collectors.toSet());
        doReturn(entrySet).when(role).entrySet();
        instance.setState(mockFlatNode, role, nodesWithActivity);
        verify(value1).containsKey(5l);
        verify(value2).containsKey(5l);
        verify(value3).containsKey(5l);
        verify(instance).addActivityToFlatNode(mockFlatNode, role, nodesWithActivity, Activities.administration);
        verify(instance, never()).addActivityToFlatNode(mockFlatNode, role, nodesWithActivity, Activities.depot);
        verify(instance).addActivityToFlatNode(mockFlatNode, role, nodesWithActivity, Activities.publication);
        verify(instance, never()).addActivityToFlatNode(mockFlatNode, role, nodesWithActivity, Activities.telechargement);
    }

    /**
     * Test of warpStatesForNode method, of class TreeBuilder.
     */
    @Test
    public void testWarpStatesForNode() {
        Set<Long> nodesWithActivity = mock(Set.class, "nodesWithActivity");
        doReturn(mockFlatNode).when(mockFlatTree).getData();
        Map activitiesForAllGroups = mock(Map.class);
        Set<Activities> activities = Stream.of(Activities.administration, Activities.associate).collect(Collectors.toSet());
        doReturn(activities).when(activitiesForAllGroups).keySet();
        doReturn(activitiesForAllGroups).when(mockFlatNode).getActivitiesForAllGroups();
        doNothing().when(instance).wrapStateForNode(mockFlatTree, Activities.administration, nodesWithActivity);
        doNothing().when(instance).wrapStateForNode(mockFlatTree, Activities.associate, nodesWithActivity);
        instance.warpStatesForNode(mockFlatTree, nodesWithActivity);
        verify(instance).wrapStateForNode(mockFlatTree, Activities.administration, nodesWithActivity);
        verify(instance).wrapStateForNode(mockFlatTree, Activities.associate, nodesWithActivity);
    }

    /**
     * Test of wrapStateForNode method, of class TreeBuilder.
     */
    @Test
    public void testWrapStateForNode2() {
        List object1 = mock(List.class);
        List object2 = mock(List.class);
        Boolean conflictDateBeg = Boolean.TRUE;
        Boolean conflictDateEnd = Boolean.FALSE;
        Boolean conflictDateBeg2 = Boolean.FALSE;
        Boolean conflictDateEnd2 = Boolean.TRUE;
        Activities priv = Activities.administration;
        ITreeNode<IFlatNode> parentNode1 = mock(ITreeNode.class, "parent1");
        ITreeNode<IFlatNode> parentNode2 = mock(ITreeNode.class, "parent2");
        Map<Activities, Map<Group, List<LocalDate>>> activitiesForAllGroups = mock(Map.class);
        Map<Group, List<LocalDate>> datesForGroups = new HashMap<>();
        Group group1 = mock(Group.class, "group1");
        List<LocalDate> dates = Stream.of(LocalDate.of(2012, 12, 1), LocalDate.of(2012, 12, 2)).collect(Collectors.toList());
        datesForGroups.put(group, dates);
        when(activitiesForAllGroups.get(Activities.administration)).thenReturn(datesForGroups);
        when(mockFlatNode.getActivitiesForAllGroups()).thenReturn(activitiesForAllGroups);
        Set nodesWithActivity = new HashSet();
        instance.wrapStateForNode(mockFlatTree, priv, nodesWithActivity);
        verify(instance, times(1)).setStateIncludeChildrens(mockFlatTree, FlatNode.CHECKED_STATE, priv, activitiesForAllGroups, nodesWithActivity);
        when(mockFlatTree.getParent()).thenReturn(parentNode1);
        instance.wrapStateForNode(mockFlatTree, priv, nodesWithActivity);
        verify(instance, times(2)).setStateIncludeChildrens(mockFlatTree, FlatNode.CHECKED_STATE, priv, activitiesForAllGroups, nodesWithActivity);
        when(parentNode1.getData()).thenReturn(mockFlatNode);
        when(parentNode1.getParent()).thenReturn(parentNode2);
        IFlatNode flatNode2 = mock(IFlatNode.class, "flatNode2");
        when(parentNode2.getData()).thenReturn(flatNode2);

    }

    /**
     * Test of wrapStateForNode method, of class TreeBuilder.
     */
    @Test
    public void testWrapStateForNode() {
        Map<Activities, Map<Group, List<LocalDate>>> activitiesForAllGroups = mock(Map.class, "activitiesForAllGroups");
        Map<Activities, List<LocalDate>> ownActivities = mock(Map.class, "ownActivities");
        Set<Long> nodesWithActivity = mock(Set.class, "nodesWithActivity");
        doReturn(activitiesForAllGroups).when(mockFlatNode).getActivitiesForAllGroups();
        doReturn(ownActivities).when(mockFlatNode).getOwnActivities();
        doReturn(mockFlatTreeParent).when(mockFlatTree).getParent();
        doReturn(mockFlatNodeParent).when(mockFlatTreeParent).getData();
        doReturn(5l).when(mockFlatNodeParent).getId();
        when(nodesWithActivity.contains(5l)).thenReturn(Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
        Map<Group, List<LocalDate>> datesForGroups = mock(Map.class, "datesForGroups");
        doReturn(datesForGroups).when(activitiesForAllGroups).get(Activities.administration);
        List<LocalDate> ownDates = mock(List.class, "ownDates");
        doReturn(ownDates).when(ownActivities).get(Activities.administration);
        doNothing().when(instance).
                setStateIncludeChildrens(mockFlatTree, CHECKED_STATE, Activities.administration, activitiesForAllGroups, nodesWithActivity);
        doReturn(mockFlatTreeParent).when(instance).warpStateAndReturnParent(mockFlatTreeParent, Activities.administration, mockFlatTree, datesForGroups, ownDates);
        instance.wrapStateForNode(mockFlatTree, Activities.administration, nodesWithActivity);
        verify(instance).
                setStateIncludeChildrens(mockFlatTree, CHECKED_STATE, Activities.administration, activitiesForAllGroups, nodesWithActivity);
        verify(instance, times(2)).warpStateAndReturnParent(mockFlatTreeParent, Activities.administration, mockFlatTree, datesForGroups, ownDates);

    }

    /**
     * Test of warpStateAndReturnParent method, of class TreeBuilder.
     */
    @Test
    public void testwarpStateAndReturnParent() {
        Map<Group, List<LocalDate>> datesForGroups = mock(Map.class, "datesForGroups");
        List<LocalDate> ownDates = mock(List.class, "ownDates");
        doNothing().when(instance).setRealState(mockFlatTreeParent, Activities.administration, mockFlatTree, datesForGroups);
        doNothing().when(instance).setOwnRealState(mockFlatTreeParent, Activities.administration, mockFlatTree, ownDates);
        when(mockFlatTreeParent.getParent()).thenReturn(mockFlatTreeChild);
        ITreeNode result = instance.warpStateAndReturnParent(mockFlatTreeParent, Activities.administration, mockFlatTree, datesForGroups, ownDates);
        verify(instance).setRealState(mockFlatTreeParent, Activities.administration, mockFlatTree, datesForGroups);
        verify(instance).setOwnRealState(mockFlatTreeParent, Activities.administration, mockFlatTree, ownDates);
        assertEquals(mockFlatTreeChild, result);
    }

    /**
     * Test of warpStateAndReturnParent method, of class TreeBuilder.
     */
    @Test
    public void testsetOwnRealState() {
        List ownStateNodeAccordingChildren = mock(List.class);
        int ownRealState = 0;
        List<LocalDate> ownDates = mock(List.class);
        doReturn(mockFlatNodeParent).when(mockFlatTreeParent).getData();
        doReturn(ownStateNodeAccordingChildren).when(instance).getOwnStateNodeAccordingChildren(mockFlatTreeParent, Activities.associate, 0, ownDates);
        when(ownStateNodeAccordingChildren.get(0)).thenReturn(ownRealState);
        when(ownStateNodeAccordingChildren.get(1)).thenReturn(ownDates);
        doNothing().when(instance).setOwnState(mockFlatNodeParent, Activities.associate, ownRealState, ownDates);
        instance.setOwnRealState(mockFlatTreeParent, Activities.associate, mockFlatTree, ownDates);
        verify(instance).getOwnStateNodeAccordingChildren(mockFlatTreeParent, Activities.associate, 0, ownDates);
        verify(ownStateNodeAccordingChildren).get(0);
        verify(ownStateNodeAccordingChildren).get(1);
        verify(instance).setOwnState(mockFlatNodeParent, Activities.associate, ownRealState, ownDates);

    }

    /**
     * Test of warpStateAndReturnParent method, of class TreeBuilder.
     */
    @Test
    public void testsetRealState() {
        List stateNodeAccordingChildren = mock(List.class);
        int realState = 0;
        Map<Group, List<LocalDate>> datesForGroups = mock(Map.class);
        Map<Activities, Integer> realStates = mock(Map.class);
        doReturn(mockFlatNode).when(mockFlatTree).getData();
        doReturn(realStates).when(mockFlatNode).getRealState();
        doReturn(mockFlatNodeParent).when(mockFlatTreeParent).getData();
        doReturn(0).when(realStates).get(Activities.associate);
        doReturn(stateNodeAccordingChildren).when(instance).getStateNodeAccordingChildren(mockFlatTreeParent, Activities.associate, 0, datesForGroups);
        when(stateNodeAccordingChildren.get(0)).thenReturn(realState);
        when(stateNodeAccordingChildren.get(1)).thenReturn(datesForGroups);
        doNothing().when(instance).setState(mockFlatNodeParent, Activities.associate, realState, datesForGroups);
        instance.setRealState(mockFlatTreeParent, Activities.associate, mockFlatTree, datesForGroups);
        verify(instance).getStateNodeAccordingChildren(mockFlatTreeParent, Activities.associate, 0, datesForGroups);
        verify(stateNodeAccordingChildren).get(0);
        verify(stateNodeAccordingChildren).get(1);
        verify(instance).setState(mockFlatNodeParent, Activities.associate, realState, datesForGroups);

    }

}
