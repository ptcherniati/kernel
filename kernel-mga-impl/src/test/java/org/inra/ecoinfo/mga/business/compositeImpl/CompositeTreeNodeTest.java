/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business.compositeImpl;

import org.inra.ecoinfo.mga.business.composite.INode;
import org.junit.*;
import org.mockito.Mockito;

/**
 * @author tcherniatinsky
 */
public class CompositeTreeNodeTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    CompositeTreeNode instance;

    /**
     *
     */
    public CompositeTreeNodeTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        instance = new CompositeTreeNode();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getNode method, of class CompositeTreeNode.
     */
    @Test
    public void testGetNode() {
        CompositeTreeNode instance = new CompositeTreeNode();
        INode expResult = Mockito.mock(INode.class);
        instance.setNode(expResult);
        INode result = instance.getNode();
    }

}
