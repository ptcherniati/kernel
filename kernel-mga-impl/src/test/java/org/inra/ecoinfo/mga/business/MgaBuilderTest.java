/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class MgaBuilderTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    INodeable nodeable = new NodeableImpl("code");
    @Mock(name = "realNode")
    RealNode realNode;
    @Mock(name = "recorder")
    IMgaRecorder recorder;
    @Mock(name = "entityManager")
    EntityManager entityManager;
    MgaBuilder instance;
    /**
     *
     */
    public MgaBuilderTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        instance = new MgaBuilder();
        MockitoAnnotations.openMocks(this);
        when(recorder.getEntityManager()).thenReturn(entityManager);
        instance.setRecorder(recorder);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getOrCreateRealNode method, of class MgaBuilder.
     */
    @Test
    public void testGetParentPath() {
        String path = "a/b/c,u/v,d/e,m/w/h/x";
        MgaBuilder instance = new MgaBuilder();
        String result = instance.getParentPath(path);
        assertEquals("a/b/c,u/v,d/e", result);
        result = instance.getParentPath(result);
        assertEquals("a/b/c,u/v", result);
        result = instance.getParentPath(result);
        assertEquals("a/b/c", result);
        result = instance.getParentPath(result);
        assertEquals("", result);
        result = instance.getParentPath(result);
        assertEquals("", result);
    }

    /**
     *
     */
    @Test
    public void testGetAncestorPath() {
        String path = "a/b/c,u/v,d/e,m/w/h/x";
        MgaBuilder instance = new MgaBuilder();
        String result = instance.getAncestorPath(path);
        assertEquals("m/w/h", result);
        result = instance.getAncestorPath(result);
        assertEquals("m/w", result);
        result = instance.getAncestorPath(result);
        assertEquals("m", result);
        result = instance.getAncestorPath(result);
        assertEquals("", result);
        path = "a/b/c,u/v,d/e";
        result = instance.getAncestorPath(path);
        assertEquals("d", result);
        result = instance.getAncestorPath(result);
        assertEquals("", result);
        path = "u/v";
        result = instance.getAncestorPath(path);
        assertEquals("u", result);
        result = instance.getAncestorPath(result);
        assertEquals("", result);
        path = "a/b/c";
        result = instance.getAncestorPath(path);
        assertEquals("a/b", result);
        result = instance.getAncestorPath(result);
        assertEquals("a", result);
        result = instance.getAncestorPath(result);
        assertEquals("", result);
        result = instance.getAncestorPath(result);
        assertEquals("", result);
    }

    /**
     * Test of getUniqueCode method, of class MgaBuilder.
     */
    @Test
    public void testGetUniqueCode_INodeable_String() {
        INodeable n = new NodeableImpl("code");
        String separator = "/";
        String expResult = "NodeableImpl/code";
        String result = MgaBuilder.getUniqueCode(n, separator);
        assertEquals(expResult, result);
    }

    /**
     * Test of getUniqueCode method, of class MgaBuilder.
     */
    @Test
    public void testGetUniqueCode_3args() {
        String expResult = "NodeableImpl/code";
        String result = MgaBuilder.getUniqueCode(NodeableImpl.class, "/", "code");
        assertEquals(expResult, result);
    }

    /**
     * Test of buildOrderedPaths method, of class MgaBuilder.
     */
    @Test
    public void testBuildOrderedPaths() {
        URL resource = getClass().getResource("texte.csv");
        String splitter = ",";
        Integer[] order = new Integer[]{1, 3, 5};
        String expResult = "variable_2_2,variable_4_2\n"
                + "variable_2_3,variable_4_3";
        Stream<String> result = instance.buildOrderedPaths(resource.getPath(), order, splitter, true);
        String collect = result.collect(Collectors.joining("\n"));
        assertEquals(expResult, collect);
        order = new Integer[]{4, 0, 2};
        expResult = "variable_5_2,variable_1_2,variable_3_2\n"
                + "variable_5_3,variable_1_3,variable_3_3";
        result = instance.buildOrderedPaths(resource.getPath(), order, splitter, true);
        collect = result.collect(Collectors.joining("\n"));
        assertEquals(expResult, collect);
    }

    /**
     * Test of buildLeavesForPathes method, of class MgaBuilder.
     */
    @Test
    public void testBuildLeavesForPathes() {
        RealNode rn1 = mock(RealNode.class);
        when(rn1.getPath()).thenReturn("path1");
        RealNode rn2 = mock(RealNode.class);
        when(rn2.getPath()).thenReturn("path2");
        Stream<RealNode> realNodes = Stream.of(rn1, rn2);
        ArgumentCaptor<Map> realNodes2 = ArgumentCaptor.forClass(Map.class);
        when(recorder.getAllRealNodes()).thenReturn(realNodes);
        instance = spy(instance);
        Stream<String> pathes = Stream.of("path1", "path2");
        Class[] typeResources = new Class[]{};
        Map<String, INodeable> entities = mock(Map.class);
        WhichTree whichTree = WhichTree.TREEDATASET;
        List<? extends INodeable> stickyNodeables = mock(List.class);
        Stream<INode> expResult = null;
        INode n1_1 = mock(INode.class, "1_1");
        INode n1_2 = mock(INode.class, "1_2");
        Stream<INode> list1 = Stream.of(n1_1, n1_2);
        INode n2_1 = mock(INode.class, "2_1");
        INode n2_2 = mock(INode.class, "2_2");
        Stream<INode> list2 = Stream.of(n2_1, n2_2);
        doReturn(list1).when(instance).addLeafForBranchPath(realNodes2.capture(), eq("path1"), eq(typeResources), eq(entities), eq(WhichTree.TREEDATASET), eq(stickyNodeables));
        doReturn(list2).when(instance).addLeafForBranchPath(realNodes2.capture(), eq("path2"), eq(typeResources), eq(entities), eq(WhichTree.TREEDATASET), eq(stickyNodeables));
        Stream<INode> result = instance.buildLeavesForPathes(pathes, typeResources, entities, whichTree, stickyNodeables);
        verify(recorder).getAllRealNodes();
        String collect = result.map(t -> t.toString()).collect(Collectors.joining(";"));
        assertEquals("1_1;1_2;2_1;2_2", collect);
        final Map value = realNodes2.getValue();
        assertEquals(rn1, value.get("path1"));
        assertEquals(rn2, realNodes2.getValue().get("path2"));
        verify(instance).addLeafForBranchPath(value, "path1", typeResources, entities, WhichTree.TREEDATASET, stickyNodeables);
        verify(instance).addLeafForBranchPath(value, "path2", typeResources, entities, WhichTree.TREEDATASET, stickyNodeables);

    }

    /**
     * Test of getOrCreateRealNode method, of class MgaBuilder.
     */
    @Test
    public void testGetOrCreateRealNode() {
        RealNode rn1 = mock(RealNode.class, "rn1");
        String path1 = "path1";
        when(rn1.getPath()).thenReturn(path1);
        RealNode rn2 = mock(RealNode.class, "rn2");
        String path2 = "path2";
        when(rn2.getPath()).thenReturn(path2);
        Map<String, RealNode> realNodes = Stream.of(rn1, rn2)
                .collect(Collectors.toMap(RealNode::getPath, c -> c));
        Map<String, INodeable> entities = mock(Map.class, "entities");
        Class[] typeResources = new Class[]{};
        instance = spy(instance);
//null path
        RealNode result = instance.getOrCreateRealNode(realNodes, null, entities, typeResources);
        assertNull(result);
//empty path
        result = instance.getOrCreateRealNode(realNodes, "", entities, typeResources);
        assertNull(result);
//path in map
        result = instance.getOrCreateRealNode(realNodes, path1, entities, typeResources);
        assertEquals(rn1, result);
//path ,ot in map
        String path = "path";
        doReturn(Optional.ofNullable(nodeable)).when(instance).getEntityFromPath(path, typeResources, entities);
        doReturn(path1).when(instance).getParentPath(path);
        doReturn(path2).when(instance).getAncestorPath(path);
        doReturn(rn1).when(instance).getOrCreateRealNode(realNodes, path1, entities, typeResources);
        doReturn(rn2).when(instance).getOrCreateAnscestorRealNode(realNodes, NodeableImpl.class, path2, entities);
        result = instance.getOrCreateRealNode(realNodes, path, entities, typeResources);
        assertEquals(rn1, result.getParent());
        assertEquals(rn2, result.getAncestor());
        assertEquals(path, result.getPath());
        assertEquals("code", result.getCode());
        verify(recorder).saveOrUpdate(result);
        assertEquals(realNodes.get(path), result);
    }

    /**
     * Test of getOrCreateAnscestorRealNode method, of class MgaBuilder.
     */
    @Test
    public void testGetOrCreateAnscestorRealNode() {
        RealNode rn1 = mock(RealNode.class, "rn1");
        String path1 = "code";
        when(rn1.getPath()).thenReturn(path1);
        RealNode rn2 = mock(RealNode.class, "rn2");
        String path2 = "path2";
        when(rn2.getPath()).thenReturn(path2);
        Map<String, RealNode> realNodes = Stream.of(rn1, rn2)
                .collect(Collectors.toMap(RealNode::getPath, c -> c));
        Map<String, INodeable> entities = mock(Map.class, "entities");
        Class[] typeResources = new Class[]{};
        instance = spy(instance);
        //null path
        RealNode result = instance.getOrCreateAnscestorRealNode(realNodes, NodeableImpl.class, null, entities);
        assertNull(result);
        //empty path
        result = instance.getOrCreateAnscestorRealNode(realNodes, NodeableImpl.class, "", entities);
        assertNull(result);
        //existing path
        doReturn(Optional.ofNullable(nodeable)).when(instance).getEntityFromPath("path2", NodeableImpl.class, entities);
        doReturn(nodeable).when(entityManager).merge(nodeable);
        result = instance.getOrCreateAnscestorRealNode(realNodes, NodeableImpl.class, "path2", entities);
        assertEquals(rn2, result);
        //not existing path
        realNodes.clear();
        doReturn(path2).when(instance).getAncestorPath("path");
        doReturn(rn2).when(instance).getOrCreateAnscestorRealNode(realNodes, NodeableImpl.class, path2, entities);
        doReturn(Optional.ofNullable(nodeable)).when(instance).getEntityFromPath("path", NodeableImpl.class, entities);
        result = instance.getOrCreateAnscestorRealNode(realNodes, NodeableImpl.class, "path", entities);
        assertNull(result.getParent());
        assertEquals(rn2, result.getAncestor());
        assertEquals("code", result.getCode());
        assertEquals("path", result.getPath());
        verify(recorder).saveOrUpdate(result);
        assertEquals(result, realNodes.get("path"));
    }

    /**
     * Test of createBranchNode method, of class MgaBuilder.
     */
    @Test
    public void testCreateBranchNode() {
        NodeDataSet abn1 = mock(NodeDataSet.class, "abn1");
        NodeDataSet abn2 = mock(NodeDataSet.class, "abn2");
        RealNode rn1 = mock(RealNode.class, "rn1");
        String path1 = "nodeableimpl/code";
        when(rn1.getPath()).thenReturn(path1);
        INodeable ndb1 = mock(INodeable.class);
        when(rn1.getNodeable()).thenReturn(ndb1);
        when(ndb1.getCode()).thenReturn("ndb1");
        RealNode rn2 = mock(RealNode.class, "rn2");
        INodeable ndb2 = mock(INodeable.class);
        when(rn2.getNodeable()).thenReturn(ndb2);
        when(ndb2.getCode()).thenReturn("ndb2");
        String path2 = "path2";
        when(rn2.getPath()).thenReturn(path2);
        Map<String, RealNode> realNodes = Stream.of(rn1, rn2)
                .collect(Collectors.toMap(RealNode::getPath, c -> c));
        Map<String, INodeable> entities = mock(Map.class, "entities");
        Class[] typeResources = new Class[]{};
        instance = spy(instance);
        doReturn(rn1).when(instance).getOrCreateRealNode(realNodes, "path", entities, typeResources);
        doReturn(Optional.ofNullable(nodeable)).when(instance).getEntityFromPath("path", typeResources, entities);
        doReturn(path1).when(instance).getParentPath("path");
        doReturn(path2).when(instance).getAncestorPath("path");
        doReturn(abn1).when(instance).createBranchNode(realNodes, path1, entities, typeResources, WhichTree.TREEDATASET);
        doReturn(abn2).when(instance).createAnscestorBranchNode(realNodes, NodeableImpl.class, path2, entities, WhichTree.TREEDATASET);
        AbstractBranchNode result = instance.createBranchNode(realNodes, "path", entities, typeResources, WhichTree.TREEDATASET);
        assertEquals(abn1, result.getParent());
        assertEquals(abn2, result.getAncestor());
        assertEquals(rn1, result.getRealNode());
    }

    /**
     * Test of createAnscestorBranchNode method, of class MgaBuilder.
     */
    @Test
    public void testCreateAnscestorBranchNode() {
        NodeDataSet abn1 = mock(NodeDataSet.class, "abn1");
        NodeDataSet abn2 = mock(NodeDataSet.class, "abn2");
        RealNode rn1 = mock(RealNode.class, "rn1");
        String path1 = "nodeableimpl/code";
        INodeable ndb1 = mock(INodeable.class);
        when(rn1.getNodeable()).thenReturn(ndb1);
        when(ndb1.getCode()).thenReturn("ndb1");
        when(rn1.getPath()).thenReturn(path1);
        RealNode rn2 = mock(RealNode.class, "rn2");
        INodeable ndb2 = mock(INodeable.class);
        when(rn2.getNodeable()).thenReturn(ndb2);
        when(ndb2.getCode()).thenReturn("ndb2");
        String path2 = "path2";
        when(rn2.getPath()).thenReturn(path2);
        Map<String, RealNode> realNodes = Stream.of(rn1, rn2)
                .collect(Collectors.toMap(RealNode::getPath, c -> c));
        Map<String, INodeable> entities = mock(Map.class, "entities");
        Class[] typeResources = new Class[]{};
        instance = spy(instance);
        doReturn(Optional.ofNullable(nodeable)).when(instance).getEntityFromPath("path", NodeableImpl.class, entities);
        doReturn(rn2).when(instance).getOrCreateAnscestorRealNode(realNodes, NodeableImpl.class, "path", entities);
        doReturn(path2).when(instance).getAncestorPath("path");
        doReturn(abn2).when(instance).createAnscestorBranchNode(realNodes, NodeableImpl.class, path2, entities, WhichTree.TREEDATASET);
        AbstractBranchNode result = instance.createAnscestorBranchNode(realNodes, NodeableImpl.class, "path", entities, WhichTree.TREEDATASET);
        assertEquals(rn2, result.getRealNode());
        assertNull(result.getParent());
        assertEquals(abn2, result.getAncestor());
    }

    /**
     * Test of addLeafForBranchPath method, of class MgaBuilder.
     */
    @Test
    public void testAddLeafForBranchPath() {
        NodeDataSet abn1 = mock(NodeDataSet.class, "abn1");
        NodeDataSet abn2 = mock(NodeDataSet.class, "abn2");
        RealNode rn1 = mock(RealNode.class, "rn1");
        String path1 = "nodeableimpl/code";
        when(rn1.getPath()).thenReturn(path1);
        RealNode rn2 = mock(RealNode.class, "rn2");
        String path2 = "path2";
        when(rn2.getPath()).thenReturn(path2);
        Map<String, RealNode> realNodes = Stream.of(rn1, rn2)
                .collect(Collectors.toMap(RealNode::getPath, c -> c));
        Map<String, INodeable> entities = mock(Map.class, "entities");
        Class[] typeResources = new Class[]{};
        instance = spy(instance);
        doReturn(abn1).when(instance).createBranchNode(realNodes, "path", entities, typeResources, WhichTree.TREEDATASET);
        //null sticky nodeable
        Stream<INode> result = instance.addLeafForBranchPath(realNodes, "path", typeResources, entities, WhichTree.TREEDATASET, null);
        INode nodeResult = result.findFirst().orElse(null);
        assertEquals(abn1, nodeResult);
        //empty sticky nodeables
        result = instance.addLeafForBranchPath(realNodes, "path", typeResources, entities, WhichTree.TREEDATASET, new LinkedList());
        nodeResult = result.findFirst().orElse(null);
        assertEquals(abn1, nodeResult);
        //not empty sticky nodeables
        doReturn(abn2).when(instance).addStickyLeafForPathAndLeafNode(nodeable, abn1, "path", WhichTree.TREEDATASET);
        result = instance.addLeafForBranchPath(realNodes, "path", typeResources, entities, WhichTree.TREEDATASET, Stream.of(nodeable).collect(Collectors.toList()));
        nodeResult = result.findFirst().orElse(null);
        assertEquals(abn2, nodeResult);
        verify(instance).addStickyLeafForPathAndLeafNode(nodeable, abn1, "path", WhichTree.TREEDATASET);
    }

    /**
     * Test of addStickyLeafForPathAndLeafNode method, of class MgaBuilder.
     */
    @Test
    public void testAddStickyLeafForPathAndLeafNode() {
        NodeDataSet abn1 = mock(NodeDataSet.class, "abn1");
        when(abn1.getRealNode()).thenReturn(realNode);
        ArgumentCaptor<RealNode> rn = ArgumentCaptor.forClass(RealNode.class);
        INode result = instance.addStickyLeafForPathAndLeafNode(nodeable, abn1, "path", WhichTree.TREEDATASET);
        verify(recorder).saveOrUpdate(rn.capture());
        assertEquals(rn.getValue(), result.getRealNode());
        assertEquals(realNode, rn.getValue().getParent());
        assertNull(rn.getValue().getAncestor());
        assertEquals(abn1, result.getParent());
        assertNull(result.getAncestor());
    }

    /**
     * Test of getEntityFromPath method, of class MgaBuilder.
     */
    @Test
    public void testGetEntityFromPath_3args_1() {
        String path = "path1,path2";
        NodeableImpl nodeable1 = new NodeableImpl("code1");
        NodeableImpl2 nodeable2 = new NodeableImpl2("code2");
        Class[] typeResources = new Class[]{NodeableImpl.class, NodeableImpl2.class};
        Map<String, INodeable> entities = new HashMap<>();
        entities.put("NodeableImpl/path1", nodeable1);
        entities.put("NodeableImpl2/path2", nodeable2);
        Optional<INodeable> expResult = Optional.ofNullable(nodeable2);
        Optional<INodeable> result = instance.getEntityFromPath(path, typeResources, entities);
        assertEquals(expResult, result);
    }

    /**
     * Test of getEntityFromPath method, of class MgaBuilder.
     */
    @Test
    public void testGetEntityFromPath_3args_2() {
        instance = spy(instance);
        String path = "path1,path2";
        Map<String, INodeable> entities = new HashMap<>();
        NodeableImpl nodeable1 = new NodeableImpl("code1");
        NodeableImpl2 nodeable2 = new NodeableImpl2("code2");
        entities.put("nodeableimpl/path1", nodeable1);
        entities.put("nodeableimpl2/path2", nodeable2);
        final Class<NodeableImpl2> typeResource = NodeableImpl2.class;
        final Class[] typeResources = new Class[]{typeResource};
        Optional<INodeable> expResult = Optional.ofNullable(nodeable2);

        doReturn(Optional.ofNullable(nodeable2)).when(instance).getEntityFromPath(path, typeResources, entities);
        Optional result = instance.getEntityFromPath(path, typeResource, entities);
        assertEquals(expResult, result);
    }

    /**
     * Test of setRecorder method, of class MgaBuilder.
     */
    @Test
    public void testSetRecorder() {
        assertEquals(recorder, instance.recorder);
    }

    /**
     * Test of orderLine method, of class MgaBuilder.
     */
    @Test
    public void testOrderLine() {
        String line = "text1,text2,text3,text4";
        String splitter = ",";
        Integer[] order = new Integer[]{2, 0, 3, 1};
        MgaBuilder instance = new MgaBuilder();
        String expResult = "text3,text1,text4,text2";
        String result = instance.orderLine(line, splitter, order);
        assertEquals(expResult, result);
        order = new Integer[]{2, 3};
        result = instance.orderLine(line, splitter, order);
        expResult = "text3,text4";
        assertEquals(expResult, result);
    }

    class NodeableImpl extends Nodeable {

        public NodeableImpl(String code) {
            setCode(code);
        }

        @Override
        public String getName() {
            return "Nodeable Impl";
        }

        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) NodeableImpl.class;
        }

    }

    class NodeableImpl2 extends Nodeable {

        public NodeableImpl2(String code) {
            setCode(code);
        }

        @Override
        public String getName() {
            return "Nodeable Impl";
        }

        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) NodeableImpl.class;
        }

    }

}
