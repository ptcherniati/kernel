/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.junit.*;
import static org.junit.Assert.*;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class MgaDisplayerConfigurationTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    AbstractMgaDisplayerConfiguration instance;

    /**
     *
     */
    public MgaDisplayerConfigurationTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new DisplayerMgaDisplayerConfiguration();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getDisplayColumnNames method, of class MgaDisplayerConfiguration.
     */
    @Test
    public void testGetDisplayColumnNames() {
        assertTrue(instance.getDisplayColumnNames().isEmpty());
        String[] columnNames = new String[]{"1", "2"};
        instance.addColumnNamesForInstanceType(Nodeable.class, columnNames);
        assertTrue(instance.getDisplayColumnNames().size() == 1);
        assertArrayEquals(columnNames, instance.getDisplayColumnNames().get(Nodeable.class));
    }

    /**
     * Test of autoUpdateColumn method, of class MgaDisplayerConfiguration.
     */
    @Test
    public void testAutoUpdateColumn() {
        String[] columnNames1 = new String[]{"1", "2"};
        String[] columnNames2 = new String[]{"3"};
        String[] columnNames3 = new String[]{"4", "5", "6"};
        instance.getDisplayColumnNames().clear();
        instance.addColumnNamesForInstanceType(Nodeable.class, columnNames1);
        instance.addColumnNamesForInstanceType(MgaIOConfigurationTest.Site.class, columnNames2);
        instance.addColumnNamesForInstanceType(MgaIOConfigurationTest.Theme.class, columnNames3);
        assertFalse(instance.getDisplayColumnNames().values().stream().allMatch(t -> t.length >= 3));
        instance.autoUpdateColumn(4);
        assertTrue(instance.getDisplayColumnNames().size() == 3);
        assertTrue(instance.getDisplayColumnNames().values().stream().allMatch(
                t -> t.length == 5)
        );

    }

    class DisplayerMgaDisplayerConfiguration extends AbstractMgaDisplayerConfiguration {

    }

}
