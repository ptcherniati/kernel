/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.caching.jpa;
import java.time.Instant;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.caching.CachingUpdate;
import org.inra.ecoinfo.mga.caching.CachingUpdate_;
import org.inra.ecoinfo.mga.caching.ICachingDAO;

/**
 *
 * @author ptcherniati
 */
public class JPACachingUpdateDAO extends  AbstractJPADAO<CachingUpdate> implements ICachingDAO{

    @Override
    public Optional<Instant> getInstantCaching(Integer configuration) {
        CriteriaQuery<Instant> query = builder.createQuery(Instant.class);
        Root<CachingUpdate> instant = query.from(CachingUpdate.class);
        query
                .select(instant.get(CachingUpdate_.updateDate))
                .where(builder.equal(instant.get(CachingUpdate_.configurationNumber), configuration));
        return getOptional(query);
    }
}
