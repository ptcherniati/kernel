/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business;

import java.util.*;
import org.inra.ecoinfo.mga.business.composite.IDataNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;

/**
 *
 * @author tcherniatinsky
 * @param <T>
 */
public class DefaultTreeNode<T extends IDataNode> implements ITreeNode<T> {

    T data;
    String type;
    List<ITreeNode<T>> children = new ArrayList();
    ITreeNode<T> parent;
    IMgaIOConfiguration config;

    /**
     *
     * @param type
     * @param data
     * @param parent
     * @param config
     */
    public DefaultTreeNode(String type, T data, ITreeNode<T> parent, IMgaIOConfiguration config) {
        this.type = type;
        this.data = data;
        this.parent = parent;
        this.config = config;
        Optional.ofNullable(parent)
                .map(p -> p.getChildren())
                .ifPresent(children -> children.add(this));
    }

    DefaultTreeNode(DefaultTreeNode node) {
        this.children = new LinkedList<>();
        this.type = node.type;
        this.data = (T) node.getData();
        this.config = node.getConfiguration();
    }

    DefaultTreeNode(T data, IMgaIOConfiguration config) {
        this.data = data;
        this.config = config;
    }

    @Override
    public String toString() {
        return data == null ? "root node" : String.format("id=%s->%s", data.getId(), data.getPath());
    }

    /**
     *
     * @return
     */
    @Override
    public IMgaIOConfiguration getConfiguration() {
        return config;
    }

    /**
     *
     * @return
     */
    public long getId() {
        return data.getId();
    }

    /**
     *
     * @return
     */
    @Override
    public List getChildren() {
        return children;
    }

    /**
     *
     * @return
     */
    @Override
    public T getData() {
        return data;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isLeaf() {
        return children.isEmpty();
    }

    /**
     *
     * @return
     */
    @Override
    public ITreeNode getParent() {
        return parent;
    }

    void setParent(ITreeNode<T> parent) {
        this.parent = parent;
        if (!parent.getChildren().contains(this)) {
            parent.getChildren().add(this);
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash+=Optional.ofNullable(data).map(d->d.getId().hashCode()).orElse(0);
        hash+=getChildren().stream().mapToInt(c->c.hashCode()).sum();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultTreeNode<?> other = (DefaultTreeNode<?>) obj;
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        if (!Objects.equals(this.children, other.children)) {
            return false;
        }
        return true;
    }

}
