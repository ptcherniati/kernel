package org.inra.ecoinfo.mga.caching;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;

/**
 *
 * @author ryahiaoui
 */
public class TreeSessionCacheManager implements ITreeSessionCacheManager {

    private static final int SIZE_OF_TREE = 3;
    private final Map<ICompositeGroup, Map<Integer, ITreeNode>> treeFlatMap;
    private final Map<ITreeNode, Instant> treeTimestamp;
    private ITreeApplicationCacheManager treeApplicationCacheManager;

    /**
     *
     */
    public TreeSessionCacheManager() {
        this.treeFlatMap = new HashMap();
        this.treeTimestamp = new HashMap();
    }

    /**
     *
     */
    @Override
    public void initMap() {
        this.treeFlatMap.clear();
    }

    /**
     *
     * @param compositeGroup
     * @param codeConf
     * @return
     */
    @Override
    public ITreeNode getFlatTree(ICompositeGroup compositeGroup, Integer codeConfiguration) {
        final ITreeNode tree = this.treeFlatMap
                .getOrDefault(compositeGroup, new HashMap<>())
                .get(codeConfiguration);
        return tree;
    }

    /**
     *
     * @param compositeGroup
     * @param treeNode
     * @param codeConf
     */
    @Override
    public void saveFlatTree(ICompositeGroup compositeGroup, ITreeNode treeNode, Integer codeConf) {
        if (this.treeFlatMap
                .getOrDefault(compositeGroup, new HashMap<>()).size() > SIZE_OF_TREE) {
            ICompositeGroup keyToRemove = this.treeFlatMap.keySet().stream().findFirst().orElse(null);
            this.treeFlatMap.getOrDefault(compositeGroup, new HashMap<>()).remove(keyToRemove);
        }
        this.treeFlatMap
                .computeIfAbsent(compositeGroup, k -> new HashMap<>())
                .put(codeConf, treeNode);
        this.treeTimestamp.put(treeNode, treeApplicationCacheManager.getTimeStamp(codeConf));
    }

    /**
     *
     * @param compositeGroup
     * @param codeConf
     */
    @Override
    public void removeFlatTree(ICompositeGroup compositeGroup, Integer codeConfiguration) {
        treeApplicationCacheManager.getConfigurationMap().values().stream()
                .filter(list -> list.contains(codeConfiguration))
                .flatMap(Collection::stream)
                .distinct()
                .forEach(k->
                        this.treeFlatMap
                        .getOrDefault(compositeGroup, new HashMap<>())
                        .remove(codeConfiguration)
                );
        ;
    }

    /**
     *
     */
    @Override
    public void clearFlatTree() {
        this.treeFlatMap.clear();
    }

    /**
     *
     * @param compositeGroup
     * @param codeConf
     * @return
     */
    @Override
    public boolean containKeyFlatTree(ICompositeGroup compositeGroup, Integer codeConfiguration) {
        final ITreeNode tree = this.treeFlatMap
                .getOrDefault(compositeGroup, new HashMap<>())
                .get(codeConfiguration);        
        if(tree==null){
            return false;
        }
        Instant timestamp = treeTimestamp.get(tree);
        Instant timestamp2 = treeApplicationCacheManager.getTimeStamp(codeConfiguration);
        if(timestamp.isBefore(timestamp2)){
            treeTimestamp.remove(tree);
            treeFlatMap.remove(codeConfiguration);
            return false;
        }
        return true;
    }

    public void setTreeApplicationCacheManager(ITreeApplicationCacheManager treeApplicationCacheManager) {
        this.treeApplicationCacheManager = treeApplicationCacheManager;
    }
}
