/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.deployment;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.mga.caching.CachingUpdate;
import org.inra.ecoinfo.mga.caching.ICachingDAO;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.viewadapter.ISkeletonBuilder;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ryahiaoui
 */
@Component
public class OnDeploymentListener implements ApplicationListener<ContextRefreshedEvent> {

    private static Map<String, ISkeletonBuilder> treeApplicationCacheManagers = new HashMap<>();
    private static ApplicationContext context;
    private static ISkeletonBuilder treeBuilder;

    /**
     *
     * @return
     */
    public static ISkeletonBuilder getTreeBuilder() {
        return treeBuilder;
    }

    /**
     *
     * @param configuration
     * @return
     */
    public static ISkeletonBuilder getSkeletonBuilder(IMgaIOConfiguration configuration) {
        return treeApplicationCacheManagers.getOrDefault(configuration.getSkeletonBuilder(), treeBuilder);
    }

    /**
     *
     * @param event
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void onApplicationEvent(ContextRefreshedEvent event) {

        context = event.getApplicationContext();
        treeApplicationCacheManagers = Stream.of(context.getBeanNamesForType(ISkeletonBuilder.class))
                .collect(Collectors.toMap(name -> name, name -> (ISkeletonBuilder) context.getBean(name)));
        treeBuilder = (ISkeletonBuilder) context.getBean("treeBuilder");
        try {
            ICachingDAO cachingDAO = (ICachingDAO) context.getBean("mgaCachingUpdate");
            cachingDAO.removeAll(CachingUpdate.class);
        } catch (PersistenceException ex) {
            LoggerFactory.getLogger(getClass()).error("can't purge CachingUpdate table in database");
        }
        IMgaIOConfigurator mgaIOConfigurator = (IMgaIOConfigurator) context.getBean("mgaIOConfigurator");
        mgaIOConfigurator.getConfigurations().values()
                .stream()
                .filter(t -> t.loadOnStartUp())
                .forEach(config -> treeApplicationCacheManagers.getOrDefault(config.getSkeletonBuilder(), treeBuilder).getOrLoadSkeletonTree(config));
    }


}
