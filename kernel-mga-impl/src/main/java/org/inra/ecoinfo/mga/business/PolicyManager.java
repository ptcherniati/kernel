package org.inra.ecoinfo.mga.business;

import com.google.common.base.Strings;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.MapUtils;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.ActivityCondition;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.mga.viewadapter.ITreeBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * u
 *
 * @author yahiaoui
 */
public class PolicyManager implements IPolicyManager {

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(PolicyManager.class);
    public static final Logger LOGGER_CONNECTION = LoggerFactory.getLogger("connection.logger");
    public static final ICompositeGroup publicCompositeGroup = null;
    /**
     *
     */
    public static final String GOOD_EXPRESSION_PATTERN_REGEXP = PatternConfigurator.GOOD_EXPRESSION_PATTERN_REGEXP;
    private static final String PROPERTY_MSG_EXISTING_GROUP = "PROPERTY_MSG_EXISTING_GROUP";
    private static final String PROPERTY_MSG_UNKNOWN_GROUP = "PROPERTY_MSG_UNKNOWN_GROUP";
    private static final String PROPERTY_MSG_USER_GROUP = "PROPERTY_MSG_USER_GROUP";
    private static final String PROPERTY_MSG_INVALID_GROUP_NAME = "PROPERTY_MSG_INVALID_GROUP_NAME";
    private static final String PROPERTY_MSG_RESERVED_WORDS = "PROPERTY_MSG_RESERVED_WORDS";
    private static final List<String> RESERVED_WORDS = Arrays.asList(new String[]{"public", "admin"});

    /**
     *
     * @param <T>
     * @param path
     * @return
     */
    public static final <T extends IDataNode> Predicate<ITreeNode<T>> filterTreeByPath(String path) {
        return t -> t.getData() != null && t.getData().getPath().equals(path);
    }

    /**
     *
     * @param <T>
     * @param id
     * @return
     */
    public static final <T extends IDataNode> Predicate<ITreeNode<T>> filterTreeById(Long id) {
        return t -> t.getData() != null && t.getData().getId().equals(id);
    }

    /**
     *
     * @param activityOrActivitiesList
     * @return
     */
    protected static boolean isActivityOrActivitiesList(String activityOrActivitiesList) {
        if (Strings.isNullOrEmpty(activityOrActivitiesList)) {
            return false;
        }
        String[] activities = activityOrActivitiesList.split(Pattern.quote(","));
        return Arrays.stream(activities).allMatch(p -> isActivity(p));
    }

    /**
     *
     * @param activity
     * @return
     */
    protected static boolean isActivity(String activity) {
        return !Strings.isNullOrEmpty(activity) && Activities.isAnActivity(activity);
    }

    /**
     *
     */
    // protected IMgaServiceBuilder mgaServiceBuilder;
    /**
     *
     */
    protected ITreeBuilder treeBuilder;

    /**
     *
     */
    protected final Map<WhichTree, Map<ICompositeGroup, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>>>> maactivityUsers;

    /**
     *
     */
    //protected final HashMap<String, TreeNode> mapTrees;
    /**
     *
     */
    protected IUser user;

    /**
     *
     */
    public PolicyManager() {

        this.maactivityUsers = new HashMap();
    }

    /**
     *
     * @param treeBuilder
     */
    public void settreeBuilder(ITreeBuilder treeBuilder) {
        this.treeBuilder = treeBuilder;
    }

    /**
     *
     * @param codeConfiguration
     */
    @Override
    public void removeTreeFromSession(ICompositeGroup compositeGroup, Integer codeConfiguration) {
        treeBuilder.removeTreeFromSession(compositeGroup, codeConfiguration);
    }

    /**
     *      * return the roles map for user on whichTree</p>
     *
     * @param compositeGroup
     * @param whichTree the kind of role map
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class, noRollbackFor = Exception.class)
    public Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> getOrLoadActivities(ICompositeGroup compositeGroup, WhichTree whichTree) {

        if (maactivityUsers
                .getOrDefault(whichTree, new HashMap())
                .containsKey(compositeGroup)) {
            return maactivityUsers.get(whichTree).get(compositeGroup);
        }
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles = getRoles(compositeGroup, whichTree);
        maactivityUsers.computeIfAbsent(whichTree, k -> new HashMap<>()).put(compositeGroup, roles);
        return Optional.ofNullable(roles).orElse(new HashMap());

    }

    /**
     *
     * @param compositeGroup
     * @param whichTree
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class, noRollbackFor = Exception.class)
    public List<Group> getGroups(ICompositeGroup compositeGroup, WhichTree whichTree) {
        return treeBuilder.getMgaServiceBuilder().getRecorder()
                .getGroups(compositeGroup, whichTree)
                .collect(Collectors.toList());
    }

    /**
     *
     * @param compositeGroup
     * @param whichTree
     * @return
     */
    protected Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> getRoles(ICompositeGroup compositeGroup, WhichTree whichTree) {
        Stream<Group> croles = treeBuilder.getMgaServiceBuilder().getRecorder().getGroups(compositeGroup, whichTree);
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles = new HashMap<>();
        croles.forEach(groupEntry -> addGroupEntryToActivitiesMap(roles, groupEntry));
        return roles;
    }

    /**
     *      * An expression is as <ul>
     * <li>E->(piegeage_en_montee:datatype,mon_theme:theme,nom_site:site
     * ||piegeage_en_montee:datatype )</li>
     * <li>E->(nom_theme:theme,nom_site:site|nom_site:site )</li></ul></p>
     *
     *      * activities as depot,extaction
     *
     * @param compositeGroup
     * @param codeConfiguration
     * @param priv
     * @param expression
     * @return
     */
    @Override
    public boolean checkActivity(ICompositeGroup compositeGroup, Integer codeConfiguration, String priv, String expression) {

        if (getCurrentUserLogin() == null) {
            return false;
        } else if (isRoot()) {
            return true;
        }

        if (!isExpression(expression)) {
            return checkActivityWithNotExpression(priv, compositeGroup, codeConfiguration);
        }
        ITreeNode<IFlatNode> treeActivities = getOrLoadTree(compositeGroup, codeConfiguration);

        final WhichTree whichTree = treeBuilder.getMgaServiceBuilder()
                .getMgaIOConfigurator()
                .getConfiguration(codeConfiguration)
                .map(codeConf -> codeConf.getWhichTree())
                .orElse(WhichTree.TREEDATASET);

        if (treeActivities.getData() == null && codeConfiguration != null) {
            return treeActivities.getChildren().stream()
                    .anyMatch(t -> decodeExpression(compositeGroup, expression, priv, t, whichTree));
        }
        return decodeExpression(compositeGroup, expression, priv, treeActivities, whichTree);

        // return false;
    }

    /**
     *
     * @param selectdUserOrGroup
     * @param codeConfiguration
     * @return
     */
    @Override
    @Transactional()
    public ITreeNode<IFlatNode> getOrLoadTree(ICompositeGroup selectdUserOrGroup, Integer codeConfiguration) {
        Optional<IMgaIOConfiguration> configuration = getMgaServiceBuilder().getMgaIOConfigurator().getConfiguration(codeConfiguration);
        final WhichTree whichTree = configuration
                .map(codeConf -> codeConf.getWhichTree())
                .orElse(WhichTree.TREEDATASET);
        selectdUserOrGroup = Optional.ofNullable(selectdUserOrGroup)
                .orElse(getPublicCompositeGroupe());
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> selectedUserActivities = getOrLoadActivities(selectdUserOrGroup, whichTree);
        return treeBuilder.getOrLoadTree(configuration, selectedUserActivities, selectdUserOrGroup);
    }

    /**
     *
     * @param priv
     * @param compositeGroup
     * @param codeConfiguration
     * @return
     */
    protected boolean checkActivityWithNotExpression(String priv, ICompositeGroup compositeGroup, Integer codeConfiguration) {
        if (Strings.isNullOrEmpty(priv)) {
            return true;
        } else if (priv.trim().equals(PatternConfigurator.ASTERIX)) {
            final WhichTree whichTree = treeBuilder.getMgaServiceBuilder()
                    .getMgaIOConfigurator()
                    .getConfiguration(codeConfiguration)
                    .map(codeConf -> codeConf.getWhichTree())
                    .orElse(WhichTree.TREEDATASET);
            final Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles = getOrLoadActivities(compositeGroup, whichTree);
            return !roles.isEmpty();
        }
        return isActivityOrActivitiesList(priv) && checKActivityWithNotExpresionAndNotGenericsActivities(priv, compositeGroup, codeConfiguration);
    }

    /**
     *
     * @param priv
     * @param compositeGroup
     * @param codeConfiguration
     * @return
     */
    protected boolean checKActivityWithNotExpresionAndNotGenericsActivities(String priv, ICompositeGroup compositeGroup, Integer codeConfiguration) {
        String[] privils = priv.trim().toLowerCase().split(Pattern.quote(","));
        final WhichTree whichTree = treeBuilder.getMgaServiceBuilder()
                .getMgaIOConfigurator()
                .getConfiguration(codeConfiguration)
                .map(codeConf -> codeConf.getWhichTree())
                .orElse(WhichTree.TREEDATASET);
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role = getOrLoadActivities(compositeGroup, whichTree);
        return Arrays.asList(privils).stream().anyMatch(privil
                -> isActivityOrActivitiesList(privil) && role.containsKey(Activities.convertToActivity(privil)));
    }

    /**
     *
     * @param <T>
     * @param root
     * @param id
     * @return
     */
    @Override
    public <T extends IDataNode> T getNodeById(ITreeNode<T> root, Long id) {

        final T data = root.getData();
        return Optional.ofNullable(data)
                .filter(d -> d.getId() == id)
                .orElseGet(() -> createNewData(root, id));
    }

    private <T extends IDataNode> T createNewData(ITreeNode<T> root, Long id) {
        Optional<T> nodeById = root.getChildren().stream()
                .map((childrenTree) -> {
                    final T childData = childrenTree.getData();
                    T r = null;
                    if (childData == null) {
                        return null;
                    }
                    if (childData.getId() == id) {
                        r = childData;
                    } else if (!childrenTree.isLeaf()) {
                        r = getNodeById(childrenTree, id);
                    }

                    if (r != null) {
                        return r;
                    } else {
                        return null;
                    }
                })
                .filter(r -> r != null)
                .findFirst();
        return nodeById.isPresent() ? nodeById.get() : null;
    }

    /**
     *
     * @param tree
     * @param activityUser
     * @param conditionPrivilRestriction
     * @param priv
     * @return
     */
    @Override
    public ITreeNode restrictTree(ITreeNode tree,
            Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activityUser,
            ActivityCondition conditionPrivilRestriction,
            List<Activities> priv) {

        if (!isRoot()) {
            for (Iterator<ITreeNode> iterator
                    = tree.getChildren().iterator();
                    iterator.hasNext();) {

                ITreeNode treeNode = iterator.next();
                IFlatNode node = (IFlatNode) treeNode.getData();
                Long id = node.getId();

                if (conditionPrivilRestriction == ActivityCondition.ALL) {
                    restrictCaseUsingAllActivity(priv, activityUser, id, iterator);
                } else if (conditionPrivilRestriction == ActivityCondition.ATLEAST) {
                    restrictCaseUsingAtleastOneActivity(priv, activityUser, id, iterator);
                }

                restrictLeavesIfExists(treeNode, activityUser, conditionPrivilRestriction, priv);
            }
        }

        return tree;
    }

    /**
     *
     * @param treeNode
     * @param activityUser
     * @param conditionPrivilRestriction
     * @param priv
     */
    protected void restrictLeavesIfExists(ITreeNode treeNode, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activityUser, ActivityCondition conditionPrivilRestriction, List<Activities> priv) {
        if (!treeNode.isLeaf()) {
            restrictTree(treeNode, activityUser, conditionPrivilRestriction, priv);
        }
    }

    /**
     *      * remove iterator node if there isn't any activity in priv that contains
     * id key
     *
     * @param priv
     * @param activityUser
     * @param id
     * @param iterator
     */
    protected void restrictCaseUsingAtleastOneActivity(List<Activities> priv, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activityUser, Long id, Iterator<ITreeNode> iterator) {
        boolean atLeastOnePrivMatchForId = priv.stream()
                .anyMatch(k -> activityUser.containsKey(k) && activityUser.get(k).containsKey(id));
        if (!atLeastOnePrivMatchForId) {
            iterator.remove();
        }
    }

    /**
     *      * remove iterator node if any values of activityUser for key in priv
     * doesn't contains id key
     *
     * @param priv
     * @param activityUser
     * @param id
     * @param iterator
     */
    protected void restrictCaseUsingAllActivity(List<Activities> priv, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activityUser, Long id, Iterator<ITreeNode> iterator) {
        boolean existOnePrivThatDoesntMatch = priv.stream()
                .anyMatch(k -> !activityUser.containsKey(k) || !activityUser.get(k).containsKey(id));

        if (existOnePrivThatDoesntMatch) {
            iterator.remove();
        }
    }

    /**
     *
     * @param tree
     * @param conditionPrivilRestriction
     * @param activities
     * @return
     */
    @Override
    public DefaultTreeNode<IFlatNode> restrictTreeOnLeafsAccordingActivities(ITreeNode<IFlatNode> tree, ActivityCondition conditionPrivilRestriction, List<Activities> activities) {
        DefaultTreeNode<IFlatNode> restrictedTree = new DefaultTreeNode<>((DefaultTreeNode) tree);
        if (!isRoot()) {
            for (Iterator<ITreeNode<IFlatNode>> iterator = tree.getChildren().iterator(); iterator.hasNext();) {
                ITreeNode<IFlatNode> childTreeNode = iterator.next();
                IFlatNode node = childTreeNode.getData();
                final DefaultTreeNode<IFlatNode> leafReturnNode;

                if (conditionPrivilRestriction == ActivityCondition.ALL) {
                    if (!node.getRealState().keySet().containsAll(activities) || !node.hasAllActivities(activities)) {
                        continue;
                    }
                } else if (conditionPrivilRestriction == ActivityCondition.ATLEAST && !node.hasAtLeastOneActivity(activities)) {
                    continue;
                }
                if (!childTreeNode.isLeaf()) {
                    leafReturnNode = restrictTreeOnLeafsAccordingActivities(
                            childTreeNode,
                            conditionPrivilRestriction,
                            activities);
                    if (leafReturnNode.getChildren().isEmpty()) {
                        continue;
                    }
                } else {
                    leafReturnNode = (DefaultTreeNode<IFlatNode>) childTreeNode;
                }
                leafReturnNode.setParent(restrictedTree);
            }
        }

        return restrictedTree;
    }

    /**
     *
     * @param tree
     * @return
     */
    @Override
    public Set<Long> getAllTreeNodesIds(ITreeNode tree) {
        return getAllNodesIds(tree, new HashSet());
    }

    /**
     *
     * @param tree
     * @param setIds
     * @return
     */
    protected Set<Long> getAllNodesIds(ITreeNode<? extends IDataNode> tree, Set<Long> setIds) {
        tree.getChildren().stream().forEach((next) -> {
            INode node = (INode) next.getData();
            setIds.add(node.getId());
            setIds.addAll(node.getColliderNodes());
            if (!next.isLeaf()) {
                getAllNodesIds(next, setIds);
            }
        });
        return setIds;
    }

    /**
     *
     * @param compositeGroup
     * @param whichTree
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class,
            noRollbackFor = Exception.class)
    public Set<String> getOnlyNamesOfNodesWhichHaveAtLeastOneActivity(ICompositeGroup compositeGroup, WhichTree whichTree) {
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles = getOrLoadActivities(compositeGroup, whichTree);
        return getNodeNamesWhichHasActivitiesForActivity(roles, whichTree).orElse(new HashSet<>());
    }

    /**
     *
     */
    @Override
    public void releaseContext() {
        Optional.ofNullable(user)
                .ifPresent(utilisateur -> {
                    MDC.put("connect.type", "deconnection");
                    MDC.put("connect.date", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.YYYY_MM_DD_HHMMSS));
                    MDC.put("connect.user.name", utilisateur.getNom());
                    MDC.put("connect.user.login", utilisateur.getLogin());
                    MDC.put("connect.user.surname", utilisateur.getPrenom());
                    MDC.put("connect.user.email", utilisateur.getEmail());
                    LOGGER_CONNECTION.info("{} s'est déconnecté", utilisateur.getLogin());
                });
        maactivityUsers.clear();
        this.clearTreeFromSession();
        user = null;
    }

    /**
     *
     * @param compositeGroup
     * @param activityCode
     * @param whichTree
     * @return
     */
    @Override
    public boolean hasActivity(ICompositeGroup compositeGroup, String activityCode, WhichTree whichTree) {
        final Activities activity;
        try {
            activity = Activities.convertToActivity(activityCode);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
        return getOrLoadActivities(compositeGroup, whichTree).keySet().contains(activity);
    }

    /**
     *
     * @param code
     * @param whichTree
     * @return
     */
    @Override
    public Optional<INode> getNodeByNodeableCode(String code, WhichTree whichTree) {
        return treeBuilder.getMgaServiceBuilder()
                .getRecorder().getNodeByNodeableCode(code, whichTree);
    }

    /**
     *
     * @param code
     * @param whichTree
     * @return
     */
    @Override
    public Stream<INode> getNodesByNodeableCode(String code, WhichTree whichTree) {
        return treeBuilder.getMgaServiceBuilder()
                .getRecorder().getNodesByNodeableCodeName(whichTree, code);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isRoot() {
        return user != null && user.getIsRoot();
    }

    /**
     *
     * @param compositeActivity
     * @param whichTree
     */
    @Override
    public void removeActivity(ICompositeActivity compositeActivity, WhichTree whichTree) {
        this.maactivityUsers
                .getOrDefault(whichTree, new HashMap())
                .remove(compositeActivity);
    }

    /**
     *
     * @param group
     * @param whichTree
     * @param role
     */
    @Override
    public void updateActivities(Group group, WhichTree whichTree, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role) {
        this.maactivityUsers
                .computeIfAbsent(whichTree, k -> new HashMap<>())
                .put(group, role);
    }

    /**
     *
     * @return
     */
    @Override
    public String getCurrentUserLogin() {
        return user == null ? null : user.getLogin();
    }

    /**
     *
     * @param r
     */
    @Override
    @Transactional(rollbackFor = Exception.class,
            noRollbackFor = Exception.class)
    public void mergeActivity(ICompositeActivity r) {
        r.synchronizeActivitiesMap();
        getMgaServiceBuilder().getRecorder().mergeActivity(r);
    }

    /**
     *
     * @param r
     */
    @Override
    @Transactional(rollbackFor = Exception.class,
            noRollbackFor = Exception.class)
    public void persistActivity(ICompositeActivity r) {
        r.synchronizeActivitiesMap();
        IMgaRecorder recorder = getMgaServiceBuilder().getRecorder();
        recorder.persistActivity(r);
    }

    /**
     *
     * @return
     */
    @Override
    public IMgaServiceBuilder getMgaServiceBuilder() {
        return treeBuilder.getMgaServiceBuilder();
    }

    /**
     *
     * @param codeConfiguration
     * @param priv
     * @param resource
     * @return
     */
    @Override
    public boolean checkCurrentUserActivity(Integer codeConfiguration, String priv, String resource) {
        return checkActivity(getCurrentUser(), codeConfiguration, priv, resource);
    }

    /*
   /*********************** Private Methods ***********************
     */
    /**
     *
     * @param priv
     * @param role
     * @return
     */
    protected boolean existActivity(String priv, Map<Activities, Map<Long, List<LocalDate>>> role) {

        if (priv.contains(",")) {
            return existAtLeastOneActivity(priv.split(Pattern.quote(PatternConfigurator.PATH_SEPARATOR)), role);
        }

        return role.containsKey(Activities.convertToActivity(priv));
    }

    /**
     *
     * @param privs
     * @param role
     * @return
     */
    protected boolean existAtLeastOneActivity(String[] privs, Map<Activities, Map<Long, List<LocalDate>>> role) {
        for (String priv : privs) {
            if (role.containsKey(Activities.convertToActivity(priv))) {
                return true;
            }
        }
        return false;
    }

    /**
     * *
     * E->(piegeage_en_montee:datatype,mon_theme:theme,nom_site:site
     * ||piegeage_en_montee:datatype )
     * E->(nom_theme:theme,nom_site:site|nom_site:site )
     *
     * @param compositeGroup
     * @param expression
     * @param activities
     * @param treeActivity
     * @param whichTree
     * @return
     */
    protected boolean decodeExpression(ICompositeGroup compositeGroup, String expression, String activities,
            ITreeNode treeActivity, WhichTree whichTree) {

        if (expression.trim().equals(PatternConfigurator.ASTERIX)) {
            if (activities.trim().equals(PatternConfigurator.ASTERIX)) {
                return true;
            }
            String[] activitiesArray = activities.trim().toLowerCase().split(Pattern.quote(PatternConfigurator.PATH_SEPARATOR));
            Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role = getOrLoadActivities(compositeGroup, whichTree);
            return hasActivityForActivities(activitiesArray, role);
        } else {
            String[] subExpressions = expression.trim().replaceFirst(".*->\\(", PatternConfigurator.EMPTY_STRING).
                    replaceFirst("\\)$", PatternConfigurator.EMPTY_STRING).split(Pattern.quote("||"));
            return decodeComplexeExpression(subExpressions, treeActivity, activities);
        }
    }

    /**
     * return true if one of the subExpression matches activities in
     * treeactivity or on a sub-chil
     *
     * @param subExpressions arrays of expression as
     * E->(nom_theme:theme,nom_site:site|nom_site:site )
     * @param treeActivity a TreeNode
     * @param activities as depot,extraction
     * @return
     */
    protected boolean decodeComplexeExpression(String[] subExpressions, ITreeNode treeActivity, String activities) {
        return Arrays.stream(subExpressions).anyMatch(
                (simpleExpression) -> {
                    List<String> expressions = Arrays.asList(simpleExpression.split(Pattern.quote(PatternConfigurator.PATH_SEPARATOR)));
                    return decodeSimpleExpression(expressions, treeActivity, activities, false);
                });
    }

    /**
     *
     * @param activities
     * @param role
     * @return
     */
    protected boolean hasActivityForActivities(String[] activities, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role) {
        return Arrays.stream(activities).anyMatch((priv) -> {
            return isActivityOrActivitiesList(priv) && role.containsKey(Activities.convertToActivity(priv));
        });
    }

    /**
     *
     * @param expressions
     * @param treeActivity
     * @param activities (depot,extraction)
     * @param inDeep
     * @return true if treeActivity has or contains a sub-child whose
     */
    protected boolean decodeSimpleExpression(List<String> expressions, ITreeNode<? extends IDataNode> treeActivity, String activities, boolean inDeep) {
        if (expressions.isEmpty()) {
            return false;
        }
        if (inDeep && expressions.isEmpty()) {
            return true;
        }
        LinkedList<String> expressionsList = new LinkedList(expressions);
        String exp = expressionsList.pop();
        final String[] split = exp.split(Pattern.quote(PatternConfigurator.COLON));
        if (PatternConfigurator.ASTERIX.equals(exp)) {
            return decodeSimpleExpression(expressionsList, treeActivity, activities, true);
        } else if (inDeep) {
            if (expressionsList.isEmpty()) {
                List<ITreeNode> matchingNode = getTreeNodeByNameAndType(treeActivity,
                        split[0],
                        split[1]);
                return matchingNode != null && matchingNode.stream()
                        .anyMatch(m -> m != null && testTreeNode(split[0], split[1], m, activities));
            } else if (PatternConfigurator.ASTERIX.equals(expressionsList.peekFirst())) {
                //getTreeNodeByNameAndType(treeActivity, split[0], split[1]);
                //decodeSimpleExpression(expressionsList, getTreeNodeByNameAndType(treeActivity, split[0], split[1]), activities, true);
                expressionsList.pop();
                return decodeSimpleExpression(expressionsList, treeActivity, activities, true);
            }
            List<ITreeNode> findTreeNode = getTreeNodeByNameAndType(treeActivity,
                    split[0],
                    split[1]);
            return findTreeNode != null && findTreeNode.stream()
                    .anyMatch(f -> f != null && decodeSimpleExpression(expressionsList, f, activities, true));
        } else if (testTreeNode(split[0], split[1], treeActivity, activities)) {
            if (expressionsList.isEmpty()) {
                return true;
            }
            return treeActivity.getChildren().stream().anyMatch(child -> decodeSimpleExpression(expressionsList, child, activities, false));
        }
        return false;
    }

    /**
     *
     * @param name
     * @param type
     * @param findTreeNode
     * @param activities
     * @return
     */
    protected boolean testTreeNode(String name, String type, ITreeNode findTreeNode, String activities) {
        if (findTreeNode == null
                || findTreeNode.getData() == null
                || !((IFlatNode) findTreeNode.getData()).getCode().equalsIgnoreCase(name)
                || !((IFlatNode) findTreeNode.getData()).getTypeResource().getSimpleName()
                        .equalsIgnoreCase(type)) {
            return false;
        } else if (!isActivityOrActivitiesList(activities)) {
            return !((IFlatNode) findTreeNode.getData()).getActivitiesForAllGroups().isEmpty();
        }
        return hasRecursiveActivities(findTreeNode, activities);

    }

    public boolean hasRecursiveActivities(ITreeNode findTreeNode, String activities) {
        IFlatNode currentNode = (IFlatNode) findTreeNode.getData();
        boolean hasAtLeastOneActivity = hasAtLeastOneActivity(currentNode, activities);
        return hasAtLeastOneActivity || findTreeNode.getChildren().stream()
                .anyMatch(
                        ft -> hasRecursiveActivities((ITreeNode) ft, activities)
                );
    }

    /**
     *
     * @param node
     * @param privs
     * @return
     */
    protected boolean hasAtLeastOneActivity(IFlatNode node, String privs) {

        if (isActivityOrActivitiesList(privs)) {
            if (privs.contains(",")) {
                return Arrays.stream(privs.split(Pattern.quote(PatternConfigurator.PATH_SEPARATOR))).anyMatch(priv -> node.hasActivity(Activities.convertToActivity(priv)));
            } else {
                return node.hasActivity(Activities.convertToActivity(privs));
            }
        }
        return false;
    }

    /**
     * *
     * E->(piegeage_en_montee:datatype,mon_theme:theme,nom_site:site
     * ||piegeage_en_montee:datatype )
     * E->(nom_theme:theme,nom_site:site|nom_site:site)
     *
     * @param expression
     * @return
     */
    protected boolean isExpression(String expression) {
        if (Strings.isNullOrEmpty(expression)) {
            return false;
        }
        if (expression.trim().equals(PatternConfigurator.ASTERIX)) {
            return true;
        }
        String IS_GOOD_PATTERN = "E->\\((.+)\\)";
        Matcher matcher = Pattern.compile(IS_GOOD_PATTERN).matcher(expression);
        if (!matcher.matches()) {
            return false;
        } else {
            expression = matcher.group(1);
            return Stream.of(expression.split(","))
                    .allMatch(this::isGroupExpression);
        }
    }

    protected boolean isGroupExpression(String expression) {
        String FIND_PATTERNS = "(\\*|[\\w-]+:[\\w-]+)";
        Matcher matcher = Pattern.compile(FIND_PATTERNS).matcher(expression);
        return matcher.matches();
    }

    /**
     *      * From a root TreeNode, find recursively a child whose data node has
     * code as <b>name</b> and is a nodeable <b>type</b>.</p>
     *
     * @param <T>
     * @param root the TreeNode node from which find the research
     * @param name the name of the entity
     * @param type the type of the entity
     * @return root
     */
    protected <T extends IDataNode> List<ITreeNode> getTreeNodeByNameAndType(ITreeNode<T> root, String name, String type) {

        List<ITreeNode> r = new LinkedList<>();

        if (root.getData() != null && ((IFlatNode) root.getData()).getCode().equalsIgnoreCase(name)
                && ((IFlatNode) root.getData()).getTypeResource()
                        .getSimpleName()
                        .equalsIgnoreCase(type)) {
            r.add(root);
        }
        for (ITreeNode childrenTree : root.getChildren()) {
            if (((IFlatNode) childrenTree.getData()).getCode().equalsIgnoreCase(name)
                    && ((IFlatNode) childrenTree.getData()).getTypeResource()
                            .getSimpleName()
                            .equalsIgnoreCase(type)) {
                r.add(childrenTree);
            } else if (!childrenTree.isLeaf()) {
                r.addAll(getTreeNodeByNameAndType(childrenTree, name, type));
            }
        }

        return r;
    }

    /**
     *
     * @param codeConfiguration
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class,
            noRollbackFor = Exception.class)
    public List retrieveAllUsers(Integer codeConfiguration) {
        return treeBuilder.getMgaServiceBuilder().getRecorder().getCompositeGroupDAO(codeConfiguration).retrieveAllUsers();
    }

    /**
     *
     * @param codeConfiguration
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class,
            noRollbackFor = Exception.class)
    public List retrieveAllUserOrGroups(Integer codeConfiguration) {
        final Optional<IMgaIOConfiguration> configuration = getMgaServiceBuilder().getMgaIOConfigurator().getConfiguration(codeConfiguration);
        WhichTree whichTree = configuration
                .map(codeConf -> codeConf.getWhichTree())
                .orElse(WhichTree.TREEDATASET);
        return treeBuilder.getMgaServiceBuilder().getRecorder().getCompositeGroupDAO(codeConfiguration).retrieveAllUsersOrGroups(whichTree);
    }

    /**
     *
     * @param login
     * @param extractActivity
     */
    @Override
    @Transactional(rollbackFor = Exception.class,
            noRollbackFor = Exception.class)
    public void persistExtractActivity(String login, Map<Long, List<LocalDate>> extractActivity) {

        if (MapUtils.isEmpty(extractActivity)) {
            return;
        }
        removeExtractActivity(login);
        extractActivity.entrySet().stream()
                .forEach(entrySet -> persistExtractActivityEntry(entrySet, login));
        getMgaServiceBuilder().getRecorder().getEntityManager().flush();
        addMissingParentsNodes(login);

    }

    /**
     *
     * @param entrySet
     * @param login
     */
    protected void persistExtractActivityEntry(Map.Entry<Long, List<LocalDate>> entrySet, String login) {
        Long key = entrySet.getKey();
        List<LocalDate> dates = entrySet.getValue();
        treeBuilder.getMgaServiceBuilder()
                .getRecorder()
                .persist(new ExtractActivity(
                        key,
                        login,
                        dates == null || dates.get(0) == null ? DateUtil.MIN_LOCAL_DATE : dates.get(0),
                        dates == null || dates.get(1) == null ? DateUtil.MAX_LOCAL_DATE : dates.get(1)
                )
                );
    }

    /**
     *
     * @param login
     */
    @Override
    @Transactional(rollbackFor = Exception.class,
            noRollbackFor = Exception.class)
    public void removeExtractActivity(String login) {
        treeBuilder.getMgaServiceBuilder().getRecorder().removeExtractActivity(login);
    }

    /**
     *
     * @return
     */
    @Override
    public IUser getCurrentUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    @Override
    public void setCurrentUser(IUser user) {
        Optional.ofNullable(user)
                .ifPresent(utilisateur -> {
                    MDC.put("connect.type", "connection");
                    MDC.put("connect.date", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.YYYY_MM_DD_HHMMSS));
                    MDC.put("connect.user.name", utilisateur.getNom());
                    MDC.put("connect.user.name", utilisateur.getNom());
                    MDC.put("connect.user.login", utilisateur.getLogin());
                    MDC.put("connect.user.surname", utilisateur.getPrenom());
                    MDC.put("connect.user.email", utilisateur.getEmail());
                    LOGGER_CONNECTION.info("{} s'est connecté", utilisateur.getLogin());
                });

        this.user = user;
    }

    /**
     *
     * @param listChild
     */
    @Override
    @Transactional(rollbackFor = Exception.class,
            noRollbackFor = Exception.class)
    public void persistNodes(Stream<INode> listChild) {

        listChild.forEach((node) -> {
            getMgaServiceBuilder().getRecorder().persist(node);
        });
    }

    /**
     *
     * @param login
     */
    protected void addMissingParentsNodes(String login) {
        treeBuilder.getMgaServiceBuilder().getRecorder().getEntityManager().flush();
        treeBuilder.getMgaServiceBuilder()
                .getRecorder()
                .getMissingParentsNodes(login)
                .forEach((t) -> treeBuilder.getMgaServiceBuilder().getRecorder().persist(t));
    }

    /**
     *
     */
    @Override
    public void clearTreeFromSession() {
        this.treeBuilder.clearTreeFromSession();
    }

    /**
     *      * find names of node in whichTree tree that have their id in map
     *
     * @param roles
     * @param whichTree
     * @return
     */
    protected Optional<Set<String>> getNodeNamesWhichHasActivitiesForActivity(Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles, WhichTree whichTree) {
        if (MapUtils.isEmpty(roles)) {
            return Optional.of(new HashSet());
        }
        ArrayList<Long> ids = roles.keySet().stream()
                .map(p -> roles.get(p).keySet())
                .collect(ArrayList<Long>::new, ArrayList<Long>::addAll, ArrayList<Long>::addAll);

        return Optional.ofNullable(treeBuilder.getMgaServiceBuilder().getRecorder()
                .getNodesByIds(ids, whichTree)
                .map(node -> node.getName())
                .collect(Collectors.toSet())
        );
    }

    @Override
    public <T extends IDataNode> Optional<ITreeNode<T>> getTreeNodeByFilterPredicate(ITreeNode<T> root, Predicate<ITreeNode<T>> filter) {
        if (filter.test(root)) {
            return Optional.ofNullable(root);
        } else if (root.isLeaf()) {
            return Optional.empty();
        }
        return root.getChildren().stream()
                .map(childrenTree -> getTreeNodeByFilterPredicate(childrenTree, filter))
                .filter(opt -> !Optional.empty().equals(opt))
                .findFirst()
                .orElse(Optional.empty());
    }

    @Override
    public <T extends IDataNode> Optional<ITreeNode< T>> getTreeNodeById(ITreeNode< T> root, Long id) {
        final Optional<ITreeNode<T>> filteredNode = getTreeNodeByFilterPredicate(root, filterTreeById(id));
        return filteredNode;
    }

    /**
     *
     * @param whichTree
     * @return
     */
    protected Optional<Group> getPublicGroup(WhichTree whichTree) {
        return treeBuilder.getMgaServiceBuilder().getRecorder().getPublicGroup();
    }

    protected ICompositeGroup getPublicCompositeGroupe() {
        if (publicCompositeGroup == null) {
            return new PubligGroup();
        }
        return publicCompositeGroup;
    }

    /**
     *
     * @param roles
     * @param group
     */
    protected void addGroupEntryToActivitiesMap(Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles, Group group) {
        Optional.ofNullable(group)
                .map(g -> g.getActivitiesMap())
                .map(a -> a.entrySet())
                .ifPresent(entry -> entry.forEach(activityEntry -> addActivityEntryToActivitiesMap(roles, group, activityEntry)));
    }

    /**
     *
     * @param roles
     * @param compositeGroup
     * @param activityEntry
     */
    protected void addActivityEntryToActivitiesMap(Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles, Group compositeGroup, Map.Entry<Activities, Map<Long, List<LocalDate>>> activityEntry) {
        Activities activity = activityEntry.getKey();
        activityEntry.getValue().entrySet()
                .forEach(nodeEntry -> addNodeEntryToActivitiesMap(roles, compositeGroup, activity, nodeEntry));
    }

    /**
     *
     * @param roles
     * @param compositeGroup
     * @param activity
     * @param nodeEntry
     */
    protected void addNodeEntryToActivitiesMap(Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> roles, Group compositeGroup, Activities activity, Map.Entry<Long, List<LocalDate>> nodeEntry) {
        Long nodeId = nodeEntry.getKey();
        List<LocalDate> value = nodeEntry.getValue();
        roles
                .computeIfAbsent(activity, k -> new HashMap<>())
                .computeIfAbsent(nodeId, k -> new HashMap<>())
                .put(compositeGroup, value);

    }

    /**
     *
     * @param groupName
     * @param whichTree
     * @return
     */
    @Override
    public Optional<Group> getGroupByGroupNameAndWhichTree(String groupName, WhichTree whichTree) {
        return getMgaServiceBuilder().getRecorder().getGroupByGroupNameAndWhichTree(groupName, whichTree);
    }

    /**
     *
     * @param user
     * @return the map with group as key and true as value if the user is linked
     * to the group
     */
    @Override
    public Map<Group, Boolean> getAllPossiblesGroupUser(IUser user) {
        return treeBuilder.getMgaServiceBuilder().getRecorder().getCompositeGroupDAO(0).getAllPossiblesGroupUser(user);
    }

    /**
     *
     * @param <T>
     * @param user
     * @param group
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public <T extends IUser> Optional<T> addGroupToUser(T user, Group group) {
        return getMgaServiceBuilder().getRecorder().getCompositeGroupDAO(0).addGroupToUser(user, group);
    }

    /**
     *
     * @param <T>
     * @param user
     * @param group
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public <T extends IUser> Optional<T> removeGroupOfUser(T user, Group group) {
        return getMgaServiceBuilder().getRecorder().getCompositeGroupDAO(0).removeGroupOfUser(user, group);
    }

    /**
     *
     * @param groupName
     * @param whichTree
     * @param groupType
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public BusinessException addGroup(String groupName, WhichTree whichTree, GroupType groupType) {
        if (RESERVED_WORDS.contains(groupName)) {
            return new BusinessException(PROPERTY_MSG_RESERVED_WORDS);
        }
        boolean invalide = Strings.isNullOrEmpty(groupName) || !groupName.matches("^[0-9a-zA-Z]{3,20}$");
        if (invalide) {
            return new BusinessException(PROPERTY_MSG_INVALID_GROUP_NAME);
        }

        Optional<Group> dbGroup = getMgaServiceBuilder().getRecorder().getGroupByGroupNameAndWhichTree(groupName, whichTree);
        if (dbGroup.isPresent()) {
            return new BusinessException(PROPERTY_MSG_EXISTING_GROUP);
        }
        Group group = getMgaServiceBuilder().getRecorder().createGroup(groupName, whichTree, groupType);
        getMgaServiceBuilder().getRecorder().saveOrUpdate(group);
        return null;
    }

    /**
     *
     * @param groupName
     * @param whichTree
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public BusinessException removeGroup(String groupName, WhichTree whichTree) {
        Optional<ICompositeGroup> dbGroup = getMgaServiceBuilder().getRecorder().getCompositeGroupDAO(0).retrieveAllUsersOrGroups(whichTree)
                .stream()
                .filter(compositeGroup -> compositeGroup.getGroupName().equals(groupName))
                .findFirst();
        if (!dbGroup.isPresent()) {
            return new BusinessException(PROPERTY_MSG_UNKNOWN_GROUP);
        }
        if (dbGroup.filter(group -> group instanceof IUser).isPresent()) {
            return new BusinessException(PROPERTY_MSG_USER_GROUP);
        }
        getMgaServiceBuilder().getRecorder().getCompositeGroupDAO(0).remove(dbGroup.get());
        return null;
    }

    class PubligGroup implements ICompositeGroup, IUser {

        @Override
        public String getGroupName() {
            return "public";
        }

        @Override
        public Set<Group> getAllGroups() {
            return Stream.of(getPublicGroup(WhichTree.TREEDATASET))
                    .map(og -> og.orElse(null))
                    .collect(Collectors.toSet());
        }

        @Override
        public Group getOwnGroup(WhichTree whichTree) {
            return getPublicGroup(WhichTree.TREEDATASET).orElse(null);
        }

        @Override
        public Long getId() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean testIsMutableGroup(Group group) {
            return false; //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Boolean getIsRoot() {
            return false;
        }

        @Override
        public String getLogin() {
            return "login";
        }

        @Override
        public String getEmail() {
            return null;
        }

        @Override
        public String getLanguage() {
            return null;
        }

        @Override
        public String getNom() {
            return getLogin();
        }

        @Override
        public String getPassword() {
            return null;
        }

        @Override
        public String getPrenom() {
            return getLogin();
        }

    }
}
