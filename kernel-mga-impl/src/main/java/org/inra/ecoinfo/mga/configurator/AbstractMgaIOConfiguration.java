/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.configurator;

import java.util.*;
import java.util.stream.Collectors;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;

/**
 *
 * @author tcherniatinsky
 */
public abstract class AbstractMgaIOConfiguration implements IMgaIOConfiguration {
    
    /**
     *
     */
    protected static final Map<Integer, Map<Integer, String>> columnsTreeNames = new HashMap();

    /**
     *
     */
    protected final Integer codeConfiguration;

    /**
     *
     */
    protected final Class<INodeable> leafType;

    /**
     *
     */
    protected final Integer[] entryOrder;

    /**
     *
     */
    protected final Class<INodeable>[] entryTypes;

    /**
     *
     */
    protected final Activities[] activities;

    /**
     *
     */
    protected final Integer[] sortOrder;

    /**
     *
     */
    protected final boolean includeAncestor;

    /**
     *
     */
    protected final WhichTree whichTree;

    /**
     *
     */
    protected final Class<INodeable> stickyLeafType;

    /**
     *
     */
    protected String prefixInRole = "";

    /**
     *
     */
    protected final boolean displayColumnNames;

    /**
     *
     */
    protected String skeletonBuilder = "treeBuilder";

    /**
     *
     */
    protected Boolean loadOnStartUp = true;

    /**
     *
     * @param codeConfig
     * @param leafType
     * @param entryOrder
     * @param entryTypes
     * @param activities
     * @param sortOrder
     * @param includeAncestor
     * @param whichTree
     * @param stickyLeafType
     * @param displayColumnNames
     */
    public AbstractMgaIOConfiguration(Integer codeConfiguration, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
        this.codeConfiguration = codeConfiguration;
        this.leafType = (Class<INodeable>) leafType;
        this.entryOrder = entryOrder;
        this.entryTypes = entryTypes;
        this.activities = activities;
        this.sortOrder = sortOrder;
        this.includeAncestor = includeAncestor;
        this.whichTree = whichTree;
        this.stickyLeafType = (Class<INodeable>) stickyLeafType;
        this.displayColumnNames = displayColumnNames;
    }

    /**
     *
     * @return
     */
    @Override
    public Integer[] getEntryOrder() {
        return entryOrder;
    }

    /**
     *
     * @return
     */
    @Override
    public Integer[] getSortOrder() {
        return sortOrder;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T>[] getEntryType() {
        return (Class<T>[]) entryTypes;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getLeafType() {
        return (Class<T>) leafType;
    }

    /**
     *
     * @return
     */
    @Override
    public Integer getCodeConfiguration() {
        return codeConfiguration;
    }

    /**
     *
     * @return
     */
    @Override
    public Activities[] getActivities() {
        return activities;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isIncludeAncestor() {
        return includeAncestor;
    }

    /**
     *
     * @return
     */
    @Override
    public WhichTree getWhichTree() {
        return whichTree;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T>  getStickyLeafType(){
        return (Class<T>) stickyLeafType;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean displayColumnNames() {
        return displayColumnNames;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.codeConfiguration;
        hash = 97 * hash + Objects.hashCode(this.leafType);
        hash = 97 * hash + Arrays.deepHashCode(this.entryOrder);
        hash = 97 * hash + Arrays.deepHashCode(this.entryTypes);
        hash = 97 * hash + Arrays.deepHashCode(this.activities);
        hash = 97 * hash + Arrays.deepHashCode(this.sortOrder);
        hash = 97 * hash + (this.includeAncestor ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.whichTree);
        hash = 97 * hash + Objects.hashCode(this.whichTree);
        hash = 97 * hash + Objects.hashCode(this.stickyLeafType);
        hash = 97 * hash + (this.displayColumnNames ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractMgaIOConfiguration other = (AbstractMgaIOConfiguration) obj;
        if (this.codeConfiguration != other.codeConfiguration) {
            return false;
        }
        if (this.includeAncestor != other.includeAncestor) {
            return false;
        }
        if (this.displayColumnNames != other.displayColumnNames) {
            return false;
        }
        if (!Objects.equals(this.leafType, other.leafType)) {
            return false;
        }
        if (!Arrays.deepEquals(this.entryOrder, other.entryOrder)) {
            return false;
        }
        if (!Arrays.deepEquals(this.entryTypes, other.entryTypes)) {
            return false;
        }
        if (!Arrays.deepEquals(this.activities, other.activities)) {
            return false;
        }
        if (!Arrays.deepEquals(this.sortOrder, other.sortOrder)) {
            return false;
        }
        if (this.whichTree != other.whichTree) {
            return false;
        }
        if (this.whichTree != other.whichTree) {
            return false;
        }
        return this.stickyLeafType == other.stickyLeafType;
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> getColumnsTreeNames() {
        final Map<Integer, String> columns = columnsTreeNames.getOrDefault(codeConfiguration, new HashMap());
        return columns.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getKey)).map(Map.Entry::getValue).collect(Collectors.toList());
    }

    /**
     *
     * @param <T>
     * @param index
     */
    @Override
    public  <T extends INodeable> void addColumn(String columnName, int index) {
        this.columnsTreeNames
                .computeIfAbsent(codeConfiguration, (Integer k) -> new HashMap())
                .computeIfAbsent(index, k -> columnName);
    }

    /**
     *
     */
    @Override
    public void clearColumns() {
        this.columnsTreeNames.getOrDefault(codeConfiguration, new HashMap()).clear();
    }

    /**
     *
     * @return
     */
    @Override
    public Boolean loadOnStartUp() {
        return loadOnStartUp;
    }

    /**
     *
     * @return
     */
    @Override
    public String getSkeletonBuilder() {
        return skeletonBuilder;
    }

    /**
     *
     * @param skeletonBuilder
     */
    public void setSkeletonBuilder(String skeletonBuilder) {
        this.skeletonBuilder = skeletonBuilder;
    }

    /**
     *
     * @param loadOnStartUp
     */
    public void setLoadOnStartUp(Boolean loadOnStartUp) {
        this.loadOnStartUp = loadOnStartUp;
    }
    
}
