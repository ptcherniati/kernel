/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.text.Normalizer;
import org.mozilla.universalchardet.UniversalDetector;

/**
 *
 * @author tcherniatinsky
 */
public class Utils {

    /**
     * The Constant PATTERN_NOPRMALIZE_ACCENT.
     */
    private static final String PATTERN_NOPRMALIZE_ACCENT = "[\u0300-\u036F]";
    /**
     * The Constant PATTERN_DOUBLE_SPACE.
     */
    private static final String PATTERN_DOUBLE_SPACE = "\\b\\s{2,}\\b";
    /**
     * The Constant SPACE.
     */
    private static final String SPACE = " ";
    private static final String PATTERN_CHAR_TO_REPLACE = PATTERN_DOUBLE_SPACE + "|" + SPACE + "|'|\"";
    /**
     * The Constant UNDERSCORE.
     */
    private static final String UNDERSCORE = "_";
    /**
     * The Constant ENCODING_ISO_8859_1.
     */
    public static final String ENCODING_ISO_8859_1 = "iso-8859-1";
    /**
     * The Constant ENCODING_ISO_8859_1.
     */
    public static final String ENCODING_ISO_8859_15 = "iso-8859-15";
    /**
     * The Constant ENCODING_UTF8.
     */
    public static final String ENCODING_UTF8 = "utf-8";

    /**
     * Detect stream encoding.
     *
     * @param bis the BufferedInputStream bis
     * @return the String string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String detectStreamEncoding(final BufferedInputStream bis) throws IOException {
        final byte[] buf = new byte[4_096];
        String encoding = null;
        final UniversalDetector detector = new UniversalDetector(null);
        int nread = bis.read(buf);
        while (nread > 0 && !detector.isDone()) {
            detector.handleData(buf, 0, nread);
            nread = bis.read(buf);
        }
        detector.dataEnd();
        encoding = detector.getDetectedCharset();
        detector.reset();
        if (encoding == null) {
            return Utils.ENCODING_UTF8;
        }
        if (!Utils.ENCODING_UTF8.equalsIgnoreCase(encoding)) {
            encoding = Utils.ENCODING_ISO_8859_15;
        }
        return encoding;
    }

    /**
     *
     * @param result
     * @return
     */
    public static String normalizer(String result) {
        return Normalizer.normalize(result, Normalizer.Form.NFD).replaceAll(Utils.PATTERN_NOPRMALIZE_ACCENT, "");
    }

    /**
     * Creates the code from string.
     *
     * @param string the String string
     * @return the String string
     */
    public static String createCodeFromString(final String string) {
        if (string == null) {
            return null;
        }
        String result = string.trim().toLowerCase();
        result = result.replaceAll(PATTERN_CHAR_TO_REPLACE, Utils.UNDERSCORE);
        return Normalizer.normalize(result, Normalizer.Form.NFD).replaceAll(Utils.PATTERN_NOPRMALIZE_ACCENT, "");
    }

}
