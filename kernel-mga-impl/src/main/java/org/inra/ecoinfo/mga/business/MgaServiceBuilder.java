package org.inra.ecoinfo.mga.business;

import java.util.stream.Stream;
import javax.persistence.criteria.*;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;

/**
 *
 * @author yahiaoui
 */
public class MgaServiceBuilder implements IMgaServiceBuilder {

    /**
     *
     * @param <N>
     * @param <T>
     * @param builder
     * @param treeClass
     * @param nodeableClass
     * @return
     */
    public static final <N extends Nodeable, T extends AbstractBranchNode> CriteriaQuery<INode> buildQuery(
            CriteriaBuilder builder,
            Class<T> treeClass,
            Class<N> nodeableClass) {
        CriteriaQuery<INode> query = builder.createQuery(INode.class);
        Root<T> node = query.from(treeClass);
        Root<N> castedNodeable = query.from(nodeableClass);
        Join<T, RealNode> realNode = node.join(AbstractBranchNode_.realNode);
        final Join<RealNode, Nodeable> nodeable1 = realNode.join(RealNode_.nodeable);
        query
                .where(builder.equal(castedNodeable, nodeable1))
                .select(node)
                .orderBy(builder.asc(nodeable1.get(Nodeable_.order)));
        return query;
    }

    private IMgaRecorder recorder;

    private IMgaBuilder mgaBuilder;

//    @Autowired
//    @Qualifier("mgaIOConfigurator")
    private IMgaIOConfigurator mgaIOConfigurator;

    /**
     *
     */
    public MgaServiceBuilder() {
    }

    /**
     *
     * @param mgaIOConfiguration
     * @param isCurrentUserRoot
     * @param isForceExtendsQuery
     * @return
     */
    @Override
    public Stream<INode> loadNodes(IMgaIOConfiguration mgaIOConfiguration, boolean isCurrentUserRoot, boolean isForceExtendsQuery) {

        final Class<Nodeable> leafType = mgaIOConfiguration.getLeafType();
        Class<Nodeable> stickyLeafType = mgaIOConfiguration.getStickyLeafType();
        final WhichTree whichTree = mgaIOConfiguration.getWhichTree();
        Class<AbstractBranchNode> treeClass = whichTree.getEntityTreeClass();
        if (isForceExtendsQuery && stickyLeafType != null
                && (isCurrentUserRoot || recorder.existNodeAccordingTypeResource(whichTree, stickyLeafType))) {
            return recorder.getNodes(builder -> buildQuery(builder, treeClass, stickyLeafType));
        }
        return recorder.getNodes(builder -> buildQuery(builder, treeClass, leafType));
    }

    /**
     *
     * @param builder
     * @return
     */
    protected CriteriaQuery<INode> get(CriteriaBuilder builder) {
        CriteriaQuery<INode> query = builder.createQuery(INode.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Root<Nodeable> nodeable = query.from(Nodeable.class);
        query.where(builder.equal(node.join(AbstractBranchNode_.realNode).join(RealNode_.nodeable), nodeable));
        query.select(node);
        return query;
    }

    /**
     *
     * @param <T>
     * @param wt
     * @param typeResource
     * @return
     */
    @Override
    public <T extends INodeable> Stream<INode> loadNodesByTypeResource(WhichTree wt, Class<T> typeResource) {
        return recorder.getNodesByTypeResource(wt, typeResource);
    }

    /**
     *
     * @return
     */
    @Override
    public IMgaRecorder getRecorder() {
        return recorder;
    }

    /**
     *
     * @param recorder
     */
    @Override
    public void setRecorder(IMgaRecorder recorder) {
        this.recorder = recorder;
    }

    /**
     *
     * @return
     */
    @Override
    public IMgaBuilder getMgaBuilder() {
        mgaBuilder.setRecorder(recorder);
        return mgaBuilder;
    }

    /**
     *
     * @param mgaBuilder
     */
    @Override
    public void setMgaBuilder(IMgaBuilder mgaBuilder) {
        this.mgaBuilder = mgaBuilder;
    }

    /**
     *
     * @return
     */
    @Override
    public IMgaIOConfigurator getMgaIOConfigurator() {
        return mgaIOConfigurator;
    }

    /**
     *
     * @param mgaIOConfigurator
     */
    @Override
    public void setMgaIOConfigurator(IMgaIOConfigurator mgaIOConfigurator) {
        this.mgaIOConfigurator = mgaIOConfigurator;
    }
}
