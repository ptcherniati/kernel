package org.inra.ecoinfo.mga.business;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.assertj.core.util.Strings;
import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author yahiaoui
 */
public class MgaBuilder implements IMgaBuilder {

    /**
     *
     * @param n
     * @param separator
     * @return
     */
    public static String getUniqueCode(INodeable n, String separator) {
        return getUniqueCode(n.getNodeableType(), separator, n.getCode());
    }

    /**
     *
     * @param <T>
     * @param typeResource
     * @param separator
     * @param name
     * @return
     */
    public static <T extends INodeable> String getUniqueCode(Class<T> typeResource, String separator, String name) {
        return typeResource.getSimpleName().concat(separator).concat(name);
    }
    Logger LOGGER = LoggerFactory.getLogger(MgaBuilder.class);

    /**
     *
     */
    protected IMgaRecorder recorder;

    /**
     *
     */
    public MgaBuilder() {
    }

    /**
     *
     * @param path
     * @param order
     * @param splitter
     * @param skipHeader
     * @return
     */
    @Override
    public Stream<String> buildOrderedPaths(final String path, final Integer[] order, final String splitter, boolean skipHeader) {
        try (
                final FileInputStream fis = new FileInputStream(path);
                final BufferedInputStream bufferedInputStream = new BufferedInputStream(fis);) {
            final String fileEncoding = Utils.detectStreamEncoding(bufferedInputStream);
            return Files.lines(Paths.get(path), Charset.forName(fileEncoding))
                    .skip(skipHeader?1:0)
                    .map(line -> orderLine(line, splitter, order))
                    .distinct()
                    .sorted();
        } catch (IOException ex) {
            LoggerFactory.getLogger(getClass()).error("buildOrderedPaths", ex);
        }

        return null;
    }

    /**
     *
     * @param line
     * @param splitter
     * @param order
     * @return
     */
    protected String orderLine(String line, String splitter, Integer[] order) {
        StringBuilder tmp = new StringBuilder();
        String[] splitedLine = line.split(splitter);

        for (int i = 0; i < order.length; i++) {
            if (order[i] < splitedLine.length) {
                tmp.append(Utils.createCodeFromString(splitedLine[order[i]])).append(PatternConfigurator.PATH_SEPARATOR);
            }
        }

        tmp.deleteCharAt(tmp.length() - 1);
        return tmp.toString();
    }

    /**
     *      * From a pathes build a list of leafNode that have path</p>
     *
     * @param <T>
     * @param pathes
     * @param typeResources
     * @param entities
     * @param whichTree
     * @param stickyNodeables
     * @return the
     * java.util.List<org.inra.ecoinfo.mga.business.composite.AbstractBranchNode>
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public <T extends INodeable> Stream<INode> buildLeavesForPathes(Stream<String> pathes, Class<T>[] typeResources, Map<String, INodeable> entities, WhichTree whichTree, List<? extends INodeable> stickyNodeables) {
        Map<String, RealNode> realNodesMap = recorder.getAllRealNodes()
                .collect(Collectors.toMap(RealNode::getPath, c -> c));
        return pathes
                .map(path -> addLeafForBranchPath(realNodesMap, path, typeResources, entities, whichTree, stickyNodeables))
                .flatMap(t -> t);
    }

    /**
     *
     * @param path
     * @return
     */
    protected String getAncestorPath(String path) {
        Pattern p = Pattern.compile("(^.*?)([^,]*)(/[^/,]*$)");
        Matcher m = p.matcher(path);
        if (m.matches()) {
            return m.group(2);
        }
        return "";
    }

    /**
     *
     * @param path
     * @return
     */
    protected String getParentPath(String path) {
        Pattern p = Pattern.compile("(^.*),[^,]*$");
        Matcher m = p.matcher(path);
        if (m.matches()) {
            return m.group(1);
        }
        return "";
    }

    /**
     *
     * @param <T>
     * @param realNodesMap
     * @param path
     * @param entities
     * @param typeResources
     * @return
     */
    protected <T extends INodeable> RealNode getOrCreateRealNode(Map<String, RealNode> realNodesMap, String path, Map<String, INodeable> entities, Class<T>[] typeResources) {
        if (Strings.isNullOrEmpty(path)) {
            return null;
        }
        RealNode realNode = realNodesMap.get(path);
        if (realNode == null) {
            INodeable nodeable = getEntityFromPath(path, typeResources, entities).orElse(null);
            final RealNode parent = getOrCreateRealNode(realNodesMap, getParentPath(path), entities, typeResources);
            final RealNode ancestor = getOrCreateAnscestorRealNode(realNodesMap, nodeable.getNodeableType(), getAncestorPath(path), entities);
            realNode = new RealNode(parent, ancestor, nodeable, path);
            recorder.saveOrUpdate(realNode);
            realNodesMap.put(path, realNode);
        }
        return realNode;
    }

    /**
     *
     * @param <T>
     * @param realNodesMap
     * @param typeResource
     * @param path
     * @param entities
     * @return
     */
    protected <T extends INodeable> RealNode getOrCreateAnscestorRealNode(Map<String, RealNode> realNodesMap, Class<T> typeResource, String path, Map<String, INodeable> entities) {
        if (Strings.isNullOrEmpty(path)) {
            return null;
        }
        RealNode realNode = realNodesMap.get(path);
        if (realNode == null) {
            INodeable nodeable = getEntityFromPath(path, typeResource, entities).orElse(null);
            final RealNode ancestor = getOrCreateAnscestorRealNode(realNodesMap, typeResource, getAncestorPath(path), entities);
            realNode = new RealNode(null, ancestor, nodeable, path);
            recorder.saveOrUpdate(realNode);
            realNodesMap.put(realNode.getPath(), realNode);
        }
        return realNode;
    }

    /**
     *      * create a branch node(and its parents and anscestor if needed) which have
     * that path</p>
     *
     * @param <T>
     * @param realNodesMap
     * @param path
     * @param entities
     * @param typeResources
     * @param whichTree
     * @return
     */
    protected <T extends INodeable> AbstractBranchNode createBranchNode(Map<String, RealNode> realNodesMap, String path, Map<String, INodeable> entities,
            Class<T>[] typeResources, WhichTree whichTree) {
        RealNode realNode = getOrCreateRealNode(realNodesMap, path, entities, typeResources);
        String parentPath = getParentPath(path);
        AbstractBranchNode parentNode = Optional.ofNullable(parentPath)
                .filter(p -> !p.isEmpty())
                .map(p -> createBranchNode(realNodesMap, p, entities, typeResources, whichTree)).orElse(null);
        INodeable nodeable = getEntityFromPath(path, typeResources, entities).orElse(null);
        String ancestorPath = getAncestorPath(path);
        AbstractBranchNode ancestorNode = Optional.ofNullable(ancestorPath)
                .filter(p -> !p.isEmpty())
                .map(p -> createAnscestorBranchNode(realNodesMap, nodeable.getNodeableType(), p, entities, whichTree)).orElse(null);
        return FactoryNode.createTransientNode(whichTree, realNode, parentNode, ancestorNode);
    }

    /**
     *
     * @param <T>
     * @param realNodesMap
     * @param typeResource
     * @param path
     * @param entities
     * @param whichTree
     * @return
     */
    protected <T extends INodeable> AbstractBranchNode createAnscestorBranchNode(Map<String, RealNode> realNodesMap, Class<T> typeResource, String path, Map<String, INodeable> entities, WhichTree whichTree) {
        RealNode realNode = getOrCreateAnscestorRealNode(realNodesMap, typeResource, path, entities);
        String ancestorPath = getAncestorPath(path);
        AbstractBranchNode ancestorNode = Optional.ofNullable(ancestorPath)
                .filter(p -> !p.isEmpty())
                .map(p -> createAnscestorBranchNode(realNodesMap, typeResource, p, entities, whichTree))
                .orElse(null);
        return FactoryNode.createTransientNode(whichTree, realNode, null, ancestorNode);
    }

    /**
     *      * From a path add a leafNode that have path and add it to returnLeaves</p>
     *
     * @param <T>
     * @param realNodesMap
     * @param path
     * @param typeResources
     * @param entities
     * @param whichTree
     * @param stickyNodeables
     * @return
     */
    protected <T extends INodeable> Stream<INode> addLeafForBranchPath(
            Map<String, RealNode> realNodesMap, final String path, Class<T>[] typeResources,
            Map<String, INodeable> entities, WhichTree whichTree, List<? extends INodeable> stickyNodeables) {
        AbstractBranchNode leafNode = createBranchNode(realNodesMap, path, entities, typeResources, whichTree);
        if (stickyNodeables == null) {
            return Stream.of(leafNode);
        }
        if (stickyNodeables.isEmpty()) {
            return Stream.of(leafNode);
        }
        return stickyNodeables.stream()
                .map(nodeable -> addStickyLeafForPathAndLeafNode(nodeable, leafNode, path, whichTree));
    }

    /**
     *
     * @param nodeable
     * @param leafNode
     * @param path
     * @param whichTree
     * @return
     */
    protected INode addStickyLeafForPathAndLeafNode(INodeable nodeable, AbstractBranchNode leafNode, String path, WhichTree whichTree) {
        RealNode realNode = new RealNode(leafNode.getRealNode(), null, nodeable, path);
        recorder.saveOrUpdate(realNode);
        AbstractBranchNode stickyNode = FactoryNode.createTransientNode(whichTree, realNode, leafNode, null);
        return stickyNode;
    }

    /**
     *
     * @param <T>
     * @param path
     * @param typeResources
     * @param entities
     * @return the nodable corresponding to the last pattern of path
     */
    protected <T extends INodeable> Optional<INodeable> getEntityFromPath(String path, Class<T>[] typeResources, Map<String, INodeable> entities) {
        final String[] pathes = path.split(PatternConfigurator.PATH_SEPARATOR);
        String typeResource = typeResources[pathes.length - 1].getSimpleName();
        String entityPath = String.format("%s/%s", typeResource, pathes[pathes.length - 1]);
        if(!entities.containsKey(entityPath)){
            String message = String.format("can't find entity for entityPath %s", entityPath);
            LOGGER.error(message);
        }
        return Optional.ofNullable(entities.get(entityPath));
    }

    /**
     *
     * @param <T>
     * @param path
     * @param typeResource
     * @param entities
     * @return
     */
    protected <T extends INodeable> Optional<INodeable> getEntityFromPath(String path, Class<T> typeResource, Map<String, INodeable> entities) {
        return getEntityFromPath(path, new Class[]{typeResource}, entities);
    }

    /**
     *
     * @param recorder
     */
    @Override
    public void setRecorder(IMgaRecorder recorder) {
        this.recorder = recorder;
    }

}
