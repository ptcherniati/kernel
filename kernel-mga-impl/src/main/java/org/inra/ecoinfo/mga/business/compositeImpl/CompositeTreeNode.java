package org.inra.ecoinfo.mga.business.compositeImpl ;

import javax.swing.tree.DefaultMutableTreeNode;
import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.ICompositeTreeNode;
import org.inra.ecoinfo.mga.business.composite.INode;

/**
 *
 * @author yahiaoui
 */
public class CompositeTreeNode extends DefaultMutableTreeNode implements ICompositeTreeNode {
  
    private INode	node ;

    /**
     *
     */
    public CompositeTreeNode () {}

    /**
     *
     * @param node
     */
    public CompositeTreeNode ( AbstractBranchNode node ) {
        this.node = node ;
    }

    /**
     *
     * @return
     */
    @Override
    public INode getNode () {
        return node ;
    }

    /**
     *
     * @param node
     */
    @Override
    public void setNode ( INode node ) {
        this.node = node ;
    }
   
}
