package org.inra.ecoinfo.mga.middleware;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.*;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.MgaBuilder;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity_;
import org.inra.ecoinfo.mga.business.composite.activities.IExtractActivity;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.Group_;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yahiaoui
 */
public class MgaRecorder extends AbstractJPADAO<INodeable> implements IMgaRecorder {

    private static final Logger LOGGER = LoggerFactory.getLogger(MgaRecorder.class);
    private static final String PUBLIC = "public";
    Map<Integer, ICompositeGroupDAO> compositeGroupDao = new HashMap();

    /**
     *
     */
    public MgaRecorder() {
    }

    /**
     *
     * @param userDao
     */
    @Override
    public void setUserDao(ICompositeGroupDAO userDao, Integer codeConfiguration) {
        this.compositeGroupDao.put(codeConfiguration, userDao);
    }

    /**
     *
     * @param node
     */
    @Override
    public void persist(INode node) {
        Optional.ofNullable(node)
                .map(n -> n.getParent())
                .filter(parent -> !entityManager.contains(parent))
                .ifPresent(parent -> persist(parent));
        Optional.ofNullable(node)
                .map(n -> n.getAncestor())
                .filter(ancestor -> !entityManager.contains(ancestor))
                .ifPresent(anscestor -> persist(anscestor));
        final RealNode realNode = entityManager.merge(node.getRealNode());
        node.setRealNode(realNode);
        entityManager.merge(node);
    }

    /**
     *
     * @param extractActivity
     */
    @Override
    public void persist(IExtractActivity extractActivity) {
        try {
            entityManager.persist(extractActivity);
        } catch (Exception x) {
            LOGGER.error(x.getMessage(), x);
        }
    }

    /**
     *
     * @param node
     * @return
     */
    @Override
    public INode merge(INode node) {
        return entityManager.merge(node);
    }

    /**
     *
     * @param node
     */
    @Override
    public void remove(INode node) {
        if (node == null) {
            return;
        }
        node = entityManager.merge(node);
        entityManager.remove(node);
        /*if (node.getParent() != null) {
            remove(node.getParent());
        }
        if (node.getAncestor() != null) {
            remove(node.getAncestor());
        }*/
    }

    /**
     *
     * @param login
     */
    @Override
    public void removeExtractActivity(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaDelete<ExtractActivity> delete = builder.createCriteriaDelete(ExtractActivity.class);
        Root<ExtractActivity> er = delete.from(ExtractActivity.class);
        delete.where(builder.equal(er.get(ExtractActivity_.login), login));
        entityManager.createQuery(delete).executeUpdate();
    }

    /**
     *
     * @return
     */
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     *
     * @param compositeGroup
     * @param whichTree
     * @return
     */
    @Override
    public Stream<Group> getGroups(ICompositeGroup compositeGroup, WhichTree whichTree) {
        Stream<Group> groups = compositeGroup instanceof IUser
                ? getAllRoles((IUser) compositeGroup, whichTree)
                : getSingleRole((Group) compositeGroup, whichTree);
        return groups;
    }

    /**
     *
     * @param groupName
     * @param whichTree
     * @param groupType
     * @return
     */
    @Override
    public Group createGroup(String groupName, WhichTree whichTree, GroupType groupType) {
        ;
        return getGroupByGroupNameAndWhichTree(groupName, whichTree).orElseGet(() -> {
            Group group = new Group(groupName, whichTree, groupType);
            saveOrUpdate(group);
            return group;
        });

    }

    /**
     *
     */
    @Override
    public void mergeActivity(ICompositeActivity activity) {
        this.entityManager.merge(activity);
    }

    /**
     *
     */
    @Override
    public void persistActivity(ICompositeActivity activity) {
        this.entityManager.persist(activity);
        this.flush();
    }

    /**
     *
     * @param query
     * @return
     */
    @Override
    public Stream< INode> getNodes(Function<CriteriaBuilder, CriteriaQuery<INode>> query) {
        return getResultListToStream(query.apply(builder));
    }

    /**
     *
     * @param query
     * @return
     */
    @Override
    public boolean emptyQueryResult(Function<CriteriaBuilder, CriteriaQuery<INode>> query) {
        return (!getResultList(query.apply(builder)).isEmpty());
    }

    /**
     *
     * @param <T>
     * @param whichTree
     * @param type
     * @return
     */
    @Override
    public <T extends INodeable> boolean existNodeAccordingTypeResource(WhichTree whichTree, Class<T> type) {

        if (type == null) {
            return false;
        }
        return getNodesByTypeResource(whichTree, type).findFirst().isPresent();
    }

    /**
     *
     * @return @throws PersistenceException
     */
    @Override
    public Stream<INodeable> getNodeables() {
        CriteriaQuery<INodeable> query = builder.createQuery(INodeable.class);
        Root<Nodeable> nodeable = query.from(Nodeable.class);
        query.select(nodeable);
        return getResultListToStream(query);
    }

    /**
     *
     * @param <T>
     * @param type
     * @return
     * @throws PersistenceException
     */
    @Override
    public <T extends INodeable> Stream<T> getNodeables(Class<T> type) {
        CriteriaQuery<T> query = builder.createQuery(type);
        Root<T> nodeable = query.from(type);
        query.select(nodeable);
        return getResultListToStream(query);
    }

    /**
     *
     * @param <T>
     * @param whichTree
     * @param typeResource
     * @return
     */
    @Override
    public <T extends INodeable> Stream<INode> getNodesByTypeResource(WhichTree whichTree, Class<T> typeResource) {
        CriteriaQuery<INode> query = builder.createQuery(INode.class);
        Root<AbstractBranchNode> node = query.from(whichTree.getEntityTreeClass());
        Root<T> nodeable = query.from(typeResource);
        query.where(builder.equal(node.join(AbstractBranchNode_.realNode).join(RealNode_.nodeable), nodeable));
        query.select(node);
        return getResultListToStream(query);
    }

    /**
     *
     * @param codeName
     * @param whichTree
     * @return
     */
    @Override
    public Optional<INode> getNodeByNodeableCodeName(String codeName, WhichTree whichTree) {
        return getNodesByNodeableCodeName(whichTree, codeName).findFirst();
    }

    /**
     *
     * @param whichTree
     * @param codeName
     * @return
     */
    @Override
    public Stream<INode> getNodesByNodeableCodeName(WhichTree whichTree, String codeName) {
        CriteriaQuery<INode> query = builder.createQuery(INode.class);
        Root<AbstractBranchNode> node = query.from(whichTree.getEntityTreeClass());
        query.select(node);
        return getResultListToStream(query)
                .filter((n) -> (MgaBuilder.getUniqueCode(
                n.getNodeable(),
                PatternConfigurator.ANCESTOR_SEPARATOR).toLowerCase().equals(codeName.toLowerCase())));
    }

    /**
     *
     * @param nodeIds
     * @param whichTree
     * @return
     */
    @Override
    public Stream<INode> getNodesByIds(List<Long> nodeIds, WhichTree whichTree) {

        if (nodeIds == null || nodeIds.isEmpty()) {
            return new ArrayList().stream();
        }
        CriteriaQuery<INode> query = builder.createQuery(INode.class);
        Root<AbstractBranchNode> node = query.from(whichTree.getEntityTreeClass());
        query.where(node.get(AbstractBranchNode_.id).in(nodeIds));
        query.select(node);
        return getResultListToStream(query);
    }

    /**
     *
     * @param code
     * @param whichTree
     * @return
     */
    @Override
    public Optional<INode> getNodeByNodeableCode(String code, WhichTree whichTree) {
        CriteriaQuery<INode> query = builder.createQuery(INode.class);
        Root<AbstractBranchNode> node = query.from(whichTree.getEntityTreeClass());
        Join<RealNode, Nodeable> nodeable = builder.treat(node.join(AbstractBranchNode_.realNode).join(RealNode_.nodeable), Nodeable.class);
        query.where(builder.equal(nodeable.get(Nodeable_.code), code));
        query.select(node);
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public Map<String, RealNode> getDbNodes() {
        return getAllRealNodes()
                .collect(Collectors.toMap(realNode -> realNode.getPath(), realNode -> realNode));
    }

    /**
     *
     * @param <T>
     * @param parent
     * @param whichTree
     */
    @Override
    public <T extends AbstractBranchNode> void removeStickyLeaves(T parent, WhichTree whichTree) {
        T nullNode = null;
        CriteriaUpdate<T> update = builder.createCriteriaUpdate((Class<T>) parent.getClass());
        Root<T> node = update.from(whichTree.getEntityTreeClass());
        update.set(node, nullNode);
        update.where(builder.equal(node.get(AbstractBranchNode_.parent), parent));
        entityManager.createQuery(update).executeUpdate();
    }

    /**
     *
     * @param realNode
     */
    @Override
    public void saveOrUpdate(RealNode realNode) {
        Optional.ofNullable(realNode)
                .map(n -> n.getParent())
                .ifPresent(parent -> realNode.setParent(entityManager.merge(parent)));
        Optional.ofNullable(realNode)
                .map(n -> n.getAncestor())
                .ifPresent(anscestor -> realNode.setAncestor(entityManager.merge(anscestor)));
        realNode.setNodeable(entityManager.merge(realNode.getNodeable()));
        entityManager.persist(realNode);
    }

    /**
     *
     * @param path
     * @return
     */
    @Override
    public Optional<RealNode> getRealNodeByNKey(String path) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> realNode = query.from(RealNode.class);
        query.where(builder.equal(realNode.get(RealNode_.path), path));
        query.select(realNode);
        return getOptional(query);

    }

    /**
     *
     * @param realNodeId
     * @return
     */
    @Override
    public Optional<RealNode> getRealNodeById(Long realNodeId) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> realNode = query.from(RealNode.class);
        query.where(builder.equal(realNode.get(RealNode_.id), realNodeId));
        query.select(realNode);
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public Stream<RealNode> getAllRealNodes() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> realNode = query.from(RealNode.class);
        query.select(realNode);
        return getResultListToStream(query);
    }

    /**
     *
     * @param login
     * @return
     */
    @Override
    public Stream<ExtractActivity> getMissingParentsNodes(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ExtractActivity> query = builder.createQuery(ExtractActivity.class);
        Root<ExtractActivity> er = query.from(ExtractActivity.class);
        Root<NodeDataSet> nds = query.from(NodeDataSet.class);
        List<Predicate> and = new LinkedList();
        and.add(builder.equal(er.get(ExtractActivity_.login), login));
        and.add(builder.equal(nds.get(NodeDataSet_.id), er.get(ExtractActivity_.idNode)));
        query.select(builder.construct(ExtractActivity.class, nds.join(NodeDataSet_.parent).get(NodeDataSet_.id), er.get(ExtractActivity_.login), er.get(ExtractActivity_.dateStart), er.get(ExtractActivity_.dateEnd)));
        Subquery<Long> subquery = query.subquery(Long.class);
        Root<ExtractActivity> erIn = subquery.from(ExtractActivity.class);
        subquery.select(erIn.get(ExtractActivity_.idNode));
        subquery.where(builder.equal(erIn.get(ExtractActivity_.login), login));
        and.add(nds.join(NodeDataSet_.parent).get(NodeDataSet_.id).in(subquery).not());
        query.where(builder.and(and.toArray(new Predicate[and.size()])));
        query.distinct(true);
        return getResultListToStream(query);
    }

    /**
     *
     * @return
     */
    @Override
    public Optional<Group> getPublicGroup() {
        return getGroupByGroupNameAndWhichTree(PUBLIC, WhichTree.TREEDATASET);
    }

    /**
     *
     * @param user
     * @param whichTree
     * @return
     */
    protected Stream<Group> getAllRoles(IUser user, WhichTree whichTree) {
        return user.getAllGroups().stream()
                .filter(g -> g.getWhichTree().equals(whichTree));
    }

    /**
     *
     * @param compositeGroup
     * @param whichTree
     * @return
     */
    protected Stream<Group> getSingleRole(Group compositeGroup, WhichTree whichTree) {
        assert compositeGroup.getWhichTree().equals(whichTree) : String.format("not is %s", compositeGroup.getWhichTree());
        LinkedList<Group> sg = new LinkedList();
        sg.add(compositeGroup);
        return sg.stream();

    }

    /**
     *
     * @param group
     */
    @Override
    public void saveOrUpdate(Group group) {
        try {
            saveOrUpdateGeneric(group);
        } catch (org.inra.ecoinfo.utils.exceptions.PersistenceException ex) {
            LOGGER.debug(String.format("can't perist group %s %s", group.getGroupName(), group.getWhichTree().name()), ex);
        }
    }

    /**
     *
     * @param groupName
     * @param whichTree
     * @return
     */
    @Override
    public Optional<Group> getGroupByGroupNameAndWhichTree(String groupName, WhichTree whichTree) {
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> group = query.from(Group.class);
        query.where(
                builder.equal(group.get(Group_.groupName), groupName),
                builder.equal(group.get(Group_.whichTree), whichTree)
        );
        query.select(group);
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public ICompositeGroupDAO getCompositeGroupDAO(Integer codeConfiguration) {
        return compositeGroupDao.containsKey(codeConfiguration) ? compositeGroupDao.get(codeConfiguration) : compositeGroupDao.get(0);
    }

    @Override
    public Nodeable remove(Nodeable nodeable) {
        nodeable = entityManager.merge(nodeable);
        CriteriaDelete<NodeDataSet> deleteNode = builder.createCriteriaDelete(NodeDataSet.class);
        Root<NodeDataSet> node = deleteNode.from(NodeDataSet.class);
        Subquery<NodeDataSet> subquery = deleteNode.subquery(NodeDataSet.class);
        Root<NodeDataSet> nodeToDelete = subquery.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rn = nodeToDelete.join(AbstractBranchNode_.realNode);
        Join<RealNode, Nodeable> ndb = rn.join(RealNode_.nodeable);
        subquery
                .where(builder.equal(ndb, nodeable))
                .select(nodeToDelete);
        deleteNode.where(node.in(subquery));
        delete(deleteNode);
        CriteriaDelete<RealNode> deleteRealNode = builder.createCriteriaDelete(RealNode.class);
        Root<RealNode> rn2 = deleteRealNode.from(RealNode.class);
        Subquery<RealNode> subquery2 = deleteNode.subquery(RealNode.class);
        Root<RealNode> rnToDelete = subquery2.from(RealNode.class);
        Join<RealNode, Nodeable> ndb2 = rnToDelete.join(RealNode_.nodeable);
        subquery2
                .where(builder.equal(ndb2, nodeable))
                .select(rnToDelete);
        deleteRealNode.where(rn2.in(subquery2));
        delete(deleteRealNode);
        entityManager.remove(nodeable);
        return nodeable;
    }

    @Override
    public RealNode remove(RealNode realNode) {
        realNode = entityManager.merge(realNode);
        getvariableNodes(realNode).stream()
                .forEach(
                        node -> remove(node)
                );
        entityManager.flush();
        getEmbeddedNodes(realNode).stream()
                .forEach(node -> remove(node));
        entityManager.remove(realNode);
        entityManager.flush();
        return realNode;
    }

    private List<RealNode> getvariableNodes(RealNode realNode) {
        if (realNode == null) {
            return new LinkedList();
        }
        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> node = query.from(RealNode.class);
        query.where(builder.equal(node.get(RealNode_.parent), realNode));
        query.select(node);
        return getResultList(query);
    }

    private List<INode> getEmbeddedNodes(RealNode realNode) {
        if (realNode == null) {
            return new LinkedList();
        }
        CriteriaQuery<INode> query = builder.createQuery(INode.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        query.where(builder.equal(node.join(AbstractBranchNode_.realNode), realNode));
        query.select(node);
        return getResultList(query);
    }
}
