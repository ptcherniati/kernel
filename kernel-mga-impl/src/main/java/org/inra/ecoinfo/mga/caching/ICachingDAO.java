/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.caching;

import java.time.Instant;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface ICachingDAO  extends IDAO<CachingUpdate>{

    /**
     *
     * @param configuration
     * @return
     */
    public Optional<Instant> getInstantCaching(Integer configuration);
    
}
