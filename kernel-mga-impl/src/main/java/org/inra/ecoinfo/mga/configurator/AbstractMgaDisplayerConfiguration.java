package org.inra.ecoinfo.mga.configurator;

import java.util.HashMap;
import java.util.Map;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.utils.MgaUtils;

/**
 *
 * @author ryahiaoui
 */
public abstract class AbstractMgaDisplayerConfiguration implements IMgaDisplayerConfiguration {

      private final Map<Class<? extends INodeable>, String[]> displayColumnNames = new HashMap<>();

    private int tabSize = 0;

    /**
     *
     * @param <T>
     * @param instanceType
     * @param columnNames
     */
    public <T extends INodeable> void addColumnNamesForInstanceType(Class<T> instanceType, String... columnNames) {
        displayColumnNames.put(instanceType, columnNames);
        tabSize = buildTabSize();
    }

    private Integer buildTabSize() {
        return displayColumnNames.entrySet().stream().map(entry -> entry.getValue().length).min(Integer::compare).orElse(0);
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Class<? extends INodeable>, String[]> getDisplayColumnNames() {
        return displayColumnNames;
    }

    /**
     *
     * @param index
     */
    @Override
    public void autoUpdateColumn(int index) {

        if (index >= tabSize) {
            displayColumnNames.entrySet().stream()
                    .filter(
                            entry -> index >= entry.getValue().length
                    )
                    .forEach((entry) -> {
                        while (displayColumnNames.get(entry.getKey()).length <= index) {
                            displayColumnNames.put(entry.getKey(), MgaUtils.addValue(entry.getValue(), String.format("%s_%d",entry.getKey().getSimpleName() , displayColumnNames.get(entry.getKey()).length+1)));
                        }

                    });

            tabSize = buildTabSize();
        }
    }

    /**
     *
     * @return
     */
    public int getTabSize() {
        return tabSize;
    }

}
