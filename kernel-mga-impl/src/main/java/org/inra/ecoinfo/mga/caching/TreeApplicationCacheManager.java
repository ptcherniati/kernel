package org.inra.ecoinfo.mga.caching;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Stream;
import org.inra.ecoinfo.mga.business.DefaultTreeNode;
import org.inra.ecoinfo.mga.business.MgaBuilder;
import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ryahiaoui
 */
public class TreeApplicationCacheManager implements ITreeApplicationCacheManager {

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(TreeApplicationCacheManager.class);

    private ICachingDAO cachingDAO;

    protected Map<String, List<Integer>> configurationMap = new HashMap();


    private final Map< Integer, ITreeNode> treeMap;
    private final Map< Integer, Instant> treeTimestamp;

    private IMgaIOConfigurator mgaIOConfigurator;
    private final Lock monitor = new ReentrantLock();

    /**
     *
     */
    public TreeApplicationCacheManager() {
        this.treeMap = new ConcurrentHashMap();
        this.treeTimestamp = new ConcurrentHashMap();
    }

    /**
     *
     */
    @Override
    public void initMap() {
        this.treeMap.clear();
    }

    /**
     *
     * @return
     */
    public ICachingDAO getCachingDAO() {
        return cachingDAO;
    }

    /**
     *
     * @param cachingDAO
     */
    public void setCachingDAO(ICachingDAO cachingDAO) {
        this.cachingDAO = cachingDAO;
    }

    /**
     *
     * @param key
     * @param treeNode
     */
    public void addSkeletonTree(Integer key, ITreeNode treeNode) {
        Boolean isReload = cachingDAO.getInstantCaching(key)
                .filter(
                        instant -> !treeMap.containsKey(key) || instant.equals(treeTimestamp.get(key))
                )
                .map(instant -> true)
                .orElse(false);
        this.treeMap.put(key, treeNode);
        final Instant now = Instant.now();
        this.treeTimestamp.put(key, now);
        if (isReload) {
            try {
                cachingDAO.saveOrUpdate(cachingDAO.merge(new CachingUpdate(key, now)));
            } catch (PersistenceException ex) {
                LOGGER.error("Can't update database for caching update info");
            }
        }
    }

    /**
     *
     * @param key
     * @return
     */
    @Override
    public ITreeNode getSkeletonTree(Integer key) {
        final Optional<Instant> instantCaching = cachingDAO.getInstantCaching(key);
        if (instantCaching.isPresent()) {
            return instantCaching
                    .filter(
                            instant -> instant.equals((treeTimestamp.get(key)))
                    )
                    .map(Instant -> treeMap.getOrDefault(key, null))
                    .orElse(null);
        }
        return treeMap.getOrDefault(key, null);
    }

    /**
     *
     * @param config
     * @param leaves
     * @param caching
     * @return
     */
    @Override
    public ITreeNode<AbstractBranchNode> buildSkeletonTree(IMgaIOConfiguration config,
            Stream<INode> leaves, boolean caching) {

        ITreeNode<AbstractBranchNode> root = new DefaultTreeNode(null, null, null, config);

        try {

            if (monitor.tryLock(5, TimeUnit.SECONDS)) {

                config.clearColumns();
                Optional<ArrayList<INode>> max = leaves.map(
                        (leaf) -> {
                            leaf.setLeaf(leaf);
                            leaf.setIsLeaf(true);
                            ArrayList<INode> branchNodes = new ArrayList();
                            if (config.isIncludeAncestor()) {
                                mgaIOConfigurator.setDeepestIndexNode(
                                        Math.max(mgaIOConfigurator.getDeepestIndexNode(),
                                                leaf.depthIncludeAncestor()));
                            } else {
                                mgaIOConfigurator.setDeepestIndexNode(
                                        Math.max(mgaIOConfigurator.getDeepestIndexNode(),
                                                leaf.depth()));
                            }
                            final Integer[] sortOrder = config.getSortOrder(leaf.depth());
                            for (int index = sortOrder.length - 1; index >= 0; index--) {
                                int indexNode = sortOrder[index];
                                INode parentNodeDepth = leaf.getNodeDepth(indexNode);
                                if (parentNodeDepth == null) {
                                    continue;
                                }
                                parentNodeDepth.setLeaf(leaf);

                                branchNodes.add(0, parentNodeDepth);
                                if (config.isIncludeAncestor()) {
                                    parentNodeDepth.scanAncestors(branchNodes);
                                }
                            }

                            /* Sticky Nodes : add them as Leafs */
                            if (!Arrays.asList(sortOrder).contains(leaf.depth())) {
                                /* Then leaf is a StickyNode */
                                branchNodes.add(leaf);
                            }
                            loadBranchNode(root, branchNodes, config);
                            return branchNodes;
                        })
                        .max((o1, o2) -> {
                            return o1.size() - o2.size();
                        });

                max.ifPresent(branchNodes -> {
                    int i = 0;
                    for (INode n : branchNodes) {
                        mgaIOConfigurator.addColumn(n.getNodeable().getNodeableType(),
                                i++,
                                config.getCodeConfiguration()
                        );
                    }
                });
            } else {
                LOGGER.info("can't lock");
            }
        } catch (InterruptedException ex) {
            LOGGER.debug(ex.getMessage(), ex);
        } finally {
            monitor.unlock();
        }
        if (caching) {
            addSkeletonTree(config.getCodeConfiguration(), root);
        }
        return root;

    }

    private void loadBranchNode(ITreeNode<AbstractBranchNode> root,
            ArrayList<INode> branchNodes, IMgaIOConfiguration config) {

        boolean existNode;
        ITreeNode<AbstractBranchNode> rootTreeNode = root;

        for (int i = 0; i < branchNodes.size(); i++) {
            INode itemBranchNodeI = branchNodes.get(i);

            existNode = false;

            mgaIOConfigurator.getExtractedNodeables().put(
                    MgaBuilder.getUniqueCode(itemBranchNodeI.getNodeable(),
                            PatternConfigurator.ANCESTOR_SEPARATOR),
                    itemBranchNodeI.getNodeable());

            for (ITreeNode<AbstractBranchNode> childrenTree : rootTreeNode.getChildren()) {

                AbstractBranchNode childrenTreeNode = (AbstractBranchNode) childrenTree.getData();

                if (childrenTreeNode.getCode().equals(itemBranchNodeI.getCode())) {

                    childrenTreeNode.addColliderNode(itemBranchNodeI.getId());
                    rootTreeNode = childrenTree;
                    existNode = true;

                    break;
                }
            }

            if (!existNode) {

                rootTreeNode
                        = new DefaultTreeNode(itemBranchNodeI.getName(),
                                itemBranchNodeI, rootTreeNode, config);
            }
        }

    }

    /**
     *
     * @param mgaIOConfigurator
     */
    @Override
    public void setMgaIOConfigurator(IMgaIOConfigurator mgaIOConfigurator) {
        this.mgaIOConfigurator = mgaIOConfigurator;
    }

    /**
     *
     * @return
     */
    @Override
    public IMgaIOConfigurator getMgaIOConfigurator() {
        return this.mgaIOConfigurator;
    }

    /**
     *
     * @param key
     */
    @Override
    public void removeSkeletonTree(Integer key) {
        try {
            if (monitor.tryLock(1, TimeUnit.MINUTES)) {
                configurationMap.values().stream()
                        .filter(
                                list -> list.contains(key)
                        )
                        .flatMap(Collection::stream)
                        .distinct()
                        .forEach(
                                k -> {
                                    this.treeMap.remove(k);
                                    final Instant now = Instant.now();
                                    try {
                                        cachingDAO.saveOrUpdate(cachingDAO.merge(new CachingUpdate(key, now)));
                                    } catch (PersistenceException ex) {
                                        LOGGER.error("Can't update database for caching update info");
                                    }
                                });
            }
        } catch (InterruptedException ex) {
            LOGGER.debug(ex.getMessage(), ex);
        } finally {
            monitor.unlock();
        }
    }

    /**
     *
     */
    @Override
    public void clearSkeletons() {

        try {
            if (monitor.tryLock(1, TimeUnit.MINUTES)) {
                this.treeMap.clear();

            }
        } catch (InterruptedException ex) {
            LOGGER.debug(ex.getMessage(), ex);
        } finally {
            monitor.unlock();
        }
    }

    /**
     *
     * @return
     */
    @Override
    public int size() {
        return this.treeMap.size();
    }

    /**
     *
     * @return
     */
    public Map<String, List<Integer>> getConfigurationMap() {
        return configurationMap;
    }

    /**
     *
     * @param key
     * @return
     */
    @Override
    public Instant getTimeStamp(int key) {
        return treeTimestamp.get(key);
    }

}
