package org.inra.ecoinfo.mga.business;

import java.util.Objects;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;

/**
 *
 * @author yahiaoui
 */
public class FactoryNode implements IFactoryNode {

    /**
     *
     * @param whichTree
     * @param realNode
     * @param parentNode
     * @param ancestorNode
     * @return
     */
    public static AbstractBranchNode createTransientNode(WhichTree whichTree, RealNode realNode, AbstractBranchNode parentNode, AbstractBranchNode ancestorNode) {
        Objects.requireNonNull(realNode, "null realNode");
        AbstractBranchNode brancheNode = null;
        if (null == whichTree) {
            throw new NullPointerException("null branch node");
        } else {
            switch (whichTree) {
                case TREEDATASET:
                    brancheNode = new NodeDataSet((NodeDataSet)parentNode,(NodeDataSet)ancestorNode);
                    break;
                case TREEREFDATA:
                    brancheNode = new NodeRefData((NodeRefData)parentNode,(NodeRefData)ancestorNode);
                    break;
                default:
                    throw new NullPointerException("null branch node");
            }
        }
        brancheNode.setRealNode(realNode);
        return brancheNode;
    }

    /**
     *
     * @param realNode
     * @param parentNode
     * @param recorder
     * @param ancestorNode
     * @param whichTree
     * @return
     */
    public static INode createPersistedNode(RealNode realNode, AbstractBranchNode parentNode, AbstractBranchNode ancestorNode, IMgaRecorder recorder, WhichTree whichTree) {
        INode node = FactoryNode.createTransientNode(whichTree, realNode, parentNode, ancestorNode);
        recorder.persist(node);
        return node;
    }

}
