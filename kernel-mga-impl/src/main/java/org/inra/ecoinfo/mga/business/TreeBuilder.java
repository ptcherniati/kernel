package org.inra.ecoinfo.mga.business;

//~--- non-JDK imports --------------------------------------------------------

import com.google.common.base.Strings;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.mga.business.composite.*;
import static org.inra.ecoinfo.mga.business.composite.FlatNode.*;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.caching.ITreeApplicationCacheManager;
import org.inra.ecoinfo.mga.caching.ITreeSessionCacheManager;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.viewadapter.ISkeletonBuilder;
import org.inra.ecoinfo.mga.viewadapter.ITreeBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yahiaoui
 */
public class TreeBuilder implements ITreeBuilder, ISkeletonBuilder {

    /**
     *
     * @param currentNode
     * @param activity
     * @param entryGroup
     */
    protected static void addGroupActivityForActivity(IFlatNode currentNode, Activities activity, Map.Entry<Group, List<LocalDate>> entryGroup) {
        currentNode.getActivitiesForAllGroups().entrySet().stream()
                .filter(entry -> activity.equals(entry.getKey()))
                .forEach((entry) -> {
                    entry.getValue().put(entryGroup.getKey(), entryGroup.getValue());
                    if (entryGroup.getKey().equals(currentNode.getOwnGroup())) {
                        currentNode.getOwnActivities().put(activity, entryGroup.getValue());
                    }
                });
    }

    Logger LOGGER = LoggerFactory.getLogger(TreeBuilder.class);

    /**
     *
     */
    protected IMgaServiceBuilder mgaServiceBuilder;

    /**
     *
     */
    protected ITreeApplicationCacheManager treeApplicationCacheManager;

    /**
     *
     */
    protected ITreeSessionCacheManager sessionCacheManager;

    /**
     *
     * @param treeApplicationCacheManager
     */
    public void setTreeApplicationCacheManager(ITreeApplicationCacheManager treeApplicationCacheManager) {
        this.treeApplicationCacheManager = treeApplicationCacheManager;
    }

    /**
     *
     * @param sessionCacheManager
     */
    public void setSessionCacheManager(ITreeSessionCacheManager sessionCacheManager) {
        this.sessionCacheManager = sessionCacheManager;
    }

    /**
     *
     * @param mgaServiceBuilder
     */
    @Override
    public void setMgaServiceBuilder(IMgaServiceBuilder mgaServiceBuilder) {
        this.mgaServiceBuilder = mgaServiceBuilder;
    }

    /**
     *
     * @return
     */
    @Override
    public IMgaServiceBuilder getMgaServiceBuilder() {
        return mgaServiceBuilder;
    }

    /**
     *
     * @return
     */
    @Override
    public ITreeApplicationCacheManager getApplicationCacheManager() {
        return treeApplicationCacheManager;
    }

    /**
     *
     * @return
     */
    @Override
    public ITreeSessionCacheManager getSessionCacheManager() {
        return sessionCacheManager;
    }

    /**
     *
     * @param compositeGroup
     * @param codeConf
     */
    @Override
    public void removeTreeFromSession(ICompositeGroup compositeGroup, Integer codeConfiguration) {
        this.sessionCacheManager.removeFlatTree(compositeGroup, codeConfiguration);
    }

    /**
     *
     */
    @Override
    public void clearTreeFromSession() {
        this.sessionCacheManager.clearFlatTree();
    }

    /**
     *      * get or load if needded, a flat tree for a config and with
     * selectUserActivities</p>
     *      * the flat tree show for each node the state of eachActivity</p>
     *
     * @param configuration
     * @param compositeGroup
     * @param selectUserActivities
     * @return
     */
    @Override
    public ITreeNode<IFlatNode> getOrLoadTree(Optional<IMgaIOConfiguration> configurationOpt, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> selectUserActivities, ICompositeGroup compositeGroup) {
        if (!configurationOpt.isPresent()) {
            return new DefaultTreeNode(null,null);
        }
        IMgaIOConfiguration configuration = configurationOpt.get();
        if (sessionCacheManager.containKeyFlatTree(compositeGroup, configuration.getCodeConfiguration())) {
            return sessionCacheManager.getFlatTree(compositeGroup, configuration.getCodeConfiguration());
        }
        ITreeNode<AbstractBranchNode> skeletonTreeNode = getOrLoadSkeletonTree(configuration);
        Set<Long> nodesWithActivity = new HashSet();
        ITreeNode<IFlatNode> flatTree = buildFlatTree(compositeGroup.getOwnGroup(configuration.getWhichTree()), skeletonTreeNode, selectUserActivities, null, "", nodesWithActivity, configuration);
        wrapStates(flatTree, nodesWithActivity);
        sessionCacheManager.saveFlatTree(compositeGroup, flatTree, configuration.getCodeConfiguration());
        return flatTree;

    }

    /**
     *      * build a flat tree for a configuration config</p>
     *      * for each leaf build all the ordered node for the leaf regarding
     * config</p>
     *      * for each node in branche build or get if exists the itreenode</p>
     *
     * @param config
     * @return
     */
    @Override
    public ITreeNode<AbstractBranchNode> getOrLoadSkeletonTree(IMgaIOConfiguration config) {
        ITreeNode<AbstractBranchNode> root;
        if (treeApplicationCacheManager.getSkeletonTree(config.getCodeConfiguration()) != null) {
            root = treeApplicationCacheManager.getSkeletonTree(config.getCodeConfiguration());
        } else {
            Stream<INode> extractedLeafs = mgaServiceBuilder.loadNodes(config, true, true);
            root = treeApplicationCacheManager.buildSkeletonTree(config, extractedLeafs, true);
        }
        return root;
    }

    /**
     *      * set the state of activity activity of flatNode and all its children to
     * state</p>
     *
     * @param flatTreeNode
     * @param state
     * @param activity
     * @param activitiesForAllGroups
     * @param nodesWithActivity
     */
    protected void setStateIncludeChildrens(ITreeNode<IFlatNode> flatTreeNode, int state, Activities activity, Map<Activities, Map<Group, List<LocalDate>>> activitiesForAllGroups, Set<Long> nodesWithActivity) {
        assert state == UNCHECKED_STATE || state == CHECKED_STATE : new BusinessException("a state for children can only be 0 or 1");
        IFlatNode currentNode = flatTreeNode.getData();
        currentNode.addRealState(activity, state);
        Optional.ofNullable(activitiesForAllGroups.get(activity))
                .map(activities -> activities.get(currentNode.getOwnGroup()))
                .ifPresent(dates -> currentNode.addOwnRealState(activity, state));
        if (CHECKED_STATE == state) {
            activitiesForAllGroups.get(activity).entrySet().stream()
                    .filter(entryGroup -> entryGroup.getValue() != null && !DateUtil.MIN_LOCAL_DATE.equals(entryGroup.getValue().get(0)) && !DateUtil.MIN_LOCAL_DATE.equals(entryGroup.getValue().get(1)))
                    .forEach(entryGroup -> addGroupActivityForActivity(currentNode, activity, entryGroup));
        } else {
            currentNode.getActivitiesForAllGroups().remove(activity);
            currentNode.getOwnActivities().remove(activity);
        }
        flatTreeNode.getChildren().stream()
                .filter(flatChildTreeNode -> Optional.ofNullable(flatChildTreeNode.getData()).map(data -> !nodesWithActivity.contains(data.getId())).orElse(Boolean.TRUE))
                .forEach((flatChildTreeNode) -> setStateIncludeChildrens(flatChildTreeNode, state, activity, activitiesForAllGroups, nodesWithActivity));
    }

    /**
     *      * for flatnode node, setRealState if isChecked<:p>
     *
     * @param flatNode
     * @param activity
     * @param state
     * @param datesForGroup
     */
    protected void setState(IFlatNode flatNode, Activities activity, int state, Map<Group, List<LocalDate>> datesForGroup) {
        if (flatNode != null) {
            if (UNCHECKED_STATE != state) {
                flatNode.addRealState(activity, state);
                flatNode.getActivitiesForAllGroups().put(activity, datesForGroup);
            } else {
                flatNode.getRealState().remove(activity);
                flatNode.getActivitiesForAllGroups().remove(activity);
            }
        }
    }

    /**
     *
     * @param flatNode
     * @param activity
     * @param ownState
     * @param ownDates
     */
    protected void setOwnState(IFlatNode flatNode, Activities activity, int ownState, List<LocalDate> ownDates) {
        if (flatNode != null) {
            if (UNCHECKED_STATE != ownState) {
                flatNode.addOwnRealState(activity, ownState);
                flatNode.getOwnActivities().put(activity, ownDates);
            } else {
                flatNode.getOwnRealState().remove(activity);
                flatNode.getOwnActivities().remove(activity);
            }
        }
    }

    /**
     *      * return a list with state ConflicDateBeg ConflicDateEnd </p>
     *
     * @param node
     * @param activity
     * @param currentState
     * @param datesForGroups
     * @return
     */
    protected Object getStateNodeAccordingChildren(ITreeNode<IFlatNode> node, Activities activity, int currentState, Map<Group, List<LocalDate>> datesForGroups) {

        boolean indeterminate = currentState == INDETERMINATE_STATE;
        boolean checked = currentState == CHECKED_STATE;
        boolean unchecked = currentState == UNCHECKED_STATE;
        try {
            for (ITreeNode<IFlatNode> nChild : node.getChildren()) {
                currentState = nChild.getData().getRealState(activity);
                switch (currentState) {
                    case UNCHECKED_STATE:
                        unchecked = true;
                        break;
                    case CHECKED_STATE:
                        checked = true;
                        break;
                    case INDETERMINATE_STATE:
                        indeterminate = true;
                        break;
                    default:
                        unchecked = true;
                        break;
                }
                final Map<Group, List<LocalDate>> date1 = Optional.ofNullable(datesForGroups).orElseGet(HashMap::new);
                final Map<Group, List<LocalDate>> date2 = nChild.getData().getActivitiesForAllGroups().getOrDefault(activity, new HashMap<>());
                datesForGroups = testConflictDate(date1, date2);
                if ((checked && unchecked) || indeterminate) {
                    return Arrays.asList(INDETERMINATE_STATE, datesForGroups);
                }
            }
            return Arrays.asList(checked ? CHECKED_STATE : UNCHECKED_STATE, datesForGroups);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        return null;
    }

    /**
     *      * return a list with state ConflicDateBeg ConflicDateEnd </p>
     *
     * @param node the node
     * @param activity
     * @param currentState the state of one child of node
     * @param dates the dates of this child
     * @return
     */
    protected Object getOwnStateNodeAccordingChildren(ITreeNode<IFlatNode> node, Activities activity, int currentState, List<LocalDate> dates) {

        boolean indeterminate = currentState == INDETERMINATE_STATE;
        boolean checked = currentState == CHECKED_STATE;
        boolean unchecked = currentState == UNCHECKED_STATE;
        try {
            for (ITreeNode<IFlatNode> nChild : node.getChildren()) {
                final IFlatNode flatChildNode = nChild.getData();
                currentState = flatChildNode.getOwnRealState().getOrDefault(activity, 0);
                switch (currentState) {
                    case UNCHECKED_STATE:
                        unchecked = true;
                        break;
                    case CHECKED_STATE:
                        checked = true;
                        break;
                    case INDETERMINATE_STATE:
                        indeterminate = true;
                        break;
                    default:
                        unchecked = true;
                        break;
                }
                final List<LocalDate> childDates = Optional.ofNullable(nChild.getData().getOwnActivities())
                        .map(a -> a.get(activity))
                        .orElse(null);
                dates = mergeGroupDates(dates, childDates);
                if ((checked && unchecked) || indeterminate) {
                    return Arrays.asList(INDETERMINATE_STATE, dates);
                }
            }
            return Arrays.asList(checked ? CHECKED_STATE : UNCHECKED_STATE, dates);

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        return null;
    }

    /**
     *      * test if dateInNode is in conflict with dateBegCurrNode</p>
     *
     * @param datesForGroupsForParent
     * @param datesForGroupsForChild
     * @return
     */
    protected Map<Group, List<LocalDate>> testConflictDate(Map<Group, List<LocalDate>> datesForGroupsForParent, Map<Group, List<LocalDate>> datesForGroupsForChild) {
        final Stream<Group> datesStreamConcat = Stream.concat(
                datesForGroupsForChild.keySet().stream(),
                datesForGroupsForParent.keySet().stream()
        );
        Map<Group, List<LocalDate>> datesForGroups = datesStreamConcat
                .distinct()
                .collect(Collectors.toMap(
                        group -> group,
                        group -> mergeGroupDates(datesForGroupsForChild.get(group), datesForGroupsForParent.get(group))));
        return datesForGroups;
    }

    /**
     *
     * @param childDates
     * @param parentDates
     * @return
     */
    protected List<LocalDate> mergeGroupDates(List<LocalDate> childDates, List<LocalDate> parentDates) {
        if (childDates == null) {
            return parentDates;
        }
        if (parentDates == null) {
            return childDates;
        }
        return Stream.of(0, 1)
                .map(i -> merge(childDates.get(i), parentDates.get(i)))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param d1
     * @param d2
     * @return
     */
    protected LocalDate merge(LocalDate d1, LocalDate d2) {
        if (d1 == null && d2 == null) {
            return null;
        } else if (d1 != null && d1.equals(d2)) {
            return d1;
        } else {
            return DateUtil.MIN_LOCAL_DATE;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public IMgaIOConfigurator getMgaIOConfigurator() {
        return this.treeApplicationCacheManager.getMgaIOConfigurator();
    }

    /**
     *      * Build a flat tree from tree and add it to tinyTree children </p>
     *      * when a flatNode has activity its id is adding to checkedActivity</p>
     *
     * @param ownGroup
     * @param tree
     * @param activities
     * @param tinyTree
     * @param tinyTreePath
     * @param nodesWithActivity
     * @param config
     * @return
     */
    @Override
    public ITreeNode<IFlatNode> buildFlatTree(Group ownGroup, ITreeNode<AbstractBranchNode> tree, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activities, ITreeNode<IFlatNode> tinyTree, String tinyTreePath, Set<Long> nodesWithActivity, IMgaIOConfiguration config) {
        if (tinyTree == null && tree != null) {
            tinyTree = new DefaultTreeNode(null, config);
        }
        if (tree != null) {
            for (ITreeNode<AbstractBranchNode> childrenTree : tree.getChildren()) {
                buildFlatNodeForBranchNode(ownGroup, childrenTree.getData(), activities, nodesWithActivity, tinyTree, tinyTreePath, childrenTree, config);
            }
        }
        return tinyTree;
    }

    /**
     *      * Take a branchNode and build a flatNode add to child of tinyTree</p>
     *
     * @param ownGroup
     * @param branchNode
     * @param activities
     * @param nodesWithActivities
     * @param tinyTree
     * @param tinyTreePath
     * @param childrenTree
     * @param config
     */
    protected void buildFlatNodeForBranchNode(Group ownGroup, AbstractBranchNode branchNode, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activities, Set<Long> nodesWithActivities, ITreeNode<IFlatNode> tinyTree, String tinyTreePath, ITreeNode<AbstractBranchNode> childrenTree, IMgaIOConfiguration config) {
        if (branchNode != null && !branchNode.isAnAncestor()) {
            IFlatNode flatNode = new FlatNode(branchNode, ownGroup);
            setState(flatNode, activities, nodesWithActivities);
            ITreeNode childTinyTree = new DefaultTreeNode(flatNode.getTypeResource().getSimpleName(), flatNode, tinyTree, config);
            final String childrenPath = Strings.isNullOrEmpty(tinyTreePath)
                    ? flatNode.getCode()
                    : String.format("%s%s%s", tinyTreePath, PatternConfigurator.PATH_SEPARATOR, flatNode.getCode());
            flatNode.setPath(childrenPath);
            if (!childrenTree.isLeaf()) {
                buildFlatTree(ownGroup, childrenTree, activities, childTinyTree, childrenPath, nodesWithActivities, config);
            }
        }
    }

    /**
     *      * When a flatnode is created and user has role add activities that match
     * flatNode id</p>
     *
     * @param flatNode
     * @param role
     * @param nodesWithActivity
     */
    protected void setState(IFlatNode flatNode, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role, Set<Long> nodesWithActivity) {
        if (role != null) {
            role.entrySet().stream()
                    .filter(entryPriv -> entryPriv.getValue().containsKey(flatNode.getId()))
                    .forEach(entryPriv -> addActivityToFlatNode(flatNode, role, nodesWithActivity, entryPriv.getKey()));
        }
    }

    /**
     *      * add activity to flatNode </p>
     *
     * @param flatNode
     * @param role
     * @param nodesWithActivity
     * @param activity
     */
    protected void addActivityToFlatNode(IFlatNode flatNode, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role, Set<Long> nodesWithActivity, Activities activity) {
        assert role.get(activity) != null && role.get(activity).containsKey(flatNode.getId()) : new BusinessException("can only add activity to flatNode with activity");
        final Map<Group, List<LocalDate>> activitiesForNode = role.get(activity).get(flatNode.getId());
        flatNode.addActivity(activity, activitiesForNode);
        Optional.ofNullable(activitiesForNode.get(flatNode.getOwnGroup()))
                .ifPresent(dates -> flatNode.addOwnActivity(activity, dates));
        nodesWithActivity.add(flatNode.getId());
    }

    @Override
    public <T extends IDataNode> ITreeNode<T> getTreeNodeByID(ITreeNode<T> root, Long id) {

        long idNode = id;
        ITreeNode r = null;

        if (root.getParent() != null && root.getData().getId() == idNode) {
            return root;
        }

        for (ITreeNode childrenTree : root.getChildren()) {
            if ((childrenTree.getData()).getId() == idNode) {
                r = childrenTree;
            } else if (!childrenTree.isLeaf()) {
                r = getTreeNodeByID(childrenTree, id);
            }
            if (r != null) {
                return r;
            }
        }

        return null;
    }

    /**
     * <i> when loading tree </i>
     *      * calculate for each node the state of each activity
     *
     * @param <T>
     * @param tree
     * @param nodesWithActivity
     * @return
     */
    protected <T extends IDataNode> ITreeNode<T> wrapStates(ITreeNode<T> tree, Set<Long> nodesWithActivity) {
        HashMap<Long, ITreeNode> tmpResultSearchNodes = new HashMap();
        nodesWithActivity.stream()
                .map((checkedNode) -> tmpResultSearchNodes.computeIfAbsent(checkedNode, k -> getTreeNodeByID(tree, checkedNode)))
                .forEach((treeNodeByID) -> warpStatesForNode(treeNodeByID, nodesWithActivity));
        return tree;
    }

    /**
     * <i> when loading tree </i>
     *      * calculate for this node the state of each activity
     *
     * @param treeNodeByID
     * @param nodesWithActivity
     */
    protected void warpStatesForNode(ITreeNode<IFlatNode> treeNodeByID, Set<Long> nodesWithActivity) {
        IFlatNode curNode = treeNodeByID.getData();
        for (Activities activity : curNode.getActivitiesForAllGroups().keySet()) {
            wrapStateForNode(treeNodeByID, activity, nodesWithActivity);
        }
    }

    /**
     * <i> when loading tree </i>
     * wrap states including ancestor and parentNode for activitys </p>
     *      * then recalculate state for parent </p>
     *
     * @param node
     * @param activity
     * @param nodesWithActivity
     */
    protected void wrapStateForNode(ITreeNode<IFlatNode> node, Activities activity, Set<Long> nodesWithActivity) {
        setStateIncludeChildrens(node, CHECKED_STATE, activity, node.getData().getActivitiesForAllGroups(), nodesWithActivity);
        Map<Group, List<LocalDate>> datesForGroups = node.getData().getActivitiesForAllGroups().get(activity);
        List<LocalDate> ownDates = node.getData().getOwnActivities().get(activity);
        ITreeNode<IFlatNode> parentNode = node.getParent();
        while (parentNode != null && parentNode.getData() != null && !nodesWithActivity.contains(parentNode.getData().getId())) {
            parentNode = warpStateAndReturnParent(parentNode, activity, node, datesForGroups, ownDates);
        }
    }

    /**
     *
     * @param parentNode
     * @param activity
     * @param node
     * @param datesForGroups
     * @param ownDates
     * @return
     */
    protected ITreeNode<IFlatNode> warpStateAndReturnParent(ITreeNode<IFlatNode> parentNode, Activities activity, ITreeNode<IFlatNode> node, Map<Group, List<LocalDate>> datesForGroups, List<LocalDate> ownDates) {
        setRealState(parentNode, activity, node, datesForGroups);
        setOwnRealState(parentNode, activity, node, ownDates);
        return parentNode.getParent();
    }

    /**
     *
     * @param parentNode
     * @param activity
     * @param node
     * @param ownDates
     */
    protected void setOwnRealState(ITreeNode<IFlatNode> parentNode, Activities activity, ITreeNode<IFlatNode> node, List<LocalDate> ownDates) {
        Object ownStateNodeAccordingChildren = getOwnStateNodeAccordingChildren(parentNode, activity, node.getData().getOwnRealState().getOrDefault(activity, 0), ownDates);
        int ownRealState = (int) ((List) ownStateNodeAccordingChildren).get(0);
        ownDates = (List<LocalDate>) ((List) ownStateNodeAccordingChildren).get(1);
        setOwnState(parentNode.getData(), activity, ownRealState, ownDates);
    }

    /**
     *
     * @param parentNode
     * @param activity
     * @param node
     * @param datesForGroups
     */
    protected void setRealState(ITreeNode<IFlatNode> parentNode, Activities activity, ITreeNode<IFlatNode> node, Map<Group, List<LocalDate>> datesForGroups) {
        Object stateNodeAccordingChildren = getStateNodeAccordingChildren(parentNode, activity, node.getData().getRealState().get(activity), datesForGroups);
        int realState = (int) ((List) stateNodeAccordingChildren).get(0);
        datesForGroups = (Map<Group, List<LocalDate>>) ((List) stateNodeAccordingChildren).get(1);
        setState(parentNode.getData(), activity, realState, datesForGroups);
    }

}
