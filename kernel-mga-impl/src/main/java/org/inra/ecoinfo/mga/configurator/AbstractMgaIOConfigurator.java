/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.configurator;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author tcherniatinsky
 */
public abstract class AbstractMgaIOConfigurator implements IMgaIOConfigurator {

    /**
     *
     */
    public static final int REFDATA_CONFIGURATION = 3;

    /**
     *
     */
    public static final int REFDATA_CONFIGURATION_RIGHTS = 1;

    /**
     *
     */
    public static final int DATASET_CONFIGURATION = 2;

    /**
     *
     */
    public static final int DATASET_CONFIGURATION_RIGHTS = 0;

    /**
     *
     */
    public static final int ASSOCIATE_CONFIGURATION = 4;

    /**
     *
     */
    public static final int SYNTHESIS_CONFIGURATION = 5;

    /**
     *
     */
    public static final int GENERIC_DATATYPE_CONFIGURATION = 6;

    private static final Map<Integer, IMgaIOConfiguration> configurations = new HashMap();

    /**
     *
     */
    protected final AbstractMgaDisplayerConfiguration displayerConfiguration;

    /**
     *
     */
    protected final Map<String, INodeable> extractedNodeables;

    /**
     *
     */
    protected int deepestIndexNode = 0;

    /**
     *
     * @param displayerConfiguration
     */
    public AbstractMgaIOConfigurator(AbstractMgaDisplayerConfiguration displayerConfiguration) {
        this.extractedNodeables = new ConcurrentHashMap();
        this.displayerConfiguration = displayerConfiguration;
        initConfigurations();
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Integer, IMgaIOConfiguration> getConfigurations() {
        return configurations;
    }

    /**
     *
     */
    protected abstract void initConfigurations();

    /**
     *
     * @param codeConf
     * @return
     */
    @Override
    public Optional<IMgaIOConfiguration> getConfiguration(Integer codeConfiguration) {
        return Optional.ofNullable(codeConfiguration)
                .map(code->configurations.get(code));
    }

    /**
     *
     * @return
     */
    @Override
    public Map<String, INodeable> getExtractedNodeables() {
        return extractedNodeables;
    }

    /**
     *
     * @return
     */
    @Override
    public int getDeepestIndexNode() {
        return deepestIndexNode;
    }

    /**
     *
     * @param deepestIndexNode
     */
    @Override
    public void setDeepestIndexNode(int deepestIndexNode) {
        this.deepestIndexNode = deepestIndexNode;
    }

    /**
     *
     * @param conf
     * @return
     */
    @Override
    public List<String> getColumnsTreeNames(Integer codeConfiguration) {
        return configurations.get(codeConfiguration).getColumnsTreeNames();
    }

    /**
     *
     * @param conf
     * @return
     */
    @Override
    public List<String> getColumnsTreeNames(Integer codeConfiguration, List<Optional<RealNode>> branche) {
        List<String> columsNameslist = new LinkedList<>();
        Class<? extends INodeable> columnType = null;
        int index = 0;
        for (Optional<RealNode> realnodeOpt : branche) {
            if (!realnodeOpt.isPresent()) {
                return configurations.get(codeConfiguration).getColumnsTreeNames();
            }
            RealNode currentNode = realnodeOpt.get();
            if (columnType == currentNode.getNodeable().getClass()) {
                index++;
            } else {
                columnType = currentNode.getNodeable().getClass();
            }
            final String[] columnsNames = displayerConfiguration.getDisplayColumnNames().get(columnType);
            if(index>columnsNames.length){
                index= columnsNames.length-1;
            }
            String columnName = columnsNames[index];
            columsNameslist.add(columnName);
        }
        return columsNameslist;
    }

    /**
     *
     * @param <T>
     * @param columnType
     * @param conf
     */
    @Override
    public <T extends INodeable> void addColumn(Class<T> columnType, int index, Integer codeConfiguration) {
        if (displayerConfiguration.getDisplayColumnNames().containsKey(columnType)) {
            displayerConfiguration.autoUpdateColumn(index);
        }
        configurations.get(codeConfiguration).addColumn(
                displayerConfiguration
                        .getDisplayColumnNames()
                        .computeIfAbsent(columnType, k -> new String[index + 1])[index],
                index
        );
    }

    /**
     *
     * @param conf
     */
    @Override
    public void clearColumns(Integer codeConfiguration) {
        configurations.get(codeConfiguration).clearColumns();
    }

}
