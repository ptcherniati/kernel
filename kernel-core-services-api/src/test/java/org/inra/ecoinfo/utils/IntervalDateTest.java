package org.inra.ecoinfo.utils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.junit.*;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class IntervalDateTest {

    /**
     *
     */
    protected static final String DATE_PATTERN = "yyyy-MM-dd";

    /**
     *
     */
    protected static final String TIME_PATTERN = "HH:mm:ss";

    /**
     *
     */
    protected static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     *
     */
    protected static final String START_DATE = "2010-01-23";

    /**
     *
     */
    protected static final String START_TIME = "22:15:56";

    /**
     *
     */
    protected static final String START_DATE_TIME = "2010-01-23T22:15:56";

    /**
     *
     */
    protected static final String END_DATE = "2012-12-23";

    /**
     *
     */
    protected static final String END_TIME = "22:15:56";

    /**
     *
     */
    protected static final String END_DATE_TIME = "2012-12-23T21:19:50";
    private static LocalDateTime localStartDateTime, localEndDateTime;

    /**
     * @throws DateTimeParseException
     */
    @BeforeClass
    public static void init() throws DateTimeParseException {
        localStartDateTime = DateUtil.readLocalDateTimeFromText(DATE_TIME_PATTERN, START_DATE_TIME);
        localEndDateTime = DateUtil.readLocalDateTimeFromText(DATE_TIME_PATTERN, END_DATE_TIME);
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    @Mock
            ILocalizationManager localizationManager;
    IntervalDate instance;
    /**
     *
     */
    public IntervalDateTest() {
    }

    /**
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Before
    public void setUp() throws BadExpectedValueException {
        MockitoAnnotations.openMocks(this);
        instance = new IntervalDate(START_DATE_TIME, END_DATE_TIME, DATE_TIME_PATTERN);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getIntervalDateddMMyyyy method, of class IntervalDate.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetIntervalDateddMMyyyy() throws Exception {
        IntervalDate result = IntervalDate.getIntervalDateddMMyyyy("23-01-2012", "30-12-2015");
        assertEquals("du 23-01-2012 au 30-12-2015", result.toString());
    }

    /**
     * Test of getIntervalDateMMyyyy method, of class IntervalDate.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetIntervalDateMMyyyy() throws Exception {
        IntervalDate result = IntervalDate.getIntervalDateMMyyyy("01-2012", "12-2015");
        assertEquals("du 01-01-2012 au 31-12-2015", result.toString());
    }

    /**
     * Test of getIntervalDateyyyy method, of class IntervalDate.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetIntervalDateyyyy() throws Exception {
        IntervalDate result = IntervalDate.getIntervalDateyyyy("2012", "2015");
        assertEquals("du 2012-01-01 au 2015-12-31", result.toString());
    }

    /**
     * Test of getIntervalDateyyyyMM method, of class IntervalDate.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetIntervalDateyyyyMM() throws Exception {
        IntervalDate result = IntervalDate.getIntervalDateyyyyMM("2012-01", "2015-05");
        assertEquals("du 01-01-2012 au 31-12-2015", result.toString());
    }

    /**
     * Test of getIntervalDateyyyyMMdd method, of class IntervalDate.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetIntervalDateyyyyMMdd() throws Exception {
        IntervalDate result = IntervalDate.getIntervalDateyyyyMMdd("2012-01-21", "2015-05-05");
        assertEquals("du 2012-01-21 au 2015-05-05", result.toString());
    }

    /**
     * Test of getLocalBeginDate method, of class IntervalDate.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Test
    public void testGetLocalBeginDate() throws BadExpectedValueException {
        IntervalDate instance = new IntervalDate(START_DATE_TIME, END_DATE_TIME, DATE_TIME_PATTERN);
        LocalDateTime result = instance.getBeginDate();
        assertEquals(localStartDateTime, result);
    }

    /**
     * Test of getUTCBeginDate method, of class IntervalDate.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
//    @Test
//    public void testGetUTCBeginDate() throws BadExpectedValueException {
//        System.out.println("getUTCBeginDate");
//        IntervalDate instance = new IntervalDate(START_DATE_TIME, END_DATE_TIME, DATE_TIME_PATTERN);
//        LocalDateTime result = instance.getUTCBeginDate();
//        assertEquals(utcStartDateTime, result);
//    }

    /**
     * Test of getBeginDateToString method, of class IntervalDate.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Test
    public void testGetBeginDateToString() throws BadExpectedValueException {
        IntervalDate instance = new IntervalDate(START_DATE_TIME, END_DATE_TIME, DATE_TIME_PATTERN);
        String result = instance.getBeginDateToString();
        assertEquals(START_DATE_TIME, result);
    }

    /**
     * Test of getLocalEndDate method, of class IntervalDate.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Test
    public void testGetLocalEndDate() throws BadExpectedValueException {
        IntervalDate instance = new IntervalDate(START_DATE_TIME, END_DATE_TIME, DATE_TIME_PATTERN);
        LocalDateTime result = instance.getEndDate();
        assertEquals(localEndDateTime, result);
    }

    /**
     * Test of getUTCEndDate method, of class IntervalDate.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
//    @Test
//    public void testGetUTCEndDate() throws BadExpectedValueException {
//        System.out.println("getUTCEndDate");
//        IntervalDate instance = new IntervalDate(START_DATE_TIME, END_DATE_TIME, DATE_TIME_PATTERN);
//        Date result = instance.getUTCEndDate();
//        assertEquals(utcEndDateTime, result);
//    }

    /**
     * Test of getEndDateToString method, of class IntervalDate.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Test
    public void testGetEndDateToString() throws BadExpectedValueException {
        IntervalDate instance = new IntervalDate(START_DATE_TIME, END_DATE_TIME, DATE_TIME_PATTERN);
        String result = instance.getEndDateToString();
        assertEquals(END_DATE_TIME, result);
    }

    /**
     * Test of toString method, of class IntervalDate.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Test
    public void testToString() throws BadExpectedValueException {
        IntervalDate instance = new IntervalDate(START_DATE_TIME, END_DATE_TIME, DATE_TIME_PATTERN);
        String result = instance.toString();
        assertEquals("du " + START_DATE_TIME + " au " + END_DATE_TIME, result);
    }

    /**
     * Test of getDate method, of class IntervalDate.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetDate() throws Exception {
        IntervalDate instance = new IntervalDate("dd/MM/yyyy");
        LocalDateTime result = instance.getDate("23/01/2012");
        assertEquals("2012-01-23T00:00", result.toString());
        instance = new IntervalDate("HH:mm");
        result = instance.getDate("23:30");
        assertEquals("1970-01-01T23:30", result.toString());
        instance = new IntervalDate("dd/MM/yyyy HH:mm");
        result = instance.getDate("23/01/2012 23:30");
        assertEquals("2012-01-23T23:30", result.toString());
    }

    /**
     * Test of setDates method, of class IntervalDate.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Test
    public void testSetDates() throws DateTimeParseException, BadExpectedValueException {
        IntervalDate instance = new IntervalDate(DATE_TIME_PATTERN);
        instance.setDates(localStartDateTime, localEndDateTime);
        assertEquals(START_DATE_TIME, instance.getBeginDateToString());
        assertEquals(END_DATE_TIME, instance.getEndDate().toString());
        assertEquals("PT25559H3M54S", instance.getDuration().toString());
    }

    /**
     * Test of setDateFormatter method, of class IntervalDate.
     */
    @Test
    public void testSetDateFormat() {
        IntervalDate instance = this.instance;
        assertEquals(START_DATE_TIME, instance.getBeginDateToString());
        instance.setDateFormatter(DATE_PATTERN);
        assertEquals(START_DATE, instance.getBeginDateToString());
        instance.setDateFormatter(TIME_PATTERN);
        assertEquals(START_TIME, instance.getBeginDateToString());
    }

    /**
     * Test of compareTo method, of class IntervalDate.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Test
    public void testCompareTo() throws BadExpectedValueException {
        IntervalDate o = null;
        IntervalDate instance = new IntervalDate("2012-01-01", "2012-12-31", DATE_PATTERN);
        assertEquals(0, instance.compareTo(instance));
        o = new IntervalDate("2012-01-01", "2012-12-31", DATE_PATTERN);
        assertEquals(0, instance.compareTo(o));
        o = new IntervalDate("2011-12-31", "2012-12-31", DATE_PATTERN);
        assertEquals(-1, instance.compareTo(o));
        o = new IntervalDate("2012-01-02", "2013-12-31", DATE_PATTERN);
        assertEquals(1, instance.compareTo(o));
        o = new IntervalDate("2012-01-01", "2012-12-30", DATE_PATTERN);
        assertEquals(-1, instance.compareTo(o));
        o = new IntervalDate("2012-01-01", "2013-01-01", DATE_PATTERN);
        assertEquals(1, instance.compareTo(o));
    }

    /**
     * Test of equals method, of class IntervalDate.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BadExpectedValueException
     */
    @Test
    public void testEquals() throws BadExpectedValueException {
        assertEquals(instance, instance);
        assertEquals(new IntervalDate(START_DATE_TIME, END_DATE_TIME, DATE_TIME_PATTERN), instance);
        assertNotEquals(new IntervalDate(START_DATE, END_DATE, DATE_PATTERN), instance);
    }

    /**
     * Test of getBeginDate method, of class IntervalDate.
     */
    @Test
    public void testGetBeginDate() {
        assertEquals(START_DATE_TIME, instance.getBeginDate().toString());
    }

    /**
     * Test of getEndDate method, of class IntervalDate.
     */
    @Test
    public void testGetEndDate() {
        assertEquals(END_DATE_TIME, instance.getEndDate().toString());
    }

    /**
     * Test of isInto method, of class IntervalDate.
     */
    @Test
    public void testIsInto_LocalDateTime() {
        assertTrue(instance.isInto(localStartDateTime));
        assertFalse(instance.isInto(localStartDateTime.minus(1, ChronoUnit.MILLIS)));
        assertTrue(instance.isInto(localEndDateTime));
        assertFalse(instance.isInto(localEndDateTime.plus(1, ChronoUnit.MILLIS)));
        assertTrue(instance.isInto(LocalDateTime.of(2_011, Month.MARCH, 1, 0, 0)));
    }

    /**
     * Test of isInto method, of class IntervalDate.
     */
    @Test
    public void testIsInto_3args() {
        assertTrue(instance.isInto(LocalDateTime.of(2_011, Month.MARCH, 1, 0, 0), true, true));
        assertTrue(instance.isInto(localStartDateTime, true, true));
        assertTrue(instance.isInto(localStartDateTime, true, false));
        assertFalse(instance.isInto(localStartDateTime, false, true));
        assertTrue(instance.isInto(localEndDateTime, true, true));
        assertFalse(instance.isInto(localEndDateTime, true, false));
        assertTrue(instance.isInto(localEndDateTime, false, true));
    }

    /**
     * Test of overlaps method, of class IntervalDate.
     */
    @Test
    public void testOverlaps() {
        boolean result = instance.overlaps(instance);
        assertTrue(instance.overlaps(instance));
    }

    /**
     * Test of getDuration method, of class IntervalDate.
     */
    @Test
    public void testGetDuration() {
        Duration result = instance.getDuration();
        assertEquals("PT25559H3M54S", result.toString());
    }
}
