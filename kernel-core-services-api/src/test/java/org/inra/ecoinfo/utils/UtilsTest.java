package org.inra.ecoinfo.utils;

import com.Ostermiller.util.CSVParser;
import java.io.BufferedInputStream;
import java.io.File;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.*;
import static org.junit.Assert.*;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 * @author ptcherniati
 */
public class UtilsTest {

    /**
     *
     */
    protected static final String ERREUR_MESSAGE = "L'argument n°%d ne peut pas être null";

    /**
     *
     */
    protected static final String ERREUR_MESSAGE2 = "L'objet \"%s\" n'est pas une instance de \"%s\"";

    /**
     *
     */
    protected static final String ISO_FILE = "src/test/resources/org/inra/ecoinfo/utils/fichierISO1.csv";

    /**
     *
     */
    protected static final String UTF_8 = "src/test/resources/org/inra/ecoinfo/utils/fichierUTF-8.csv";

    /**
     *
     */
    protected static final String SANITIZE_TEXT = "src/test/resources/org/inra/ecoinfo/utils/sanitizeText.csv";

    /**
     *
     */
    protected static final String EMPTY_LINE_FILE = "src/test/resources/org/inra/ecoinfo/utils/emptyLine.csv";

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    @Mock
            ILocalizationManager localizationManager;
    /**
     *
     */
    public UtilsTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(localizationManager.getMessage("org.inra.ecoinfo.utils.messages", "PROPERTY_MSG_ERROR")).thenReturn("Erreur");
        when(localizationManager.getMessage("org.inra.ecoinfo.utils.messages", "PROPERTY_MSG_NO_NULL_ARG")).thenReturn(ERREUR_MESSAGE);
        when(localizationManager.getMessage("org.inra.ecoinfo.utils.messages", "PROPERTY_MSG_MISMATCH_CLASS_OBJECT")).thenReturn(ERREUR_MESSAGE2);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of createCodeFromString method, of class Utils.
     */
    @Test
    public void testCreateCodeFromString() {
        assertEquals("a_a_c_e_e_e_i_o_u_u", Utils.createCodeFromString("à  â ç è é ê î'ô ù\"û"));
        assertNull(Utils.createCodeFromString(null));
    }

    /**
     * Test of createIdFromString method, of class Utils.
     */
    @Test
    public void testCreateIdFromString() {
        assertEquals("acacleeeiouue", Utils.createIdFromString("à  Câ ç Lè é ê î  ô ù ûĖ"));
    }

    /**
     *
     */
    @Test
    public void instance() {
        Object l = 2L;
        assertTrue(l.getClass() == Long.class);
        assertFalse(l.getClass() == Number.class);
    }

    /**
     * Test of testCastedArguments method, of class Utils.
     */
    @Test
    public void testTestCastedArguments() {
        String nullString = null;
        Utils.testCastedArguments("bonjour", String.class, localizationManager);
        Utils.testCastedArguments(2L, Long.class, localizationManager);
        try {
            Utils.testCastedArguments(3L, Number.class, localizationManager);
        } catch (IllegalArgumentException e) {
            assertEquals("L'objet \"java.lang.Long\" n'est pas une instance de \"java.lang.Number\"", e.getMessage());
        }
        try {
            Utils.testCastedArguments("bonjour", Object.class, localizationManager);
        } catch (IllegalArgumentException e) {
            assertEquals("L'objet \"java.lang.String\" n'est pas une instance de \"java.lang.Object\"", e.getMessage());
        }
        try {
            Utils.testCastedArguments(null, Object.class, localizationManager);
        } catch (IllegalArgumentException e) {
            assertEquals("Erreur BusinessException	 - L'argument n°1 ne peut pas être null \n", e.getMessage());
        }
    }

    /**
     * Test of testNotNullArguments method, of class Utils.
     */
    @Test
    public void testTestNotNullArguments() {
        Object nullObject = null;
        ErrorsReport result = Utils.testNotNullArguments(localizationManager, "coucou");
        assertFalse(result.hasErrors());
        result = Utils.testNotNullArguments(localizationManager, "coucou", "au revoir");
        assertFalse(result.hasErrors());
        result = Utils.testNotNullArguments(localizationManager);
        assertFalse(result.hasErrors());
        try {
            result = Utils.testNotNullArguments(localizationManager, nullObject);
        } catch (IllegalArgumentException e) {
            assertEquals("Erreur BusinessException	 - L'argument n°1 ne peut pas être null \n", e.getMessage());
        }
        try {
            result = Utils.testNotNullArguments(localizationManager, "chat", null, "chien");
        } catch (IllegalArgumentException e) {
            assertEquals("Erreur BusinessException	 - L'argument n°2 ne peut pas être null \n", e.getMessage());
        }
    }

    /**
     * Test of isPOSIXFree method, of class Utils.
     */
    @Test
    public void testIsPOSIXFree() {
        for (String POSIX_CAR : Utils.POSIX_CAR) {
            assertFalse(Utils.isPOSIXFree("b" + POSIX_CAR + "c"));
        }
        assertFalse(Utils.isPOSIXFree("ab*"));
        assertFalse(Utils.isPOSIXFree("$bc"));
        assertTrue(Utils.isPOSIXFree("abc"));
        assertTrue(Utils.isPOSIXFree(""));
        assertTrue(Utils.isPOSIXFree(null));
    }

    /**
     * Test of convertEncoding method, of class Utils.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testConvertEncoding() throws Exception {
        System.out.println("convertEncoding");
        File isoFile = new File(ISO_FILE);
        File utfFile = new File(UTF_8);
        byte[] isoByteArray = FileUtils.readFileToByteArray(isoFile);
        byte[] utfByteArray = FileUtils.readFileToByteArray(utfFile);
        byte[] result = Utils.convertEncoding(isoByteArray, Utils.ENCODING_UTF8);
        assertArrayEquals(utfByteArray, result);
        result = Utils.convertEncoding(utfByteArray, Utils.ENCODING_UTF8);
        assertArrayEquals(utfByteArray, result);
        result = Utils.convertEncoding(isoByteArray, Utils.ENCODING_ISO_8859_15);
        assertArrayEquals(isoByteArray, result);
        result = Utils.convertEncoding(utfByteArray, Utils.ENCODING_ISO_8859_15);
        assertArrayEquals(isoByteArray, result);
    }

    /**
     * Test of detectStreamEncoding method, of class Utils.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDetectStreamEncoding_BufferedInputStream() throws Exception {
        System.out.println("detectStreamEncoding");
        File isoFile = new File(ISO_FILE);
        File utfFile = new File(UTF_8);
        BufferedInputStream isoByteBIS = new BufferedInputStream(FileUtils.openInputStream(isoFile));
        BufferedInputStream utfByteBIS = new BufferedInputStream(FileUtils.openInputStream(utfFile));
        String returnValue = Utils.detectStreamEncoding(isoByteBIS);
        assertEquals(Utils.ENCODING_ISO_8859_15, returnValue);
        returnValue = Utils.detectStreamEncoding(utfByteBIS);
        assertEquals(Utils.ENCODING_UTF8.toLowerCase(), returnValue.toLowerCase());
    }

    /**
     * Test of detectStreamEncoding method, of class Utils.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDetectStreamEncoding_byteArr() throws Exception {
        System.out.println("detectStreamEncoding");
        File isoFile = new File(ISO_FILE);
        File utfFile = new File(UTF_8);
        byte[] isoByteArray = FileUtils.readFileToByteArray(isoFile);
        byte[] utfByteArray = FileUtils.readFileToByteArray(utfFile);
        String returnValue = Utils.detectStreamEncoding(isoByteArray);
        assertEquals(Utils.ENCODING_ISO_8859_15, returnValue);
        returnValue = Utils.detectStreamEncoding(utfByteArray);
        assertEquals(Utils.ENCODING_UTF8.toLowerCase(), returnValue.toLowerCase());
    }

    /**
     * Test of matchExceptionCause method, of class Utils.
     */
    @Test
    public void testMatchExceptionCause() {
        System.out.println("matchExceptionCause");
        Exception1 exception1 = mock(Exception1.class);
        Exception exception2 = mock(Exception2.class);
        when(exception2.getCause()).thenReturn(new Exception1());
        Exception exception3 = mock(Exception3.class);
        when(exception3.getCause()).thenReturn(exception2);
        Boolean result = Utils.matchExceptionCause(exception1, Exception1.class);
        assertFalse(result);
        result = Utils.matchExceptionCause(exception2, Exception1.class);
        assertTrue(result);
        result = Utils.matchExceptionCause(exception2, Exception2.class);
        assertFalse(result);
        result = Utils.matchExceptionCause(exception3, Exception1.class);
        assertTrue(result);
    }

    /**
     *
     */
    @Test
    public void unTest() {
        String replaceAll = "sur \"\" plusieurs\"\"\n".replaceAll("\"\"", "@@@@@");
        final String name = "9,3;4,5;2,3;\n-9,3;-4,5;-2,3;\n9,3;\"text4,5\";\"text2,3\";\n\"9,3text\";4,5;\"2,3text\";\n\"text9,3\";\"4,5text\";2,3;\n1;2;3;\n\"a\";\"23/01/14\";\"à  â ç è é ê î'ô ù\\\"\"û\";\n;\"9,2,3,5,4,4\";\"9,2,3,5,4,5\";\n\"un\ntexte\nsur \"\" plusieurs\"\"\nLignes.\";\"10-12\";\"25/12/65\";\"12-2015\"\n\"Le 25-12-1965\";\"En 12-2015\";;".replaceAll("\"\"", "@@@");
        String[][] parse = CSVParser.parse(name, "\"", "@@", "!");
        Stream.of(parse).forEach(u -> Stream.of(u).forEach(System.out::println));
    }

    /**
     * Test of sanitizeData method, of class Utils.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testSanitizeData() throws Exception {
        System.out.println("sanitizeData");
        File isoFile = new File(ISO_FILE);
        File utfFile = new File(UTF_8);
        File emptyLineFile = new File(EMPTY_LINE_FILE);
        File sanitizeFile = new File(SANITIZE_TEXT);
        byte[] isoByteArray = FileUtils.readFileToByteArray(isoFile);
        byte[] utfByteArray = FileUtils.readFileToByteArray(utfFile);
        byte[] emptyLineByteArray = FileUtils.readFileToByteArray(emptyLineFile);
        byte[] sanitizeByteArray = FileUtils.readFileToByteArray(sanitizeFile);
        byte[] returnData = Utils.sanitizeData(isoByteArray, localizationManager);
        assertArrayEquals(sanitizeByteArray, returnData);
        returnData = Utils.sanitizeData(utfByteArray, localizationManager);
        assertArrayEquals(sanitizeByteArray, returnData);
        assertArrayEquals(returnData, Utils.sanitizeData(returnData, localizationManager));
        when(localizationManager.getMessage(Utils.BUNDLE_SOURCE_PATH, Utils.MSG_BAD_CSV)).thenReturn("errorMessage");
        try {
            Utils.sanitizeData(emptyLineByteArray, localizationManager);
            fail("no exception thrown");
        } catch (BusinessException ex) {
        }
    }


    /**
     * Test of sanitizeDates method, of class Utils.
     */
    @Test
    public void testSanitizeDates() {
        System.out.println("sanitizeDates");
        assertEquals("20/02/1954", Utils.sanitizeDates("20-02-1954"));
        assertEquals("20/02/2054", Utils.sanitizeDates("20-02-2054"));
        assertEquals("20/02/54", Utils.sanitizeDates("20-02-54"));
        assertEquals("02/2054", Utils.sanitizeDates("02-2054"));
        assertEquals("10-20", Utils.sanitizeDates("10-20"));
        assertEquals("54-12-25", Utils.sanitizeDates("54-12-25"));
    }

    /**
     * Test of sanitizeNumbers method, of class Utils.
     */
    @Test
    public void testSanitizeNumbers() {
        System.out.println("sanitizeNumbers");
        assertEquals("54.4", Utils.sanitizeNumbers("54,4"));
        assertEquals("-54.4", Utils.sanitizeNumbers("-54,4"));
        assertEquals("0.1254", Utils.sanitizeNumbers("0,1254"));
        assertEquals("20-02-1654", Utils.sanitizeNumbers("20-02-1654"));
        assertEquals("10-20", Utils.sanitizeNumbers("10-20"));
    }

    /**
     * Test of sanitizeQuotes method, of class Utils.
     */
    @Test
    public void testSanitizeQuotes() {
        System.out.println("sanitizeQuotes");
        assertEquals("\"\";9,3;4,5;2,3;\"\";\"\";", Utils.sanitizeQuotes("\"\";9,3;4,5;2,3;\"\";\"\";"));
    }

    class Exception1 extends Exception {

    }

    class Exception2 extends Exception {

    }

    class Exception3 extends Exception1 {

    }
}
