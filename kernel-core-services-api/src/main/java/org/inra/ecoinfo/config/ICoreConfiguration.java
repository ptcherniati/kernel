package org.inra.ecoinfo.config;

import java.io.Serializable;
import java.util.Map;

/**
 * The Interface ICoreConfiguration.
 */
public interface ICoreConfiguration extends Serializable {

    /**
     * Gets the String id.
     *
     * @return the String id
     */
    String getId();

    /**
     * Gets the Map<String,String> internationalized names.
     *
     * @return the Map<String,String> internationalized names
     */
    Map<String, String> getInternationalizedNames();

    /**
     * Gets the String mail admin.
     *
     * @return the String mail admin
     */
    String getMailAdmin();
    
    /**
     * Gets the String Anonymous Login.
     *
     * @return the String Anonymous Login
     */
    String getAnonymousLogin();

    /**
     * Gets the String mail host.
     *
     * @return the String mail host
     */
    String getMailHost();

    /**
     * Gets the String repository uri.
     *
     * @return the String repository uri
     */
    String getRepositoryURI();

    /**
     * Gets the String site separator for file names.
     *
     * @return the String site separator for file names
     */
    String getSiteSeparatorForFileNames();

    /**
     * Sets the void id.
     *
     * @param id the String id
     */
    void setId(String id);

    /**
     * Sets the internationalized names.
     *
     * @param internationalizedNames the Map<String,String> internationalized
     * names
     */
    void setInternationalizedNames(Map<String, String> internationalizedNames);

    /**
     * Sets the void mail admin.
     *
     * @param mailAdmin the String mail admin
     */
    void setMailAdmin(String mailAdmin);

    /**
     * Sets the void mail host.
     *
     * @param mailHost the String mail host
     */
    void setMailHost(String mailHost);

    /**
     * Sets the void repository uri.
     *
     * @param repositoryURI the String repository uri
     */
    void setRepositoryURI(String repositoryURI);

    /**
     * Sets the void site separator for file names.
     *
     * @param siteSeparatorForFileNames the String site separator for file names
     */
    void setSiteSeparatorForFileNames(String siteSeparatorForFileNames);
}
