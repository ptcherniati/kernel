package org.inra.ecoinfo.config;

import java.util.List;

/**
 * The Interface ILocalisationConfiguration.
 */
public interface ILocalisationConfiguration {

    /**
     * Adds the language.
     *
     * @param language the String language
     */
    void addLanguage(String language);

    /**
     * Gets the String default localisation.
     *
     * @return the String default localisation
     */
    String getDefaultLocalisation();

    /**
     * Gets the List<String> localisations.
     *
     * @return the List<String> localisations
     */
    List<String> getLocalisations();

    /**
     * Sets the void default localisation.
     *
     * @param defaultLocalisations the String default localisations
     */
    void setDefaultLocalisation(String defaultLocalisations);
}
