/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.jobs;

import java.time.LocalDateTime;
import org.inra.ecoinfo.mga.business.IUser;

/**
 *
 * @author ptcherniati
 */
public interface IStatusBar {

    /**
     *
     * @return
     */
    Integer getProgress();

    /**
     *
     * @param progress
     */
    void setProgress(Integer progress);

    /**
     *
     * @return
     */
    LocalDateTime getDate();

    /**
     *
     * @return
     */
    String getGroupName();

    /**
     *
     * @return
     */
    IUser getUser();

    /**
     *
     * @param date
     */
    void setDate(LocalDateTime date);

    /**
     *
     * @param groupName
     */
    void setGroupName(String groupName);

    /**
     *
     * @param user
     */
    void setUser(IUser user);
}
