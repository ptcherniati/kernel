package org.inra.ecoinfo.identification.entity;

import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import org.hibernate.annotations.CreationTimestamp;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(indexes = {
    @Index(name = "rightrequest_utilisateur_idx", columnList = RightRequest.USER_FIELD_NAME)})
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "rightRequestCDC")
public class RightRequest implements IRightRequest {

    /**
     *
     */
    public static final String USER_FIELD_NAME = "usr_id";
    /**
     * The id @link(Long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    protected Long id;

    /**
     *
     */
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = USER_FIELD_NAME, referencedColumnName = Utilisateur.UTILISATEUR_NAME_ID, nullable = false)
    protected Utilisateur user;

    /**
     *
     */
    @Size(min = 1)
    @NotEmpty
    @Column(columnDefinition = "TEXT",nullable = false)
    protected String request;

    /**
     *
     */
    @CreationTimestamp
    protected LocalDateTime createDate;

    /**
     *
     */
    @Column
    protected String motivation;

    /**
     *
     */
    @Column
    protected String reference;

    /**
     *
     */
    @Column
    protected String infos;

    /**
     *
     */
    @Column
    protected Boolean status = false;

    /**
     *
     */
    @Column
    protected Boolean concluded = false;

    /**
     *
     */
    @Column
    protected String answer;

    /**
     *
     */
    public RightRequest() {
        super();
    }

    /**
     *
     * @return
     */
    @Override
    public Utilisateur getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(Utilisateur user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    public String getRequest() {
        return request;
    }

    /**
     *
     * @param request
     */
    public void setRequest(String request) {
        this.request = request;
    }

    /**
     *
     * @return
     */
    public String getMotivation() {
        return motivation;
    }

    /**
     *
     * @param motivation
     */
    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    /**
     *
     * @return
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public Boolean getConcluded() {
        return concluded;
    }

    /**
     *
     * @param concluded
     */
    public void setConcluded(Boolean concluded) {
        this.concluded = concluded;
    }

    /**
     *
     * @return
     */
    public String getAnswer() {
        return answer;
    }

    /**
     *
     * @param answer
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    /**
     *
     * @return
     */
    public String getReference() {
        return reference;
    }

    /**
     *
     * @param reference
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     *
     * @return
     */
    public String getInfos() {
        return infos;
    }

    /**
     *
     * @param infos
     */
    public void setInfos(String infos) {
        this.infos = infos;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    /**
     *
     * @return
     */
    public boolean isValidated() {
        return concluded;
    }

    /**
     *
     * @param validated
     */
    public void setValidated(boolean validated) {
        this.concluded = validated;
    }
}
