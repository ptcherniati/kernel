package org.inra.ecoinfo.identification.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.WhichTree;

/**
 * The Class Utilisateur.
 */
@Entity(name = "Utilisateur")
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"login"}),
    @UniqueConstraint(columnNames = {"email"})
})
@XmlRootElement
public class Utilisateur implements IUser, Serializable {

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = "utilisateur";

    /**
     *
     */
    public static final String GROUPS_FORMULA = "g.groupName FROM " + Group.tableName + " g, "
            + GroupUtilisateur.TABLE_NAME + " gu gu WHERE g." + Group.JPA_FIELD_GROUP_NAME
            + " = 'public' or (gu." + GroupUtilisateur.GROUP_FIELD_NAME + " = g and gu."
            + GroupUtilisateur.USER_FIELD_NAME + " = login";
    /**
     * The Constant UTILISATEUR_NAME_ID.
     */
    public static final String UTILISATEUR_NAME_ID = "id";

    /**
     *
     */
    public static final String JPA_NAME_UTILISATEUR_FK = "utilisateur_id";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -7_409_043_304_071_348_847L;

    @Transient
    Group _ownGroup;
    /**
     * The Long id.
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected Long id;

    /**
     * The String email.
     */
    @Column(nullable = false, unique = true)
    private String email;
    /**
     * The Boolean is root.
     */
    @Column(nullable = true)
    private Boolean isRoot = false;
    /**
     * The String language.
     */
    @Column(name = "language")
    private String language = Localization.getDefaultLocalisation();
    /**
     * The String login.
     */
    @Column(nullable = false, unique = true)
    @NaturalId
    private String login;
    /**
     * The String nom.
     */
    @Column(nullable = false)
    private String nom;
    /**
     * The String password.
     */
    @Column(nullable = false)
    private String password;
    /**
     * The String prenom.
     */
    @Column(nullable = false)
    private String prenom;

    @Column(nullable = false)
    private Boolean active = false;

    /**
     * The emploi @link(String).
     */
    @Column
    private String emploi;

    /**
     * The poste @link(String).
     */
    @Column
    private String poste;

    @Transient
    Set<Group> groups = new HashSet();

    @OneToMany(targetEntity = GroupUtilisateur.class, mappedBy = "user")
    @Column(insertable = false, updatable = false)
    Set<GroupUtilisateur> groupsUsers = new HashSet();

    /**
     * Instantiates a new Utilisateur utilisateur.
     */
    public Utilisateur() {
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Utilisateur{" + "login=" + login + '}';
    }

    /**
     *
     */
    @PostLoad
    public void initGroups() {
        getAllGroups().addAll(
                getGroupsUsers().stream()
                        .map(gu -> gu.getGroup())
                        .collect(Collectors.toSet())
        );
    }

    /**
     *
     * @return
     */
    @XmlTransient
    public Set<GroupUtilisateur> getGroupsUsers() {
        return groupsUsers;
    }

    /**
     * Gets the String email.
     *
     * @return the String email
     */
    @Override
    public String getEmail() {
        return email;
    }

    /**
     * Sets the void email.
     *
     * @param email the String email
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Gets the Long id.
     *
     * @return the Long id
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Sets the void id.
     *
     * @param id the Long id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the Boolean checks if is root.
     *
     * @return the Boolean checks if is root
     */
    @Override
    public Boolean getIsRoot() {
        return isRoot;
    }

    /**
     * Sets the void checks if is root.
     *
     * @param isRoot the Boolean is root
     */
    public void setIsRoot(final Boolean isRoot) {
        this.isRoot = isRoot;
    }

    /**
     * Gets the String language.
     *
     * @return the String language
     */
    @Override
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the void language.
     *
     * @param language the String language
     */
    public void setLanguage(final String language) {
        this.language = language;
    }

    /**
     * Gets the String login.
     *
     * @return the String login
     */
    @Override
    public String getLogin() {
        return login;
    }

    /**
     * Sets the void login.
     *
     * @param login the String login
     */
    public void setLogin(final String login) {
        this.login = login;
    }

    /**
     * Gets the String nom.
     *
     * @return the String nom
     */
    @Override
    public String getNom() {
        return nom;
    }

    /**
     * Sets the void nom.
     *
     * @param nom the String nom
     */
    public void setNom(final String nom) {
        this.nom = nom;
    }

    /**
     * Gets the String password.
     *
     * @return the String password
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * Sets the void password.
     *
     * @param password the String password
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Gets the String prenom.
     *
     * @return the String prenom
     */
    @Override
    public String getPrenom() {
        return prenom;
    }

    /**
     * Sets the void prenom.
     *
     * @param prenom the String prenom
     */
    public void setPrenom(final String prenom) {
        this.prenom = prenom;
    }

    /**
     *
     * @return
     */
    public String getEmploi() {
        return emploi;
    }

    /**
     *
     * @param emploi
     */
    public void setEmploi(String emploi) {
        this.emploi = emploi;
    }

    /**
     *
     * @return
     */
    public String getPoste() {
        return poste;
    }

    /**
     *
     * @param poste
     */
    public void setPoste(String poste) {
        this.poste = poste;
    }

    /**
     *
     * @return
     */
    public Boolean getActive() {
        return active;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.login);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Utilisateur other = (Utilisateur) obj;
        return Objects.equals(this.login, other.login);
    }

    /**
     *
     * @param active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     *
     * @return
     */
    @Override
    public String getGroupName() {
        return login;
    }

    /**
     *
     * @param groups
     */
    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    /**
     *
     * @return
     */
    @Override
    public Set<Group> getAllGroups() {
        return groups;
    }

    /**
     *
     * @param groups
     */
    public void setAllGroups(List<Group> groups) {
        this.groups = groups.stream().collect(Collectors.toSet());
    }

    /**
     *
     * @param whichTree
     * @return
     */
    @Override
    public Group getOwnGroup(WhichTree whichTree) {
        if (_ownGroup == null) {
            _ownGroup = getAllGroups().stream().filter(u -> u.getGroupName().equals(login) && u.getWhichTree().equals(whichTree)).findFirst().orElse(_ownGroup);
        }
        return _ownGroup;
    }

    /**
     *
     * @param group
     * @return
     */
    @Override
    public boolean testIsMutableGroup(Group group) {
        if ("public".equals(group.getGroupName())) {
            return false;
        }
        if (group.getGroupName().equals(login)) {
            return !groupsUsers.stream().anyMatch(gu -> gu.group.equals(group));
        }
        return true;
    }
}
