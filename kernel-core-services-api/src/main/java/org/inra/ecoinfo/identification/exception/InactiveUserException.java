package org.inra.ecoinfo.identification.exception;

import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class UnknownUserException.
 */
public class InactiveUserException extends BusinessException {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    Utilisateur user;

    /**
     * Instantiates a new UnknownUserException unknown user exception.
     * @param utlstr
     * @param user
     * @throws org.inra.ecoinfo.identification.exception.UnknownUserException
     */
    public InactiveUserException(Utilisateur user) throws UnknownUserException {
        super();
        if(user==null){
            throw new UnknownUserException();
        }
        this.user = user;
    }

    /**
     * Instantiates a new UnknownUserException unknown user exception.
     *
     * @param arg0 the String arg0
     * @param utlstr
     * @param user
     * @throws org.inra.ecoinfo.identification.exception.UnknownUserException
     */
    public InactiveUserException(final String arg0, Utilisateur user) throws UnknownUserException {
        super(arg0);
        if(user==null){
            throw new UnknownUserException(arg0);
        }
        this.user = user;
    }

    /**
     * Instantiates a new UnknownUserException unknown user exception.
     *
     * @param arg0 the String arg0
     * @param arg1 the Throwable arg1
     * @param utlstr
     * @param user
     * @throws org.inra.ecoinfo.identification.exception.UnknownUserException
     */
    public InactiveUserException(final String arg0, final Throwable arg1, Utilisateur user) throws UnknownUserException {
        super(arg0, arg1);
        if(user==null){
            throw new UnknownUserException(arg0, arg1);
        }
        this.user = user;
    }

    /**
     * Instantiates a new UnknownUserException unknown user exception.
     *
     * @param arg0 the Throwable arg0
     * @param utlstr
     * @param user
     * @throws org.inra.ecoinfo.identification.exception.UnknownUserException
     */
    public InactiveUserException(final Throwable arg0, Utilisateur user) throws UnknownUserException {
        super(arg0);
        if(user==null){
            throw new UnknownUserException(arg0);
        }
        this.user = user;
    }

    /**
     *
     * @return
     */
    public Utilisateur getUser() {
        return user;
    }
}
