/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.identification.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.mga.business.composite.groups.Group;

/**
 *
 * @author tcherniatinsky
 */
@Entity
@Table(name = GroupUtilisateur.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        GroupUtilisateur.USER_FIELD_NAME, GroupUtilisateur.GROUP_FIELD_NAME }))
public class GroupUtilisateur implements Serializable {

    private static final long serialVersionUID = -8_312_600_573_880_526_639L;

    /**
     *
     */
    public static final String TABLE_NAME = "Group_Utilisateur";

    /**
     *
     */
    public static final String USER_FIELD_NAME = "usr_id";

    /**
     *
     */
    public static final String GROUP_FIELD_NAME = "group_id";

    /**
     *
     */
    @Id
    @ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, optional = false)
    @JoinColumn(name = USER_FIELD_NAME, referencedColumnName = Utilisateur.UTILISATEUR_NAME_ID, nullable = false)
    protected Utilisateur user;

    /**
     *
     */
    @Id
    @ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, optional = false)
    @JoinColumn(name = GROUP_FIELD_NAME, referencedColumnName = Group.ID_JPA, nullable = false)
    protected Group group;

    /**
     *
     * @param utlstr
     * @param user
     * @param group
     */
    public GroupUtilisateur(Utilisateur user, Group group) {
        this.user = user;
        this.group = group;
    }

    /**
     *
     */
    public GroupUtilisateur() {
    }

    /**
     *
     * @return
     */
    public Utilisateur getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(Utilisateur user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    public Group getGroup() {
        return group;
    }

    /**
     *
     * @param group
     */
    public void setGroup(Group group) {
        this.group = group;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.user);
        hash = 59 * hash + Objects.hashCode(this.group);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GroupUtilisateur other = (GroupUtilisateur) obj;
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        return Objects.equals(this.group, other.group);
    }

}
