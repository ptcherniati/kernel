package org.inra.ecoinfo.identification;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IUsersManager.
 */
public interface IUsersManager extends Serializable {

    /**
     * Adds the user profile.
     *
     * @param utilisateur the Utilisateur utilisateur
     * @param baseUrl
     * @throws BusinessException the business exception
     */
    void addUserProfile(Utilisateur utilisateur, String baseUrl) throws BusinessException;

    /**
     *
     * @param utilisateur
     * @param baseUrl
     * @param otherRecipients
     * @throws BusinessException
     */
    void addUserProfile(final Utilisateur utilisateur, final String baseUrl, final List<Utilisateur> otherRecipients) throws BusinessException;

    /**
     * Activate the user profile.
     *
     * @param login the String login
     * @throws BusinessException the business exception
     */
    void activateUser(String login) throws BusinessException;

    /**
     * Check password.
     *
     * @param login the String login
     * @param password the String password
     * @return the Utilisateur utilisateur
     * @throws BusinessException the business exception
     */
    Utilisateur checkPassword(String login, String password) throws BusinessException;

    /**
     * Delete user profile.
     *
     * @param id the long id
     * @return 
     * @throws BusinessException the business exception
     */
    InfosReport deleteUserProfile(Utilisateur utilisateur) throws BusinessException;

    /**
     * Gets the Utilisateur utilisateur by login.
     *
     * @param login the String login
     * @return the Utilisateur utilisateur by login
     */
    Optional<Utilisateur> getUtilisateurByLogin(String login);

    /**
     *
     * @param email
     * @return
     */
    Utilisateur getUtilisateurByEmail(String email);

    /**
     * Initialise.
     *
     * @param utilisateur the Utilisateur utilisateur
     * @throws BusinessException the business exception
     */
    void initialise(Utilisateur utilisateur) throws BusinessException;

    /**
     * Reset password.
     *
     * @param utilisateur the Utilisateur utilisateur
     * @param baseUrl the String base url
     * @param locale the Locale locale
     * @throws BusinessException the business exception
     */
    void resetPassword(Utilisateur utilisateur, String baseUrl, Locale locale) throws BusinessException;

    /**
     * Retrieve all users.
     *
     * @return the List<Utilisateur> list
     */
    List<Utilisateur> retrieveAllUsers();

    /**
     * Update user profile.
     *
     * @param utilisateur the Utilisateur utilisateur
     * @throws BusinessException the business exception
     */
    void updateUserProfile(Utilisateur utilisateur) throws BusinessException;

    /**
     *
     * @param utilisateur
     * @param baseUrl
     * @param resourceBundle
     * @param otherRecipients
     */
    void sendValidationEmail(final Utilisateur utilisateur, final String baseUrl, final ResourceBundle resourceBundle, List<Utilisateur> otherRecipients);

    /**
     *
     * @return
     */
    String getMailAdmin();
}
