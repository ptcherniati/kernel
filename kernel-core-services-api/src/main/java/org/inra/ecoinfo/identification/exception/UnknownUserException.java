package org.inra.ecoinfo.identification.exception;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class UnknownUserException.
 */
public class UnknownUserException extends BusinessException {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new UnknownUserException unknown user exception.
     */
    public UnknownUserException() {
        super();
    }

    /**
     * Instantiates a new UnknownUserException unknown user exception.
     *
     * @param arg0 the String arg0
     */
    public UnknownUserException(final String arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new UnknownUserException unknown user exception.
     *
     * @param arg0 the String arg0
     * @param arg1 the Throwable arg1
     */
    public UnknownUserException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new UnknownUserException unknown user exception.
     *
     * @param arg0 the Throwable arg0
     */
    public UnknownUserException(final Throwable arg0) {
        super(arg0);
    }
}
