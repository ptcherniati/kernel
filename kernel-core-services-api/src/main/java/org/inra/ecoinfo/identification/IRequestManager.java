/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.identification;

import java.util.List;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 * @param <R>
 */
public interface IRequestManager<R> {

    /**
     *
     * @param request
     * @throws BusinessException
     */
    void addNewRequest(R request) throws BusinessException;

    /**
     *
     * @return
     */
    List<R> getRestrictedRequests();

    /**
     *
     * @param request
     */
    void saveRequest(R request);

    /**
     *
     * @param request
     */
    void deleteRequest(R request);
    
    /**
     *
     * @param admin
     * @param utilisateur
     * @param rightRequest
     * @return
     */
    String getMailMessageForRequest(Utilisateur admin, final Utilisateur utilisateur, R rightRequest);
    
    
}
