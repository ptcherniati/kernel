package org.inra.ecoinfo.identification.exception;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class BadPasswordException.
 */
public class BadPasswordException extends BusinessException {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new BadPasswordException bad password exception.
     */
    public BadPasswordException() {
        super();
    }

    /**
     * Instantiates a new BadPasswordException bad password exception.
     *
     * @param string
     * @param arg0 the String arg0
     */
    public BadPasswordException(final String arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new BadPasswordException bad password exception.
     *
     * @param arg0 the String arg0
     * @param thrwbl
     * @param arg1 the Throwable arg1
     */
    public BadPasswordException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new BadPasswordException bad password exception.
     *
     * @param thrwbl
     * @param arg0 the Throwable arg0
     */
    public BadPasswordException(final Throwable arg0) {
        super(arg0);
    }
}
