package org.inra.ecoinfo.identification;

import java.io.Serializable;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.exceptions.DeleteUserException;

/**
 * The Interface IUsersManager.
 */
public interface IUsersDeletion extends Serializable {

    /**   
     * @param utilisateur
     * @return InfosReport
     * @throws DeleteUserException the DeleteUserException exception
     */
    public InfosReport deleteDependancesUserProfile(Utilisateur utilisateur) throws DeleteUserException;

}
