package org.inra.ecoinfo.localization.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index; 
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.NaturalId;

/**
 * The Class Localization.
 */
@Entity(name = "Localisation")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"localization", "entite", "colonne", "defaultString"}),
       indexes = {
           @Index(name = "loc_loc_ent_col_idx", columnList = "localization,entite,colonne"),
           @Index(name = "loc_ent_col_def_idx", columnList = "entite,colonne,defaultString"),
           @Index(name = "loc_loc_ent_col_def_idx", columnList = "localization,entite,colonne,defaultString")})
public class Localization implements Serializable {

    /**
     * The List<String> localisations.
     */
    private static List<String> localisations = new ArrayList();
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The String default localisation.
     */
    protected static String defaultLocalisation = Locale.FRANCE.getLanguage();

    /**
     * Adds the language.
     *
     * @param language the String language
     */
    public static void addLanguage(final String language) {
        if (!Localization.localisations.contains(language)) {
            Localization.localisations.add(language);
        }
    }

    /**
     * Gets the String default localisation.
     *
     * @return the String default localisation
     */
    public static String getDefaultLocalisation() {
        return Localization.defaultLocalisation;
    }

    /**
     * Sets the void default localisation.
     *
     * @param defaultLocalisation the String default localisation
     */
    public static void setDefaultLocalisation(final String defaultLocalisation) {
        Localization.defaultLocalisation = defaultLocalisation;
    }

    /**
     * Gets the List<String> localisations.
     *
     * @return the List<String> localisations
     */
    public static List<String> getLocalisations() {
        return Localization.localisations;
    }

    /**
     * Sets the void localisations.
     *
     * @param localisations the List<String> localisations
     */
    public static void setLocalisations(final List<String> localisations) {
        Localization.localisations = localisations;
    }
    /**
     * The int id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    /**
     * The String entite.
     */
    @NaturalId
    private String entite;

    /**
     * The String colonne.
     */
    @NaturalId
    private String colonne;
    /**
     * The String _localization.
     */
    @Column(name = "localization")
    @NaturalId
    private String _localization = Localization.defaultLocalisation;
    /**
     * The String default string. /** The String default string.
     */

    @Column(columnDefinition = "TEXT")
    @NaturalId
    private String defaultString = "";
    /**
     * The String locale string.
     */
    @Column(columnDefinition = "TEXT")
    private String localeString;

    /**
     * Instantiates a new Localization _localization.
     */
    public Localization() {
        super();
    }

    /**
     * Instantiates a new Localization _localization.
     *
     * @param localization the String _localization
     * @param entite the String entite
     * @param colonne the String colonne
     * @param defaultString the String default string
     * @param localeString the String locale string
     */
    public Localization(final String localization, final String entite, final String colonne, final String defaultString, final String localeString) {
        super();
        this._localization = localization;
        this.entite = entite;
        this.colonne = colonne.replace(String.format("_%s$", localization), "");
        this.defaultString = defaultString;
        this.localeString = localeString;
    }

    /**
     * Gets the String colonne.
     *
     * @return the String colonne
     */
    public String getColonne() {
        return colonne;
    }

    /**
     * Sets the void colonne.
     *
     * @param colonne the String colonne
     */
    public void setColonne(final String colonne) {
        this.colonne = colonne;
    }

    /**
     * Gets the String default string.
     *
     * @return the String default string
     */
    public String getDefaultString() {
        return defaultString;
    }

    /**
     * Sets the void default string.
     *
     * @param defaultString the String default string
     */
    public void setDefaultString(final String defaultString) {
        this.defaultString = defaultString;
    }

    /**
     * Gets the String entite.
     *
     * @return the String entite
     */
    public String getEntite() {
        return entite;
    }

    /**
     * Sets the void entite.
     *
     * @param entite the String entite
     */
    public void setEntite(final String entite) {
        this.entite = entite;
    }

    /**
     * Gets the int id.
     *
     * @return the int id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the String locale string.
     *
     * @return the String locale string
     */
    public String getLocaleString() {
        return localeString;
    }

    /**
     * Sets the void locale string.
     *
     * @param localeString the String locale string
     */
    public void setLocaleString(final String localeString) {
        this.localeString = localeString;
    }

    /**
     * Gets the String _localization.
     *
     * @return the String _localization
     */
    public String getLocalization() {
        return _localization;
    }

    /**
     * Sets the void _localization.
     *
     * @param localization the String _localization
     */
    public void setLocalization(final String localization) {
        this._localization = localization;
    }
}
