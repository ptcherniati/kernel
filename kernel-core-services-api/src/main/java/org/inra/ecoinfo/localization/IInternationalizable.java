package org.inra.ecoinfo.localization;

/**
 * The Interface IInternationalizable.
 */
public interface IInternationalizable {

    /**
     * Sets the void localization manager.
     *
     * @param localizationManager the ILocalizationManager localization manager
     */
    void setLocalizationManager(ILocalizationManager localizationManager);
}
