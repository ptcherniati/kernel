package org.inra.ecoinfo.localization.entity;

import java.io.Serializable;
import java.util.Locale;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.inra.ecoinfo.identification.entity.Utilisateur;

/**
 * The Class UserLocale.
 */
@Entity
@Table(name = Utilisateur.NAME_ENTITY_JPA)
public class UserLocale implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The String language.
     */
    @Column(name = "language")
    private String language = Localization.getDefaultLocalisation();
    /**
     * The Locale locale.
     */
    @Transient
    private Locale locale = new Locale(language);
    /**
     * The Long id.
     */
    @Id
    protected Long id;

    /**
     * Gets the Long id.
     *
     * @return the Long id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the void id.
     *
     * @param id the Long id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the String language.
     *
     * @return the String language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the void language.
     *
     * @param localeString the String locale string
     */
    public void setLanguage(final String localeString) {
        this.language = Localization.getDefaultLocalisation();
        for (final String localisation : Localization.getLocalisations()) {
            if (localisation.equals(localeString)) {
                this.language = localeString;
                break;
            }
        }
        this.locale = new Locale(language);
    }

    /**
     * Gets the Locale locale.
     *
     * @return the Locale locale
     */
    public Locale getLocale() {
        return locale;
    }
}
