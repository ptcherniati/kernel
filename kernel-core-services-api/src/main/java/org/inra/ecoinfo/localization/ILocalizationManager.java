package org.inra.ecoinfo.localization;

import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.entity.UserLocale;
import org.inra.ecoinfo.mga.managedbean.ILocalizationEntitiesManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface ILocalizationManager.
 */
public interface ILocalizationManager extends ILocalizationEntitiesManager {

    /**
     * Change locale.
     *
     * @param defaultSessionLocale the Locale default session locale
     * @return the Locale locale
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    Locale changeLocale(Locale defaultSessionLocale) throws BusinessException;

    /**
     * Delete unused properties.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    void deleteUnusedProperties() throws BusinessException;

    /**
     * Gets the Localization by n key.
     *
     * @param localizationString the String localization string
     * @param entity the String entity
     * @param colonne the String colonne
     * @param defaultString the String default string
     * @return the Localization by n key
     */
    Optional<Localization> getByNKey(String localizationString, String entity, String colonne, String defaultString);

    /**
     * Gets the String message.
     *
     * @param bundleSourcePath the String bundle source path
     * @param key the String key
     * @return the String message
     */
    String getMessage(String bundleSourcePath, String key);

    /**
     * Gets the UserLocale user locale.
     *
     * @return the UserLocale user locale
     */
    UserLocale getUserLocale();

    /**
     * Inits the locale.
     *
     * @param defaultSessionLocale the Locale default session locale
     * @return the Locale locale
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    Locale initLocale(Locale defaultSessionLocale) throws BusinessException;

    /**
     * New properties.
     *
     * @param entity the String entity
     * @param column the String column
     * @return the Properties properties
     */
    Properties newProperties(String entity, String column);

    /**
     * New properties.
     *
     * @param entity the String entity
     * @param column the String column
     * @param locale the Locale locale
     * @return the Properties properties
     */
    Properties newProperties(String entity, String column, Locale locale);

    /**
     * Save localization.
     *
     * @param localization the Localization localization
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    void saveLocalization(Localization localization) throws BusinessException;

    /**
     * Save user locale.
     *
     * @param userLocale the UserLocale user locale
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    void saveUserLocale(UserLocale userLocale) throws BusinessException;

    /**
     * Sets the void adds the language.
     *
     * @param language the String language
     */
    void setAddLanguage(String language);

    /**
     * Sets the void default language.
     *
     * @param defaultLocalization the String default localization
     */
    void setDefaultLanguage(String defaultLocalization);

    /**
     *
     * @param localizationString
     * @param entity
     * @param colonne
     * @param defaultString
     * @param localeString
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    Localization createorUpdateLocalization(final String localizationString, final String entity, final String colonne, final String defaultString, final String localeString) throws BusinessException;
}
