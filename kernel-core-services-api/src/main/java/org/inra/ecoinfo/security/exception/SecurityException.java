package org.inra.ecoinfo.security.exception;

/**
 * The Class SecurityException.
 */
public class SecurityException extends Exception {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new SecurityException security exception.
     */
    public SecurityException() {
        super();
    }

    /**
     * Instantiates a new SecurityException security exception.
     * 
     * @param arg0
     *            the String arg0
     */
    public SecurityException(final String arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new SecurityException security exception.
     * 
     * @param arg0
     *            the String arg0
     * @param arg1
     *            the Throwable arg1
     */
    public SecurityException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new SecurityException security exception.
     * 
     * @param arg0
     *            the Throwable arg0
     */
    public SecurityException(final Throwable arg0) {
        super(arg0);
    }
}
