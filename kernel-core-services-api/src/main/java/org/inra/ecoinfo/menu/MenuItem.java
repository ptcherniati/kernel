package org.inra.ecoinfo.menu;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class MenuItem.
 */
public class MenuItem implements Serializable, Cloneable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The String action.
     */
    private String action = "";
    /**
     * The List<MenuItem> children.
     */
    private List<MenuItem> children = new LinkedList<>();
    /**
     * The String comment.
     */
    private String comment = "";
    /**
     * The String direct action.
     */
    private String directAction = "";
    /**
     * The Boolean disabled.
     */
    private Boolean disabled = false;
    /**
     * The String icon.
     */
    private String icon;
    
    private Boolean root = false;
    /**
     * The String label.
     */
    private String label = "";
    /**
     * The Menu menu parent.
     */
    private Menu menuParent;
    /**
     * The String permissions.
     */
    private String permissions;
    /**
     * The String resources.
     */
    private String resources = "*";
    /**
     * The String codeConf.
     */
    private Integer codeConfiguration;

    /**
     * Instantiates a new MenuItem menu item.
     */
    public MenuItem() {
        super();
    }

    /**
     *
     * @return
     */
    public Integer getCodeConf() {
        return codeConfiguration;
    }

    /**
     *
     * @param codeConfiguration
     * @param codeConf
     */
    public void setCodeConf(Integer codeConfiguration) {
        this.codeConfiguration = codeConfiguration;
    }

    /**
     * Adds the menu item.
     *
     * @param menuItem the MenuItem menu item
     */
    public void addMenuItem(final MenuItem menuItem) {
        children.add(menuItem);
    }

    /**
     * Builds the permissions array.
     *
     * @return the String[] string[]
     */
    public String buildPermissionsArray() {
        if (permissions == null) {
            return PatternConfigurator.ASTERIX;
        }
        final String permissionsEpurated = Utils.createCodeFromString(permissions.replaceAll(" ", "").trim());
        return permissionsEpurated;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#clone()
     */

    /**
     *
     * @return
     * @throws CloneNotSupportedException
     */

    @Override
    public MenuItem clone() throws CloneNotSupportedException {
        super.clone();
        final MenuItem menuItem = new MenuItem();
        menuItem.setComment(comment);
        menuItem.setResources(resources);
        menuItem.setLabel(label);
        menuItem.setIcon(icon);
        menuItem.setPermissions(permissions);
        menuItem.setDisabled(disabled);
        menuItem.setAction(action);
        menuItem.setDirectAction(directAction);
        menuItem.setCodeConf(codeConfiguration);
        menuItem.setRoot(root);
        final List<MenuItem> menuItems = new LinkedList<>();
        for (final MenuItem mnt : children) {
            if (mnt != null) {
                menuItem.children.add(mnt.clone());
            }
        }
        return menuItem;
    }

    /**
     * Gets the String action.
     *
     * @return the String action
     */
    public String getAction() {
        return action;
    }

    /**
     * Gets the List<MenuItem> children.
     *
     * @return the List<MenuItem> children
     */
    public List<MenuItem> getChildren() {
        return children;
    }

    /**
     * Gets the String comment.
     *
     * @return the String comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Gets the String direct action.
     *
     * @return the String direct action
     */
    public String getDirectAction() {
        return directAction;
    }

    /**
     * Gets the Boolean disabled.
     *
     * @return the Boolean disabled
     */
    public Boolean getDisabled() {
        return disabled;
    }

    /**
     * Gets the String icon.
     *
     * @return the String icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Gets the String label.
     *
     * @return the String label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Gets the Menu menu parent.
     *
     * @return the Menu menu parent
     */
    public Menu getMenuParent() {
        return menuParent;
    }

    /**
     * Gets the String permissions.
     *
     * @return the String permissions
     */
    public String getPermissions() {
        return permissions;
    }

    /**
     * Gets the String resources.
     *
     * @return the String resources
     */
    public String getResources() {
        return resources;
    }

    /**
     * Sets the void action.
     *
     * @param action the String action
     */
    public void setAction(final String action) {
        this.action = action;
    }

    /**
     * Sets the void children.
     *
     * @param children the List<MenuItem> children
     */
    public void setChildren(final List<MenuItem> children) {
        this.children = children;
    }

    /**
     * Sets the void comment.
     *
     * @param comment the String comment
     */
    public void setComment(final String comment) {
        this.comment = comment;
    }

    /**
     * Sets the void direct action.
     *
     * @param directAction the String direct action
     */
    public void setDirectAction(final String directAction) {
        this.directAction = directAction;
    }

    /**
     * Sets the void disabled.
     *
     * @param disabled the Boolean disabled
     */
    public void setDisabled(final Boolean disabled) {
        this.disabled = disabled;
    }

    /**
     * Sets the void icon.
     *
     * @param icon the String icon
     */
    public void setIcon(final String icon) {
        this.icon = icon;
    }

    /**
     * Sets the void label.
     *
     * @param label the String label
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Sets the void menu parent.
     *
     * @param menuParent the Menu menu parent
     */
    public void setMenuParent(final Menu menuParent) {
        this.menuParent = menuParent;
    }

    /**
     * Sets the void permissions.
     *
     * @param permissions the String permissions
     */
    public void setPermissions(final String permissions) {
        if (permissions != null) {
            this.permissions = Utils.createCodeFromString(permissions);
        }
    }

    /**
     * Sets the void resources.
     *
     * @param resources the String resources
     */
    public void setResources(final String resources) {
        if (resources != null) {
            // this.resources = Utils.createCodeFromString(resources);
            this.resources = resources;
        }
    }
    
    /**
     *
     * @return
     */
    public Boolean getRoot() {
        return root;
    }
    
    /**
     *
     * @param root
     */
    public void setRoot(final Boolean root) {
        this.root = root;
    }

}
