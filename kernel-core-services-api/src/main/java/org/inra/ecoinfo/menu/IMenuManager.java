package org.inra.ecoinfo.menu;

import java.io.Serializable;

/**
 * The Interface IMenuManager.
 */
public interface IMenuManager extends Serializable{

    /**
     * Builds the restricted menu.
     *
     * @return the Menu menu
     * @throws CloneNotSupportedException the clone not supported exception
     */
    Menu buildRestrictedMenu() throws CloneNotSupportedException;

    /**
     * Gets the Menu menu.
     *
     * @return the Menu menu
     */
    Menu getMenu();
}
