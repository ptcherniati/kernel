package org.inra.ecoinfo.menu;

/**
 * The Interface IMenuBuilder.
 */
public interface IMenuBuilder {

    /**
     * Builds the.
     *
     * @param menuPath the String menu path
     * @return the Menu menu
     */
    Menu build(String menuPath);
}
