package org.inra.ecoinfo.menu;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


/**
 * The Class Menu.
 */
public class Menu implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    /** The List<MenuItem> children. */
    private List<MenuItem> children = new LinkedList<>();

    /**
     * Adds the menu item.
     * 
     * @param menuItem
     *            the MenuItem menu item
     */
    public void addMenuItem(final MenuItem menuItem) {
        children.add(menuItem);
    }

    /**
     * Gets the List<MenuItem> menu items.
     * 
     * @return the List<MenuItem> menu items
     */
    public List<MenuItem> getMenuItems() {
        return children;
    }

    /**
     * Sets the void menu items.
     * 
     * @param children
     *            the List<MenuItem> children
     */
    public void setMenuItems(final List<MenuItem> children) {
        this.children = children;
    }
}

