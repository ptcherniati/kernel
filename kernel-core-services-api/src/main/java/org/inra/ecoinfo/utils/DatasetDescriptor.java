
package org.inra.ecoinfo.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.localization.entity.Localization;

/**
 * The Class DatasetDescriptor.
 */
public class DatasetDescriptor implements Serializable{

    /**
     * The List<Column> columns.
     */
    private List<Column> columns = new ArrayList();
    /**
     * The String name.
     */
    private String name;
    /**
     * The int undefined column.
     */
    private int undefinedColumn = 0;

    /**
     * Adds the column.
     *
     * @param column the Column column
     */
    public void addColumn(final Column column) {

        if (column.isLocalizable() &&  !CollectionUtils.isEmpty(Localization.getLocalisations())) {
            if(column.isKey()){
                columns.add(new Column(column, "key"));
            }
            Localization.getLocalisations().stream().forEach((localization) -> {
                columns.add(new Column(column, localization));
            });
        } else {
            columns.add(column);
        }
    }

    /**
     * Gets the List<Column> columns.
     *
     * @return the List<Column> columns
     */
    public List<Column> getColumns() {
        return columns;
    }

    /**
     * Sets the void columns.
     *
     * @param columns the List<Column> columns
     */
    public void setColumns(final List<Column> columns) {
        this.columns = columns;
    }

    /**
     * Gets the String name.
     *
     * @return the String name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the void name.
     *
     * @param name the String name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the int undefined column.
     *
     * @return the int undefined column
     */
    public int getUndefinedColumn() {
        return undefinedColumn;
    }

    /**
     * Sets the void undefined column.
     *
     * @param undefinedColumn the String undefined column
     */
    public void setUndefinedColumn(final String undefinedColumn) {
        this.undefinedColumn = Integer.parseInt(undefinedColumn);
    }
}
