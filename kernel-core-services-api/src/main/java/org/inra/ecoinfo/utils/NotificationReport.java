/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 16:37:26
 */
package org.inra.ecoinfo.utils;

import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

/**
 * The Class NotificationReport.
 *
 * @author "Antoine Schellenberger"
 */
public class NotificationReport {

    /**
     * The bads formats report @link(BadsFormatsReport).
     */
    private BadsFormatsReport badsFormatsReport;
    /**
     * The error message @link(String).
     */
    private String errorMessage;
    /**
     * The error name @link(String).
     */
    private String errorName;
    /**
     * The records @link(List<Record>).
     */
    private List<Record> records = new LinkedList<>();

    /**
     * Adds the record.
     *
     * @param record the record
     * @link(Record) the record
     */
    public void addRecord(final Record record) {
        getRecords().add(record);
    }

    /**
     * Gets the bads formats report.
     *
     * @return the bads formats report
     */
    public BadsFormatsReport getBadsFormatsReport() {
        return badsFormatsReport;
    }

    /**
     * Sets the bads formats report.
     *
     * @param badsFormatsReport the new bads formats report
     */
    public void setBadsFormatsReport(final BadsFormatsReport badsFormatsReport) {
        this.badsFormatsReport = badsFormatsReport;
    }

    /**
     * Gets the error message.
     *
     * @return the error message
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the error message.
     *
     * @param errorMessage the new error message
     */
    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Gets the error name.
     *
     * @return the error name
     */
    public String getErrorName() {
        return errorName;
    }

    /**
     * Sets the error name.
     *
     * @param errorName the new error name
     */
    public void setErrorName(final String errorName) {
        this.errorName = errorName;
    }

    /**
     * Gets the records.
     *
     * @return the records
     */
    public List<Record> getRecords() {
        return records;
    }

    /**
     * Sets the records.
     *
     * @param records the new records
     */
    public void setRecords(final List<Record> records) {
        this.records = records;
    }
}
