package org.inra.ecoinfo.utils;

import java.io.PrintStream;
import java.io.Serializable;
import java.time.DateTimeException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAdjusters;
import java.util.Objects;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 * The Class IntervalDate.
 *
 * @author Philippe TCHERNIATINSKY Utilitaire de gestion des intervales de dates
 */
public class IntervalDate implements Comparable<IntervalDate>, Serializable {

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.utils.messages";
    private static final String FIRST_DAY_OF_MOUNTH_BEFORE = "01-%s";
    private static final String FIRST_DAY_OF_MOUNTH_AFTER = "%s-01";
    private static final String DECEMBER_LAST_BEFORE = "31-12-%s";
    private static final String JANUARY_FIRST_BEFORE = "01-01-%s";
    /**
     * The Constant PROPERTY_MSG_BAD_DATES.
     */
    private static final String PROPERTY_MSG_BAD_DATES = "PROPERTY_MSG_BAD_DATES";
    private static final String PROPERTY_MSG_FROM_TO = "PROPERTY_MSG_FROM_TO";
    /**
     * The Constant PROPERTY_MSG_BAD_DATES_FOR_PERIOD.
     */
    private static final String PROPERTY_MSG_BAD_DATES_FOR_PERIOD = "PROPERTY_MSG_BAD_DATES_FOR_PERIOD";
    /**
     * The localization manager.
     */
    private static ILocalizationManager localizationManager;

    /**
     * Gets the IntervalDate interval datedd mmyyyy.
     *
     * @param beginDateddMMyyyy the String begin datedd m myyyy
     * @param endDateddMMyyyy the String end datedd m myyyy
     * @return the IntervalDate interval datedd m myyyy
     * @throws BadExpectedValueException the bad expected value exception
     */
    public static IntervalDate getIntervalDateddMMyyyy(final String beginDateddMMyyyy, final String endDateddMMyyyy) throws BadExpectedValueException, DateTimeParseException {
        final IntervalDate intervalDate = new IntervalDate(DateUtil.DD_MM_YYYY_FILE);
        intervalDate.setDates(intervalDate.getDate(beginDateddMMyyyy), intervalDate.getDate(endDateddMMyyyy));
        return intervalDate;
    }

    /**
     * Gets the IntervalDate interval date m myyyy.
     *
     * @param beginDateMMyyyy the String begin date m myyyy
     * @param endDateMMyyyy the String end date m myyyy
     * @return the IntervalDate interval date m myyyy
     * @throws BadExpectedValueException the bad expected value exception
     */
    public static IntervalDate getIntervalDateMMyyyy(String beginDateMMyyyy, String endDateMMyyyy) throws BadExpectedValueException, DateTimeParseException {
        beginDateMMyyyy = String.format(FIRST_DAY_OF_MOUNTH_BEFORE, beginDateMMyyyy);
        LocalDateTime beginDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, beginDateMMyyyy).with(TemporalAdjusters.firstDayOfYear());
        endDateMMyyyy = String.format(FIRST_DAY_OF_MOUNTH_BEFORE, endDateMMyyyy);
        LocalDateTime endDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, endDateMMyyyy).with(TemporalAdjusters.lastDayOfYear());
        final IntervalDate intervalDate = new IntervalDate(
                beginDate,
                endDate,
                DateUtil.DD_MM_YYYY_FILE);
        return intervalDate;
    }

    /**
     * Gets the IntervalDate interval dateyyyy.
     *
     * @param beginDateyyyy the String begin dateyyyy
     * @param endDateyyyy the String end dateyyyy
     * @return the IntervalDate interval dateyyyy
     * @throws BadExpectedValueException the bad expected value exception
     */
    public static IntervalDate getIntervalDateyyyy(String beginDateyyyy, String endDateyyyy) throws BadExpectedValueException, DateTimeParseException {
        beginDateyyyy = String.format(JANUARY_FIRST_BEFORE, beginDateyyyy);
        LocalDateTime beginDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, beginDateyyyy);
        endDateyyyy = String.format(DECEMBER_LAST_BEFORE, endDateyyyy);
        LocalDateTime endDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, endDateyyyy);
        final IntervalDate intervalDate = new IntervalDate(
                beginDate,
                endDate,
                DateUtil.YYYY_MM_DD_FILE);
        return intervalDate;
    }

    /**
     * Gets the IntervalDate interval dateyyyy mm.
     *
     * @param beginDateyyyyMM the String begin dateyyyy mm
     * @param endDateyyyyMM the String end dateyyyy mm
     * @return the IntervalDate interval dateyyyy mm
     * @throws BadExpectedValueException the bad expected value exception
     */
    public static IntervalDate getIntervalDateyyyyMM(String beginDateyyyyMM, String endDateyyyyMM) throws BadExpectedValueException, DateTimeParseException {
        beginDateyyyyMM = String.format(FIRST_DAY_OF_MOUNTH_AFTER, beginDateyyyyMM);
        LocalDateTime beginDate = DateUtil.readLocalDateTimeFromText(DateUtil.YYYY_MM_DD_FILE, beginDateyyyyMM).with(TemporalAdjusters.firstDayOfYear());
        endDateyyyyMM = String.format(FIRST_DAY_OF_MOUNTH_AFTER, endDateyyyyMM);
        LocalDateTime endDate = DateUtil.readLocalDateTimeFromText(DateUtil.YYYY_MM_DD_FILE, endDateyyyyMM).with(TemporalAdjusters.lastDayOfYear());
        final IntervalDate intervalDate = new IntervalDate(
                beginDate,
                endDate,
                DateUtil.DD_MM_YYYY_FILE);
        return intervalDate;
    }

    /**
     * Gets the IntervalDate interval dateyyyy m mdd.
     *
     * @param beginDateyyyyMMdd the String begin dateyyyy m mdd
     * @param endDateyyyyMMdd the String end dateyyyy m mdd
     * @return the IntervalDate interval dateyyyy m mdd
     * @throws BadExpectedValueException the bad expected value exception
     */
    public static IntervalDate getIntervalDateyyyyMMdd(final String beginDateyyyyMMdd, final String endDateyyyyMMdd) throws BadExpectedValueException, DateTimeParseException {
        final IntervalDate intervalDate = new IntervalDate(DateUtil.YYYY_MM_DD_FILE);
        intervalDate.setDates(intervalDate.getDate(beginDateyyyyMMdd), intervalDate.getDate(endDateyyyyMMdd));
        return intervalDate;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public static void setLocalizationManager(ILocalizationManager localizationManager) {
        IntervalDate.localizationManager = localizationManager;
    }

    /**
     * The Date begin date.
     */
    protected LocalDateTime beginDate;
    /**
     * The Date end date.
     */
    protected LocalDateTime endDate;
    private Duration duration;
    /**
     * The SimpleDateFormat date format.
     */
    private String dateFormat = DateUtil.DD_MM_YYYY_FILE;
    private DateTimeFormatter dateFormatter = new DateTimeFormatterBuilder()
            .appendPattern(dateFormat)
            .toFormatter();

    /**
     * Instantiates a new IntervalDate interval date.
     *
     * @param localBeginDate the Date begin date
     * @param localEndDate the Date end date
     * @param dateFormat the SimpleDateFormat format
     * @throws BadExpectedValueException the bad expected value exception
     */
    public IntervalDate(final LocalDateTime localBeginDate, final LocalDateTime localEndDate, final String dateFormat) throws BadExpectedValueException, DateTimeParseException {
        super();
        setDateFormatter(dateFormat);
        setDates(localBeginDate, localEndDate);
    }

    /**
     * Instantiates a new IntervalDate interval date.
     *
     * @param dateFormat the SimpleDateFormat date format
     */
    public IntervalDate(final String dateFormat) {
        super();
        setDateFormatter(dateFormat);
    }

    /**
     * Instantiates a new IntervalDate interval date.
     *
     * @param beginDate the String begin date
     * @param endDate the String end date
     * @param dateFormat
     * @throws BadExpectedValueException the bad expected value exception
     */
    public IntervalDate(final String beginDate, final String endDate, final String dateFormat) throws BadExpectedValueException, DateTimeParseException {
        super();
        setDateFormatter(dateFormat);
        setDates(getDate(beginDate), getDate(endDate));
    }

    /**
     *
     * @param dateFormat
     */
    protected void setDateFormatter(String dateFormat) {
        this.dateFormat = dateFormat;
        if (dateFormat != null) {
            this.dateFormatter = new DateTimeFormatterBuilder().appendPattern(dateFormat).toFormatter();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */

    /**
     *
     * @param o
     * @return
     */

    @Override
    public int compareTo(final IntervalDate o) {
        final int compareBegin = o.getBeginDate().compareTo(getBeginDate());
        if (compareBegin == 0) {
            return o.getEndDate().compareTo(getEndDate());
        }
        return compareBegin;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    /**
     *
     * @param obj
     * @return
     */

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof IntervalDate)) {
            return false;
        }
        IntervalDate toCompare = (IntervalDate) obj;
        return Objects.equals(beginDate, toCompare.beginDate)
                && Objects.equals(endDate, toCompare.endDate);
    }

    /**
     *
     * @return
     */
    public LocalDateTime getBeginDate() throws DateTimeParseException {
        return beginDate;
    }

    /**
     * Gets the String begin date to string.
     *
     * @return the String begin date to string
     */
    public String getBeginDateToString() throws DateTimeException {
        return dateFormatter.withZone(DateUtil.UTC_ZONEID).format(beginDate);
    }

    /**
     * Gets the SimpleDateFormat date format.
     *
     * @return the SimpleDateFormat date format
     */
    public DateTimeFormatter getDateFormat() {
        return this.dateFormatter;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getEndDate() {
        return endDate;
    }

    /**
     * Gets the String end date to string.
     *
     * @return the String end date to string
     */
    public String getEndDateToString() throws DateTimeException {
        return dateFormatter.withZone(DateUtil.UTC_ZONEID).format(endDate);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    /**
     *
     * @return
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (beginDate == null ? 0 : beginDate.hashCode());
        result = prime * result + (endDate == null ? 0 : endDate.hashCode());
        return result;
    }

    /**
     * Checks if is into.
     *
     * @param date the Date date
     * @return true, if is into
     */
    public boolean isInto(final LocalDateTime date) {
        return !(date.isAfter(getEndDate()) || date.isBefore(getBeginDate()));
    }

    /**
     * Checks if is into.
     *
     * @param date the Date date
     * @param fromInclusive the boolean from inclusive
     * @param toInclusive the boolean to inclusive
     * @return true, if is into
     */
    public boolean isInto(final LocalDateTime date, final boolean fromInclusive, final boolean toInclusive) {
        final boolean isAfterBeginDate = fromInclusive ? !date.isBefore(getBeginDate()) : getBeginDate().isBefore(date);
        final boolean isBeforeEndDate = toInclusive ? !date.isAfter(getEndDate()) : getEndDate().isAfter(date);
        return isAfterBeginDate && isBeforeEndDate;
    }

    /**
     * Overlaps.
     *
     * @param intervalDate the IntervalDate interval date
     * @return true, if successful
     */
    public boolean overlaps(final IntervalDate intervalDate) {
        if (intervalDate == null) {
            return false;
        } else {

            return beginDate.isBefore(intervalDate.getEndDate()) && endDate.isAfter(intervalDate.getBeginDate());
        }
    }

    /**
     *
     * @param printStream
     * @param localizationManager
     */
    public void toLocalString(final PrintStream printStream, ILocalizationManager localizationManager) {
        printStream.println(String.format(
                this.localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_FROM_TO),
                this.getBeginDateToString(),
                this.getEndDateToString()
        ));
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */

    /**
     *
     * @return
     */

    @Override
    public String toString() {
        return "du " + getBeginDateToString() + " au " + getEndDateToString();
    }

    /**
     * Gets the Date date.
     *
     * @param UTCdate
     * @return the Date date
     * @throws BadExpectedValueException the bad expected value exception
     */
    protected LocalDateTime getDate(final String UTCdate) throws BadExpectedValueException {
        try {
            if (DateUtil.MM_YYYY.equals(dateFormat)) {
                return DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "01/".concat(UTCdate));
            }
            if (DateUtil.MM_YYYY_FILE.equals(dateFormat)) {
                return DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, "01-".concat(UTCdate));
            }
            if (DateUtil.YYYY_MM.equals(dateFormat)) {
                return DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, UTCdate.concat("/01"));
            }
            if (DateUtil.YYYY.equals(dateFormat)) {
                return DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "01/01/".concat(UTCdate));
            }
            return DateUtil.readLocalDateTimeFromText(dateFormat, UTCdate);
        } catch (DateTimeParseException ex) {
            throw new BadExpectedValueException(String.format(IntervalDate.localizationManager.getMessage(IntervalDate.BUNDLE_SOURCE_PATH, IntervalDate.PROPERTY_MSG_BAD_DATES), UTCdate), ex);

        }
    }

    /**
     * Sets the dates.
     *
     * @param localBeginDate the Date begin date
     * @param localEndDate the Date end date
     * @throws BadExpectedValueException the bad expected value exception
     */
    protected void setDates(final LocalDateTime localBeginDate, final LocalDateTime localEndDate) throws BadExpectedValueException {
        if (localBeginDate.isAfter(localEndDate)) {
            throw new BadExpectedValueException(IntervalDate.localizationManager.getMessage(IntervalDate.BUNDLE_SOURCE_PATH, IntervalDate.PROPERTY_MSG_BAD_DATES_FOR_PERIOD));
        }
        this.beginDate = localBeginDate;
        this.endDate = localEndDate;
        this.duration = Duration.between(this.beginDate, this.endDate);
    }

    /**
     *
     * @return
     */
    public Duration getDuration() {
        return duration;
    }
}
