/**
 * OREILacs project - see LICENCE.txt for use created: 28 juil. 2009 16:26:22
 */
package org.inra.ecoinfo.utils;

import com.Ostermiller.util.CSVParser;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.mozilla.universalchardet.UniversalDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Utils.
 */
public class Utils {

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger("Utils");
    /**
     * The Constant MSG_NO_DATA.
     */
    public static final String MSG_NO_DATA = "PROPERTY_MSG_NO_DATA";
    /**
     * The Constant ENCODING_ISO_8859_1.
     */
    public static final String ENCODING_ISO_8859_1 = "iso-8859-1";
    /**
     * The Constant ENCODING_ISO_8859_1.
     */
    public static final String ENCODING_ISO_8859_15 = "iso-8859-15";
    /**
     * The Constant ENCODING_UTF8.
     */
    public static final String ENCODING_UTF8 = "utf-8";
    /**
     * The Constant SEPARATOR_URL.
     */
    public static final String SEPARATOR_URL = "/";
    /**
     * The Constant EXTENSION_FILE_CSV.
     */
    public static final String EXTENSION_FILE_CSV = ".csv";
    /**
     * The Constant EXTENSION_FILE_ZIP.
     */
    public static final String EXTENSION_FILE_ZIP = ".zip";
    /**
     *
     */
    public static final String[] POSIX_CAR = new String[]{"$", "^", "[", "]", "(", ")", "|", "+", "?", "*", ".", "\\"};

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.utils.messages";
    /**
     * The Constant PROPERTY_MSG_NO_NULL_ARG.
     */
    protected static final String PROPERTY_MSG_NO_NULL_ARG = "PROPERTY_MSG_NO_NULL_ARG";
    /**
     * The Constant DOT.
     */
    protected static final String DOT = ".";
    /**
     * The Constant COLON_REGEXP.
     */
    protected static final String COLON_REGEXP = "^([-]?[0-9]*),([0-9]*)$";
    /**
     * The Constant COLON_REGEXP.
     */
    protected static final String COLON_REPLACE_DOT = "$1.$2";
    /**
     * The Constant PATTERN_NOPRMALIZE_ACCENT.
     */
    protected static final String PATTERN_NOPRMALIZE_ACCENT = "[\u0300-\u036F]";
    /**
     * The Constant PATTERN_DOUBLE_SPACE.
     */
    protected static final String PATTERN_DOUBLE_SPACE = "\\b\\s{2,}\\b";
    /**
     * The Constant SPACE.
     */
    protected static final String SPACE = " ";

    /**
     *
     */
    protected static final String PATTERN_CHAR_TO_REPLACE = PATTERN_DOUBLE_SPACE + "|" + SPACE + "|'|\"";
    /**
     * The Constant UNDERSCORE.
     */
    protected static final String UNDERSCORE = "_";
    /**
     * The Constant MSG_MISMATCH_CLASS_OBJECT.
     */
    protected static final String MSG_MISMATCH_CLASS_OBJECT = "PROPERTY_MSG_MISMATCH_CLASS_OBJECT";
    /**
     * The Constant MSG_BAD_CSV.
     */
    protected static final String MSG_BAD_CSV = "PROPERTY_MSG_BAD_CSV";
    /**
     * The Constant PATTERN_LIGNE_VIDE.
     */
    protected static final String PATTERN_LIGNE_VIDE = "(?m)^$";
    /**
     * The Constant PATTERN_BAD_DATES_FIND.
     */
    protected static final String PATTERN_BAD_DATES_DDMMYYYY_FIND = "^(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-((?:[1-2][0-9])?[0-9]{2})$";
    /**
     * The Constant PATTERN_BAD_DATES_REPLACE.
     */
    protected static final String PATTERN_BAD_DATES_DDMMYYYY_REPLACE = "$1/$2/$3";
    /**
     * The Constant PATTERN_BAD_DATES_FIND.
     */
    protected static final String PATTERN_BAD_DATES_MMYYYY_FIND = "^(0[1-9]|1[012])-([0-9]{4})$";
    /**
     * The Constant PATTERN_BAD_DATES_REPLACE.
     */
    protected static final String PATTERN_BAD_DATES_MMYYYY_REPLACE = "$1/$2";

    /**
     *
     */
    protected static final String COLON_PATTERN_REPLACE = "$1.$2";

    /**
     * Convert encoding.
     *
     * @param sourceBytes the byte[] source bytes
     * @param targetEncoding the String target encoding
     * @return the byte[] byte[]
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static byte[] convertEncoding(final byte[] sourceBytes, final String targetEncoding) throws IOException {
        return new String(sourceBytes, Utils.detectStreamEncoding(sourceBytes)).getBytes(targetEncoding);
    }

    /**
     * Creates the code from string.
     *
     * @param string the String string
     * @return the String string
     */
    public static String createCodeFromString(final String string) {
        if (string == null) {
            return null;
        }
        String result = string.trim().toLowerCase();
        result = result.replaceAll(PATTERN_CHAR_TO_REPLACE, Utils.UNDERSCORE);
        return Normalizer.normalize(result, Normalizer.Form.NFD).replaceAll(Utils.PATTERN_NOPRMALIZE_ACCENT, "");
    }

    /**
     * Creates the id from string.
     *
     * @param identifiant the String identifiant
     * @return the String string
     */
    public static String createIdFromString(final String identifiant) {
        String result = identifiant.toLowerCase();
        result = result.replaceAll(Utils.SPACE, "");
        return Normalizer.normalize(result, Normalizer.Form.NFD).replaceAll(Utils.PATTERN_NOPRMALIZE_ACCENT, "");
    }

    /**
     * Detect stream encoding.
     *
     * @param bis the BufferedInputStream bis
     * @return the String string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String detectStreamEncoding(final BufferedInputStream bis) throws IOException {
        final byte[] buf = new byte[4_096];
        String encoding = null;
        final UniversalDetector detector = new UniversalDetector(null);
        int nread = bis.read(buf);
        while (nread > 0 && !detector.isDone()) {
            detector.handleData(buf, 0, nread);
            nread = bis.read(buf);
        }
        detector.dataEnd();
        encoding = detector.getDetectedCharset();
        detector.reset();
        if (encoding == null) {
            return Utils.ENCODING_UTF8;
        }
        if (!Utils.ENCODING_UTF8.equalsIgnoreCase(encoding)) {
            encoding = Utils.ENCODING_ISO_8859_15;
        }
        return encoding;
    }

    /**
     * Detect stream encoding.
     *
     * @param bytes the byte[] bytes
     * @return the String string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String detectStreamEncoding(final byte[] bytes) throws IOException {
        String encoding = null;
        final UniversalDetector detector = new UniversalDetector(null);
        detector.handleData(bytes, 0, bytes.length);
        detector.dataEnd();
        encoding = detector.getDetectedCharset();
        detector.reset();
        if (encoding == null) {
            return Utils.ENCODING_UTF8;
        }
        if (!Utils.ENCODING_UTF8.equalsIgnoreCase(encoding)) {
            encoding = Utils.ENCODING_ISO_8859_15;
        }
        return encoding;
    }

    /**
     * Match exception cause.
     *
     * @param exception the Throwable exception
     * @param exceptionCauseClass the Class<? extends Exception> exception cause
     * class
     * @return the Boolean boolean
     */
    public static Boolean matchExceptionCause(final Throwable exception, final Class<? extends Exception> exceptionCauseClass) {
        if (exception.getCause() == null) {
            return false;
        } else {
            if (exception.getCause().getClass().equals(exceptionCauseClass)) {
                return true;
            } else {
                return Utils.matchExceptionCause(exception.getCause(), exceptionCauseClass);
            }
        }
    }

    /**
     * Sanitize data.
     *
     * @param data the byte[] data
     * @param localizationManager the ILocalizationManager localization manager
     * @return the byte[] byte[]
     * @throws BusinessException the business exception
     */
    public static byte[] sanitizeData(final byte[] data, final ILocalizationManager localizationManager) throws BusinessException {
        if (data == null) {
            throw new BusinessException(localizationManager.getMessage(Utils.BUNDLE_SOURCE_PATH, Utils.MSG_NO_DATA));
        }
        try {
            final String encoding = Utils.detectStreamEncoding(data);
            String dataString = new String(Utils.convertEncoding(data, encoding), encoding)
                    .replaceAll("(?m)(?<=^)\"\"(?=;)|(?<=;)\"\"(?=;)|(?<=;)\"\"(?=$)", "")
                    .replaceAll("\"\"", "@!!@");
            if (Pattern.compile(Utils.PATTERN_LIGNE_VIDE).matcher(dataString).find()) {
                throw new BusinessException(localizationManager.getMessage(Utils.BUNDLE_SOURCE_PATH, Utils.MSG_BAD_CSV));
            }
            CSVParser csvParser = new CSVParser(new StringReader(sanitizeQuotes(dataString)), ';');
            String[] line = csvParser.getLine();
            StringBuilder st = new StringBuilder();
            while (line != null) {
                final String ligne = Stream.of(line)
                        .map(Utils::sanitizeQuotes)
                        .map(Utils::sanitizeDates)
                        .map(Utils::sanitizeNumbers)
                        .collect(Collectors.joining("\";\"", "\"", "\"\n"));
                st.append(ligne);
                line = csvParser.getLine();
            }
            return st.toString().substring(0, st.toString().length() - 1).getBytes(Utils.ENCODING_UTF8);
        } catch (final UnsupportedEncodingException e) {
            throw new BusinessException("unsupporting encoding", e);
        } catch (final IOException e) {
            throw new BusinessException("io exception", e);
        }
    }

    /**
     *
     * @param dataString
     * @return
     */
    public static String sanitizeQuotes(String dataString) {
//                .replaceAll("(?<=;)\"\"(?=;)|(?<=^)\"\"(?=;)|(?<=;)\"\"(?=$)","")
        return dataString
                .replaceAll("@!!@(.*?)@!!@","«$1»")
                .replaceAll("@!!@","\"\"");
    }

    /**
     *
     * @param field
     * @return
     */
    public static String sanitizeDates(String field) {
        return field.replaceAll(Utils.PATTERN_BAD_DATES_DDMMYYYY_FIND, Utils.PATTERN_BAD_DATES_DDMMYYYY_REPLACE)
                .replaceAll(Utils.PATTERN_BAD_DATES_MMYYYY_FIND, Utils.PATTERN_BAD_DATES_MMYYYY_REPLACE);
    }

    /**
     *
     * @param field
     * @return
     */
    public static String sanitizeNumbers(String field) {
        return field.replaceAll(Utils.COLON_REGEXP, COLON_PATTERN_REPLACE);
    }

    /**
     * Test casted arguments.
     *
     * @param objectToCast the Object object to cast
     * @param targetCast the Class<?> target cast
     * @param localizationManager the ILocalizationManager localization manager
     */
    public static void testCastedArguments(final Object objectToCast, final Class<?> targetCast, final ILocalizationManager localizationManager) {
        testNotNullArguments(localizationManager, objectToCast);
        Class<?> clazz = objectToCast.getClass();
        if (clazz != targetCast && !clazz.toString().contains(targetCast.toString())) {
            throw new IllegalArgumentException(String.format(localizationManager.getMessage(Utils.BUNDLE_SOURCE_PATH, Utils.MSG_MISMATCH_CLASS_OBJECT), clazz.getName(), targetCast.getName()));
        }
    }

    /**
     * Test not null arguments.
     *
     * @param localizationManager the ILocalizationManager localization manager
     * @param args the Object[] args
     * @return the ErrorsReport errors report
     */
    public static ErrorsReport testNotNullArguments(final ILocalizationManager localizationManager, final Object... args) {
        final String defaultMessage = localizationManager.getMessage(ErrorsReport.BUNDLE_NAME, ErrorsReport.PROPERTY_MSG_ERROR);
        final ErrorsReport errorsReport = new ErrorsReport(defaultMessage);
        Integer argIndex = 0;
        for (final Object object : args) {
            argIndex++;
            if (object == null) {
                final String errorMessage = localizationManager.getMessage(Utils.BUNDLE_SOURCE_PATH, Utils.PROPERTY_MSG_NO_NULL_ARG);
                errorsReport.addException(new BusinessException(String.format(errorMessage, argIndex)));
            }
        }
        if (errorsReport.hasErrors()) {
            throw new IllegalArgumentException(errorsReport.getMessages());
        }
        return errorsReport;
    }

    /**
     *
     * @param testString
     * @return
     */
    public static Boolean isPOSIXFree(String testString) {
        return 0 > StringUtils.indexOfAny(testString, POSIX_CAR);
    }

    /**
     *
     * @param <T>
     * @param object
     * @return
     */
    protected static <T> T printObjectInStream(T object) {
        LOGGER.debug("{}", object);
        return object;
    }


    /**
     *
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String toMD5(final String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] thedigest = md.digest(password.getBytes(StandardCharsets.UTF_8));
        BigInteger bI = new BigInteger(1, thedigest);
        return bI.toString(16);
    }

    /**
     *
     */
    protected Utils() {
        super();
    }

}
