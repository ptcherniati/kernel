package org.inra.ecoinfo.mailsender;

import java.util.List;
import org.inra.ecoinfo.identification.entity.Utilisateur;

/**
 * The Interface IMailSender.
 */
public interface IMailSender {

    /**
     * Send mail.
     *
     * @param from the String from
     * @param to the String to
     * @param subject the String subject
     * @param message the String message
     * @return the int int
     */
    int sendMail(String from, String to, String subject, String message);

    /**
     *
     * @param from
     * @param to
     * @param cc
     * @param subject
     * @param message
     * @return
     */
    int sendMail(final String from, final String to, final List<Utilisateur> cc, final String subject, final String message);

}
