package org.inra.ecoinfo.notifications.entity;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import static org.inra.ecoinfo.notifications.entity.Notification.JPA_NAME_ARCHIVED;
import static org.inra.ecoinfo.notifications.entity.Notification.JPA_NAME_DATE;

/**
 * The Class Notification.
 * uniqueConstraints = @UniqueConstraint(columnNames = {Utilisateur.JPA_NAME_UTILISATEUR_FK, Notification.JPA_NAME_DATE, "message"}),     
 */
@Entity
@Table(name = Notification.NAME_TABLE,
       indexes = {
           @Index(name = "not_date_idx", columnList = JPA_NAME_DATE),
           @Index(name = "not_archived_idx", columnList = JPA_NAME_ARCHIVED),
           @Index(name = "not_user_idx", columnList = Utilisateur.JPA_NAME_UTILISATEUR_FK),
           @Index(name = "not_user_date_idx", columnList = Utilisateur.JPA_NAME_UTILISATEUR_FK + "," + Notification.JPA_NAME_DATE),
           @Index(name = "not_user_date_archived_idx", columnList = Utilisateur.JPA_NAME_UTILISATEUR_FK + "," + Notification.JPA_NAME_DATE+","+Notification.JPA_NAME_ARCHIVED)}
)
public class Notification implements Serializable {

    /**
     * The Constant ERROR.
     */
    public static final String ERROR = "error";
    /**
     * The Constant INFO.
     */
    public static final String INFO = "info";
    /**
     * The Constant MAX_BODY_LENGHT.
     */
    public static final int MAX_BODY_LENGHT = 1_000;
    /**
     * The Constant NAME_TABLE.
     */
    public static final String NAME_TABLE = "Notification";

    /**
     *
     */
    public static final String JPA_NAME_DATE = "date";

    /**
     *
     */
    public static final String JPA_NAME_ARCHIVED = "archived";
    /**
     * The Constant WARN.
     */
    public static final String WARN = "warn";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The boolean archived.
     */
    @Column(name = JPA_NAME_ARCHIVED, nullable = false)
    private boolean archived = false;
    /**
     * The String attachment.
     */
    @Column(nullable = true,columnDefinition = "TEXT")
    private String attachment;
    /**
     * The String body.
     */
    @Column(columnDefinition = "TEXT")
    private String body;
    /**
     * The Date date.
     */
    @Column(name = JPA_NAME_DATE)
    @CreationTimestamp
    private LocalDateTime date;
    /**
     * The String level.
     */
    @Column(nullable = false)
    private String level;
    /**
     * The String message.
     */
    @Column(nullable = false, columnDefinition = "TEXT")
    private String message;
    /**
     * The Utilisateur utilisateur.
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Utilisateur.JPA_NAME_UTILISATEUR_FK, referencedColumnName = "id", nullable = false)
    private Utilisateur utilisateur;
    /**
     * The id @link(Long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE) 
    protected Long id;

    /**
     *
     */
    public Notification() {
    }

    /**
     * Instantiates a new Notification notification.
     *
     * @param level the String level
     * @param message the String message
     * @param body the String body
     */
    public Notification(final String level, final String message, final String body) {
        super();
        this.level = level;
        this.message = message;
        setBody(body);
    }
    /**
     * Instantiates a new Notification notification.
     * @param date
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    /**
     * Gets the String attachment.
     *
     * @return the String attachment
     */
    public String getAttachment() {
        return Optional.ofNullable(attachment).orElseGet(String::new );
    }

    /**
     * Sets the void attachment.
     *
     * @param attachment the String attachment
     */
    public void setAttachment(final String attachment) {
        this.attachment = attachment;
    }

    /**
     * Gets the String body.
     *
     * @return the String body
     */
    public String getBody() {
        return body;
    }

    /**
     * Sets the void body.
     *
     * @param body the String body
     */
    public void setBody(final String body) {
        final StringBuilder bodyBuilder = new StringBuilder();
        if (body != null && body.length() > 0) {
            bodyBuilder.append("<p>").append(body.replaceAll("^.*Exception:", "").replaceAll("\n|\r|\n\r|\r\n", "</p><p>")).append("</p>");
        }
        this.body = bodyBuilder.toString();
    }

    /**
     * Gets the String body for affichage.
     *
     * @return the String body for affichage
     */
    public String getBodyForAffichage() {
        return !Strings.isNullOrEmpty(body) ? body.substring(0, Math.min(body.length() == 0 ? 0 : body.length(), Notification.MAX_BODY_LENGHT)) : "";
    }

    /**
     * Gets the int body length.
     *
     * @return the int body length
     */
    public int getBodyLength() {
        return body != null ? body.length() : 0;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the String level.
     *
     * @return the String level
     */
    public String getLevel() {
        return level;
    }

    /**
     * Sets the void level.
     *
     * @param level the String level
     */
    public void setLevel(final String level) {
        this.level = level;
    }

    /**
     * Gets the String message.
     *
     * @return the String message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the void message.
     *
     * @param message the String message
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * Gets the Utilisateur utilisateur.
     *
     * @return the Utilisateur utilisateur
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     * Sets the void utilisateur.
     *
     * @param utilisateur the Utilisateur utilisateur
     */
    public void setUtilisateur(final Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    /**
     * Checks if is archived.
     *
     * @return true, if is archived
     */
    public boolean isArchived() {
        return archived;
    }

    /**
     * Sets the void archived.
     *
     * @param archived the boolean archived
     */
    public void setArchived(final boolean archived) {
        this.archived = archived;
    }

    /**
     * Checks if is body troncate.
     *
     * @return true, if is body troncate
     */
    public boolean isBodyTroncate() {
        return body != null && !getBody().equals(getBodyForAffichage());
    }
}
