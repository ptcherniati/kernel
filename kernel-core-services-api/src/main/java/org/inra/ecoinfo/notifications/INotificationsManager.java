package org.inra.ecoinfo.notifications;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface INotificationsManager.
 */
public interface INotificationsManager extends Serializable {

    /**
     * The String id bean.
     */
    String ID_BEAN = "notificationsManager";

    /**
     * Adds the notification.
     *
     * @param notification the Notification notification
     * @param target the String target
     * @throws BusinessException the business exception
     */
    void addNotification(Notification notification, String target) throws BusinessException;

    /**
     * Gets the List<Notification> all no archived by user login.
     *
     * @param userLogin the String user login
     * @return the List<Notification> all no archived by user login
     */
    List<Notification> getAllNoArchivedByUserLogin(String userLogin);

    /**
     * Gets the List<Notification> all notifications.
     *
     * @return the List<Notification> all notifications
     */
    List<Notification> getAllNotifications();

    /**
     * Gets the List<Notification> all transients notifications.
     *
     * @param date the Date date
     * @return the List<Notification> all transients notifications
     */
    List<Notification> getAllTransientsNotifications(LocalDateTime date);

    /**
     * Gets the List<Notification> all user notifications.
     *
     * @param userLogin the String user login
     * @return the List<Notification> all user notifications
     */
    List<Notification> getAllUserNotifications(String userLogin);

    /**
     * Gets the List<Notification> archived notifications.
     *
     * @param login the String login
     * @return the List<Notification> archived notifications
     */
    List<Notification> getArchivedNotifications(String login);

    /**
     * Gets the byte[] attachment.
     *
     * @param notificationId the Long notification id
     * @return the byte[] attachment
     * @throws BusinessException the business exception
     */
    byte[] getAttachment(Long notificationId) throws BusinessException;

    /**
     * Gets the byte[] body.
     *
     * @param notificationId the Long notification id
     * @return the byte[] body
     */
    byte[] getBody(Long notificationId);

    /**
     * Gets the Notification notification by id.
     *
     * @param id the Long id
     * @return the Notification notification by id
     */
    Optional<Notification> getNotificationById(Long id);

    /**
     * Gets the List<Notification> transients user notifications.
     *
     * @param login the String login
     * @param date the Date date
     * @return the List<Notification> transients user notifications
     */
    List<Notification> getTransientsUserNotifications(String login, LocalDateTime date);

    /**
     * Mark all as read.
     *
     * @param utilisateurId the Long utilisateur id
     */
    void markAllAsRead(Long utilisateurId);

    /**
     * Mark notification as archived.
     *
     * @param id the long id
     */
    void markNotificationAsArchived(long id);

    /**
     * Mark notification as new.
     *
     * @param id the Long id
     */
    void markNotificationAsNew(Long id);

    /**
     * Removes the all notification.
     *
     * @param id the Long id
     * @throws BusinessException the business exception
     */
    void removeAllNotification(Long id) throws BusinessException;

    /**
     * Removes the notification.
     *
     * @param longValue the long long value
     * @throws BusinessException the business exception
     */
    void removeNotification(long longValue) throws BusinessException;
}
