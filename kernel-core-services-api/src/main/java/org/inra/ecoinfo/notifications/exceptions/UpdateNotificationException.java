/**
 * OREILacs project - see LICENCE.txt for use created: 25 févr. 2009 14:21:16
 */
package org.inra.ecoinfo.notifications.exceptions;

import org.inra.ecoinfo.utils.NotificationReport;

/**
 * The Class UpdateNotificationException.
 *
 * @author "Antoine Schellenberger"
 */
public class UpdateNotificationException extends Exception {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The notification report @link(NotificationReport).
     */
    private NotificationReport notificationReport;

    /**
     * Instantiates a new update notification exception.
     *
     * @param notificationReport the notification report
     * @link(NotificationReport) the notification report
     */
    public UpdateNotificationException(final NotificationReport notificationReport) {
        this.notificationReport = notificationReport;
        notificationReport.setErrorName(getClass().getSimpleName());
    }

    /**
     * Gets the notification report.
     *
     * @return the notification report
     */
    public NotificationReport getNotificationReport() {
        return notificationReport;
    }

    /**
     * Sets the notification report.
     *
     * @param notificationReport the new notification report
     */
    public void setNotificationReport(final NotificationReport notificationReport) {
        this.notificationReport = notificationReport;
    }
}
