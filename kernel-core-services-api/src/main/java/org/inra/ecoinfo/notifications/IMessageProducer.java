package org.inra.ecoinfo.notifications;

import org.inra.ecoinfo.notifications.entity.Notification;


/**
 * The Interface IMessageProducer.
 */
public interface IMessageProducer {

    /**
     * Push.
     *
     * @param login the String login
     * @param notification
     * @param notificationId
     */
    void push(String login, Notification notification);
}
