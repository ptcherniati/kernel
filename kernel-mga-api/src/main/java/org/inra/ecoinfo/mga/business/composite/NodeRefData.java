package org.inra.ecoinfo.mga.business.composite;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author yahiaoui
 */
@Entity
@Table(name = NodeRefData.tableName)
public class NodeRefData extends AbstractBranchNode<NodeRefData> implements INode<NodeRefData>, Serializable {

    /**
     *
     */
    public static final String tableName = "composite_node_ref_data";

    /**
     *
     * @param parentNode
     * @param ancestorNode
     */
    public NodeRefData(NodeRefData parentNode, NodeRefData ancestorNode) {
        super(parentNode, ancestorNode);
    }

    /**
     *
     */
    public NodeRefData() {
    }

}
