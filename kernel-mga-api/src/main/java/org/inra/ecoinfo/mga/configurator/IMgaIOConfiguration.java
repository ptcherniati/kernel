package org.inra.ecoinfo.mga.configurator;

import java.util.List;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;

/**
 *
 * @author yahiaoui
 */
public interface IMgaIOConfiguration {

    /**
     *
     * @return
     */
    Integer[] getEntryOrder();

    /**
     *
     * @return
     */
    Integer[] getSortOrder();

    /**
     *
     * @param nbElementInPath
     * @return
     */
    default Integer[] getSortOrder(int nbElementInPath) {
        return getSortOrder();
    }

    /**
     *
     * @param <T>
     * @return
     */
    <T extends INodeable> Class<T>[] getEntryType();

    /**
     *
     * @param <T>
     * @return
     */
    <T extends INodeable> Class<T> getLeafType();

    /**
     *
     * @return
     */
    Integer getCodeConfiguration();

    /**
     *
     * @return
     */
    boolean isIncludeAncestor();

    /**
     *
     * @return
     */
    Activities[] getActivities();

    /**
     *
     * @return
     */
    WhichTree getWhichTree();
    /**
     *
     * @param <T>
     * @return
     */
    <T extends INodeable> Class<T>  getStickyLeafType();

    /**
     *
     * @return
     */
    boolean displayColumnNames();

    /**
     *
     * @return
     */
    List<String> getColumnsTreeNames();

    /**
     *
     * @param <T>
     * @param columnName
     * @param index
     */
     <T extends INodeable> void addColumn(String columnName, int index);

    /**
     *
     */
    void clearColumns();

    /**
     *
     * @return
     */
    Boolean loadOnStartUp();

    /**
     *
     * @return
     */
    String getSkeletonBuilder();
}
