package org.inra.ecoinfo.mga.enums;

/**
 *
 * @author ryahiaoui
 */
public enum WhichTreatement {

    /**
     *
     */
    SAVE,
    /**
     *
     */
    UPDATE;

    /**
     *
     * @param whichTreatement
     * @return
     */
    public static WhichTreatement convertToWhichTreatement(String whichTreatement) {
        try {
            return WhichTreatement.valueOf(whichTreatement.toUpperCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException("Illegal whichTreatement : " + whichTreatement, ex);
        }
    }

}
