package org.inra.ecoinfo.mga.configuration;

/**
 *
 * @author yahiaoui
 */
public interface PatternConfigurator {

    /**
     *
     */
    String SPLITER = ";";

    /**
     *
     */
    String PATH_SEPARATOR = ",";

    /**
     *
     */
    String ANCESTOR_SEPARATOR = "/";

    /**
     *
     */
    String UNDERSCORE = "_";

    /**
     *
     */
    String EMPTY_STRING = "";

    /**
     *
     */
    String ASTERIX = "*";

    /**
     * *
     * dataset->(piegeage_en_montee:datatype,mon_theme:theme,nom_site:site||piegeage_en_montee:datatype
     * ) refdata->(nom_theme:theme,nom_site:site|nom_site:site)
     *
     */
    String PATTERN_NODE = "(([\\w-]+(?:\\:[\\w-]+))|\\*)";

    /**
     *
     */
    String PATTERN_PATH = PATTERN_NODE + "(?:," + PATTERN_NODE + ")*";

    /**
     *
     */
    String GOOD_EXPRESSION_PATTERN_REGEXP = "(E)->"
            + "\\("
            + PatternConfigurator.PATTERN_PATH
            + "(\\|\\|"
            + PatternConfigurator.PATTERN_PATH + ")*"
            + "\\)";

    /**
     *
     */
    String dateFormat = "dd/MM/yyyy";

    /**
     *
     */
    String MINUS_SEPARATOR = "-";

    /**
     *
     */
    String BRACKET_OPEN = "{";

    /**
     *
     */
    String BRACKET_CLOSE = "}";

    /**
     *
     */
    String COLON = ":";

    /**
     *
     */
    String INVALIDATE_DATE_STRING = "**/**/****";

    /**
     *
     */
    String DEFAULT_DATE_BEG = "defaultDateBeg";

    /**
     *
     */
    String DEFAULT_DATE_END = "defaultDateEnd";

    /**
     *
     */
    String UNDERSCORE_INPUT = "_input";

    /**
     *
     */
    String SPLITTER = "_._";

    /**
     *
     */
    String SPLITTER_VALUE = "_v_";

    /**
     *
     */
    String EMPTY_BRACKETS = "{_}";

    /**
     *
     */
    String TO_UPDATE_CHEKS = "toUpdateCheks";

    /**
     *
     */
    String ID_INTERMEDIATE_STATE = "idCheckIntermediateState";

    /**
     *
     */
    String TEMPORARY_DATES = "temporaryDates";

    /**
     *
     */
    String WHICH_DATES = "whichDates";

    /**
     *
     */
    String DATE_BEG = "beg";

    /**
     *
     */
    String DATE_END = "end";

    /**
     *
     */
    String FORMAT_ID_OF_NODE = "format_id_of_node";

    /**
     *
     */
    String ID_SELECTED_CHECK = "IdSelectedCheck";

    /**
     *
     */
    String PATTERN_ROWKEY = ":ROWKEY:";

    /**
     *
     */
    String IMPLICITE_UPDATE_STATE = "impliciteUpdateState";

}
