/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business.composite.groups;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.mga.business.composite.ICompositeActivity;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.utils.MgaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tcherniatinsky
 */
@Entity
@Table(
        name = Group.tableName,
        uniqueConstraints = @UniqueConstraint(columnNames = {Group.JPA_FIELD_GROUP_NAME, Group.JPA_FIELD_WHiCH_TREE}))
@XmlRootElement
public class Group implements ICompositeGroup, ICompositeActivity, Serializable {

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(Group.class);

    /**
     *
     */
    public static final String ID_JPA = "id";

    /**
     *
     */
    public static final String GROUP_NAME_ID = "group_id";

    /**
     *
     */
    public static final String JPA_FIELD_GROUP_NAME = "group_name";

    /**
     *
     */
    public static final String JPA_FIELD_WHiCH_TREE = "group_which_tree";

    /**
     *
     */
    public static final String JPA_FIELD_TYPE = "group_type";

    /**
     *
     */
    public static final String tableName = "groupe";

    /**
     * The Constant UTILISATEUR_NAME_ID.
     */

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = ID_JPA)
    private Long id;
    @Column(name = JPA_FIELD_GROUP_NAME)
    @NaturalId
    String groupName;
    @Enumerated(EnumType.STRING)
    @Column(name = JPA_FIELD_WHiCH_TREE)
    @NaturalId
    WhichTree whichTree;
    
    @Enumerated(EnumType.STRING)
    @Column(name = JPA_FIELD_TYPE)
    @NaturalId
    GroupType groupType;

    @Transient
    private Map<Activities, Map<Long, List<LocalDate>>> activitiesMap = new HashMap<>();

    @Column(columnDefinition = "TEXT")
    private String activityAsString = "{}";

    /**
     *
     */
    public Group() {
    }

    /**
     *
     * @param groupName
     * @param whichTree
     * @param groupType
     */
    public Group(String groupName, WhichTree whichTree, GroupType groupType) {
        this.groupName = groupName;
        this.whichTree = whichTree;
        this.groupType = groupType;
    }
    @Override
    public String toString() {
        return id + ":" + groupName + ":" + whichTree.name();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.groupName.hashCode();
        hash = 79 * hash + this.whichTree.name().hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Group other = (Group) obj;
        if (!this.groupName.equals(other.groupName)) {
            return false;
        }
        return this.whichTree==other.whichTree;
    }
    

    /**
     *
     * @return
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    @Override
    public String getGroupName() {
        return groupName;
    }

    /**
     *
     * @return
     */
    public WhichTree getWhichTree() {
        return whichTree;
    }

    /**
     *
     * @return
     */
    @Override
    public Set<Group> getAllGroups() {
        Set<Group> groups = new HashSet<>();
        groups.add(this);
        return groups;
    }

    /**
     *
     * @param whichTree
     */
    public void setWhichTree(WhichTree whichTree) {
        this.whichTree = whichTree;
    }

    /**
     *
     * @throws DateTimeParseException
     * @throws IOException
     */
    @PostLoad
    @Override
    public void initMap() throws DateTimeParseException, IOException {
        if(activityAsString==null){
            activityAsString="{}";
        }
        this.activitiesMap = MgaUtils.jsonToMap(activityAsString,
                new TypeReference<  Map<Activities, Map<Long, List<LocalDate>>>>() {
        });
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Activities, Map<Long, List<LocalDate>>> getActivitiesMap() {
        return activitiesMap;
    }

    /**
     *
     * @param activitiesMap
     */
    @Override
    public void setActivitiesMap(Map<Activities, Map<Long, List<LocalDate>>> activitiesMap) {
        try {
            this.activitiesMap = activitiesMap;
            this.activityAsString = MgaUtils.mapToJson(activitiesMap);
        } catch (JsonProcessingException ex) {
            LOGGER.error(ex.getOriginalMessage(), ex);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String getActivityAsString() {
        return activityAsString;
    }

    /**
     *
     * @param activityAsString
     */
    @Override
    public void setActivityAsString(String activityAsString) {
        try {
            this.activityAsString = activityAsString;
            this.activitiesMap = MgaUtils.jsonToMap(activityAsString,
                    new TypeReference<  Map<Activities, Map<Long, List<LocalDate>>>>() {
            });
        } catch (IOException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }

    }

    /**
     *
     */
    @Override
    public void synchronizeActivitiesMap() {
        setActivitiesMap(activitiesMap);
    }

    /**
     *
     */
    @Override
    public void synchronizeActivityAsString() {
        setActivityAsString(activityAsString);

    }

    /**
     *
     * @param whichTree
     * @return
     */
    @Override
    public Group getOwnGroup(WhichTree whichTree) {
        return this;
    }

    /**
     *
     * @param group
     * @return
     */
    @Override
    public boolean testIsMutableGroup(Group group) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    /**
     *
     * @return
     */
    public GroupType getGroupType() {
        return groupType;
    }

    /**
     *
     * @param groupType
     */
    public void setGroupType(GroupType groupType) {
        this.groupType = groupType;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
