package org.inra.ecoinfo.mga.caching;

import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;

/**
 *
 * @author ryahiaoui
 */
public interface ITreeSessionCacheManager {

    /**
     *
     */
    void initMap();

    /**
     *
     * @param group
     * @param codeConfiguration
     * @param codeConf
     * @return
     */
    ITreeNode getFlatTree(ICompositeGroup group, Integer codeConfiguration);

    /**
     *
     * @param group
     * @param treeNode
     * @param condeConf
     */
    void saveFlatTree(ICompositeGroup group, ITreeNode treeNode, Integer condeConf);

    /**
     *
     * @param group
     * @param codeConfiguration
     * @param codeConf
     */
    void removeFlatTree(ICompositeGroup group, Integer codeConfiguration);

    /**
     *
     */
    void clearFlatTree();

    /**
     *
     * @param group
     * @param codeConfiguration
     * @param codeConf
     * @return
     */
    boolean containKeyFlatTree(ICompositeGroup group, Integer codeConfiguration);

}
