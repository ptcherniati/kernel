package org.inra.ecoinfo.mga.business.composite;

import javax.swing.tree.TreeNode;

/**
 *
 * @author ryahiaoui
 */
public interface ICompositeTreeNode {

    /**
     *
     * @return
     */
    INode getNode();

    /**
     *
     * @param node
     */
    void setNode(INode node);

    /**
     *
     * @return
     */
    int getChildCount();

    /**
     *
     * @param index
     * @return
     */
    TreeNode getChildAt(int index);

}
