package org.inra.ecoinfo.mga.business;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;

/**
 *
 * @author yahiaoui
 */
public interface IMgaBuilder {

    /**
     *
     * @param path
     * @param order
     * @param splitter
     * @param skipHeader
     * @return
     */
    Stream<String> buildOrderedPaths(final String path, final Integer[] order, final String splitter, boolean skipHeader);

    /**
     *
     * @param <T>
     * @param pathes
     * @param typeResources
     * @param entities
     * @param whichTree
     * @param stickyNodeables
     * @return the
     * java.util.List<org.inra.ecoinfo.mga.business.composite.AbstractBranchNode>
     */
    <T extends INodeable> Stream<INode> buildLeavesForPathes(Stream<String> pathes, Class<T>[] typeResources, Map<String, INodeable> entities, WhichTree whichTree, List<? extends INodeable> stickyNodeables);

    /**
     *
     * @param recorder
     */
    void setRecorder(IMgaRecorder recorder);
}
