package org.inra.ecoinfo.mga.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.IOException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;

/**
 *
 * @author ryahiaoui
 */
public class MgaUtils {

    /**
     * The Constant PATTERN_DOUBLE_SPACE.
     */
    private static final String PATTERN_DOUBLE_SPACE = "\\b\\s{2,}\\b";
    /**
     * The Constant SPACE.
     */
    private static final String SPACE = " ";
    private static final String PATTERN_CHAR_TO_REPLACE = PATTERN_DOUBLE_SPACE + "|" + SPACE + "|'|\"";
    /**
     * The Constant UNDERSCORE.
     */
    private static final String UNDERSCORE = "_";
    private static final String PATTERN_NOPRMALIZE_ACCENT = "[\u0300-\u036F]";

    /**
     *
     * @param map
     * @return
     * @throws JsonProcessingException
     */
    public static String mapToJson(Map map) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return mapper.writeValueAsString(map);
    }

    /**
     * Creates the code from string.
     *
     * @param string the String string
     * @return the String string
     */
    public static String createCodeFromString(final String string) {
        if (string == null) {
            return null;
        }
        String result = string.trim().toLowerCase();
        result = result.replaceAll(PATTERN_CHAR_TO_REPLACE, UNDERSCORE);
        return Normalizer.normalize(result, Normalizer.Form.NFD).replaceAll(PATTERN_NOPRMALIZE_ACCENT, "");
    }

    /**
     *
     * @param jsonString
     * @param type
     * @return
     * @throws IOException
     */
    public static Map jsonToMap(String jsonString, TypeReference type) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        return (Map) mapper.readValue(jsonString, type);

    }

    /**
     *
     * @param set
     * @return
     * @throws JsonProcessingException
     */
    public static String setToJson(Set set) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        SimpleDateFormat sdf = new SimpleDateFormat(PatternConfigurator.dateFormat);
        mapper.setDateFormat(sdf);

        return mapper.writeValueAsString(set);
    }

    /**
     *
     * @param list
     * @return
     * @throws JsonProcessingException
     */
    public static String listToJson(List list) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        SimpleDateFormat sdf = new SimpleDateFormat(PatternConfigurator.dateFormat);
        mapper.setDateFormat(sdf);

        return mapper.writeValueAsString(list);
    }

    /**
     *
     * @param org
     * @param added
     * @return
     */
    public static String[] addValue(String[] org, String added) {
        String[] result = Arrays.copyOf(org, org.length + 1);
        result[org.length] = added;
        return result;
    }

}
