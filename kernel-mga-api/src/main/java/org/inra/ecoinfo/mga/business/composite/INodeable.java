package org.inra.ecoinfo.mga.business.composite;

/**
 *
 * @author yahiaoui
 */
public interface INodeable extends Comparable<INodeable> {

    /**
     *
     * @param idNodeable
     */
    void setId(Long idNodeable);

    /**
     *
     * @return
     */
    Long getId();

    /**
     *
     * @return
     */
    String getCode();

    /**
     *
     * @return
     */
    String getName();

    /**
     *
     * @param <T>
     * @return
     */
    <T extends INodeable> Class<T> getNodeableType();

    /**
     *
     * @param o
     * @return
     */
    @Override
    default int compareTo(INodeable o) {
        return o.getCode().compareTo(getCode());
    }

    /**
     *
     * @return
     */
    Long getOrder();

    /**
     *
     * @return
     */
    String getUniqueCode();

}
