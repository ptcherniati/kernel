/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business.composite.groups;

import java.io.Serializable;
import java.util.Set;
import org.inra.ecoinfo.mga.enums.WhichTree;

/**
 *
 * @author tcherniatinsky
 */
public interface ICompositeGroup extends Serializable{

    /**
     *
     * @return
     */
    String getGroupName();

    /**
     *
     * @return
     */
    Set<Group> getAllGroups();

    /**
     *
     * @param whichTree
     * @return
     */
    Group getOwnGroup(WhichTree whichTree);

    /**
     *
     * @return
     */
    Long getId();

    /**
     *
     * @param group
     * @return
     */
    boolean testIsMutableGroup(Group group);
}
