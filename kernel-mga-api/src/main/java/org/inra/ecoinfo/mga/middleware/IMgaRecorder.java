package org.inra.ecoinfo.mga.middleware;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.IExtractActivity;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;

/**
 *
 * @author ryahiaoui
 */
public interface IMgaRecorder extends IDAO<INodeable> {

    /**
     *
     * @param node
     */
    void remove(INode node);

    /**
     *
     * @param login
     */
    void removeExtractActivity(String login);

    /**
     *
     * @param query
     * @return
     */
    boolean emptyQueryResult(Function<CriteriaBuilder, CriteriaQuery<INode>> query);

    /**
     *
     * @param <T>
     * @param whichTree
     * @param type
     * @return
     */
    <T extends INodeable> boolean existNodeAccordingTypeResource(WhichTree whichTree, Class<T> type);

    /**
     *
     * @return
     */
    EntityManager getEntityManager();

    /**
     *
     * @param codeName
     * @param whichTree
     * @return
     */
    Optional<INode> getNodeByNodeableCodeName(String codeName, WhichTree whichTree);

    /**
     *
     * @param whichTree
     * @param codeName
     * @return
     */
    Stream<INode> getNodesByNodeableCodeName(WhichTree whichTree, String codeName);

    /**
     *
     * @return @throws PersistenceException
     */
    Stream<INodeable> getNodeables();

    /**
     *
     * @param <T>
     * @param type
     * @return
     * @throws PersistenceException
     */
    <T extends INodeable> Stream<T> getNodeables(Class<T> type);

    /**
     *
     * @param query
     * @return
     */
    Stream< INode> getNodes(Function<CriteriaBuilder, CriteriaQuery<INode>> query);

    /**
     *
     * @param nodeIds
     * @param whichTree
     * @return
     */
    Stream<INode> getNodesByIds(List<Long> nodeIds, WhichTree whichTree);

    /**
     *
     * @param <T>
     * @param whichTree
     * @param typeResource
     * @return
     */
    <T extends INodeable> Stream<INode> getNodesByTypeResource(WhichTree whichTree, Class<T> typeResource);

    /**
     *
     * @param compositeGroup
     * @param whichTree
     * @return
     */
    Stream<Group> getGroups(ICompositeGroup compositeGroup, WhichTree whichTree);

    /**
     *
     * @param node
     * @return
     */
    INode merge(INode node);

    /**
     *
     * @param activity
     */
    void mergeActivity(ICompositeActivity activity);

    /**
     *
     * @param activity
     */
    void persistActivity(ICompositeActivity activity);

    /**
     *
     * @param node
     */
    void persist(INode node);

    /**
     *
     * @param extractActivity
     */
    void persist(IExtractActivity extractActivity);

    /**
     *
     * @param code
     * @param whichTree
     * @return
     */
    Optional<INode> getNodeByNodeableCode(String code, WhichTree whichTree);

    /**
     *
     * @param <T>
     * @param parent
     * @param whichTree
     */
    <T extends AbstractBranchNode> void removeStickyLeaves(T parent, WhichTree whichTree);

    /**
     *
     * @return
     */
    Map<String, RealNode> getDbNodes();

    /**
     *
     * @param realNode
     */
    void saveOrUpdate(RealNode realNode);

    /**
     *
     * @param path
     * @return
     */
    Optional<RealNode> getRealNodeByNKey(String path);

    /**
     *
     * @return
     */
    Stream<RealNode> getAllRealNodes();

    /**
     *
     * @param login
     * @return
     */
    Stream<ExtractActivity> getMissingParentsNodes(String login);

    /**
     *
     * @param realNodeLeafId
     * @return
     */
    Optional<RealNode> getRealNodeById(Long realNodeLeafId);

    /**
     *
     * @return
     */
    Optional<Group> getPublicGroup();

    /**
     *
     * @param group
     */
    void saveOrUpdate(Group group);

    /**
     *
     * @param userDao
     * @param codeConfiguration
     */
    void setUserDao(ICompositeGroupDAO userDao, Integer codeConfiguration);

    /**
     *
     * @param groupName
     * @param whichTree
     * @return
     */
    Optional<Group> getGroupByGroupNameAndWhichTree(String groupName, WhichTree whichTree);

    /**
     *
     * @param codeConfiguration
     * @return
     */
    ICompositeGroupDAO getCompositeGroupDAO(Integer codeConfiguration);

    /**
     *
     * @param groupName
     * @param whichTree
     * @param groupType
     * @return
     */
    Group createGroup(String groupName, WhichTree whichTree, GroupType groupType);

    /**
     *
     * @param realNode
     * @return
     */
    RealNode remove(RealNode realNode);

    /**
     *
     * @param nodeable
     * @return
     */
    Nodeable remove(Nodeable nodeable);

}
