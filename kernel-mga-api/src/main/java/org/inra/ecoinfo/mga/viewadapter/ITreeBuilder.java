package org.inra.ecoinfo.mga.viewadapter;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.inra.ecoinfo.mga.business.IMgaServiceBuilder;
import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.IDataNode;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.caching.ITreeApplicationCacheManager;
import org.inra.ecoinfo.mga.caching.ITreeSessionCacheManager;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.Activities;

/**
 *
 * @author ryahiaoui
 */
public interface ITreeBuilder {

    /**
     *
     * @param <T>
     * @param root
     * @param id
     * @return
     */
    <T extends IDataNode> ITreeNode<T> getTreeNodeByID(ITreeNode<T> root, Long id);

    /**
     *
     * @param compositeGroup
     * @param codeConf
     */
    void removeTreeFromSession(ICompositeGroup compositeGroup, Integer codeConfiguration);

    /**
     *
     * @param config
     * @param activities
     * @param compositeGroup
     * @return
     */
    ITreeNode<IFlatNode> getOrLoadTree(Optional<IMgaIOConfiguration> configuration, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activities, ICompositeGroup compositeGroup);

    /**
     *
     * @return
     */
    IMgaIOConfigurator getMgaIOConfigurator();

    /**
     *
     * @param mgaServiceBuilder
     */
    void setMgaServiceBuilder(IMgaServiceBuilder mgaServiceBuilder);

    /**
     *
     * @return
     */
    IMgaServiceBuilder getMgaServiceBuilder();

    /**
     *
     */
    void clearTreeFromSession();

    /**
     *
     * @return
     */
    ITreeApplicationCacheManager getApplicationCacheManager();

    /**
     *
     * @return
     */
    ITreeSessionCacheManager getSessionCacheManager();

    /**
     *      * Build a flat tree from tree and add it to tinyTree children </p>
     *      * when a flatNode has activity its id is adding to checkedActivity</p>
     *
     * @param ownGroup
     * @param tree
     * @param activities
     * @param tinyTree
     * @param tinyTreePath
     * @param nodesWithActivity
     * @param config
     * @return
     */
    ITreeNode<IFlatNode> buildFlatTree(Group ownGroup, ITreeNode<AbstractBranchNode> tree, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activities, ITreeNode<IFlatNode> tinyTree, String tinyTreePath, Set<Long> nodesWithActivity, IMgaIOConfiguration config);

}
