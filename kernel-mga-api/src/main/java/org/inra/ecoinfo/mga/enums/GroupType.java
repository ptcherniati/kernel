/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.enums;

/**
 *
 * @author ptcherniati
 * 
 * enum that determine the type of a group
 */
public enum GroupType {

    /**
     * this is a group not linked to other object
     */
    SIMPLE_GROUP("SIMPLE_GROUP"),

    /**
     * this is a group linked to an user
     */
    USER_TYPE("USER_TYPE"),
    /**
     * this is a group linked to a file
     */
    FILE_TYPE("FILE_TYPE");
    
    private String groupType;

    private GroupType(String groupType) {
        this.groupType = groupType;
    }

    /**
     *
     * @return
     */
    public String getEntityTreeClass() {
        return groupType;
    }
    
}
