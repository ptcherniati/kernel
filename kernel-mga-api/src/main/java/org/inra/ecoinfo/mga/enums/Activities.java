package org.inra.ecoinfo.mga.enums;

import com.google.common.base.Strings;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;

/**
 *
 * @author yahiaoui
 */
public enum Activities {

    /**
     *
     */
    synthese(false),
    /**
     *
     */
    administration(false),
    /**
     *
     */
    publication(false),
    /**
     *
     */
    depot(false),
    /**
     *
     */
    suppression(false),
    /**
     *
     */
    associate(true),
    /**
     *
     */
    extraction(true),
    /**
     *
     */
    edition(false),
    /**
     *
     */
    telechargement(false);
    private final boolean temporal;

    private Activities(boolean temporal) {
        this.temporal = temporal;
    }

    /**
     *
     * @return
     */
    public boolean isTemporal() {
        return temporal;
    }

    private final static List<String> PRIVILEGES = Arrays.asList(values()).stream().map(a -> a.toString()).collect(Collectors.toList());

    /**
     *
     * @param activity
     * @return
     */
    public static Activities convertToActivity(String activity) {
        try {
            return Activities.valueOf(activity.toLowerCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException("Illegal Activity : " + activity, ex);
        }
    }

    /**
     *
     * @param priv
     * @return
     */
    public static boolean isAnActivity(String priv) {
        return PRIVILEGES.contains(priv.trim());
    }

    /**
     *
     * @param activities
     * @return
     */
    public static List<Activities> convertToListActivities(String activities) {

        List<Activities> list = new ArrayList<>();
        if (Strings.isNullOrEmpty(activities)) {
            return list;
        }

        try {
            String[] splitedPrivils = activities.split(PatternConfigurator.PATH_SEPARATOR);

            for (String priv : splitedPrivils) {
                list.add(Activities.convertToActivity(priv.toLowerCase()));
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Illegal Activity List : " + activities, e);
        }

        return list;
    }
}
