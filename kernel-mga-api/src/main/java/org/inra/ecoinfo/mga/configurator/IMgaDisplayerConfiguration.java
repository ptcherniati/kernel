/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.configurator;

import java.util.Map;
import org.inra.ecoinfo.mga.business.composite.INodeable;

/**
 *
 * @author tcherniatinsky
 */
public interface IMgaDisplayerConfiguration {

    /**
     *
     * @param index
     */
    void autoUpdateColumn(int index);

    /**
     *
     * @return
     */
    Map<Class<? extends INodeable>, String[]> getDisplayColumnNames();
    
}
