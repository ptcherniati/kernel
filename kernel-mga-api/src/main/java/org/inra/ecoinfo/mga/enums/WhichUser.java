package org.inra.ecoinfo.mga.enums;

/**
 *
 * @author yahiaoui
 */
public enum WhichUser {

    /**
     *
     */
    CURRENTUSER,
    /**
     *
     */
    SELECTEDUSER;

    /**
     *
     * @param whichUser
     * @return
     */
    public static WhichUser convertToWhichUser(String whichUser) {
        try {
            return WhichUser.valueOf(whichUser.toUpperCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException("Illegal whichUser : " + whichUser, ex);
        }
    }
}
