package org.inra.ecoinfo.mga.business.composite;

import java.time.LocalDate;
import java.util.*;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author ryahiaoui
 */
public class FlatNode implements IFlatNode {

    /**
     *
     */
    public static final int UNCHECKED_STATE = 0;

    /**
     *
     */
    public static final int CHECKED_STATE = 1;

    /**
     *
     */
    public static final int START_DATE_INDEX = 0;

    /**
     *
     */
    public static final int END_DATE_INDEX = 1;

    /**
     *
     */
    public static final int INDETERMINATE_STATE = 2;

    Long id;
    final String code;
    final Long realNode;
    final long nodeable;
    final String name;
    final Long parent;
    final Long ancestor;
    String path;
    final boolean isLeaf;
    final Long leafId;
    final Long realNodeLeafId;
    final String leafNodePath;
    final Long order;

    /* 01/01/0001 for indeterminate Date */
    /**
     *
     */
    protected Map<Activities, Map<Group, List<LocalDate>>> activitiesForAllGroups = new HashMap();

    /**
     *
     */
    protected Map<Activities, List<LocalDate>> ownActivities = new HashMap();

    /**
     *
     */
    protected Map<Activities, Integer> ownRealState = new HashMap();

    /**
     *
     */
    protected Map<Activities, Integer> realState = new HashMap();

    /**
     *
     */
    protected Set<Long> colliderNodes = new HashSet();

    private final Class<Nodeable> typeResource;

    /**
     *
     */
    protected Map<Activities, Map< Integer, List<LocalDate>>> ownTmpState = new HashMap();

    /**
     *
     */
    protected Group ownGroup;

    ;
    /**
     *
     * @param node
     * @param ownGroup
     */
    public FlatNode(INode node, Group ownGroup) {
        this.ownGroup = ownGroup;
        this.id = node.getId();
        this.code = node.getCode();
        this.name = node.getName();
        this.parent = node.getParent() == null ? null : node.getParent().getId();
        this.nodeable = node.getNodeable() == null ? Long.MIN_VALUE : node.getNodeable().getId();
        this.typeResource = node.getNodeable() == null ? null : node.getNodeable().getNodeableType();
        this.ancestor = node.getAncestor() == null ? null : node.getAncestor().getId();
        this.realNode = node.getRealNode() == null ? null : node.getRealNode().getId();
        this.path = node.getPath();
        this.isLeaf = node.isIsLeaf();
        this.colliderNodes = node.getColliderNodes();
        this.leafId = node.getLeaf() == null ? id : node.getLeaf().getId();
        this.realNodeLeafId = node.getLeaf() == null || node.getLeaf().getRealNode() == null ? null : node.getLeaf().getRealNode().getId();
        this.leafNodePath = node.getLeaf() == null ? "" : node.getLeaf().getPath();
        this.order = node.getOrder();
    }

    /**
     *
     * @return
     */
    @Override
    public Long getOrder() {
        return order;
    }

    @Override
    public String toString() {
        return String.format("id = %s -> %s", id, getPath());
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Activities, Integer> getOwnRealState() {
        return ownRealState;
    }

    /**
     *
     * @param ownRealState
     */
    @Override
    public void setOwnRealState(Map<Activities, Integer> ownRealState) {
        this.ownRealState = ownRealState;
    }

    /**
     *
     * @return
     */
    @Override
    public Group getOwnGroup() {
        return ownGroup;
    }

    /**
     *
     * @param ownGroup
     */
    @Override
    public void setOwnGroup(Group ownGroup) {
        this.ownGroup = ownGroup;
    }

    /**
     *
     * @return
     */
    @Override
    public String getLeafNodePath() {
        return leafNodePath;
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Activities, Map<Group, List<LocalDate>>> getActivitiesForAllGroups() {
        return activitiesForAllGroups;
    }

    /**
     *
     * @param activities
     */
    @Override
    public void setActivitiesForAllGroups(Map<Activities, Map<Group, List<LocalDate>>> activities) {
        this.activitiesForAllGroups = activities;
    }

    /**
     *
     * @param ownActivities
     */
    @Override
    public void setOwnActivities(Map<Activities, List<LocalDate>> ownActivities) {
        this.ownActivities = ownActivities;
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Activities, Integer> getRealState() {
        return realState;
    }

    /**
     *
     * @param realState
     */
    @Override
    public void setRealState(Map<Activities, Integer> realState) {
        this.realState = realState;
    }

    /**
     *
     * @return
     */
    @Override
    public Set<Long> getColliderNodes() {
        return colliderNodes;
    }

    /**
     *
     * @param colliderNodes
     */
    @Override
    public void setColliderNodes(Set<Long> colliderNodes) {
        this.colliderNodes = colliderNodes;
    }

    /**
     *
     * @param id
     */
    @Override
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    @Override
    public Long getRealNodeId() {
        return realNode;
    }

    /**
     *
     * @return
     */
    @Override
    public long getNodeableId() {
        return nodeable;
    }

    /**
     *
     * @return
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     */
    @Override
    public Long getParent() {
        return parent;
    }

    /**
     *
     * @return
     */
    @Override
    public Long getAncestorId() {
        return ancestor;
    }

    /**
     *
     * @return
     */
    @Override
    public String getPath() {
        return path;
    }

    /**
     *
     * @param path
     */
    @Override
    public void setPath(String path) {
        this.path = path;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getTypeResource() {
        return (Class<T>) typeResource;
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Activities, List<LocalDate>> getOwnActivities() {
        return ownActivities;
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Activities, Map<Integer, List<LocalDate>>> getOwnTmpState() {
        return ownTmpState;
    }

    /**
     *
     * @return
     */
    @Override
    public String getCode() {
        return code;
    }

    /**
     *
     * @param activity
     * @param whichDate
     * @return
     */
    @Override
    public String dateAsString(Activities activity, int whichDate) {
        if (START_DATE_INDEX != whichDate && END_DATE_INDEX != whichDate) {
            return PatternConfigurator.EMPTY_STRING;
        }
        List<LocalDate> dates = Optional.ofNullable(ownTmpState.get(activity))
                .map(activityState -> (Integer) activityState.keySet().toArray()[0])
                .map(state -> ownTmpState.get(activity).get(state))
                .orElse(
                        Optional.ofNullable(ownActivities)
                                .map(ots -> ots.get(activity))
                                .orElse(null)
                );

        if (!CollectionUtils.isEmpty(dates) && dates.get(whichDate) != null) {
            final String utcDateTextFromLocalDate = DateUtil.getUTCDateTextFromLocalDateTime(dates.get(whichDate).atStartOfDay(), DateUtil.DD_MM_YYYY);
            return DateUtil.MIN_LOCAL_DATE.equals(dates.get(whichDate))
                    ? PatternConfigurator.INVALIDATE_DATE_STRING
                    : utcDateTextFromLocalDate;
        }
        return PatternConfigurator.EMPTY_STRING;
    }

    /**
     *
     * @param activity
     * @return
     */
    @Override
    public String dateBegAsString(Activities activity) {
        return dateAsString(activity, START_DATE_INDEX);
    }

    /**
     *
     * @param activity
     * @return
     */
    @Override
    public String dateEndAsString(Activities activity) {
        return dateAsString(activity, END_DATE_INDEX);
    }

    /**
     *
     * @param o
     */
    public void setDateAsString(Object o) {
        //nothing to do
    }

    /**
     *
     * @param p
     * @return
     */
    @Override
    public boolean hasActivity(Activities p) {
        return realState.entrySet().stream()
                .anyMatch(entry -> entry.getKey().equals(p) && entry.getValue() != 0);
    }

    /**
     *
     * @param activity
     * @param datesMap
     */
    @Override
    public void addActivity(Activities activity, Map<Group, List<LocalDate>> datesMap) {
        datesMap.entrySet().stream()
                .forEach((entryActivity) -> {
                    List<LocalDate> dates = Optional.ofNullable(entryActivity.getValue())
                            .orElse(Arrays.asList(new LocalDate[]{null, null}));
                    this.activitiesForAllGroups
                            .computeIfAbsent(activity, k -> new HashMap<>())
                            .put(entryActivity.getKey(), dates);
                });
    }

    /**
     *
     * @param processName
     * @param dates
     */
    @Override
    public void addOwnActivity(Activities processName, List<LocalDate> dates) {
        if (dates == null) {
            this.addOwnActivity(processName, Arrays.asList(new LocalDate[]{null, null}));
        } else {
            this.ownActivities.put(processName, dates);
        }
    }

    /**
     *
     * @param activity
     * @return
     */
    @Override
    public int copyRealToTmpState(Activities activity) {
        List<LocalDate> dates = Optional.ofNullable(getActivitiesForAllGroups())
                .map(a -> a.get(activity))
                .map(m -> m.get(getOwnGroup()))
                .orElse(Arrays.asList(new LocalDate[]{null, null}));
        getOwnTmpState().computeIfAbsent(activity, k -> new HashMap<>())
                .put(getRealState(activity), dates);
        return getRealState(activity);
    }

    /**
     *
     * @param priv
     * @return
     */
    @Override
    public int getRealState(Activities priv) {
        return realState.getOrDefault(priv, 0);
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Activities, Map<Long, List<LocalDate>>> getOwnCheckedActivities() {

        Map<Activities, Map<Long, List<LocalDate>>> returnMap = new HashMap<>();
        Set<Activities> lTmpStates = new HashSet<>(ownTmpState.keySet());
        Set<Activities> lOwnRealStates = new HashSet<>(ownRealState.keySet());

        lOwnRealStates.removeAll(lTmpStates);

        lOwnRealStates.stream()
                .filter(notTmpActivity -> ownRealState.get(notTmpActivity) == CHECKED_STATE)
                .peek(notTmpActivity -> returnMap.computeIfAbsent(notTmpActivity, k -> new HashMap()).put(id, ownActivities.get(notTmpActivity)))
                .forEach((notTmpActivity) -> propageOwnRealActivitiesToColliders(notTmpActivity, returnMap));
        lTmpStates.stream()
                .filter(tmpActivity -> ownTmpState.get(tmpActivity).get(CHECKED_STATE) != null)
                .peek(tmpActivity -> returnMap.computeIfAbsent(tmpActivity, k -> new HashMap()).put(id, ownTmpState.get(tmpActivity).get(CHECKED_STATE)))
                .forEach((tmpActivity) -> propageTmpActivitiesToColliders(tmpActivity, returnMap));

        return returnMap;
    }

    private void propageOwnRealActivitiesToColliders(Activities activity, Map<Activities, Map<Long, List<LocalDate>>> activitiesMap) {
        getColliderNodes().stream()
                .forEach(colliderId -> activitiesMap.get(activity).put(colliderId, ownActivities.get(activity)));
    }

    private void propageTmpActivitiesToColliders(Activities activity, Map<Activities, Map<Long, List<LocalDate>>> activitiesMap) {
        getColliderNodes().stream()
                .forEach(colliderId -> activitiesMap.get(activity).put(colliderId, ownTmpState.get(activity).get(CHECKED_STATE)));
    }

    /**
     *
     * @param priv
     * @param state
     */
    @Override
    public void addRealState(Activities priv, Integer state) {
        this.realState.put(priv, state);
    }

    /**
     *
     * @param priv
     * @param state
     */
    @Override
    public void addOwnRealState(Activities priv, Integer state) {
        this.ownRealState.put(priv, state);
    }

    /**
     *
     * @param privs
     * @param state
     */
    @Override
    public void addRealStates(List<Activities> privs, Integer state) {
        privs.stream().forEach(priv -> this.realState.put(priv, state));
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isIsLeaf() {
        return isLeaf;
    }

    /**
     *
     * @param activities
     * @return
     */
    @Override
    public boolean hasAllActivities(List<Activities> activities) {
        return activities.stream().noneMatch((p) -> (!this.hasActivity(p)));
    }

    /**
     *
     * @param privils
     * @return
     */
    @Override
    public boolean hasAtLeastOneActivity(List<Activities> privils) {
        return privils.stream().anyMatch((p) -> this.hasActivity(p));
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Activities, Integer> getActivatedActivities() {

        Map<Activities, Integer> activityMap = new HashMap();

        realState.keySet().stream()
                .forEach((pr) -> {
                    int localRealState = getRealState(pr);
                    if (localRealState == CHECKED_STATE || localRealState == INDETERMINATE_STATE) {
                        activityMap.put(pr, localRealState);
                    }
                });
        return activityMap;
    }

    /**
     *
     * @return
     */
    @Override
    public Long getLeafId() {
        return leafId;
    }

    /**
     *
     * @return
     */
    @Override
    public Long getRealNodeLeafId() {
        return realNodeLeafId;
    }
}
