/**
 *
 * @author yahiaoui
 */
package org.inra.ecoinfo.mga.business.composite;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author yahiaoui
 */
@Entity
@Table(name = NodeDataSet.tableName)
public class NodeDataSet extends AbstractBranchNode<NodeDataSet> implements INode<NodeDataSet>, Serializable {

    /**
     *
     */
    public static final String tableName = "composite_node_data_set";

    /**
     *
     * @param parentNode
     * @param ancestorNode
     */
    public NodeDataSet(NodeDataSet parentNode, NodeDataSet ancestorNode) {
        super(parentNode, ancestorNode);
    }

    /**
     *
     */
    public NodeDataSet() {
    }
}
