package org.inra.ecoinfo.mga.enums;

/**
 *
 * @author ryahiaoui
 */
public enum ActivityCondition {

    /**
     *
     */
    ALL,
    /**
     *
     */
    ATLEAST;

    /**
     *
     * @param activityCondition
     * @return
     */
    public static ActivityCondition convertToActivityCondition(String activityCondition) {
        try {
            return ActivityCondition.valueOf(activityCondition.toUpperCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException("Illegal Activity Cond : " + activityCondition, ex);
        }
    }
}
