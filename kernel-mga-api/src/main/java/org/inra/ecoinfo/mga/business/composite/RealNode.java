package org.inra.ecoinfo.mga.business.composite;

//~--- non-JDK imports --------------------------------------------------------

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import static javax.persistence.CascadeType.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import static org.inra.ecoinfo.mga.business.composite.RealNode.*;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;

/**
 *
 * @author Rac021
 */
@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(name = "real_node_nk", columnNames = {RealNode.RECURENT_NAME_NODEABLE_ID, RealNode.RECURENT_NAME_PARENT_ID}),
    @UniqueConstraint(name = "real_node_path", columnNames = {RealNode.NAME_PATH})},
        indexes = {
            @Index(name = "realNode_parent_idx", columnList = RECURENT_NAME_PARENT_ID)
            ,
            @Index(name = "realNode_ancestor_idx", columnList = RECURENT_NAME_ANCESTOR_ID)
            ,
            @Index(name = "realNode_nodeable_idx", columnList = RECURENT_NAME_NODEABLE_ID)
            ,
            @Index(name = "realNode_path_idx", columnList = "path")
        })
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "tree")
public class RealNode implements Serializable, Comparable<RealNode>{

    /**
     * The Constant RECURENT_NAME_COLUMN_PARENT_ID @link(String).
     */
    /**
     *
     */
    public static final String ID_JPA = "id";

    /**
     *
     */
    public static final String RECURENT_NAME_COLUMN_PARENT_ID = "parent";

    /**
     *
     */
    public static final String RECURENT_NAME_ID = ID_JPA;

    /**
     *
     */
    public static final String RECURENT_NAME_PARENT_ID = "id_parent_node";

    /**
     *
     */
    public static final String RECURENT_NAME_NODEABLE_ID = "id_nodeable";

    /**
     *
     */
    public static final String RECURENT_NAME_ANCESTOR_ID = "id_ancestor";

    /**
     *
     */
    public static final String NAME_PATH = "path";

    /**
     *
     */
    @Id
    @Column(name = RealNode.RECURENT_NAME_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    //@GeneratedValue (strategy = GenerationType.TABLE, generator = "nodeIdGenerator")

    /**
     *
     */
    protected long id;

    /**
     * The parent @link(T).
     */
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = true)
    @JoinColumn(name = RealNode.RECURENT_NAME_PARENT_ID, referencedColumnName = RealNode.RECURENT_NAME_ID, nullable = true
    )
    @Fetch(FetchMode.SELECT)
    protected RealNode parent;

    /**
     *
     */
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = RealNode.RECURENT_NAME_ANCESTOR_ID, referencedColumnName = RealNode.RECURENT_NAME_ID, nullable = true
    )
    @Fetch(FetchMode.SELECT)
    protected RealNode ancestor;

    /**
     *
     */
    @OneToOne(cascade = {MERGE, PERSIST, REFRESH},fetch = FetchType.EAGER)
    @JoinColumn(name = RECURENT_NAME_NODEABLE_ID)
    @Fetch(FetchMode.SELECT)
    protected Nodeable nodeable;

    @Column(name = NAME_PATH, nullable = false)
    private String path;


    @Transient
    RealNode leaf;

    /**
     *
     */
    @Transient
    @OneToMany(orphanRemoval = true, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinColumn(name = AbstractBranchNode.REAL_NODE_NAME)
    protected Set<AbstractBranchNode> colliderNodes = new HashSet();

    /**
     *
     */
    public RealNode() {
    }

    /**
     *
     * @param parent
     * @param ancestor
     * @param nodeable
     * @param path
     */
    public RealNode(RealNode parent, RealNode ancestor, INodeable nodeable, String path) {
        this.nodeable = (Nodeable) nodeable;
        this.ancestor = ancestor;
        this.parent = parent;
        this.path = path;
    }

    /**
     *
     * @param nodeable
     */
    public RealNode(INodeable nodeable) {
        this.nodeable = (Nodeable) nodeable;
    }

    /**
     *
     * @param path
     */
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + getPath().hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RealNode other = (RealNode) obj;
        return getPath().equals(other.getPath());
    }

    /**
     *
     * @return
     */
    public RealNode getAncestor() {
        return ancestor;
    }

    /**
     *
     * @return
     */
    public RealNode getParent() {
        return parent;
    }

    /**
     *
     * @param parent
     */
    public void setParent(RealNode parent) {
        this.parent = parent;
    }

    /**
     *
     * @return
     */
    public Set<AbstractBranchNode> getColliderNodes() {
        return colliderNodes;
    }

    /**
     *
     * @param colliderNodes
     */
    public void setColliderNodes(Set<AbstractBranchNode> colliderNodes) {
        this.colliderNodes = colliderNodes;
    }

    /**
     *
     * @param ancestor
     */
    public void setAncestor(RealNode ancestor) {
        this.ancestor = ancestor;
    }

    /**
     *
     * @param node
     */
    public void addColliderNode(INode node) {
        this.colliderNodes.add((AbstractBranchNode) node);
    }

    /**
     * Setters And Getters
     *
     * @return
     */
    public INodeable getNodeable() {
        return nodeable;
    }

    /**
     *
     * @param nodeable
     */
    public void setNodeable(INodeable nodeable) {
        this.nodeable = (Nodeable) nodeable;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return nodeable.getName();
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return getNodeable().getCode();
    }

    /**
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getDepositPlacePrefixForFileName() {

        String placePrefix = PatternConfigurator.EMPTY_STRING;
        if (this.nodeable != null) {
            if (this.parent != null) {
                placePrefix += this.parent.getClass().getSimpleName() + PatternConfigurator.UNDERSCORE
                        + this.nodeable.getNodeableType().getSimpleName();

                return placePrefix;
            }

            return this.nodeable.getClass().getSimpleName();
        }

        return PatternConfigurator.UNDERSCORE;
    }

    /**
     *
     * @param <T>
     * @param nodeableType
     * @return
     */
    public <T extends INodeable> RealNode getNodeByNodeableTypeResource(Class<T> nodeableType) {
        if (nodeable != null && this.getNodeable().getNodeableType().equals(nodeableType)) {
            return this;
        }

        RealNode parentNode = this.getParent();

        while (parentNode != null) {
            if (nodeableType.isInstance(parentNode.getNodeable())) {
                return parentNode;
            } else {
                parentNode = parentNode.getParent();
            }
        }

        return null;

    }

    /**
     *
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @return
     */
    public String getPathIncludeAncestorPath() {

        String path = PatternConfigurator.EMPTY_STRING;

        if (this.parent != null) {
            if (this.ancestor != null) {
                path += this.parent.getPathIncludeAncestorPath()
                        + PatternConfigurator.PATH_SEPARATOR
                        + this.ancestor.getPathIncludeAncestorPath()
                        + PatternConfigurator.ANCESTOR_SEPARATOR
                        + this.nodeable.getName();
            } else {
                path += this.parent.getPathIncludeAncestorPath()
                        + PatternConfigurator.PATH_SEPARATOR + this.nodeable.getName();
            }
        } else if (this.ancestor != null) {
            path += this.ancestor.getPathIncludeAncestorPath()
                    + PatternConfigurator.ANCESTOR_SEPARATOR
                    + this.nodeable.getName();
        } else {
            path += this.nodeable.getName();
        }

        return path;
    }

    /**
     *
     * @return
     */
    public RealNode getLeaf() {
        return leaf;
    }

    @Override
    public String toString() {
        return getPath();
    }

    @Override
    public int compareTo(RealNode t) {
        if(t==null){
            return -1;
        }
        return path.compareTo(t.path);
    }

}
