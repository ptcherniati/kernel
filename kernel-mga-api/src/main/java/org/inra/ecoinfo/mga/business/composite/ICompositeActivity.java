package org.inra.ecoinfo.mga.business.composite;

import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;
import javax.persistence.PostLoad;
import org.inra.ecoinfo.mga.enums.Activities;

/**
 *
 * @author ryahiaoui
 */
public interface ICompositeActivity extends Serializable {

    /**
     *
     * @throws DateTimeParseException
     * @throws IOException
     */
    @PostLoad
    void initMap() throws DateTimeParseException, IOException;

    /**
     *
     * @return
     */
    String getActivityAsString();

    /**
     *
     * @param activity
     */
    void setActivityAsString(String activity);

    /**
     *
     * @return
     */
    Map<Activities, Map<Long, List<LocalDate>>> getActivitiesMap();

    /**
     *
     * @param activitiesMap
     */
    void setActivitiesMap(Map<Activities, Map<Long, List<LocalDate>>> activitiesMap);

    /**
     *
     */
    void synchronizeActivitiesMap();

    /**
     *
     */
    void synchronizeActivityAsString();

}
