/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.middleware;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.WhichTree;

/**
 *
 * @author tcherniatinsky
 */
public interface ICompositeGroupDAO {

    /**
     *
     * @param <T>
     * @return
     */
    <T extends ICompositeGroup> List<T> retrieveAllUsers();

    /**
     *
     * @param whichTree
     * @return
     */
    List<ICompositeGroup> retrieveAllUsersOrGroups(WhichTree whichTree);

    /**
     *
     * @param user
     * @return
     */
    Map<Group, Boolean> getAllPossiblesGroupUser(IUser user);

    /**
     *
     * @param <T>
     * @param user
     * @param group
     * @return
     */
    <T extends ICompositeGroup> Optional<T> addGroupToUser(T user, Group group);

    /**
     *
     * @param <T>
     * @param user
     * @param group
     * @return
     */
    <T extends ICompositeGroup> Optional<T> removeGroupOfUser(T user, Group group);

    /**
     *
     * @param group
     */
    void remove(ICompositeGroup group);

}
