package org.inra.ecoinfo.mga.caching;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;

/**
 *
 * @author ryahiaoui
 */
public interface ITreeApplicationCacheManager extends Serializable{

    /**
     *
     */
    void initMap();

    /**
     *
     * @param key
     * @return
     */
    ITreeNode getSkeletonTree(Integer key);

    /**
     *
     * @param config
     * @param leafs
     * @return
     */
    ITreeNode buildSkeletonTree(IMgaIOConfiguration config, Stream<INode> leafs, boolean caching);

    /**
     *
     * @param mgaIOConfigurator
     */
    void setMgaIOConfigurator(IMgaIOConfigurator mgaIOConfigurator);

    /**
     *
     * @return
     */
    IMgaIOConfigurator getMgaIOConfigurator();

    /**
     *
     * @param key
     */
    void removeSkeletonTree(Integer key );
    void addSkeletonTree(Integer key, ITreeNode treeNode);
    
    /**
     *
     */
    void clearSkeletons();
    
    /**
     *
     * @return
     */
    int size(); 

    Map<String, List<Integer>> getConfigurationMap();
    Instant getTimeStamp(int key);

}
