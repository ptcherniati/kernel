/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.managedbean;

import java.io.Serializable;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.INodeable;

/**
 *
 * @author tcherniatinsky
 */
public interface ILocalizationEntitiesManager extends Serializable{

    /**
     *
     * @param nodeable
     * @return
     */
    String getLocalName(INodeable nodeable);

    /**
     *
     * @param flatNode
     * @return
     */
    String getLocalName(IFlatNode flatNode);
}
