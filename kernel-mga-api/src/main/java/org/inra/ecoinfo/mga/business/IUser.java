/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business;

import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;

/**
 *
 * @author tcherniatinsky
 */
public interface IUser extends ICompositeGroup {

    /**
     *
     * @return
     */
    Boolean getIsRoot();

    /**
     *
     * @return
     */
    String getLogin();

    /**
     *
     * @return
     */
    String getEmail();

    /**
     *
     * @return
     */
    @Override
    Long getId();

    /**
     *
     * @return
     */
    String getLanguage();

    /**
     *
     * @return
     */
    String getNom();

    /**
     *
     * @return
     */
    String getPassword();

    /**
     *
     * @return
     */
    String getPrenom();

}
