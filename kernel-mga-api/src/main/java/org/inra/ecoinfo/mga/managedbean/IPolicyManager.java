package org.inra.ecoinfo.mga.managedbean;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.inra.ecoinfo.mga.business.IMgaServiceBuilder;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.*;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.ActivityCondition;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author yahiaoui
 */
public interface IPolicyManager extends Serializable {

    /**
     *
     * @return
     */
    IMgaServiceBuilder getMgaServiceBuilder();

    /**
     *
     * @param login
     * @param codeConfiguration
     * @param priv
     * @param source
     * @return
     */
    boolean checkActivity(ICompositeGroup login, Integer codeConfiguration, String priv, String source);

    /**
     *
     * @param codeConfiguration
     * @param patternActivity
     * @param ressource
     * @return
     */
    boolean checkCurrentUserActivity(Integer codeConfiguration, String patternActivity, String ressource);

    /**
     *
     * @param compositeGroup
     * @param activity
     * @param whichTree
     * @return
     */
    boolean hasActivity(ICompositeGroup compositeGroup, String activity, WhichTree whichTree);

    // Nodes 
    /**
     *
     * @param currentTreeNode
     * @return
     */
    Set<Long> getAllTreeNodesIds(ITreeNode currentTreeNode);

    /**
     *
     * @param <T>
     * @param root
     * @param id
     * @return
     */
    <T extends IDataNode> T getNodeById(ITreeNode<T> root, Long id);

    /**
     *
     * @param <T>
     * @param root
     * @param id
     * @return
     */
    <T extends IDataNode> Optional<ITreeNode<T>> getTreeNodeById(ITreeNode<T> root, Long id);

    /**
     *
     * @param code
     * @param whichTree
     * @return
     */
    Optional<INode> getNodeByNodeableCode(String code, WhichTree whichTree);

    /**
     *
     * @param code
     * @param whichTree
     * @return
     */
    Stream<INode> getNodesByNodeableCode(String code, WhichTree whichTree);

    /**
     *
     * @param compositeGroup
     * @param whichTree
     * @return
     */
    Set<String> getOnlyNamesOfNodesWhichHaveAtLeastOneActivity(ICompositeGroup compositeGroup, WhichTree whichTree);

    // Users
    /**
     *
     * @return
     */
    boolean isRoot();

    /**
     *
     * @return
     */
    String getCurrentUserLogin();

    /**
     *
     * @return
     */
    IUser getCurrentUser();

    /**
     *
     * @param currentUser
     */
    void setCurrentUser(IUser currentUser);

    /**
     *
     * @param codeConfiguration
     * @return
     */
    List retrieveAllUsers(Integer codeConfiguration);

    /**
     *
     * @param codeConfiguration
     * @return
     */
    List retrieveAllUserOrGroups(Integer codeConfiguration);

    /**
     *
     * @param tree
     * @param activityUser
     * @param conditionPrivil
     * @param priv
     * @return
     */
    ITreeNode restrictTree(ITreeNode tree,
            Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activityUser,
            ActivityCondition conditionPrivil,
            List<Activities> priv);

    /**
     *
     * @param tree
     * @param conditionPrivilRestriction
     * @param priv
     * @return
     */
    ITreeNode<IFlatNode> restrictTreeOnLeafsAccordingActivities(ITreeNode<IFlatNode> tree, ActivityCondition conditionPrivilRestriction, List<Activities> priv);

    /**
     *
     * @param compositeGroup
     * @param whichTree
     * @return
     */
    Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> getOrLoadActivities(ICompositeGroup compositeGroup, WhichTree whichTree);

    /**
     *
     * @param compositeActivity
     * @param whichTree
     */
    void removeActivity(ICompositeActivity compositeActivity, WhichTree whichTree);

    /**
     *
     * @param group
     * @param whichTree
     * @param role
     */
    void updateActivities(Group group, WhichTree whichTree, Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role);

    /**
     *
     * @param r
     */
    void mergeActivity(ICompositeActivity r);

    /**
     *
     * @param r
     */
    void persistActivity(ICompositeActivity r);

    /**
     *
     * @param login
     * @param extractActivity
     */
    void persistExtractActivity(String login, Map<Long, List<LocalDate>> extractActivity);

    /**
     *
     * @param login
     */
    void removeExtractActivity(String login);
    // Context Releaser

    /**
     *
     */
    void releaseContext();

    /**
     *
     * @param listChild
     */
    void persistNodes(Stream<INode> listChild);

    /**
     *
     * @param <T>
     * @param root
     * @param filter
     * @return
     */
    <T extends IDataNode> Optional<ITreeNode<T>> getTreeNodeByFilterPredicate(ITreeNode<T> root, Predicate<ITreeNode<T>> filter);

    /**
     *
     * @param compositeGroup
     * @param codeConfiguration
     */
    void removeTreeFromSession(ICompositeGroup compositeGroup, Integer codeConfiguration);

    /**
     *
     */
    void clearTreeFromSession();

    /**
     *
     * @param selectdUserOrGroup
     * @param codeConfiguration
     * @return
     */
    ITreeNode<IFlatNode> getOrLoadTree(ICompositeGroup selectdUserOrGroup, Integer codeConfiguration);

    /**
     *
     * @param groupName
     * @param whichTree
     * @return
     */
    Optional<Group> getGroupByGroupNameAndWhichTree(String groupName, WhichTree whichTree);

    /**
     *
     * @param compositeGroup
     * @param whichTree
     * @return
     */
    List<Group> getGroups(ICompositeGroup compositeGroup, WhichTree whichTree);

    /**
     *
     * @param user
     * @return
     */
    Map<Group, Boolean> getAllPossiblesGroupUser(IUser user);

    /**
     *
     * @param <T>
     * @param user
     * @param group
     * @return
     */
    <T extends IUser> Optional<T> addGroupToUser(T user, Group group);

    /**
     *
     * @param <T>
     * @param user
     * @param group
     * @return
     */
    <T extends IUser> Optional<T> removeGroupOfUser(T user, Group group);

    /**
     *
     * @param _groupName
     * @param whichTree
     * @param groupType
     * @return
     */
    BusinessException addGroup(String _groupName, WhichTree whichTree, GroupType groupType);

    /**
     *
     * @param _groupName
     * @param whichTree
     * @return
     */
    BusinessException removeGroup(String _groupName, WhichTree whichTree);
}
