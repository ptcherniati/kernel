package org.inra.ecoinfo.mga.business.composite;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Proxy;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Rac021
 */
@Entity(name = "Nodeable")
@Table(name = Nodeable.TABLE_NAME, uniqueConstraints = 
        @UniqueConstraint(name = "unique_code_DBTYPE_UK", columnNames = {Nodeable.RECURENT_NAME_DTYPE, Nodeable.RECURENT_NAME_CODE}))
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = Nodeable.RECURENT_NAME_DTYPE, discriminatorType = DiscriminatorType.STRING)
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "tree")
@Proxy(lazy = true)
public abstract class Nodeable implements Serializable, INodeable {
    public static  final Logger LOGGER = LoggerFactory.getLogger(Nodeable.class);

    /**
     *
     */
    public final static String RECURENT_NAME_CODE = "code";

    /**
     *
     */
    public final static String RECURENT_NAME_DTYPE = "dtype";

    /**
     *
     */
    public final static String ENTITE_COLUMN_NAME = "name";

    /**
     *
     */
    public static final String TABLE_NAME = "composite_nodeable";

    /**
     *
     * @param clazz
     * @return
     */
    public static String getLocalisationEntite(Class clazz) {
        return clazz.getSimpleName().toLowerCase();
    }
    @Id
    @Column(nullable = false, name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    /**
     * The code @link(String).b
     */
    @Column(name = Nodeable.RECURENT_NAME_CODE, nullable = false)
    protected String code = "";
    
    /**
     *
     */
    @GeneratedValue
    @Column(name = "orderNumber", nullable = true)
    protected Long order;

    /**
     *
     */
    public Nodeable() {
    }

    /**
     *
     */
    public Nodeable(String code) {
        this.code=code;
    }

    /**
     *
     * @return
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     *
     * @param idNodeable
     */
    @Override
    public void setId(Long idNodeable) {
        this.id = idNodeable;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Nodeable other = (Nodeable) obj;
        return Objects.equals(this.code, other.code);
    }

    /**
     *
     * @return
     */
    @Override
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    @Override
    public String getUniqueCode() {
        return getClass().getSimpleName()
                + PatternConfigurator.ANCESTOR_SEPARATOR
                + getCode();

    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    @Override
    public Long getOrder() {
        return order;
    }

    /**
     *
     * @param order
     */
    public void setOrder(Long order) {
        this.order = order;
    }

}
