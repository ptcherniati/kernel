/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business.composite;

import org.inra.ecoinfo.utils.IFormatter;

/**
 *
 * @author tcherniatinsky
 */
public interface INodeNameFormatter extends IFormatter<INode> {

    @Override
    default String format(INode toFormat, Object... arguments) {
        return toFormat.getName();
    }

}
