/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business.composite;

import java.time.LocalDate;
import java.util.*;
import java.util.regex.Pattern;
import javax.persistence.*;
import static javax.persistence.CascadeType.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import static org.inra.ecoinfo.mga.business.composite.AbstractBranchNode.*;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.enums.Activities;

/**
 *
 * @author tcherniatinsky
 * @param <T>
 */
@MappedSuperclass
@Table(
        indexes = {
            @Index(name = "abn_parent__idx", columnList = RECURENT_NAME_PARENT_ID)
            ,
           @Index(name = "abn_ancestor__idx", columnList = RECURENT_NAME_ANCESTOR_ID)
            ,
           @Index(name = "abn_realNode__idx", columnList = REAL_NODE_NAME)
        })
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "tree")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractBranchNode<T extends AbstractBranchNode> implements INode<T>, Comparable<AbstractBranchNode> {

    /**
     *
     */
    public static final String ID_JPA = "branch_node_id";

    /**
     *
     */
    public static final String REAL_NODE_NAME = "realNode";

    /**
     *
     */
    public static final String RECURENT_NAME_ID = ID_JPA;

    /**
     *
     */
    public static final String COLLIDER_NODE_NAME = "colliderNodes";

    /**
     *
     */
    public static final String RECURENT_NAME_PARENT_ID = "id_parent_node";

    /**
     *
     */
    public static final String RECURENT_NAME_ANCESTOR_ID = "id_ancestor";

    /**
     *
     */
    @Id
    @Column(name = AbstractBranchNode.ID_JPA)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected long id;

    /**
     * The parent @link(T).
     */
    @OneToOne(cascade = CascadeType.ALL, optional = true)
    @JoinColumn(name = AbstractBranchNode.RECURENT_NAME_PARENT_ID, referencedColumnName = AbstractBranchNode.RECURENT_NAME_ID, nullable = true)
    @Fetch(FetchMode.SELECT)
    protected T parent;

    /**
     *
     */
    @OneToOne(cascade = CascadeType.ALL, optional = true)
    @JoinColumn(name = RECURENT_NAME_ANCESTOR_ID, referencedColumnName = AbstractBranchNode.RECURENT_NAME_ID, nullable = true)
    @Fetch(FetchMode.SELECT)
    protected T ancestor;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH},optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = REAL_NODE_NAME, nullable = false)
    @Fetch(FetchMode.SELECT)
    RealNode realNode;

    int index = 0;

    /**
     *
     */
    @Transient
    protected boolean isAncestor;

    /* 0 = False; 1 = True; 2 = Indeterminate */
    /**
     *
     */
    @Transient
    protected Map<Activities, Integer> realState;

    /**
     *
     */
    @Transient
    protected Map<Activities, List<LocalDate>> activities;

    /**
     *
     */
    @Transient
    protected Map<Activities, Map< Integer, List<LocalDate>>> tmpState = Collections.EMPTY_MAP;

    /**
     *
     */
    @Transient
    protected boolean conflictState;

    /**
     *
     */
    @Transient
    protected String formatter;

    /**
     *
     */
    @Transient
    protected Set<Long> colliderNodes = new HashSet();

    /**
     *
     */
    @Transient
    protected boolean isLeaf = false;

    @Transient
    INode leaf;

    /**
     *
     * @param parentNode
     * @param ancestorNode
     */
    public AbstractBranchNode(T parentNode, T ancestorNode) {
        this.ancestor = ancestorNode;
        this.parent = parentNode;
    }

    /**
     *
     */
    public AbstractBranchNode() {
    }

    /**
     *
     * @param ancestor
     */
    @Override
    public void setAncestor(T ancestor) {
        this.ancestor = ancestor;
    }

    /**
     *
     * @return
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    @Override
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    @Override
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    @Override
    public void setRealNode(RealNode realNode) {
        if (this.realNode != null && this.realNode.colliderNodes.contains(this)) {
            this.realNode.colliderNodes.remove(this);
        }
        this.realNode = realNode;
        realNode.getColliderNodes().add(this);
        index = index <= 0
                ? realNode.getColliderNodes().stream()
                        .max(Comparator.comparing(t -> t.index))
                        .map(n -> n.index + 1)
                        .orElse(index)
                : index;
        realNode.ancestor = ancestor == null ? null : ancestor.getRealNode();
        realNode.parent = parent == null ? null : parent.getRealNode();

    }

    /**
     *
     * @return
     */
    public String getFormatter() {
        return formatter;
    }

    /**
     *
     * @param formatter
     */
    public void setFormatter(String formatter) {
        this.formatter = formatter;
    }

    /**
     *
     * @return
     */
    public int getIndex() {
        return index;
    }

    /**
     *
     * @param index
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     *
     */
    @PostLoad
    protected void initCollections() {
        this.activities = new HashMap<>();
        this.realState = new HashMap<>();
        this.tmpState = new HashMap<>();
        this.formatter = PatternConfigurator.dateFormat;
    }

    /**
     *
     * @return
     */
    @Override
    public int depth() {
        return getPath().split(PatternConfigurator.PATH_SEPARATOR).length - 1;
    }

    /**
     *
     * @return
     */
    @Override
    public int depthIncludeAncestor() {
        return getPathIncludeAncestorPath().
                replace(PatternConfigurator.PATH_SEPARATOR, Pattern.quote("*--*")).
                replace(PatternConfigurator.ANCESTOR_SEPARATOR, Pattern.quote("*--*")).
                split(Pattern.quote("*--*")).length - 1;
    }

    /**
     *
     * @return
     */
    @Override
    public Set<Long> getColliderNodes() {
        return colliderNodes;
    }

    /**
     *
     * @param colliderNodes
     */
    @Override
    public void setColliderNodes(Set<AbstractBranchNode> colliderNodes) {
        realNode.colliderNodes = colliderNodes;
    }

//    /**
//     *
//     * @param p
//     * @return
//     */
//    @Override
//    public boolean hasActivity(Activities p) {
//        if (tmpState.containsKey(p)) {
//            return new ArrayList<>(this.tmpState.get(p).keySet()).get(0) == 1;
//        }
//        return this.activities.keySet().contains(p);
//    }
//
//    /**
//     *
//     * @param p
//     * @return
//     */
//    @Override
//    public boolean hasActivity(String p) {
//        return hasActivity(Activities.convertToActivity(p));
//    }
//
//    /**
//     *
//     * @param privils
//     * @return
//     */
//    @Override
//    public boolean hasAtLeastOneActivity(List<Activities> privils) {
//        return privils.stream().anyMatch((p) -> (this.hasActivity(p))
//        );
//    }
//
//    /**
//     *
//     * @param privils
//     * @return
//     */
//    @Override
//    public boolean hasAllActivities(List<Activities> privils) {
//        return privils.stream().noneMatch((p) -> (!this.hasActivity(p)));
//    }
//
//    /**
//     *
//     * @return
//     */
//    @Override
//    public Map<Activities, Integer> getActivatedActivities() {
//
//        Map<Activities, Integer> priv = new HashMap();
//
//        realState.keySet().stream().forEach((pr) -> {
//            int localRealState = getRealState(pr);
//            if (localRealState == 1 || localRealState == 2) {
//                priv.put(pr, localRealState);
//            }
//        });
//        return priv;
//    }
    /**
     *
     * @return
     */
    @Override
    public boolean isIsLeaf() {
        return isLeaf;
    }

    /**
     *
     * @param isLeaf
     */
    @Override
    public void setIsLeaf(boolean isLeaf) {
        this.isLeaf = isLeaf;
    }

    /**
     *
     * @return
     */
    @Override
    public T getParent() {
        return parent;
    }

    /**
     *
     * @param parent
     */
    @Override
    public void setParent(T parent) {
        this.parent = parent;
    }

    /**
     *
     * @return
     */
    @Override
    public T getRoot() {

        if (this.parent != null) {
            return (T) parent.getRoot();
        }

        return (T) this;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.getNodeable().getClass());
        hash = 97 * hash + Objects.hashCode(this.getNodeable().getCode());
        hash = 97 * hash + this.index;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractBranchNode other = (AbstractBranchNode) obj;
        return Objects.equals(this.getNodeable(), other.getNodeable());
    }

    /**
     *
     * @param set
     * @return
     */
    @Override
    public ArrayList<T> scanNodes(ArrayList<T> set) {

        set.add(0, (T) this);

        if (this.parent != null) {
            this.parent.scanNodes(set);
        }

        return set;
    }

    /**
     *
     * @param set
     * @return
     */
    @Override
    public ArrayList<T> scanAncestors(ArrayList<T> set) {

        if (this.ancestor != null) {
            set.add(0, ancestor);
            this.ancestor.scanAncestors(set);
        }

        return set;
    }

    /**
     *
     * @param set
     * @return
     */
    @Override
    public ArrayList<T> scanNodesAndAncestors(ArrayList<T> set) {

        set.add((T) this);

        if (this.ancestor != null) {
            scanAncestors(set);
        }

        if (this.parent != null) {
            this.parent.scanNodesAndAncestors(set);
        }

        return set;
    }

    /**
     *
     * @param depth
     * @return
     */
    @Override
    public T getNodeDepth(int depth) {

        if (this.depth() == depth) {
            return (T) this;
        } else if (this.parent != null) {
            return (T) this.parent.getNodeDepth(depth);
        } else {
            return null;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public T getAncestor() {
        return this.ancestor;
    }

    /**
     *
     * @return
     */
    @Override
    public T getLeaf() {
        return (T) leaf;
    }

    @Override
    public void setNodeable(INodeable nodeable) {
        realNode.setNodeable(nodeable);
    }

    @Override
    public void addColliderNode(T idNode) {
        realNode.addColliderNode(idNode);
    }

    @Override
    public String getCode() {
        return realNode.getCode();
    }

    @Override
    public String getName() {
        return realNode.getName();
    }

    @Override
    public INodeable getNodeable() {
        return realNode.getNodeable();
    }

    /**
     *
     * @return
     */
    @Override
    public String getPath() {
        String path = PatternConfigurator.EMPTY_STRING;
        return Optional.ofNullable(getNodeable())
                .filter(nodeable -> this.parent != null)
                .map(nodeable -> parent.getPath() + PatternConfigurator.PATH_SEPARATOR + getNodeable().getCode())
                .orElseGet(() -> getNodeable().getCode());
    }

    @Override
    public String getPathIncludeAncestorPath() {
        return getPath();
    }

    @Override
    public String getDepositPlacePrefixForFileName() {
        return realNode.getDepositPlacePrefixForFileName();
    }

    @Override
    public <N extends Nodeable> T getNodeByNodeableTypeResource(Class<N> nodeableType) {

        if (getNodeable() != null && this.getNodeable().getNodeableType().equals(nodeableType)) {
            return (T) this;
        }

        T parentNode = this.getParent();

        while (parentNode != null) {
            Class<Nodeable> type = parentNode.getNodeable().getNodeableType();
            if (type.equals(nodeableType) || nodeableType.isAssignableFrom(type)) {
                return parentNode;
            } else {
                parentNode = (T) parentNode.getParent();
            }
        }

        return null;
    }

    @Override
    public void setLeaf(T leaf) {
        this.leaf = leaf;
        realNode.leaf = ((AbstractBranchNode) leaf).realNode;
    }

    /**
     *
     * @param id
     */
    public void addColliderNode(long id) {
        this.colliderNodes.add(id);
    }

    @Override
    public int compareTo(AbstractBranchNode o) {
        if (o == null) {
            return -1;
        }
        return getPath().compareTo(o.getPath());
    }

    @Override
    public String toString() {
        return realNode == null ? "" : realNode.getPath();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isAnAncestor() {
        return this.isAncestor;
    }

}
