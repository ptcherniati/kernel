package org.inra.ecoinfo.mga.business.composite.activities;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import org.hibernate.annotations.NaturalId;

/**
 *
 * @author ryahiaoui
 */
@Entity(name = "ExtractActivity")
@Table(name = "CompositeActivityExtraction", 
        uniqueConstraints = @UniqueConstraint(columnNames = {"idNode", "login"}), 
        indexes = {
            @Index(name = "idNodeidx", columnList = "idNode"),
            @Index(name = "idNode_loginidx", columnList = "idNode,login")
        })
public class ExtractActivity implements IExtractActivity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Column(name = "idNode", nullable = false)
    @NaturalId
    private Long idNode;

    @Column(name = "login", nullable = false)
    @NaturalId
    private String login;

    @Column(name = "dateStart")
    private LocalDate dateStart;

    @Column(name = "dateEnd")
    private LocalDate dateEnd;

    /**
     *
     */
    public ExtractActivity() {

    }

    /**
     *
     * @param idNode
     * @param login
     * @param dateStart
     * @param dateEnd
     */
    public ExtractActivity(Long idNode, String login, LocalDate dateStart, LocalDate dateEnd) {
        this.idNode = idNode;
        this.login = login;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }

    /**
     *
     * @return
     */
    public String getLogin() {
        return login;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateStart() {
        return dateStart;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateEnd() {
        return dateEnd;
    }

    /**
     *
     * @return
     */
    public Long getIdNode() {
        return idNode;
    }

    /**
     *
     * @param idNode
     */
    public void setIdNode(Long idNode) {
        this.idNode = idNode;
    }

    /**
     *
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     *
     * @param dateStart
     */
    public void setDateStart(LocalDate dateStart) {
        this.dateStart = dateStart;
    }

    /**
     *
     * @param dateEnd
     */
    public void setDateEnd(LocalDate dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

}
