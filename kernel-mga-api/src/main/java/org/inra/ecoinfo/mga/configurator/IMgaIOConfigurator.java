package org.inra.ecoinfo.mga.configurator;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author yahiaoui
 */
public interface IMgaIOConfigurator {

    /**
     *
     * @return
     */
    Map<String, INodeable> getExtractedNodeables();

    /**
     *
     * @param codeConf
     * @return
     */
    Optional<IMgaIOConfiguration> getConfiguration(Integer codeConfiguration);

    /**
     *
     * @param <T>
     * @param columnType
     * @param index
     * @param conf
     */
     <T extends INodeable> void addColumn(Class<T> columnType, int index, Integer codeConfiguration);

    /**
     *
     * @param conf
     */
    void clearColumns(Integer codeConfiguration);

    /**
     *
     * @return
     */
    int getDeepestIndexNode();

    /**
     *
     * @param deepestIndexNode
     */
    void setDeepestIndexNode(int deepestIndexNode);

    /**
     *
     * @param codeConf
     * @return
     */
    List<String> getColumnsTreeNames(Integer codeConfiguration);

    /**
     *
     * @return
     */
     Map<Integer, IMgaIOConfiguration> getConfigurations();

    /**
     *
     * @param conf
     * @return
     */
    List<String> getColumnsTreeNames(Integer codeConfiguration, List<Optional<RealNode>> branche);

}
