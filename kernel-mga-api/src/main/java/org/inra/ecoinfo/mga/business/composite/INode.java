package org.inra.ecoinfo.mga.business.composite;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author yahiaoui
 * @param <T>
 */
public interface INode<T extends INode> extends IDataNode {

    /**
     *
     * @param idNode
     */
    void addColliderNode(T idNode);

    /**
     *
     * @param priv
     * @param state
     */
    /**
     *
     */
    //void clearAllActivities();
    /**
     *
     * @return
     */
    int depth();

    /**
     *
     * @return
     */
    int depthIncludeAncestor();

    /**
     *
     * @return
     */
    //Map<Activities, Integer> getActivatedActivities();
    /**
     *
     * @return
     */
    T getAncestor();

    /**
     *
     * @param anscestor
     */
    void setAncestor(T anscestor);

    /**
     *
     * @return
     */
    String getCode();

    /**
     *
     * @return
     */
    Set<Long> getColliderNodes();

    /**
     *
     * @return
     */
    String getName();

    /**
     *
     * @param depth
     * @return
     */
    T getNodeDepth(int depth);

    /**
     *
     * @return
     */
    INodeable getNodeable();

    /**
     *
     * @return
     */
    T getParent();

    /**
     *
     * @param node
     */
    void setParent(T node);

    /**
     *
     * @return
     */
    String getPathIncludeAncestorPath();

    /**
     *
     * @return
     */
    //Map<Activities, Integer> getRealState();
    /**
     *
     * @param priv
     * @return
     */
    //int getRealState(Activities priv);
    /**
     *
     * @return
     */
    T getRoot();

    /**
     *
     * @param processName
     * @param date
     * @return
     */
    //LocalDate date(String processName, int date);
    /**
     *
     * @param activity
     * @param whichDate
     * @return
     */
    //String dateAsString(String activity, int whichDate);
    /**
     *
     * @param p
     * @return
     */
    //String dateBegAsString(Activities p);
    /**
     *
     * @param p
     * @return
     */
    //String dateEndAsString(Activities p);
    /**
     *
     * @return
     */
    T getLeaf();

    /**
     *
     * @param leaf
     */
    void setLeaf(T leaf);

    /**
     *
     * @return
     */
    //boolean isConflictState();
//    String activitiesDateAsString(String processName, int date);
//
//    String activitiesDateStringFormat(Activities processName, int date);
    /**
     *
     * @param set
     * @return
     */
    List<T> scanAncestors(ArrayList<T> set);

    /**
     *
     * @param set
     * @return
     */
    List<T> scanNodes(ArrayList<T> set);

    /**
     *
     * @param set
     * @return
     */
    List<T> scanNodesAndAncestors(ArrayList<T> set);

    /**
     *
     * @param colliderNodes
     */
    void setColliderNodes(Set<AbstractBranchNode> colliderNodes);

    /**
     *
     * @param conflictState
     */
    //void setConflictState(boolean conflictState);
    /**
     *
     * @param id
     */
    void setId(long id);

    /**
     *
     * @param nodeable
     */
    void setNodeable(INodeable nodeable);

    /**
     *
     * @param isLeaf
     */
    void setIsLeaf(boolean isLeaf);

    /**
     *
     * @return
     */
    boolean isIsLeaf();

    /**
     *
     * @return
     */
    String getDepositPlacePrefixForFileName();

    /**
     *
     * @param <N>
     * @param nodeableType
     * @return
     */
    <N extends Nodeable> T getNodeByNodeableTypeResource(Class<N> nodeableType);

    /**
     *
     * @return
     */
    RealNode getRealNode();

    /**
     *
     * @param realNode
     */
    void setRealNode(RealNode realNode);

    /**
     *
     * @return
     */
    boolean isAnAncestor();

    /**
     *
     * @return
     */
    @Override
    default Long getOrder() {
        return getNodeable().getOrder();
    }

}
