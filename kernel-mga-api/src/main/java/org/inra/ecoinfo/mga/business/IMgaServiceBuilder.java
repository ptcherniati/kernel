package org.inra.ecoinfo.mga.business;

import java.util.stream.Stream;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;

/**
 *
 * @author yahiaoui
 */
public interface IMgaServiceBuilder {

    /**
     *
     * @return
     */
    IMgaBuilder getMgaBuilder();

    /**
     *
     * @return
     */
    IMgaIOConfigurator getMgaIOConfigurator();

    /**
     *
     * @return
     */
    IMgaRecorder getRecorder();

    /**
     *
     * @param mgaIOConfiguration
     * @param isCurrentUserRoot
     * @param forceExtends
     * @return
     */
    Stream<INode> loadNodes(IMgaIOConfiguration mgaIOConfiguration, boolean isCurrentUserRoot, boolean forceExtends);

    /**
     *
     * @param <T>
     * @param wt
     * @param typeResource
     * @return
     */
    <T extends INodeable> Stream<INode> loadNodesByTypeResource(WhichTree wt, Class<T> typeResource);

    /**
     *
     * @param mgaBuilder
     */
    void setMgaBuilder(IMgaBuilder mgaBuilder);

    /**
     *
     * @param mgaIOConfigurator
     */
    void setMgaIOConfigurator(IMgaIOConfigurator mgaIOConfigurator);

    /**
     *
     * @param recorder
     */
    void setRecorder(IMgaRecorder recorder);

}
