package org.inra.ecoinfo.mga.enums;

import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeRefData;

/**
 *
 * @author yahiaoui
 */
public enum WhichTree {

    /**
     *
     */
    TREEDATASET(NodeDataSet.class),
    /**
     *
     */
    TREEREFDATA(NodeRefData.class);
    /**
     *
     * @param whichTree
     * @return
     */
    public static WhichTree convertToWhichTree(String whichTree) {
        try {
            return WhichTree.valueOf(whichTree.toUpperCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException("Illegal whichTree : " + whichTree, ex);
        }
    }
    private Class<? extends AbstractBranchNode> entityTreeClass;

    private <T extends AbstractBranchNode> WhichTree(Class<T> entityTreeClass) {
        this.entityTreeClass = entityTreeClass;
    }

    /**
     *
     * @param <T>
     * @return
     */
    public <T extends AbstractBranchNode> Class<T> getEntityTreeClass() {
        return (Class<T>) entityTreeClass;
    }
}
