package org.inra.ecoinfo.mga.viewadapter;

import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;

/**
 *
 * @author ryahiaoui
 */
public interface ISkeletonBuilder {

    /**
     *
     * @param config
     * @return
     */
    ITreeNode<AbstractBranchNode> getOrLoadSkeletonTree(IMgaIOConfiguration config);
}
