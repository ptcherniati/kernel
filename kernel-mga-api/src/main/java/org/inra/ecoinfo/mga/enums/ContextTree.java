package org.inra.ecoinfo.mga.enums;

/**
 *
 * @author yahiaoui
 */
public enum ContextTree {

    /**
     *
     */
    DATASETTREE,
    /**
     *
     */
    REFDATATREE,
    /**
     *
     */
    FILETREE;

    /**
     *
     * @param context
     * @return
     */
    public static ContextTree convertToContextTree(String context) {
        try {
            return ContextTree.valueOf(context.toUpperCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException("Illegal ContextTree : " + context, ex);
        }
    }

}
