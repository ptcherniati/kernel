/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business.composite;

import java.io.Serializable;

/**
 *
 * @author tcherniatinsky
 */
public interface IDataNode extends Serializable{

    /**
     *
     * @return
     */
    String getPath();

    /**
     *
     * @return
     */
    Long getId();

    /**
     *
     * @return
     */
    Long getOrder();
}
