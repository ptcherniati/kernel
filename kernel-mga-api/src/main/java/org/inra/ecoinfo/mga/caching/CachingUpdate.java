/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.caching;

import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(
        name = CachingUpdate.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = 
                {CachingUpdate.CACHE_CONFIGURATION_NUMBER}
        )
)
public class CachingUpdate {

    /**
     *
     */
    public static final String TABLE_NAME ="caching_update";

    /**
     *
     */
    public static final String CACHE_CONFIGURATION_NUMBER ="cache_configuration_number";

    /**
     *
     */
    public static final String CACHE_INSTANT ="cache_instant";
    @Column(name = CACHE_CONFIGURATION_NUMBER)
    @Id
    private Integer configurationNumber;
    @Column(name = CACHE_INSTANT)
    private Instant updateDate;

    /**
     *
     */
    public CachingUpdate() {
    }

    /**
     *
     * @param intgr
     * @param instnt
     */
    public CachingUpdate(Integer configurationNumber, Instant updateDate) {
        this.configurationNumber = configurationNumber;
        this.updateDate = updateDate;
    }

    /**
     *
     * @return
     */
    public Integer getConfigurationNumber() {
        return configurationNumber;
    }

    /**
     *
     * @param configurationNumber
     */
    public void setConfigurationNumber(Integer configurationNumber) {
        this.configurationNumber = configurationNumber;
    }

    /**
     *
     * @return
     */
    public Instant getUpdateDate() {
        return updateDate;
    }

    /**
     *
     * @param updateDate
     */
    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }
    
}
