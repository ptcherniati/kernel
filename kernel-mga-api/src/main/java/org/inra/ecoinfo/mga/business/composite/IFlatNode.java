package org.inra.ecoinfo.mga.business.composite;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.Activities;

/**
 *
 * @author ryahiaoui
 */
public interface IFlatNode extends IDataNode {

    /**
     *
     * @return
     */
    /**
     *
     * @param activities
     */
    /**
     *
     * @return
     */
    Set<Long> getColliderNodes();

    /**
     *
     * @param colliderNodes
     */
    void setColliderNodes(Set<Long> colliderNodes);

    /**
     *
     * @param id
     */
    void setId(Long id);

    /**
     *
     * @return
     */
    long getNodeableId();

    /**
     *
     * @return
     */
    String getName();

    /**
     *
     * @return
     */
    Long getParent();

    /**
     *
     * @return
     */
    Long getAncestorId();

    /**
     *
     * @return
     */
    Long getRealNodeId();

    /**
     *
     * @return
     */
    Long getLeafId();

    /**
     *
     * @return
     */
    Long getRealNodeLeafId();

    /**
     *
     * @param <T>
     * @return
     */
    <T extends INodeable> Class<T> getTypeResource();

    /**
     *
     * @return
     */
    Map<Activities, Integer> getRealState();

    /**
     *
     * @param realState
     */
    void setRealState(Map<Activities, Integer> realState);

    /**
     *
     * @param activity
     * @param whichDate
     * @return
     */
    String dateAsString(Activities activity, int whichDate);

    /**
     *
     * @param p
     * @return
     */
    String dateBegAsString(Activities p);

    /**
     *
     * @param p
     * @return
     */
    String dateEndAsString(Activities p);

    /**
     *
     * @param p
     * @return
     */

    /**
     *
     * @param processName
     * @param dates
     */
    /**
     *
     * @param activitiesDates
     */
    /**
     *
     * @param activity
     * @return
     */
    int copyRealToTmpState(Activities activity);

    /**
     *
     * @param priv
     * @return
     */
    int getRealState(Activities priv);

    /**
     *
     * @return
     */
    Map<Activities, Map<Long, List<LocalDate>>> getOwnCheckedActivities();

    /**
     *
     * @param privs
     * @param state
     */
    void addRealStates(List<Activities> privs, Integer state);

    /**
     *
     * @return
     */
    boolean isIsLeaf();

    /**
     *
     * @return
     */
    String getCode();

    /**
     *
     * @return
     */
    Map<Activities, Integer> getActivatedActivities();

    /**
     *
     * @param privils
     * @return
     */

    /**
     *
     * @param path
     */
    void setPath(String path);

    /**
     *
     * @return
     */
    String getLeafNodePath();

    /**
     *
     * @param processName
     * @param dates
     */
    void addActivity(Activities processName, Map<Group, List<LocalDate>> dates);

    /**
     *
     * @param processName
     * @param dates
     */
    void addOwnActivity(Activities processName, List<LocalDate> dates);

    /**
     *
     * @return
     */
    Map<Activities, Map<Group, List<LocalDate>>> getActivitiesForAllGroups();

    /**
     *
     * @param activities
     */
    void setActivitiesForAllGroups(Map<Activities, Map<Group, List<LocalDate>>> activities);

    /**
     *
     * @param priv
     * @param state
     */
    void addRealState(Activities priv, Integer state);

    /**
     *
     * @return
     */
    Map<Activities, List<LocalDate>> getOwnActivities();

    /**
     *
     * @return
     */
    Map<Activities, Map<Integer, List<LocalDate>>> getOwnTmpState();

    /**
     *
     * @param ownActivities
     */
    void setOwnActivities(Map<Activities, List<LocalDate>> ownActivities);

    ;

    /**
     *
     * @param p
     * @return
     */
    boolean hasActivity(Activities p);

    /**
     *
     * @param privils
     * @return
     */
    boolean hasAllActivities(List<Activities> privils);

    /**
     *
     * @param privils
     * @return
     */
    boolean hasAtLeastOneActivity(List<Activities> privils);

    /**
     *
     * @param ownGroup
     */
    void setOwnGroup(Group ownGroup);

    /**
     *
     * @return
     */
    Group getOwnGroup();

    /**
     *
     * @return
     */
    Map<Activities, Integer> getOwnRealState();

    /**
     *
     * @param ownRealState
     */
    void setOwnRealState(Map<Activities, Integer> ownRealState);

    /**
     *
     * @param priv
     * @param state
     */
    void addOwnRealState(Activities priv, Integer state);
}
