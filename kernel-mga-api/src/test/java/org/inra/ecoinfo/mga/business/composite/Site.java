package org.inra.ecoinfo.mga.business.composite;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Représente un site scientifique.
 *
 * @author "Antoine Schellenberger"
 */
@Entity(name = Site.NAME_ENTITY_JPA)
@Table(name = Site.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = "sit_id")
public class Site extends Nodeable implements Serializable {

    /**
     * The Constant PERSISTENT_NAME_ID @link(String).
     */
    public static final String PERSISTENT_NAME_ID = "id";
    /**
     * The Constant NAME_ATTRIBUTS_DESCRIPTION @link(String).
     */
    public static final String NAME_ATTRIBUTS_DESCRIPTION = "description";
    /**
     * The Constant NAME_ATTRIBUTS_NAME @link(String).
     */
    public static final String NAME_ATTRIBUTS_NAME = "name";
    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    public static final String NAME_ENTITY_JPA = "site";
    /**
     * The Constant SITE_NAME_ID @link(String).
     */
    public static final String SITE_NAME_ID = "id";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The name @link(String).
     */
    @Column(nullable = false, unique = true, name = Site.NAME_ATTRIBUTS_NAME)
    private String name;

    /**
     *
     */
    public Site() {
    }

    /**
     * @param name
     */
    public Site(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) Site.class;
    }

    @Override
    public String getName() {
        return name;
    }


    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Site other = (Site) obj;
        return Objects.equals(this.name, other.name);
    }

}
