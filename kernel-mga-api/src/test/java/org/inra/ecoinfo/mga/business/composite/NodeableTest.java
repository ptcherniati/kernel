/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business.composite;

import java.time.LocalDate;
import java.time.Month;
import java.util.Optional;
import org.concordion.api.ExpectedToPass;
import org.inra.ecoinfo.AttributeValue;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ptchernia
 */

/**
 * The Class ConfigurationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@ExpectedToPass
public class NodeableTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    @Autowired
    JPASite siteDAO;

    /**
     *
     */
    public NodeableTest() {
    }


    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(JPASite siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     */
    @Before
    public void setUp() {
        siteDAO.initCriteriaBuilder();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getLocalisationEntite method, of class Nodeable.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testEntities() throws PersistenceException {
        Site site = new Site("nom");
        siteDAO.saveOrUpdate(site);
        siteDAO.flush();
        Site byId = siteDAO.getById(Site.class, site.getId()).orElseThrow(() -> new PersistenceException());
        assertNotNull(byId);
        assertNotNull(byId.getId());

        EntitieTest entity = new EntitieTest("Dupont", "D", LocalDate.of(1965, Month.DECEMBER, 25));
        siteDAO.saveOrUpdateGeneric(entity);
        Optional<EntitieTest> entity1 = siteDAO.getEntity("Dupont", "D");
        assertTrue(entity1.isPresent());
        Optional<EntitieTest> entity2 = siteDAO.getEntity("Dupont", "T");
        assertFalse(entity2.isPresent());
        Optional<EntitieTest> entity3 = siteDAO.getByNaturalId(
                EntitieTest.class,
                AttributeValue.getIntance(EntitieTest_.prenom, "D"),
                AttributeValue.getIntance(EntitieTest_.nom, "Dupont"));
        assertTrue(entity3.isPresent());
    }
}
