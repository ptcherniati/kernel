/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.business.composite;

import java.util.Optional;
import org.hibernate.LockOptions;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author ptchernia
 */
public class JPASite extends AbstractJPADAO<Site> {

    /**
     * @param nom
     * @param prenom
     * @return
     */
    public Optional<EntitieTest> getEntity(String nom, String prenom) {
        return getSession().byNaturalId(EntitieTest.class)
                .with(LockOptions.READ)
                .using(EntitieTest_.nom.getName(), nom)
                .using(EntitieTest_.prenom.getName(), prenom)
                .loadOptional();
    }
}
