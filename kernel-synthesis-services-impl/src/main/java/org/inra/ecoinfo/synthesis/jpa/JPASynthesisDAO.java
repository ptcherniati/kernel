/**
 * OREILacs project - see LICENCE.txt for use created: 13 août 2009 14:33:30
 */
package org.inra.ecoinfo.synthesis.jpa;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.GenericDatatype;
import org.inra.ecoinfo.dataset.versioning.entity.GenericDatatype_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.IDataNode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.synthesis.GenericMetaSynthesisDatatype;
import org.inra.ecoinfo.synthesis.ISynthesisDAO;
import org.inra.ecoinfo.synthesis.ISynthesisRegister;
import org.inra.ecoinfo.synthesis.SynthesisUtils;
import org.inra.ecoinfo.synthesis.entity.DefaultSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.DefaultSynthesisDatatype_;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue_;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class JPASynthesisDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class JPASynthesisDAO extends AbstractJPADAO<GenericSynthesisDatatype> implements ISynthesisDAO {

    /**
     * The Constant PATTERN_STRING_FORMAT_SYNTHESISDATATYPE_CLASSNAME.
     * @link(String).
     */
    private static final String PATTERN_STRING_FORMAT_SYNTHESISDATATYPE_CLASSNAME = "%s.%s.SynthesisDatatype";
    /**
     * The Constant PATTERN_STRING_FORMAT_SYNTHESISVALUE_CLASS @link(String).
     */
    private static final String PATTERN_STRING_FORMAT_SYNTHESISVALUE_CLASS = "%s.%s.SynthesisValue";

    private static final String VARIABLE = "VARIABLE";
    private static final String DATE = "DATE";
    private static final String REALNODE = "REALNODE";
    private static final String ALIAS_REALNODE_DATATYPE = "ALIAS_REALNODE_DATATYPE";
    private static final String ALIAS_REALNODE_IDNODE = "ALIAS_REALNODE_IDNODE";
    private static final String ALIAS_REALNODE_DATE_DEBUT = "ALIAS_REALNODE_DATE_DEBUT";
    private static final String ALIAS_REALNODE_DATE_FIN = "ALIAS_REALNODE_DATE_FIN";

    /**
     *
     */
    protected ISynthesisRegister synthesisRegister;
    protected Class<? extends Nodeable> siteClass = Site.class;

    /**
     * Gets the all synthesis datatypes.
     *
     * @param synthesisDatatypeClass the synthesis datatype class
     * @return the all synthesis datatypes
     * @see
     * org.inra.ecoinfo.synthesis.ISynthesisDAO#getAllSynthesisDatatypes(java.lang.Class)
     */
    @Override
    public List<GenericSynthesisDatatype> getAllSynthesisDatatypes(final Class<GenericSynthesisDatatype> synthesisDatatypeClass) {
        CriteriaQuery<GenericSynthesisDatatype> query = builder.createQuery(synthesisDatatypeClass);
        Root<GenericSynthesisDatatype> synDty = query.from(synthesisDatatypeClass);
        query.select(synDty);
        return getResultList(query);
    }
    
    /**
     *
     * @return
     */
    @Override
    public List<String> getDefaultSynthesisDatatypeNames(){
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<GenericDatatype> genericDatatype = query.from(GenericDatatype.class);
        Join<GenericDatatype, Dataset> dataset = genericDatatype.join(GenericDatatype_.dataset);
        Join<Dataset, RealNode> rnd = dataset.join(Dataset_.realNode);
        Join<RealNode, Nodeable> datatype = rnd.join(RealNode_.nodeable);
        
        query
                .select(datatype.get(Nodeable_.code));
        return getResultList(query);
    }

    /**
     *
     * @param datatypeName
     * @return
     */
    @Override
    public List<GenericSynthesisDatatype> getAllDefaultSynthesisDatatypes(String datatypeName) {
        CriteriaQuery<GenericSynthesisDatatype> query = builder.createQuery(GenericSynthesisDatatype.class);
        Root<DefaultSynthesisDatatype> synDty = query.from(DefaultSynthesisDatatype.class);
        query
                .select(synDty)
                .where(
                        builder.equal(synDty.get(DefaultSynthesisDatatype_.datatypeName), datatypeName)
                );
        return getResultList(query);
    }

    /**
     * Gets the date by site and variable.
     *
     * @param datasetNodeLeafContextId
     * @param contextName
     * @param variableName the variable name
     * @param datatypeName the datatype name
     * @param synthesisValueClass
     * @return the date by site and variable
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.synthesis.ISynthesisDAO#getDateBySiteAndVariable(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public IntervalDate getDateByContextNodeableAndVariableNodeable(Long datasetNodeLeafContextId, Class<GenericSynthesisValue> synthesisValueClass) throws PersistenceException {
        try {
            CriteriaQuery<IntervalDate> query = builder.createQuery(IntervalDate.class);
            Root<GenericSynthesisValue> syntVal = query.from(synthesisValueClass);
            query.where(
                    builder.equal(syntVal.get(GenericSynthesisValue_.idNode), datasetNodeLeafContextId)
            );
            query.select(builder.construct(
                    IntervalDate.class,
                    builder.least(syntVal.get(GenericSynthesisValue_.date)),
                    builder.greatest(syntVal.get(GenericSynthesisValue_.date)),
                    builder.literal(DateUtil.DD_MM_YYYY_HH_MM_SS)
            ));
            return getResultList(query).stream().findFirst()
                    .orElse(new IntervalDate(DateUtil.MAX_LOCAL_DATE_TIME, DateUtil.MAX_LOCAL_DATE_TIME, DateUtil.DD_MM_YYYY_HH_MM_SS));
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Gets the synthesis values by variable and site.
     *
     * @param nodeDatasetVariableId
     * @param contextName
     * @param variableName
     * @param synthesisValueClass
     * @return the synthesis values by variable and site
     * @see
     * org.inra.ecoinfo.synthesis.ISynthesisDAO#getSynthesisValuesByVariableAndSite(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<GenericSynthesisValue> getSynthesisValuesByVariableNodeableAndContext(Long nodeDatasetVariableId, Class<GenericSynthesisValue> synthesisValueClass) {
        CriteriaQuery<GenericSynthesisValue> query = builder.createQuery(synthesisValueClass);
        Root<GenericSynthesisValue> syntVal = query.from(synthesisValueClass);
        query.where(
                builder.equal(syntVal.get(GenericSynthesisValue_.idNode), nodeDatasetVariableId)
        );
        query.select(syntVal);
        return getResultList(query);
    }

    /**
     * Gets the synthesis values by variable and site by dates interval.
     *
     * @param contextName
     * @param start the start
     * @param end the end
     * @param node
     * @param synthesisValueClass
     * @return the synthesis values by variable and site by dates interval
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.synthesis.ISynthesisDAO#getSynthesisValuesByVariableAndSiteByDatesInterval(java.lang.String,
     * java.lang.String, java.lang.String, java.time.LocalDateTime,
     * java.time.LocalDateTime)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<GenericSynthesisValue> getSynthesisValuesByVariableNodeableAndSiteByDatesInterval(LocalDateTime start, LocalDateTime end, NodeDataSet node, Class<GenericSynthesisValue> synthesisValueClass) throws PersistenceException {
        try {

            CriteriaQuery<GenericSynthesisValue> query = builder.createQuery(synthesisValueClass);
            Root<GenericSynthesisValue> syntVal = query.from(synthesisValueClass);
            query.where(
                    builder.equal(syntVal.get(GenericSynthesisValue_.idNode), node.getId()),
                    builder.between(syntVal.<LocalDateTime>get(GenericSynthesisValue_.date), start, end)
            );
            query.select(syntVal);
            return getResultList(query);
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Purge synthesis data.
     *
     * @throws PersistenceException the persistence exception
     * @see org.inra.ecoinfo.synthesis.ISynthesisDAO#purgeSynthesisData()
     */
    @Override
    public void purgeSynthesisData() throws PersistenceException {
        final Set<String> datatypesNames = synthesisRegister.getDatatypesNames();
        List<Exception> exceptions = new LinkedList();
        CriteriaDelete<DefaultSynthesisDatatype> createCriteriaDelete = builder.createCriteriaDelete(DefaultSynthesisDatatype.class);
        delete(DefaultSynthesisDatatype.class);
        datatypesNames.forEach((t) -> {
            if (!(synthesisRegister.retrieveMetadataSynthesisDatatype(t) instanceof GenericMetaSynthesisDatatype)) {
                if (exceptions.isEmpty()) {
                    delete(t, PATTERN_STRING_FORMAT_SYNTHESISDATATYPE_CLASSNAME, exceptions);
                    delete(t, PATTERN_STRING_FORMAT_SYNTHESISVALUE_CLASS, exceptions);
                }
            };
        });

        if (!exceptions.isEmpty()) {
            String message = exceptions.stream()
                    .map(e -> e.getMessage())
                    .collect(Collectors.joining("|"));
            throw new PersistenceException(message);
        }
    }

    private void delete(String datatypeName, String pattern, List<Exception> exceptions) {
        try {
            final String prefix = synthesisRegister.retrieveMetadataSynthesisDatatype(datatypeName).getPrefix();
            if (prefix.equals("generic")) {
                return;
            }
            final Class clazz = Class.forName(String.format(pattern, synthesisRegister.getBasePackage(), prefix));
            delete(clazz);
        } catch (ClassNotFoundException | PersistenceException ex) {
            exceptions.add(ex);
        }
    }

    /**
     * Sets the synthesis register.
     *
     * @param synthesisRegister the new synthesis register
     */
    public void setSynthesisRegister(final ISynthesisRegister synthesisRegister) {
        this.synthesisRegister = synthesisRegister;
    }

    /**
     *
     * @param user
     * @param synthesisActivities
     * @param synthesisValueClass
     * @param datasetNodeLeafContextId
     * @return
     */
    @Override
    public List<NodeDataSet> getAllAvailablesVariablesSynthesisNodes(IUser user, Map<Long, Map<Group, List<LocalDate>>> synthesisActivities, Class<? extends GenericSynthesisValue> synthesisValueClass, Long datasetNodeLeafContextId) {
        CriteriaQuery<Tuple> criteria = builder.createTupleQuery();
        Root<NodeDataSet> ndsVariable = criteria.from(NodeDataSet.class
        );
        Join<NodeDataSet, AbstractBranchNode> ndsContext = ndsVariable.join(NodeDataSet_.parent);
        Join<NodeDataSet, RealNode> realNode = ndsVariable.join(NodeDataSet_.realNode);
        Root sv = criteria.from(synthesisValueClass);
        List<Predicate> predicatesAnd = new ArrayList();
        predicatesAnd.add(builder.equal(sv.get(GenericSynthesisValue_.idNode), ndsVariable.get(NodeDataSet_.id)));
        predicatesAnd.add(builder.equal(ndsContext.get(NodeDataSet_.id), datasetNodeLeafContextId));
        criteria.where(builder.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
        criteria.distinct(true);
        criteria.orderBy(builder.asc(realNode.get(RealNode_.path)));
        if (user == null || !user.getIsRoot()) {
            predicatesAnd.add(ndsVariable.get(NodeDataSet_.id).in(synthesisActivities.keySet()));
            criteria.multiselect(
                    ndsVariable.alias(VARIABLE),
                    realNode.alias(REALNODE),
                    sv.get(GenericSynthesisValue_.date).alias(DATE)
            );
        } else {
            criteria
                    .multiselect(ndsVariable.alias(VARIABLE),
                            realNode.alias(REALNODE));
        }
        List<Tuple> resultList = getResultList(criteria);
        if (user != null && !user.getIsRoot()) {
            final List<NodeDataSet> collect = resultList.stream()
                    .filter((tuple)
                            -> synthesisActivities.containsKey(((IDataNode) tuple.get(VARIABLE)).getId()) && SynthesisUtils.isBetween((LocalDateTime) tuple.get(DATE), synthesisActivities.get(((IDataNode) tuple.get(VARIABLE)).getId())))
                    .map(tuple -> (NodeDataSet) tuple.get(VARIABLE))
                    .distinct()
                    .collect(Collectors.toList());
            return collect;
        }
        final List<NodeDataSet> collect = resultList.stream()
                .map(tuple -> (NodeDataSet) tuple.get(VARIABLE))
                .distinct()
                .collect(Collectors.toList());
        return collect;
    }

    /**
     *
     * @param nodeDatasetContextId
     * @return
     */
    @Override
    public Optional<NodeDataSet> getByNodeDatasetId(Long nodeDatasetContextId) {
        CriteriaQuery<NodeDataSet> query = builder.createQuery(NodeDataSet.class
        );
        Root<NodeDataSet> nds = query.from(NodeDataSet.class
        );
        query
                .select(nds)
                .where(builder.equal(nds.get(NodeDataSet_.id), nds));
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    public Map<String, Map<String, Optional<DefaultSynthesisDatatype>>> getDefaultSynthesisDatatypes() {
        CriteriaQuery<Tuple> query = builder.createTupleQuery();
        Root<VersionFile> version = query.from(VersionFile.class
        );
        Join<VersionFile, Dataset> dataset = version.join(VersionFile_.dataset);
        Root<NodeDataSet> node = query.from(NodeDataSet.class
        );
        Path<RealNode> rnd = dataset.get(Dataset_.realNode);
        Path<RealNode> rnd2 = node.get(NodeDataSet_.realNode);

        query
                .multiselect(
                        rnd.alias(ALIAS_REALNODE_DATATYPE),
                        node.get(NodeDataSet_.id).alias(ALIAS_REALNODE_IDNODE),
                        dataset.get(Dataset_.dateDebutPeriode).alias(ALIAS_REALNODE_DATE_DEBUT),
                        dataset.get(Dataset_.dateFinPeriode).alias(ALIAS_REALNODE_DATE_FIN)
                )
                .where(
                        builder.isNotNull(dataset.get(Dataset_.publishVersion)),
                        builder.isNotNull(version.get(VersionFile_.fileName)),
                        builder.equal(rnd2, rnd)
                );
        Map<Long, DefaultSynthesisDatatype> SynthesisDatatypes = new HashMap<>();
        getResultAsStream(query)
                .forEach(t -> getSynthesisDatatype(t, SynthesisDatatypes));
        return SynthesisDatatypes
                .values()
                .stream()
                .collect(
                        Collectors.groupingBy(
                                dsd -> dsd.getDatatypeName(),
                                Collectors.groupingBy(
                                        dsd -> dsd.getSite(),
                                        Collectors.reducing(this::reduce)
                                )
                        )
                );
    }

    private DefaultSynthesisDatatype reduce(DefaultSynthesisDatatype dsd1, DefaultSynthesisDatatype dsd2) {
        dsd1.setMinDate(dsd1.getMinDate().isBefore(dsd2.getMinDate()) ? dsd1.getMinDate() : dsd2.getMinDate());
        dsd1.setMaxDate(dsd1.getMaxDate().isAfter(dsd2.getMaxDate()) ? dsd1.getMaxDate() : dsd2.getMaxDate());
        dsd1.setIdNodes(String.format("%s,%s", dsd1.getIdNodes(), dsd2.getIdNodes()));
        return dsd1;
    }

    private void getSynthesisDatatype(Tuple t, Map<Long, DefaultSynthesisDatatype> SynthesisDatatypes) {
        RealNode rnd = t.get(ALIAS_REALNODE_DATATYPE, RealNode.class
        );
        Long idNode = t.get(ALIAS_REALNODE_IDNODE, Long.class
        );
        LocalDateTime startdate = t.get(ALIAS_REALNODE_DATE_DEBUT, LocalDateTime.class
        );
        LocalDateTime endDate = t.get(ALIAS_REALNODE_DATE_FIN, LocalDateTime.class
        );
        if (siteClass == null) {
            siteClass = DataType.class;
        }
        DefaultSynthesisDatatype SynthesisDatatype = new DefaultSynthesisDatatype(rnd.getNodeable().getCode(), rnd.getNodeByNodeableTypeResource(siteClass).getPath(), startdate, endDate, idNode.toString());
        SynthesisDatatypes.put(idNode, SynthesisDatatype);
    }

    /**
     *
     * @param siteClassName
     */
    public void setSiteClassName(String siteClassName) {
        try {
            siteClass = (Class<? extends Nodeable>) Class.forName(siteClassName);
        } catch (ClassNotFoundException ex) {
            LOGGER.error(ex.getMessage());
            siteClass = Site.class;
        }
    }

    /**
     * return all children nodes dataset where
     * <ul>
     * <li> realnode parent node id = realNodeContextId</li>
     * <li> is root or (node id in synthesisActivities and date has corrects
     * rights)</li>
     * </ul
     *
     * @param user
     * @param synthesisActivities
     * @param synthesisValueClass
     * @param contextName
     * @param datatypeName
     * @param realNodeLeafContextId
     * @return
     */
//    @Override
//    public List<NodeDataSet> getAllAvailablesVariablesSynthesisNodes(IUser user, Map<Long, Map<Group, List<LocalDate>>> synthesisActivities, Class<? extends GenericSynthesisValue> synthesisValueClass, String contextName, String datatypeName, Long realNodeLeafContextId) {
//
//        CriteriaQuery<Tuple> criteria = builder.createTupleQuery();
//        Root<NodeDataSet> nds = criteria.from(NodeDataSet.class);
//        Join<NodeDataSet, RealNode> rn = nds.join(NodeDataSet_.realNode);
//        Join<RealNode, Nodeable> ndb = rn.join(RealNode_.nodeable);
//        Root sv = criteria.from(synthesisValueClass);
//        List<Predicate> predicatesAnd = new ArrayList();
//        predicatesAnd.add(builder.equal(sv.get(GenericSynthesisValue_.site), contextName));
//        predicatesAnd.add(builder.equal(sv.get(GenericSynthesisValue_.variable), ndb.get(Nodeable_.code)));
//        predicatesAnd.add(builder.equal(rn.get(RealNode_.parent).get(RealNode_.id), realNodeLeafContextId));
//        criteria.where(builder.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
//        criteria.distinct(true);
//        if (user == null || !user.getIsRoot()) {
//            predicatesAnd.add(nds.get(NodeDataSet_.id).in(synthesisActivities.keySet()));
//            criteria.multiselect(
//                    nds,
//                    sv.get(GenericSynthesisValue_.date)
//            );
//        } else {
//            criteria.multiselect(nds);
//        }
//        List<Tuple> resultList = getResultList(criteria);
//        if (user != null && !user.getIsRoot()) {
//            final List<NodeDataSet> collect = resultList.stream()
//                    .filter((tuple)
//                            -> synthesisActivities.containsKey(((IDataNode) tuple.get(0)).getId()) && SynthesisUtils.isBetween((LocalDateTime) tuple.get(1), synthesisActivities.get(((IDataNode) tuple.get(0)).getId())))
//                    .map(tuple -> (NodeDataSet) tuple.get(0))
//                    .sorted()
//                    .distinct()
//                    .collect(Collectors.toList());
//            return collect;
//        }
//        final List<NodeDataSet> collect = resultList.stream()
//                .map(tuple -> (NodeDataSet) tuple.get(0))
//                .sorted()
//                .distinct()
//                .collect(Collectors.toList());
//        return collect;
//    }
}
