/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.synthesis.ISynthesisPredicate;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

/**
 *
 * @author tcherniatinsky
 */
public class SynthesisPredicate extends AbstractSynthesisPredicate {

    /**
     *
     */
    public SynthesisPredicate() {
    }

    /**
     *
     * @param sm
     */
    public SynthesisPredicate(SortedMap<Long, GenericSynthesisDatatype> synthesisDatatypesByNodeId) {
        this.synthesisDatatypesByNodeId = synthesisDatatypesByNodeId;
    }

    /**
     *
     * @param node
     * @return
     */
    @Override
    public List<INode> apply(INode node) {
        List<INode> nodes = new LinkedList();
        if (synthesisDatatypesByNodeId.containsKey(node.getId())) {
            nodes.add(node);
        }
        return nodes;
    }

    /**
     *
     * @param synthesisDatatypesByNodeId
     * @return
     */
    @Override
    public ISynthesisPredicate getInstance(SortedMap<Long, GenericSynthesisDatatype> synthesisDatatypesByNodeId) {
        return new SynthesisPredicate(synthesisDatatypesByNodeId);
    }

}
