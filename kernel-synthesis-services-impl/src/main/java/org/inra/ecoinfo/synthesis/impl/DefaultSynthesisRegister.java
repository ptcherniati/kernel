package org.inra.ecoinfo.synthesis.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.synthesis.GenericMetaSynthesisDatatype;
import org.inra.ecoinfo.synthesis.ISynthesisRegister;
import org.inra.ecoinfo.synthesis.MetaSynthesisDatatype;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class DefaultSynthesisRegister.
 */
public class DefaultSynthesisRegister implements ISynthesisRegister {

    /**
     * The base package @link(String).
     */
    protected static String basePackage;
    /**
     * The meta synthesis datatypes map.
     * @link(Map<String,MetaSynthesisDatatype>).
     */
    protected static Map<String, MetaSynthesisDatatype> metaSynthesisDatatypesMap = new HashMap<>();

    /**
     * Sets the base package.
     *
     * @param basePackage the new base package
     */
    private static void setStaticBasePackage(final String basePackage) {
        DefaultSynthesisRegister.basePackage = basePackage;
    }

    /**
     * Sets the meta synthesis datatypes map.
     *
     * @param metaSynthesisDatatypesMap the meta synthesis datatypes map
     * @link(Map<String,MetaSynthesisDatatype>) the meta synthesis datatypes map
     */
    private static void setStaticMetaSynthesisDatatypesMap(final Map<String, MetaSynthesisDatatype> metaSynthesisDatatypesMap) {
        DefaultSynthesisRegister.metaSynthesisDatatypesMap = metaSynthesisDatatypesMap;
    }

    /**
     * Gets the base package.
     *
     * @return the base package
     * @see org.inra.ecoinfo.synthesis.ISynthesisRegister#getBasePackage()
     */
    @Override
    public String getBasePackage() {
        return DefaultSynthesisRegister.basePackage;
    }

    /**
     * Sets the base package.
     *
     * @param basePackage the new base package
     */
    public void setBasePackage(final String basePackage) {
        DefaultSynthesisRegister.setStaticBasePackage(basePackage);
    }

    /**
     * Gets the datatypes names.
     *
     * @return the datatypes names
     * @see org.inra.ecoinfo.synthesis.ISynthesisRegister#getDatatypesNames()
     */
    @Override
    public Set<String> getDatatypesNames() {
        return DefaultSynthesisRegister.metaSynthesisDatatypesMap.keySet();
    }

    /**
     * Gets the meta synthesis datatypes map.
     *
     * @return the meta synthesis datatypes map
     */
    public Map<String, MetaSynthesisDatatype> getMetaSynthesisDatatypesMap() {
        return DefaultSynthesisRegister.metaSynthesisDatatypesMap;
    }

    /**
     * Sets the meta synthesis datatypes map.
     *
     * @param metaSynthesisDatatypesMap the meta synthesis datatypes map
     * @link(Map<String,MetaSynthesisDatatype>) the meta synthesis datatypes map
     */
    public void setMetaSynthesisDatatypesMap(final Map<String, MetaSynthesisDatatype> metaSynthesisDatatypesMap) {
        DefaultSynthesisRegister.setStaticMetaSynthesisDatatypesMap(metaSynthesisDatatypesMap);
    }

    /**
     * Retrieve metadata synthesis datatype.
     *
     * @param datatypeName the datatype name
     * @return the meta synthesis datatype
     * @see
     * org.inra.ecoinfo.synthesis.ISynthesisRegister#retrieveMetadataSynthesisDatatype(java.lang.String)
     */
    @Override
    public MetaSynthesisDatatype retrieveMetadataSynthesisDatatype(final String datatypeName) {
        for (Map.Entry<String, MetaSynthesisDatatype> entrySet : metaSynthesisDatatypesMap.entrySet()) {
            String key = entrySet.getKey();
            if (Utils.createCodeFromString(key).equals(Utils.createCodeFromString(datatypeName))) {
                return entrySet.getValue();
            }
        }
        return DefaultSynthesisRegister.metaSynthesisDatatypesMap.get(datatypeName);
    }

    @Override
    public void addGenericMetaSynthesisDatatypes(List<String> datatypesName) {
        if (CollectionUtils.isEmpty(datatypesName)) {
            return;
        }
        metaSynthesisDatatypesMap.values().removeIf(v -> v instanceof GenericMetaSynthesisDatatype);
        GenericMetaSynthesisDatatype genericMetaSynthesisDatatype = new GenericMetaSynthesisDatatype("generic");
        datatypesName.forEach(
                datatypeName->metaSynthesisDatatypesMap.put(datatypeName, genericMetaSynthesisDatatype)
        );
    }
}
