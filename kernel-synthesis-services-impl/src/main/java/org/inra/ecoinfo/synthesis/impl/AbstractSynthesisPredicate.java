/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis.impl;

import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.synthesis.ISynthesisPredicate;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

/**
 *
 * @author tcherniatinsky
 */
public abstract class AbstractSynthesisPredicate implements ISynthesisPredicate {

    /**
     *
     */
    protected static GenericSynthesisDatatype DEFAULT_GENERIC_SYNTHESIS_DATATYPE;

    /**
     *
     */
    protected SortedMap<Long, GenericSynthesisDatatype> synthesisDatatypesByNodeId = new TreeMap();

    /**
     *
     */
    protected SortedMap<String, GenericSynthesisDatatype> _synthesisPathes = new TreeMap();

    /**
     *
     * @param nodePath
     * @return
     */
    protected SortedMap<String, String> _nodePathes = new TreeMap();

    /**
     *
     * @return
     */
    @Override
    public Optional<GenericSynthesisDatatype> getSynthesisDatatype(Long id) {
        return Optional.ofNullable(synthesisDatatypesByNodeId.get(id));
    }

    /**
     *
     * @param nodePath
     * @return
     */
    @Override
    public Optional<String> getSynthesisPath(String nodePath) {
        return Optional.ofNullable(_nodePathes.get(nodePath));
    }

    /**
     *
     * @param entry
     * @param noden
     * @param codeNode
     */
    protected void registerNode(Map.Entry<String, GenericSynthesisDatatype> entry, INode noden, String codeNode) {
        _synthesisPathes.computeIfAbsent(entry.getKey(), (String k) -> entry.getValue());
        _nodePathes.computeIfAbsent(codeNode, (String k) -> entry.getKey());
    }

}
