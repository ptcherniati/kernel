/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis.impl;

import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.synthesis.ILocalizedFormatter;

/**
 *
 * @author tcherniatinsky
 */
class LocalizedFormatterForVariableNameGetDescription implements ILocalizedFormatter<DatatypeVariableUnite> {

    ILocalizationManager localizationManager;

    public LocalizedFormatterForVariableNameGetDescription() {
    }

    @Override
    public String format(DatatypeVariableUnite nodeable, Locale locale, Object... arguments) {
        Optional<DatatypeVariableUnite> dvu = Optional.ofNullable(nodeable);
        final Properties propertiesVariablesDefinitions = localizationManager.newProperties(Nodeable.getLocalisationEntite(Variable.class), "definition", locale);
        return dvu.map(nodeabledvu -> StringUtils.isEmpty(nodeabledvu.getVariable().getDefinition()) ? nodeabledvu.getVariable().getName() : nodeabledvu.getVariable().getDefinition())
                .map(definition -> propertiesVariablesDefinitions.getProperty(definition, definition))
                .orElse("Error while retrieving variable definition)");
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

}
