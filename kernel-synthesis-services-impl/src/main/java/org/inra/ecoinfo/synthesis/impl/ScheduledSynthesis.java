/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis.impl;

import org.inra.ecoinfo.synthesis.ISynthesisManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author tcherniatinsky
 */
public class ScheduledSynthesis {

    private ISynthesisManager synthesisManager;

    /**
     *
     * @param synthesisManager
     */
    public void setSynthesisManager(ISynthesisManager synthesisManager) {
        this.synthesisManager = synthesisManager;
    }

    /**
     *
     * @throws BusinessException
     */
    public void buildSynthesis() throws BusinessException{
        synthesisManager.buildSynthesis();
    }
    
    
}
