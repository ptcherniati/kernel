/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.synthesis;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.synthesis.entity.DefaultSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface ISynthesisDAO.
 *
 * @author "Antoine Schellenberger"
 */
public interface ISynthesisDAO extends IDAO<GenericSynthesisDatatype> {

    /**
     * Gets the all synthesis datatypes.
     *
     * @param synthesisDatatypeClass the synthesis datatype class
     * @return the all synthesis datatypes
     * @link(Class<GenericSynthesisDatatype>) the synthesis datatype class
     */
    List<GenericSynthesisDatatype> getAllSynthesisDatatypes(Class<GenericSynthesisDatatype> synthesisDatatypeClass);

    /**
     * Gets the date by contextNodeable and variableNodeable.
     *
     * @param datasetNodeLeafContextId
     * @param contextName the contextNodeable name
     * @param variableName the variableNodeable name
     * @param datatypeName the datatype name
     * @param synthesiValueClass
     * @return the date by contextNodeable and variableNodeable
     * @throws PersistenceException the persistence exception
     * @link(String) the contextNodeable name
     * @link(String) the variableNodeable name
     * @link(String) the datatype name
     */
    IntervalDate getDateByContextNodeableAndVariableNodeable(Long datasetNodeLeafContextId, Class<GenericSynthesisValue> synthesiValueClass) throws PersistenceException;

    /**
     * Gets the synthesis values by variableNodeable and contextNodeable.
     *
     * @param nodeDatasetVariableId
     * @param contextName
     * @param variableName
     * @param synthesisValueClass
     * @return the synthesis values by variableNodeable and contextNodeable
     * @link(String) the contextNodeable
     * @link(String) the variableNodeable
     * @link(String) the datatype name
     */
    List<GenericSynthesisValue> getSynthesisValuesByVariableNodeableAndContext(Long nodeDatasetVariableId, Class<GenericSynthesisValue> synthesisValueClass);

    /**
     * Gets the synthesis values by variableNodeable and contextNodeable by
     * dates interval.
     *
     * @param contextName
     * @param start the start
     * @param end the end
     * @param node
     * @param synthesisValueClass
     * @return the synthesis values by variableNodeable and contextNodeable by
     * dates interval
     * @throws PersistenceException the persistence exception
     * @link(String) the contextNodeable
     * @link(String) the variableNodeable
     * @link(String) the datatype name
     * @link(Date) the start
     * @link(Date) the end
     */
    List<GenericSynthesisValue> getSynthesisValuesByVariableNodeableAndSiteByDatesInterval(LocalDateTime start, LocalDateTime end, NodeDataSet node, Class<GenericSynthesisValue> synthesisValueClass) throws PersistenceException;

    /**
     * Purge synthesis data.
     *
     * @throws PersistenceException the persistence exception
     */
    void purgeSynthesisData() throws PersistenceException;

    /**
     * Save or update generic.
     *
     * @param object the object
     * @throws PersistenceException the persistence exception
     * @link(Object) the object
     */
    void saveOrUpdateGeneric(Object object) throws PersistenceException;

    /**
     *
     * @param user
     * @param synthesisActivities
     * @param synthesisValueClass
     * @param datasetNodeLeafContextId
     * @return
     */
    List<NodeDataSet> getAllAvailablesVariablesSynthesisNodes(IUser user, Map<Long, Map<Group, List<LocalDate>>> synthesisActivities, Class<? extends GenericSynthesisValue> synthesisValueClass, Long datasetNodeLeafContextId);

    /**
     *
     * @param nodeDatasetContextId
     * @return
     */
    Optional<NodeDataSet> getByNodeDatasetId(Long nodeDatasetContextId);

    /**
     *
     * @return
     */
    Map<String, Map<String, Optional<DefaultSynthesisDatatype>>> getDefaultSynthesisDatatypes();

    /**
     *
     * @param datatypeName
     * @return
     */
    List<GenericSynthesisDatatype> getAllDefaultSynthesisDatatypes(String datatypeName);

    /**
     *
     * @return
     */
    List<String> getDefaultSynthesisDatatypeNames();
}
