package org.inra.ecoinfo.synthesis.impl;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Stream;
import javax.faces.context.FacesContext;
import javax.persistence.Transient;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.DefaultTreeNode;
import org.inra.ecoinfo.mga.business.IMgaServiceBuilder;
import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.caching.ITreeApplicationCacheManager;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.viewadapter.ISkeletonBuilder;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitativeDAO;
import org.inra.ecoinfo.synthesis.GenericMetaSynthesisDatatype;
import org.inra.ecoinfo.synthesis.IJPASynthesisValueDAO;
import org.inra.ecoinfo.synthesis.ILocalizedFormatter;
import org.inra.ecoinfo.synthesis.INodeableSynthesisDAO;
import org.inra.ecoinfo.synthesis.ISynthesisDAO;
import org.inra.ecoinfo.synthesis.ISynthesisManager;
import org.inra.ecoinfo.synthesis.ISynthesisPredicate;
import org.inra.ecoinfo.synthesis.ISynthesisRegister;
import org.inra.ecoinfo.synthesis.SynthesisDatatype;
import org.inra.ecoinfo.synthesis.SynthesisUtils;
import org.inra.ecoinfo.synthesis.entity.DefaultSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class LocalSynthesisManager.
 *
 * @author "Antoine Schellenberger"
 */
public class LocalSynthesisManager extends MO implements ISynthesisManager, ISkeletonBuilder {

    /**
     *
     */
    public static final String BUNDLE_SOURCE_PATH_SECURITY = "org.inra.ecoinfo.security.messages";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.synthesis.impl.messages";
    /**
     * The Constant MSG_BUILD_DATAS_SYNTHESIS_DUPLICATE @link(String).
     */
    private static final String MSG_BUILD_DATAS_SYNTHESIS_DUPLICATE = "PROPERTY_MSG_BUILD_DATAS_SYNTHESIS_DUPLICATE";
    /**
     * The Constant MSG_BUILD_DATAS_SYNTHESIS_ERROR @link(String).
     */
    private static final String MSG_BUILD_DATAS_SYNTHESIS_ERROR = "PROPERTY_MSG_BUILD_DATAS_SYNTHESIS_ERROR";
    /**
     * The Constant MSG_BUILD_DATAS_SYNTHESIS_FINISHED @link(String).
     */
    private static final String MSG_BUILD_DATAS_SYNTHESIS_FINISHED = "PROPERTY_MSG_BUILD_DATAS_SYNTHESIS_FINISHED";
    /**
     * The Constant MSG_STARTING_BUILD_DATAS_SYNTHESIS @link(String).
     */
    private static final String MSG_STARTING_BUILD_DATAS_SYNTHESIS = "PROPERTY_MSG_STARTING_BUILD_DATAS_SYNTHESIS";
    /**
     * The Constant PATTERN_STRING_FORMAT_SYNTHESISDATATYPE_CLASSNAME.
     * @link(String).
     */
    private static final String PATTERN_STRING_FORMAT_SYNTHESISDATATYPE_CLASSNAME = "%s.%s.SynthesisDatatype";
    /**
     * The Constant PATTERN_STRING_FORMAT_SYNTHESISVALUE_CLASS @link(String).
     */
    private static final String PATTERN_STRING_FORMAT_SYNTHESISVALUE_CLASS = "%s.%s.SynthesisValue";
    private static final String PARAM_MSG_NO_RIGHT_FOR_UPDATE = "PARAM_MSG_NO_RIGHT_FOR_UPDATE";
    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalSynthesisManager.class.getName());
    private static Logger SYNTHESIS_LOGGER = LoggerFactory.getLogger("synthesis.logger");
    /**
     * The mutex @link(int).
     */
    private int mutex = 0;

    /**
     *
     */
    protected ILocalizedFormatter<INodeable> localizedFormatterForVariableNameGetDisplay;

    /**
     *
     */
    protected ILocalizedFormatter<INodeable> localizedFormatterForUniteNameGetAxisName;

    /**
     *
     */
    protected ILocalizedFormatter<INodeable> localizedFormatterForVariableNameGetDescription;


    /**
     * The chart builder @link(IChartBuilder).
     */
    protected IDatatypeDAO datatypeDAO;
    /**
     * The site dao @link(ISiteDAO).
     */
    protected ISiteDAO siteDAO;
    /**
     * The synthesis dao @link(ISynthesisDAO).
     */
    protected ISynthesisDAO synthesisDAO;
    /**
     * The synthesis register @link(ISynthesisRegister).
     */
    protected ISynthesisRegister synthesisRegister;

    /**
     *
     */
    protected INodeableSynthesisDAO nodeableSynthesisDAO;
    /**
     * The valeur qualitative dao @link(IValeurQualitativeDAO).
     */
    protected IValeurQualitativeDAO valeurQualitativeDAO;
    private Class<? extends Nodeable> variableTypeResource;

    /**
     *
     */
    protected ITreeApplicationCacheManager treeApplicationCacheManager;

    /**
     *
     */
    protected IMgaServiceBuilder mgaServiceBuilder;

    /**
     *
     */
    protected ISynthesisPredicate synthesisPredicate;

    /**
     *
     * @param localizedFormatterForVariableNameGetDescription
     */
    public void setLocalizedFormatterForVariableNameGetDescription(ILocalizedFormatter<INodeable> localizedFormatterForVariableNameGetDescription) {
        this.localizedFormatterForVariableNameGetDescription = localizedFormatterForVariableNameGetDescription;
    }

    /**
     *
     * @return
     */
    public ISynthesisPredicate getSynthesisPredicate() {
        return synthesisPredicate;
    }

    /**
     *
     * @param synthesisPredicate
     */
    public void setSynthesisPredicate(ISynthesisPredicate synthesisPredicate) {
        this.synthesisPredicate = synthesisPredicate;
    }

    /**
     *
     * @param <T>
     * @return
     */
    public <T extends Nodeable> Class<T> getVariableTypeResource() {
        return (Class< T>) variableTypeResource;
    }

    /**
     *
     * @param <T>
     * @param variableTypeResource
     */
    public <T extends Nodeable> void setVariableTypeResource(Class<T> variableTypeResource) {
        this.variableTypeResource = variableTypeResource;
    }

    /**
     *
     * @param localizedFormatterForVariableNameGetDisplay
     */
    public void setLocalizedFormatterForVariableNameGetDisplay(ILocalizedFormatter<INodeable> localizedFormatterForVariableNameGetDisplay) {
        this.localizedFormatterForVariableNameGetDisplay = localizedFormatterForVariableNameGetDisplay;
    }

    /**
     *
     * @param localizedFormatterForUniteNameGetAxisName
     */
    public void setLocalizedFormatterForUniteNameGetAxisName(ILocalizedFormatter<INodeable> localizedFormatterForUniteNameGetAxisName) {
        this.localizedFormatterForUniteNameGetAxisName = localizedFormatterForUniteNameGetAxisName;
    }

    /**
     *
     * @param localizationManager
     */
    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager);
    }

    /**
     *
     * @param policyManager
     */
    @Override
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Builds the synthesis.
     *
     * @throws BusinessException the business exception
     * @see org.inra.ecoinfo.synthesis.ISynthesisManager#buildSynthesis()
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @Scheduled(cron = "0 47 1 * * MON-FRI")
    public void buildSynthesis() throws BusinessException {
        MDC.put("synthesis.date", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.YYYY_MM_DD_HHMMSS));
        MDC.put("synthesis.type", "buildSynthesis");
        testIsRoot();
        Utilisateur utilisateur = null;
        try {
            if (FacesContext.getCurrentInstance() != null) {
                utilisateur = (Utilisateur) policyManager.getCurrentUser();
                testIsRoot();
                if (mutex == 0) {
                    Optional.ofNullable(policyManager.getCurrentUser())
                            .ifPresent((user) -> {
                                MDC.put("synthesis.user.login", user.getLogin());
                                MDC.put("synthesis.user.email", user.getEmail());
                                MDC.put("synthesis.user.name", user.getNom());
                                MDC.put("synthesis.user.surname", user.getPrenom());
                            });
                    synchronisForUSer(utilisateur);
                } else {
                    throw new BusinessException(localizationManager.getMessage(LocalSynthesisManager.BUNDLE_SOURCE_PATH, LocalSynthesisManager.MSG_BUILD_DATAS_SYNTHESIS_DUPLICATE));
                }
            } else {
                MDC.put("synthesis.user.login", "cron action");
                mutexForCron();
            }
        } catch (final PersistenceException e) {
            mutexWithPersistenceException(utilisateur, e);
            MDC.put("synthesis.error", e.getMessage());
            LOGGER.error("can't get mutex", e);
        } catch (final BeanCreationException e) {
            mutexForCron();
            LOGGER.error("can't get mutex", e);
        } catch (final ClassNotFoundException | IllegalAccessException | InstantiationException | InvocationTargetException | BusinessException e) {
            mutex = 0;
            LOGGER.error("can't get mutex", e);
            MDC.put("synthesis.error", e.getMessage());
            sendNotificationErrorIfNotNullUser(utilisateur, LocalSynthesisManager.MSG_BUILD_DATAS_SYNTHESIS_ERROR, e);
        } finally {
            SYNTHESIS_LOGGER.info(String.format("%s a demandé la synthèse", MDC.get("synthesis.user.login")));
        }
    }

    /**
     * Builds the synthesis datatype v os.
     *
     * @param datatypeName the datatype name
     * @param synthesisDatatypeVOs the synthesis datatype v os
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(String) the datatype name
     * @link(List<SynthesisDatatypeVO>) the synthesis datatype v os
     */
    @SuppressWarnings("unchecked")
    public void buildSynthesisDatatypes(final String datatypeName, final List<SynthesisDatatype> synthesisDatatypeVOs) throws BusinessException {
        try {
            List<GenericSynthesisDatatype> synthesisDatatypes = getSynthesisDatatypeForDatatype(datatypeName);
            synthesisDatatypes.stream().map((synthesisDatatype) -> new SynthesisDatatype(synthesisDatatype, datatypeName)).forEach((synthesisDatatypeVO) -> {
                synthesisDatatypeVOs.add(synthesisDatatypeVO);
            });
        } catch (ClassNotFoundException ex) {
            throw new BusinessException(ex);
        }
    }

    private Class<GenericSynthesisValue> getSynthesisValueClass(String datatypeName) throws ClassNotFoundException {
        final String prefix = synthesisRegister.retrieveMetadataSynthesisDatatype(datatypeName).getPrefix();
        return (Class<GenericSynthesisValue>) Class.forName(String.format(LocalSynthesisManager.PATTERN_STRING_FORMAT_SYNTHESISVALUE_CLASS, synthesisRegister.getBasePackage(), prefix));
    }

    private Class<GenericSynthesisDatatype> getSynthesisDatatypeClass(final String datatypeName) throws ClassNotFoundException {
        final String prefix = synthesisRegister.retrieveMetadataSynthesisDatatype(datatypeName).getPrefix();
        return (Class<GenericSynthesisDatatype>) Class.forName(String.format(LocalSynthesisManager.PATTERN_STRING_FORMAT_SYNTHESISDATATYPE_CLASSNAME, synthesisRegister.getBasePackage(), prefix));
    }

    @Override
    public List<NodeDataSet> getAllRestrictedAvailablesVariablesSynthesisNodes(String datatypeName, Long datasetNodeLeafContextId) {
        final Class<? extends GenericSynthesisValue> synthesisValueClass;
        if (isGenericdatatype(datatypeName)) {
            return new LinkedList<>();
        } else {
            try {
                synthesisValueClass = getSynthesisValueClass(datatypeName);
            } catch (ClassNotFoundException ex) {
                LOGGER.error("no synthesisValue for datatype", ex);
                return new LinkedList();
            }
            final Map<Long, Map<Group, List<LocalDate>>> synthesisActivities = policyManager.getOrLoadActivities(policyManager.getCurrentUser(), WhichTree.TREEDATASET).get(Activities.synthese);
            return synthesisDAO.getAllAvailablesVariablesSynthesisNodes(policyManager.getCurrentUser(), synthesisActivities, synthesisValueClass, datasetNodeLeafContextId);

        }
    }

    /**
     * Gets the date by site and variable.
     *
     * @param contextName the site name
     * @param datatypeName the datatype name
     * @return the date by site and variable
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.synthesis.ISynthesisManager#getDateBySiteAndVariable(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public IntervalDate getDateByContextAndVariable(Long datasetNodeLeafContextId, String datatypeName) throws BusinessException {
        try {
            return synthesisDAO.getDateByContextNodeableAndVariableNodeable(datasetNodeLeafContextId, getSynthesisValueClass(datatypeName));
        } catch (final PersistenceException | ClassNotFoundException e) {
            throw new BusinessException("no variable", e);
        }
    }

    /**
     * Retrieve all synthesis datatype.
     *
     * @return the list
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.synthesis.ISynthesisManager#retrieveAllSynthesisDatatype()
     */
    @Override
    public List<SynthesisDatatype> retrieveAllSynthesisDatatype() throws BusinessException {

        final List<SynthesisDatatype> synthesisDatatypes = new LinkedList<>();
        for (final String datatypeName : synthesisRegister.getDatatypesNames()) {
            buildSynthesisDatatypes(datatypeName, synthesisDatatypes);
        }
        return synthesisDatatypes;
    }

    /**
     * Retrieve values synthesis.
     *
     * @param nodeDatasetVariableId
     * @param site the site
     * @param datatype the datatype
     * @param variable the variable
     * @return the list
     * @see
     * org.inra.ecoinfo.synthesis.ISynthesisManager#retrieveValuesSynthesis(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public List<GenericSynthesisValue> retrieveValuesSynthesis(Long nodeDatasetVariableId, Class<GenericSynthesisValue> synthesisValueClass) {
        return synthesisDAO.getSynthesisValuesByVariableNodeableAndContext(nodeDatasetVariableId, synthesisValueClass);
    }

    /**
     * Sets the datatype dao.
     *
     * @param datatypeDAO the new datatype dao
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     * Sets the synthesis dao.
     *
     * @param synthesisDAO the new synthesis dao
     */
    public void setSynthesisDAO(final ISynthesisDAO synthesisDAO) {
        this.synthesisDAO = synthesisDAO;
    }

    /**
     * Sets the synthesis register.
     *
     * @param synthesisRegister the new synthesis register
     */
    public void setSynthesisRegister(final ISynthesisRegister synthesisRegister) {
        this.synthesisRegister = synthesisRegister;
    }

    /**
     * Sets the valeur qualitative dao.
     *
     * @param valeurQualitativeDAO the new valeur qualitative dao
     */
    public void setValeurQualitativeDAO(final IValeurQualitativeDAO valeurQualitativeDAO) {
        this.valeurQualitativeDAO = valeurQualitativeDAO;
    }

    private void synchronisForUSer(final Utilisateur utilisateur) throws BusinessException, PersistenceException, ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException {
        sendNotificationIfNotNullUser(utilisateur, LocalSynthesisManager.MSG_STARTING_BUILD_DATAS_SYNTHESIS);
        mutex++;
        synthesisDAO.purgeSynthesisData();
        parseDatatypesNames();
        reBuildSynthesisTree(AbstractMgaIOConfigurator.SYNTHESIS_CONFIGURATION);
        mutex--;
        sendNotificationIfNotNullUser(utilisateur, LocalSynthesisManager.MSG_BUILD_DATAS_SYNTHESIS_FINISHED);
    }

    /**
     *
     * @param utilisateur
     * @param message
     * @throws BusinessException
     */
    protected void sendNotificationIfNotNullUser(final Utilisateur utilisateur, String message) throws BusinessException {
        if (utilisateur != null) {
            sendNotification(localizationManager.getMessage(LocalSynthesisManager.BUNDLE_SOURCE_PATH, message), Notification.INFO, null, utilisateur);
        } else {
            LOGGER.info(localizationManager.getMessage(LocalSynthesisManager.BUNDLE_SOURCE_PATH, message));
        }
    }

    /**
     *
     * @param utilisateur
     * @param message
     * @param e
     * @throws BusinessException
     */
    protected void sendNotificationErrorIfNotNullUser(final Utilisateur utilisateur, String message, Throwable e) throws BusinessException {
        if (utilisateur != null) {
            sendNotification(localizationManager.getMessage(LocalSynthesisManager.BUNDLE_SOURCE_PATH, message), Notification.ERROR, e.getMessage(), utilisateur);
        }
        throw new BusinessException(localizationManager.getMessage(LocalSynthesisManager.BUNDLE_SOURCE_PATH, message), e);

    }

    private void parseDatatypesNames() throws PersistenceException, ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException {
        for (final String datatypeName : synthesisRegister.getDatatypesNames()) {
            final IJPASynthesisValueDAO synthesisValueDAO = synthesisRegister.retrieveMetadataSynthesisDatatype(datatypeName).getSynthesisValueDAO();
            if (synthesisValueDAO != null) {
                registerSynthesisValues(datatypeName, synthesisValueDAO);
            } else {
                LOGGER.info(String.format("NO SynthesisDAO found for %s", datatypeName));
            }
        }
        Map<String, Map<String, Optional<DefaultSynthesisDatatype>>> defaultSynthesisDatatypesByDatatypeAndSite = synthesisDAO.getDefaultSynthesisDatatypes();

        GenericMetaSynthesisDatatype genericMetaSynthesisDatatype = new GenericMetaSynthesisDatatype("generic");
        for (Map.Entry<String, Map<String, Optional<DefaultSynthesisDatatype>>> entryByDatatype : defaultSynthesisDatatypesByDatatypeAndSite.entrySet()) {
            String datatype = entryByDatatype.getKey();
            Map<String, Optional<DefaultSynthesisDatatype>> defaultSynthesisDatatypesBySite = entryByDatatype.getValue();
            for (Map.Entry<String, Optional<DefaultSynthesisDatatype>> entryBySite : defaultSynthesisDatatypesBySite.entrySet()) {
                String site = entryBySite.getKey();
                entryBySite.getValue().ifPresent(defaultSynthesisDatatype -> {
                    try {
                        synthesisDAO.saveOrUpdate(defaultSynthesisDatatype);
                    } catch (PersistenceException ex) {
                        LOGGER.error(ex.getMessage());
                    }
                });
            }
        }

    }

    /**
     *
     * @param datatypeName
     * @param synthesisValueDAO
     * @throws ClassNotFoundException
     * @throws PersistenceException
     */
    protected void registerSynthesisValues(String datatypeName, IJPASynthesisValueDAO synthesisValueDAO) throws ClassNotFoundException, PersistenceException {
        Stream<GenericSynthesisValue> valuesForDatatypeName = synthesisValueDAO.getSynthesisValue();
        boolean hasError = valuesForDatatypeName.anyMatch(synthesisValue -> {
            try {
                synthesisDAO.saveOrUpdateGeneric(synthesisValue);
                return false;
            } catch (PersistenceException ex) {
                LOGGER.error(String.format("can't register synthesis value for %s", datatypeName), ex);
                return true;
            }
        });
        if (hasError) {
            throw new PersistenceException();
        }
        registerSynthesisDatatype(datatypeName, synthesisValueDAO);
    }

    private void mutexWithPersistenceException(final Utilisateur utilisateur, final PersistenceException e) throws BusinessException {
        mutex = 0;
        sendNotificationErrorIfNotNullUser(utilisateur, LocalSynthesisManager.MSG_BUILD_DATAS_SYNTHESIS_ERROR, e);
    }

    private void mutexForCron() throws BusinessException {
        try {
            if (mutex == 0) {
                mutex++;
                synthesisDAO.purgeSynthesisData();
                parseDatatypesNames();
                reBuildSynthesisTree(AbstractMgaIOConfigurator.SYNTHESIS_CONFIGURATION);
                mutex--;
            }
        } catch (final ClassNotFoundException | IllegalAccessException | InstantiationException | InvocationTargetException | PersistenceException ex) {
            mutex = 0;
            throw new BusinessException("mutex error " + ex.getMessage(), ex);
        }
    }

    /**
     *
     * @param datatypeName
     * @param datasetcontextNodeId
     * @return
     * @throws ClassNotFoundException
     */
    @Override
    public Optional<GenericSynthesisDatatype> getSynthesisDatatypeForDatatypeAndNode(final String datatypeName, long datasetcontextNodeId) throws ClassNotFoundException {
        if (isGenericdatatype(datatypeName)) {
            return synthesisDAO.getAllDefaultSynthesisDatatypes(datatypeName).stream()
                    .filter(sd -> sd.getIdNodesStream().anyMatch(idN -> idN.equals(datasetcontextNodeId)))
                    .findFirst();
        } else {
            Class<?> synthesisDatatypeClass = getSynthesisDatatypeClass(datatypeName);
            return getSynthesisDatatypeForDatatype(datatypeName).stream()
                    .filter(sd -> sd.getIdNodesStream().anyMatch(idN -> idN.equals(datasetcontextNodeId)))
                    .findFirst();
        }
    }

    /**
     *
     * @param datatypeName
     * @return
     */
    @Override
    public boolean isGenericdatatype(final String datatypeName) {
        return synthesisRegister.retrieveMetadataSynthesisDatatype(datatypeName) instanceof GenericMetaSynthesisDatatype;
    }

    /**
     *
     * @param datatypeName
     * @return
     * @throws ClassNotFoundException
     */
    protected List<GenericSynthesisDatatype> getSynthesisDatatypeForDatatype(final String datatypeName) throws ClassNotFoundException {
        final List<GenericSynthesisDatatype> synthesisDatatypes;
        if (isGenericdatatype(datatypeName)) {
            synthesisDatatypes = synthesisDAO.getAllDefaultSynthesisDatatypes(datatypeName);
        } else {
            Class<?> synthesisDatatypeClass = getSynthesisDatatypeClass(datatypeName);
            synthesisDatatypes = synthesisDAO.getAllSynthesisDatatypes((Class<GenericSynthesisDatatype>) synthesisDatatypeClass);
        }
        return synthesisDatatypes;
    }

    /**
     *
     * @throws BusinessException
     */
    protected void testIsRoot() throws BusinessException {
        if (!policyManager.isRoot()) {
            String message = localizationManager.getMessage(BUNDLE_SOURCE_PATH_SECURITY, PARAM_MSG_NO_RIGHT_FOR_UPDATE);
            notificationsManager.addNotification(buildNotification(Notification.INFO, message, null, null), policyManager.getCurrentUserLogin());
            throw new BusinessException(message);
        }
    }

    /**
     *
     * @param variableNodeId
     * @param activity
     * @return
     */
    @Override
    public boolean hasRoleForNodeIdAndActivity(Long variableNodeId, Activities activity) {
        if (policyManager.isRoot()) {
            return true;
        }
        final Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> rolesMap = policyManager.getOrLoadActivities(policyManager.getCurrentUser(), WhichTree.TREEDATASET);
        if (rolesMap == null || rolesMap.get(activity) == null) {
            return false;
        }
        return rolesMap.get(activity).containsKey(variableNodeId);
    }

    /**
     *
     * @param datatypeName
     * @param synthesisValueDAO
     * @throws ClassNotFoundException
     * @throws PersistenceException
     */
    protected void registerSynthesisDatatype(String datatypeName, IJPASynthesisValueDAO synthesisValueDAO) throws ClassNotFoundException, PersistenceException {
        Stream<GenericSynthesisDatatype> datatypeForDatatypeName = synthesisValueDAO.getSynthesisDatatype();
        boolean hasError = datatypeForDatatypeName.anyMatch(synthesisDatatype -> {
            try {
                synthesisDAO.saveOrUpdateGeneric(synthesisDatatype);
                return false;
            } catch (PersistenceException ex) {
                LOGGER.error(String.format("can't register synthesis datatype for %s", datatypeName), ex);
                return true;
            }
        });
        if (hasError) {
            throw new PersistenceException();
        }
    }

    private ITreeNode reBuildSynthesisTree(int configuration) throws BusinessException {
        List<String> genericDatatypesNames = synthesisDAO.getDefaultSynthesisDatatypeNames();
        synthesisRegister.addGenericMetaSynthesisDatatypes(genericDatatypesNames);
        SortedMap<Long, GenericSynthesisDatatype> synthesisDatatypes = new TreeMap();
        synthesisRegister.getDatatypesNames()
                .stream()
                .forEach(datatypeName -> {
                    List<GenericSynthesisDatatype> synthesisdatatypes;
                    try {
                        synthesisdatatypes = getSynthesisDatatypeForDatatype(datatypeName);
                    } catch (ClassNotFoundException ex) {
                        LOGGER.error(String.format("can't getSynthesisDatatype for datatype %s", datatypeName), ex);
                        synthesisdatatypes = new LinkedList<>();
                    }
                    synthesisdatatypes
                            .stream()
                            .forEach(synthesisDatatype -> {
                                synthesisDatatype.getIdNodesList().stream()
                                        .forEach(id -> synthesisDatatypes.put(id, synthesisDatatype));
                            });
                });
        synthesisPredicate = synthesisPredicate.getInstance(synthesisDatatypes);
        Stream<INode> synthesisNodes0 = mgaServiceBuilder
                .loadNodes(mgaServiceBuilder.getMgaIOConfigurator().getConfiguration(configuration)
                        .orElseThrow(() -> new BusinessException("no synthesis configuration number")),
                        true,
                        true
                )
                .map(synthesisPredicate)
                .flatMap(List::stream);;
        return treeApplicationCacheManager.buildSkeletonTree(mgaServiceBuilder.getMgaIOConfigurator().getConfiguration(configuration).get(), synthesisNodes0, true);

//        synthesisRegister.getDatatypesNames()
//                .stream()
//                .forEach(datatypeName -> getSynthesisDatatypeForDatatypeLogError(datatypeName, pathes));
//        synthesisPredicate = synthesisPredicate.getInstance(pathes);
//        Stream<INode> synthesisNodes = mgaServiceBuilder
//                .loadNodes(mgaServiceBuilder.getMgaIOConfigurator().getConfiguration(AbstractMgaIOConfigurator.SYNTHESIS_CONFIGURATION)
//                        .orElseThrow(() -> new BusinessException("no synthesis configuration number")),
//                        true,
//                        true
//                )
//                .map(synthesisPredicate)
//                .flatMap(List::stream);
//        return treeApplicationCacheManager.buildSkeletonTree(mgaServiceBuilder.getMgaIOConfigurator().getConfiguration(AbstractMgaIOConfigurator.SYNTHESIS_CONFIGURATION).get(), synthesisNodes);
    }

//    /**
//     *
//     * @param datatypeName
//     * @param pathes
//     */
//    protected void getSynthesisDatatypeForDatatypeLogError(String datatypeName, SortedMap<String, GenericSynthesisDatatype> pathes) {
//        try {
//            getSynthesisDatatypeForDatatype(datatypeName).stream()
//                    .forEach((sdt) -> pathes.put(String.format("%s@@%s", sdt.getSite(), Utils.createCodeFromString(datatypeName)), sdt));
//        } catch (ClassNotFoundException ex) {
//            LOGGER.error(String.format("can't getSynthesisDatatype for datatype %s", datatypeName), ex);
//        }
//    }
    /**
     *
     * @param treeApplicationCacheManager
     */
    public void setTreeApplicationCacheManager(ITreeApplicationCacheManager treeApplicationCacheManager) {
        this.treeApplicationCacheManager = treeApplicationCacheManager;
    }

    /**
     *
     * @param mgaServiceBuilder
     */
    public void setMgaServiceBuilder(IMgaServiceBuilder mgaServiceBuilder) {
        this.mgaServiceBuilder = mgaServiceBuilder;
    }

    /**
     *
     * @param nodePath
     * @return
     */
    @Override
    public Optional<String> getSynthesisPath(String nodePath) {
        return synthesisPredicate.getSynthesisPath(nodePath);
    }

    /**
     *
     * @param config
     * @return
     */
    @Override
    public ITreeNode<AbstractBranchNode> getOrLoadSkeletonTree(IMgaIOConfiguration config) {
        ITreeNode<AbstractBranchNode> root = new DefaultTreeNode(null, null, null, config);
        if (treeApplicationCacheManager.getSkeletonTree(config.getCodeConfiguration()) != null) {
            root = treeApplicationCacheManager.getSkeletonTree(config.getCodeConfiguration());
        } else {
            try {
                root = reBuildSynthesisTree(config.getCodeConfiguration());
            } catch (BusinessException ex) {
                LOGGER.error("can't load synthesisSkeleton", ex);
            }
        }
        return root;
    }

    /**
     *
     * @param context
     * @param dateDebut
     * @param dateFin
     * @param node
     * @param synthesisValueClass
     * @param activities
     * @return
     * @throws BusinessException
     */
    @Override
    public Stream<GenericSynthesisValue> getSynthesisValuesByVariableNodeableAndSiteByDatesInterval(LocalDateTime dateDebut, LocalDateTime dateFin, NodeDataSet node, Class<GenericSynthesisValue> synthesisValueClass, List<Activities> activities) throws BusinessException {
        try {
            Boolean isRoot = policyManager.getCurrentUser().getIsRoot();
            final Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> activitiesMap = policyManager.getOrLoadActivities(policyManager.getCurrentUser(), WhichTree.TREEDATASET);
            return synthesisDAO.getSynthesisValuesByVariableNodeableAndSiteByDatesInterval(dateDebut, dateFin, node, synthesisValueClass)
                    .stream()
                    .filter(sv -> isRoot || activities.stream().anyMatch(activity -> SynthesisUtils.isBetween(sv.getDate(), activitiesMap.getOrDefault(activity, new HashMap<>()).getOrDefault(node.getId(), new HashMap<>()))));
        } catch (PersistenceException ex) {
            throw new BusinessException(ex);
        }
    }
}
