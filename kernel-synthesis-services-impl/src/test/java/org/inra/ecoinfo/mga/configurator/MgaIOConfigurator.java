package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.mga.business.composite.INodeable;
import static org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 *
 * @author yahiaoui
 */
public class MgaIOConfigurator extends AbstractMgaIOConfigurator {

    private static final Integer[] NORMAL_ORDER_0_1_2_3 = new Integer[]{0, 1, 2, 3};
    private static final Integer[] NORMAL_ORDER_0_1_2 = new Integer[]{0, 1, 2};
    private static final Integer[] DATASET_ORDER_0 = new Integer[]{0};
    private static final Class<INodeable>[] ENTRY_INSTANCE_STD = new Class[]{
            Site.class,
            Theme.class,
            DataType.class
        };
    private static final Class<INodeable>[] ENTRY_INSTANCE_STDV = new Class[]{
            Site.class,
            Theme.class,
            DataType.class,
            Variable.class
        };
    private static final Activities[] ACTIVITIES_SAPDE
                = new Activities[]{
                    Activities.synthese,
                    Activities.administration,
                    Activities.publication,
                    Activities.depot,
                    Activities.suppression,
                    Activities.extraction};
    private static final Activities[] ACTIVITIES_ESTA
                = new Activities[]{
                    Activities.edition,
                    Activities.suppression,
                    Activities.telechargement,
                    Activities.administration};
    private static final Activities[] ACTIVITIES_REF
                = new Activities[]{
                    Activities.associate
                };
    private static final Class<INodeable>[] ENTRY_INSTANCE_REF = new Class[]{
            Refdata.class
        };
    Integer[] entryOrderRefData = DATASET_ORDER_0;
    Integer[] sortOrderRefdata = DATASET_ORDER_0;


    /**
     *
     */
    public MgaIOConfigurator() {
        super(new AbstractMgaDisplayerConfiguration() {
        });
    }

    /**
     *
     */
    @Override
    protected void initConfigurations() {

        /**
         * Configuration Zero
         */

        boolean includeAncestorDataset = true;

        WhichTree whichTreeDataSet = WhichTree.TREEDATASET;

        Class<DatatypeVariableUnite> stickyLeafDataset = null;
        Class<DatatypeVariableUnite> stickyLeafDatasetRights = DatatypeVariableUnite.class;

        boolean displayColumnNamesDataset = false;

        AbstractMgaIOConfiguration datasetConfigurationRights
                = new ConfigurationTest(
                        DATASET_CONFIGURATION_RIGHTS,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_STDV,
                        ACTIVITIES_SAPDE,
                        NORMAL_ORDER_0_1_2_3,
                        includeAncestorDataset,
                        whichTreeDataSet,
                        stickyLeafDatasetRights,
                        displayColumnNamesDataset);

        AbstractMgaIOConfiguration datasetConfiguration
                = new ConfigurationTest(
                        DATASET_CONFIGURATION,
                        DataType.class,
                        NORMAL_ORDER_0_1_2,
                        ENTRY_INSTANCE_STD,
                        ACTIVITIES_SAPDE,
                        NORMAL_ORDER_0_1_2,
                        includeAncestorDataset,
                        whichTreeDataSet,
                        stickyLeafDataset,
                        displayColumnNamesDataset);

        /**
         * Configuration Zero
         */

        boolean includeAncestorRefdata = true;

        WhichTree whichTreeRefdata = WhichTree.TREEREFDATA;

        Class<DatatypeVariableUnite> stickyLeafRefdata = null;

        boolean displayColumnNamesRefdata = false;

        AbstractMgaIOConfiguration refDataConfigurationRights
                = new ConfigurationTest(REFDATA_CONFIGURATION_RIGHTS,
                        Refdata.class,
                        entryOrderRefData,
                        ENTRY_INSTANCE_REF,
                        ACTIVITIES_ESTA,
                        sortOrderRefdata,
                        includeAncestorRefdata,
                        whichTreeRefdata,
                        stickyLeafRefdata,
                        displayColumnNamesRefdata);

        AbstractMgaIOConfiguration refDataConfiguration
                = new ConfigurationTest(REFDATA_CONFIGURATION,
                        Refdata.class,
                        entryOrderRefData,
                        ENTRY_INSTANCE_REF,
                        ACTIVITIES_ESTA,
                        sortOrderRefdata,
                        includeAncestorRefdata,
                        whichTreeRefdata,
                        stickyLeafRefdata,
                        displayColumnNamesRefdata);

        AbstractMgaIOConfiguration extractionConfiguration
                = new ConfigurationTest(SYNTHESIS_CONFIGURATION,
                        DataType.class,
                        NORMAL_ORDER_0_1_2,
                        ENTRY_INSTANCE_STD,
                        ACTIVITIES_SAPDE,
                        NORMAL_ORDER_0_1_2,
                        includeAncestorDataset,
                        whichTreeDataSet,
                        stickyLeafDataset,
                        displayColumnNamesDataset);

        /**
         * Configuration Associate // For RefData Insertion
         */
        AbstractMgaIOConfiguration associateConfiguration
                = new ConfigurationTest(ASSOCIATE_CONFIGURATION,
                        DataType.class,
                        NORMAL_ORDER_0_1_2,
                        ENTRY_INSTANCE_STD,
                        ACTIVITIES_REF,
                        NORMAL_ORDER_0_1_2,
                        includeAncestorDataset,
                        whichTreeDataSet,
                        stickyLeafDataset,
                        displayColumnNamesDataset);

        /**
         * ------------------------------------------------------------------------------------
         * *
         */
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION_RIGHTS, k -> datasetConfigurationRights);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION_RIGHTS, k -> refDataConfigurationRights);
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION, k -> datasetConfiguration);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION, k -> refDataConfiguration);
        getConfigurations().computeIfAbsent(ASSOCIATE_CONFIGURATION, k -> associateConfiguration);
        getConfigurations().computeIfAbsent(SYNTHESIS_CONFIGURATION, k -> extractionConfiguration);

        /**
         * ------------------------------------------------------------------------------------
         * *
         */
    }

    class ConfigurationTest extends AbstractMgaIOConfiguration {

        public ConfigurationTest(Integer codeConfiguration, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfiguration, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

    }
}
