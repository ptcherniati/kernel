package org.inra.ecoinfo.dataset.versioning.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * @author ptcherniati
 */
public class VersionFileTest {

    @Mock
    Dataset dataset;
    VersionFile versionFile = new VersionFile();

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        versionFile.setDataset(dataset);
    }

    /**
     *
     */
    @Test
    public void testGetFileSizeAffichage() {
        versionFile.setFileSize(25);
        Assert.assertEquals("25 bytes", versionFile.getFileSizeAffichage());
        versionFile.setFileSize(1_023);
        Assert.assertEquals("1023 bytes", versionFile.getFileSizeAffichage());
        versionFile.setFileSize(1_024);
        Assert.assertEquals("1 KB", versionFile.getFileSizeAffichage());
        versionFile.setFileSize(10_245);
        Assert.assertEquals("10 KB", versionFile.getFileSizeAffichage());
        versionFile.setFileSize(10_245_265);
        Assert.assertEquals("10 MB", versionFile.getFileSizeAffichage());
        versionFile.setFileSize(1_048_575);
        Assert.assertEquals("1024 KB", versionFile.getFileSizeAffichage());
        versionFile.setFileSize(1_048_576);
        Assert.assertEquals("1 MB", versionFile.getFileSizeAffichage());
    }

    /**
     *
     */
    @Test
    public void testSetData() {
        versionFile.setData(new byte[0]);
        Assert.assertTrue(versionFile.getData().length == 0);
        versionFile.setData("bonjour".getBytes());
        Assert.assertTrue(versionFile.getData().length == 7);
        Assert.assertEquals("bonjour", new String(versionFile.getData()));
    }
}
