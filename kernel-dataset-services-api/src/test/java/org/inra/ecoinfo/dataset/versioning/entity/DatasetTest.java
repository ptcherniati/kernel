package org.inra.ecoinfo.dataset.versioning.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.dataset.IFileNameChecker;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.config.impl.DataTypeDescription;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 * @author ptcherniati
 */
public class DatasetTest {

    @Mock
    VersionFile versionFile;
    @Mock
    IDatasetConfiguration configuration;
    @Mock
    VersionFile versionFile2;
    @Mock
    DataType datatype;
    @Mock
    RealNode nodeDataSet;
    Dataset dataset;
    LocalDateTime beginDate;
    LocalDateTime endDate;

    /**
     * @throws DateTimeParseException
     */
    @Before
    public void setUp() throws DateTimeParseException {
        MockitoAnnotations.openMocks(this);
        dataset = new Dataset();
        beginDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "13/07/1985");
        endDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "17/11/1986");
    }

    /**
     *
     */
    @Test
    public void testAddVersion() {
        dataset.addVersion(versionFile);
        dataset.addVersion(versionFile);
        Mockito.verify(versionFile, Mockito.times(2)).setDataset(dataset);
        Mockito.verify(versionFile).setVersionNumber(1);
        Mockito.verify(versionFile).setVersionNumber(2);
    }

    /**
     * @throws DateTimeParseException
     */
    @Test
    public void testBuildDownloadFilename() throws DateTimeParseException {
        dataset.setRealNode(nodeDataSet);
        dataset.setDateDebutPeriode(beginDate);
        dataset.setDateFinPeriode(endDate);
        final String place = "site_sous-site";
        List<DataTypeDescription> descriptions = new LinkedList<>();
        DataTypeDescription description = Mockito.mock(DataTypeDescription.class);
        descriptions.add(description);
        when(configuration.getDataTypes()).thenReturn(descriptions);
        when(description.getCode()).thenReturn("pasdatatype");
        when(nodeDataSet.getCode()).thenReturn("datatype");
        when(configuration.getFileNameDateFormat("datatype")).thenReturn("dd-MM-yyyy", "MM-yy");
        RealNode site = Mockito.mock(RealNode.class);
        when(site.getCode()).thenReturn(place);
        when(nodeDataSet.getNodeByNodeableTypeResource(Site.class)).thenReturn(site);
        Mockito.when(nodeDataSet.getDepositPlacePrefixForFileName()).thenReturn(place);
        Mockito.when(nodeDataSet.getPath()).thenReturn("site_sous-site_datatype");
        Mockito.when(datatype.getCode()).thenReturn("datatype");
        when(nodeDataSet.getNodeByNodeableTypeResource(DataType.class)).thenReturn(nodeDataSet);
        when(nodeDataSet.getNodeable()).thenReturn(datatype);
        String name = dataset.buildDownloadFilename(configuration);
        Assert.assertEquals("site_sous-site_datatype_13-07-1985_17-11-1986.csv", name);
        name = dataset.buildDownloadFilename(configuration);
        Assert.assertEquals("site_sous-site_datatype_07-85_11-86.csv", name);
        when(description.getCode()).thenReturn("datatype");
        // use existing fileNameChecker
        IFileNameChecker fileNameChecker = Mockito.mock(IFileNameChecker.class);
        when(fileNameChecker.getFilePath(dataset)).thenReturn("site_sous-site_datatype_07-85_11-86.csv");
        List<IFileNameChecker> fileNameCheckers = Stream.of(fileNameChecker).collect(Collectors.toList());
        when(description.getFileCheckers()).thenReturn(fileNameCheckers);
        name = dataset.buildDownloadFilename(configuration);
        Assert.assertEquals("site_sous-site_datatype_07-85_11-86.csv", name);
        Mockito.verify(fileNameChecker).getFilePath(dataset);
    }

    /**
     *
     */
    @Test
    public void testGetCollectionVersions() {
        dataset.addVersion(versionFile);
        dataset.addVersion(versionFile2);
        final List<VersionFile> versionFiles = dataset.getCollectionVersions();
        Assert.assertTrue(versionFiles.size() == 2);
        Assert.assertEquals(versionFiles.get(0), versionFile);
        Assert.assertEquals(versionFiles.get(1), versionFile2);
    }

    /**
     * @throws BadExpectedValueException
     */
    @Test
    public void testGetIntervalDate() throws BadExpectedValueException {
        dataset.setDateDebutPeriode(beginDate);
        dataset.setDateFinPeriode(endDate);
        final IntervalDate intervalDate = dataset.getIntervalDate();
        Assert.assertEquals("13/07/1985", intervalDate.getBeginDateToString());
        Assert.assertEquals("17/11/1986", intervalDate.getEndDateToString());
    }

    /**
     *
     */
    @Test
    public void testGetLastVersionNumber() {
        Assert.assertTrue(dataset.getLastVersionNumber() == 0);
        dataset.addVersion(versionFile);
        Assert.assertTrue(dataset.getLastVersionNumber() == 1);
        dataset.addVersion(versionFile2);
        Assert.assertTrue(dataset.getLastVersionNumber() == 2);
    }
}
