package org.inra.ecoinfo.dataset.config;

import java.util.List;
import org.inra.ecoinfo.dataset.config.impl.DataTypeDescription;

/**
 * The Interface IDatasetConfiguration.
 */
public interface IDatasetConfiguration {

    /**
     * Adds the data type.
     *
     * @param dataType the data type
     */
    void addDataType(DataTypeDescription dataType);

    /**
     * Gets the data types.
     *
     * @return the data types
     */
    List<DataTypeDescription> getDataTypes();

    /**
     * Sets the data types.
     *
     * @param dataTypes the new data types
     */
    void setDataTypes(List<DataTypeDescription> dataTypes);

    /**
     *
     * @param datatypeCode
     * @return
     */
    String getFileNameDateFormat(String datatypeCode);
}
