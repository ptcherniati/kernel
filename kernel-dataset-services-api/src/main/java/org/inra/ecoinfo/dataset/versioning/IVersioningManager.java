package org.inra.ecoinfo.dataset.versioning;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface IVersioningManager.
 */
public interface IVersioningManager {

    /**
     * Check version.
     *
     * @param version the version
     * @throws BusinessException the business exception
     * @link(VersionFile) the version
     */
    void checkVersion(VersionFile version) throws BusinessException;

    /**
     * Delete version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    void deleteVersion(VersionFile versionFile) throws BusinessException;

    /**
     * Download version by id.
     *
     * @param versionFileId the version file id
     * @return the byte[]
     * @throws BusinessException the business exception
     * @link(Long) the version file id
     */
    byte[] downloadVersionById(Long versionFileId) throws BusinessException;

    /**
     * Gets the version file by id.
     *
     * @param versionFileId the version file id
     * @return the version file by id
     * @throws BusinessException the business exception
     * @link(Long) the version file id
     */
    VersionFile getVersionFileById(Long versionFileId) throws BusinessException;

    /**
     * Gets the version file helper resolver.
     *
     * @return the version file helper resolver
     */
    IVersionFileHelperResolver getVersionFileHelperResolver();

    /**
     * Checks if is valid file name.
     *
     * @param fileName the file name
     * @param realNodeLeafId
     * @return true, if successful
     * @throws BusinessException the business exception
     * @link(String) the file name
     * @link(VersionFile) the version
     */
    Optional<VersionFile> testIsValidFileNameAndGetNewVersion(String fileName, Long realNodeLeafId) throws BusinessException;

    /**
     * Publish version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    void publishVersion(VersionFile versionFile) throws BusinessException;

    /**
     * Retrieve datasets.
     *
     * @param leafNodeId
     * @return the list
     * @throws BusinessException the business exception*/
    List<Dataset> retrieveDatasets( Long leafNodeId) throws BusinessException;

    /**
     * Un publish.
     *
     * @param versionFile the version file
     * @return 
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    int unPublish(VersionFile versionFile) throws BusinessException;

    /**
     * Upload version.
     *
     * @param version the version
     * @return the dataset
     * @throws BusinessException the business exception
     * @link(VersionFile) the version
     */
    Dataset uploadVersion(VersionFile version) throws BusinessException;
    
    /**
     * anonymiseUserVersionsFile.
     *
     * @param utilisateur
     * @throws PersistenceException
     * @link(VersionFile) the version
     */
    void anonymiseUserVersionsFile(Utilisateur utilisateur) throws PersistenceException;
}
