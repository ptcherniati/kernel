package org.inra.ecoinfo.dataset;

/**
 * The Interface IRequestProperties. This interface can be used to store
 * retrieved information in data processing. It will be passed to functions
 * throughout the treatment.
 */
public interface IRequestProperties {

}
