/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.dataset.versioning.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ptchernia
 */
@Entity
public class GenericDatatype {

    /**
     *
     */
    public static final String ID_JPA = "generic_datatype_id";
    
    @Id
    @Column(name = GenericDatatype.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    Long id;
    /**
     * The dataset @link(Dataset).
     */
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST}, optional = false)
    @JoinColumn(referencedColumnName = Dataset.ID_JPA, nullable = false, name = Dataset.ID_JPA)
    private Dataset dataset;

    /**
     *
     */
    public GenericDatatype() {
    }

    /**
     *
     * @param dtst
     */
    public GenericDatatype(Dataset dataset) {
        this.dataset = dataset;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Dataset getDataset() {
        return dataset;
    }

    /**
     *
     * @param dataset
     */
    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }
    
}
