package org.inra.ecoinfo.dataset.versioning.entity;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 * The Class Dataset.
 *
 * @author Philippe TCHERNIATINSKY Entity implementation class for Entity:
 * Dataset
 */
@Entity
@Table(name = "insertion_dataset_ids",
        uniqueConstraints = @UniqueConstraint(columnNames = {
    RealNode.ID_JPA,
    Dataset.DATE_DEBUT_ID,
    Dataset.DATE_FIN_ID}),
        indexes = {
            @Index(name = "ids_createUser_idx", columnList = "ids_create_user")
            ,
            @Index(name = "ids_publish_idx", columnList = "ids_publish_user")
            ,
            @Index(name = "ids_datedebut_idx", columnList = Dataset.DATE_DEBUT_ID)
            ,
            @Index(name = "ids_datefin_idx", columnList = Dataset.DATE_FIN_ID)
            ,
            @Index(name = "ids_dtn_idx", columnList = RealNode.ID_JPA)
            ,
            @Index(name = "ids_version_idx", columnList = VersionFile.ID_JPA)}
)
public class Dataset implements Comparable<Dataset>, Serializable {

    /**
     * The Constant DATE_DEBUT_ID @link(String).
     */
    public static final String DATE_DEBUT_ID = "ids_date_debut_periode";
    /**
     * The Constant DATE_FIN_ID @link(String).
     */
    public static final String DATE_FIN_ID = "ids_date_fin_periode";
    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "ids_id";
    /**
     * Default serial.
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     * @return
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    /**
     * The create date @link(Date).
     */
    /**
     * The id @link(Long).
     */
    @Id
    @Column(name = Dataset.ID_JPA)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected Long id;
    @Column(nullable = false, unique = false, name = "ids_create_date")
    @CreationTimestamp
    private LocalDateTime createDate;
    /**
     * The create user @link(Utilisateur).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(referencedColumnName = Utilisateur.UTILISATEUR_NAME_ID, nullable = false, name = "ids_create_user")
    private Utilisateur createUser;
    /**
     * The date debut periode @link(Date).
     */
    @Column(nullable = false, unique = false, name = Dataset.DATE_DEBUT_ID)
    @NaturalId
    private LocalDateTime dateDebutPeriode;
    /**
     * The date fin periode @link(Date).
     */
    @Column(nullable = false, unique = false, name = Dataset.DATE_FIN_ID)
    @NaturalId
    private LocalDateTime dateFinPeriode;
    /**
     * The publish comment @link(String).
     */
    @Column(nullable = true, unique = false, name = "ids_publish_comment")
    private String publishComment;
    /**
     * The publish date @link(Date).
     */
    @Column(nullable = true, unique = false, name = "ids_publish_date")
    private LocalDateTime publishDate;
    /**
     * The publish user @link(Utilisateur).
     */
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true)
    @JoinColumn(referencedColumnName = Utilisateur.UTILISATEUR_NAME_ID, name = "ids_publish_user")
    private Utilisateur publishUser;
    /**
     * The publish version @link(VersionFile).
     */
    @OneToOne(cascade = ALL, optional = true)
    @JoinColumn(name = VersionFile.ID_JPA)
    private VersionFile publishVersion;
    /**
     * The leaf node @link(LeafNode).
     */
    @ManyToOne(cascade = MERGE, optional = false)
    @JoinColumn(referencedColumnName = RealNode.ID_JPA, nullable = false, name = RealNode.ID_JPA)
    @NaturalId
    private RealNode realNode;
    /**
     * The versions @link(Map<Long,VersionFile>).
     */
    @OneToMany(mappedBy = "dataset", cascade = {ALL}, fetch = FetchType.EAGER)
    @LazyToOne(LazyToOneOption.PROXY)
    @MapKey(name = "versionNumber")
    @OrderBy("versionNumber")
    private Map<Long, VersionFile> versions = new HashMap<>();
    
    @Transient
    boolean sanitize=true;

    /**
     * Instantiates a new dataset.
     */
    public Dataset() {
        super();
    }

    /**
     * Adds the version.
     *
     * @param iVersion the i version
     * @link(VersionFile) the i version
     */
    public void addVersion(final VersionFile iVersion) {
        final VersionFile version = iVersion;
        synchronized (this) {
            final Long versionNumber = getLastVersionNumber() + 1;
            version.setVersionNumber(versionNumber);
            version.setDataset(this);
            getVersions().put(versionNumber, version);
        }
    }

    /**
     *
     * @param configuration
     * @return
     */
    public String buildDownloadFilename(IDatasetConfiguration configuration) {
        String fileName = "";
        try {
            fileName = configuration.getDataTypes().stream()
                    .filter(d -> 
                            d.getCode().equals(getRealNode().getNodeByNodeableTypeResource(DataType.class).getNodeable().getCode()))
                    .findFirst()
                    .map(r -> 
                            r.getFileCheckers().stream().findFirst().orElse(null))
                    .map(fileNameChecker -> 
                            fileNameChecker.getFilePath(this))
                    .orElseGet(()->defaultBuildFileName(configuration));
        } catch (final Exception e) {
            UncatchedExceptionLogger.log("can't found build name", e);
            return "";
        }
        return fileName;
    }

    private String defaultBuildFileName(IDatasetConfiguration configuration) throws DateTimeException {
        StringBuilder nomFichier = new StringBuilder();
        String site;
        site = realNode.getNodeByNodeableTypeResource(Site.class).getCode().replaceAll(PatternConfigurator.ANCESTOR_SEPARATOR, PatternConfigurator.MINUS_SEPARATOR);
        if (!Strings.isNullOrEmpty(site)) {
            nomFichier.append(site).append(PatternConfigurator.UNDERSCORE).append(realNode.getCode()).append(PatternConfigurator.UNDERSCORE);
        }
        String fileNameDateFormat = configuration.getFileNameDateFormat(realNode.getCode());
        fileNameDateFormat = fileNameDateFormat.replaceAll("/", "-");
        if (Strings.isNullOrEmpty(fileNameDateFormat)) {
            fileNameDateFormat = DateUtil.DD_MM_YYYY_FILE;
        }
        return nomFichier.append(DateUtil.getUTCDateTextFromLocalDateTime(getDateDebutPeriode(), fileNameDateFormat))
                .append(PatternConfigurator.UNDERSCORE)
                .append(DateUtil.getUTCDateTextFromLocalDateTime(getDateFinPeriode(), fileNameDateFormat))
                .append(IVersionFileHelper.EXTENSION_FILE_CSV)
                .toString();
    }

    /**
     * Compare to.
     *
     * @param o the o
     * @return the int
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final Dataset o
    ) {
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            return 0;
        }
        return new CompareToBuilder().append(this.getRealNode(), o.getRealNode()).append(this.getDateDebutPeriode(), o.getDateDebutPeriode()).append(this.getDateFinPeriode(), o.getDateFinPeriode()).toComparison();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    /**
     *
     * @param obj
     * @return
     */

    @Override
    public boolean equals(Object obj
    ) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dataset other = (Dataset) obj;
        if (!Objects.equals(this.dateDebutPeriode, other.dateDebutPeriode)) {
            return false;
        }
        if (!Objects.equals(this.dateFinPeriode, other.dateFinPeriode)) {
            return false;
        }
        return Objects.equals(this.realNode, other.realNode);
    }

    /**
     * Gets the collection versions.
     *
     * @return the collection versions
     */
    public List<VersionFile> getCollectionVersions() {
        return new LinkedList<>(versions.values());
    }

    /**
     * Gets the creates the user.
     *
     * @return the creates the user
     */
    public Utilisateur getCreateUser() {
        return createUser;
    }

    /**
     * Sets the creates the user.
     *
     * @param createUser the new creates the user
     */
    public void setCreateUser(final Utilisateur createUser) {
        this.createUser = createUser;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the interval date.
     *
     * @return the interval date
     * @throws BadExpectedValueException the bad expected value exception
     */
    @Transient
    public IntervalDate getIntervalDate() throws BadExpectedValueException {
        return new IntervalDate(getDateDebutPeriode(), getDateFinPeriode(), DateUtil.DD_MM_YYYY);
    }

    /**
     * Gets the last version.
     *
     * @return the last version
     */
    public VersionFile getLastVersion() {
        return versions.get(getLastVersionNumber());
    }

    /**
     * Gets the last version number.
     *
     * @return the last version number
     */
    public long getLastVersionNumber() {
        if (versions.isEmpty()) {
            return 0;
        }
        long lastVersionNumber = -1;
        for (final Entry<Long, VersionFile> entry : versions.entrySet()) {
            lastVersionNumber = Math.max(lastVersionNumber, entry.getKey());
        }
        return lastVersionNumber;
    }

    /**
     * Gets the publish comment.
     *
     * @return the publish comment
     */
    public String getPublishComment() {
        return publishComment;
    }

    /**
     * Sets the publish comment.
     *
     * @param publishComment the new publish comment
     */
    public void setPublishComment(final String publishComment) {
        this.publishComment = publishComment;
    }

    /**
     *
     * @param createDate
     */
    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    /**
     *
     * @param dateDebutPeriode
     */
    public void setDateDebutPeriode(LocalDateTime dateDebutPeriode) {
        this.dateDebutPeriode = dateDebutPeriode;
    }

    /**
     *
     * @param dateFinPeriode
     */
    public void setDateFinPeriode(LocalDateTime dateFinPeriode) {
        this.dateFinPeriode = dateFinPeriode;
    }

    /**
     *
     * @param publishDate
     */
    public void setPublishDate(LocalDateTime publishDate) {
        this.publishDate = publishDate;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getDateDebutPeriode() {
        return dateDebutPeriode;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getDateFinPeriode() {
        return dateFinPeriode;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getPublishDate() {
        return publishDate;
    }

    /**
     * Gets the publish user.
     *
     * @return the publish user
     */
    public Utilisateur getPublishUser() {
        return publishUser;
    }

    /**
     * Sets the publish user.
     *
     * @param publishUser the new publish user
     */
    public void setPublishUser(final Utilisateur publishUser) {
        this.publishUser = publishUser;
    }

    /**
     * Gets the publish version.
     *
     * @return the publish version
     */
    public VersionFile getPublishVersion() {
        return publishVersion;
    }

    /**
     * Sets the publish version.
     *
     * @param publishVersion the new publish version
     */
    public void setPublishVersion(final VersionFile publishVersion) {
        this.publishVersion = publishVersion;
    }

    /**
     * Gets the versions.
     *
     * @return the versions
     */
    public Map<Long, VersionFile> getVersions() {
        return versions;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param nodeDataset
     */
    public void setRealNode(RealNode nodeDataset) {
        this.realNode = nodeDataset;
    }

    /**
     * Sets the versions.
     *
     * @param versions the versions
     * @link(Map<Long,VersionFile>) the versions
     */
    public void setVersions(final Map<Long, VersionFile> versions) {
        this.versions = versions;
    }

    /**
     *
     * @return
     */
    public boolean isSanitize() {
        return sanitize;
    }

    /**
     *
     * @param sanitize
     */
    public void setSanitize(boolean sanitize) {
        this.sanitize = sanitize;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    /**
     *
     * @return
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (dateDebutPeriode == null ? 0 : dateDebutPeriode.hashCode());
        result = prime * result + (dateFinPeriode == null ? 0 : dateFinPeriode.hashCode());
        result = prime * result + (realNode == null ? 0 : realNode.hashCode());
        return result;
    }
}
