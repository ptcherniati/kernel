package org.inra.ecoinfo.dataset.versioning;

import java.io.Serializable;
import java.util.Optional;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IVersionManager.
 */
public interface IVersionManager extends Serializable{

    /**
     * Gets the download file name.
     *
     * @param versionFile the version file
     * @return the download file name
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    String getDownloadFileName(VersionFile versionFile) throws BusinessException;

    /**
     * Gets the version.
     *
     * @param currentSelection the current selection
     * @return the version
     * @throws BusinessException the business exception
     * @link(ICurrentSelection) the current selection
     */
    VersionFile getVersion(ICurrentSelection currentSelection) throws BusinessException;

    /**
     * Gets the version.
     *
     * @param fileName the file name
     * @return the version
     * @throws BusinessException the business exception
     * @link(String) the file name
     */
    VersionFile getVersion(String fileName) throws BusinessException;

    /**
     * Checks if is valid file name.
     *
     * @param fileName the file name
     * @param realNodeLeafId
     * @return true, if successful
     * @throws BusinessException the business exception
     * @link(String) the file name
     * @link(VersionFile) the version
     */
    Optional<VersionFile> testIsValidFileNameAndGetNewVersion(String fileName, Long realNodeLeafId) throws BusinessException;
}
