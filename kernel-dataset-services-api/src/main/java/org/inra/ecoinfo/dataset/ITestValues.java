/*
 *
 */
package org.inra.ecoinfo.dataset;

import com.Ostermiller.util.CSVParser;
import java.io.Serializable;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * 
 * object interface used to test the consistency of the values ​​of a file.
 * 
 * 
 * @author Tcherniatinsky Philippe
 */
public interface ITestValues extends Serializable {

    /**
     * 
     * test the data of a file.
     * 
     * 
     * @param startline
     *            int the startline
     * @param parser
     *            the parser
     * @param versionFile
     * @link(VersionFile)
     * @link(IRequestPropertiesACBB) the session properties
     * @param encoding
     *            the encoding
     * @param badsFormatsReport
     * @link(BadsFormatsReport)
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @param datatypeName
     * @link(String) the datatype name
     * 
     * @throws BusinessException
     *             the business exception {@link VersionFile} the version file
     *             {@link IRequestPropertiesACBB} the session properties {@linkBadsFormatsReport}
     *             the bads formats report {@link DatasetDescriptorACBB} the dataset descriptor
     *             {@link String} the datatype name
     * @link(VersionFile) the version file
     * @link(IRequestPropertiesACBB) the session properties
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(String) the datatype name
     */
    void testValues(long startline, CSVParser parser, VersionFile versionFile, String encoding,
            BadsFormatsReport badsFormatsReport, 
            String datatypeName) throws BusinessException;

}
