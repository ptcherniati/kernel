/**
 * OREILacs project - see LICENCE.txt for use created: 18 févr. 2009 16:45:14
 */
package org.inra.ecoinfo.dataset.config.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.dataset.IFileNameChecker;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class DataType.
 *  * record a <datatype> tag from the configuration.xml file.
 *
 * @author "Antoine Schellenberger"
 */
public class DataTypeDescription implements Serializable {

    /**
     *
     */
    public static final DataTypeDescription genericDatatypeDescription = new DataTypeDescription();

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    private static final String KEY = "key";
    /**
     * The can overlap dates @link(boolean).
     */
    private boolean canOverlapDates = false;
    /**
     * The daos @link(List<String>).
     */
    private List<String> daos = new LinkedList<>();
    /**
     * The internationalized descriptions @link(Map<String,String>).
     */
    private Map<String, String> internationalizedDescriptions = new HashMap<>();
    /**
     * The internationalized names @link(Map<String,String>).
     */
    private Map<String, String> internationalizedNames = new HashMap<>();
    /**
     * The name file checkers @link(List<String>).
     */
    private List<String> nameFileCheckers = new LinkedList<>();

    private List<IFileNameChecker> FileCheckers = new LinkedList<>();

    /**
     * The recorder @link(String).
     */
    private String recorder;

    private String fileNameDateFormat = DateUtil.DD_MM_YYYY_FILE;

    /**
     * Instantiates a new data type.
     */
    public DataTypeDescription() {
        super();
    }

    /**
     * Instantiates a new data type.
     *
     * @param datatype the datatype
     * @link(DataType) the datatype
     */
    public DataTypeDescription(final DataTypeDescription datatype) {
        super();
        setInternationalizedNames(datatype.internationalizedNames);
        setInternationalizedDescriptions(datatype.internationalizedDescriptions);
    }

    /**
     *
     * @return
     */
    public List<IFileNameChecker> getFileCheckers() {
        return FileCheckers;
    }

    /**
     *
     * @param FileCheckers
     */
    public void setFileCheckers(List<IFileNameChecker> FileCheckers) {
        this.FileCheckers = FileCheckers;
    }

    /**
     *
     * @return
     */
    public String getFileNameDateFormat() {
        return fileNameDateFormat;
    }

    /**
     *
     * @param fileNameDateFormat
     */
    public void setFileNameDateFormat(String fileNameDateFormat) {
        this.fileNameDateFormat = fileNameDateFormat;
    }

    /**
     * Adds the dao.
     *
     * @param dao the dao
     * @link(StringBuffer) the dao
     */
    public void addDao(final StringBuilder dao) {
        daos.add(dao.toString());
    }

    /**
     * Adds the internationalized description.
     *
     * @param language the language
     * @param value the value
     * @link(String) the language
     * @link(String) the value
     */
    public void addInternationalizedDescription(final String language, final String value) {
        internationalizedDescriptions.put(language == null ? Localization.getDefaultLocalisation() : language, value);
    }

    /**
     * Adds the internationalized name.
     *
     * @param language the language
     * @param value the value
     * @link(String) the language
     * @link(String) the value
     */
    public void addInternationalizedName(final String language, final String value) {
        internationalizedNames.put(language == null ? Localization.getDefaultLocalisation() : language, value);
    }

    /**
     * Adds the name file checker.
     *
     * @param nameFileChecker the name file checker
     * @link(StringBuffer) the name file checker
     */
    public void addNameFileChecker(final StringBuilder nameFileChecker) {
        nameFileCheckers.add(nameFileChecker.toString());
    }

    /**
     * Can overlap dates.
     *
     * @return true, if successful
     */
    public boolean canOverlapDates() {
        return canOverlapDates;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        final String code = getKeyOrDefault(internationalizedNames);
        return Utils.createCodeFromString(code);
    }

    /**
     * Gets the daos.
     *
     * @return the daos
     */
    public List<String> getDaos() {
        return daos;
    }

    /**
     * Sets the daos.
     *
     * @param daos the new daos
     */
    public void setDaos(final List<String> daos) {
        this.daos = daos;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return getKeyOrDefault(internationalizedDescriptions);
    }

    private String getKeyOrDefault(Map<String, String> internationalizedStrings) {
        return internationalizedStrings.getOrDefault(KEY, internationalizedStrings.get(Localization.getDefaultLocalisation()));
    }

    /**
     * Gets the internationalized descriptions.
     *
     * @return the internationalized descriptions
     */
    public Map<String, String> getInternationalizedDescriptions() {
        return internationalizedDescriptions;
    }

    /**
     * Sets the internationalized descriptions.
     *
     * @param internationalizedDescription the internationalized description
     * @link(Map<String,String>) the internationalized description
     */
    public final void setInternationalizedDescriptions(final Map<String, String> internationalizedDescription) {
        this.internationalizedDescriptions = internationalizedDescription;
    }

    /**
     * Gets the internationalized names.
     *
     * @return the internationalized names
     */
    public Map<String, String> getInternationalizedNames() {
        return internationalizedNames;
    }

    /**
     * Sets the internationalized names.
     *
     * @param internationalizedNames the internationalized names
     * @link(Map<String,String>) the internationalized names
     */
    public final void setInternationalizedNames(final Map<String, String> internationalizedNames) {
        this.internationalizedNames = internationalizedNames;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return getKeyOrDefault(internationalizedNames);
    }

    /**
     * Gets the name files checkers.
     *
     * @return the name files checkers
     */
    public List<String> getNameFilesCheckers() {
        return nameFileCheckers;
    }

    /**
     * Gets the recorder.
     *
     * @return the recorder
     */
    public String getRecorder() {
        return recorder;
    }

    /**
     * Sets the recorder.
     *
     * @param recorder the new recorder
     */
    public void setRecorder(final String recorder) {
        this.recorder = recorder;
    }

    /**
     * Sets the can overlap dates.
     *
     * @param canOverlapDates the new can overlap dates
     */
    public void setCanOverlapDates(final boolean canOverlapDates) {
        this.canOverlapDates = canOverlapDates;
    }

    /**
     * Sets the name file checkers.
     *
     * @param nameFileCheckers the new name file checkers
     */
    public void setNameFileCheckers(final List<String> nameFileCheckers) {
        this.nameFileCheckers = nameFileCheckers;
    }
}
