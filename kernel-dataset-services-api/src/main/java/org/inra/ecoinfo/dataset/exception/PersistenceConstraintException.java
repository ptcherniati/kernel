/**
 * OREILacs project - see LICENCE.txt for use created: 3 févr. 2010 13:45:08
 */
package org.inra.ecoinfo.dataset.exception;

import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class PersistenceConstraintException.
 *
 * @author "Antoine Schellenberger"
 */
public class PersistenceConstraintException extends PersistenceException {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new persistence constraint exception.
     */
    public PersistenceConstraintException() {
        super();
    }

    /**
     * Instantiates a new persistence constraint exception.
     *
     * @param string
     * @link(String) the arg0
     */
    public PersistenceConstraintException(final String arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new persistence constraint exception.
     *
     * @param string
     * @param arg0 the arg0
     * @param thrwbl
     * @param arg1 the arg1
     * @link(String) the arg0
     * @link(Throwable) the arg1
     */
    public PersistenceConstraintException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new persistence constraint exception.
     *
     * @param thrwbl
     * @link(Throwable) the arg0
     */
    public PersistenceConstraintException(final Throwable arg0) {
        super(arg0);
    }
}
