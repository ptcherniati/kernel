package org.inra.ecoinfo.dataset.versioning.exception;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class MalformedFilenameException.
 */
public class MalformedFilenameException extends BusinessException {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new malformed filename exception.
     */
    public MalformedFilenameException() {
    }

    /**
     * Instantiates a new malformed filename exception.
     *
     * @param arg0 the arg0
     * @link(String) the arg0
     */
    public MalformedFilenameException(final String arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new malformed filename exception.
     *
     * @param arg0 the arg0
     * @param arg1 the arg1
     * @link(String) the arg0
     * @link(Throwable) the arg1
     */
    public MalformedFilenameException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new malformed filename exception.
     *
     * @param arg0 the arg0
     * @link(Throwable) the arg0
     */
    public MalformedFilenameException(final Throwable arg0) {
        super(arg0);
    }
}
