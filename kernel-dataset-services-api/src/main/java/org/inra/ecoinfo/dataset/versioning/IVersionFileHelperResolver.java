package org.inra.ecoinfo.dataset.versioning;

import org.inra.ecoinfo.dataset.versioning.exception.NoVersionFileHelperResolvedException;

/**
 * The Interface IVersionFileHelperResolver.
 */
public interface IVersionFileHelperResolver {

    /**
     * Resolve by datatype.
     *
     * @param datatypeCode the datatype code
     * @return the i version file helper
     * @throws org.inra.ecoinfo.dataset.versioning.exception.NoVersionFileHelperResolvedException
     * @link(String) the datatype code
     */
    IVersionFileHelper resolveByDatatype(String datatypeCode)
            throws NoVersionFileHelperResolvedException;
}
