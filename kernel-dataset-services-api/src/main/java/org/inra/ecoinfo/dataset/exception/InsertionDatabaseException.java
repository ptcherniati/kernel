/**
 * OREILacs project - see LICENCE.txt for use created: 3 févr. 2010 13:45:08
 */
package org.inra.ecoinfo.dataset.exception;

/**
 * The Class InsertionDatabaseException.
 *
 * @author "Antoine Schellenberger"
 */
public class InsertionDatabaseException extends Exception {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new insertion database exception.
     */
    public InsertionDatabaseException() {
        super();
    }

    /**
     * Instantiates a new insertion database exception.
     *
     * @param arg0 the arg0
     * @link(String) the arg0
     */
    public InsertionDatabaseException(final String arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new insertion database exception.
     *
     * @param arg0 the arg0
     * @param arg1 the arg1
     * @link(String) the arg0
     * @link(Throwable) the arg1
     */
    public InsertionDatabaseException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new insertion database exception.
     *
     * @param arg0 the arg0
     * @link(Throwable) the arg0
     */
    public InsertionDatabaseException(final Throwable arg0) {
        super(arg0);
    }
}
