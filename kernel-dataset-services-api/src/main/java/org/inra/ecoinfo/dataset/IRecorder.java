/**
 * OREILacs project - see LICENCE.txt for use created: 17 févr. 2009 12:07:12
 */
package org.inra.ecoinfo.dataset;

import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IRecorder.
 *
 * @author "Antoine Schellenberger"
 */
public interface IRecorder {

    /**
     * Delete records.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    void deleteRecords(VersionFile versionFile) throws BusinessException;

    /**
     * Record.
     *
     * @param versionFile the version file
     * @param fileEncoding the file encoding
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     * @link(String) the file encoding
     */
    void record(VersionFile versionFile, String fileEncoding) throws BusinessException;

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    void setLocalizationManager(ILocalizationManager localizationManager);

    /**
     * Test format.
     *
     * @param versionFile the version file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     * @link(String) the encoding
     */
    void testFormat(VersionFile versionFile, String encoding) throws BusinessException;

    /**
     * Test in deep filename.
     *
     * @param fileName the file name
     * @throws BusinessException the business exception
     * @link(String) the file name
     */
    void testInDeepFilename(String fileName) throws BusinessException;
}
