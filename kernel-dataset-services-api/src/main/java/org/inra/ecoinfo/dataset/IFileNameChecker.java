package org.inra.ecoinfo.dataset;

import java.io.Serializable;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

/**
 * The Interface IFileNameChecker.
 */
public interface IFileNameChecker extends Serializable{

    /**
     * The pattern file name path @link(String).
     */
    String PATTERN_FILE_NAME_PATH = "%s_%s_%s_%s#V%d#.csv";

    /**
     * Gets the file path.
     *
     * @param version the version
     * @return the file path
     * @link(VersionFile) the version
     */
    default String getFilePath(VersionFile version){
        return version==null?"":getFilePath(version.getDataset());
    }

    /**
     *
     * @param dataset
     * @return
     */
    String getFilePath(Dataset dataset);

    /**
     * Checks if is valid file name.
     *
     * @param fileName the file name
     * @param version the version
     * @return true, if is valid file name
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @link(String) the file name
     * @link(VersionFile) the version
     */
    boolean isValidFileName(String fileName, VersionFile version) throws InvalidFileNameException;
}
