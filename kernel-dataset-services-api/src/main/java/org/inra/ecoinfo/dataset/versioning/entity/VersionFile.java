package org.inra.ecoinfo.dataset.versioning.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Type;
import org.inra.ecoinfo.identification.entity.Utilisateur;

/**
 * The Class VersionFile.
 *
 * @author philippe Entity implementation class for Entity: VersionFile
 */
@Entity
@Table(name = "insertion_version_file_ivf", uniqueConstraints = {
    @UniqueConstraint(columnNames = {VersionFile.VERSION_NUMBER, Dataset.ID_JPA})},
        indexes = {
            @Index(name = "ivf_ids_idx", columnList = Dataset.ID_JPA)})
@BatchSize(size = VersionFile.BATCH_SIZE)
@NamedEntityGraph(name = VersionFile.COMPLETE_GRAPH,
        attributeNodes = {
            @NamedAttributeNode("id")
            ,
            @NamedAttributeNode("size")
            ,
            @NamedAttributeNode("versionNumber")
            ,
            @NamedAttributeNode("dataset")
            ,
            @NamedAttributeNode("uploadUser")
            ,
            @NamedAttributeNode("uploadDate")
            ,
            @NamedAttributeNode("uploadDate")
            ,
            @NamedAttributeNode("fileName")
            ,
            @NamedAttributeNode("extention")
        })
public class VersionFile implements Comparable<VersionFile>, Serializable {

    /**
     *
     */
    public static final String COMPLETE_GRAPH = "graph.org.inra.ecoinfo.dataset.versioning.entity.VersionFile";

    /**
     * The Constant ID_JPA @link(String).
     */
    public static final String ID_JPA = "ivf_id";
    /**
     * The Constant SIZE @link(String).
     */
    public static final String SIZE = "ivf_size";
    /**
     * The Constant VERSION_NUMBER @link(String).
     */
    public static final String VERSION_NUMBER = "ivf_version_number";

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * The Constant MB.
     */
    private static final int MB = 1_048_576;
    /**
     * The Constant KB.
     */
    private static final int KB = 1_024;
    /**
     * The Constant BYTES.
     */
    private static final String BYTES = "bytes";
    /**
     * The Constant HEXA_20 @link(int).
     */
    private static final int HEXA_20 = 32;
    /**
     * The Constant BATCH_SIZE @link(int).
     */
    static final int BATCH_SIZE = 14;
    /**
     * The data @link(byte[]).
     */
    @Type(type = "org.hibernate.type.MaterializedBlobType")
    @Basic(fetch = FetchType.LAZY)
    private byte[] data;
    /**
     * The dataset @link(Dataset).
     */
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST}, optional = false)
    @JoinColumn(referencedColumnName = Dataset.ID_JPA, nullable = false, name = Dataset.ID_JPA)
    @NaturalId
    private Dataset dataset;
    /**
     * The id @link(long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = VersionFile.ID_JPA)
    private long id;
    /**
     * The size @link(int).
     */
    @Column(name = VersionFile.SIZE)
    private int size = 0;
    /**
     * The upload date @link(Date).
     */
    @Column(nullable = false, unique = false, name = "ivf_upload_date")
    @CreationTimestamp
    private LocalDateTime uploadDate;
    /**
     * The upload user @link(Utilisateur).
     */
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST}, optional = false)
    @JoinColumn(referencedColumnName = Utilisateur.UTILISATEUR_NAME_ID, nullable = false, name = "ivf_upload_user")
    private Utilisateur uploadUser;
    /**
     * The version number @link(long).
     */
    @Column(name = VersionFile.VERSION_NUMBER)
    @NaturalId
    private long versionNumber = -1;
    
    @Column
    String fileName;
    
    @Column
    String extention;

    /**
     * Compare to.
     *
     * @param o the o
     * @return the int
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final VersionFile o) {
        if (o == null) {
            return -1;
        }
        if (getDataset().compareTo(o.getDataset()) != 0) {
            return getDataset().compareTo(o.getDataset());
        }
        return getVersionNumber() < o.getVersionNumber() ? 1 : -1;
    }

    /**
     * /* (non-Javadoc)
     *
     * @param obj
     * @return 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof VersionFile) {
            VersionFile version = (VersionFile) obj;
            return Objects.equals(dataset, version.dataset)
                    && versionNumber == version.versionNumber;
        }
        return false;
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public byte[] getData() {
        return Optional.ofNullable(data)
                .map(data -> Arrays.copyOf(data, data.length))
                .orElse(null);
    }

    /**
     * Sets the data.
     *
     * @param d the new data
     */
    public void setData(final byte[] d) {
        this.data = Optional.ofNullable(d)
                .map(data -> Arrays.copyOf(data, data.length))
                .orElse(new byte[0]);
    }

    /**
     * Gets the dataset.
     *
     * @return the dataset
     */
    public Dataset getDataset() {
        if (dataset == null) {
            dataset = new Dataset();
        }
        return dataset;
    }

    /**
     * Sets the dataset.
     *
     * @param dataset the new dataset
     */
    public void setDataset(final Dataset dataset) {
        this.dataset = dataset;
    }

    /**
     * Gets the file size.
     *
     * @return the file size
     */
    public int getFileSize() {
        return size;
    }

    /**
     * Sets the file size.
     *
     * @param size the new file size
     */
    public void setFileSize(final int size) {
        this.size = size;
    }

    /**
     * Gets the file size affichage.
     *
     * @return the file size affichage
     */
    public String getFileSizeAffichage() {
        String fileSize = "";
        if (size < VersionFile.KB) {
            fileSize = String.format("%s %s", size, VersionFile.BYTES);
        } else if (size < VersionFile.MB) {
            fileSize = String.format("%d %s", Math.round((double) size / VersionFile.KB), "KB");
        } else {
            fileSize = String.format("%d %s", Math.round((double) size / VersionFile.MB), "MB");
        }
        return fileSize;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getUploadDate() {
        return uploadDate;
    }

    /**
     *
     * @param uploadDate
     */
    public void setUploadDate(LocalDateTime uploadDate) {
        this.uploadDate = uploadDate;
    }

    /**
     * Gets the upload user.
     *
     * @return the upload user
     */
    public Utilisateur getUploadUser() {
        return uploadUser;
    }

    /**
     * Sets the upload user.
     *
     * @param uploadUser the new upload user
     */
    public void setUploadUser(final Utilisateur uploadUser) {
        this.uploadUser = uploadUser;
    }

    /**
     * Gets the version number.
     *
     * @return the version number
     */
    public long getVersionNumber() {
        return versionNumber;
    }

    /**
     * Sets the version number.
     *
     * @param versionNumber the new version number
     */
    public void setVersionNumber(final long versionNumber) {
        this.versionNumber = versionNumber;
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (dataset == null ? 0 : dataset.getId().hashCode());
        result = prime * result + (int) (versionNumber ^ versionNumber >>> VersionFile.HEXA_20);
        return result;
    }

    /**
     *
     * @return
     */
    public boolean isPublished() {
        return equals(dataset.getPublishVersion());
    }

    /**
     *
     * @param published
     */
    public void setPublished(boolean published) {
        // do nothing
    }

    /**
     *
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     *
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     *
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     *
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     *
     * @return
     */
    public String getExtention() {
        return extention;
    }

    /**
     *
     * @param extention
     */
    public void setExtention(String extention) {
        this.extention = extention;
    }
}
