/*
 *
 */
package org.inra.ecoinfo.dataset;

import com.Ostermiller.util.CSVParser;
import java.io.Serializable;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IprocessRecord.
 * 
 * interface objects used to publish versions of files
 * 
 * 
 * @author Tcherniatinsky Philippe
 */
public interface IProcessRecord extends Serializable {

    /**
     *
     * @param parser
     * @param versionFile
     * @param fileEncoding
     * @throws BusinessException
     */
    void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding) throws BusinessException;

}
