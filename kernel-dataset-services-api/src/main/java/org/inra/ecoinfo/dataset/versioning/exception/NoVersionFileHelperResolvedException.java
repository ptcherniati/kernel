package org.inra.ecoinfo.dataset.versioning.exception;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class NoVersionFileHelperResolvedException.
 */
public class NoVersionFileHelperResolvedException extends BusinessException {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The datatype code @link(String).
     */
    private final String datatypeCode;

    /**
     * Instantiates a new no version file helper resolved exception.
     *
     * @param string
     * @link(String) the datatype code
     */
    public NoVersionFileHelperResolvedException(final String datatypeCode) {
        this.datatypeCode = datatypeCode;
    }

    /**
     * Gets the datatype code.
     *
     * @return the datatype code
     */
    public String getDatatypeCode() {
        return datatypeCode;
    }
}
