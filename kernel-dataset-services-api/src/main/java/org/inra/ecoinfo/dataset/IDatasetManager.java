package org.inra.ecoinfo.dataset;

import java.io.Serializable;
import java.util.List;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IDatasetManager.
 */
public interface IDatasetManager extends Serializable {

    /**
     * The id bean @link(String).
     */
    String ID_BEAN = "datasetManager";

    /**
     * Check version.
     *
     * @param version the version
     * @throws BusinessException the business exception
     * @link(VersionFile) the version
     */
    void checkVersion(VersionFile version) throws BusinessException;

    /**
     * Delete version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    void deleteVersion(VersionFile versionFile) throws BusinessException;

    /**
     * Download version by id.
     *
     * @param versionFileId the version file id
     * @return the byte[]
     * @throws BusinessException the business exception
     * @link(Long) the version file id
     */
    byte[] downloadVersionById(Long versionFileId) throws BusinessException;

    /**
     * Gets the version file by id.
     *
     * @param versionFileId the version file id
     * @return the version file by id
     * @throws BusinessException the business exception
     * @link(Long) the version file id
     */
    VersionFile getVersionFileById(Long versionFileId) throws BusinessException;

    /**
     * Gets the version file helper resolver.
     *
     * @return the version file helper resolver
     */
    IVersionFileHelperResolver getVersionFileHelperResolver();

    /**
     * Gets the version from file name.
     *
     * @param fileName the file name
     * @param realNodeLeafId
     * @return the version from file name
     * @throws BusinessException the business exception
     * @link(String) the file name
     */
    VersionFile getVersionFromFileName(String fileName, Long realNodeLeafId) throws BusinessException;

    /**
     * Publish version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    void publishVersion(VersionFile versionFile) throws BusinessException;

    /**
     * Retrieve datasets.
     *
     * @param realNodeLeafId
     * @return the list
     * @throws BusinessException the business exception
     */
    List<Dataset> retrieveDatasets(Long realNodeLeafId) throws BusinessException;
    /**
     * Un publish version.
     *
     * @param versionFile the version file
     * @return 
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    int unPublishVersion(VersionFile versionFile) throws BusinessException;

    /**
     * Upload version.
     *
     * @param versionFile the version file
     * @return the dataset
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     */
    Dataset uploadVersion(VersionFile versionFile) throws BusinessException;
}
