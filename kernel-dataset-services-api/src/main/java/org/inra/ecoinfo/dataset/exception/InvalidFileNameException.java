package org.inra.ecoinfo.dataset.exception;

/**
 * The Class InvalidFileNameException.
 */
public class InvalidFileNameException extends Exception {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new invalid file name exception.
     *
     * @param string
     * @link(String) the arg
     */
    public InvalidFileNameException(final String arg) {
        super(arg);
    }

    /**
     * Instantiates a new invalid file name exception.
     *
     * @param string
     * @param message the message
     * @param thrwbl
     * @param cause the cause
     */
    public InvalidFileNameException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
