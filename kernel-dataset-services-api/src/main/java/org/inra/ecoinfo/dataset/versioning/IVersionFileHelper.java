package org.inra.ecoinfo.dataset.versioning;

import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IVersionFileHelper.
 */
public interface IVersionFileHelper {

    /**
     * The Constant FILE_SEPARATOR @link(String).
     */
    String FILE_SEPARATOR = System.getProperty("file.separator");
    /**
     * The Constant EXTENSION_FILE_CSV @link(String).
     */
    String EXTENSION_FILE_CSV = ".csv";

    /**
     * Builds the download filename.
     *
     * @param versionFile the version file
     * @return the string
     * @link(VersionFile) the version file
     */
    String buildDownloadFilename(VersionFile versionFile);

    /**
     * Test basic filename.
     *
     * @param uploadedFilename the uploaded filename
     * @param sitePath the site path
     * @param datatypeCode the datatype code
     * @throws BusinessException the business exception
     * @link(String) the uploaded filename
     * @link(String) the site path
     * @link(String) the datatype code
     */
    void testBasicFilename(String uploadedFilename, String sitePath, String datatypeCode)
            throws BusinessException;
}
