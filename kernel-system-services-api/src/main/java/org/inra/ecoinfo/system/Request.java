package org.inra.ecoinfo.system;

class Request {

    private long queryMemory;
    public final long threadId;
    public final String memoryName;
    /**
     *
     */
    private boolean canSatisfy = false;

    public Request(long queryMemory, long threadId, String memoryName) {
        super();
        this.queryMemory = queryMemory;
        this.threadId = threadId;
        this.memoryName = memoryName;
    }

    public long getQueryMemory() {
        return queryMemory;
    }

    public void setQueryMemory(long queryMemory) {
        this.queryMemory = queryMemory;
    }

    public void increaseQueryMemory(long queryMemory) {
        this.queryMemory += queryMemory;
    }

    public void decreaseQueryMemory(long queryMemory) {
        this.queryMemory -= queryMemory;
    }

    public boolean isCanSatisfy() {
        return canSatisfy;
    }

    public void setCanSatisfy(boolean canSatisfy) {
        this.canSatisfy = canSatisfy;
    }
}
