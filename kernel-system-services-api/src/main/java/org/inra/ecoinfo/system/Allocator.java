package org.inra.ecoinfo.system;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.inra.ecoinfo.system.config.impl.ISystemConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Allocator. You have to create a singleton of this class before
 * using it.
 *
 * Put the instance od this allocator into itself
 * Allocator.setAllocator(instance);
 *
 * So you can have the instance of the allocator by using the
 * Allocator.getInstance() function.
 *
 *  * When you want to reserve a portion of the memory of the JVM for an expensive
 * treatment:
 * </p>
 * <ul>
 * <li>Estimate the cost of treatment byte.</li>
 * <li>Book memory space by calling allocatorInstance.allocate(queryMemory) if
 * memory is not available, the process will be put on hold until there
 * sufisement memory released by other processes.</li>
 * <li>Make your treatment</li>
 * <li>Free memory allocated by calling
 * allocatorInstance.free(realeasedMemory).<br />
 * The waiting processes can be launched into space freed memory</li>
 * </ul>
 */
public class Allocator {
    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Allocator.class.getName());

    static final String MEMORY_INFO = "requesting %d free for memory name %s : %d%nTotal free memory :%d";
    /**
     * The allocator.
     */
    static Allocator allocator;

    /**
     * Gets the single instance of Allocator.
     *
     * @return single instance of Allocator
     */
    public static Allocator getInstance() {
        return Allocator.allocator;
    }
    ISystemConfiguration systemConfiguration;
    final Map<String, Long> memoryLimits = new HashMap<>();


    final ReadWriteLock lock = new ReentrantReadWriteLock();

    /**
     * The total request memory.
     */
    final Map<String, Long> totalRequestMemories = new HashMap<>();


    /**
     * The waiting requests.
     */
    final Map<String, Map<Long, Request>> waitingRequests = new HashMap<>();

    /**
     * The runing requests.
     */
    final Map<String, Map<Long, Request>> runingRequests = new HashMap();
    final long availableMemory = Runtime.getRuntime().maxMemory();

    /**
     *
     */
    public Allocator() {
        super();
        allocator = this;
    }

    /**
     * Allocate.
     *
     * <ul>
     * <li>
     * Book memory space by calling allocatorInstance.allocate(queryMemory)
     * <br />
     * if memory is not available, the process will be put on hold until there
     * sufisement memory released by other processes.
     * <Li>
     * </ul>
     *
     * @param memoryName
     * @param queryMemory the query memory
     * @throws InterruptedException the interrupted exception
     */
    public void allocate(String memoryName, long queryMemory) throws InterruptedException {
        long localQueryMemory = queryMemory;
        String localMemoryName = cleanNameMemory(memoryName);
        if (localQueryMemory > availableMemory) {
            MemoryOverflowException memoryOverflowException = new MemoryOverflowException(availableMemory);
            Allocator.LOGGER.info(memoryOverflowException.getMessage(), memoryOverflowException);
            localQueryMemory = availableMemory;

        }
        Request r = null;
        lock.writeLock().lock();
        try {
            if (localQueryMemory <= this.getFreeMemory(localMemoryName) || getRuningRequestsCount(localMemoryName) == 0) {
                totalRequestMemories.put(localMemoryName, totalRequestMemories.get(localMemoryName) + localQueryMemory);
                Allocator.LOGGER.info(String.format(MEMORY_INFO, localQueryMemory, localMemoryName, getFreeMemory(localMemoryName), getfreeMemory()));
                Request rr = new Request(localQueryMemory, getCurrentThreadId(), localMemoryName);
                rr.setCanSatisfy(true);
                if (!runingRequests.get(localMemoryName).containsKey(getCurrentThreadId())) {
                    runingRequests.get(localMemoryName).put(getCurrentThreadId(), rr);
                } else {
                    runingRequests.get(localMemoryName).get(getCurrentThreadId()).increaseQueryMemory(localQueryMemory);
                }
            } else {
                r = new Request(localQueryMemory, getCurrentThreadId(), localMemoryName);
                this.waitingRequests.get(localMemoryName).put(getCurrentThreadId(), r);
            }
        } finally {
            lock.writeLock().unlock();
        }
        if (r != null) {
            synchronized (r) {
                Allocator.LOGGER.info("{} threads blocked", waitingRequests.get(localMemoryName).size());
                while (!r.isCanSatisfy()) {
                    r.wait();
                }
            }
        }
    }

    /**
     * Free. Free memory allocated by calling
     * allocatorInstance.free(realeasedMemory).<br />
     * The waiting processes can be launched into space freed memory.
     *
     * @param memoryName
     */
    public void free(String memoryName) {
        String localMemoryName = cleanNameMemory(memoryName);
        lock.writeLock().lock();
        try {
            long releasedMemory = Optional.ofNullable(runingRequests)
                    .map(rr->rr.get(localMemoryName))
                    .map(lmm->lmm.get(getCurrentThreadId()))
                    .map(cti->cti.getQueryMemory()).orElse(0l);
            this.totalRequestMemories.put(localMemoryName, totalRequestMemories.get(localMemoryName) - releasedMemory);
            Allocator.LOGGER.info("releasing {} free {}", releasedMemory, getFreeMemory(localMemoryName));
            runingRequests.get(localMemoryName).remove(getCurrentThreadId());
            assert !runingRequests.get(localMemoryName).containsKey(getCurrentThreadId()) : "pas vidé";
            Iterator<Entry<Long, Request>> it = waitingRequests.get(localMemoryName).entrySet().iterator();
            while (it.hasNext()) {
                Request r = it.next().getValue();
                synchronized (r) {
                    if (r.getQueryMemory() <= this.getFreeMemory(localMemoryName) || (getRuningRequestsCount(localMemoryName) == 0 && getWaitingRequestsCount(localMemoryName) > 0)) {
                        it.remove();
                        runingRequests.get(localMemoryName).put(r.threadId, r);
                        this.totalRequestMemories.put(localMemoryName, totalRequestMemories.get(localMemoryName) + r.getQueryMemory());
                        Allocator.LOGGER.info("requesting {} free {}", releasedMemory, getFreeMemory(localMemoryName));
                        Allocator.LOGGER.info("1 thread released now {} threads blocked", waitingRequests.get(localMemoryName).size());
                        r.setCanSatisfy(true);
                        r.notify();
                    }
                }
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Gets the runing requests count.
     *
     * @param memoryName
     * @return the runing requests count
     */
    public int getRuningRequestsCount(String memoryName) {
        String localMemoryName = cleanNameMemory(memoryName);
        try {
            lock.readLock().lock();
            return runingRequests.get(localMemoryName).size();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Gets the runing requests size.
     *
     * @param memoryName
     * @return the runing requests size allocated
     */
    public long getRuningRequestsSize(String memoryName) {
        String localMemoryName = cleanNameMemory(memoryName);
        try {
            lock.readLock().lock();
            long runingRequestsSize = 0;
            runingRequestsSize = runingRequests.get(localMemoryName).entrySet().stream().map((sizeEntry) -> sizeEntry.getValue().getQueryMemory()).reduce(runingRequestsSize, (accumulator, _item) -> accumulator + _item);
            return runingRequestsSize;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * uuibeanch Gets the waiting requests count.
     *
     * @param memoryName
     *
     * @return the waiting requests count
     */
    public int getWaitingRequestsCount(String memoryName) {
        String localMemoryName=cleanNameMemory(memoryName);
        try {
            lock.readLock().lock();
            return Allocator.getInstance().waitingRequests.get(localMemoryName).size();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Gets the waiting requests size.
     *
     * @param memoryName
     * @return the waiting requests size
     */
    public long getWaitingRequestsSize(String memoryName) {
        String localMemoryName = cleanNameMemory(memoryName);
        try {
            lock.readLock().lock();
            long waitingRequestsSize = 0;
            waitingRequestsSize = waitingRequests.get(localMemoryName).entrySet().stream().map((requestEntry) -> requestEntry.getValue().getQueryMemory()).reduce(waitingRequestsSize, (accumulator, _item) -> accumulator + _item);
            return waitingRequestsSize;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     *
     * @param systemConfiguration
     */
    public void setSystemConfiguration(ISystemConfiguration systemConfiguration) {
        this.systemConfiguration = systemConfiguration;
        initMemories();
    }

    private long getfreeMemory() {
        return Runtime.getRuntime().freeMemory();
    }

    /**
     * Gets the free memory.
     *
     * @return the free memory
     */
    private long getFreeMemory(String memoryName) {
        String localMemoryName = cleanNameMemory(memoryName);
        long abstractFreeMemory = memoryLimits.get(localMemoryName) - totalRequestMemories.get(localMemoryName);
        long realFreeMemory = Runtime.getRuntime().freeMemory();
        return Math.min(realFreeMemory, abstractFreeMemory);
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    private Long getCurrentThreadId() {
        return Thread.currentThread().getId();
    }

    private String cleanNameMemory(String memoryName) {
        if (memoryName == null || !memoryLimits.containsKey(memoryName)) {
            return ISystemConfiguration.DEFAULT_MEMORY_ALLOCATION;
        }
        return memoryName;
    }

    private void initMemories() {
        systemConfiguration.getMemoryNames().stream().map((memoryName) -> {
            memoryLimits.put(memoryName, (long) ((availableMemory) * systemConfiguration.getMemory(memoryName) / 100));
            return memoryName;
        }).map((memoryName) -> {
            runingRequests.put(memoryName, new HashMap<>());
            return memoryName;
        }).map((memoryName) -> {
            waitingRequests.put(memoryName, new HashMap<>());
            return memoryName;
        }).forEach((memoryName) -> {
            totalRequestMemories.put(memoryName, (long) 0);
        });
    }
}
