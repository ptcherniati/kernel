package org.inra.ecoinfo.system;

/**
 *
 * @author ptcherniati
 */
public class MemoryOverflowException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String PROPERTY_MSG_MEMORY_OVERFLOW = "You ask more memory than the maximum %d of memory of the current JVM";
    private final long availableMemory;

    /**
     *
     * @param l
     * @param availableMemory
     */
    public MemoryOverflowException(long availableMemory) {
        super(String.format(PROPERTY_MSG_MEMORY_OVERFLOW, availableMemory));
        this.availableMemory = availableMemory;
    }

    /**
     *
     * @return
     */
    public long getAvailableMemory() {
        return availableMemory;
    }
}
