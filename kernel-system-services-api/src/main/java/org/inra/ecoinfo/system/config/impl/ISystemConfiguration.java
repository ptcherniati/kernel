package org.inra.ecoinfo.system.config.impl;

import java.util.List;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface ICoreConfiguration.
 */
public interface ISystemConfiguration {

    /**
     *
     */
    String DEFAULT_MEMORY_ALLOCATION = "defaultMemory";

    /**
     *
     */
    String EXTRACT_MEMORY_ALLOCATION = "extract";

    /**
     *
     */
    String PUBLISH_MEMORY_ALLOCATION = "publish";

    /**
     *
     * @param name
     * @param memory
     * @throws BusinessException
     */
    void addMemory(String name, String memory) throws BusinessException;

    /**
     *
     * @return
     */
    List<String> getMemoryNames();

    /**
     *
     * @param name
     * @return
     */
    float getMemory(String name);
}
