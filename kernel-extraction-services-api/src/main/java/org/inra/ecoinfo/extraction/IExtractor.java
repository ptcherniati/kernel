package org.inra.ecoinfo.extraction;

import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IExtractor.
 */
public interface IExtractor {

    /**
     * Extract.
     *
     * @param parameters the parameters
     * @throws BusinessException the business exception
     * @link(IParameter) the parameters
     */
    void extract(IParameter parameters) throws BusinessException;

    /**
     * Sets the extraction.
     *
     * @param extraction the new extraction
     */
    void setExtraction(Extraction extraction);

    /**
     *
     * @param parameters
     * @return
     */
    long getExtractionSize(IParameter parameters);
}
