package org.inra.ecoinfo.extraction.exception;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class NoExtractionResultException.
 */
public class NoExtractionResultException extends BusinessException {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.utils.messages";

    /**
     *
     */
    public static final String ERROR = "ERROR";

    /**
     *
     */
    public static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    /**
     *
     */
    public static final String PROPERTY_MSG_NO_EXTRACTION_RESULT_FOR_TYPE = "PROPERTY_MSG_NO_EXTRACTION_RESULT_FOR_TYPE";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new no extraction result exception.
     *
     * @param string
     * @param propertyMsgNoExtractionResult the property msg no extraction
     * result
     * @link(String) the property msg no extraction result
     */
    public NoExtractionResultException(final String propertyMsgNoExtractionResult) {
        super(propertyMsgNoExtractionResult);
    }
}
