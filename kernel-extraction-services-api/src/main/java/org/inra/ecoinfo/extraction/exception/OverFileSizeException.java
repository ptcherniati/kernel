/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction.exception;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class OverFileSizeException extends BusinessException{

    /**
     *
     * @param info
     */
    public OverFileSizeException(String info) {
        super(info);
    }
    
}
