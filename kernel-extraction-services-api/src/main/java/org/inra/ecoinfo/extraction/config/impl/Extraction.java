package org.inra.ecoinfo.extraction.config.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class Extraction.
 *  * record an <Extraction> tag from the configuration.xml file.
 *
 * @author Philippe TCHERNIATINSKY tcherniatinsky
 */
public class Extraction implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The extractor @link(String).
     */
    private String extractor;
    
    private String extractionFormatter;
    /**
     * The internationalized descriptions @link(Map<String,String>).
     */
    private Map<String, String> internationalizedDescriptions = new HashMap<>();
    /**
     * The internationalized names @link(Map<String,String>).
     */
    private Map<String, String> internationalizedNames = new HashMap<>();
    /**
     * The output displays @link(TreeMap<String,String>).
     */
    private Map<String, String> outputDisplays = new TreeMap<>();

    /**
     * Instantiates a new extraction.
     */
    public Extraction() {
        super();
    }

    /**
     * Adds the internationalized description.
     *
     * @param language the language
     * @param value the value
     * @link(String) the language
     * @link(String) the value
     */
    public void addInternationalizedDescription(final String language, final String value) {
        internationalizedDescriptions.put(language == null ? Localization.getDefaultLocalisation() : language, value);
    }

    /**
     * Adds the internationalized name.
     *
     * @param language the language
     * @param value the value
     * @link(String) the language
     * @link(String) the value
     */
    public void addInternationalizedName(final String language, final String value) {
        internationalizedNames.put(language == null ? Localization.getDefaultLocalisation() : language, value);
    }

    /**
     * Adds the output display.
     *
     * @param number the number
     * @param outputDisplay the output display
     * @link(int) the number
     * @link(StringBuffer) the output display
     */
    public void addOutputDisplay(final int number, final StringBuilder outputDisplay) {
        outputDisplays.put(String.valueOf(number), outputDisplay.toString());
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return Utils.createCodeFromString(internationalizedNames.get(Localization.getDefaultLocalisation()));
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return internationalizedDescriptions.get(Localization.getDefaultLocalisation());
    }

    /**
     * Gets the extractor.
     *
     * @return the extractor
     */
    public String getExtractor() {
        return extractor;
    }

    /**
     * Sets the extractor.
     *
     * @param extractor the new extractor
     */
    public void setExtractor(final String extractor) {
        this.extractor = extractor;
    }

    /**
     * Gets the internationalized descriptions.
     *
     * @return the internationalized descriptions
     */
    public Map<String, String> getInternationalizedDescriptions() {
        return internationalizedDescriptions;
    }

    /**
     * Sets the internationalized descriptions.
     *
     * @param internationalizedDescription the internationalized description
     * @link(Map<String,String>) the internationalized description
     */
    public void setInternationalizedDescriptions(final Map<String, String> internationalizedDescription) {
        this.internationalizedDescriptions = internationalizedDescription;
    }

    /**
     * Gets the internationalized names.
     *
     * @return the internationalized names
     */
    public Map<String, String> getInternationalizedNames() {
        return internationalizedNames;
    }

    /**
     * Sets the internationalized names.
     *
     * @param internationalizedNames the internationalized names
     * @link(Map<String,String>) the internationalized names
     */
    public void setInternationalizedNames(final Map<String, String> internationalizedNames) {
        this.internationalizedNames = internationalizedNames;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return internationalizedNames.get(Localization.getDefaultLocalisation());
    }

    /**
     * Gets the output displays.
     *
     * @return the output displays
     */
    public Map<String, String> getOutputDisplays() {
        return outputDisplays;
    }

    /**
     * Sets the ouput displays.
     *
     * @param outputDisplays the output displays
     * @link(TreeMap<String,String>) the output displays
     */
    public void setOuputDisplays(final Map<String, String> outputDisplays) {
        this.outputDisplays = outputDisplays;
    }

    /**
     *
     * @return
     */
    public String getExtractionFormatter() {
        return extractionFormatter;
    }

    /**
     *
     * @param extractionFormatter
     */
    public void setExtractionFormatter(String extractionFormatter) {
        this.extractionFormatter = extractionFormatter;
    }
}
