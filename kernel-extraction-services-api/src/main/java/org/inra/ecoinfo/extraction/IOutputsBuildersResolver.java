package org.inra.ecoinfo.extraction;

import java.util.List;
import java.util.Map;

/**
 * The Interface IOutputsBuildersResolver.
 */
public interface IOutputsBuildersResolver {

    /**
     * Sert les outputBuilders nécessaires à partir de l'analyse de la Map de
     * métadonnées.
     *
     * @param metadatasMap the metadatas map
     * @return les outputBuilders demandées à partir de la Map
     * @link(Map<String,Object>) the metadatas map
     */
    List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap);
}
