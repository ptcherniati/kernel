package org.inra.ecoinfo.extraction.config;

import java.io.Serializable;
import java.util.List;
import org.inra.ecoinfo.extraction.config.impl.Extraction;

/**
 * The Interface IExtractionConfiguration.
 */
public interface IExtractionConfiguration extends Serializable {

    /**
     * Adds the extraction.
     *
     * @param extraction the extraction
     */
    void addExtraction(Extraction extraction);

    /**
     * Gets the extractions.
     *
     * @return the extractions
     */
    List<Extraction> getExtractions();

    /**
     * Sets the extractions.
     *
     * @param extractions the new extractions
     */
    void setExtractions(List<Extraction> extractions);
}
