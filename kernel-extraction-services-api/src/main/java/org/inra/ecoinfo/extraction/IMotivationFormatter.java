/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction;

import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
public interface IMotivationFormatter {

    /**
     *
     * @param parameters
     * @param motivation
     * @param localizationManager
     * @param user
     */
    public void formatMotivation(IParameter parameters, IMotivation motivation, ILocalizationManager localizationManager, Utilisateur user);
}
