/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.extraction;

/**
 *
 * @author ptcherniati
 */
public interface IMotivation {

    /**
     *
     * @param motivation
     */
    void setMotivation(String motivation);    

    /**
     *
     * @return
     */
    String getMotivation();  
}
