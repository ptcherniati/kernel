package org.inra.ecoinfo.extraction.genericfilesrepository.api;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.mga.business.composite.FlatNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author antoine Schellenberger
 */
public interface IGenericDatatypeManager extends Serializable{

    /**
     *
     * @param periods
     * @param codeConfiguration
     * @return
     */
    Map<DataType, ITreeNode<FlatNode>> getDatatype(List<IntervalDate> periods, Integer codeConfiguration);
}
