package org.inra.ecoinfo.extraction.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.inra.ecoinfo.extraction.ExtractionType;
import org.inra.ecoinfo.identification.entity.Utilisateur;

/**
 * The Class Requete.
 *
 * @author "Antoine Schellenberger"
 */
@Entity
@Table(indexes = {
    @Index(name = "requete_extractiontype_idx", columnList = ExtractionType.JPA_NAME_EXTRACTION_TYPE_FK),
    @Index(name = "requete_user_idx", columnList = Utilisateur.JPA_NAME_UTILISATEUR_FK)
})
public class Requete implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The commentaire @link(String).
     */
    @Column(columnDefinition = "TEXT")
    private String commentaire;
    /**
     * The date @link(Date).
     */
    @Column
    @CreationTimestamp
    private LocalDateTime date;
    /**
     * The duree extraction @link(Long).
     */
    @Column(nullable = false, name = "duree_extraction")
    private Long dureeExtraction;
    /**
     * The nom fichier @link(String).
     */
    @Column(nullable = false, name = "nom_fichier")
    private String nomFichier;
    /**
     * The id @link(Long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected Long id;
    /**
     * The extraction type @link(ExtractionType).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = ExtractionType.JPA_NAME_EXTRACTION_TYPE_FK, referencedColumnName = "id", nullable = false)
    ExtractionType extractionType;
    /**
     * The utilisateur @link(Utilisateur).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = Utilisateur.JPA_NAME_UTILISATEUR_FK, referencedColumnName = "id", nullable = false)
    Utilisateur utilisateur;

    /**
     * Instantiates a new requete.
     */
    public Requete() {
    }

    /**
     * Instantiates a new requete.
     *
     * @param extractionType the extraction type
     * @param utilisateur the utilisateur
     * @param commentaire the commentaire
     * @param nomFichier the nom fichier
     * @param l
     * @param dureeExtraction the duree extraction
     * @link(ExtractionType) the extraction type
     * @link(Utilisateur) the utilisateur
     * @link(Date) the date
     * @link(String) the commentaire
     * @link(String) the nom fichier
     * @link(Long) the duree extraction
     */
    public Requete(final ExtractionType extractionType, final Utilisateur utilisateur, final String commentaire, final String nomFichier, final Long dureeExtraction) {
        super();
        if (utilisateur == null) {
            throw new IllegalArgumentException("L'objet \"utilisateur\" ne peut pas être null");
        }
        if (extractionType == null) {
            throw new IllegalArgumentException("L'objet \"extractionType\" ne peut pas être null");
        }
        this.utilisateur = utilisateur;
        this.extractionType = extractionType;
        this.commentaire = commentaire;
        this.nomFichier = nomFichier;
        this.dureeExtraction = dureeExtraction;
    }

    /**
     * Gets the commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets the commentaire.
     *
     * @param commentaire the new commentaire
     */
    public void setCommentaire(final String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * Gets the duree extraction.
     *
     * @return the duree extraction
     */
    public Long getDureeExtraction() {
        return dureeExtraction;
    }

    /**
     *
     * @param date
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    /**
     * Sets the duree extraction.
     *
     * @param dureeExtraction the new duree extraction
     */
    public void setDureeExtraction(final Long dureeExtraction) {
        this.dureeExtraction = dureeExtraction;
    }

    /**
     * Gets the extraction type.
     *
     * @return the extraction type
     */
    public ExtractionType getExtractionType() {
        return extractionType;
    }

    /**
     * Sets the extraction type.
     *
     * @param extractionType the new extraction type
     */
    public void setExtractionType(final ExtractionType extractionType) {
        this.extractionType = extractionType;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the nom fichier.
     *
     * @return the nom fichier
     */
    public String getNomFichier() {
        return nomFichier;
    }

    /**
     * Sets the nom fichier.
     *
     * @param nomFichier the new nom fichier
     */
    public void setNomFichier(final String nomFichier) {
        this.nomFichier = nomFichier;
    }

    /**
     * Gets the utilisateur.
     *
     * @return the utilisateur
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     * Sets the utilisateur.
     *
     * @param utilisateur the new utilisateur
     */
    public void setUtilisateur(final Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }
}
