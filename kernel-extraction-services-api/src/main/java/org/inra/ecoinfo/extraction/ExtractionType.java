package org.inra.ecoinfo.extraction;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.entity.Requete;

/**
 * The Class ExtractionType.
 *
 * @author ptcherniati
 */
@Entity
@Table(indexes = @Index(name = "extractiontype_code_idx", columnList = "code"))
public class ExtractionType implements Serializable {

    /**
     *
     */
    public static final String JPA_NAME_EXTRACTION_TYPE_FK = "extractionType_id";
    /**
     * The Constant EXTRACTIONTYPE_NAME_ID @link(String).
     */
    public static final String EXTRACTIONTYPE_NAME_ID = "id";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The code @link(String).
     */
    @Column(nullable = false, unique = true)
    @NaturalId
    private String code;
    /**
     * The description @link(String).
     */
    @Column(columnDefinition = "TEXT")
    private String description;
    /**
     * The name @link(String).
     */
    @Column(nullable = false, unique = true)
    private String name;
    /**
     * The requetes @link(List<Requete>).
     */
    @Transient
    private List<Requete> requetes = new LinkedList<>();
    /**
     * The id @link(Long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected Long id;

    /**
     * Instantiates a new extraction type.
     */
    public ExtractionType() {
        super();
    }

    /**
     * Instantiates a new extraction type.
     *
     * @param extraction the extraction
     * @link(Extraction) the extraction
     */
    public ExtractionType(final Extraction extraction) {
        super();
        this.code = extraction.getCode();
        this.name = extraction.getName();
        this.description = extraction.getDescription();
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the requetes.
     *
     * @return the requetes
     */
    public List<Requete> getRequetes() {
        return requetes;
    }

    /**
     * Sets the requetes.
     *
     * @param requetes the new requetes
     */
    public void setRequetes(final List<Requete> requetes) {
        this.requetes = requetes;
    }
}
