package org.inra.ecoinfo.extraction;

import java.util.List;
import java.util.Map;

/**
 * The Interface IParameter.
 *
 * @author Philippe TCHERNIATINSKY
 */
public interface IParameter {

    /**
     * Gets the commentaire.
     *
     * @return the commentaire
     */
    String getCommentaire();

    /**
     * Gets the extraction type code.
     *
     * @return the extraction type code
     */
    String getExtractionTypeCode();

    /**
     * Gets the parameters.
     *
     * @return the parameters
     */
    Map<String, Object> getParameters();

    /**
     * Gets the results.
     *
     * @return the results
     */
    @SuppressWarnings("rawtypes")
    Map<String, Map<String, List>> getResults();

    /**
     * Sets the commentaire.
     *
     * @param commentaire the new commentaire
     */
    void setCommentaire(String commentaire);

    /**
     * Sets the parameters.
     *
     * @param parameters the parameters
     * @link(Map<String,Object>) the parameters
     */
    void setParameters(Map<String, Object> parameters);
}
