package org.inra.ecoinfo.extraction;

import java.io.Serializable;
import java.util.List;
import org.inra.ecoinfo.extraction.entity.Requete;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IExtractionManager.
 */
public interface IExtractionManager extends Serializable {

    /**
     * The Constant ID_BEAN @link(String).
     */
    String ID_BEAN = "extractionManager";
    /**
     * The Constant KEYMAP_COMMENTS @link(String).
     */
    String KEYMAP_COMMENTS = "comments";

    /**
     * Delete requests from ids.
     *
     * @param ids the ids
     * @throws BusinessException the business exception
     * @link(Long[]) the ids
     */
    void deleteRequestsFromIds(Long[] ids) throws BusinessException;

    /**
     * Do download.
     *
     * @param requete the requete
     * @return the byte[]
     * @throws BusinessException the business exception
     * @link(RequeteVO) the requete
     */
    byte[] doDownload(Requete requete) throws BusinessException;

    /**
     * Do download.
     *
     * @param codeDatatype the code datatype
     * @param suffixNameFile the suffix name file
     * @return the byte[]
     * @throws BusinessException the business exception
     * @link(String) the code datatype
     * @link(String) the suffix name file
     */
    byte[] doDownload(String codeDatatype, String suffixNameFile) throws BusinessException;

    /**
     * Extract.
     *
     * @param parameters the parameters
     * @param code the code
     * @throws BusinessException the business exception
     * @link(IParameter) the parameters
     * @link(int) the code
     */
    void extract(IParameter parameters, int code) throws BusinessException;

    /**
     * Gets the extractiontypes results availables.
     *
     * @param utilisateur the utilisateur
     * @return the extractiontypes results availables
     * @throws BusinessException the business exception
     * @link(Utilisateur) the utilisateur
     */
    List<ExtractionType> getExtractiontypesResultsAvailables(Utilisateur utilisateur) throws BusinessException;

    /**
     * Gets the requests results by extraction type code.
     *
     * @param extractionTypeCode the extraction type code
     * @param utilisateur the utilisateur
     * @return the requests results by extraction type code
     * @throws BusinessException the business exception
     * @link(String) the extraction type code
     * @link(Utilisateur) the utilisateur
     */
    List<Requete> getRequestsResultsByExtractionTypeCode(String extractionTypeCode, Utilisateur utilisateur) throws BusinessException;
}
