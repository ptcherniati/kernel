package org.inra.ecoinfo.extraction;

import java.time.LocalDateTime;
import java.util.zip.ZipOutputStream;

/**
 * The Class RObuildZipOutputStream.
 */
public class RObuildZipOutputStream {

    /**
     * The file suffix @link(String).
     */
    private final String fileSuffix;
    /**
     * The start date @link(Date).
     */
    private final LocalDateTime startDate;
    /**
     * The zip output stream @link(ZipOutputStream).
     */
    private final ZipOutputStream zipOutputStream;

    /**
     * Instantiates a new r obuild zip output stream.
     *
     * @param zipOutputStream the zip output stream
     * @param fileSuffix the file suffix
     * @param ldt
     * @param startDate the start date
     * @link(ZipOutputStream) the zip output stream
     * @link(String) the file suffix
     * @link(Date) the start date
     */
    public RObuildZipOutputStream(final ZipOutputStream zipOutputStream, final String fileSuffix,
            final LocalDateTime startDate) {
        super();
        this.zipOutputStream = zipOutputStream;
        this.fileSuffix = fileSuffix;
        this.startDate = startDate;
    }

    /**
     * Gets the file suffix.
     *
     * @return the file suffix
     */
    public String getFileSuffix() {
        return fileSuffix;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public LocalDateTime getStartDate() {
        return startDate;
    }

    /**
     * Gets the zip output stream.
     *
     * @return the zip output stream
     */
    public ZipOutputStream getZipOutputStream() {
        return zipOutputStream;
    }
}
