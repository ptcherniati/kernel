package org.inra.ecoinfo.extraction;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IOutputBuilder.
 */
public interface IOutputBuilder {

    /**
     * Builds the output.
     *
     * @param parameters the parameters
     * @return 
     * @throws BusinessException the business exception
     * @link(IParameter) the parameters
     */
    RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException;
}
