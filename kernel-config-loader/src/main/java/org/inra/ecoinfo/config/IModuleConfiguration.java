package org.inra.ecoinfo.config;

import java.io.InputStream;
import java.io.Serializable;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IModuleConfiguration.
 */
public interface IModuleConfiguration extends Serializable {

    /**
     * Creates the config.
     *
     * @param digester the Digester digester
     */
    void createConfig(Digester digester);

    /**
     * Gets the String module name.
     *
     * @return the String module name
     */
    String getModuleName();

    /**
     * Gets the String schema path.
     *
     * @return the String schema path
     */
    String getSchemaPath();

    /**
     * Gets the stream schema.
     *
     * @return The Stream resource of the schema
     */
    InputStream getStreamSchema();

    /**
     * Checks if is auto load.
     *
     * @return the Boolean boolean
     */
    Boolean isAutoLoad();

    /**
     *
     * @return
     */
    default Boolean ignore() {
        return false;
    }

    ;

    /**
     * Post config.
     *
     * @throws BusinessException the business exception
     */
    void postConfig() throws BusinessException;
}
