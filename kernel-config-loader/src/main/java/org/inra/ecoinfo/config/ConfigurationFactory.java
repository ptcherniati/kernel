package org.inra.ecoinfo.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.Transient;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * A factory for creating Configuration objects.
 */
public class ConfigurationFactory {

    protected static final String SRC_MAIN_RESOURCES_LOG4J_PROPERTIES = "src/main/resources/log4j.properties";
    /**
     * The Constant CORRECT_LOADING.
     */
    private static final String CORRECT_LOADING = "Le module %s a été correctement chargé";
    /**
     * The Constant BAD_LOADING.
     */
    private static final String BAD_LOADING = "Le module %s n'a pas été correctement chargé";
    /**
     * The Constant CORRECT_LOADING.
     */
    private static final String CORRECT_CONFIG = "Le module %s a été correctement paramétré";
    /**
     * The Constant BAD_LOADING.
     */
    private static final String BAD_CONFIG = "Le module %s n'a pas été correctement paramétré %n \t %s";
    /**
     * The Constant DYNAMIC_VALIDATION.
     */
    private static final String DYNAMIC_VALIDATION = "http://apache.org/xml/features/validation/dynamic";
    /**
     * The Constant CONFIG_PATH.
     */
    private static final String CONFIG_PATH = "/META-INF/configuration.xml";
    /**
     * The Logger LOGGER.
     */
    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationFactory.class);

    /**
     *
     */
    public static final String LOGGER_ERROR = "Method %s -> %s";
    /**
     * The Map<String,IModuleConfiguration> modules configuration.
     */
    private Map<String, IModuleConfiguration> modulesConfiguration = new HashMap<>();

    /**
     * Instantiates a new ConfigurationFactory configuration factory.
     *
     */
    public ConfigurationFactory() {
    }

    /**
     * Creates a new Configuration object.
     *
     * @return the module configuration
     * @throws ParserConfigurationException the parser configuration exception
     */
    public ModuleConfiguration createConfig() throws ParserConfigurationException {
        if (modulesConfiguration != null) {
            final InfosReport infosreport = new InfosReport("Loading and validate configuration success");
            final InfosReport errorsreport = new InfosReport("Loading and validate configuration errors");
            for (final Entry<String, IModuleConfiguration> moduleConfigurationEntry : modulesConfiguration.entrySet()) {
                final Digester digester = new Digester();
                // final File configurationFile = new
                // File(getClass().getResource(ConfigurationFactory.CONFIG_PATH).getPath());
                InputStream configurationFile = this.getClass().getResourceAsStream(ConfigurationFactory.CONFIG_PATH);

                try {
                    digester.setXMLSchema(getSchema(moduleConfigurationEntry.getValue()));
                    digester.setValidating(true);
                    digester.setNamespaceAware(true);
                    ErrorHandlerImpl errorHandler = new ErrorHandlerImpl();
                    digester.setErrorHandler(errorHandler);
                    digester.setUseContextClassLoader(true);
                    digester.setFeature(ConfigurationFactory.DYNAMIC_VALIDATION, true);
                } catch (final SAXException e1) {
                    UncatchedExceptionLogger.log(
                            String.format(ConfigurationFactory.CORRECT_LOADING, moduleConfigurationEntry.getKey()), e1);
                    infosreport.addInfos(moduleConfigurationEntry.getKey(),
                            String.format(ConfigurationFactory.CORRECT_LOADING, moduleConfigurationEntry.getKey()));
                }
                if (moduleConfigurationEntry.getValue().isAutoLoad()) {
                    digester.push(moduleConfigurationEntry.getValue());
                    moduleConfigurationEntry.getValue().createConfig(digester);
                }
                try {
                    digester.parse(configurationFile);
                } catch (final IOException | SAXException e) {
                    LOGGER.debug(String.format(ConfigurationFactory.BAD_LOADING, moduleConfigurationEntry.getKey()), e);
                    errorsreport.addInfos(moduleConfigurationEntry.getKey(),
                            String.format(ConfigurationFactory.BAD_LOADING, moduleConfigurationEntry.getKey()));
                }
                infosreport.addInfos(moduleConfigurationEntry.getKey(),
                        String.format(ConfigurationFactory.CORRECT_LOADING, moduleConfigurationEntry.getKey()));
            }
            if (!infosreport.getinfoMessages().isEmpty()) {
                LOGGER.info(infosreport.getMessages());
            }
            if (!errorsreport.getinfoMessages().isEmpty()) {
                LOGGER.error(infosreport.getMessages());
            }
        }
        final ModuleConfiguration moduleConfiguration = new ModuleConfiguration();
        moduleConfiguration.setModulesConfiguration(modulesConfiguration);
        return moduleConfiguration;
    }

    /**
     * Gets the Map<String,IModuleConfiguration> modules configuration.
     *
     * @return the Map<String,IModuleConfiguration> modules configuration
     */
    public Map<String, IModuleConfiguration> getModulesConfiguration() {
        return modulesConfiguration;
    }

    /**
     * Sets the modules configuration.
     *
     * @param modulesConfiguration the Map<String,IModuleConfiguration> modules
     *                             configuration
     */
    public void setModulesConfiguration(Map<String, IModuleConfiguration> modulesConfiguration) {
        this.modulesConfiguration = modulesConfiguration;
    }

    /**
     * Gets the Schema schema.
     *
     * @param moduleConfiguration the IModuleConfiguration module configuration
     * @return the Schema schema
     * @throws SAXException the sAX exception
     */
    public final Schema getSchema(IModuleConfiguration moduleConfiguration) throws SAXException {
        final StreamSource schema = new StreamSource(moduleConfiguration.getStreamSchema());
        final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        return schemaFactory.newSchema(schema);
    }

    /**
     * New integer class array.
     *
     * @return the Class[] class[]
     */
    @SuppressWarnings("rawtypes")
    public Class[] newIntegerClassArray() {
        return new Class[] { Integer.class, StringBuilder.class };
    }

    /**
     * New string class array.
     *
     * @return the Class[] class[]
     */
    @SuppressWarnings("rawtypes")
    public Class[] newStringClassArray() {
        return new Class[] { String.class, String.class };
    }

    /**
     * Post config.
     *
     * @param moduleConfiguration the module configuration
     * @return the string
     */
    @Transactional(rollbackFor = Exception.class)
    public String postConfig(IModuleConfiguration moduleConfiguration) {
        final InfosReport infosreport = new InfosReport("Configuring post-configuration success");
        final InfosReport errorsreport = new InfosReport("Configuring post-configuration errors");
        if (modulesConfiguration != null) {
            try {
                moduleConfiguration.postConfig();
                infosreport.addInfos(moduleConfiguration.getModuleName(),
                        String.format(ConfigurationFactory.CORRECT_CONFIG, moduleConfiguration.getModuleName()));
            } catch (final BusinessException e) {
                UncatchedExceptionLogger.log(String.format(ConfigurationFactory.BAD_CONFIG,
                        moduleConfiguration.getModuleName(), e.getMessage()), e);
                errorsreport.addInfos(moduleConfiguration.getModuleName(), String
                        .format(ConfigurationFactory.BAD_CONFIG, moduleConfiguration.getModuleName(), e.getMessage()));
            }
            if (!infosreport.getinfoMessages().isEmpty()) {
                LOGGER.info(infosreport.getMessages());
            }
            if (!errorsreport.getinfoMessages().isEmpty()) {
                LOGGER.error(errorsreport.getMessages());
            }
        }
        return infosreport.buildHTMLMessages();
    }

    private static class ErrorHandlerImpl implements ErrorHandler {

        @Override
        public void warning(final SAXParseException exception) throws SAXException {
            LOGGER.warn("parse Exception", exception);
        }

        @Override
        public void fatalError(final SAXParseException exception) throws SAXException {
            LOGGER.error("parse Exception", exception);
        }

        @Override
        public void error(final SAXParseException exception) throws SAXException {
            LOGGER.error("parse Exception", exception);
        }
    }
}
