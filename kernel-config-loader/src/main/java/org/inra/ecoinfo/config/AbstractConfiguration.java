package org.inra.ecoinfo.config;

import java.io.InputStream;
import java.util.function.Function;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AbstractConfiguration. Can be use to implements
 * IModuleConfiguration classes. Add some fonction to help building digester
 * parsing
 */
public abstract class AbstractConfiguration implements IModuleConfiguration {

    private static final long serialVersionUID = 8644246580256369739L;
    private boolean skip= false;

    /**
     *
     */
    public AbstractConfiguration() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getStreamSchema()
     */

    /**
     *
     * @return
     */

    @Override
    public InputStream getStreamSchema() {
        return getClass().getResourceAsStream(getSchemaPath());
    }

    /**
     * New integer class array.
     *
     * @return the Class[] class[]
     */
    @SuppressWarnings("rawtypes")
    public Class[] newIntegerClassArray() {
        return new Class[] { Integer.class, StringBuilder.class };
    }

    /**
     * New string class array.
     *
     * @return the Class[] class[]
     */
    @SuppressWarnings("rawtypes")
    public Class[] newStringClassArray() {
        return new Class[] { String.class, String.class };
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#postConfig()
     */

    /**
     *
     * @throws BusinessException
     */

    @Override
    public void postConfig() throws BusinessException {
        LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).info("no postConfig");
    }

    /**
     *
     * @return
     */
    public Function getPatternInRole() {
        return Function.identity();
    }

    /**
     *
     * @return
     */
    public boolean isSkip() {
        return skip;
    }

    /**
     *
     * @param skip
     */
    public void setSkip(boolean skip) {
        this.skip = skip;
    }
}
