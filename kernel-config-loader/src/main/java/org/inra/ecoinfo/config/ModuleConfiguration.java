package org.inra.ecoinfo.config;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class ModuleConfiguration.
 */
public class ModuleConfiguration {

    /**
     * The Map<String,IModuleConfiguration> modules configuration.
     */
    private Map<String, IModuleConfiguration> modulesConfiguration = new HashMap<>();

    /**
     * Adds the module configuration.
     *
     * @param moduleConfiguration the IModuleConfiguration module configuration
     */
    public void addModuleConfiguration(IModuleConfiguration moduleConfiguration) {
        LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).info("no addModuleConfiguration");
    }

    /**
     * Gets the module configuration.
     *
     * @param moduleName the module name
     * @return the module configuration
     */
    public IModuleConfiguration getModuleConfiguration(String moduleName) {
        return modulesConfiguration.get(moduleName);
    }

    /**
     * Gets the Map<String,IModuleConfiguration> modules configuration.
     *
     * @return the Map<String,IModuleConfiguration> modules configuration
     */
    public Map<String, IModuleConfiguration> getModulesConfiguration() {
        return modulesConfiguration;
    }

    /**
     * Sets the modules configuration.
     *
     * @param modulesConfiguration the Map<String,IModuleConfiguration> modules
     * configuration
     */
    public void setModulesConfiguration(Map<String, IModuleConfiguration> modulesConfiguration) {
        this.modulesConfiguration = modulesConfiguration;
    }
}
