package org.inra.ecoinfo;

/**
 * The Interface IIdConfiguration.
 */
public interface IIdConfiguration {

    /**
     * Gets the String id.
     *
     * @return the String id
     */
    String getId();

    /**
     * Sets the void id.
     *
     * @param id the String id
     */
    void setId(String id);
}
