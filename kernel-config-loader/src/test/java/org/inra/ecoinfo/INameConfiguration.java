package org.inra.ecoinfo;

import java.util.Map;

/**
 * The Interface INameConfiguration.
 */
public interface INameConfiguration {

    /**
     * Gets the Map<String,String> internationalized names.
     *
     * @return the Map<String,String> internationalized names
     */
    Map<String, String> getInternationalizedNames();

    /**
     * Sets the internationalized names.
     *
     * @param internationalizedNames the Map<String,String> internationalized
     *                               names
     */
    void setInternationalizedNames(Map<String, String> internationalizedNames);
}
