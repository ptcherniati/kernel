package org.inra.ecoinfo;

/**
 * The Interface IMailHostConfiguration.
 */
public interface IMailHostConfiguration {

    /**
     * Gets the String mail host.
     *
     * @return the String mail host
     */
    String getMailHost();

    /**
     * Sets the void mail host.
     *
     * @param mailHost the String mail host
     */
    void setMailHost(String mailHost);
}
