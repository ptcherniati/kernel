package org.inra.ecoinfo.config;

import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.IMailHostConfiguration;

/**
 * The Class MailHostConfiguration.
 */
public class MailHostConfiguration extends AbstractConfiguration implements IMailHostConfiguration {

    private static final long serialVersionUID = 8628336654255373442L;
    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "moduleConfiguration.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "mailHostConfiguration";
    /**
     * The mail host.
     */
    private String mailHost;

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.config.IModuleConfiguration#createConfig(org.apache.commons.
     * digester.Digester)
     */

    /**
     * @param digester
     */

    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("configuration/module/coreConfiguration/mailHost", "setMailHost", 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ICoreConfiguration#getMailHost()
     */
    @Override
    public String getMailHost() {
        return mailHost;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ICoreConfiguration#setMailHost(java.lang.String)
     */
    @Override
    public void setMailHost(String mailHost) {
        this.mailHost = mailHost;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getModuleName()
     */

    /**
     * @return
     */

    @Override
    public String getModuleName() {
        return MailHostConfiguration.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractConfiguration#getSchemaPath()
     */

    /**
     * @return
     */

    @Override
    public String getSchemaPath() {
        return MailHostConfiguration.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#isAutoLoad()
     */

    /**
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }
}
