package org.inra.ecoinfo.config;

import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.IIdConfiguration;

/**
 * The Class IdConfiguration.
 */
public class IdConfiguration extends AbstractConfiguration implements IIdConfiguration {

    private static final long serialVersionUID = -2055178402790191068L;
    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "moduleConfiguration.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "idConfiguration";
    /**
     * The id.
     */
    private String id;

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.config.IModuleConfiguration#createConfig(org.apache.commons.
     * digester.Digester)
     */

    /**
     * @param digester
     */

    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("configuration/module/coreConfiguration/id", "setId", 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ICoreConfiguration#getId()
     */

    /**
     * @return
     */

    @Override
    public String getId() {
        return id;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ICoreConfiguration#setId(java.lang.String)
     */

    /**
     * @param id
     */

    @Override
    public void setId(String id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getModuleName()
     */

    /**
     * @return
     */

    @Override
    public String getModuleName() {
        return IdConfiguration.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractConfiguration#getSchemaPath()
     */

    /**
     * @return
     */

    @Override
    public String getSchemaPath() {
        return IdConfiguration.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#isAutoLoad()
     */

    /**
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }
}
