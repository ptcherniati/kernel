package org.inra.ecoinfo.config;

import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.IMailAdminConfiguration;

/**
 * The Class MailAdminConfiguration.
 */
public class MailAdminConfiguration extends AbstractConfiguration implements IMailAdminConfiguration {

    private static final long serialVersionUID = -2091658121671350749L;
    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "moduleConfiguration.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "mailAdminConfiguration";
    /**
     * The mail admin.
     */
    private String mailAdmin;

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.config.IModuleConfiguration#createConfig(org.apache.commons.
     * digester.Digester)
     */

    /**
     * @param digester
     */

    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("configuration/module/coreConfiguration/mailAdmin", "setMailAdmin", 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ICoreConfiguration#getMailAdmin()
     */
    @Override
    public String getMailAdmin() {
        return mailAdmin;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.config.ICoreConfiguration#setMailAdmin(java.lang.String)
     */
    @Override
    public void setMailAdmin(String mailAdmin) {
        this.mailAdmin = mailAdmin;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getModuleName()
     */

    /**
     * @return
     */

    @Override
    public String getModuleName() {
        return MailAdminConfiguration.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractConfiguration#getSchemaPath()
     */

    /**
     * @return
     */

    @Override
    public String getSchemaPath() {
        return MailAdminConfiguration.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#isAutoLoad()
     */

    /**
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }
}
