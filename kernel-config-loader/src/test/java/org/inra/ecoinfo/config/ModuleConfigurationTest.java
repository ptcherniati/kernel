package org.inra.ecoinfo.config;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.dbunit.DatabaseUnitException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * The Class ModuleConfigurationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContext.xml"})
public class ModuleConfigurationTest {

    /**
     * The module configuration.
     */
    @Autowired
    IdConfiguration idConfiguration;
    /**
     * The mail admin configuration.
     */
    @Autowired
    MailAdminConfiguration mailAdminConfiguration;
    /**
     * The mail host configuration.
     */
    @Autowired
    MailHostConfiguration mailHostConfiguration;
    /**
     * The name configuration.
     */
    @Autowired
    NameConfiguration nameConfiguration;

    /**
     * Test.
     *
     * @throws URISyntaxException           the uRI syntax exception
     * @throws IOException                  Signals that an I/O exception has occurred.
     * @throws IllegalAccessException       the illegal access exception
     * @throws ConfigurationException       the configuration exception
     * @throws PersistenceException         the persistence exception
     * @throws DatabaseUnitException        the database unit exception
     * @throws SQLException                 the sQL exception
     * @throws ParserConfigurationException the parser configuration exception
     */
    @Test
    public void test() throws URISyntaxException, IOException, IllegalAccessException, ConfigurationException, PersistenceException, DatabaseUnitException, SQLException, ParserConfigurationException {
        Assert.assertNotNull("moduleConfiguration", idConfiguration);
        Assert.assertEquals("id is not defined", "projet-ORE", idConfiguration.getId());
        Assert.assertNotNull("moduleConfiguration", mailAdminConfiguration);
        Assert.assertEquals("mail admin is not defined", "appli@orleans.inra.fr", mailAdminConfiguration.getMailAdmin());
        Assert.assertNotNull("moduleConfiguration", mailHostConfiguration);
        Assert.assertEquals("mail host is not defined", "localhost", mailHostConfiguration.getMailHost());
        Assert.assertNotNull("moduleConfiguration", nameConfiguration);
        Assert.assertEquals("id is not defined", "ORE", nameConfiguration.getInternationalizedNames().get("fr"));
        Assert.assertEquals("id is not defined", "ORE_en", nameConfiguration.getInternationalizedNames().get("en"));
    }
}
