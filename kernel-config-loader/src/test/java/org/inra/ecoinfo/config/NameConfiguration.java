package org.inra.ecoinfo.config;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.INameConfiguration;

/**
 * The Class NameConfiguration.
 */
public class NameConfiguration extends AbstractConfiguration implements INameConfiguration {

    private static final long serialVersionUID = 2837907666586979583L;
    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "moduleConfiguration.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "nameConfiguration";
    /**
     * The internationalized names.
     */
    private Map<String, String> internationalizedNames = new HashMap<>();

    /**
     * Adds the internationalized name.
     *
     * @param language the String language
     * @param value    the String value
     */
    public void addInternationalizedName(final String language, final String value) {
        internationalizedNames.put(language, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.config.IModuleConfiguration#createConfig(org.apache.commons.
     * digester.Digester)
     */

    /**
     * @param digester
     */

    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("configuration/module/coreConfiguration/name", "addInternationalizedName", 2,
                newStringClassArray());
        digester.addCallParam("configuration/module/coreConfiguration/name", 0, "language");
        digester.addObjectCreate("configuration/module/coreConfiguration/name", String.class);
        digester.addCallMethod("configuration/module/coreConfiguration/name", "concat", 0);
        digester.addCallParam("configuration/module/coreConfiguration/name", 1, false);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ICoreConfiguration#getInternationalizedNames()
     */
    @Override
    public Map<String, String> getInternationalizedNames() {
        return internationalizedNames;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.config.ICoreConfiguration#setInternationalizedNames(java.
     * util.Map)
     */
    @Override
    public void setInternationalizedNames(Map<String, String> internationalizedNames) {
        this.internationalizedNames = internationalizedNames;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getModuleName()
     */

    /**
     * @return
     */

    @Override
    public String getModuleName() {
        return NameConfiguration.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractConfiguration#getSchemaPath()
     */

    /**
     * @return
     */

    @Override
    public String getSchemaPath() {
        return NameConfiguration.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#isAutoLoad()
     */

    /**
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }
}
