package org.inra.ecoinfo;

/**
 * The Interface IMailAdminConfiguration.
 */
public interface IMailAdminConfiguration {

    /**
     * Gets the String mail admin.
     *
     * @return the String mail admin
     */
    String getMailAdmin();

    /**
     * Sets the void mail admin.
     *
     * @param mailAdmin the String mail admin
     */
    void setMailAdmin(String mailAdmin);
}
