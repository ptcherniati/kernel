package org.inra.ecoinfo.jsf.logging;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import org.junit.After;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 * The Class LoggingTest.
 */
public class LoggingTest {

    /**
     * The Constant BUNDLE_PATH_SITE.
     */
    private static final String BUNDLE_PATH_SITE = "org.inra.ecoinfo.jsf.site";
    /**
     * The Constant BUNDLE_PATH_INDEX.
     */
    private static final String BUNDLE_PATH_INDEX = "org.inra.ecoinfo.jsf.index";
    /**
     * The Constant resourceBundle_site_fr.
     */
    private static final ResourceBundle resourceBundle_site_fr = ResourceBundle.getBundle(LoggingTest.BUNDLE_PATH_SITE, Locale.FRANCE);
    /**
     * The Constant resourceBundle_site_en.
     */
    private static final ResourceBundle resourceBundle_site_en = ResourceBundle.getBundle(LoggingTest.BUNDLE_PATH_SITE, Locale.ENGLISH);
    /**
     * The Constant resourceBundle_index_fr.
     */
    private static final ResourceBundle resourceBundle_index_fr = ResourceBundle.getBundle(LoggingTest.BUNDLE_PATH_INDEX, Locale.FRANCE);
    /**
     * The Constant resourceBundle_index_en.
     */
    private static final ResourceBundle resourceBundle_index_en = ResourceBundle.getBundle(LoggingTest.BUNDLE_PATH_INDEX, Locale.ENGLISH);
    /**
     * The Constant PARAM_MSG_SITE_TITLE.
     */
    private static final String PARAM_MSG_SITE_TITLE = "PARAM_MSG_SITE_TITLE";
    /**
     * The Constant PARAM_MSG_ACCUEIL.
     */
    private static final String PARAM_MSG_ACCUEIL = "PARAM_MSG_ACCUEIL";
    /**
     * The HtmlUnitDriver driver.
     */
    HtmlUnitDriver driver = new HtmlUnitDriver(true);

    /**
     * Connect login html.
     */
    @Test
    @Ignore
    public void connectLoginHTML() {
        final String siteTitle_fr = (String) LoggingTest.resourceBundle_site_fr.getObject(LoggingTest.PARAM_MSG_SITE_TITLE);
        final String siteTitle_en = (String) LoggingTest.resourceBundle_site_en.getObject(LoggingTest.PARAM_MSG_SITE_TITLE);
        final String home_fr = (String) LoggingTest.resourceBundle_index_fr.getObject(LoggingTest.PARAM_MSG_ACCUEIL);
        final String home_en = (String) LoggingTest.resourceBundle_index_en.getObject(LoggingTest.PARAM_MSG_ACCUEIL);
        driver.get("http://localhost:9090/si_kernel/index.jsf");
        final String titre = driver.getTitle();
        Assert.assertTrue("pas de connection possible", titre != null && !"".equals(titre));
        Assert.assertTrue(String.format("Le titre \"%s\" est incorrect il devrait commencer par \"%s\" ou \"%s\".", titre, siteTitle_fr, siteTitle_en), titre.startsWith(siteTitle_fr) || titre.startsWith(siteTitle_en));
        Assert.assertTrue(String.format("Le titre \"%s\" est incorrect il devrait finir par \"%s\" ou \"%s\".", titre, home_fr, home_en), titre.endsWith(home_fr) || titre.endsWith(home_en));
        driver.findElementByCssSelector("input[type=text]").sendKeys("admin");
        driver.findElementByCssSelector("input[type=password]").sendKeys("admin");
        driver.findElementByCssSelector("input[type=submit]").click();
        Assert.assertEquals("admin admin", driver.findElementByCssSelector("span.login").getText());
        final List<WebElement> sousMenus = driver.findElementsByCssSelector(".submenu");
        sousMenus.stream().map((webElement) -> webElement.findElements(By.cssSelector(".menuItem A"))).forEach((menuItems) -> {
            for (final WebElement webElement2 : menuItems) {
            }
        });
    }

    /**
     * Stop driver.
     */
    @After
    public void stopDriver() {
        driver.close();
    }
}
