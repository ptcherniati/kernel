package org.inra.ecoinfo.jsf.logging;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * The Class AllTests.
 */
@RunWith(Suite.class)
@SuiteClasses({LoggingTest.class})
public class AllTests {
}
