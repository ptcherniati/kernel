
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.utils;

import java.time.LocalDate;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import org.easymock.Mock;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class LocalDateConverterMM_YYYYTest {
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    @Mock
    FacesContext context = null;
    @Mock
    UIComponent component = null;
    LocalDateConverterMM_YYYY instance;
    String date = "12/1965";
    String resultDate = "1965-12-01";
    String format = DateUtil.MM_YYYY;
    LocalDate localDate = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, "01/12/1965");
    /**
     *
     */
    public LocalDateConverterMM_YYYYTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new LocalDateConverterMM_YYYY();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getAsObject method, of class LocalDateConverterMM_YYYY.
     */
    @Test
    public void testGetAsObject() {
        Object result = instance.getAsObject(context, component, date);
        assertEquals(resultDate, result.toString());
    }

    /**
     * Test of getAsString method, of class LocalDateConverterMM_YYYY.
     */
    @Test
    public void testGetAsString() {
        String result = instance.getAsString(context, component, localDate);
        assertEquals(date, result);
    }

}
