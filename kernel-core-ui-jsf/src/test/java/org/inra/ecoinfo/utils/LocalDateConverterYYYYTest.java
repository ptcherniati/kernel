
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.utils;

import java.time.LocalDate;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import org.easymock.Mock;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class LocalDateConverterYYYYTest {
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    @Mock
    FacesContext context = null;
    @Mock
    UIComponent component = null;
    LocalDateConverterYYYY instance;
    String date = "1965";
    String resultDate = "1965-01-01";
    String format = DateUtil.YYYY;
    LocalDate localDate = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, "01/01/1965");
    /**
     *
     */
    public LocalDateConverterYYYYTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new LocalDateConverterYYYY();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getAsObject method, of class LocalDateConverterYYYY.
     */
    @Test
    public void testGetAsObject() {
        Object result = instance.getAsObject(context, component, date);
        assertEquals(resultDate, result.toString());
    }

    /**
     * Test of getAsString method, of class LocalDateConverterYYYY.
     */
    @Test
    public void testGetAsString() {
        String result = instance.getAsString(context, component, localDate);
        assertEquals(date, result);
    }

}
