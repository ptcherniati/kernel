
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.utils;

import java.time.LocalTime;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import org.easymock.Mock;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class LocalTimeConverterHH_MM_SSTest {
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    @Mock
    FacesContext context = null;
    @Mock
    UIComponent component = null;
    LocalTimeConverterHH_MM_SS instance;
    String date = "23:30:41";
    String resultDate = "23:30:41";
    String format = DateUtil.HH_MM_SS;
    LocalTime localDate = DateUtil.readLocalTimeFromText(format, date);
    /**
     *
     */
    public LocalTimeConverterHH_MM_SSTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new LocalTimeConverterHH_MM_SS();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getAsObject method, of class LocalTimeConverterHH_MM_SS.
     */
    @Test
    public void testGetAsObject() {
        Object result = instance.getAsObject(context, component, date);
        assertEquals(resultDate, result.toString());
    }

    /**
     * Test of getAsString method, of class LocalTimeConverterHH_MM_SS.
     */
    @Test
    public void testGetAsString() {
        String result = instance.getAsString(context, component, localDate);
        assertEquals(date, result);
    }

}
