package org.inra.ecoinfo.utils;

import java.time.LocalDateTime;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import org.easymock.Mock;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class LocalDateTimeConverterDD_MM_YYYY_FILETest {
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    @Mock
    FacesContext context = null;
    @Mock
    UIComponent component = null;
    LocalDateTimeConverterDD_MM_YYYY_FILE instance;
    String date = "25-12-1965";
    String resultDate = "1965-12-25T00:00";
    String format = DateUtil.DD_MM_YYYY_FILE;
    LocalDateTime localDate = DateUtil.readLocalDateTimeFromText(format, date);
    /**
     *
     */
    public LocalDateTimeConverterDD_MM_YYYY_FILETest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new LocalDateTimeConverterDD_MM_YYYY_FILE();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getAsObject method, of class LocalDateConverterDD_MM_YYYY.
     */
    @Test
    public void testGetAsObject() {
        Object result = instance.getAsObject(context, component, date);
        assertEquals(resultDate, result.toString());
    }

    /**
     * Test of getAsString method, of class LocalDateConverterDD_MM_YYYY.
     */
    @Test
    public void testGetAsString() {
        String result = instance.getAsString(context, component, localDate);
        assertEquals(date, result);
    }

}
