
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.utils;

import java.time.LocalDate;
import javax.faces.context.FacesContext;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.primefaces.component.calendar.Calendar;

/**
 * @author tcherniatinsky
 */
public class LocalDateConverterDD_MM_YYYYTest {
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    @Mock
    FacesContext context = null;
    @Mock
    Calendar component;
    LocalDateConverterDD_MM_YYYY instance;
    String date = "25/12/1965";
    String resultDate = "1965-12-25";
    String date2 = "12/1965";
    String resultDate2 = "1965-12-01";
    String format = DateUtil.DD_MM_YYYY;
    LocalDate localDate = DateUtil.readLocalDateFromText(format, date);
    LocalDate localDate2 = DateUtil.readLocalDateFromText(format, "01/".concat(date2));
    /**
     *
     */
    public LocalDateConverterDD_MM_YYYYTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new LocalDateConverterDD_MM_YYYY();
        Mockito.when(component.getPattern()).thenReturn(DateUtil.DD_MM_YYYY);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getAsObject method, of class LocalDateConverterDD_MM_YYYY.
     */
    @Test
    public void testGetAsObject() {
        Object result = instance.getAsObject(context, component, date);
        assertEquals(resultDate, result.toString());
        Mockito.when(component.getPattern()).thenReturn(DateUtil.MM_YYYY);
        result = instance.getAsObject(context, component, date2);
        assertEquals(resultDate2, result.toString());
    }

    /**
     * Test of getAsString method, of class LocalDateConverterDD_MM_YYYY.
     */
    @Test
    public void testGetAsString() {
        String result = instance.getAsString(context, component, localDate);
        assertEquals(date, result);
        Mockito.when(component.getPattern()).thenReturn(DateUtil.MM_YYYY);
        result = instance.getAsString(context, component, localDate2);
        assertEquals(date2, result);
    }

}
