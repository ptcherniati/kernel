
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.utils;

import java.time.LocalDateTime;
import java.util.Locale;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import org.mockito.Mock;
import static org.mockito.Mockito.doReturn;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class LocalDateTimeConverterDD_MM_YYYY_AT_HH_MMTest {
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    @Mock
    FacesContext contextFr;
    @Mock
    ExternalContext externalContextFr;
    @Mock
    FacesContext contextEn;
    @Mock
    ExternalContext externalContextEn;
    @Mock
    UIComponent component;
    LocalDateTimeConverterDD_MM_YYYY_AT_HH_MM instance;
    String dateFr = "25/12/1965 à 23:30";
    String dateEn = "25/12/1965 at 23:30";
    String resultDate = "1965-12-25T23:30";
    String formatFr = "dd/MM/yyyy 'à' HH:mm";
    String formatEn = "dd/MM/yyyy 'at' HH:mm";
    LocalDateTime localDateFr;
    LocalDateTime localDateEn;
    /**
     *
     */
    public LocalDateTimeConverterDD_MM_YYYY_AT_HH_MMTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new LocalDateTimeConverterDD_MM_YYYY_AT_HH_MM();
        localDateFr = DateUtil.readLocalDateTimeFromText(formatFr, dateFr);
        localDateEn = DateUtil.readLocalDateTimeFromText(formatEn, dateEn);
        doReturn(externalContextFr).when(contextFr).getExternalContext();
        doReturn(externalContextEn).when(contextEn).getExternalContext();
        doReturn(Locale.FRANCE).when(externalContextFr).getRequestLocale();
        doReturn(Locale.ENGLISH).when(externalContextEn).getRequestLocale();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getAsObject method, of class
     * LocalDateConverterDD_MM_YYYY_AT_HH_MM.
     */
    @Test
    public void testGetAsObject() {
        Object result = instance.getAsObject(contextFr, component, dateFr);
        assertEquals(resultDate, result.toString());
        result = instance.getAsObject(contextEn, component, dateEn);
        assertEquals(resultDate, result.toString());
    }

    /**
     * Test of getAsString method, of class
     * LocalDateConverterDD_MM_YYYY_AT_HH_MM.
     */
    @Test
    public void testGetAsString() {
        String result = instance.getAsString(contextFr, component, localDateFr);
        assertEquals(dateFr, result);
        result = instance.getAsString(contextEn, component, localDateEn);
        assertEquals(dateEn, result);
    }

}
