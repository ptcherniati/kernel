package org.inra.ecoinfo.localization.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class UIBeanLocalization.
 */
@ManagedBean(name = "uiLocalization")
@SessionScoped
public class UIBeanLocalization implements Serializable {


    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The boolean change localization.
     */
    private boolean changeLocalization = false;
    /**
     * The List<Language> languages.
     */
    private final List<Language> languages = new LinkedList<>();
    /**
     * The boolean locale changed.
     */
    private boolean localeChanged = false;
    /**
     * The ILocalizationManager localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    private ILocalizationManager localizationManager;
    /**
     * The String new localization.
     */
    private String newLocalization = "";
    /**
     * The IPolicyManager security context.
     */
    @ManagedProperty(value = "#{policyManager}")
    private IPolicyManager policyManager;

    /**
     * Instantiates a new UIBeanLocalization uI bean localization.
     */
    public UIBeanLocalization() {
        initLanguages();
    }

    /**
     * Change localization.
     *
     * @return the String string
     */
    public String changeLocalization() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        newLocalization = params.get("localization");

        changeLocalization = !changeLocalization;
        if (policyManager.getCurrentUser()== null) {
            setJSFLocale(new Locale(newLocalization));
            localizationManager.getUserLocale().setLanguage(newLocalization);
            localeChanged = true;
        } else {
            try {
                setJSFLocale(localizationManager.changeLocale(new Locale(newLocalization)));
            } catch (final BusinessException e) {
                UncatchedExceptionLogger.logUncatchedException("uncatched PersistenceException in changeLocalization", e);
            }
            localeChanged = false;
        }
        try {
            final String page = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRequestURI().replaceAll("^.*/", "");
            getContext().getExternalContext().redirect(page);
        } catch (final IOException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched IOException in changeLocalization", e);
        }
        return "";
    }

    /**
     * Gets the boolean checks if is change localization.
     *
     * @return the boolean checks if is change localization
     */
    public boolean getIsChangeLocalization() {
        return changeLocalization;
    }

    /**
     * Gets the List<Language> languages.
     *
     * @return the List<Language> languages
     */
    public List<Language> getLanguages() {
        final List<Language> languagesToShow = new LinkedList<>();
        languages.stream().filter((language) -> (changeLocalization || language.getIsSelected())).forEach((language) -> {
            languagesToShow.add(language);
        });
        return languagesToShow;
    }

    /**
     * Gets the Locale locale.
     *
     * @return the Locale locale
     */
    public Locale getLocale() {
        return localizationManager.getUserLocale().getLocale();
    }

    /**
     * Gets the ILocalizationManager localization manager.
     *
     * @return the ILocalizationManager localization manager
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     * Sets the void localization manager.
     *
     * @param localizationManager the ILocalizationManager localization manager
     */
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
        this.localizationManager.getUserLocale().setLanguage(getContext().getViewRoot().getLocale().getLanguage());
    }

    /**
     * Gets the List<String> localizations.
     *
     * @return the List<String> localizations
     */
    public List<String> getLocalizations() {
        return Localization.getLocalisations();
    }

    /**
     * Inits the locale.
     */
    public void initLocale() {
        if (localeChanged) {
            try {
                getContext().getViewRoot().setLocale(localizationManager.changeLocale(getJSFLocale()));
            } catch (final BusinessException e) {
                UncatchedExceptionLogger.logUncatchedException("uncatched PersistenceException in initLocale", e);
            }
            localeChanged = false;
            return;
        }
        try {
            final Locale registeredLocale = localizationManager.initLocale(getJSFLocale());
            getContext().getViewRoot().setLocale(registeredLocale);
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched PersistenceException in initLocale", e);
        }
    }

    /**
     * Sets the void new localization.
     *
     * @param newLocalization the String new localization
     */
    public void setNewLocalization(final String newLocalization) {
        this.newLocalization = newLocalization;
    }

    /**
     * Sets the void security context.
     *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Toggle localization.
     *
     * @return the String string
     */
    public String toggleLocalization() {
        changeLocalization = !changeLocalization;
        return "true";
    }

    /**
     * Gets the FacesContext context.
     *
     * @return the FacesContext context
     */
    protected FacesContext getContext() {
        return FacesContext.getCurrentInstance();
    }

    /**
     * Gets the Locale jSF locale.
     *
     * @return the Locale jSF locale
     */
    protected Locale getJSFLocale() {
        return getContext().getViewRoot().getLocale();
    }

    /**
     * Sets the void jSF locale.
     *
     * @param locale the Locale locale
     */
    protected void setJSFLocale(final Locale locale) {
        getContext().getViewRoot().setLocale(locale);
    }

    /**
     * Inits the languages.
     */
    protected void initLanguages() {
        languages.clear();
        getLocalizations().stream().map((localization) -> new Language(localization)).forEach((language) -> {
            languages.add(language);
        });
    }

    /**
     * The Class Language.
     */
    public class Language {

        /**
         * The Constant JSF_LOCALIZATION.
         */
        private static final String JSF_LOCALIZATION = "org.inra.ecoinfo.localization.jsf.localization";
        /**
         * The String localization.
         */
        private final String localization;

        /**
         * Instantiates a new UIBeanLocalization language.
         *
         * @param localization the String localization
         */
        public Language(final String localization) {
            super();
            this.localization = localization;
        }

        /**
         * Gets the String click action.
         *
         * @return the String click action
         */
        public String getClickAction() {
            return getIsSelected() ? "toggleLocalization()" : String.format("changeLocalization([{name:'localization', value:'%s'}])", localization);
        }

        /**
         * Gets the boolean checks if is selected.
         *
         * @return the boolean checks if is selected
         */
        public boolean getIsSelected() {
            if (localizationManager == null) {
                return getJSFLocale().getLanguage().equals(localization);
            } else {
                return getLocale().getLanguage().equals(localization);
            }
        }

        /**
         * Gets the String localization.
         *
         * @return the String localization
         */
        public String getLocalization() {
            return localization;
        }

        /**
         * Gets the String tool tip.
         *
         * @return the String tool tip
         */
        public String getToolTip() {
            String tooltip = localizationManager.getMessage(Language.JSF_LOCALIZATION, String.format("PROPERTY_MSG_CHANGE_%s", localization.toUpperCase()));
            if (getIsSelected() && !changeLocalization) {
                tooltip = localizationManager.getMessage(Language.JSF_LOCALIZATION, "PROPERTY_MSG_CURRENT_NAVIGATION");
            }
            return tooltip;
        }
    }
}
