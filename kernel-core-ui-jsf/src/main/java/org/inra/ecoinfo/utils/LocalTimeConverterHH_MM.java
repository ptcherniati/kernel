package org.inra.ecoinfo.utils;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Faces converter for support of LocalDate
 *
 * @author Juneau
 */
@FacesConverter(value = "localTimeConverteHH_MM")
public class LocalTimeConverterHH_MM implements javax.faces.convert.Converter {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalTimeConverterHH_MM.class);

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.utils.dateConverter";
    /**
     * The Constant MSG_END_DATE_INVALID.
     */
    private static final String MSG_END_TIME_INVALID = "PROPERTY_MSG_END_TIME_INVALID";
    /**
     * The Constant MSG_START_DATE_INVALID.
     */
    private static final String MSG_START_TIME_INVALID = "PROPERTY_MSG_START_TIME_INVALID";
    private static final String COMPONENT_TIME_START_ID = "timeStart";
    private static final String COMPONENT_TIME_END_ID = "timeEnd";
    private static final Map<String, ResourceBundle> bundles = new HashMap<String, ResourceBundle>();

    /**
     *
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        LocalTime time = null;
        try {
            time = DateUtil.readLocalTimeFromText(DateUtil.HH_MM, value);
        } catch (DateTimeParseException e) {
            Locale locale = context.getViewRoot().getLocale();
            ResourceBundle bundle = bundles
                    .computeIfAbsent(
                            locale.getLanguage(),
                            k -> ResourceBundle.getBundle(
                                    BUNDLE_SOURCE_PATH,
                                    locale
                            )
                    );
            String message = String.format(
                    bundle.getString(COMPONENT_TIME_START_ID.equals(component.getId()) ? MSG_START_TIME_INVALID : MSG_END_TIME_INVALID),
                    value,
                    DateUtil.HH_MM
            );

            throw new ConverterException(message, e);
        }
        return time;
    }

    /**
     *
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        LocalTime dateValue = (LocalTime) value;
        String time = null;
        try {
            time = DateUtil.getUTCDateTextFromLocalDateTime(dateValue, DateUtil.HH_MM);
        } catch (DateTimeParseException e) {
            throw new ConverterException(e);
        } catch (ClassCastException e) {
            LOGGER.debug(e.getMessage(), e);
            return value.toString();
        }
        return time;
    }

}
