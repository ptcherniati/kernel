package org.inra.ecoinfo.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Faces converter for support of LocalDate
 *
 * @author Juneau
 */
@FacesConverter(value = "localDateTimeConverterDD_MM_YYYY_AT_HH_MM")
public class LocalDateTimeConverterDD_MM_YYYY_AT_HH_MM implements javax.faces.convert.Converter {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalDateTimeConverterDD_MM_YYYY_AT_HH_MM.class);

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.utils.dateConverter";
    /**
     * The Constant MSG_END_DATE_INVALID.
     */
    private static final String MSG_END_DATE_INVALID = "PROPERTY_MSG_END_DATE_INVALID";
    /**
     * The Constant MSG_START_DATE_INVALID.
     */
    private static final String MSG_START_DATE_INVALID = "PROPERTY_MSG_START_DATE_INVALID";
    private static final String COMPONENT_DATE_START_ID = "dateStart";
    private static final String COMPONENT_DATE_END_ID = "dateEnd";
    private static final Map<String, ResourceBundle> bundles = new HashMap<String, ResourceBundle>();
    private static final String BUNDLE_PATH_INDEX = "org.inra.ecoinfo.jsf.date_converter";
    private static final String PARAM_MSG_AT = "PARAM_MSG_AT";

    /**
     *
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Locale locale = context.getExternalContext().getRequestLocale();
        ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_PATH_INDEX, locale);
        String at = bundle.getString(PARAM_MSG_AT);
        at = Strings.isNullOrEmpty(at) ? "at" : at;
        LocalDateTime date = null;
        String format = "";
        try {
            format = String.format("%s '%s' %s", DateUtil.DD_MM_YYYY, at, DateUtil.HH_MM);
            date = DateUtil.readLocalDateTimeFromText(format, value);
        } catch (DateTimeParseException e) {
            ResourceBundle bundle2 = bundles
                    .computeIfAbsent(
                            locale.getLanguage(),
                            k -> ResourceBundle.getBundle(
                                    BUNDLE_SOURCE_PATH,
                                    locale
                            )
                    );
            String message = String.format(
                    bundle.getString(COMPONENT_DATE_START_ID.equals(component.getId()) ? MSG_START_DATE_INVALID : MSG_END_DATE_INVALID),
                    value,
                    format
            );

            throw new ConverterException(message, e);
        }
        return date;
    }

    /**
     *
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        LocalDateTime dateValue = (LocalDateTime) value;
        Locale locale = context.getExternalContext().getRequestLocale();
        ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_PATH_INDEX, locale);
        String at = bundle.getString(PARAM_MSG_AT);
        at = Strings.isNullOrEmpty(at) ? "at" : at;
        String format = null;
        try {
            format = DateUtil.getUTCDateTextFromLocalDateTime(dateValue, String.format("%s '%s' %s", DateUtil.DD_MM_YYYY, at, DateUtil.HH_MM));
        } catch (DateTimeParseException e) {
            throw new ConverterException(e);
        } catch (ClassCastException e) {
            LOGGER.debug(e.getMessage(), e);
            return value.toString();
        }
        return format;
    }
}
