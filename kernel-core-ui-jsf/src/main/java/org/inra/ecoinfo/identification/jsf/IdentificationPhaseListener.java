package org.inra.ecoinfo.identification.jsf;

import java.io.IOException;
import javax.faces.application.ViewExpiredException;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.localization.IInternationalizable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The listener interface for receiving identificationPhase events. The class
 * that is interested in processing a identificationPhase event implements this
 * interface, and the object created with that class is registered with a
 * component using the component's <code>addIdentificationPhaseListener<code> method. When
 * the identificationPhase event occurs, that object's appropriate
 * method is invoked.
 *
 * @see IdentificationPhaseEvent
 */
public class IdentificationPhaseListener implements PhaseListener, IInternationalizable {

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.identification.messages";
    /**
     * The Constant EXTENTION_CSS.
     */
    private static final String EXTENTION_CSS = ".css";
    /**
     * The Constant EXTENTION_PAGE_PATTERN.
     */
    private static final String EXTENTION_PAGE_PATTERN = "%s%s%s";
    /**
     * The Constant EXTENTION_XHTML.
     */
    private static final String EXTENTION_XHTML = ".xhtml";
    /**
     * The Constant MSG_SESSION_EXPIRATED.
     */
    private static final String MSG_SESSION_EXPIRATED = "MSG_SESSION_EXPIRATED";
    /**
     * The Constant NAVIGATION_RETRIEVE_PASSWORD.
     */
    private static final String NAVIGATION_RETRIEVE_PASSWORD = "retrievePassword";
    /**
     * The Constant NAVIGATION_RETRIEVE_PASSWORD_LOST.
     */
    private static final String NAVIGATION_RETRIEVE_PASSWORD_LOST = "retrievePasswordSendEmail";
    /**
     * The Constant NAVIGATION_CREATE_PROFILE @link(String).
     */
    private static final String NAVIGATION_CREATE_PROFILE = "createProfile";
    /**
     * The Constant NAVIGATION_MENTION_LEGALE @link(String).
     */
    private static final String NAVIGATION_MENTION_LEGALE = "mentionLegale";
    /**
     * The Constant NAVIGATION_CONTACT @link(String).
     */
    private static final String NAVIGATION_CONTACT = "contact";
    /**
     * The Constant NAVIGATION_MAIL_ACCOUNT @link(String).
     */
    private static final String NAVIGATION_MAIL_ACCOUNT = "mailAccount";
    /**
     * The Constant NAVIGATION_VALIDATE_ACCOUNT @link(String).
     */
    private static final String NAVIGATION_VALIDATE_ACCOUNT = "validateAccount";
    /**
     * The Constant NAVIGATION_CHARTER @link(String).
     */
    private static final String NAVIGATION_CHARTER = "charter";
    /**
     * The Constant NAVIGATION_SYNTHESIS @link(String).
     */
    private static final String NAVIGATION_SYNTHESIS = "synthesisManagement";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The Constant STRING_EMPTY.
     */
    private static final String STRING_EMPTY = "";
    /**
     * The Constant NAVIGATION_LOGIN.
     */
    static final String NAVIGATION_LOGIN = "login";
    /**
     * The Constant NAVIGATION_LOGIN.
     */
    static final String NAVIGATION_INDEX = "home";
    /**
     * The ILocalizationManager localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    private ILocalizationManager localizationManager;
    /**
     * The String[] ressources identification shunted.
     */
    String[] ressourcesIdentificationShunted = {String.format(IdentificationPhaseListener.EXTENTION_PAGE_PATTERN, IdentificationPhaseListener.STRING_EMPTY, IdentificationPhaseListener.NAVIGATION_LOGIN, IdentificationPhaseListener.EXTENTION_XHTML),
        IdentificationPhaseListener.EXTENTION_CSS,
        String.format(IdentificationPhaseListener.EXTENTION_PAGE_PATTERN, IdentificationPhaseListener.STRING_EMPTY, IdentificationPhaseListener.NAVIGATION_RETRIEVE_PASSWORD, IdentificationPhaseListener.EXTENTION_XHTML),
        String.format(IdentificationPhaseListener.EXTENTION_PAGE_PATTERN, IdentificationPhaseListener.STRING_EMPTY, IdentificationPhaseListener.NAVIGATION_RETRIEVE_PASSWORD_LOST, IdentificationPhaseListener.EXTENTION_XHTML),
        String.format(IdentificationPhaseListener.EXTENTION_PAGE_PATTERN, IdentificationPhaseListener.STRING_EMPTY, IdentificationPhaseListener.NAVIGATION_CREATE_PROFILE, IdentificationPhaseListener.EXTENTION_XHTML),
        String.format(IdentificationPhaseListener.EXTENTION_PAGE_PATTERN, IdentificationPhaseListener.STRING_EMPTY, IdentificationPhaseListener.NAVIGATION_SYNTHESIS, IdentificationPhaseListener.EXTENTION_XHTML),
        String.format(IdentificationPhaseListener.EXTENTION_PAGE_PATTERN, IdentificationPhaseListener.STRING_EMPTY, IdentificationPhaseListener.NAVIGATION_MENTION_LEGALE, IdentificationPhaseListener.EXTENTION_XHTML),
        String.format(IdentificationPhaseListener.EXTENTION_PAGE_PATTERN, IdentificationPhaseListener.STRING_EMPTY, IdentificationPhaseListener.NAVIGATION_CONTACT, IdentificationPhaseListener.EXTENTION_XHTML),
        String.format(IdentificationPhaseListener.EXTENTION_PAGE_PATTERN, IdentificationPhaseListener.STRING_EMPTY, IdentificationPhaseListener.NAVIGATION_MAIL_ACCOUNT, IdentificationPhaseListener.EXTENTION_XHTML),
        String.format(IdentificationPhaseListener.EXTENTION_PAGE_PATTERN, IdentificationPhaseListener.STRING_EMPTY, IdentificationPhaseListener.NAVIGATION_VALIDATE_ACCOUNT, IdentificationPhaseListener.EXTENTION_XHTML),
        String.format(IdentificationPhaseListener.EXTENTION_PAGE_PATTERN, IdentificationPhaseListener.STRING_EMPTY, IdentificationPhaseListener.NAVIGATION_CHARTER, IdentificationPhaseListener.EXTENTION_XHTML)};

    /*
     * (non-Javadoc)
     * 
     * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
     */
    /**
     *
     * @param event
     */
    @Override
    public void afterPhase(final PhaseEvent event) {
        final FacesContext context = event.getFacesContext();
        if ("true".equals(context.getExternalContext().getRequestParameterMap().get("activate"))) {
            activateUser(context);
        }
        final HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        final IPolicyManager policyManager = (IPolicyManager) session.getAttribute("scopedTarget.policyManager");
        final String viewId = context.getViewRoot().getViewId();
        final boolean isIdentificationNeeded = ((javax.servlet.http.HttpServletRequest) context.getExternalContext().getRequest()).getParameter("retrievePassword") == null && isIdentificationNeeded(viewId, context);
        if (isIdentificationNeeded) {
            final boolean noUserInContext = policyManager == null || policyManager.getCurrentUser()== null;
            if (noUserInContext) {
                redirectWithRefresh(context, IdentificationPhaseListener.NAVIGATION_LOGIN);
            } else if (viewId.contains(IdentificationPhaseListener.NAVIGATION_LOGIN)) {
                redirectWithRefresh(context, IdentificationPhaseListener.NAVIGATION_INDEX);
            }
        } else {
            initContext(policyManager, viewId, context);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent)
     */
    /**
     *
     * @param event
     */
    @Override
public void beforePhase(final PhaseEvent event) {
    //LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).info("IdentificationPhaseListener beforePhase unused");
}

    /*
     * (non-Javadoc)
     * 
     * @see javax.faces.event.PhaseListener#getPhaseId()
     */
    /**
     *
     * @return
     */
@Override
public PhaseId getPhaseId() {
    return PhaseId.RESTORE_VIEW;
}

/**
 * Checks if is identification needed.
     *
     * @param viewId the String view id
     * @param context the FacesContext context
     * @return the Boolean boolean
     */
    public Boolean isIdentificationNeeded(final String viewId, final FacesContext context) {
        if (context.getExceptionHandler().getUnhandledExceptionQueuedEvents().iterator().hasNext()) {
            final java.util.Iterator<ExceptionQueuedEvent> unhandledExceptionsQueuedEvents = context.getExceptionHandler().getUnhandledExceptionQueuedEvents().iterator();
            final ExceptionQueuedEvent exceptionQueuedEvent = unhandledExceptionsQueuedEvents.next();
            if (exceptionQueuedEvent.getContext().getException() instanceof ViewExpiredException) {
                if (localizationManager != null) {
                    UncatchedExceptionLogger.info(localizationManager.getMessage(IdentificationPhaseListener.BUNDLE_SOURCE_PATH, IdentificationPhaseListener.MSG_SESSION_EXPIRATED), new BusinessException());
                }
                unhandledExceptionsQueuedEvents.remove();
                return true;
            }
        }
        for (final String ressource : ressourcesIdentificationShunted) {
            if (viewId.contains(ressource)) {
                return false;
            }
        }
        return true;
    }

    /*
    * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.localization.IInternationalizable#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    /**
     *
     * @param localizationManager
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }
    
    private void activateUser(FacesContext context) {
        String login = context.getExternalContext().getRequestParameterMap().get("login");
        IUsersManager usersManager = (IUsersManager) context.getApplication().getELResolver().getValue(context.getELContext(), null, "usersManager");
        try {
            usersManager.activateUser(login);
        } catch (BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("business exception", e);
        }
    }
    
    /**
     *
     * @param policyManager
     * @param viewId
     * @param context
     */
    protected void initContext(final IPolicyManager policyManager, final String viewId, final FacesContext context) {
        if (policyManager != null) {
            if (policyManager.getCurrentUser()== null) {
                policyManager.setCurrentUser(policyManager.getCurrentUser());
            } else if (viewId.contains(IdentificationPhaseListener.NAVIGATION_LOGIN)) {
                redirectWithRefresh(context, IdentificationPhaseListener.NAVIGATION_INDEX);
            }
        }
    }
    
    /**
     *
     * @param context
     * @param redirectPage
     */
    protected void redirectWithRefresh(final FacesContext context, String redirectPage) {
        context.getApplication().getNavigationHandler().handleNavigation(context, null, redirectPage);
        try {
            ((javax.servlet.http.HttpServletResponse) context.getExternalContext().getResponse()).sendRedirect(context.getViewRoot().getViewId().replaceAll("(.*)xhtml$", ".$1jsf"));
        } catch (IOException e) {
            UncatchedExceptionLogger.logUncatchedException("io exception", e);
        }
    }
}
