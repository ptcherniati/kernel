package org.inra.ecoinfo.identification.jsf;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.ValidationException;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.popup.UIBeanPopup;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UIBeanChangeProfile.
 */
@ManagedBean(name = "uiChangeProfile")
@ViewScoped
public class UIBeanChangeProfile implements Serializable {

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.identification.messages";
    /**
     * The Constant BUNDLE_SOURCE_PATH_CHANGE_PROFIL.
     */
    private static final String BUNDLE_SOURCE_PATH_CHANGE_PROFIL = "org.inra.ecoinfo.identification.jsf.changeProfile";
    /**
     * The Constant PROPERTY_MSG_BAD_CURRENT_PASSWORD.
     */
    private static final String PROPERTY_MSG_BAD_CURRENT_PASSWORD = "PROPERTY_MSG_BAD_CURRENT_PASSWORD";
    /**
     * The Constant PROPERTY_MSG_BAD_PASSWORD_BIS.
     */
    private static final String PROPERTY_MSG_BAD_PASSWORD_BIS = "PROPERTY_MSG_BAD_PASSWORD_BIS";
    /**
     * The Constant PROPERTY_MSG_CHANGE_PROFIL.
     */
    private static final String PROPERTY_MSG_CHANGE_PROFIL = "PROPERTY_MSG_CHANGE_PROFIL";
    /**
     * The Constant PROPERTY_MSG_EMPTY_FIRST_PASSWORD.
     */
    private static final String PROPERTY_MSG_EMPTY_FIRST_PASSWORD = "PROPERTY_MSG_EMPTY_FIRST_PASSWORD";
    /**
     * The Constant PROPERTY_MSG_EMPTY_LOGIN.
     */
    private static final String PROPERTY_MSG_EMPTY_LOGIN = "PROPERTY_MSG_EMPTY_LOGIN";
    /**
     * The Constant PROPERTY_MSG_EMPTY_PASSWORD.
     */
    private static final String PROPERTY_MSG_EMPTY_PASSWORD = "PROPERTY_MSG_EMPTY_PASSWORD";
    /**
     * The Constant PROPERTY_MSG_EXISTING_LOGIN.
     */
    private static final String PROPERTY_MSG_EXISTING_LOGIN = "PROPERTY_MSG_EXISTING_LOGIN";
    /**
     * The Constant PROPERTY_MSG_NEW_PROFIL.
     */
    private static final String PROPERTY_MSG_NEW_PROFIL = "PROPERTY_MSG_CHANGE_NEW_PROFIL";
    /**
     * The Constant PROPERTY_MSG_NO_MORE_ROOT.
     */
    private static final String PROPERTY_MSG_NO_MORE_ROOT = "PROPERTY_MSG_NO_MORE_ROOT";
    /**
     * The Constant PROPERTY_MSG_NOTHING_TO_DO.
     */
    private static final String PROPERTY_MSG_NOTHING_TO_DO = "PROPERTY_MSG_NOTHING_TO_DO";
    /**
     * The Constant PROPERTY_MSG_OTHER_PROFIL.
     */
    private static final String PROPERTY_MSG_OTHER_PROFIL = "PROPERTY_MSG_CHANGE_OTHER_PROFIL";
    /**
     * The Constant PROPERTY_MSG_PROFILE_CHANGED.
     */
    private static final String PROPERTY_MSG_PROFILE_CHANGED = "PROPERTY_MSG_PROFILE_CHANGED";
    /**
     * The Constant PROPERTY_MSG_UNVALID_FORM.
     */
    private static final String PROPERTY_MSG_UNVALID_FORM = "PROPERTY_MSG_UNVALID_FORM";
    /**
     * The Constant PROPERTY_MSG_USER_ADDED.
     */
    private static final String PROPERTY_MSG_USER_ADDED = "PROPERTY_MSG_USER_ADDED";
    /**
     * The Constant PROPERTY_MSG_USER_NOT_ADDED.
     */
    static final String PROPERTY_MSG_USER_NOT_ADDED = "PROPERTY_MSG_USER_NOT_ADDED";
    /**
     * The Constant PROPERTY_MSG_USER_NOT_CHANGED.
     */
    private static final String PROPERTY_MSG_USER_NOT_CHANGED = "PROPERTY_MSG_USER_NOT_CHANGED";
    /**
     * The Constant PARAM_MSG_TITLE_CHANGE_USER.
     */
    private static final String PARAM_MSG_TITLE_CHANGE_USER = "PARAM_MSG_TITLE_CHANGE_USER";
    /**
     * The Constant PARAM_MSG_TITLE_NEW_USER.
     */
    private static final String PARAM_MSG_TITLE_NEW_USER = "PARAM_MSG_TITLE_NEW_USER";
    /**
     * The Constant PARAM_MSG_TITLE_CHANGE_USER_X.
     */
    private static final String PARAM_MSG_TITLE_CHANGE_USER_X = "PARAM_MSG_TITLE_CHANGE_USER_X";
    /**
     * The Constant RULE_NAVIGATION_CHANGE_PROFILE_JSF.
     */
    private static final String RULE_NAVIGATION_CHANGE_PROFILE_JSF = "changeProfile";
    /**
     * The Constant RULE_NAVIGATION_CHANGE_PROFILES_JSF.
     */
    private static final String RULE_NAVIGATION_CHANGE_PROFILES_JSF = "changeProfiles";
    private static final String PROPERTY_MSG_BAD_EMAIL = "PROPERTY_MSG_BAD_EMAIL";
    private static final String PROPERTY_MSG_EXISTING_EMAIL = "PROPERTY_MSG_EXISTING_EMAIL";
    private static final String PROFILE_PATH = "changeProfilesPanelForm:changeProfilePanelInclude:changeProfilePanelForm";
    private static final String PROFILES_PATH = "changeProfilesPanelForm:changeProfilePanelInclude:supAdmin";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    private static Logger LOGGER = LoggerFactory.getLogger(UIBeanChangeProfile.class);
    /**
     * The Constant STRING_SPACE.
     */
    private static final String STRING_SPACE = " ";
    private static final String PROPERTY_MSG_VALIDATE = "PROPERTY_MSG_VALIDATE";
    private static final String PROPERTY_MSG_VALIDATE_MAIL_SENT = "PROPERTY_MSG_VALIDATE_MAIL_SENT";
    /**
     * The Constant PATTERN_END_MAIL.
     */
    static final String PATTERN_END_MAIL = "/[^/]*$";
    /**
     * The Constant STRING_EMPTY.
     */
    static final String STRING_EMPTY = "";
    /**
     * The Utilisateur current utilisateur.
     */
    private Utilisateur currentUtilisateur;
    /**
     * The String nom.
     */
    private String nom;
    /**
     * The String prenom.
     */
    private String prenom;
    /**
     * The String email.
     */
    private String email;
    /**
     * The String login.
     */
    private String login;
    /**
     * The String current password.
     */
    private String currentPassword = UIBeanChangeProfile.STRING_EMPTY;
    /**
     * The String password.
     */
    private String password = UIBeanChangeProfile.STRING_EMPTY;
    /**
     * The String password bis.
     */
    private String passwordBis = UIBeanChangeProfile.STRING_EMPTY;
    /**
     * The Boolean root.
     */
    private Boolean root = false;
    /**
     * The String filter nom utilisateur.
     */
    private String filterNomUtilisateur;
    /**
     * The boolean is modified.
     */
    private boolean isModified = false;
    /**
     * The boolean is valid.
     */
    private boolean isValid = false;
    /**
     * The String save message.
     */
    private String saveMessage = UIBeanChangeProfile.STRING_EMPTY;
    /**
     * The boolean selected.
     */
    private boolean selected = false;
    /**
     * The HtmlPanelGroup ui form.
     */
    private List<Utilisateur> utilisateurs;
    /**
     * The ILocalizationManager localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;
    /**
     * The IPolicyManager security context.
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;
    /**
     * The IUsersManager users manager.
     */
    @ManagedProperty(value = "#{usersManager}")
    protected IUsersManager usersManager;

    /**
     * Instantiates a new UIBeanChangeProfile uI bean change profile.
     */
    public UIBeanChangeProfile() {
    }
    
    /**
     *
     * @param utilisateur
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public void deleteUser(Utilisateur utilisateur) throws BusinessException{
        usersManager.deleteUserProfile(utilisateur);
        updateUtilisateurs();
    }

    /**
     *
     * @param event
     */
    public void validatePasswords(ComponentSystemEvent event) {
        UIComponent components = event.getComponent();
        if (isNouveau()) {
            testPasswords(components);
        } else if (isMe() && !Strings.isNullOrEmpty(Field.PASSWORD.getValue())) {
            testCurrentPassword(components);
            testPasswords(components);
        }
    }

    private boolean testCurrentPassword(UIComponent components) {
        UIInput currentPasswordComponent = (UIInput) components.findComponent(Field.CURRENT_PASSWORD.fieldId);
        String currentPasswordValue = Optional.ofNullable(currentPasswordComponent)
                .map(c -> (String) c.getValue())
                .map(p -> trim(p))
                .orElse("");
        if (Strings.isNullOrEmpty(currentPasswordValue)) {
            currentPasswordComponent.setValid(false);
            FacesContext.getCurrentInstance().validationFailed();
            LOGGER.debug("bad password");
            FacesContext.getCurrentInstance().addMessage(currentPasswordComponent.getClientId(), new FacesMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_EMPTY_PASSWORD)));
            return false;
        }
        try {
            if (usersManager.checkPassword(getCurrentUtilisateur().getLogin(), currentPasswordValue).getId().equals(getCurrentUtilisateur().getId())) {
                currentPasswordComponent.setValid(true);
                return true;
            } else {
                FacesContext.getCurrentInstance().validationFailed();
                throw new BusinessException("can't validate");
            }
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.log("unvalid password", e);
            currentPasswordComponent.setValid(false);
            FacesContext.getCurrentInstance().validationFailed();
            LOGGER.debug("bad password");
            FacesContext.getCurrentInstance().addMessage(currentPasswordComponent.getClientId(), new FacesMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_BAD_CURRENT_PASSWORD)));
        }
        return false;
    }

    private boolean testPasswords(UIComponent components) {
        UIInput passwordComponent = (UIInput) components.findComponent(Field.PASSWORD.fieldId);
        UIInput passwordBisComponent = (UIInput) components.findComponent(Field.PASSWORD_BIS.fieldId);
        String passwordValue = Optional.ofNullable(passwordComponent)
                .map(c -> (String) c.getValue())
                .map(p -> trim(p))
                .orElse("");
        String passwordBisValue = Optional.ofNullable(passwordBisComponent)
                .map(c -> (String) c.getValue())
                .map(p -> trim(p))
                .orElse("");
        if (Strings.isNullOrEmpty(passwordValue)) {
            passwordComponent.setValid(false);
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage(passwordComponent.getClientId(), new FacesMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_EMPTY_FIRST_PASSWORD)));
            return false;
        } else if (Strings.isNullOrEmpty(passwordValue)) {
            passwordBisComponent.setValid(false);
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage(passwordBisComponent.getClientId(), new FacesMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_EMPTY_FIRST_PASSWORD)));
            return false;
        }
        boolean isValid = passwordBisValue.equals(passwordValue);
        passwordBisComponent.setValid(isValid);
        if (!isValid) {
            FacesContext.getCurrentInstance().addMessage(passwordBisComponent.getClientId(), new FacesMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_BAD_PASSWORD_BIS)));
            FacesContext.getCurrentInstance().validationFailed();
        }
        return isValid;
    }

    /**
     * Gets the Utilisateur current utilisateur.
     *
     * @return the Utilisateur current utilisateur
     */
    public Utilisateur getCurrentUtilisateur() {
        return currentUtilisateur;
    }

    /**
     * Sets the void current utilisateur.
     *
     * @param currentUtilisateur the Utilisateur current utilisateur
     */
    public void setCurrentUtilisateur(final Utilisateur currentUtilisateur) {
        this.currentUtilisateur = currentUtilisateur;
        if (currentUtilisateur != null) {
            selected = true;
        }
        reset();
    }
    


    /**
     * Gets the String filter nom utilisateur.
     *
     * @return the String filter nom utilisateur
     */
    public String getFilterNomUtilisateur() {
        return filterNomUtilisateur;
    }

    /**
     * Sets the void filter nom utilisateur.
     *
     * @param filterNomUtilisateur the String filter nom utilisateur
     */
    public void setFilterNomUtilisateur(final String filterNomUtilisateur) {
        this.filterNomUtilisateur = filterNomUtilisateur;
    }

    /**
     * Gets the String save message.
     *
     * @return the String save message
     */
    public String getSaveMessage() {
        return saveMessage;
    }

    /**
     * Sets the void save message.
     *
     * @param saveMessage the String save message
     */
    public void setSaveMessage(final String saveMessage) {
        this.saveMessage = saveMessage;
    }

    /**
     * Gets the String title.
     *
     * @return the String title
     */
    public String getTitle() {
        if (getCurrentUtilisateur() == null) {
            return UIBeanChangeProfile.STRING_EMPTY;
        }
        if (getCurrentUtilisateur().getId() == null) {
            return localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_NEW_PROFIL);
        } else {
            return getCurrentUtilisateur().getId().equals(policyManager.getCurrentUser().getId()) ? localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_CHANGE_PROFIL) : localizationManager
                    .getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_OTHER_PROFIL).concat(getCurrentUtilisateur().getPrenom().concat(UIBeanChangeProfile.STRING_SPACE).concat(getCurrentUtilisateur().getNom()));
        }
    }

    /**
     * Gets the String title change user.
     *
     * @return the String title change user
     */
    public String getTitleChangeUser() {
        if (currentUtilisateur == null || isMe()) {
            return localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH_CHANGE_PROFIL, UIBeanChangeProfile.PARAM_MSG_TITLE_CHANGE_USER);
        } else if (isNouveau()) {
            return localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH_CHANGE_PROFIL, UIBeanChangeProfile.PARAM_MSG_TITLE_NEW_USER);
        } else {
            return String.format(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH_CHANGE_PROFIL, UIBeanChangeProfile.PARAM_MSG_TITLE_CHANGE_USER_X), currentUtilisateur.getNom());
        }
    }

    /**
     *
     * @return
     */
    public String getMailAdmin() {
        return usersManager.getMailAdmin();
    }

    /**
     * Gets the List<Utilisateur> utilisateurs.
     *
     * @return the List<Utilisateur> utilisateurs
     * @throws BusinessException the business exception
     */
    public List<Utilisateur> getUtilisateurs() throws BusinessException {
        if (utilisateurs == null) {
            updateUtilisateurs();
        }
        return utilisateurs;
    }

    /**
     * Checks if is me.
     *
     * @return true, if is me
     */
    public boolean isMe() {
        return !isNouveau() && currentUtilisateur.getId().equals(policyManager.getCurrentUser().getId());
    }

    /**
     * Checks if is me root.
     *
     * @return true, if is me root
     */
    public boolean isMeRoot() {
        return policyManager.isRoot();
    }

    /**
     * Checks if is modified.
     *
     * @return true, if is modified
     */
    public boolean isModified() {
        return isModified;
    }

    /**
     * Sets the void modified.
     *
     * @param isModified the boolean is modified
     */
    public void setModified(final boolean isModified) {
        this.isModified = isModified;
    }

    /**
     * Checks if is nouveau.
     *
     * @return true, if is nouveau
     */
    public boolean isNouveau() {
        return currentUtilisateur instanceof NewUtilisateur;
    }

    /**
     * Checks if is selected.
     *
     * @return true, if is selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Navigate.
     *
     * @return the String string
     */
    public String navigate() {
        return UIBeanChangeProfile.RULE_NAVIGATION_CHANGE_PROFILE_JSF;
    }

    /**
     * Navigate all.
     *
     * @return the String string
     */
    public String navigateAll() {
        return UIBeanChangeProfile.RULE_NAVIGATION_CHANGE_PROFILES_JSF;
    }

    /**
     * New user.
     *
     * @return the String string
     */
    public String newUser() {
        currentUtilisateur = new NewUtilisateur();
        reset();
        selected = true;
        return null;
    }

    /**
     * Reset.
     *
     * @return the String string
     */
    public String reset() {
        setNom(getCurrentUtilisateur().getNom());
        setPrenom(getCurrentUtilisateur().getPrenom());
        setEmail(getCurrentUtilisateur().getEmail());
        setEmail(getCurrentUtilisateur().getEmail());
        setLogin(getCurrentUtilisateur().getLogin());
        setRoot(getCurrentUtilisateur().getIsRoot());

        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String updateWithConfirm() throws BusinessException {
        update(true);
        String message = String.format(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_VALIDATE), email);
        setSaveMessage(message);
        UIBeanPopup.complete(message);
        return null;

    }

    /**
     *
     * @return @throws BusinessException
     */
    public String updateWithEMail() throws BusinessException {
        assert isNouveau() : "this function create a new user and send a validate email";
        if (!isFormValid()) {
            String message = String.format(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_UNVALID_FORM), email, policyManager.getCurrentUser().getEmail());
            setSaveMessage(message);
            UIBeanPopup.complete(message);
            return null;
        }
        isModified = false;
        List<Utilisateur> otherRecipients = new LinkedList<>();
        otherRecipients.add((Utilisateur) policyManager.getCurrentUser());
        saveNewUser(otherRecipients, false);
        String message = String.format(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_VALIDATE_MAIL_SENT), getLogin(), getEmail(), policyManager.getCurrentUser().getEmail());
        setSaveMessage(message);
        UIBeanPopup.complete(message);
        return null;

    }

    /**
     * Update.
     *
     * @return the String string
     * @throws BusinessException the business exception
     */
    public String update() throws BusinessException {
        return update(false);
    }

    /**
     *
     * @param isNewWithValidation
     * @return
     * @throws BusinessException
     */
    public String update(Boolean isNewWithValidation) throws BusinessException {
        if (!isFormValid()) {
            setSaveMessage(
                    isChanged()
                            ? localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_UNVALID_FORM)
                            : localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_NOTHING_TO_DO)
            );
            UIBeanPopup.complete(getSaveMessage());
            return null;
        }
        if (testFields(isNewWithValidation)) {
            UIBeanPopup.complete(getSaveMessage());
            return null;
        }
        updateOrSaveUser();
        UIBeanPopup.complete(getSaveMessage());
        return null;
    }

    /**
     * Validate is root.
     *
     * @param context the FacesContext context
     * @param component the UIComponent component
     * @param value the Object value
     * @throws BusinessException the business exception
     */
    public void validateIsRoot(final FacesContext context, final UIComponent component, Object value) throws BusinessException {
        String localValue = trim(value);
        UIInput uiSupAdmin = (UIInput) component;
        boolean getSupAdmin = Boolean.valueOf(localValue) || getUtilisateurs().stream()
                .filter(utilisateur -> !utilisateur.equals(currentUtilisateur) && utilisateur.getIsRoot())
                .findAny()
                .isPresent();
        if (getSupAdmin) {
            uiSupAdmin.setValid(true);
            return;
        }
        uiSupAdmin.setValid(false);
        FacesContext.getCurrentInstance().validationFailed();
        LOGGER.debug("bad isroot");
        context.addMessage(uiSupAdmin.getClientId(context), new FacesMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_NO_MORE_ROOT)));
    }

    /**
     * Validate login.
     *
     * @param context the FacesContext context
     * @param component the UIComponent component
     * @param value the Object value
     * @throws ValidationException the validation exception
     */
    public void validateLogin(final FacesContext context, final UIComponent component, Object value) throws ValidationException {
        String localValue = trim(value);
        if (getCurrentUtilisateur().getLogin() != null && getCurrentUtilisateur().getLogin().equals(localValue)) {
            ((EditableValueHolder) component).setValid(true);
        } else if (localValue == null || UIBeanChangeProfile.STRING_EMPTY.equals(localValue)) {
            ((EditableValueHolder) component).setValid(false);
            FacesContext.getCurrentInstance().validationFailed();
            LOGGER.debug("bad login");
            context.addMessage(component.getClientId(context), new FacesMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_EMPTY_LOGIN)));
        } else {
            String message = localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_EXISTING_LOGIN);
            tryGetUserLogin(localValue, component, context, message);
        }

    }

    /**
     *
     * @param context
     * @param component
     * @param value
     * @throws ValidationException
     */
    public void validateEmail(final FacesContext context, final UIComponent component, Object value) throws ValidationException {
        String localValue = trim(value);
        String message;
        if (localValue == null || UIBeanChangeProfile.STRING_EMPTY.equals(localValue)) {
            ((EditableValueHolder) component).setValid(false);
            FacesContext.getCurrentInstance().validationFailed();
            LOGGER.debug("bad email");
            message = localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_BAD_EMAIL);
            context.addMessage(component.getClientId(context), new FacesMessage(message));
            return;
        }
        Utilisateur utilisateurByEmail = usersManager.getUtilisateurByEmail(localValue);
        if (utilisateurByEmail != null && !utilisateurByEmail.getEmail().equals(getCurrentUtilisateur().getEmail())) {
            message = String.format(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_EXISTING_EMAIL), localValue);
            ((EditableValueHolder) component).setValid(false);
            FacesContext.getCurrentInstance().validationFailed();
            LOGGER.debug("bad email");
            context.addMessage(component.getClientId(context), new FacesMessage(message));
        } else {
            ((EditableValueHolder) component).setValid(true);
        }
    }

    /**
     * Checks if is changed.
     *
     * @return true, if is changed
     */
    private boolean isChanged() {
        if (isNouveau()) {
            isModified = true;
            return isModified;
        }
        isModified = !getCurrentUtilisateur().getNom().equals(nom);
        isModified = isModified || !getCurrentUtilisateur().getPrenom().equals(prenom);
        isModified = isModified || !getCurrentUtilisateur().getEmail().equals(email);
        isModified = isModified || !getCurrentUtilisateur().getLogin().equals(login);
        isModified = isModified || !getCurrentUtilisateur().getIsRoot().equals(root);
        isModified = isModified || !UIBeanChangeProfile.STRING_EMPTY.equals(password);
        return isModified;
    }

    /**
     * Checks if is form valid.
     *
     * @return the Boolean boolean
     * @throws BusinessException the business exception
     */
    private Boolean isFormValid() throws BusinessException {
        isValid = isChanged() && Field.NOM.getComponent().isValid();
        isValid = isValid && Field.PRENOM.getComponent().isValid();
        isValid = isValid && Field.EMAIL.getComponent().isValid();
        isValid = isValid && Field.LOGIN.getComponent().isValid();
        isValid = isValid && Field.CURRENT_PASSWORD.getComponent().isValid();
        isValid = isValid && Field.PASSWORD.getComponent().isValid();
        isValid = isValid && Field.PASSWORD_BIS.getComponent().isValid();
        isValid = isValid && Field.SUP_ADMIN.getComponent().isValid();
        return isValid;
    }

    /*
        update or save the user
     */
    private boolean updateOrSaveUser() {
        if (isModified) {
            try {
                usersManager.updateUserProfile(getCurrentUtilisateur());
                updateUtilisateurs();
                selected = false;
            } catch (final BusinessException e) {
                UncatchedExceptionLogger.log("unvalid form", e);
                setSaveMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_USER_NOT_CHANGED));
                return true;
            }
            setSaveMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_PROFILE_CHANGED));
        } else {
            setSaveMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_NOTHING_TO_DO));
        }
        isModified = false;
        isValid = false;
        return false;
    }

    /**
     * Test fields on update searchin changes
     *
     * @param isNewWithValidation
     * @return
     */
    private boolean testFields(Boolean isNewWithValidation) {
        if (isNouveau()) {
            saveNewUser(null, isNewWithValidation);
            return true;
        }
        if (!getCurrentUtilisateur().getNom().equals(nom)) {
            getCurrentUtilisateur().setNom(nom);
        }
        if (!getCurrentUtilisateur().getPrenom().equals(prenom)) {
            getCurrentUtilisateur().setPrenom(prenom);
        }
        if (!getCurrentUtilisateur().getLogin().equals(login)) {
            getCurrentUtilisateur().setLogin(login);
        }
        if (!getCurrentUtilisateur().getEmail().equals(email)) {
            getCurrentUtilisateur().setEmail(email);
        }
        if (!root.equals(getCurrentUtilisateur().getIsRoot())) {
            getCurrentUtilisateur().setIsRoot(root);
        }
        if (!Strings.isNullOrEmpty(password)) {
            getCurrentUtilisateur().setPassword(password);
        }
        return false;
    }

    private void saveNewUser(List<Utilisateur> otherRecipients, Boolean isNewAndConformed) {
        final HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        final String baseUrl = request.getRequestURL().toString().replaceAll(UIBeanChangeProfile.PATTERN_END_MAIL, UIBeanChangeProfile.STRING_EMPTY);
        try {
            final Utilisateur utilisateur = new Utilisateur();
            utilisateur.setNom(nom);
            utilisateur.setPrenom(prenom);
            utilisateur.setEmail(email);
            utilisateur.setLogin(login);
            utilisateur.setPassword(password);
            utilisateur.setIsRoot(root);
            utilisateur.setActive(isNewAndConformed);
            usersManager.addUserProfile(utilisateur, baseUrl, otherRecipients);
            setSaveMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_USER_ADDED));
            updateUtilisateurs();
            setCurrentUtilisateur(utilisateur);
            selected = true;
            utilisateurs=null;
        } catch (BusinessException e) {
            UncatchedExceptionLogger.log("invalid form", e);
            setSaveMessage(localizationManager.getMessage(UIBeanChangeProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_USER_NOT_ADDED));
        }
    }

    /**
     * Update utilisateurs.
     *
     * @throws BusinessException the business exception
     */
    private void updateUtilisateurs() throws BusinessException {
        utilisateurs = usersManager.retrieveAllUsers();
    }

    private void tryGetUserLogin(String localValue, final UIComponent component, final FacesContext context, String message) {
        try {
            boolean existLogin = Arrays.stream(WhichTree.values())
                    .anyMatch(
                            t -> policyManager.getGroupByGroupNameAndWhichTree(localValue, t).isPresent()
                    );
            if (existLogin) {
                ((EditableValueHolder) component).setValid(false);
                FacesContext.getCurrentInstance().validationFailed();
                LOGGER.debug("bad login");
                context.addMessage(component.getClientId(context), new FacesMessage(message));
                throw new BusinessException(message);
            } else {
                ((EditableValueHolder) component).setValid(true);
            }
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.log("unvalid component", e);
            ((EditableValueHolder) component).setValid(false);
            FacesContext.getCurrentInstance().validationFailed();
            LOGGER.debug("bad login");
            context.addMessage(component.getClientId(context), new FacesMessage(message));
        }
    }

    private String trim(Object chaine) {
        return chaine == null ? "" : chaine.toString().trim();
    }

    /**
     * Gets the String nom.
     *
     * @return the String nom
     */
    public String getNom() {
        if (nom == null) {
            nom = getCurrentUtilisateur().getNom();
        }
        return nom;
    }

    /**
     * Sets the void nom.
     *
     * @param nom the String nom
     */
    public void setNom(final String nom) {
        this.nom = trim(nom);
    }

    /**
     * Gets the String prenom.
     *
     * @return the String prenom
     */
    public String getPrenom() {
        if (prenom == null) {
            prenom = getCurrentUtilisateur().getPrenom();
        }
        return prenom;
    }

    /**
     * Sets the void prenom.
     *
     * @param prenom the String prenom
     */
    public void setPrenom(final String prenom) {
        this.prenom = trim(prenom);
    }

    /**
     * Gets the String email.
     *
     * @return the String email
     */
    public String getEmail() {
        if (email == null) {
            email = getCurrentUtilisateur().getEmail();
        }
        return email;
    }

    /**
     * Sets the void email.
     *
     * @param email the String email
     */
    public void setEmail(final String email) {
        this.email = trim(email);
    }

    /**
     * Gets the String login.
     *
     * @return the String login
     */
    public String getLogin() {
        if (login == null) {
            login = getCurrentUtilisateur().getLogin();
        }
        return login;
    }

    /**
     * Sets the void login.
     *
     * @param login the String login
     */
    public void setLogin(final String login) {
        this.login = trim(login);
    }

    /**
     * Gets the String current password.
     *
     * @return the String current password
     */
    public String getCurrentPassword() {
        return currentPassword;
    }

    /**
     * Sets the void current password.
     *
     * @param currentPassword the String current password
     */
    public void setCurrentPassword(final String currentPassword) {
        this.currentPassword = trim(currentPassword);
    }

    /**
     * Gets the String password.
     *
     * @return the String password
     */
    public String getPassword() {
        if (password == null) {
            password = getCurrentUtilisateur().getPassword();
        }
        return password;
    }

    /**
     * Sets the void password.
     *
     * @param newPassword the String new password
     */
    public void setPassword(final String newPassword) {
        this.password = trim(newPassword);
    }

    /**
     * Gets the String password bis.
     *
     * @return the String password bis
     */
    public String getPasswordBis() {
        return passwordBis;
    }

    /**
     * Sets the void password bis.
     *
     * @param reNewPassword the String re new password
     */
    public void setPasswordBis(final String reNewPassword) {
        this.passwordBis = trim(reNewPassword);
    }

    /**
     * Gets the Boolean root.
     *
     * @return the Boolean root
     */
    public Boolean getRoot() {
        if (root == null) {
            root = getCurrentUtilisateur().getIsRoot();
        }
        return root;
    }

    /**
     * Sets the void root.
     *
     * @param root the Boolean root
     */
    public void setRoot(final Boolean root) {
        this.root = root;
    }

    /**
     * Gets the IUsersManager users manager.
     *
     * @return the IUsersManager users manager
     */
    public IUsersManager getUsersManager() {
        return usersManager;
    }

    /**
     * Sets the void users manager.
     *
     * @param usersManager the IUsersManager users manager
     */
    public void setUsersManager(final IUsersManager usersManager) {
        this.usersManager = usersManager;
    }

    /**
     * Sets the void localization manager.
     *
     * @param localizationManager the ILocalizationManager localization manager
     */
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the void security context.
     *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
        Optional.ofNullable((Utilisateur) policyManager.getCurrentUser()).
                ifPresent(utilisateur -> setCurrentUtilisateur(utilisateur));
    }


    static class NewUtilisateur extends Utilisateur {

    }

    /**
     *
     */
    public enum Field {
        /**
         *
         */
        NOM("nom"), /**
         *
         */
        PRENOM("prenom"), /**
         *
         */
        EMAIL("email"), /**
         *
         */
        LOGIN("login"), /**
         *
         */
        CURRENT_PASSWORD("currentPassword"), /**
         *
         */
        PASSWORD("password"), /**
         *
         */
        PASSWORD_BIS("passwordBis"), /**
         *
         */
        SUP_ADMIN("supAdmin");
        private String fieldId;

        private Field(String fieldId) {
            this.fieldId = fieldId;
        }

        /**
         *
         * @param <T>
         * @return
         */
        public <T extends UIInput> T getComponent() {
            return (T) _getComponent()
                    .orElse((T) null);
        }

        /**
         *
         * @param <T>
         * @return
         */
        public <T> T getValue() {
            return _getComponent()
                    .map(e -> (T) e.getValue())
                    .orElse((T) null);
        }

        private <T extends UIInput> Optional<T> _getComponent() {
            UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
            Boolean isProfiles = root.findComponent(PROFILES_PATH) != null;
            return Optional.ofNullable((T) root.findComponent(String.format("%s:%s", isProfiles ? PROFILES_PATH : PROFILE_PATH, fieldId)));
        }
    }
}
