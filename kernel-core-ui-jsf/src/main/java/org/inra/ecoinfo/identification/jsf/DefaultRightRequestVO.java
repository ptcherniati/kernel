/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.identification.jsf;

import java.time.LocalDateTime;
import org.inra.ecoinfo.identification.entity.RightRequest;
import org.inra.ecoinfo.identification.entity.Utilisateur;

/**
 *
 * @author ptcherniati
 */
class DefaultRightRequestVO extends AbstractRightRequestVO<DefaultRightRequestVO, RightRequest> {
    private static DefaultRightRequestVO _getInstance() {
        return new DefaultRightRequestVO();
    }

    RightRequest rightRequest;

    public DefaultRightRequestVO(RightRequest dbRequest) {
       this.rightRequest = dbRequest;
    }

    public DefaultRightRequestVO() {
    }

    @Override
    public void setUtilisateur(Utilisateur utilisateur) {
        rightRequest.setUser(utilisateur);
    }

    @Override
    public Utilisateur getUtilisateur() {
        return rightRequest.getUser();
    }

    @Override
    public DefaultRightRequestVO getInstance(RightRequest req) {
        final DefaultRightRequestVO instance = _getInstance();
        instance.setRightRequest(req);
        return instance;
    }

    @Override
    public DefaultRightRequestVO getInstance() {
        final DefaultRightRequestVO instance = _getInstance();
        instance.setRightRequest(new RightRequest());
        return instance;
    }


    @Override
    public RightRequest getRightRequest() {
        return rightRequest;
    }

    public void setRightRequest(RightRequest rightRequest) {
        this.rightRequest = rightRequest;
    }

    @Override
    public LocalDateTime getCreateDate() {
       return  this.rightRequest.getCreateDate();
    }

    @Override
    public boolean isValidated() {
        return this.rightRequest.isValidated();
    }

    @Override
    public void setValidated(boolean validated) {
        this.rightRequest.setValidated(validated);
    }

}
