package org.inra.ecoinfo.identification.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UIBeanChangePasswordLost.
 */
@ManagedBean(name = "uiChangePasswordLost")
@ViewScoped
public class UIBeanChangePasswordLost implements Serializable {

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.identification.messages";
    /**
     * The Constant COMPONENT_EMAIL.
     */
    private static final String COMPONENT_EMAIL = "email";
    /**
     * The Constant COMPONENT_PASSWORD.
     */
    private static final String COMPONENT_PASSWORD = "password";
    /**
     * The Constant COMPONENT_PASSWORD_BIS.
     */
    private static final String COMPONENT_PASSWORD_BIS = "passwordBis";
    /**
     * The Constant NAVIGATION_INDEX_HTML.
     */
    private static final String NAVIGATION_INDEX_HTML = "index.jsf";
    /**
     * The Constant NAVIGATION_RETRIEVE_PASSWORD.
     */
    private static final String NAVIGATION_RETRIEVE_PASSWORD = "NAVIGATION_RETRIEVE_PASSWORD";
    /**
     * The Constant NAVIGATION_SUCCESS.
     */
    private static final String NAVIGATION_SUCCESS = "home";
    /**
     * The Constant PARAMETER_MAIL.
     */
    private static final String PARAMETER_MAIL = "mail";
    /**
     * The Constant PARAMETER_PASS.
     */
    private static final String PARAMETER_PASS = "pass";
    /**
     * The Constant PATTERN_END_MAIL.
     */
    private static final String PATTERN_END_MAIL = "/[^/]*$";
    /**
     * The Constant PROPERTY_MSG_BAD_PASSWORD_BIS.
     */
    private static final String PROPERTY_MSG_BAD_PASSWORD_BIS = "PROPERTY_MSG_BAD_PASSWORD_BIS";
    /**
     * The Constant PROPERTY_MSG_CHANGE_PROFIL.
     */
    private static final String PROPERTY_MSG_CHANGE_PROFIL = "PROPERTY_MSG_CHANGE_PROFIL";
    /**
     * The Constant PROPERTY_MSG_CONNECTION_SUCCESS.
     */
    private static final String PROPERTY_MSG_CONNECTION_SUCCESS = "PROPERTY_MSG_CONNECTION_SUCCESS";
    /**
     * The Constant PROPERTY_MSG_NEW_PROFIL.
     */
    private static final String PROPERTY_MSG_NEW_PROFIL = "PROPERTY_MSG_CHANGE_NEW_PROFIL";
    /**
     * The Constant PROPERTY_MSG_NOTHING_TO_DO.
     */
    private static final String PROPERTY_MSG_NOTHING_TO_DO = "PROPERTY_MSG_NOTHING_TO_DO";
    /**
     * The Constant PROPERTY_MSG_OTHER_PROFIL.
     */
    private static final String PROPERTY_MSG_OTHER_PROFIL = "PROPERTY_MSG_CHANGE_OTHER_PROFIL";
    /**
     * The Constant PROPERTY_MSG_PROFILE_CHANGED.
     */
    private static final String PROPERTY_MSG_PROFILE_CHANGED = "PROPERTY_MSG_PROFILE_CHANGED";
    /**
     * The Constant PROPERTY_MSG_UNKNOWN_EMAIL.
     */
    private static final String PROPERTY_MSG_UNKNOWN_EMAIL = "PROPERTY_MSG_UNKNOWN_EMAIL";
    /**
     * The Constant PROPERTY_MSG_USER_NOT_CHANGED.
     */
    private static final String PROPERTY_MSG_USER_NOT_CHANGED = "PROPERTY_MSG_USER_NOT_CHANGED";
    /**
     * The Constant PARAMETER_SUBMIT.
     */
    private static final Object PARAMETER_SUBMIT = "retrievePassword";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The Constant STRING_EMPTY.
     */
    private static final String STRING_EMPTY = "";
    /**
     * The Constant STRING_SPACE.
     */
    private static final String STRING_SPACE = " ";
    /**
     * The Logger LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UIBeanChangePasswordLost.class.getClass());
    /**
     * The Utilisateur current utilisateur.
     */
    private Utilisateur currentUtilisateur;
    /**
     * The String email.
     */
    private String email = UIBeanChangePasswordLost.STRING_EMPTY;
    /**
     * The boolean is modified.
     */
    private boolean isModified = false;
    /**
     * The boolean is valid.
     */
    private boolean isValid = false;
    private String saveMessage;
    /**
     * The String password.
     */
    private String password = UIBeanChangePasswordLost.STRING_EMPTY;
    /**
     * The String password bis.
     */
    private String passwordBis = UIBeanChangePasswordLost.STRING_EMPTY;
    /**
     * The int phase.
     */
    private int phase = 1;
    /**
     * The HtmlPanelGroup ui form.
     */
    private HtmlPanelGroup uiForm;
    /**
     * The List<Utilisateur> utilisateurs.
     */
    private List<Utilisateur> utilisateurs;
    /**
     * The ILocalizationManager localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;
    /**
     * The IPolicyManager security context.
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;
    /**
     * The IUsersManager users manager.
     */
    @ManagedProperty(value = "#{usersManager}")
    protected IUsersManager usersManager;

    /**
     * Instantiates a new UIBeanChangePasswordLost uI bean change password lost.
     */
    public UIBeanChangePasswordLost() {
    }

    /**
     * Connect.
     *
     * @return the String string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public String connect() throws IOException {
        try {
            final Utilisateur utilisateur = usersManager.checkPassword(getCurrentUtilisateur().getLogin(), password);
            policyManager.setCurrentUser(utilisateur);
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in connect", e);
        }
        LOGGER.debug(String.format(localizationManager.getMessage(UIBeanChangePasswordLost.BUNDLE_SOURCE_PATH, UIBeanChangePasswordLost.PROPERTY_MSG_CONNECTION_SUCCESS), getCurrentUtilisateur().getLogin()));
        return UIBeanChangePasswordLost.NAVIGATION_SUCCESS;
    }

    /**
     * Gets the String current password.
     *
     * @return the String current password
     */
    public String getCurrentPassword() {
        return UIBeanChangePasswordLost.STRING_EMPTY;
    }

    /**
     * Gets the Utilisateur current utilisateur.
     *
     * @return the Utilisateur current utilisateur
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public Utilisateur getCurrentUtilisateur() throws IOException {
        if (currentUtilisateur != null) {
            return currentUtilisateur;
        }
        final String retrievePassword = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(UIBeanChangePasswordLost.PARAMETER_SUBMIT);
        final String mail = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(UIBeanChangePasswordLost.PARAMETER_MAIL);
        final String pass = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(UIBeanChangePasswordLost.PARAMETER_PASS);
        try {
            testRetrievePassworCase(retrievePassword, mail, pass);
        } catch (final BusinessException e) {
            LOGGER.info("business exception", e);
            return null;
        }
        if (currentUtilisateur == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect(UIBeanChangePasswordLost.NAVIGATION_INDEX_HTML);
        }
        return currentUtilisateur;
    }

    /**
     * Gets the String email.
     *
     * @return the String email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the void email.
     *
     * @param email the String email
     */
    public void setEmail(final String email) {
        this.email = trim(email);
    }

    /**
     * Gets the Boolean checks if is button valid.
     *
     * @return the Boolean checks if is button valid
     */
    public Boolean getIsButtonValid() {
        return isFormValid();
    }

    /**
     * Gets the Boolean checks if is button valid for email.
     *
     * @return the Boolean checks if is button valid for email
     */
    public Boolean getIsButtonValidForEmail() {
        return !UIBeanChangePasswordLost.STRING_EMPTY.equals(getUiEmail().getValue()) && getUiEmail().isValid();
    }

    /**
     * Gets the String password.
     *
     * @return the String password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the void password.
     *
     * @param newPassword the String new password
     */
    public void setPassword(final String newPassword) {
        this.password = trim(newPassword);
    }

    /**
     * Gets the String password bis.
     *
     * @return the String password bis
     */
    public String getPasswordBis() {
        return passwordBis;
    }

    /**
     * Sets the void password bis.
     *
     * @param reNewPassword the String re new password
     */
    public void setPasswordBis(final String reNewPassword) {
        this.passwordBis = trim(reNewPassword);
    }

    /**
     * Gets the int phase.
     *
     * @return the int phase
     */
    public int getPhase() {
        return phase;
    }

    /**
     * Sets the void phase.
     *
     * @param phase the int phase
     */
    public void setPhase(final int phase) {
        this.phase = phase;
    }

    /**
     * Gets the String title.
     *
     * @return the String title
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public String getTitle() throws IOException {
        if (getCurrentUtilisateur() == null) {
            return UIBeanChangePasswordLost.STRING_EMPTY;
        }
        if (getCurrentUtilisateur().getId() == null) {
            return localizationManager.getMessage(UIBeanChangePasswordLost.BUNDLE_SOURCE_PATH, UIBeanChangePasswordLost.PROPERTY_MSG_NEW_PROFIL);
        } else {
            return getCurrentUtilisateur().getId().equals(policyManager.getCurrentUser().getId())
                   ? localizationManager.getMessage(UIBeanChangePasswordLost.BUNDLE_SOURCE_PATH, UIBeanChangePasswordLost.PROPERTY_MSG_CHANGE_PROFIL)
                   : localizationManager.getMessage(UIBeanChangePasswordLost.BUNDLE_SOURCE_PATH, UIBeanChangePasswordLost.PROPERTY_MSG_OTHER_PROFIL).concat(
                           getCurrentUtilisateur().getPrenom().concat(UIBeanChangePasswordLost.STRING_SPACE).concat(getCurrentUtilisateur().getNom()));
        }
    }

    /**
     * Gets the HtmlInputText ui email.
     *
     * @return the HtmlInputText ui email
     */
    public HtmlInputText getUiEmail() {
        return (HtmlInputText) uiForm.findComponent(UIBeanChangePasswordLost.COMPONENT_EMAIL);
    }

    /**
     * Gets the HtmlPanelGroup ui form.
     *
     * @return the HtmlPanelGroup ui form
     */
    public HtmlPanelGroup getUiForm() {
        return uiForm;
    }

    /**
     * Sets the void ui form.
     *
     * @param uiForm the HtmlPanelGroup ui form
     */
    public void setUiForm(final HtmlPanelGroup uiForm) {
        this.uiForm = uiForm;
    }

    /**
     * Gets the HtmlInputSecret ui password.
     *
     * @return the HtmlInputSecret ui password
     */
    public HtmlInputSecret getUiPassword() {
        return (HtmlInputSecret) uiForm.findComponent(UIBeanChangePasswordLost.COMPONENT_PASSWORD);
    }

    /**
     * Gets the HtmlInputSecret ui password bis.
     *
     * @return the HtmlInputSecret ui password bis
     */
    public HtmlInputSecret getUiPasswordBis() {
        return (HtmlInputSecret) uiForm.findComponent(UIBeanChangePasswordLost.COMPONENT_PASSWORD_BIS);
    }

    /**
     * Gets the IUsersManager users manager.
     *
     * @return the IUsersManager users manager
     */
    public IUsersManager getUsersManager() {
        return usersManager;
    }

    /**
     * Sets the void users manager.
     *
     * @param usersManager the IUsersManager users manager
     */
    public void setUsersManager(final IUsersManager usersManager) {
        this.usersManager = usersManager;
    }

    /**
     * Gets the List<Utilisateur> utilisateurs.
     *
     * @return the List<Utilisateur> utilisateurs
     * @throws BusinessException the business exception
     */
    public List<Utilisateur> getUtilisateurs() throws BusinessException {
        if (utilisateurs == null) {
            updateUtilisateurs();
        }
        return utilisateurs;
    }

    /**
     * Checks if is modified.
     *
     * @return true, if is modified
     */
    public boolean isModified() {
        return isModified;
    }

    /**
     * Sets the void modified.
     *
     * @param isModified the boolean is modified
     */
    public void setModified(final boolean isModified) {
        this.isModified = isModified;
    }

    /**
     * Sets the void localization manager.
     *
     * @param localizationManager the ILocalizationManager localization manager
     */
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the void save message.
     *
     * @param saveMessage the String save message
     */
    public void setSaveMessage(final String saveMessage) {
        this.saveMessage= saveMessage;
    }

    /**
     * Sets the void security context.
     *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Update.
     *
     * @return the String string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public String update() throws IOException {
        if (!isFormValid()) {
            return null;
        }
        isModified = false;
        boolean passwordValid = !UIBeanChangePasswordLost.STRING_EMPTY.equals(password) && getUiPassword().isValid();
        passwordValid = passwordValid && !UIBeanChangePasswordLost.STRING_EMPTY.equals(passwordBis) && getUiPasswordBis().isValid();
        if (passwordValid) {
            getCurrentUtilisateur().setPassword(password);
            isModified = true;
        } else {
            getCurrentUtilisateur().setPassword(null);
        }
        if (isModified) {
            return updateWhenModified();
        } else {
            setSaveMessage(localizationManager.getMessage(UIBeanChangePasswordLost.BUNDLE_SOURCE_PATH, UIBeanChangePasswordLost.PROPERTY_MSG_NOTHING_TO_DO));
            return localizationManager.getMessage(UIBeanChangePasswordLost.BUNDLE_SOURCE_PATH, UIBeanChangePasswordLost.NAVIGATION_RETRIEVE_PASSWORD);
        }
    }

    /**
     * Update for email.
     *
     * @return the String string
     * @throws BusinessException the business exception
     */
    public String updateForEmail() throws BusinessException {
        if (!getIsButtonValidForEmail()) {
            return null;
        }
        final HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        final String baseUrl = request.getRequestURL().toString().replaceAll(UIBeanChangePasswordLost.PATTERN_END_MAIL, UIBeanChangePasswordLost.STRING_EMPTY);
        usersManager.resetPassword(currentUtilisateur, baseUrl, FacesContext.getCurrentInstance().getViewRoot().getLocale());
        setPhase(2);
        
        return null;
    }

    /**
     * Validate email.
     *
     * @param context the FacesContext context
     * @param component the UIComponent component
     * @param value the Object value
     * @throws BusinessException the business exception
     */
    public void validateEmail(final FacesContext context, final UIComponent component, Object value) throws BusinessException {
        String localValue = trim(value);
        final String mail = localValue.trim();
        Utilisateur utilisateur = usersManager.getUtilisateurByEmail(mail);
        if (utilisateur != null) {
            getUiEmail().setValid(true);
            currentUtilisateur = utilisateur;
            return;
        }
        getUiEmail().setValid(false);
        context.addMessage(getUiEmail().getClientId(context), new FacesMessage(localizationManager.getMessage(UIBeanChangePasswordLost.BUNDLE_SOURCE_PATH, UIBeanChangePasswordLost.PROPERTY_MSG_UNKNOWN_EMAIL)));
    }

    /**
     * Validate password bis.
     *
     * @param context the FacesContext context
     * @param component the UIComponent component
     * @param value the Object value
     */
    public void validatePasswordBis(final FacesContext context, final UIComponent component, Object value) {
        String localValue = trim(value);
        if (localValue == null || !localValue.equals(getUiPassword().getValue().toString())) {
            getUiPasswordBis().setValid(false);
            context.addMessage(getUiPasswordBis().getClientId(context), new FacesMessage(localizationManager.getMessage(UIBeanChangePasswordLost.BUNDLE_SOURCE_PATH, UIBeanChangePasswordLost.PROPERTY_MSG_BAD_PASSWORD_BIS)));
        } else {
            getUiPasswordBis().setValid(true);
        }
    }

    /**
     * Checks if is changed.
     *
     * @return true, if is changed
     */
    private boolean isChanged() {
        return !UIBeanChangePasswordLost.STRING_EMPTY.equals(getUiPassword().getValue()) && !UIBeanChangePasswordLost.STRING_EMPTY.equals(getUiPasswordBis().getValue());
    }

    /**
     * Checks if is form valid.
     *
     * @return the Boolean boolean
     */
    private Boolean isFormValid() {
        if (!isChanged()) {
            isValid = false;
        } else {
            validatePasswordBis(FacesContext.getCurrentInstance(), getUiPasswordBis(), getUiPasswordBis().getValue());
            isValid = getUiPasswordBis().isValid();
        }
        return isValid;
    }

    private void testRetrievePassworCase(String retrievePassword, String mail, String pass) throws BusinessException {
        if ("true".equals(retrievePassword) && mail != null && pass != null) {
            utilisateurs = getUtilisateurs();
            utilisateurs.stream().filter((utilisateur) -> (utilisateur.getEmail().equals(mail) && utilisateur.getPassword().equals(pass))).forEach((utilisateur) -> {
                currentUtilisateur = utilisateur;
            });
        }
    }

    /**
     * Update utilisateurs.
     *
     * @throws BusinessException the business exception
     */
    private void updateUtilisateurs() throws BusinessException {
        utilisateurs = usersManager.retrieveAllUsers();
    }

    private String updateWhenModified() throws IOException {
        try {
            usersManager.updateUserProfile(getCurrentUtilisateur());
            connect();
            updateUtilisateurs();
            FacesContext.getCurrentInstance().getExternalContext().redirect(UIBeanChangePasswordLost.NAVIGATION_INDEX_HTML);
            setSaveMessage(localizationManager.getMessage(UIBeanChangePasswordLost.BUNDLE_SOURCE_PATH, UIBeanChangePasswordLost.PROPERTY_MSG_PROFILE_CHANGED));
            return "home";
        } catch (final BusinessException e) {
            LOGGER.error("can't update", e);
            setSaveMessage(localizationManager.getMessage(UIBeanChangePasswordLost.BUNDLE_SOURCE_PATH, UIBeanChangePasswordLost.PROPERTY_MSG_USER_NOT_CHANGED));
            return localizationManager.getMessage(UIBeanChangePasswordLost.BUNDLE_SOURCE_PATH, UIBeanChangePasswordLost.NAVIGATION_RETRIEVE_PASSWORD);
        }
    }

    private String trim(Object chaine) {
        return chaine == null ? "" : chaine.toString().trim();
    }
}
