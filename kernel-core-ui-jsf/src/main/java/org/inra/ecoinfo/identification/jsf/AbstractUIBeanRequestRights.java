package org.inra.ecoinfo.identification.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedProperty;
import org.inra.ecoinfo.identification.IRequestManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 * @param <T>
 * @param <R>
 */
abstract public class AbstractUIBeanRequestRights<T extends AbstractRightRequestVO<T, R>, R> implements Serializable {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.identification.messages";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST @link(String).
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST = "PROPERTY_MSG_NEW_RIGHT_REQUEST";

    /**
     *
     */
    public static final String PROPERTY_MSG_ABORT = "PROPERTY_MSG_ABORT";

    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractUIBeanRequestRights.class);

    /**
     * The save message @link(String).
     */
    protected String saveMessage;

    /**
     *
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    @ManagedProperty(value = "#{requestManager}")
    protected IRequestManager<R> requestManager;

    /**
     *
     */
    protected UtilisateurVO utilisateurVO;
    /**
     * return the request when asking rights
     *
     */
    protected T currentRequest;

    /**
     *
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;
    /**
     * return the current request in managing rights
     *
     */
    protected T request = getRequestVOInstance();
    /**
     * return the list of users asking tights
     */
    protected List<UtilisateurItem> requests;

    /**
     *
     */
    protected Map<Utilisateur, UtilisateurItem> items = new HashMap();

    /**
     *
     */
    public AbstractUIBeanRequestRights() {
    }

    /**
     *
     * @param dbRequest
     * @return
     */
    protected abstract T newRequestVO(R dbRequest);

    /**
     *
     * @return
     */
    abstract protected T getRequestVOInstance();

    /**
     * init ths requests if null;
     *
     * @return the list of users asking rights
     */
    public List<UtilisateurItem> getRequests() {
        if (requests == null) {
            initRequests();
        }
        return requests;
    }

    /**
     *
     * @param requests
     */
    public void setRequests(List<UtilisateurItem> requests) {
        this.requests = requests;
    }

    /**
     *
     * @return the name of jsf page for navigate (rights demand)
     */
    public String navigate() {
        return "requestRights";
    }

    /**
     *
     * @return the name of jsf page for navigate (rights manage)
     */
    public String userNavigate() {
        return "requestUsersRights";
    }

    /**
     * init user if needed
     *
     * @return the user asking requests
     */
    public UtilisateurVO getUtilisateurVO() {
        if (utilisateurVO == null) {
            initUtilisateurVo();
        }
        return this.utilisateurVO;
    }

    /**
     *
     * @param user
     */
    public void setUtilisateurVO(Utilisateur user) {
        this.utilisateurVO = new UtilisateurVO(user);
    }

    /**
     * save the tnew request
     *
     * @return @throws BusinessException
     */
    public String updateRights() throws BusinessException {
        try {
            requestManager.addNewRequest(request.getRightRequest());
            setSaveMessage(localizationManager.getMessage(UIBeanCreateProfile.BUNDLE_SOURCE_PATH_RIGHTS, PROPERTY_MSG_NEW_RIGHT_REQUEST));
            requests = null;
            return "validateRights";
        } catch (BusinessException e) {
            LOGGER.error(localizationManager.getMessage(UIBeanCreateProfile.BUNDLE_SOURCE_PATH_RIGHTS, PROPERTY_MSG_ABORT), e);
            setSaveMessage(localizationManager.getMessage(UIBeanCreateProfile.BUNDLE_SOURCE_PATH_RIGHTS, PROPERTY_MSG_ABORT));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String getSaveMessage() {
        return saveMessage;
    }

    /**
     *
     * @param saveMessage
     */
    public void setSaveMessage(String saveMessage) {
        this.saveMessage = saveMessage;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
        this.utilisateurVO = new UtilisateurVO((Utilisateur) policyManager.getCurrentUser());
    }

    /**
     *
     * @return the current request in managing rights
     */
    public T getRequest() {
        return request;
    }

    /**
     *
     * @param request
     */
    public void setRequest(T request) {
        this.request = request;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param requestManager
     */
    public void setRequestManager(IRequestManager requestManager) {
        this.requestManager = requestManager;
    }

    /**
     *
     * @return true if there's a request selected
     */
    public boolean getHasSelection() {
        return currentRequest != null;
    }

    /**
     * for saving the request
     *
     * @param validateRequest
     * @return
     */
    public String validate(T validateRequest) {
        currentRequest = request;
        requestManager.saveRequest(validateRequest.getRightRequest());
        return null;
    }

    /**
     * delete the request
     *
     * @param item
     * @param deletedRequest
     * @return
     */
    public String delete(UtilisateurItem item, T deletedRequest) {
        requestManager.deleteRequest(deletedRequest.getRightRequest());
        final int index = requests.indexOf(item);
        requests.get(index).getRequests().remove(deletedRequest);
        if (requests.get(index).getRequests().isEmpty()) {
            requests.remove(item);
        }
        currentRequest = null;
        return null;
    }

    /**
     * set the current request to request
     *
     * @param request
     * @return
     */
    public String changeCurrentRequest(T request) {
        this.currentRequest = request;
        return null;
    }

    /**
     *
     * @return the request when asking rights
     */
    public T getCurrentRequest() {
        return currentRequest;
    }

    /**
     *
     * @param currentRequest
     */
    public void setCurrentRequest(T currentRequest) {
        this.currentRequest = currentRequest;
    }

    /**
     * @return true if the asking user is admin
     */
    public boolean isAdmin() {
        return policyManager.isRoot();
    }

    /**
     * get the user and init hte default request with this user
     */
    private void initUtilisateurVo() {
        requests = null;
        Utilisateur utilisateur = (Utilisateur) policyManager.getCurrentUser();
        this.request.setUtilisateur(utilisateur);
        if (utilisateur == null) {
            return;
        }
        setUtilisateurVO(utilisateur);
        //this.request.setNameSurname(String.format("%s %s", utilisateur.getNom(), utilisateur.getPrenom()));
        //this.request.setEmail(utilisateur.getEmail());
    }

    /**
     * get a list of request and init the list of users asking rights
     */
    private void initRequests() {

        requests = new LinkedList();
        List<R> dbRequests = requestManager.getRestrictedRequests();
        dbRequests.stream().map((dbRequest) -> newRequestVO(dbRequest)).forEach((requestVO) -> {
            Utilisateur dbUtilisateur = requestVO.getUtilisateur();
            UtilisateurItem node = getOrCreateUtilisateurItem(dbUtilisateur);
            node.add(requestVO);
        });
    }

    private UtilisateurItem getOrCreateUtilisateurItem(Utilisateur dbUtilisateur) {
        if (items.containsKey(dbUtilisateur)) {
            return items.get(dbUtilisateur);
        }
        UtilisateurItem node = new UtilisateurItem(dbUtilisateur);
        items.put(dbUtilisateur, node);
        requests.add(node);
        return node;
    }

    /**
     * an user asking rights
     */
    public class UtilisateurItem {

        UtilisateurVO _utilisateur;
        private final List<T> _requests = new ArrayList();

        private UtilisateurItem(Utilisateur dbUtilisateur) {
            _utilisateur = new UtilisateurVO(dbUtilisateur);
        }

        /**
         *
         * @return
         */
        public AbstractUIBeanRequestRights getOuter() {
            return AbstractUIBeanRequestRights.this;
        }

        /**
         *
         * @return
         */
        public String getNom() {
            return String.format("%s %s", _utilisateur.getNom(), _utilisateur.getPrenom());
        }

        /**
         *
         * @return
         */
        @Override
        public String toString() {
            return String.format("%s %s", _utilisateur.getNom(), _utilisateur.getPrenom());
        }

        /**
         *
         * @return
         */
        public UtilisateurVO getUtilisateur() {
            return _utilisateur;
        }

        /**
         *
         * @param utilisateur
         */
        public void setUtilisateur(UtilisateurVO utilisateur) {
            this._utilisateur = utilisateur;
        }

        private void add(T request) {
            _requests.add(request);
        }

        /**
         *
         * @return
         */
        public List<T> getRequests() {
            return _requests;
        }
    }
}
