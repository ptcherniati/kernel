package org.inra.ecoinfo.identification.jsf;

/**
 * The Class Versions.
 */
public class Versions {

    /**
     * The String application version.
     */
    private String applicationVersion;
    /**
     * The String core version.
     */
    private String coreVersion;

    /**
     * Instantiates a new Versions versions.
     */
    public Versions() {
        super();
    }

    /**
     * Gets the String application version.
     *
     * @return the String application version
     */
    public String getApplicationVersion() {
        return applicationVersion;
    }

    /**
     * Sets the void application version.
     *
     * @param applicationVersion the String application version
     */
    public void setApplicationVersion(final String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    /**
     * Gets the String core version.
     *
     * @return the String core version
     */
    public String getCoreVersion() {
        return coreVersion;
    }

    /**
     * Sets the void core version.
     *
     * @param coreVersion the String core version
     */
    public void setCoreVersion(final String coreVersion) {
        this.coreVersion = coreVersion;
    }
}
