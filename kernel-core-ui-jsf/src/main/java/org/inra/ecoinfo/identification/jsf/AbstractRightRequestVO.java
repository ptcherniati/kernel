/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.identification.jsf;

import java.time.LocalDateTime;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 * @param <T>
 * @param <R>
 */
public abstract class AbstractRightRequestVO<T extends AbstractRightRequestVO<T,R>, R > {

    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractRightRequestVO.class.getName());

    /**
     *
     * @param req
     * @return
     */
    public abstract T getInstance(R req);

    /**
     *
     * @return
     */
    public abstract T getInstance();

    /**
     *
     * @return
     */
    public abstract R getRightRequest();

    /**
     *
     * @param utilisateur
     */
    public abstract void setUtilisateur(Utilisateur utilisateur);

    /**
     *
     * @return
     */
    public abstract Utilisateur getUtilisateur();

    /**
     *
     * @return
     */
    public abstract LocalDateTime getCreateDate();

    /**
     *
     * @return
     */
    public abstract boolean isValidated();

    /**
     *
     * @param validated
     */
    public abstract void setValidated(boolean validated);
    
    /**
     *
     * @return
     */
    public String getNameSurname(){
        if(getUtilisateur()==null){
            return "";
        }
        return String.format("%s %s", getUtilisateur().getNom(), getUtilisateur().getPrenom());
    }
}
