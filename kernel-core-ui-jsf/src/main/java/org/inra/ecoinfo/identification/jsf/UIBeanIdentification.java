package org.inra.ecoinfo.identification.jsf;

import com.google.common.base.Strings;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.exception.BadPasswordException;
import org.inra.ecoinfo.identification.exception.InactiveUserException;
import org.inra.ecoinfo.identification.exception.UnknownUserException;
import org.inra.ecoinfo.identification.impl.DefaultUsersManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.jsf.UIBeanLocalization;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.password.Password;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class UIBeanIdentification.
 */
@ManagedBean(name = "uiIdentification")
@ViewScoped
public class UIBeanIdentification implements Serializable {

    /**
     * The Constant BUNDLE_NAME.
     */
    private static final String BUNDLE_NAME = "org.inra.ecoinfo.identification.impl.messages";
    /**
     * The Constant NAVIGATION_SUCCESS.
     */
    private static final String NAVIGATION_SUCCESS = "home";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The ResourceBundle resource bundle.
     */
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(UIBeanIdentification.BUNDLE_NAME, FacesContext.getCurrentInstance().getViewRoot().getLocale());
    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    private UIComponent identificationForm;
    /**
     * The String login.
     */
    private String login = "";
    /**
     * The String password.
     */
    private String password = "";
    /**
     * The HtmlInputText ui login.
     */
    @Transient
    private InputText uiLogin;
    /**
     * The HtmlInputSecret ui password.
     */
    @Transient
    private Password uiPassword;
    private boolean incorectIdentification = false;
    private boolean inactiveUser = false;
    private String email;
    /**
     * The IPolicyManager security context.
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;
    /**
     * The UIBeanLocalization ui localization.
     */
    @ManagedProperty(value = "#{uiLocalization}")
    protected UIBeanLocalization uiLocalization;
    /**
     * The IUsersManager users manager.
     */
    @ManagedProperty(value = "#{usersManager}")
    protected IUsersManager usersManager;

    /**
     * Instantiates a new UIBeanIdentification uI bean identification.
     */
    public UIBeanIdentification() {
        super();
    }

    /**
     * Connect.
     *
     * @return the String string
     */
    public String connect() {
        inactiveUser = incorectIdentification = false;
        email = null;
        try {
            final Utilisateur utilisateur = usersManager.checkPassword(login, password);
            policyManager.setCurrentUser(utilisateur);
            uiLocalization.initLocale();
        } catch (final UnknownUserException e) {
            LOGGER.info("unKnown user", e);
            addFaceMessage(String.format(RESOURCE_BUNDLE.getString(e.getMessage()), login), uiLogin);
            incorectIdentification = true;
            return null;
        } catch (final InactiveUserException e) {
            Utilisateur user = e.getUser();
            inactiveUser = true;
            LOGGER.info("inactive user", e);
            email = user.getEmail();
            final String message = String.format(RESOURCE_BUNDLE.getString(e.getMessage()), login);
            addFaceMessage(message, uiLogin);
//            UIBeanPopup.complete(message);
//            RequestContext.getCurrentInstance().execute("hideLoginPopUp()");
            incorectIdentification = !inactiveUser;
            return null;
        } catch (final BadPasswordException e) {
            LOGGER.info("bad password", e);
            final String message = RESOURCE_BUNDLE.getString(e.getMessage());
            addFaceMessage(message, uiPassword);
//            UIBeanPopup.complete(message);
            incorectIdentification = true;
            return null;
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in connect", e);
            incorectIdentification = true;
            return null;
        } finally {
            FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("panelLogin:identificationForm");
        }
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("./index.jsf");
        } catch (IOException ex) {
            LOGGER.error("can't redirect", ex);
        }
        return UIBeanIdentification.NAVIGATION_SUCCESS;
    }

    /**
     * Gets the String connected login.
     *
     * @return the String connected login
     */
    public String getConnectedLogin() {
        return policyManager.getCurrentUserLogin();
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        if (email != null && email.matches("[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\\.[a-zA-Z]{2,4}")) {
            this.email = email;
        }
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        if (!Strings.isNullOrEmpty(email)) {
            return email;
        }
        Utilisateur utilisateurByLogin = null;
        try {
            utilisateurByLogin = usersManager.getUtilisateurByLogin(login).orElseThrow(BusinessException::new);
        } catch (BusinessException e1) {
            LOGGER.info(e1.getMessage(), e1);
            return "";
        }
        email = utilisateurByLogin.getEmail();
        return email;
    }

    /**
     *
     * @return
     */
    public String sendValidationEmail() {
        Utilisateur utilisateurByLogin = null;
        try {
            utilisateurByLogin = usersManager.getUtilisateurByLogin(login).orElseThrow(BusinessException::new);
        } catch (BusinessException e1) {
            LOGGER.info(e1.getMessage(), e1);
            return null;
        }
        inactiveUser = false;
        final ResourceBundle resourceBundle = ResourceBundle.getBundle(DefaultUsersManager.BUNDLE_NAME, utilisateurByLogin.getLanguage() == null ? new Locale(Localization.getDefaultLocalisation()) : new Locale(utilisateurByLogin.getLanguage()));
        final HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        final String baseUrl = request.getRequestURL().toString().replaceAll(UIBeanChangeProfile.PATTERN_END_MAIL, UIBeanChangeProfile.STRING_EMPTY);
        if (email != null && email.matches("[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\\.[a-zA-Z]{2,4}")) {
            utilisateurByLogin.setEmail(email);
        }
        usersManager.sendValidationEmail(utilisateurByLogin, baseUrl, resourceBundle, new LinkedList<>());
        return null;
    }

    /**
     * Gets the Utilisateur current utilisateur.
     *
     * @return the Utilisateur current utilisateur
     */
    public Utilisateur getCurrentUtilisateur() {
        return (Utilisateur) policyManager.getCurrentUser();
    }

    /**
     * Gets the String login.
     *
     * @return the String login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the void login.
     *
     * @param login the String login
     */
    public void setLogin(final String login) {
        this.login = login == null ? null : login.trim();
    }

    /**
     * Gets the String password.
     *
     * @return the String password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the void password.
     *
     * @param password the String password
     */
    public void setPassword(final String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * Gets the HtmlInputText ui login.
     *
     * @return the HtmlInputText ui login
     */
    public HtmlInputText getUiLogin() {
        return uiLogin;
    }

    /**
     * Sets the void ui login.
     *
     * @param uiLogin the HtmlInputText ui login
     */
    public void setUiLogin(final InputText uiLogin) {
        this.uiLogin = uiLogin;
    }

    /**
     * Gets the HtmlInputSecret ui password.
     *
     * @return the HtmlInputSecret ui password
     */
    public Password getUiPassword() {
        return uiPassword;
    }

    /**
     * Sets the void ui password.
     *
     * @param uiPassword the HtmlInputSecret ui password
     */
    public void setUiPassword(final Password uiPassword) {
        this.uiPassword = uiPassword;
    }

    /**
     * Logout.
     *
     * @return the String string
     */
    @Transactional(readOnly = true)
    public String logout() {
        policyManager.releaseContext();
        ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true)).invalidate();
        return null;
    }

    /**
     * Sets the void security context.
     *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Sets the void ui localization.
     *
     * @param uiLocalization the UIBeanLocalization ui localization
     */
    public void setUiLocalization(final UIBeanLocalization uiLocalization) {
        this.uiLocalization = uiLocalization;
    }

    /**
     *
     * @return
     */
    public Boolean getLogged() {
        return policyManager.getCurrentUser() != null;
    }

    /**
     * Sets the void users manager.
     *
     * @param usersManager the IUsersManager users manager
     */
    public void setUsersManager(final IUsersManager usersManager) {
        this.usersManager = usersManager;
    }

    /**
     *
     * @return
     */
    public boolean getIncorectIdentification() {
        return incorectIdentification;
    }

    /**
     *
     * @param incorectIdentification
     */
    public void setIncorectIdentification(boolean incorectIdentification) {
        this.incorectIdentification = incorectIdentification;
    }

    /**
     *
     * @return
     */
    public boolean getInactiveUser() {
        return inactiveUser;
    }

    /**
     *
     * @param inactiveUser
     */
    public void setInactiveUser(boolean inactiveUser) {
        this.inactiveUser = inactiveUser;
    }

    /**
     * Adds the face message.
     *
     * @param message the String message
     * @param uiInput the UIInput ui input
     */
    private void addFaceMessage(final String message, final UIInput uiInput) {
        final FacesMessage facesMessage = new FacesMessage(message);
        final FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(uiInput.getClientId(context), facesMessage);
    }

    /**
     *
     * @return
     */
    public UIComponent getIdentificationForm() {
        return identificationForm;
    }

    /**
     *
     * @param identificationForm
     */
    public void setIdentificationForm(UIComponent identificationForm) {
        this.identificationForm = identificationForm;
    }

    /**
     *
     * @return
     */
    public String getDate(){
        return DateUtil.getUTCDateTextFromLocalDateTime(LocalDate.now(), DateUtil.YYYY_MM_DD);
    }
}
