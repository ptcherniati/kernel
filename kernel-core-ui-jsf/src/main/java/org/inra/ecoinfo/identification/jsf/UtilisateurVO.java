/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.identification.jsf;

import org.inra.ecoinfo.identification.entity.Utilisateur;

/**
 * The Class UtilisateurVO.
 */
public class UtilisateurVO {
    
    /**
     * The utilisateur.
     */
    Utilisateur utilisateur;

    /**
     * Instantiates a new utilisateur vo.
     *
     * @param utlstr
     * @param utilisateur the utilisateur
     */
    public UtilisateurVO(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return utilisateur.getEmail();
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return utilisateur.getId();
    }

    /**
     * Gets the checks if is root.
     *
     * @return the checks if is root
     */
    public Boolean getIsRoot() {
        return utilisateur.getIsRoot();
    }

    /**
     * Gets the language.
     *
     * @return the language
     */
    public String getLanguage() {
        return utilisateur.getLanguage();
    }

    /**
     * Gets the login.
     *
     * @return the login
     */
    public String getLogin() {
        return utilisateur.getLogin();
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return utilisateur.getNom();
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return utilisateur.getPassword();
    }

    /**
     * Gets the prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return utilisateur.getPrenom();
    }

    /**
     * Gets the utilisateur.
     *
     * @return the utilisateur
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }
    
}
