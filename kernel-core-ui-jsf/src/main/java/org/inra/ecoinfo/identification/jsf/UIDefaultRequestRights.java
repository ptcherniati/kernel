/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.identification.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.identification.entity.RightRequest;
import org.inra.ecoinfo.identification.entity.Utilisateur;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiRequestRights")
@ViewScoped
public class UIDefaultRequestRights extends AbstractUIBeanRequestRights<DefaultRightRequestVO, RightRequest> {
    Utilisateur utilisateur;

    /**
     *
     * @return
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     *
     * @param utilisateur
     */
    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    /**
     *
     * @return
     */
    @Override
    protected DefaultRightRequestVO getRequestVOInstance() {
       return new DefaultRightRequestVO().getInstance();
    }

    /**
     *
     * @param dbRequest
     * @return
     */
    @Override
    protected DefaultRightRequestVO newRequestVO(RightRequest dbRequest) {
        return new DefaultRightRequestVO(dbRequest);
    }
}
