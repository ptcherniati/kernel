package org.inra.ecoinfo.identification.jsf;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.Arrays;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.ValidationException;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.component.password.Password;
import org.primefaces.context.RequestContext;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiCreateProfile")
@ViewScoped
public class UIBeanCreateProfile implements Serializable {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.identification.messages";

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String BUNDLE_SOURCE_PATH_RIGHTS = "org.inra.ecoinfo.identification.impl.messages";

    /**
     * The Constant PROPERTY_MSG_BAD_PASSWORD_BIS @link(String).
     */
    private static final String PROPERTY_MSG_BAD_PASSWORD_BIS = "PROPERTY_MSG_BAD_PASSWORD_BIS";

    /**
     * The Constant PROPERTY_MSG_EMPTY_FIRST_PASSWORD @link(String).
     */
    private static final String PROPERTY_MSG_EMPTY_FIRST_PASSWORD = "PROPERTY_MSG_EMPTY_FIRST_PASSWORD";

    private static final String PROPERTY_MSG_EXISTING_LOGIN = "PROPERTY_MSG_EXISTING_LOGIN";
    private static final String PROPERTY_MSG_EMPTY_LOGIN = "PROPERTY_MSG_EMPTY_LOGIN";
    private static final String PROPERTY_MSG_BAD_EMAIL = "PROPERTY_MSG_BAD_EMAIL";
    private static final String PROPERTY_MSG_EXISTING_EMAIL = "PROPERTY_MSG_EXISTING_EMAIL";

    /**
     * The Constant PATTERN_END_MAIL.
     */
    private static final String PATTERN_END_MAIL = "/[^/]*$";

    /**
     * The Constant RULE_NAVIGATION_CHANGE_PROFILE_JSF @link(String).
     */
    private static final String RULE_NAVIGATION_CREATE_PROFILE_JSF = "createProfile";

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * The Constant STRING_EMPTY @link(String).
     */
    private static final String STRING_EMPTY = "";

    /**
     * The current password @link(String).
     */
    private String currentPassword = UIBeanCreateProfile.STRING_EMPTY;

    /**
     * The current checkBot @link(String).
     */
    private String checkBot;

    /**
     * The email @link(String).
     */
    private String email;

    /**
     * The email @link(String).
     */
    private String emploi;

    /**
     * The email @link(String).
     */
    private String poste;

    /**
     * The is valid @link(boolean).
     */
    private boolean isValid = false;

    private boolean validateCharter = false;

    /**
     * The localization manager @link(ILocalizationManager).
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    /**
     * The login @link(String).
     */
    private String login;

    /**
     * The nom @link(String).
     */
    private String nom;

    /**
     * The password @link(String).
     */
    private String password = UIBeanCreateProfile.STRING_EMPTY;

    /**
     * The password bis @link(String).
     */
    private String passwordBis = UIBeanCreateProfile.STRING_EMPTY;

    /**
     * The prenom @link(String).
     */
    private String prenom;

    /**
     * The save message @link(String).
     */
    private String saveMessage = UIBeanCreateProfile.STRING_EMPTY;

    private Utilisateur currentUser;

    /**
     * The security context @link(IPolicyManager).
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;

    /**
     * The ui form @link(HtmlPanelGroup).
     */
    private HtmlPanelGroup uiForm;

    /**
     * The users manager @link(IUsersManager).
     */
    @ManagedProperty(value = "#{usersManager}")
    protected IUsersManager usersManager;

    /**
     * Instantiates a new uI bean change profile.
     */
    public UIBeanCreateProfile() {
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public String navigate() {
        return UIBeanCreateProfile.RULE_NAVIGATION_CREATE_PROFILE_JSF;
    }

    /**
     *
     * @return
     */
    public String getMailAdmin() {
        return usersManager.getMailAdmin();
    }

    /**
     * Gets the checks if is button valid.
     *
     * @return the checks if is button valid
     * @throws BusinessException the business exception
     */
    public boolean isButtonValid() throws BusinessException {
        final Boolean isFormValid = getIsFormValid();

        if (isFormValid) {
            RequestContext.getCurrentInstance().execute("PF('popupCreateProfileConfirmation').show()");
        }
        return isFormValid;
    }

    /**
     * Gets the checks if is button valid.
     *
     * @param is
     * @throws BusinessException the business exception
     */
    public void setIsButtonValid(boolean is) throws BusinessException {
    }

    /**
     * Gets the ui email.
     *
     * @return the ui email
     */
    public HtmlInputText getUiEmail() {
        return (HtmlInputText) uiForm.findComponent("createEmail");
    }

    /**
     * Gets the ui login.
     *
     * @return the ui login
     */
    public HtmlInputText getUiLogin() {
        return (HtmlInputText) uiForm.findComponent("createLogin");
    }

    /**
     * Gets the ui nom.
     *
     * @return the ui nom
     */
    public HtmlInputText getUiNom() {
        return (HtmlInputText) uiForm.findComponent("createNom");
    }

    /**
     * Gets the ui password.
     *
     * @return the ui password
     */
    public Password getUiPassword() {
        return (Password) uiForm.findComponent("createPassword");
    }

    /**
     * Gets the ui password bis.
     *
     * @return the ui password bis
     */
    public Password getUiPasswordBis() {
        return (Password) uiForm.findComponent("createPasswordBis");
    }

    /**
     * Gets the ui prenom.
     *
     * @return the ui prenom
     */
    public HtmlInputText getUiPrenom() {
        return (HtmlInputText) uiForm.findComponent("createPrenom");
    }

    /**
     * Checks if is form valid.
     *
     * @return the boolean
     * @throws BusinessException the business exception
     */
    private Boolean getIsFormValid() throws BusinessException {
        if (checkBot == null || UIBeanCreateProfile.STRING_EMPTY.equals(checkBot)) {
            if (UIBeanCreateProfile.STRING_EMPTY.equals(getUiNom().getValue()) || getUiNom().getValue() == null || UIBeanCreateProfile.STRING_EMPTY.equals(getUiPrenom().getValue()) || getUiPrenom().getValue() == null
                    || UIBeanCreateProfile.STRING_EMPTY.equals(getUiEmail().getValue()) || getUiEmail().getValue() == null || UIBeanCreateProfile.STRING_EMPTY.equals(getUiLogin().getValue()) || getUiLogin().getValue() == null
                    || UIBeanCreateProfile.STRING_EMPTY.equals(getUiPassword().getValue()) || getUiPassword().getValue() == null || UIBeanCreateProfile.STRING_EMPTY.equals(getUiPasswordBis().getValue()) || getUiPasswordBis().getValue() == null) {
                isValid = false;
            } else {
                validatePasswordBis(FacesContext.getCurrentInstance(), getUiPasswordBis(), getUiPasswordBis().getValue());
                isValid = getUiNom().isValid() && getUiPrenom().isValid() && getUiEmail().isValid() && getUiLogin().isValid() && getUiPassword().isValid() && getUiPasswordBis().isValid();
            }
        } else {
            isValid = false;
        }
        return isValid;
    }

    /**
     * New user.
     *
     * @return the string
     */
    public String newUser() {
        reset();
        return null;
    }

    /**
     * Reset.
     *
     * @return the string
     */
    public String reset() {
        getUiNom().setValue(UIBeanCreateProfile.STRING_EMPTY);
        getUiPrenom().setValue(UIBeanCreateProfile.STRING_EMPTY);
        getUiEmail().setValue(UIBeanCreateProfile.STRING_EMPTY);
        getUiLogin().setValue(UIBeanCreateProfile.STRING_EMPTY);
        getUiPassword().setValue(UIBeanCreateProfile.STRING_EMPTY);
        getUiPasswordBis().setValue(UIBeanCreateProfile.STRING_EMPTY);
        return null;
    }

    /**
     * Update.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public String updateUser() throws BusinessException {
        if (!getIsFormValid()) {
            return null;
        }
        final HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        final String baseUrl = request.getRequestURL().toString().replaceAll(UIBeanCreateProfile.PATTERN_END_MAIL, UIBeanCreateProfile.STRING_EMPTY);
        try {
            currentUser = new Utilisateur();
            currentUser.setNom(nom);
            currentUser.setPrenom(prenom);
            currentUser.setEmail(email);
            currentUser.setLogin(login);
            currentUser.setEmploi(emploi);
            currentUser.setPoste(poste);
            currentUser.setPassword(password);
            currentUser.setIsRoot(false);
            usersManager.addUserProfile(currentUser, baseUrl);

        } catch (final BusinessException e) {
            LoggerFactory.getLogger(UIBeanCreateProfile.class).error(localizationManager.getMessage(UIBeanCreateProfile.BUNDLE_SOURCE_PATH, UIBeanChangeProfile.PROPERTY_MSG_USER_NOT_ADDED), e);
        }
        return "mailAccount";
    }

    /**
     * Validate login.
     *
     * @param context
     * @link(FacesContext) the context
     * @param component
     * @link(UIComponent) the component
     * @param value
     * @link(Object) the value
     * @throws ValidationException the validation exception
     */
    public void validateLogin(final FacesContext context, final UIComponent component, Object value) throws ValidationException {
        value = trim(value);
        if (value == null || Strings.isNullOrEmpty(value.toString())) {
            getUiLogin().setValid(false);
            context.addMessage(getUiLogin().getClientId(context), new FacesMessage(localizationManager.getMessage(UIBeanCreateProfile.BUNDLE_SOURCE_PATH, UIBeanCreateProfile.PROPERTY_MSG_EMPTY_LOGIN)));
        } else {
            String localValue = (String) value;
            boolean existLogin = Arrays.stream(WhichTree.values()).anyMatch(t -> policyManager.getGroupByGroupNameAndWhichTree(localValue, t).isPresent());
            if (existLogin) {
                context.addMessage(getUiLogin().getClientId(context), new FacesMessage(localizationManager.getMessage(UIBeanCreateProfile.BUNDLE_SOURCE_PATH, UIBeanCreateProfile.PROPERTY_MSG_EXISTING_LOGIN)));
                getUiLogin().setValid(false);
            } else {
                getUiLogin().setValid(true);
            }
        }
        validatePassword(context, component, value);
    }

    /**
     *
     * @param context
     * @param component
     * @param value
     * @throws ValidationException
     */
    public void validateEmail(final FacesContext context, final UIComponent component, Object value) throws ValidationException {
        value = trim(value);
        if (value == null || UIBeanCreateProfile.STRING_EMPTY.equals(value.toString())) {
            getUiEmail().setValid(false);
            context.addMessage(getUiEmail().getClientId(context), new FacesMessage(localizationManager.getMessage(UIBeanCreateProfile.BUNDLE_SOURCE_PATH, UIBeanCreateProfile.PROPERTY_MSG_BAD_EMAIL)));
        } else if (usersManager.getUtilisateurByEmail((String) value) != null) {
            context.addMessage(getUiEmail().getClientId(context), new FacesMessage(String.format(localizationManager.getMessage(UIBeanCreateProfile.BUNDLE_SOURCE_PATH, UIBeanCreateProfile.PROPERTY_MSG_EXISTING_EMAIL), value)));
            getUiEmail().setValid(false);
        } else {
            getUiEmail().setValid(true);
        }
    }

    /**
     * Validate password bis for new.
     *
     * @param context
     * @link(FacesContext) the context
     * @param component
     * @link(UIComponent) the component
     * @param value
     * @link(Object) the value
     */
    public void validatePasswordBis(final FacesContext context, final UIComponent component, Object value) {
        value = trim(value);
        if (getUiPassword().isValid() && (value == null || !value.toString().equals(getUiPassword().getValue().toString()))) {
            getUiPasswordBis().setValid(false);
            context.addMessage(getUiPasswordBis().getClientId(context), new FacesMessage(localizationManager.getMessage(UIBeanCreateProfile.BUNDLE_SOURCE_PATH, UIBeanCreateProfile.PROPERTY_MSG_BAD_PASSWORD_BIS)));
        } else {
            getUiPasswordBis().setValid(true);
        }
    }

    /**
     * Validate password for new.
     *
     * @param context
     * @link(FacesContext) the context
     * @param component
     * @link(UIComponent) the component
     * @param value
     * @link(Object) the value
     */
    public void validatePassword(final FacesContext context, final UIComponent component, Object value) {
        // ne doit pas être vide
        value = trim(value);
        if (value == null || UIBeanCreateProfile.STRING_EMPTY.equals(value.toString())) {
            getUiPassword().setValid(false);
            context.addMessage(getUiPassword().getClientId(context), new FacesMessage(localizationManager.getMessage(UIBeanCreateProfile.BUNDLE_SOURCE_PATH, UIBeanCreateProfile.PROPERTY_MSG_EMPTY_FIRST_PASSWORD)));
        } else {
            getUiPassword().setValid(true);
        }
    }

    /**
     *
     * @return
     */
    public String getCurrentPassword() {
        return currentPassword;
    }

    /**
     *
     * @param currentPassword
     */
    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = trim(currentPassword);
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = trim(email);
    }

    /**
     *
     * @return
     */
    public String getEmploi() {
        return emploi;
    }

    /**
     *
     * @param emploi
     */
    public void setEmploi(String emploi) {
        this.emploi = emploi;
    }

    /**
     *
     * @return
     */
    public String getPoste() {
        return poste;
    }

    /**
     *
     * @param poste
     */
    public void setPoste(String poste) {
        this.poste = poste;
    }

    /**
     *
     * @return
     */
    public boolean isValid() {
        return isValid;
    }

    /**
     *
     * @param isValid
     */
    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @return
     */
    public String getLogin() {
        return login;
    }

    /**
     *
     * @param login
     */
    public void setLogin(String login) {
        this.login = trim(login);
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = trim(nom);
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = trim(password);
    }

    /**
     *
     * @return
     */
    public String getPasswordBis() {
        return passwordBis;
    }

    /**
     *
     * @param passwordBis
     */
    public void setPasswordBis(String passwordBis) {
        this.passwordBis = trim(passwordBis);
    }

    /**
     *
     * @return
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     *
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = trim(prenom);
    }

    /**
     *
     * @return
     */
    public String getSaveMessage() {
        return saveMessage;
    }

    /**
     *
     * @param saveMessage
     */
    public void setSaveMessage(String saveMessage) {
        this.saveMessage = saveMessage;
    }

    /**
     *
     * @return
     */
    public HtmlPanelGroup getUiForm() {
        return uiForm;
    }

    /**
     *
     * @param uiForm
     */
    public void setUiForm(HtmlPanelGroup uiForm) {
        this.uiForm = uiForm;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param usersManager
     */
    public void setUsersManager(IUsersManager usersManager) {
        this.usersManager = usersManager;
    }

    /**
     *
     * @return
     */
    public Utilisateur getCurrentUser() {
        return currentUser;
    }

    /**
     *
     * @param currentUser
     */
    public void setCurrentUser(Utilisateur currentUser) {
        this.currentUser = currentUser;
    }

    /**
     *
     * @return
     */
    public String getCheckBot() {
        return checkBot;
    }

    /**
     *
     * @param checkBot
     */
    public void setCheckBot(String checkBot) {
        this.checkBot = checkBot;
    }

    private String trim(Object chaine) {
        return chaine == null ? "" : chaine.toString().trim();
    }

    /**
     *
     * @return
     */
    public boolean isValidateCharter() {
        return validateCharter;
    }

    /**
     *
     * @param validateCharter
     */
    public void setValidateCharter(boolean validateCharter) {
        this.validateCharter = validateCharter;
    }
}
