package org.inra.ecoinfo.popup;

import java.io.Serializable;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.lang.StringEscapeUtils;
import org.primefaces.context.RequestContext;

/**
 * The Class UIBeanPopup.
 */
@ManagedBean(name = "uiPopup")
@ViewScoped
public class UIBeanPopup implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     * @param message
     */
    public static void complete(String message) {
        String action = String.format("showPopupTimer([{name: 'timeDisplay', value: '3000'}, {name: 'header', value: 'Info'}, {name: 'message', value: '%s'}]);",
                action = StringEscapeUtils.escapeJavaScript(message));
        RequestContext.getCurrentInstance().execute(action);
    }
    /**
     * The message @link(String).
     */
    private String message="message";
    private String header="head";
    /**
     * Temps d'affichage par défaut à 2s.
     */
    private String timeDisplay = "3000";
    private String widget = "popupTimer";
    /**
     * Instantiates a new uI bean popup.
     */
    public UIBeanPopup() {
    }

    /**
     *
     * @return
     */
    public String getHeader() {
        return header;
    }

    /**
     *
     */
    public void setPopup() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map map = context.getExternalContext().getRequestParameterMap();
        timeDisplay = map.containsKey("timeDisplay") ? (String) map.get("timeDisplay") : "5000";
        message = map.containsKey("message") ? (String) map.get("message") : "Un message";
        this.header = map.containsKey("header") ? (String) map.get("header") : "Information";
        this.widget = map.containsKey("widget") ? (String) map.get("widget") : "popupTimer";
        final RequestContext currentInstance = RequestContext.getCurrentInstance();
        currentInstance.reset("popupsMessages:formulaire:popupTimer");
        RequestContext.getCurrentInstance().execute("PF('popupTimer').show()");
    }

    /**
     *
     * @param header
     */
    public void setHeader(String header) {
        this.header = header;
    }


    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message.
     *
     * @param message the new message
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * Gets the temps d'affichage par défaut à 2s.
     *
     * @return the temps d'affichage par défaut à 2s
     */
    public String getTimeDisplay() {
        return timeDisplay;
    }

    /**
     * Sets the temps d'affichage par défaut à 2s.
     *
     * @param timeDisplay the new temps d'affichage par défaut à 2s
     */
    public void setTimeDisplay(final String timeDisplay) {
        this.timeDisplay = timeDisplay;
    }
}
