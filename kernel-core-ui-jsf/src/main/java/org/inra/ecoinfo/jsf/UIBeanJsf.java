package org.inra.ecoinfo.jsf;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.Transient;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * The Class UIBeanJsf.
 */
@ManagedBean(name = "uiJsf")
@ViewScoped
public class UIBeanJsf implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(UIBeanJsf.class.getName());
    /**
     * The transaction manager @link(JpaTransactionManager).
     */
    @ManagedProperty(value = "#{transactionManager}")
    protected JpaTransactionManager transactionManager;
    /**
     * The users manager @link(IUsersManager).
     */
    @ManagedProperty(value = "#{usersManager}")
    protected IUsersManager usersManager;

    /**
     * Instantiates a new uI bean jsf.
     */
    public UIBeanJsf() {
    }

    /**
     * Sets the transaction manager.
     *
     * @param transactionManager the new transaction manager
     */
    public void setTransactionManager(final JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     * Sets the users manager.
     *
     * @param usersManager the new users manager
     */
    public void setUsersManager(final IUsersManager usersManager) {
        this.usersManager = usersManager;
    }

    /**
     * Test.
     *
     * @return the string
     */
    public String test() {
        try {
            final TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));
            final Utilisateur utilisateur = usersManager.getUtilisateurByLogin("antoine").orElseThrow(BusinessException::new );
            LOGGER.info(utilisateur == null ? "pas d'utilisateur" : utilisateur.getNom());
            transactionManager.commit(status);
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in test", e);
        }
        return null;
    }
}
