package org.inra.ecoinfo.notifications.impl;

import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.IMessageProducer;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class JSFNotificationManager extends LocalNotificationManager {

    /**
     * The IMessageProducer message producer.
     */
    private IMessageProducer messageProducer;

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void addNotification(Notification notification, String target) throws BusinessException {
        super.addNotification(notification, target);
        tryPushMessage(notification == null ? null : notification, target);
    }

    /**
     * Sets the void message producer.
     *
     * @param messageProducer the IMessageProducer message producer
     */
    public void setMessageProducer(final IMessageProducer messageProducer) {
        this.messageProducer = messageProducer;
    }

    private void tryPushMessage(final Notification notification, String target) throws BusinessException {
        try {
            final Utilisateur utilisateur = utilisateurDAO.getByLogin(target).orElseThrow(PersistenceException::new);
            messageProducer.push(utilisateur.getLogin(), notification);
        } catch (final PersistenceException e) {
            throw new BusinessException("can't push message", e);
        }
    }

}
