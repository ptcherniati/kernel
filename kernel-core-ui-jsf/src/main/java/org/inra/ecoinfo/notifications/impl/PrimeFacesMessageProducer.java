package org.inra.ecoinfo.notifications.impl;

import java.util.Optional;
import org.inra.ecoinfo.notifications.IMessageProducer;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.primefaces.push.EventBusFactory;


/**
 * The Class PrimeFacesMessageProducer.
 */
public class PrimeFacesMessageProducer implements IMessageProducer {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.IMessageProducer#push(java.lang.String, java.lang.Long)
     */
    
    @Override
    public void push(final String login, final Notification notification){
        Optional.ofNullable(EventBusFactory.getDefault())
                .map(EventBusFactory::eventBus)
                .ifPresent(eventBus->eventBus.publish(String.format("/notify/%s", login), notification));
    }
}
