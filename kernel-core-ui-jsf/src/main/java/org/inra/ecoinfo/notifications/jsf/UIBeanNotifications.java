package org.inra.ecoinfo.notifications.jsf;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.Transient;
import org.apache.commons.lang.StringEscapeUtils;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UIBeanNotifications.
 */
@ManagedBean(name = "uiNotifications")
@ViewScoped
public class UIBeanNotifications implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The Constant TOGGLE_EMPTY.
     */
    private static final String TOGGLE_EMPTY = "togglePanelItemEmpty";
    /**
     * The Constant TOGGLE_NOTIFICATION.
     */
    private static final String TOGGLE_NOTIFICATION = "togglePanelItemNotifications";
    /**
     * The Logger LOGGER.
     */
    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(UIBeanNotifications.class.getName());
    private final static String CHANNEL = "/notify";
    /**
     * The Integer counter new messages.
     */
    private int counterNewMessages = 0;
    /**
     * The Boolean display new messages.
     */
    private Boolean displayNewMessages = false;
    /**
     * The String id notification.
     */
    private String idNotification;
    /**
     * The List<Notification> notifications.
     */
    private List<Notification> notifications;
    /**
     * The UIDataTable notifications data table.
     */
    /**
     * The Notification selected notification.
     */
    private Notification selectedNotification;
    /**
     * The String toggle.
     */
    private String toggle = UIBeanNotifications.TOGGLE_EMPTY;
    /**
     * The INotificationsManager notifications manager.
     */
    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;
    /**
     * The IPolicyManager security context.
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;

    private String summary;

    private String detail;

    /**
     * Instantiates a new UIBeanNotifications uI bean notifications.
     */
    public UIBeanNotifications() {
        super();
    }

    /**
     *
     * @return
     */
    public String getSummary() {
        return summary;
    }

    /**
     *
     * @param summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     *
     * @return
     */
    public String getDetail() {
        return detail;
    }

    /**
     *
     * @param detail
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     *
     */
    public void send() {
        final EventBusFactory defaultEvent = EventBusFactory.getDefault();
        EventBus eventBus = defaultEvent.eventBus();
        eventBus.publish(CHANNEL, new FacesMessage(StringEscapeUtils.escapeHtml(summary), StringEscapeUtils.escapeHtml(detail)));
    }

    /**
     * Delete all notifications.
     *
     * @return the String string
     * @throws BusinessException the business exception
     */
    public String deleteAllNotifications() throws BusinessException {
        notificationsManager.removeAllNotification(policyManager.getCurrentUser().getId());
        notifications = null;
        selectedNotification = null;
        counterNewMessages = 0;
        return null;
    }

    /**
     * Delete notification.
     *
     * @return the String string
     */
    public String deleteNotification() {
        try {
            notificationsManager.removeNotification(selectedNotification.getId());
            notifications.remove(selectedNotification);
            selectedNotification = null;
        } catch (final BusinessException e) {
            LOGGER.error("can't delete notification", e);
        }
        return null;
    }

    /**
     * Gets the Integer counter new messages.
     *
     * @return the Integer counter new messages
     * @throws BusinessException the business exception
     */
    public int getCounterNewMessages() throws BusinessException {
        if (policyManager.getCurrentUser() == null) {
            return 0;
        }
        if (counterNewMessages <= 0) {
            counterNewMessages = notificationsManager.getAllNoArchivedByUserLogin(policyManager.getCurrentUser().getLogin()).size();
        }
        return counterNewMessages;
    }

    /**
     * Gets the Boolean display new messages.
     *
     * @return the Boolean display new messages
     */
    public Boolean getDisplayNewMessages() {
        return displayNewMessages;
    }

    /**
     * Sets the void counter new messages.
     *
     * @param message the String message public void setCounterNewMessages(final
     * String message) { counterNewMessages++; }
     */
    /**
     * Sets the void display new messages.
     *
     * @param displayNewMessages the Boolean display new messages
     */
    public void setDisplayNewMessages(final Boolean displayNewMessages) {
        this.displayNewMessages = displayNewMessages;
    }

    /**
     * Gets the String id notification.
     *
     * @return the String id notification
     */
    public String getIdNotification() {
        return idNotification;
    }

    /**
     * Sets the void id notification.
     *
     * @param idNotification the String id notification
     */
    public void setIdNotification(final String idNotification) {
        this.idNotification = idNotification;
    }

    /**
     * Gets the String login.
     *
     * @return the String login
     */
    public String getLogin() {
        return policyManager.getCurrentUser().getLogin();
    }

    /**
     *
     */
    public void reloadNotifications() {
        notifications = null;
        counterNewMessages = 0;
    }

    /**
     * Gets the List<Notification> notifications.
     *
     * @return the List<Notification> notifications
     */
    public List<Notification> getNotifications() {
        if (notifications == null) {
            if (displayNewMessages) {
                notifications = notificationsManager.getAllNoArchivedByUserLogin(policyManager.getCurrentUser().getLogin());
            } else if (policyManager.getCurrentUser() != null) {
                notifications = notificationsManager.getAllUserNotifications(policyManager.getCurrentUser().getLogin());
            }
        }
        return notifications;
    }

    /**
     * Sets the void notifications.
     *
     * @param notifications the List<Notification> notifications
     */
    public void setNotifications(final List<Notification> notifications) {
        this.notifications = notifications;
    }

    /**
     * Gets the Notification selected notification.
     *
     * @return the Notification selected notification
     */
    public Notification getSelectedNotification() {
        return selectedNotification;
    }

    /**
     * Sets the void selected notification.
     *
     * @param selectedNotification the Notification selected notification
     */
    public void setSelectedNotification(final Notification selectedNotification) {
        this.selectedNotification = selectedNotification;
    }

    /**
     * Gets the String selected notification troncate text.
     *
     * @return the String selected notification troncate text
     */
    public String getSelectedNotificationTroncateText() {
        final StringBuilder message = new StringBuilder();
        if (selectedNotification != null) {
            message.append("<p>").append(selectedNotification.getBodyForAffichage().replaceAll("^.*Exception: |\n|\r|\n\r|\r\n", "</p><p>")).append("</p>");
        }
        return message.toString();
    }

    /**
     * Gets the String toggle.
     *
     * @return the String toggle
     */
    public String getToggle() {
        if (toggle == null ? UIBeanNotifications.TOGGLE_NOTIFICATION == null : toggle.equals(UIBeanNotifications.TOGGLE_NOTIFICATION)) {
            toggle = UIBeanNotifications.TOGGLE_EMPTY;
        } else {
            toggle = UIBeanNotifications.TOGGLE_NOTIFICATION;
        }
        return toggle;
    }

    /**
     * Sets the void toggle.
     *
     * @param toggle the String toggle
     */
    public void setToggle(final String toggle) {
        this.toggle = toggle;
    }

    /**
     * Mark all as read.
     *
     * @return the String string
     * @throws BusinessException the business exception
     */
    public String markAllAsRead() throws BusinessException {
        notificationsManager.markAllAsRead(policyManager.getCurrentUser().getId());
        notifications = null;
        counterNewMessages = 0;
        return null;
    }

    /**
     * Mark as read.
     *
     * @return the String string
     */
    public String markAsRead() {
        notificationsManager.markNotificationAsArchived(selectedNotification.getId());
        final Integer indexNotification = notifications.indexOf(selectedNotification);
        selectedNotification.setArchived(true);
        notifications.remove(selectedNotification);
        if (!displayNewMessages) {
            notifications.add(indexNotification, selectedNotification);
        }
        counterNewMessages--;
        return null;
    }

    /**
     *
     * @param event
     * @throws BusinessException
     */
    public void send(ValueChangeEvent event) throws BusinessException {
        Optional<Notification> notification = notificationsManager.getNotificationById((Long) event.getNewValue());
        notificationsManager.addNotification(notification.orElseThrow(()->new BusinessException("can't get notification")), "admin");
    }

    /**
     * Mark notification as new.
     *
     * @return the String string
     */
    public String markNotificationAsNew() {
        notificationsManager.markNotificationAsNew(selectedNotification.getId());
        final Integer indexNotification = notifications.indexOf(selectedNotification);
        selectedNotification.setArchived(false);
        notifications.remove(selectedNotification);
        notifications.add(indexNotification, selectedNotification);
        counterNewMessages++;
        return null;
    }

    /**
     * Sets the void notifications manager.
     *
     * @param notificationsManager the INotificationsManager notifications
     * manager
     */
    public void setNotificationsManager(final INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * Sets the void security context.
     *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Switch messages display.
     *
     * @param event
     * @return the String string
     */
    public String switchMessagesDisplay(ValueChangeEvent event) {
        displayNewMessages = (Boolean) event.getNewValue();
        if (displayNewMessages) {
            notifications = notificationsManager.getAllNoArchivedByUserLogin(policyManager.getCurrentUser().getLogin());
            selectedNotification = notifications.contains(selectedNotification) ? selectedNotification : null;
        } else {
            notifications = notificationsManager.getAllUserNotifications(policyManager.getCurrentUser().getLogin());
            selectedNotification = notifications.contains(selectedNotification) ? selectedNotification : null;
        }
        return null;
    }
}
