package org.inra.ecoinfo.menu.jsf;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.Transient;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.menu.IMenuManager;
import org.inra.ecoinfo.menu.Menu;
import org.inra.ecoinfo.menu.MenuItem;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.Utils;
import org.primefaces.component.menubar.Menubar;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.primefaces.model.menu.Submenu;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class UIBeanMenu.
 */
@ManagedBean(name = "uiMenu")
@SessionScoped
public class UIBeanMenu implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    private static final String BUNDLE_NAME = "org.inra.ecoinfo.menu.menu";
    private static final String CONFIGURATION_NUMBER = "configurationNumber";
    private static final DefaultMenuModel DEFAULT_MENU_MODEL = new DefaultMenuModel();
    Integer codeConfiguration;
    /**
     * The IMenuManager menu manager.
     */
    @ManagedProperty(value = "#{menuManager}")
    protected IMenuManager menuManager;
    /**
     * The UIToolbarGroup restricted toolbar group.
     */
    @Transient
    protected Menubar restrictedToolbarGroup;
    /**
     * The IPolicyManager security context.
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;
    private MenuModel restrictedMenu = DEFAULT_MENU_MODEL;
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    /**
     * Instantiates a new UIBeanMenu uI bean menu.
     */
    public UIBeanMenu() {
    }

    /**
     *
     * @throws CloneNotSupportedException
     */
    public void reInitMenu() throws CloneNotSupportedException {
        restrictedMenu = new DefaultMenuModel();
        final Menu menu = menuManager.buildRestrictedMenu();
        Integer dropDownMenuIndexPosition = 0;
        for (final MenuItem menuItem : menu.getMenuItems()) {
            if (policyManager.isRoot() || !menuItem.getDisabled()) {
                restrictedMenu.addElement(buildSubMenu(menuItem, dropDownMenuIndexPosition++));
            }
        }
    }

    /**
     *
     * @throws CloneNotSupportedException
     */
    public void preRender() throws CloneNotSupportedException {
        if (policyManager.getCurrentUser() != null && restrictedMenu == DEFAULT_MENU_MODEL) {
            reInitMenu();
        }
    }

    /**
     * Builds the restricted toolbar group.
     *
     * @return the UIToolbarGroup uI toolbar group
     * @throws CloneNotSupportedException the clone not supported exception
     */
    @Transactional(rollbackFor = Exception.class)
    public MenuModel buildRestrictedToolbarGroup() throws CloneNotSupportedException {
        return restrictedMenu;
    }

    /**
     * Builds the ui drop down menu.
     *
     * @param menuItemParent the MenuItem menu item parent
     * @param dropDownMenuPositionIndex the Integer drop down menu position
     * index
     * @return the UIDropDownMenu uI drop down menu
     */
    @Transactional(rollbackFor = Exception.class)

    public Submenu buildSubMenu(final MenuItem menuItemParent, final Integer dropDownMenuPositionIndex) {
        final DefaultSubMenu subMenu
                = new DefaultSubMenu(
                        localizationManager.getMessage(BUNDLE_NAME, menuItemParent.getLabel()),
                        String.format("%s", menuItemParent.getIcon().replaceAll("\\..*", ""))
                );
        final int i = 0;
        addMenuItemsToParent(menuItemParent, dropDownMenuPositionIndex, subMenu, i);
        subMenu.setId(Utils.createCodeFromString(menuItemParent.getLabel()));
        return subMenu;
    }

    /**
     * Gets the UIToolbarGroup restricted toolbar group.
     *
     * @return the UIToolbarGroup restricted toolbar group
     * @throws CloneNotSupportedException the clone not supported exception
     */
    public MenuModel getRestrictedToolbarGroup() throws CloneNotSupportedException {
        return buildRestrictedToolbarGroup();
    }

    /**
     * Sets the void restricted toolbar group.
     *
     * @param restrictedToolbarGroup the UIToolbarGroup restricted toolbar group
     */
    public void setRestrictedToolbarGroup(final Menubar restrictedToolbarGroup) {
        this.restrictedToolbarGroup = restrictedToolbarGroup;
    }

    /**
     * Sets the void menu manager.
     *
     * @param menuManager the IMenuManager menu manager
     */
    public void setMenuManager(final IMenuManager menuManager) {
        this.menuManager = menuManager;
    }

    /**
     * Sets the void security context.
     *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Adds the menu items to parent.
     *
     * @param menuItemParent the MenuItem menu item parent
     * @param dropDownMenuPositionIndex the Integer drop down menu position
     * index
     * @param context the FacesContext context
     * @param dropDownMenu the UIDropDownMenu drop down menu
     * @param index the int index
     * @return the int int
     */
    private int addMenuItemsToParent(final MenuItem menuItemParent, final Integer dropDownMenuPositionIndex, final DefaultSubMenu dropDownMenu, int index) {
        int returnIndex = index;
        for (final MenuItem menuItem : menuItemParent.getChildren()) {

            final DefaultMenuItem menItm = new DefaultMenuItem(localizationManager.getMessage(BUNDLE_NAME, menuItem.getLabel()));
            menItm.setTitle(localizationManager.getMessage(BUNDLE_NAME, menuItem.getComment()));
            if (StringUtils.isNotBlank(menuItem.getDirectAction())) {
                menItm.setOutcome(menuItem.getDirectAction());
                menItm.setParam("directAction", menuItem.getDirectAction());
                menItm.setOnclick(String.format("redirect([{directAction:'%s', value:'one'},{name:'configurationNumber', value:'%s'}]);", menuItem.getDirectAction(), menuItem.getCodeConf()));
            }
            menItm.setImmediate(true);
            final String icon = menuItem.getIcon() == null ? "" : menuItem.getIcon();
            menItm.setIcon(String.format("%s", icon.replaceAll("\\..*", "")));
            menItm.setId("id".concat(dropDownMenuPositionIndex.toString()).concat(Integer.toString(returnIndex++)));
            menItm.setDisabled(!policyManager.isRoot() && menuItem.getDisabled());
            menItm.setCommand("#{uiMenu.menuListener}");
            menItm.setEscape(false);
            //menItm.setCommand(String.format("#{uiMenu.menuListener(%s)}", menuItem.getCodeConf()));
            if (menuItem.getCodeConf() != null) {
                menItm.setParam("configurationNumber", menuItem.getCodeConf());
            }
            dropDownMenu.addElement(menItm);
        }
        return returnIndex;
    }

    /**
     *
     * @return
     */
    public Integer getConfigurationNumber() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> paramMap = context.getExternalContext().getRequestParameterMap();
        codeConfiguration = Optional.ofNullable(paramMap.get("configurationNumber"))
                .map(p -> Integer.parseInt(p))
                .orElse(
                        Optional.ofNullable(codeConfiguration)
                                .orElseGet(this::testIsSynthesis)
                );
        return codeConfiguration;
    }

    /**
     *
     * @param event
     */
    public void menuListener(ActionEvent event) {
        codeConfiguration = (Integer) event.getComponent().getAttributes().get(CONFIGURATION_NUMBER);
    }

    /**
     *
     */
    public void redirect() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        final String codeConfigurationString = params.get("configurationNumber");
        codeConfiguration = codeConfigurationString.matches("[0-9]*") ? Integer.valueOf(codeConfigurationString) : 0;
        String directAction = params.get("directAction");

    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    private Integer testIsSynthesis() {
        boolean isLoginPage = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("referer").endsWith("login.jsf");
        boolean isRequestSynthesis = "/synthesisManagement.xhtml".equals(FacesContext.getCurrentInstance().getViewRoot().getViewId());
        if (isLoginPage && isRequestSynthesis) {
            return 5;
        }
        return -1;
    }
}
