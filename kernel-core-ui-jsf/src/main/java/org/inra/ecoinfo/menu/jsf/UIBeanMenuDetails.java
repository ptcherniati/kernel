package org.inra.ecoinfo.menu.jsf;

import java.io.Serializable;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Transient;
import org.inra.ecoinfo.menu.IMenuManager;
import org.inra.ecoinfo.menu.Menu;
import org.inra.ecoinfo.menu.MenuItem;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;

/**
 * The Class UIBeanMenuDetails.
 */
@ManagedBean(name = "uiMenuDetails")
@SessionScoped
public final class UIBeanMenuDetails implements Serializable {

    private static final Menu DEFAULT_MENU = new Menu();

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The Menu menu.
     */
    protected Menu menu = DEFAULT_MENU;
    /**
     * The IMenuManager menu manager.
     */
    @ManagedProperty(value = "#{menuManager}")
    protected IMenuManager menuManager;
    /**
     * The Messages messages.
     */
    @Transient
    private final Messages messages = new Messages();
    /**
     * The IPolicyManager security context.
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;

    /**
     * Instantiates a new UIBeanMenuDetails uI bean menu details.
     */
    public UIBeanMenuDetails() {
    }

    /**
     *
     * @throws CloneNotSupportedException
     */
    public void preRender() throws CloneNotSupportedException {
        if (policyManager.getCurrentUser() != null && menu == DEFAULT_MENU) {
            reInitMenuDetail();
        }

    }

    /**
     *
     * @throws CloneNotSupportedException
     */
    public void reInitMenuDetail() throws CloneNotSupportedException {
        menu = new Menu();
        for (MenuItem menuItem : menuManager.buildRestrictedMenu().getMenuItems()) {
            menu.addMenuItem(menuItem.clone());
        }
        translateMenu(menu);
    }

    /**
     * Gets the Menu menu.
     *
     * @param codeConfiguration
     * @param codeConf
     * @return the Menu menu
     * @throws CloneNotSupportedException the clone not supported exception
     */
    public Menu menu(Integer codeConfiguration) throws CloneNotSupportedException {
        return menu;
    }

    /**
     * Sets the void menu manager.
     *
     * @param menuManager the IMenuManager menu manager
     */
    public void setMenuManager(final IMenuManager menuManager) {
        this.menuManager = menuManager;
    }

    /**
     * Sets the void security context.
     *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Translate menu.
     *
     * @param menu the Menu menu
     */
    private void translateMenu(final Menu menu) {
        menu.getMenuItems().stream().map((menuItem) -> {
            menuItem.setLabel(messages.getMessage(menuItem.getLabel()));
            return menuItem;
        }).map((menuItem) -> {
            menuItem.setComment(messages.getMessage(menuItem.getComment()));
            return menuItem;
        }).filter((menuItem) -> (!menuItem.getChildren().isEmpty())).forEach((menuItem) -> {
            translateMenuItem(menuItem);
        });
    }

    /**
     * Translate menu item.
     *
     * @param menuItemParent the MenuItem menu item parent
     */
    private void translateMenuItem(final MenuItem menuItemParent) {
        menuItemParent.getChildren().stream().map((menuItem) -> {
            menuItem.setLabel(messages.getMessage(menuItem.getLabel()));
            return menuItem;
        }).map((menuItem) -> {
            menuItem.setComment(messages.getMessage(menuItem.getComment()));
            return menuItem;
        }).filter((menuItem) -> (!menuItem.getChildren().isEmpty())).forEach((menuItem) -> {
            translateMenuItem(menuItem);
        });
    }

    /**
     * The Class Messages.
     */
    public static class Messages {

        /**
         * The Constant BUNDLE_NAME.
         */
        private static final String BUNDLE_NAME = "org.inra.ecoinfo.menu.menu";

        /**
         * Instantiates a new UIBeanMenuDetails messages.
         */
        private Messages() {
        }

        /**
         * Gets the ResourceBundle bundle.
         *
         * @return the ResourceBundle bundle
         */
        public ResourceBundle getBundle() {
            Locale locale = null;
            locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
            return ResourceBundle.getBundle(Messages.BUNDLE_NAME, locale);
        }

        /**
         * Gets the String message.
         *
         * @param key the String key
         * @return the String message
         */
        public String getMessage(final String key) {
            try {
                return getBundle().containsKey(key) ? getBundle().getString(key) : key;
            } catch (final Exception e) {
                UncatchedExceptionLogger.log("no value for key", e);
                return key;
            }
        }
    }
}
