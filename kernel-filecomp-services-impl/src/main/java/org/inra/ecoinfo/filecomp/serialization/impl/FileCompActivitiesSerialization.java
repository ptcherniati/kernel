/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.filecomp.serialization.impl;

import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.serialization.impl.AbstractActivitySerialization;

/**
 *
 * @author tcherniatinsky
 */
public class FileCompActivitiesSerialization extends AbstractActivitySerialization{

    /**
     *
     */
    public static final String PRIVILEGES_ENTRY = "activities_filecomp.csv";
    private static final String MODULE_NAME = "filecompActivitySerialization";

    /**
     *
     * @return
     */
    @Override
    public String getActivityEntry() {
        return  PRIVILEGES_ENTRY;
    }

    /**
     *
     * @return
     */
    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

    /**
     *
     * @return
     */
    @Override
    public int getConfigurationNumber() {
        return AbstractMgaIOConfigurator.ASSOCIATE_CONFIGURATION;
    }

    /**
     *
     * @return
     */
    @Override
    public WhichTree getWhichTree() {
        return WhichTree.TREEDATASET;
    }
    
}
