package org.inra.ecoinfo.filecomp;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.filecomp.entity.FileType;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<FileType> {

    /**
     * The properties description en @link(Properties).
     */
    private Properties propertiesDescriptionEN;
    /**
     * The properties nom en @link(Properties).
     */
    private Properties propertiesNomFR;
    private Properties propertiesNomEN;

    private IFileTypeDAO fileTypeDAO;

    /**
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final String code = Utils.createCodeFromString(values[0]);
                fileTypeDAO.remove(fileTypeDAO.getByCode(code)
                        .orElseThrow(() -> new BusinessException("can't get file type")));

                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param fileType
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(FileType fileType) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(fileType == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : fileType.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(fileType == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomFR.getProperty(fileType.getName(),fileType.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(fileType == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesNomEN.getProperty(fileType.getName(),fileType.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(fileType == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : fileType.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(fileType == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesDescriptionEN.getProperty(fileType.getDescription(),fileType.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }

    /**
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, FileType.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String description = tokenizerValues.nextToken();
                Optional<FileType> dbFileType = fileTypeDAO.getByCode(Utils.createCodeFromString(nom));
                if (!dbFileType.isPresent()) {
                    final FileType fileType = new FileType(nom, description);
                    fileTypeDAO.saveOrUpdate(fileType);
                } else {
                    dbFileType.get().setDescription(description);
                }
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param fileTypeDAO
     */
    public void setFileTypeDAO(IFileTypeDAO fileTypeDAO) {
        this.fileTypeDAO = fileTypeDAO;
    }

    /**
     *
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException @throws PersistenceException
     */
    @Override
    protected List<FileType> getAllElements() throws BusinessException {
        return fileTypeDAO.getAll(FileType.class);
    }

    /**
     * Inits the model grid metadata.
     *
     * @return the model grid metadata
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<FileType> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(FileType.NAME_ENTITY_JPA, "nom", Locale.FRANCE);
        propertiesNomEN = localizationManager.newProperties(FileType.NAME_ENTITY_JPA, "nom", Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(FileType.NAME_ENTITY_JPA, "description", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
}
