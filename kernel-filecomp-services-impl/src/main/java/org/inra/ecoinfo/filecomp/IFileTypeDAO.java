package org.inra.ecoinfo.filecomp;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.filecomp.entity.FileType;

/**
 * The Interface IFileTypeDAO.
 */
public interface IFileTypeDAO extends IDAO<FileType> {

    /**
     * Gets the FileType by code.
     *
     * @param code
     * @link(String) the code
     * @return the FileType
     */
    Optional<FileType> getByCode(String code);

    /**
     *
     * @return
     */
    List<FileType> getListTypesPossible();
}
