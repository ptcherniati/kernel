package org.inra.ecoinfo.filecomp.deserialization.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.digester.Digester;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.deserialization.AbstractDeserialization;
import org.inra.ecoinfo.filecomp.IFileCompDAO;
import org.inra.ecoinfo.filecomp.IFileTypeDAO;
import org.inra.ecoinfo.filecomp.deserialization.IFileCompDeserialization;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.filecomp.entity.FileType;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;

/**
 * The Class MailAdminDeserialization.
 */
public class FileCompDeserialization extends AbstractDeserialization implements IFileCompDeserialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/fileCompDeserialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "fileCompDeserialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(FileCompDeserialization.class.getName());
    private static final String FILECOMPS_ENTRY = "filecomp/fileComp.csv";

    /**
     *
     */
    public static final String HEADER_CSV_FILE = "id;active;email;emploi;isroot;language;login;nom;password;poste;prenom";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    /**
     *
     */
    public static final String PATTERN_CONCAT_FILE = "%s/%s";

    /**
     *
     */
    public static final String PATTERN_FILECOMP_FILE = "%s/filecomp/%s";
    private String id;
    /**
     * The mail admin.
     */
    private IFileCompDAO fileCompDAO;
    private IFileTypeDAO fileTypeDAO;
    private IUtilisateurDAO utilisateurDAO;
    private ICoreConfiguration configuration;
    private IMgaRecorder mgaRecorderDAO;

    /**
     *
     * @param recorder
     */
    public void setRecorder(IMgaRecorder recorder) {
        this.mgaRecorderDAO = recorder;
    }

    /**
     *
     * @param fileTypeDAO
     */
    public void setFileTypeDAO(IFileTypeDAO fileTypeDAO) {
        this.fileTypeDAO = fileTypeDAO;
    }

    /**
     *
     * @param configuration
     */
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param fileCompDAO
     */
    public void setFileCompDAO(IFileCompDAO fileCompDAO) {
        this.fileCompDAO = fileCompDAO;
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }
    
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        super.createConfig(digester);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#getModuleName()
     */
    @Override
    public String getModuleName() {
        return FileCompDeserialization.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.AbstractDeserialization#getSchemaPath()
     */
    @Override
    public String getSchemaPath() {
        return FileCompDeserialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleDeserialization#isAutoLoad()
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void deSerialize(File inZip) throws BusinessException {
        LOGGER.info("*************************************************************");
        LOGGER.info("*                déserialization fileComp                   *");
        LOGGER.info("*************************************************************");
        LOGGER.info("");
        LOGGER.info(getClass().getName());
        LOGGER.info("");
        LOGGER.info("");
        TransactionStatus tr = utilisateurDAO.beginDefaultTransaction();
        try (InputStream inputStream = openEntry(inZip, FILECOMPS_ENTRY)) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            List<String> attachments = new LinkedList();
            reader.lines()
                    .skip(1)
                    .forEach(line -> registerLine(line, inZip));
            for (String attachment : attachments) {
                writeFile(inZip, attachment);
            }
            utilisateurDAO.commitTransaction(tr);
        } catch (IOException ex) {
            LOGGER.error("can't write fileComp ", ex);
            utilisateurDAO.rollbackTransaction(tr);
        }
    }

    private void registerLine(String line, File inZip) {
        LOGGER.info(line);
        String[] fields = line.split("[\" ]*;[\" ]*");
        FileComp fileComp = null;
        try {
            final String code = fields[1];
            final String createLogin = fields[11];
            final String modifyLogin = fields[8];
            final String fileTypeCode = fields[12];
            LocalDateTime createDate = fields[2] == null || fields[2].equals("null") ? null : DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, fields[2]);
            LocalDate dateDebutPeriode = fields[3] == null || fields[3].equals("null") ? null : DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, fields[3]).toLocalDate();
            LocalDate dateFinPeriode = fields[4] == null || fields[4].equals("null") ? null : DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, fields[4]).toLocalDate();
            LocalDateTime lastModifyDate = fields[7] == null || fields[7].equals("null") ? null : DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, fields[7]);
            Utilisateur createUtilisateur = null;
            Utilisateur modifyUtilisateur = null;
            FileType fileType = null;
            fileComp = fileCompDAO.getByCode(code).orElse(new FileComp());
            createUtilisateur = utilisateurDAO.getByLogin(createLogin).orElseThrow(PersistenceException::new);
            modifyUtilisateur = utilisateurDAO.getByLogin(modifyLogin).orElseThrow(PersistenceException::new);
            fileType = fileTypeDAO.getByCode(fileTypeCode)
                    .orElseThrow(() -> new PersistenceException("can't get file type"));
            fileComp.setCode(code);
            fileComp.setCreateDate(createDate);
            fileComp.setDateDebutPeriode(dateDebutPeriode);
            fileComp.setDateFinPeriode(dateFinPeriode);
            fileComp.setLastModifyDate(lastModifyDate);
            fileComp.setLastModifyUser(modifyUtilisateur);
            fileComp.setCreateUser(createUtilisateur);
            fileComp.setDescription(fields[5]);
            fileComp.setFileName(fields[6]);
            fileComp.setMandatory(Boolean.valueOf(fields[9]));
            fileComp.setSize(Integer.parseInt(fields[10]));
            fileComp.setFileType(fileType);
            String filePath = String.format(PATTERN_CONCAT_FILE, fileComp.getFileType().getCode(), fileComp.getCode());
            fileComp.setData(writeFile(inZip, filePath));
            fileCompDAO.saveOrUpdate(fileComp);
            Group ownGroup = mgaRecorderDAO.createGroup(fileComp.getGroupName(), WhichTree.TREEDATASET, GroupType.FILE_TYPE);
            fileComp.getAllGroups().add(ownGroup);
            fileCompDAO.saveOrUpdate(fileComp);
            fileCompDAO.flush();
        } catch (DateTimeException | PersistenceException | IOException ex) {
            LOGGER.error(String.format("Can't read line %s for fileComp", line), ex);
        }
    }

    private byte[] writeFile(File inZip, String attachment) throws IOException {
        String outFileName = String.format(PATTERN_FILECOMP_FILE, configuration.getRepositoryURI(), attachment);
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(inZip));
                ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            ZipEntry ze = zis.getNextEntry();
            byte[] buffer = new byte[1024];
            while (ze != null) {
                String fileName = ze.getName();
                if (fileName.equals(attachment)) {
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        out.write(buffer, 0, len);
                    }
                    out.close();
                    break;
                }
                ze = zis.getNextEntry();
            }
            return out.toByteArray();
        } catch (Exception e) {
            throw new IOException(String.format("can't write attachment %s in %s", attachment, inZip.getName()), e);
        }
    }
}
