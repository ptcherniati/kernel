package org.inra.ecoinfo.filecomp.deserialization;

/**
 * The Interface IIdSerialization.
 */
public interface IFileCompDeserialization {

    /**
     *
     * @return
     */
    String getId();
}
