package org.inra.ecoinfo.filecomp.impl;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.filecomp.IFileCompDAO;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.IFileTypeDAO;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.filecomp.entity.FileType;
import org.inra.ecoinfo.identification.IUsersDeletion;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationDAO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.DeleteUserException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultFileCompManager extends MO implements IUsersDeletion, IFileCompManager {

    private static final String PATTERN_FILE_NAME_CODE = "%s_%s_%s";
    private static final String PATTERN_DATE_FORMAT_FILE_DD_M_MYY_HHMMSS_SSS_TXT = "YYYYMMdd-hhmmss.SSS";
    private static final String PROPERTY_MSG_FILE_NOT_UPLOADED = "PROPERTY_MSG_FILE_NOT_UPLOADED";
    private static final String PROPERTY_MSG_FILE_NOT_FOUND = "PROPERTY_MSG_FILE_NOT_FOUND";
    private static final String BUNDLE_PATH = "org.inra.ecoinfo.filecomp.messages";
    private static final String PARAM_MSG_NO_RIGHT_FOR_UPDATE = "PARAM_MSG_NO_RIGHT_FOR_UPDATE";
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.security.message";
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultFileCompManager.class);
    private static final Logger FILECOMP_LOGGER = LoggerFactory.getLogger("filecomp.logger");
    /**
     * The Constant FILECOMP @link(String).
     */
    protected static final String FILECOMP = "file";

    /**
     * The Constant PROPERTY_MSG_FOR_SUCCESS_FILCOMP_ANONYMISATION
     * @link(String).
     */
    protected static final String PROPERTY_MSG_FOR_SUCCESS_FILCOMP_ANONYMISATION = "PROPERTY_MSG_FOR_SUCCESS_FILCOMP_ANONYMISATION";
    /**
     * The Constant PROPERTY_MSG_FOR_ECHEC_FILCOMP_ANONYMISATION @link(String).
     */
    protected static final String PROPERTY_MSG_FOR_ECHEC_FILCOMP_ANONYMISATION = "PROPERTY_MSG_FOR_ECHEC_FILCOMP_ANONYMISATION";

    /**
     * The Constant EXTENSION_ZIP @link(String).
     */
    protected static final String EXTENSION_ZIP = ".zip";
    /**
     * The Constant FILE_SEPARATOR @link(String).
     */
    protected static final String FILE_SEPARATOR = System.getProperty("file.separator");
    /**
     * The Constant SEPARATOR_TEXT @link(String).
     */
    protected static final String SEPARATOR_TEXT = "_";

    private IFileCompDAO fileCompDAO;
    private IFileTypeDAO fileTypeDAO;
//    private IAssociateDAO associateDAO;
    private ILocalizationDAO localizationDAO;

    /**
     * The utilisateur dao @link(IUtilisateurDAO).
     */
    protected IUtilisateurDAO utilisateurDAO;
    /**
     * The configuration @link(Configuration).
     */
    protected ICoreConfiguration configuration;

    /**
     *
     */
    protected IFileCompConfiguration filecompConfiguration;
    private IMgaRecorder mgaRecorderDAO;

    /**
     *
     * @param recorder
     */
    public void setRecorder(IMgaRecorder recorder) {
        this.mgaRecorderDAO = recorder;
    }

    /**
     *
     * @param compositeGroup
     * @return
     */
    @Override
    public Set<Long> getAssociatesNodesIds(ICompositeGroup compositeGroup) {
        Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> rolesMap = policyManager.getOrLoadActivities(compositeGroup.getOwnGroup(WhichTree.TREEDATASET), WhichTree.TREEDATASET);
        return rolesMap.containsKey(Activities.associate) ? rolesMap.get(Activities.associate).keySet() : new HashSet();
    }

    /**
     *
     * @param originalFileName
     * @return
     */
    @Override
    public String createUniqueIdForFile(String originalFileName) {
        return String.format(PATTERN_FILE_NAME_CODE,
                originalFileName,
                RandomStringUtils.randomAlphanumeric(8),
                DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.ofInstant(Instant.now(), DateUtil.getLocaleZoneId()), PATTERN_DATE_FORMAT_FILE_DD_M_MYY_HHMMSS_SSS_TXT)
        );
    }

    /**
     * @param code
     * @return @throws org.inra.ecoinfo.utils.exceptions.BusinessException @see
     * org.inra.ecoinfo.extraction.IExtractionManager#doDownload(java.lang.String,
     * java.lang.String)
     */
    @Override
    public byte[] doDownload(final String code) throws BusinessException {
        try {
            FileComp fileComp = fileCompDAO.getByCode(code).orElseThrow(PersistenceException::new);
            if (fileComp == null) {
                throw new BusinessException(String.format(localizationManager.getMessage(BUNDLE_PATH, PROPERTY_MSG_FILE_NOT_FOUND), code));
            }
            return fileComp.getData();
        } catch (PersistenceException e) {
            throw new BusinessException(String.format(localizationManager.getMessage(BUNDLE_PATH, PROPERTY_MSG_FILE_NOT_UPLOADED), code), e);
        }
    }

    /**
     *
     * @return
     */
    public ICoreConfiguration getConfiguration() {
        return configuration;
    }

    /**
     *
     * @param configuration
     */
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FileComp> getFileCompById(Long id) {
        return fileCompDAO.getById(id);
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<FileComp> getFileCompByCode(String code) {
        return fileCompDAO.getByCode(code);
    }

    /**
     *
     * @return
     */
    @Override
    public Utilisateur getUtilisateur() {
        return (Utilisateur) policyManager.getCurrentUser();
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> retrieveListDatesPossible() throws BusinessException {
        final List<LocalDate> datesPossible = fileCompDAO.getListDatesPossible();
        final List<String> datesStringPossible = new LinkedList<>();
        datesStringPossible.add("");
        datesPossible.stream().forEach((datePossible) -> {
            datesStringPossible.add(DateUtil.getUTCDateTextFromLocalDateTime(datePossible.atStartOfDay(), DateUtil.DD_MM_YYYY_FILE));
        });
        return datesStringPossible;
    }

    /**
     *
     * @param nodeIds
     * @param intervalsDate
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<FileComp> getFileCompFromPathesAndIntervalsDate(List<Long> nodeIds, List<IntervalDate> intervalsDate) {
        List<FileComp> returnFiles = new LinkedList();
        List<FileComp> filescomp = fileCompDAO.getFilesWithAssociateForNodeIds(nodeIds);
        restrictFileOnRights(filescomp);
        for (FileComp fileComp : filescomp) {
            if (fileComp.getDateDebutPeriode() == null && fileComp.getDateFinPeriode() == null) {
                returnFiles.add(fileComp);
                continue;
            }
            for (IntervalDate intervalDate : intervalsDate) {
                if (testInterval(fileComp, intervalDate, returnFiles)) {
                    break;
                }
            }
        }
        return returnFiles;
    }

    /**
     *
     * @return
     */
    @Override
    public List<FileType> retrieveListTypesPossible() throws BusinessException {
        return fileTypeDAO.getListTypesPossible();
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> retrieveListUploadersPossible() throws BusinessException {
        return fileCompDAO.getListUploadersPossible();
    }

    @Override
    public Optional<FileType> getFileTypeFromFileTypeCode(String fileTypeCode) {
        return fileTypeDAO.getByCode(Utils.createCodeFromString(fileTypeCode));
    }

    private String saveDescriptionProperties(List<String> descriptions) throws BusinessException {
        for (int i = 1; i < descriptions.size(); i++) {
            if (i >= descriptions.size()) {
                break;
            }
            String localDescription = descriptions.get(i);
            Localization localization = localizationDAO.getByNKey(Localization.getLocalisations().get(i), FileComp.NAME_ENTITY_JPA, FileComp.ENTITY_DESCRIPTION_COLUM_NAME, descriptions.get(0))
                    .orElse(null);

            localization = localization == null ? new Localization(Localization.getLocalisations().get(i), FileComp.NAME_ENTITY_JPA, FileComp.ENTITY_DESCRIPTION_COLUM_NAME, descriptions.get(0), localDescription) : localization;
            try {
                localizationDAO.saveOrUpdate(localization);
            } catch (PersistenceException ex) {
                throw new BusinessException(ex);
            }
        }
        return CollectionUtils.isEmpty(descriptions) ? null : descriptions.get(0);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public FileComp saveFile(FileComp fileComp, List<String> descriptionsList) throws BusinessException {
        try {
            fileComp.setCode(createUniqueIdForFile(fileComp.getFileName()));
            fileComp.setCreateUser(utilisateurDAO.merge((Utilisateur) policyManager.getCurrentUser()));
            String activities = "publication,administration";
            testHasRights("*", activities);
            fileComp = updateFile(fileComp, descriptionsList);
            Group ownGroup = mgaRecorderDAO.createGroup(fileComp.getGroupName(), WhichTree.TREEDATASET, GroupType.FILE_TYPE);
            fileComp.getAllGroups().add(ownGroup);
            fileCompDAO.saveOrUpdate(fileComp);
            fileCompDAO.flush();
            fileComp = fileCompDAO.merge(fileComp);
            return fileComp;
        } catch (PersistenceException ex) {
            LOGGER.debug("Can't save fileComp");
            throw new BusinessException("Can't save fileComp", ex);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public FileComp updateFile(FileComp fileComp, List<String> descriptionsList) throws BusinessException {
        try {
            Optional.ofNullable(policyManager.getCurrentUser())
                    .ifPresent((user) -> {
                        MDC.put("filecomp.user.login", user.getLogin());
                        MDC.put("filecomp.user.email", user.getEmail());
                        MDC.put("filecomp.user.name", user.getNom());
                        MDC.put("filecomp.user.surname", user.getPrenom());
                    });
            MDC.put("filecomp.type", "updatefilecomp");
            MDC.put("filecomp.date", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.YYYY_MM_DD_HHMMSS));
            FileComp dbFileComp = fileCompDAO.getByCode(fileComp.getCode()).orElse(fileComp);
            boolean isOwner = fileComp.getCreateUser().equals(policyManager.getCurrentUser());
            boolean canAdmin = isOwner || policyManager.isRoot();
            String activities = "publication,administration";
            testHasRights("*", activities);
            if (!canAdmin) {
                return dbFileComp;
            }
            dbFileComp.setFileType(fileTypeDAO.merge(fileComp.getFileType()));
            dbFileComp.setDescription(saveDescriptionProperties(descriptionsList));
            dbFileComp.setLastModifyUser(utilisateurDAO.merge((Utilisateur) policyManager.getCurrentUser()));
            dbFileComp.setCreateDate(fileComp.getCreateDate());
            dbFileComp.setCreateUser(utilisateurDAO.merge((Utilisateur) fileComp.getCreateUser()));
            dbFileComp.setData(fileComp.getData());
            dbFileComp.setDateDebutPeriode(fileComp.getDateDebutPeriode());
            dbFileComp.setDateFinPeriode(fileComp.getDateFinPeriode());
            dbFileComp.setFileName(fileComp.getFileName());
            dbFileComp.setLastModifyDate(fileComp.getLastModifyDate());
            dbFileComp.setMandatory(fileComp.getMandatory());
            dbFileComp.setForApplication(fileComp.getForApplication());
            dbFileComp.setSize(fileComp.getSize());
            dbFileComp.setContentType(fileComp.getContentType());
            fileCompDAO.saveOrUpdate(dbFileComp);
            fileCompDAO.flush();
            MDC.put("filecomp.filetype", dbFileComp.getFileType().getCode());
            MDC.put("filecomp.name", dbFileComp.getFileName());
            if (dbFileComp.getDateDebutPeriode() != null) {
                MDC.put("filecomp.startDate", DateUtil.getUTCDateTextFromLocalDateTime(dbFileComp.getDateDebutPeriode().atStartOfDay(), DateUtil.YYYY_MM_DD_HHMMSS));
            }
            if (dbFileComp.getDateFinPeriode() != null) {
                MDC.put("filecomp.endDate", DateUtil.getUTCDateTextFromLocalDateTime(dbFileComp.getDateFinPeriode().atStartOfDay(), DateUtil.YYYY_MM_DD_HHMMSS));
            }
            MDC.put("filecomp.contentType", dbFileComp.getContentType());
            MDC.put("filecomp.size", dbFileComp.getFileSizeAffichage());
            MDC.put("filecomp.mandatory", dbFileComp.getMandatory().toString());
            MDC.put("filecomp.forApplication", dbFileComp.getForApplication().toString());
            return fileCompDAO.merge(dbFileComp);
        } catch (PersistenceException ex) {
            LOGGER.debug("Can't update fileComp");
            MDC.put("filecomp.error", ex.getMessage());
            FILECOMP_LOGGER.info(String.format("%s a updaté  le fichier complémentaire de type %s :%s", MDC.get("filecomp.user.login"), MDC.get("filecomp.filetype"), MDC.get("filecomp.name")));
            throw new BusinessException("Can't update fileComp", ex);
        }
    }

    @Override
    public InfosReport deleteDependancesUserProfile(Utilisateur utilisateur) throws DeleteUserException {
        InfosReport successFileCompReport = new InfosReport("FileComp deletion : ");
        String reportMessage;
        try {
            Utilisateur anonymousUser = utilisateurDAO.merge((Utilisateur) utilisateurDAO.getByLogin(configuration.getAnonymousLogin()).get());
            fileCompDAO.anonymiseUserFiles(utilisateur, anonymousUser);
        } catch (PersistenceException e) {
            reportMessage = String.format(localizationManager.getMessage(BUNDLE_PATH, PROPERTY_MSG_FOR_SUCCESS_FILCOMP_ANONYMISATION), utilisateur.getLogin());
            throw new DeleteUserException(reportMessage);
        }
        reportMessage = String.format(localizationManager.getMessage(BUNDLE_PATH, PROPERTY_MSG_FOR_ECHEC_FILCOMP_ANONYMISATION), utilisateur.getLogin());
        successFileCompReport.addInfos("FileComp", reportMessage);
        return successFileCompReport;
    }

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void removeFile(FileComp fileComp) throws BusinessException {
        try {
            fileCompDAO.remove(fileComp);
        } catch (PersistenceException ex) {
            throw new BusinessException(ex);
        }
    }

    /**
     *
     * @param filecompConfiguration
     */
    public void setFilecompConfiguration(IFileCompConfiguration filecompConfiguration) {
        this.filecompConfiguration = filecompConfiguration;
    }

    /**
     *
     * @param fileCompDAO
     */
    public void setFileCompDAO(IFileCompDAO fileCompDAO) {
        this.fileCompDAO = fileCompDAO;
    }

    /**
     *
     * @param fileTypeDAO
     */
    public void setFileTypeDAO(IFileTypeDAO fileTypeDAO) {
        this.fileTypeDAO = fileTypeDAO;
    }

    /**
     *
     * @param localizationDAO
     */
    public void setLocalizationDAO(ILocalizationDAO localizationDAO) {
        this.localizationDAO = localizationDAO;
    }

    /**
     *
     * @param localizationManager
     */
    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<FileComp> getAllFilesComp() {
        return fileCompDAO.getAll();
    }

    private boolean testInterval(FileComp fileComp, IntervalDate intervalDate, List<FileComp> returnFiles) {
        if (fileComp.getDateDebutPeriode() != null && intervalDate.getEndDate().toLocalDate().isBefore(fileComp.getDateDebutPeriode())) {
            return false;
        } else if (fileComp.getDateFinPeriode() != null && intervalDate.getBeginDate().toLocalDate().isAfter(fileComp.getDateFinPeriode())) {
            return false;
        }
        returnFiles.add(fileComp);
        return true;
    }

    private void testHasRights(String resourcePath, String activity) throws BusinessException {
        if (hasActivity(activity, resourcePath)) {
            return;
        }
        String message = localizationManager.getMessage(BUNDLE_SOURCE_PATH, PARAM_MSG_NO_RIGHT_FOR_UPDATE);
        notificationsManager.addNotification(buildNotification(
                Notification.INFO, message, null, null),
                policyManager.getCurrentUser().getLogin());
        throw new BusinessException(message);
    }

    private boolean hasActivity(String activity, String resourcePath) {
        return policyManager.isRoot() || policyManager.checkCurrentUserActivity(0, activity, resourcePath);
    }

    private void restrictFileOnRights(List<FileComp> filescomp) {
        Iterator<FileComp> itFilecomp = filescomp.iterator();
        while (itFilecomp.hasNext()) {
            final FileComp fileComp = itFilecomp.next();
            final Group ownGroup = fileComp.getOwnGroup(WhichTree.TREEDATASET);
            Map<Long, Map<Group, List<LocalDate>>> roles = policyManager.getOrLoadActivities(ownGroup, WhichTree.TREEDATASET).get(Activities.associate);
            boolean valid = false;
            for (Map.Entry<Long, Map<Group, List<LocalDate>>> entry : roles.entrySet()) {
                List<LocalDate> value = entry.getValue().get(ownGroup);
                final LocalDate beginRight = value.get(0);
                final LocalDate endRight = value.get(1);
                if (beginRight == null && endRight == null) {
                    valid = true;
                    break;
                }
                if (beginRight != null && fileComp.getDateFinPeriode() != null && beginRight.isAfter(fileComp.getDateFinPeriode())) {
                    continue;
                }
                if (endRight != null && fileComp.getDateDebutPeriode() != null && endRight.isBefore(fileComp.getDateDebutPeriode())) {
                    continue;
                }
                valid = true;
                break;
            }
            if (!valid) {
                itFilecomp.remove();
            }
        }

    }

    @Override
    @Transactional(readOnly = true)
    public List<FileComp> getFileCompForApplication() {
        return fileCompDAO.getFileCompForApplication();
    }

}
