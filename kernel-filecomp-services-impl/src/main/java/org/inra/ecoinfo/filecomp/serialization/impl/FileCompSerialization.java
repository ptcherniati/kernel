package org.inra.ecoinfo.filecomp.serialization.impl;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.filecomp.IFileCompDAO;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.filecomp.serialization.IFileCompSerialization;
import org.inra.ecoinfo.serialization.AbstractSerialization;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MailAdminSerialization.
 */
public class FileCompSerialization extends AbstractSerialization implements IFileCompSerialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/fileCompSerialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "fileCompSerialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(FileCompSerialization.class.getName());
    private static final String FILECOMPS_ENTRY = "filecomp/fileComp.csv";

    /**
     *
     */
    public static final String HEADER_CSV_FILE = "id;code;createDate;dateDebutPeriode;dateFinPeriode;description;fileName;lastModifyDate;lastModifyUser;mandatory;size;createUser;fileType";

    /**
     *
     */
    public static final String HEADER_ASSOCIATES_CSV_FILE = "id;path;fileCompcode";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    /**
     *
     */
    public static final String LINE_CSV_ASSOCIATE__FILE = "%n%s;%s;%s";

    /**
     *
     */
    public static final String INFORMATION_FILECOMP_PATTERN = "filecomp/filecomp.csv";

    /**
     *
     */
    public static final String FILECOMP_PATTERN = "filecomp/%s/%s";

    /**
     *
     */
    public static final String FILECOMP_PATTERN_FOR_TMP = "%s/filecomp/";
    private String id;
    /**
     * The mail admin.
     */
    private IFileCompDAO fileCompDAO;
    private ICoreConfiguration configuration;

    /**
     *
     * @param configuration
     */
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param fileCompDAO
     */
    public void setFileCompDAO(IFileCompDAO fileCompDAO) {
        this.fileCompDAO = fileCompDAO;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#createConfig(org.apache.commons.digester.Digester)
     */
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("repositoryURI", "setRepositoryURI", 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#getModuleName()
     */
    /**
     *
     * @return
     */
    @Override
    public String getModuleName() {
        return FileCompSerialization.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.AbstractSerialization#getSchemaPath()
     */
    /**
     *
     * @return
     */
    @Override
    public String getSchemaPath() {
        return FileCompSerialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#isAutoLoad()
     */
    /**
     *
     * @return
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param outZip
     * @throws BusinessException
     */
    @Override
    public void serialize(ZipOutputStream outZip) throws BusinessException {
        List<FileComp> fileComps = null;
        List<String> lines = fileCompDAO.getAll(FileComp.class).stream()
                .map(fileComp -> {
                    try {
                        return readLine(outZip, fileComp);
                    } catch (IOException ex) {
                        LOGGER.error(String.format("Can't zip filecomp %s/%s", fileComp.getFileType().getCode(), fileComp.getFileName()));
                        return (String) null;
                    }
                })
                .collect(Collectors.toList());
        ZipEntry entry = new ZipEntry(FILECOMPS_ENTRY);
        try {
            outZip.putNextEntry(entry);
            lines.stream()
                    .filter(line -> line != null)
                    .forEach(line -> {
                        try {
                            outZip.write(line.getBytes());
                        } catch (IOException ex) {
                            LOGGER.error(String.format("Can't write filecomp information %s", line));
                        }
                    });
            outZip.closeEntry();
        } catch (IOException e) {
            LOGGER.error("Can't write filecomp information ");
        }
    }

    /**
     *
     * @param outZip
     * @param fileComp
     * @return
     * @throws IOException
     */
    public String readLine(ZipOutputStream outZip, FileComp fileComp) throws IOException {
        String line = String.format(LINE_CSV_FILE,
                fileComp.getId(),
                fileComp.getCode(),
                DateUtil.getUTCDateTextFromLocalDateTime(fileComp.getCreateDate(), DateUtil.DD_MM_YYYY_HH_MM),
                Optional.ofNullable(fileComp.getDateDebutPeriode()).map(d -> DateUtil.getUTCDateTextFromLocalDateTime(d.atStartOfDay(), DateUtil.DD_MM_YYYY_HH_MM)).orElse(null),
                Optional.ofNullable(fileComp.getDateDebutPeriode()).map(f -> DateUtil.getUTCDateTextFromLocalDateTime(f.atStartOfDay(), DateUtil.DD_MM_YYYY_HH_MM)).orElse(null),
                fileComp.getDescription(),
                fileComp.getFileName(),
                Optional.ofNullable(fileComp.getDateDebutPeriode()).map(m -> DateUtil.getUTCDateTextFromLocalDateTime(m, DateUtil.DD_MM_YYYY_HH_MM)).orElse(null),
                fileComp.getLastModifyUser().getLogin(),
                fileComp.getMandatory(),
                fileComp.getSize(),
                fileComp.getCreateUser().getLogin(),
                fileComp.getFileType().getCode()
        );
        byte[] data = fileCompDAO.getById(fileComp.getId())
                .orElseThrow(() -> new IOException(String.format("can't register attachment %s in %s", fileComp.toString()))).getData();
        registerAttachment(outZip, fileComp.getAbsoluteFilePath(String.format(FILECOMP_PATTERN_FOR_TMP, configuration.getRepositoryURI())), fileComp, data);
        return line;
    }

    private void registerAttachment(ZipOutputStream outZip, String filePath, FileComp fileComp, byte[] data) throws IOException {
        ZipEntry attachmentFile = new ZipEntry(String.format(FILECOMP_PATTERN, fileComp.getFileType().getCode(), fileComp.getCode()));
        outZip.putNextEntry(attachmentFile);
        try {
            outZip.write(data);
        } catch (Exception e) {
            throw new IOException(String.format("can't register attachment %s in %s", fileComp.toString(), filePath), e);
        }
    }
}
