package org.inra.ecoinfo.filecomp.jpa;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.EntityGraph;
import javax.persistence.FlushModeType;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import org.hibernate.Session;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.filecomp.IFileCompDAO;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.filecomp.entity.FileComp_;
import org.inra.ecoinfo.filecomp.entity.FileType_;
import org.inra.ecoinfo.filecomp.entity.GroupFileComp;
import org.inra.ecoinfo.filecomp.entity.GroupFileComp_;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.entity.Utilisateur_;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAFileCompDAO extends AbstractJPADAO<FileComp> implements IFileCompDAO {

    private static final String QUERY_SELECT_FILES_WITH_ASSOCIATES_FOR_NODES_IDS = "select * "
            + "    from%n"
            + "        " + GroupFileComp.TABLE_NAME + " gfc "
            + " join " + Group.tableName + " g on g." + Group.ID_JPA + "=gfc." + GroupFileComp.GROUP_FIELD_NAME + " "
            + " join " + FileComp.NAME_ENTITY_JPA + " fc on fc." + FileComp.KEY_ID + "=gfc." + GroupFileComp.FILE_COMP_FIELD_NAME + " "
            + "    where%n"
            + "        (%n"
            + "            'file_'||fc." + FileComp.FILE_CODE + "%n"
            + "        )=g." + Group.JPA_FIELD_GROUP_NAME + " %n"
            + "        and (%n"
            + "            g.activityAsString similar to %s%n"
            + "        )";
    private static final String SQL_PATTERN_SIMILAR = "'%%(%s)%%'";
    private static final String PATTERN_JOIN = "|";
    private static final String PATTERN_WORD = "\"%s\":";


    @Override
    public void remove(FileComp fileComp) throws PersistenceException {
        removeGroupOfUser(fileComp, fileComp.getOwnGroup(WhichTree.TREEDATASET));
        remove(entityManager.merge(fileComp.getOwnGroup(WhichTree.TREEDATASET)));
        CriteriaDelete<FileComp> delete = builder.createCriteriaDelete(FileComp.class);
        Root<FileComp> fc = delete.from(FileComp.class);
        delete.where(builder.equal(fc, fileComp));
        entityManager.createQuery(delete).executeUpdate();
        ((Session) entityManager.getDelegate()).clear();
    }

    /**
     *
     * @return
     */
    @Override
    public List<FileComp> getAll() {
        CriteriaQuery<FileComp> query = builder.createQuery(FileComp.class);
        Root<FileComp> fileComp = query.from(FileComp.class);
        query.select(fileComp);
        final List<FileComp> resultList = entityManager.createQuery(query).setFlushMode(FlushModeType.COMMIT).getResultList();
        return resultList;
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<FileComp> getByCode(String code) {
        CriteriaQuery<FileComp> query = builder.createQuery(FileComp.class);
        Root<FileComp> fc = query.from(FileComp.class);
        query.where(builder.equal(fc.get(FileComp_.code), code));
        query.select(fc);
        return entityManager.createQuery(query).setFlushMode(FlushModeType.COMMIT).getResultList().stream().findFirst();
    }

    /**
     *
     * @return
     */
    @Override
    public List<LocalDate> getListDatesPossible() {
        CriteriaQuery<LocalDateTime> query = builder.createQuery(LocalDateTime.class);
        Root<FileComp> fc = query.from(FileComp.class);
        query.select(fc.get(FileComp_.createDate));
        query.distinct(true);
        return getResultList(query).stream()
                .map(d -> d.toLocalDate())
                .collect(Collectors.toList());
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> getListTypesPossible() {
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<FileComp> fc = query.from(FileComp.class);
        query.select(fc.join(FileComp_.fileType).get(FileType_.name));
        query.distinct(true);
        return getResultList(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> getListUploadersPossible() {
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<FileComp> fc = query.from(FileComp.class);
        query.select(fc.join(FileComp_.createUser).get(Utilisateur_.nom));
        query.distinct(true);
        return getResultList(query);
    }

    /**
     *
     * @param beginDate
     * @param endDate
     * @return
     */
    @Override
    public List<FileComp> retrieveAvailablesFiles(LocalDate beginDate, LocalDate endDate) {
        CriteriaQuery<FileComp> query = builder.createQuery(FileComp.class);
        Root<FileComp> f = query.from(FileComp.class);
        query.where(builder.and(
                builder.or(
                        builder.isNull(f.<LocalDate>get(FileComp_.dateDebutPeriode)),
                        builder.lessThan(f.<LocalDate>get(FileComp_.dateDebutPeriode), endDate)
                ),
                builder.or(
                        builder.isNull(f.<LocalDate>get(FileComp_.dateFinPeriode)),
                        builder.greaterThan(f.<LocalDate>get(FileComp_.dateFinPeriode), beginDate)
                )
        ));
        query.select(f);
        query.distinct(true);
        return entityManager.createQuery(query).setFlushMode(FlushModeType.COMMIT).getResultList();
    }

    /**
     *
     * @param nodeIds
     * @return
     */
    @Override
    public List<FileComp> getFilesWithAssociateForNodeIds(List<Long> nodeIds
    ) {
        try {
            final String sql = QUERY_SELECT_FILES_WITH_ASSOCIATES_FOR_NODES_IDS;
            String args = nodeIds.stream().map(t -> String.format(PATTERN_WORD, t)).collect(Collectors.joining(PATTERN_JOIN));
            args = String.format(SQL_PATTERN_SIMILAR, args);
            final Query query = entityManager.createNativeQuery(String.format(sql, args), FileComp.class);
            return query.getResultList();
        } catch (final Exception e) {
            LOGGER.debug(e.getMessage(), e);
            return new LinkedList();
        }
    }

    @Override
    public <T extends ICompositeGroup> List<T> retrieveAllUsers() {
        return getAll().stream().map(t -> (T) t).collect(Collectors.toList());
    }

    @Override
    public List<ICompositeGroup> retrieveAllUsersOrGroups(WhichTree whichTree) {
        CriteriaQuery<ICompositeGroup> query = builder.createQuery(ICompositeGroup.class);
        Root<FileComp> f = query.from(FileComp.class);
        query.select(f);
        List<ICompositeGroup> compositeGroups = getResultListToStream(query)
                .map(k -> k.getOwnGroup(WhichTree.TREEDATASET))
                .collect(Collectors.toList());
        return compositeGroups;
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Group, Boolean> getAllPossiblesGroupUser(IUser user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     *
     * @param <T>
     * @param user
     * @param group
     * @return
     */
    @Override
    public <T extends ICompositeGroup> Optional<T> addGroupToUser(T user, Group group) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param <T>
     * @param user
     * @param group
     * @return
     */
    @Override
    public <T extends ICompositeGroup> Optional<T> removeGroupOfUser(T user, Group group) {
        FileComp fileComp = (FileComp) user;
        getGroupUtilisateur(fileComp, group).ifPresent(gu -> entityManager.remove(gu));
        try {
            fileComp = merge(fileComp);
        } catch (PersistenceException ex) {
            LOGGER.error("can't merge user", ex);
        }
        return Optional.ofNullable((T) fileComp);
    }

    Optional<GroupFileComp> getGroupUtilisateur(FileComp fileComp, Group group) {
        CriteriaQuery<GroupFileComp> query = builder.createQuery(GroupFileComp.class);
        Root<GroupFileComp> gu = query.from(GroupFileComp.class);
        query.where(
                builder.equal(gu.join(GroupFileComp_.group), group),
                builder.equal(gu.join(GroupFileComp_.fileComp), fileComp)
        );
        query.select(gu);
        return getOptional(query);
    }

    /**
     *
     * @param group
     */
    @Override
    public void remove(ICompositeGroup group) {
        CriteriaDelete<GroupFileComp> delete = builder.createCriteriaDelete(GroupFileComp.class);
        Root<GroupFileComp> gu = delete.from(GroupFileComp.class);
        delete.where(builder.equal(gu.get(GroupFileComp_.group), group));
        entityManager.createQuery(delete).executeUpdate();
        entityManager.remove(group);

    }

    @Override
    public void anonymiseUserFiles( Utilisateur utilisateur, Utilisateur anonymousUser) throws PersistenceException {
        
        CriteriaUpdate<FileComp> update = builder.createCriteriaUpdate(FileComp.class);
        Root<FileComp> file = update.from(FileComp.class);
        
        update.where(builder.equal(file.get(FileComp_.createUser), utilisateur))
                .set(file.get(FileComp_.createUser), anonymousUser);
        update(update);
        
        file = update.from(FileComp.class);
        update.set(file.get(FileComp_.lastModifyUser),anonymousUser);
        update.where(builder.equal(file.get(FileComp_.lastModifyUser), utilisateur));
        update(update);

        /*CriteriaUpdate<FileComp> update = builder.createCriteriaUpdate(FileComp.class);
        Root<FileComp> file = update.from(FileComp.class);

        Subquery subqueryLastModifyUser = update.subquery(FileComp.class);
        subqueryLastModifyUser.select(builder.selectCase()
                .when(builder.equal(file.get(FileComp_.lastModifyUser), utilisateur), utilisateurDAO.merge((Utilisateur) utilisateurDAO.getByLogin(configuration.getAnonymousLogin()).get()))
                .otherwise(utilisateurDAO.merge(utilisateur)));
        
        Subquery subqueryCreateUser = update.subquery(FileComp.class);
        subqueryCreateUser.select(builder.selectCase()
                .when(builder.equal(file.get(FileComp_.createUser), utilisateur), utilisateurDAO.merge((Utilisateur) utilisateurDAO.getByLogin(configuration.getAnonymousLogin()).get()))
                .otherwise(utilisateurDAO.merge(utilisateur)));

        update.set(file.get(FileComp_.lastModifyUser), file.get(FileComp_.lastModifyUser).value(subqueryLastModifyUser));
        update.set(file.get(FileComp_.createUser), utilisateurDAO.merge((Utilisateur) utilisateurDAO.getByLogin(configuration.getAnonymousLogin()).get()));
        */

    }

    /**
     *
     * @return
     */
    @Override
    public List<FileComp> getFileCompForApplication() {
        CriteriaQuery<FileComp> query = builder.createQuery(FileComp.class);
        Root<FileComp> fileComp = query.from(FileComp.class);
        query.where(builder.equal(fileComp.get(FileComp_.forApplication), true));
        query.select(fileComp);
        final List<FileComp> resultList = entityManager.createQuery(query).setFlushMode(FlushModeType.COMMIT).getResultList();
        return resultList;
    }

    /**
     *
     * @param fileCompId
     * @return
     */
    @Override
    public Optional<FileComp> getById(Long fileCompId) {
        if (fileCompId == null) {
            return Optional.empty();
        }
        EntityGraph<FileComp> fileCompGraph = (EntityGraph<FileComp>) entityManager.getEntityGraph(FileComp.COMPLETE_GRAPH);
        Map hints = new HashMap();
        hints.put("javax.persistence.fetchgraph", fileCompGraph);
        return Optional.ofNullable(entityManager.find(FileComp.class, fileCompId, hints));
    }
}
