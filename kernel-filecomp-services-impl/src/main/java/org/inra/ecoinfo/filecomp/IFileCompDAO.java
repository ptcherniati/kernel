package org.inra.ecoinfo.filecomp;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.middleware.ICompositeGroupDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IFileCompDAO extends IDAO<FileComp>, ICompositeGroupDAO {

    /**
     *
     * @return
     */
    List<FileComp> getAll();

    /**
     *
     * @param code
     * @return
     */
    Optional<FileComp> getByCode(String code);

    /**
     *
     * @param fileCompId
     * @return
     */
    Optional<FileComp> getById(Long fileCompId);

    /**
     *
     * @return
     */
    List<LocalDate> getListDatesPossible();

    /**
     *
     * @return
     */
    List<String> getListTypesPossible();

    /**
     *
     * @return
     */
    List<String> getListUploadersPossible();

    /**
     *
     * @param beginDate
     * @param endDate
     * @return
     */
    List<FileComp> retrieveAvailablesFiles(LocalDate beginDate, LocalDate endDate);

    /**
     *
     * @param nodeIds
     * @return
     */
    List<FileComp> getFilesWithAssociateForNodeIds(List<Long> nodeIds);

    /**
     *
     * @return
     */
    List<FileComp> getFileCompForApplication();
    
    /**
     *
     * @param utilisateur
     * @param anonymousUser
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     * 
     */
    void anonymiseUserFiles(Utilisateur utilisateur, Utilisateur anonymousUser) throws PersistenceException;
}
