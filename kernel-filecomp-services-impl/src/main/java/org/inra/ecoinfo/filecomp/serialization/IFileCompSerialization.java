package org.inra.ecoinfo.filecomp.serialization;

/**
 * The Interface IIdSerialization.
 */
public interface IFileCompSerialization {

    /**
     *
     * @return
     */
    String getId();
}
