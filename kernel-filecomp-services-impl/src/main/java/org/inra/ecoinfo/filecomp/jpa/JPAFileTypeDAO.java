package org.inra.ecoinfo.filecomp.jpa;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.filecomp.IFileTypeDAO;
import org.inra.ecoinfo.filecomp.entity.FileType;
import org.inra.ecoinfo.filecomp.entity.FileType_;

/**
 *
 * @author ptcherniati
 */
public class JPAFileTypeDAO extends AbstractJPADAO<FileType> implements IFileTypeDAO {

    /**
     * @param code
     * @return
     * @see org.inra.ecoinfo.filecomp.IFileTypeDAO#getByCode(java.lang.String)
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Optional<FileType> getByCode(String code) {
        CriteriaQuery<FileType> query = builder.createQuery(FileType.class);
        Root<FileType> ft = query.from(FileType.class);
        query.where(builder.equal(ft.get(FileType_.code), code));
        query.select(ft);
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<FileType> getListTypesPossible() {
        CriteriaQuery<FileType> query = builder.createQuery(FileType.class);
        Root<FileType> ft = query.from(FileType.class);
        query.select(ft);
        query.distinct(true);
        return getResultList(query);
    }
}
