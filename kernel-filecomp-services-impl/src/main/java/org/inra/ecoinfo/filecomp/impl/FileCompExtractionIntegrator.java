/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.filecomp.impl;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 *
 * @author tcherniatinsky
 */
public class FileCompExtractionIntegrator extends AbstractIntegrator<FileComp> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileCompExtractionIntegrator.class);

    /**
     *
     */
    public static final String LOGGER_ERROR = "Method %s -> %s";

    private static final String TMPTEMPORARYFILECOMPSS = "/tmp/temporaryfilecomp/%s/%s";
    private IFileCompManager fileCompManager;

    /**
     *
     * @return
     */
    @Override
    public String getMapEntryKey() {
        return FileComp.class.getSimpleName();
    }

    /**
     *
     * @param zipOutputStream
     * @param filesComp
     * @throws IOException
     */
    @Override
    public void embed(ZipOutputStream zipOutputStream, Collection<FileComp> filesComp) throws IOException {
        Set<FileComp> files = new HashSet();
        files.addAll(Optional.ofNullable(filesComp).orElse(new HashSet()));
        files.addAll(fileCompManager.getFileCompForApplication());
        if (!CollectionUtils.isEmpty(files)) {
            Map<String, File> fileCompMap = files.stream()
                    .map(FileCompEntry::new)
                    .filter(
                            fileCompEntry -> fileCompEntry.file.map(file->file.exists() && file.canRead()).orElse(false)
                    )
                    .collect(
                            Collectors.toMap(FileCompEntry::getFileCompZipEntry, FileCompEntry::getFile)
                    );
            String filesNames = fileCompMap.keySet().stream()
                    .collect(Collectors.joining(","));
            MDC.put("filecomp", filesNames);
            embedInZip(zipOutputStream, fileCompMap);
        }
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    class FileCompEntry {

        Optional<File> file = Optional.empty();
        FileComp fileComp;

        public FileCompEntry(FileComp fileComp) {
            this.fileComp = fileComp;
            getTmpFileFromFilecomp();
        }

        private void getTmpFileFromFilecomp() {
            file = Optional.ofNullable(FileUtils.getFile(String.format(TMPTEMPORARYFILECOMPSS, fileComp.getCode(), fileComp.getFileName())));
            try {
                if (file.isPresent()) {
                    FileUtils.writeByteArrayToFile(file.get(), fileComp.getData());
                }
            } catch (IOException ex) {
            LOGGER.error(String.format(LOGGER_ERROR, "getTmpFileFromFilecomp", ex.getMessage()), ex);
            }
        }

        public File getFile() {
            return file.orElse(null);
        }

        public String getFileCompZipEntry() {
            return String.format("complementariesFiles/%s/%s",fileComp.getFileType().getCode(), fileComp.getFileName());
        }
    }

}
