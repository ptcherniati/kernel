package org.inra.ecoinfo.filecomp.config.impl;

import java.io.IOException;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.AbstractConfiguration;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.filecomp.IFileCompDAO;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class FilecompConfiguration.
 */
public class FileCompConfiguration extends AbstractConfiguration implements IFileCompConfiguration {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/fileCompConfiguration.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "fileCompConfiguration";
    private static final String REPOSITORY_PATTERN = "%s%s";
    private ICoreConfiguration coreConfiguration;
    private IFileCompDAO fileCompDAO;
    private IMgaRecorder mgaRecorderDAO;

    private String folder = "filecomp";

    private String repositoryFiles;

    /**
     * Instantiates a new dataset configuration.
     */
    public FileCompConfiguration() {
        super();
    }

    /**
     *
     * @param coreConfiguration
     */
    public void setCoreConfiguration(ICoreConfiguration coreConfiguration) {
        this.coreConfiguration = coreConfiguration;
    }

    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("configuration/module/fileCompConfiguration/folder", "setFolder", 0);
    }

    @Override
    public String getModuleName() {
        return FileCompConfiguration.MODULE_NAME;
    }

    @Override
    public String getSchemaPath() {
        return FileCompConfiguration.SCHEMA_PATH;
    }

    @Override
    public Boolean isAutoLoad() {
        return true;
    }

    /**
     * Post config.
     *
     * @throws BusinessException the business exception
     */
    @Override
    public void postConfig() throws BusinessException {
        try {
            createRepositoryIfNeeded();
            mgaRecorderDAO.setUserDao(fileCompDAO, AbstractMgaIOConfigurator.ASSOCIATE_CONFIGURATION);
        } catch (IOException e) {
            throw new BusinessException("can't create filecomp repository", e);
        }
        
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration#setFolder(java.lang.String)
     */
    /**
     *
     * @param folder
     */
    @Override
    public void setFolder(String folder) {
        this.folder = folder;
    }

    /**
     *
     * @return
     */
    @Override
    public String getRepositoryFiles() {
        return repositoryFiles;
    }

    private void createRepositoryIfNeeded() throws IOException {
        repositoryFiles = String.format(REPOSITORY_PATTERN, coreConfiguration.getRepositoryURI(), folder);
        FileWithFolderCreator.createFile(repositoryFiles + "/.");
    }

    /**
     *
     * @param fileCompDAO
     */
    public void setFileCompDAO(IFileCompDAO fileCompDAO) {
        this.fileCompDAO = fileCompDAO;
    }

    /**
     *
     * @param mgaRecorderDAO
     */
    public void setMgaRecorderDAO(IMgaRecorder mgaRecorderDAO) {
        this.mgaRecorderDAO = mgaRecorderDAO;
    }
}
