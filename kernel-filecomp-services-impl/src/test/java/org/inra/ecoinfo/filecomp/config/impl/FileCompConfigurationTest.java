package org.inra.ecoinfo.filecomp.config.impl;

import com.sun.faces.config.ConfigurationException;
import java.io.File;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class ConfigurationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
public class FileCompConfigurationTest {

    @Autowired
    IFileCompConfiguration fileCompConfiguration;

    @Autowired
    ICoreConfiguration coreConfiguration;

    /**
     * Test.
     *
     * @throws ConfigurationException the configuration exception
     */
    @Test
    public void test() {
        String filesRepository = coreConfiguration.getRepositoryURI() + "filecomp";
        Assert.assertEquals("le fichier de dépôt des fichiers complémentaires est invalide", filesRepository, fileCompConfiguration.getRepositoryFiles());
        Assert.assertTrue(new File(filesRepository).exists());
    }

    /**
     *
     * @param fileCompConfiguration
     */
    public void setFileCompConfiguration(IFileCompConfiguration fileCompConfiguration) {
        this.fileCompConfiguration = fileCompConfiguration;
    }

    /**
     *
     * @param coreConfiguration
     */
    public void setCoreConfiguration(ICoreConfiguration coreConfiguration) {
        this.coreConfiguration = coreConfiguration;
    }
}
