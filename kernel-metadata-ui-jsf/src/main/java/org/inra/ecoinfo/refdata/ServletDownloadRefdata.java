package org.inra.ecoinfo.refdata;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.inra.ecoinfo.utils.StreamUtils;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Servlet implementation class ServletRetrieveVariableGraphic.
 */
@WebServlet(value = "/downloadRefdata", name = "Servlet download reference data")
public class ServletDownloadRefdata extends HttpServlet {

    /**
     * The Constant PARAMETER_REQUEST_REFDATACODE @link(String).
     */
    private static final String PARAMETER_REQUEST_REFDATACODE = "refdatacode";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new servlet download refdata.
     *
     * @see HttpServlet#HttpServlet()
     */
    public ServletDownloadRefdata() {
        super();
    }

    /**
     * Do get.
     *
     * @param request
     * @link(HttpServletRequest) the request
     * @param response
     * @link(HttpServletResponse) the response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred. @see
     * HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final ServletContext servletContext = this.getServletContext();
        final WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        final IMetadataManager metadataManager = (IMetadataManager) applicationContext.getBean(IMetadataManager.ID_BEAN);
        final String refdataName = request.getParameter(ServletDownloadRefdata.PARAMETER_REQUEST_REFDATACODE);
        try {
            final byte[] datas = metadataManager.buildModelGridMetadata(refdataName).formatAsCSV().getBytes(Utils.ENCODING_ISO_8859_15);
            response.setContentType("application/octet-stream");
            response.setContentLength(datas.length);
            response.setHeader("Content-Disposition", String.format("attachment; filename=%s", refdataName.replaceAll(",", "_").concat(Utils.EXTENSION_FILE_CSV)));
            response.getOutputStream().write(datas);
            response.getOutputStream().flush();
            StreamUtils.closeStream(response.getOutputStream());
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in ServletDownloadRefdata", e);
        }
    }

    /**
     * Do post.
     *
     * @param request
     * @link(HttpServletRequest) the request
     * @param response
     * @link(HttpServletResponse) the response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred. @see
     * HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
