package org.inra.ecoinfo.refdata.jsf;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.Transient;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.refdata.security.ISecurityRefdataManager;
import org.inra.ecoinfo.refdata.security.PermissionRefdata;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class UIBeanSecurityRefdata.
 */
@ManagedBean(name = "uiSecurityRefdata")
@ViewScoped
public class UIBeanSecurityRefdata implements Serializable {

    /**
     * The Constant RIGHT_REFDATA_ADMINISTRATION @link(String).
     */
    private static final String RIGHT_REFDATA_ADMINISTRATION = "administration";
    /**
     * The Constant RIGHT_REFDATA_DELETE @link(String).
     */
    private static final String RIGHT_REFDATA_DELETE = "delete";
    /**
     * The Constant RIGHT_REFDATA_DOWNLOAD @link(String).
     */
    private static final String RIGHT_REFDATA_DOWNLOAD = "download";
    /**
     * The Constant RIGHT_REFDATA_EDITION @link(String).
     */
    private static final String RIGHT_REFDATA_EDITION = "edition";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The check all administration @link(Boolean).
     */
    private Boolean checkAllAdministration;
    /**
     * The check all delete @link(Boolean).
     */
    private Boolean checkAllDelete;
    /**
     * The check all download @link(Boolean).
     */
    private Boolean checkAllDownload;
    /**
     * The check all edition @link(Boolean).
     */
    private Boolean checkAllEdition;
    /**
     * The current utilisateur @link(Utilisateur).
     */
    private Utilisateur currentUtilisateur;
    /**
     * The current updatedStatus @link(Boolean).
     */
    private Boolean updatedStatus = false;
    /**
     * The filter nom utilisateur @link(String).
     */
    private String filterNomUtilisateur;
    /**
     * The permission refdata edited @link(PermissionRefdata).
     */
    @Transient
    private final PermissionRefdata permissionRefdataEdited = new PermissionRefdata();
    /**
     * The permissions refdatas user @link(List<PermissionRefdata>).
     */
    @Transient
    private List<PermissionRefdata> permissionsRefdatasUser;
    /**
     * The security refdata manager @link(ISecurityRefdataManager).
     */
    @ManagedProperty(value = "#{securityRefdataManager}")
    private ISecurityRefdataManager securityRefdataManager;
    /**
     * The type selection all @link(String).
     */
    private String typeSelectionAll;
    /**
     * The utilisateurs @link(List<Utilisateur>).
     */
    private List<Utilisateur> utilisateurs;
    /**
     * The security context @link(IPolicyManager).
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;
    /**
     * The users manager @link(IUsersManager).
     */
    @ManagedProperty(value = "#{usersManager}")
    protected IUsersManager usersManager;

    /**
     * Instantiates a new uI bean security refdata.
     */
    public UIBeanSecurityRefdata() {
    }

    /**
     * Check all.
     *
     * @return the string
     */
    public String checkAll() {
        switch (typeSelectionAll) {
            case UIBeanSecurityRefdata.RIGHT_REFDATA_DOWNLOAD:
                permissionsRefdatasUser.stream().forEach((permissionRefdata) -> {
                    permissionRefdata.setDownload(checkAllDownload);
                });
                break;
            case UIBeanSecurityRefdata.RIGHT_REFDATA_EDITION:
                permissionsRefdatasUser.stream().forEach((permissionRefdata) -> {
                    permissionRefdata.setEdition(checkAllEdition);
                });
                break;
            case UIBeanSecurityRefdata.RIGHT_REFDATA_DELETE:
                permissionsRefdatasUser.stream().forEach((permissionRefdata) -> {
                    permissionRefdata.setDelete(checkAllDelete);
                });
                break;
            case UIBeanSecurityRefdata.RIGHT_REFDATA_ADMINISTRATION:
                permissionsRefdatasUser.stream().forEach((permissionRefdata) -> {
                    permissionRefdata.setAdministration(checkAllAdministration);
                });
                break;
            default:
        }
        updatedStatus = true;
        return null;
    }

    /**
     * Gets the check all administration.
     *
     * @return the check all administration
     */
    public Boolean getCheckAllAdministration() {
        return checkAllAdministration;
    }

    /**
     * Gets the check all delete.
     *
     * @return the check all delete
     */
    public Boolean getCheckAllDelete() {
        return checkAllDelete;
    }

    /**
     * Gets the check all download.
     *
     * @return the check all download
     */
    public Boolean getCheckAllDownload() {
        return checkAllDownload;
    }

    /**
     * Gets the check all edition.
     *
     * @return the check all edition
     */
    public Boolean getCheckAllEdition() {
        return checkAllEdition;
    }

    /**
     * Gets the current utilisateur.
     *
     * @return the current utilisateur
     */
    public Utilisateur getCurrentUtilisateur() {
        return currentUtilisateur;
    }

    /**
     * Gets the filter nom utilisateur.
     *
     * @return the filter nom utilisateur
     */
    public String getFilterNomUtilisateur() {
        return filterNomUtilisateur;
    }

    /**
     * Gets the permission refdata edited.
     *
     * @return the permission refdata edited
     */
    public PermissionRefdata getPermissionRefdataEdited() {
        return permissionRefdataEdited;
    }

    /**
     * Gets the permissions refdatas user.
     *
     * @return the permissions refdatas user
     */
    public List<PermissionRefdata> getPermissionsRefdatasUser() {
        if (currentUtilisateur == null) {
            try {
                permissionsRefdatasUser = securityRefdataManager.getRestrictedPermissionsByUserId(policyManager.getCurrentUser().getId());
            } catch (final BusinessException e) {
                UncatchedExceptionLogger.logUncatchedException("uncatched exception in getPermissionsRefdatasUser", e);
            }
        }
        return permissionsRefdatasUser;
    }

    /**
     * Gets the type selection all.
     *
     * @return the type selection all
     */
    public String getTypeSelectionAll() {
        return typeSelectionAll;
    }

    /**
     * Gets the updated status.
     *
     * @return the updated status
     */
    public Boolean getUpdatedStatus() {
        return updatedStatus;
    }

    /**
     * Gets the utilisateurs.
     *
     * @return the utilisateurs
     * @throws BusinessException the business exception
     */
    public List<Utilisateur> getUtilisateurs() throws BusinessException {
        if (utilisateurs == null) {
            utilisateurs = usersManager.retrieveAllUsers();
        }
        return utilisateurs;
    }

    /**
     * Inits the all check all checkboxs.
     */
    private void initAllCheckAllCheckboxs() {
        checkAllDownload = false;
        checkAllEdition = false;
        checkAllAdministration = false;
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public String navigate() {
        return "securityRefdata";
    }

    /**
     * Retrieve permission refdata by resource.
     *
     * @param codeResource the code resource
     * @return the permission refdata
     * @link(String) the code resource
     */
    private PermissionRefdata retrievePermissionRefdataByResource(final String codeResource) {
        for (final PermissionRefdata permission : permissionsRefdatasUser) {
            if (permission.getCode().equalsIgnoreCase(codeResource)) {
                return permission;
            }
        }
        return null;
    }

    /**
     * Save permissions refdatas.
     *
     * @return the string
     */
    public String savePermissionsRefdatas() {
        try {
            securityRefdataManager.updatePermissionsUser(currentUtilisateur.getId(), permissionsRefdatasUser);
            if (!policyManager.isRoot() && currentUtilisateur.getId().equals(policyManager.getCurrentUser().getId())) {
                permissionsRefdatasUser = securityRefdataManager.getRestrictedPermissionsByUserId(currentUtilisateur.getId());
            }
            updatedStatus = false;
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in savePermissionsRefdatas", e);
        }
        return null;
    }

    /**
     * Sets the check all administration.
     *
     * @param checkAllAdministration the new check all administration
     */
    public void setCheckAllAdministration(final Boolean checkAllAdministration) {
        this.checkAllAdministration = checkAllAdministration;
    }

    /**
     * Sets the check all delete.
     *
     * @param checkAllDelete the new check all delete
     */
    public void setCheckAllDelete(final Boolean checkAllDelete) {
        this.checkAllDelete = checkAllDelete;
    }

    /**
     * Sets the check all download.
     *
     * @param checkAllDownload the new check all download
     */
    public void setCheckAllDownload(final Boolean checkAllDownload) {
        this.checkAllDownload = checkAllDownload;
    }

    /**
     * Sets the check all edition.
     *
     * @param checkAllEdition the new check all edition
     */
    public void setCheckAllEdition(final Boolean checkAllEdition) {
        this.checkAllEdition = checkAllEdition;
    }

    /**
     * Sets the current utilisateur.
     *
     * @param currentUtilisateur the new current utilisateur
     */
    public void setCurrentUtilisateur(final Utilisateur currentUtilisateur) {
        this.currentUtilisateur = currentUtilisateur;
        try {
            permissionsRefdatasUser = securityRefdataManager.getRestrictedPermissionsByUserId(currentUtilisateur.getId());
            initAllCheckAllCheckboxs();
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in setCurrentUtilisateur", e);
        }
    }

    /**
     * Sets the filter nom utilisateur.
     *
     * @param filterNomUtilisateur the new filter nom utilisateur
     */
    public void setFilterNomUtilisateur(final String filterNomUtilisateur) {
        this.filterNomUtilisateur = filterNomUtilisateur;
    }

    /**
     * Sets the security context.
     *
     * @param policyManager the new policy manager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Sets the security refdata manager.
     *
     * @param securityRefdataManager the new security refdata manager
     */
    public void setSecurityRefdataManager(final ISecurityRefdataManager securityRefdataManager) {
        this.securityRefdataManager = securityRefdataManager;
    }

    /**
     * Sets the type selection all.
     *
     * @param typeSelectionAll the new type selection all
     */
    public void setTypeSelectionAll(final String typeSelectionAll) {
        this.typeSelectionAll = typeSelectionAll;
    }

    /**
     * Sets the updated status.
     *
     * @param updatedStatus the new updated status
     */
    public void setUpdatedStatus(Boolean updatedStatus) {
        this.updatedStatus = updatedStatus;
    }

    /**
     * Sets the users manager.
     *
     * @param usersManager the new users manager
     */
    public void setUsersManager(final IUsersManager usersManager) {
        this.usersManager = usersManager;
    }

    /**
     * Update permission refdata administration.
     *
     * @return the string
     */
    public String updatePermissionRefdataAdministration() {
        final PermissionRefdata permissionRefdata = retrievePermissionRefdataByResource(permissionRefdataEdited.getCode());
        permissionRefdata.setAdministration(permissionRefdataEdited.getAdministration());
        updatedStatus = true;
        return null;
    }

    /**
     * Update permission refdata delete.
     *
     * @return the string
     */
    public String updatePermissionRefdataDelete() {
        final PermissionRefdata permissionRefdata = retrievePermissionRefdataByResource(permissionRefdataEdited.getCode());
        permissionRefdata.setDelete(permissionRefdataEdited.getDelete());
        updatedStatus = true;
        return null;
    }

    /**
     * Update permission refdata download.
     *
     * @return the string
     */
    public String updatePermissionRefdataDownload() {
        final PermissionRefdata permissionRefdata = retrievePermissionRefdataByResource(permissionRefdataEdited.getCode());
        permissionRefdata.setDownload(permissionRefdataEdited.getDownload());
        updatedStatus = true;
        return null;
    }

    /**
     * Update permission refdata edition.
     *
     * @return the string
     */
    public String updatePermissionRefdataEdition() {
        final PermissionRefdata permissionRefdata = retrievePermissionRefdataByResource(permissionRefdataEdited.getCode());
        permissionRefdata.setEdition(permissionRefdataEdited.getEdition());
        updatedStatus = true;
        return null;
    }
}
