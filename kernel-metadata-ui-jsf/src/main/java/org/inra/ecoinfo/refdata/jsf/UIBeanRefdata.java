package org.inra.ecoinfo.refdata.jsf;

import com.google.common.base.Strings;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.persistence.Transient;
import javax.validation.constraints.AssertFalse;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.managedbean.UIBeanActionTree;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.config.IMetadataConfiguration;
import org.inra.ecoinfo.refdata.config.Metadata;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NullValueException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.Visibility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UIBeanRefdata.
 */
@ManagedBean(name = "uiRefdata")
@ViewScoped
public class UIBeanRefdata implements Serializable {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.refdata.jsf.messages";
    /**
     * The Constant PROPERTY_MSG_DELETE_FAIL @link(String).
     */
    private static final String PROPERTY_MSG_DELETE_FAIL = "PROPERTY_MSG_DELETE_FAIL";
    /**
     * The Constant RULE_NAVIGATION_JSF @link(String).
     */
    private static final String RULE_NAVIGATION_JSF = "refdata";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    private static final String PROPERTY_MSG_DATE_VALUE_EXPECTED = "PROPERTY_MSG_DATE_VALUE_EXPECTED";
    private static final String PROPERTY_MSG_TIME_VALUE_EXPECTED = "PROPERTY_MSG_TIME_VALUE_EXPECTED";
    private static final String PROPERTY_MSG_MISSING_FLOAT = "PROPERTY_MSG_MISSING_FLOAT";
    private static final String PROPERTY_MSG_MISSING_INTEGER = "PROPERTY_MSG_MISSING_INTEGER";
    private static final String PROPERTY_MSG_MISSING_VALUE = "PROPERTY_MSG_MISSING_VALUE";
    private static final String PROPERTY_MSG_DUPLICATE_NKEY = "PROPERTY_MSG_DUPLICATE_NKEY";
    private static final Logger LOGGER = LoggerFactory.getLogger(UIBeanRefdata.class);
    private static final String NEW_LINE = "_newLine_";

    @ManagedProperty(value = "#{policyManager}")
    private IPolicyManager policyManager;

    @ManagedProperty(value = "#{uiBeanActionTree}")
    private UIBeanActionTree uiBeanActionTree;
    /**
     * The current selections @link(List<CurrentSelection>).
     */
    @Transient
    private DataTable datasetsDetailsDatatable;
    /**
     * The key paths @link(Set<String>).
     */
    private final SortedMap<String, LineModelGridMetadata> lines = new TreeMap();
    /**
     * The lines model grid metadata edited @link(Set<LineModelGridMetadata>).
     */
    @Transient
    private Set<LineModelGridMetadata> linesModelGridMetadataEdited = new HashSet<>();
    /**
     * The list value @link(String).
     */
    private String listValue;
    /**
     * The localization manager @link(ILocalizationManager).
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;
    /**
     * The metadata manager @link(IMetadataManager).
     */
    @ManagedProperty(value = "#{metadataManager}")
    protected IMetadataManager metadataManager;
    /**
     * The metadata selection @link(MetadataSelection).
     */
    @Transient
    private MetadataSelection metadataSelection;
    @ManagedProperty(value = "#{metadataConfiguration}")
    private IMetadataConfiguration metadataConfiguration;

    /**
     * The metadatas vo @link(List<MetadataVO>).
     */
    private List<MetadataVO> metadatasVO;
    /**
     * The model grid metadata @link(ModelGridMetadata).
     */
    @SuppressWarnings("rawtypes")
    @Transient
    private ModelGridMetadata modelGridMetadata;
    private SortedMap<String, List<Integer>> invalidCells = new TreeMap<>();
    private String filterData;
    private List<LineModelGridMetadata> filteredValues;

    private List<Boolean> visibledColumns;
    /**
     * The display dataset panel @link(Boolean).
     */
    private Boolean displayMetadataPanel;
    /**
     *
     * @param code
     * @return
     */
    Map<String, Map<String, String>> internationalizedRefdataCodes = new HashMap<>();

    /**
     * Instantiates a new uI bean refdata.
     */
    public UIBeanRefdata() {
    }

    /**
     *
     * @param metadataConfiguration
     */
    public void setMetadataConfiguration(IMetadataConfiguration metadataConfiguration) {
        this.metadataConfiguration = metadataConfiguration;
    }

    /**
     *
     * @param summary
     * @param detail
     */
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     *
     * @return
     */
    public String getFilterData() {
        return filterData;
    }

    /**
     *
     * @param filterData
     */
    public void setFilterData(String filterData) {
        this.filterData = filterData;
    }

    /**
     * Action delete line.
     *
     * @param path
     * @return the string
     */
    public String actionDeleteLine(String path) {
        final LineModelGridMetadata lineModelGridMetadata = lines.get(path);
        lineModelGridMetadata.setToDelete(!lineModelGridMetadata.getToDelete());
        return null;
    }

    /**
     * Action delete new line.
     *
     * @param path
     * @return the string
     */
    public String actionDeleteNewLine(String path) {
        final LineModelGridMetadata lineModelGridMetadata = lines.remove(path);
        if (lineModelGridMetadata == null) {
            return null;
        }
        linesModelGridMetadataEdited.remove(lineModelGridMetadata);
        modelGridMetadata.getLinesModelGridMetadatas().remove(lineModelGridMetadata);
        filteredValues = modelGridMetadata.getLinesModelGridMetadatas();
        invalidCells.remove(NEW_LINE);
        return null;
    }

    /**
     * Action delete new line.
     *
     * @return the string
     */
    public String actionDeleteNewLine() {
        final LineModelGridMetadata lineModelGridMetadata = lines.remove(NEW_LINE);
        linesModelGridMetadataEdited.remove(lineModelGridMetadata);
        modelGridMetadata.getLinesModelGridMetadatas().stream()
                .filter(line -> ((LineModelGridMetadata) line).getIsNew())
                .findFirst()
                .ifPresent(l -> modelGridMetadata.getLinesModelGridMetadatas().remove(l));
        filteredValues = modelGridMetadata.getLinesModelGridMetadatas();
        invalidCells.remove(NEW_LINE);
        return null;
    }

    /**
     * Action edit column.
     *
     * @param path
     * @param columnIndex
     * @return the string
     */
    public String actionEditColumn(String path, Integer columnIndex) {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("");
        LineModelGridMetadata localLineModelGridMetadataEdited = lines.get(path);
        if (localLineModelGridMetadataEdited == null) {
            return null;
        }
        if (!validColumn(badsFormatsReport, localLineModelGridMetadataEdited, columnIndex)) {
            RequestContext.getCurrentInstance().execute("alert('" + badsFormatsReport.getMessages() + "')");
            invalid(path, columnIndex);
        }
        valid(path, columnIndex);
        linesModelGridMetadataEdited.add(localLineModelGridMetadataEdited);
        return null;
    }

    private void valid(String path, Integer columnIndex) {
        if (invalidCells.containsKey(path)) {
            invalidCells.get(path).remove(columnIndex);
            if (invalidCells.get(path).isEmpty()) {
                invalidCells.remove(path);
            }
        }
    }

    private void invalid(String path, Integer columnIndex) {
        invalidCells.computeIfAbsent(path, k -> new LinkedList())
                .add(columnIndex);
    }

    /**
     *
     * @param badsFormatsReport
     * @param line
     * @param columnIndex
     * @return
     */
    public boolean validColumn(BadsFormatsReport badsFormatsReport, LineModelGridMetadata line, Integer columnIndex) {
        if (columnIndex + 1 > modelGridMetadata.getColumns().size()) {
            return false;
        }
        Column column = (Column) modelGridMetadata.getColumns().get(columnIndex);
        if (line == null) {
            return false;
        }
        ColumnModelGridMetadata columnMetadata = line.getColumnModelGridMetadataAt(columnIndex);
        if (columnMetadata == null) {
            return false;
        }

        return validColumn(badsFormatsReport, column, line, columnMetadata);
    }

    private boolean validColumn(BadsFormatsReport badsFormatsReport, Column column, LineModelGridMetadata lineMetadata, ColumnModelGridMetadata columnMetadata) {
        checkValue(badsFormatsReport, column, lineMetadata, columnMetadata);
        return !badsFormatsReport.hasErrors();
    }

    private void checkDateValue(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.DATE_TYPE.equalsIgnoreCase(column.getValueType().trim()) && value.length() > 0) {
            try {
                DateUtil.readLocalDateTimeFromText(Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY : column.getFormatType(), value);
            } catch (final DateTimeException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_DATE_VALUE_EXPECTED), column.getName(), column.getFormatType()), e));
            }
        }
    }

    private void checkFloatValue(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.FLOAT_TYPE.equals(column.getValueType()) && value.length() > 0) {
            try {
                Float.parseFloat(value.replaceAll(AbstractCSVMetadataRecorder.COMMA, AbstractCSVMetadataRecorder.DOT));
            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_FLOAT), column.getName()), e));
            }
        }
    }

    private void checkIntegerValue(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.INTEGER_TYPE.equals(column.getValueType()) && value.length() > 0) {
            try {
                Integer.parseInt(value);
            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_INTEGER), column.getName()), e));
            }
        }
    }

    private void checkNullableValue(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (!column.isNullable() && (!column.isKey() || column.getName().endsWith("key")) && StringUtils.isEmpty(value)) {
            badsFormatsReport.addException(new NullValueException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_VALUE), column.getName())));
        }
    }

    private void checkTimeValue(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (AbstractCSVMetadataRecorder.TIME_TYPE.equalsIgnoreCase(column.getValueType().trim()) && value.length() > 0) {
            try {
                DateUtil.readLocalDateTimeFromText(Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY : column.getFormatType(), value);
            } catch (final DateTimeException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_TIME_VALUE_EXPECTED), column.getName(), column.getFormatType()), e));
            }
        }
    }

    private void checkValue(final BadsFormatsReport badsFormatsReport, final Column column, LineModelGridMetadata lineMetadata, ColumnModelGridMetadata columnMetadata) {
        if (lineMetadata.getIsNew()) {
            chekNKey(badsFormatsReport, lineMetadata, columnMetadata);
        }
        String value = columnMetadata.getValue();
        checkNullableValue(badsFormatsReport, value, column);
        checkValueByType(badsFormatsReport, value, column);
    }

    private void chekNKey(BadsFormatsReport badsFormatsReport, LineModelGridMetadata lineMetadata, ColumnModelGridMetadata columnMetadata) {
        if (columnMetadata != null && columnMetadata.getIsNKey()) {
            for (Object line : modelGridMetadata.getLinesModelGridMetadatas()) {
                LineModelGridMetadata lineToCompare = (LineModelGridMetadata) line;
                if (lineToCompare.getIsNew()) {
                    return;
                }
                if (lineToCompare.getKeyPath().equals(lineMetadata.getKeyPath())) {
                    badsFormatsReport.addException(new NullValueException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_DUPLICATE_NKEY), lineMetadata.getKeyPath().replaceAll(("^//"), ""))));
                }
            }
        }
    }

    private void checkValueByType(final BadsFormatsReport badsFormatsReport, final String value, final Column column) {
        if (column.getValueType() != null) {
            checkDateValue(badsFormatsReport, value, column);
            checkTimeValue(badsFormatsReport, value, column);
            checkFloatValue(badsFormatsReport, value, column);
            checkIntegerValue(badsFormatsReport, value, column);
        }
    }

    /**
     * Adds the refdata.
     *
     * @return the string
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @SuppressWarnings("unchecked")
    public String addRefdata() throws BusinessException {
        LineModelGridMetadata lineModelGridMetadataEdited = modelGridMetadata.buildEmptyLineModelGridMetadata();
        linesModelGridMetadataEdited.add(lineModelGridMetadataEdited);
        lines.put(NEW_LINE, lineModelGridMetadataEdited);
        filteredValues = modelGridMetadata.getLinesModelGridMetadatas();
        testMandatoryForNewLine(lineModelGridMetadataEdited);
        return null;
    }

    /**
     *
     * @return
     */
    public boolean hasNew() {
        return lines.containsKey(NEW_LINE);
    }

    /**
     *
     */
    public void addNewLine() {
        if (getIsValidNewForm()) {
            LineModelGridMetadata newLine = lines.remove(NEW_LINE);
            lines.put(newLine.getKeyPath(), newLine);
            filteredValues = modelGridMetadata.getLinesModelGridMetadatas();
            modelGridMetadata.getLinesModelGridMetadatas().add(newLine);
        }
    }

    private void testMandatoryForNewLine(LineModelGridMetadata lineMetadata) {
        int columnIndex = 0;
        invalidCells.put(NEW_LINE, new LinkedList());
        for (ColumnModelGridMetadata columnMetadata : lineMetadata.getColumnsModelGridMetadatas()) {
            if (columnMetadata.getMandatory()) {
                invalidCells.get(NEW_LINE).add(columnIndex);
            }
            columnIndex++;
        }
        if (invalidCells.get(NEW_LINE).isEmpty()) {
            invalidCells.remove(NEW_LINE);
        }
    }

    /**
     * Gets the can save.
     *
     * @return the can save
     */
    public Boolean getCanSave() {
        if (metadataSelection == null || !getIsValidForm()) {
            return false;
        }
        boolean isAndCanModified = metadataSelection.publication && !linesModelGridMetadataEdited.isEmpty();
        boolean isAndCanDelete = metadataSelection.delete && getHasDeletedLines();
        return getIsGridOverallValid() && (isAndCanModified || isAndCanDelete);
    }

    /**
     * Gets the count deleted lines.
     *
     * @return the count deleted lines
     */
    public Integer getCountDeletedLines() {
        if (modelGridMetadata == null || !metadataSelection.delete) {
            return 0;
        }
        int count = 0;
        for (final Object line : modelGridMetadata.getLinesModelGridMetadatas()) {
            if (((LineModelGridMetadata) line).getToDelete()) {
                count++;
            }
        }
        return count;
    }

    /**
     * Gets the datasets details datatable.
     *
     * @return the datasets details datatable
     */
    public DataTable getDatasetsDetailsDatatable() {
        return datasetsDetailsDatatable;
    }

    /**
     * Gets the checks for deleted lines.
     *
     * @return the checks for deleted lines
     */
    public Boolean getHasDeletedLines() {
        if (Optional.ofNullable(modelGridMetadata)
                .map(mgm -> mgm.getLinesModelGridMetadatas())
                .map(l -> l.isEmpty())
                .orElse(false)) {
            return false;
        }
        return Optional.ofNullable(modelGridMetadata)
                .map(mgm -> mgm.getLinesModelGridMetadatas())
                .map(l -> l.stream().anyMatch((lineModelGridMetadata) -> (((LineModelGridMetadata) lineModelGridMetadata).getToDelete())))
                .orElse(false);
    }

    /**
     * Gets the checks if is grid overall valid.
     *
     * @return the checks if is grid overall valid
     */
    public Boolean getIsGridOverallValid() {
        return linesModelGridMetadataEdited.stream()
                .filter(l -> l != null)
                .noneMatch(lineModelGridMetadata -> !lineModelGridMetadata.getIsValid());
    }

    /**
     * Gets the checks if is in key path.
     *
     * @return the checks if is in key path
     */
    @AssertFalse(message = "Ligne en doublon!")
    public boolean getIsInKeyPath() {
        LineModelGridMetadata lineModelGridMetadataEdited = lines.get(NEW_LINE);
        if (lineModelGridMetadataEdited == null) {
            return false;
        }
        return getKeyPaths().contains(lineModelGridMetadataEdited.getKeyPath());
    }

    /**
     * Gets the key paths.
     *
     * @return the key paths
     */
    public Set<String> getKeyPaths() {
        return lines.keySet();
    }

    /**
     * Gets the lines model grid metadata edited.
     *
     * @return the lines model grid metadata edited
     */
    public Set<LineModelGridMetadata> getLinesModelGridMetadataEdited() {
        return linesModelGridMetadataEdited;
    }

    /**
     * Gets the list value.
     *
     * @return the list value
     */
    public String getListValue() {
        return listValue;
    }

    /**
     * Gets the metadatas.
     *
     * @param codeConfiguration
     * @param codeConfig
     * @return the metadatas
     */
    public List<MetadataVO> getMetadatas(Integer codeConfiguration) {

        if (metadatasVO == null) {
            try {
                final List<Metadata> metadatas = metadataManager.retrieveRestrictedMetadatas();
                metadatasVO = new LinkedList<>();

                metadatas.stream().forEach((metadata) -> {
                    metadatasVO.add(new MetadataVO(metadata));
                });
            } catch (final BusinessException e) {
                UncatchedExceptionLogger.logUncatchedException("uncatched exception in getMetadatas", e);
            }
        }
        return metadatasVO;
    }

    /**
     * Gets the metadata selection.
     *
     * @return the metadata selection
     */
    public MetadataSelection getMetadataSelection() {
        return metadataSelection;
    }

    /**
     * Gets the model grid metadata.
     *
     * @return the model grid metadata
     */
    @SuppressWarnings("rawtypes")
    public ModelGridMetadata getModelGridMetadata() {
        return modelGridMetadata;
    }

    /**
     *
     * @param event
     * @throws BusinessException
     */
    public void handleFileUpload(org.primefaces.event.FileUploadEvent event) throws BusinessException {
        byte[] fileToByteArray = null;
        String metadataCode = null;
        File tempFile = null;
        try {
            metadataCode = metadataSelection.getMetadata().code;
            tempFile = File.createTempFile(metadataCode, Utils.EXTENSION_FILE_CSV);
            FileUtils.copyInputStreamToFile(event.getFile().getInputstream(), tempFile);
        } catch (IOException | NullPointerException e) {
            LOGGER.error(e.getMessage(), e);
        }
        metadataManager.uploadMetadatas(tempFile, metadataCode, event.getFile().getFileName(), policyManager.getCurrentUserLogin());
        modelGridMetadata = metadataManager.buildModelGridMetadata(metadataCode);
        filteredValues = modelGridMetadata.getLinesModelGridMetadatas();
        initVisibledColumns(modelGridMetadata);
        raz();

        FacesMessage msg = new FacesMessage("Succesful",
                event.getFile().getFileName() + " Fichier uploadé.");
        FacesContext.getCurrentInstance().addMessage("messagesFile", msg);
        RequestContext.getCurrentInstance().execute("updateTable()");
    }

    /**
     *
     * @param fileName
     * @param in
     * @param destination
     */
    public void copyFile_b(String fileName, InputStream in, String destination) {
        try (OutputStream out = new FileOutputStream(new File(destination + fileName))) {
            int read = 0;
            byte[] bytes = new byte[1_024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();

        } catch (IOException e) {
            LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public String navigate() {
        return UIBeanRefdata.RULE_NAVIGATION_JSF;
    }

    /**
     * Save refdatas.
     *
     * @return the string
     */
    public String saveRefdatas() {
        try {
            metadataManager.saveAndDeleteModelGridMetadata(modelGridMetadata, linesModelGridMetadataEdited, metadataSelection.getMetadata().getCode());
            modelGridMetadata = metadataManager.buildModelGridMetadata(metadataSelection.getMetadata().getCode());
            raz();
        } catch (final Exception e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in ".concat(localizationManager.getMessage(UIBeanRefdata.BUNDLE_SOURCE_PATH, UIBeanRefdata.PROPERTY_MSG_DELETE_FAIL)), e);
        } finally {
            LineModelGridMetadata line = lines.remove(NEW_LINE);
            if (line != null) {
                linesModelGridMetadataEdited.remove(line);
            }

        }
        return null;
    }

    /**
     * Sets the datasets details datatable.
     *
     * @param datasetsDetailsDatatable the new datasets details datatable
     */
    public void setDatasetsDetailsDatatable(final DataTable datasetsDetailsDatatable) {
        this.datasetsDetailsDatatable = datasetsDetailsDatatable;
    }

    /**
     * Sets the list value.
     *
     * @param listValue the new list value
     */
    public void setListValue(final String listValue) {
        this.listValue = listValue;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the metadata manager.
     *
     * @param metadataManager the new metadata manager
     */
    public void setMetadataManager(final IMetadataManager metadataManager) {
        this.metadataManager = metadataManager;
    }

    /**
     * Sets the metadata selected.
     *
     * @param treeNode
     * @throws BusinessException the business exception
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    public void setMetadataSelected(final ITreeNode<IFlatNode> treeNode) throws BusinessException, PersistenceException {
        IFlatNode flatNode = treeNode.getData();
        if (treeNode.isLeaf()) {
            Optional<MetadataVO> metadataOptional = metadataConfiguration.getMetadatas().stream()
                    .filter(m -> m.getCode().equals(flatNode.getCode()))
                    .findFirst()
                    .flatMap(m -> Optional.ofNullable(new MetadataVO(m)));
            if (!metadataOptional.isPresent()) {
                return;
            }
            MetadataVO metadataSelected = metadataOptional.get();
            metadataSelection = new MetadataSelection(metadataSelected);
            modelGridMetadata = metadataManager.buildModelGridMetadata(metadataSelected.getCode());
            raz();
        }
    }

    private void raz() {
        linesModelGridMetadataEdited = new HashSet<>();
        lines.clear();
        invalidCells = new TreeMap<>();
        modelGridMetadata.getLinesModelGridMetadatas().stream()
                .forEach(line -> lines.put(((LineModelGridMetadata) line).getKeyPath(), (LineModelGridMetadata) line));
        filteredValues = modelGridMetadata.getLinesModelGridMetadatas();
        initVisibledColumns(modelGridMetadata);
    }

    /**
     * Validation new line.
     *
     * @param context the context
     * @param component the component
     * @param object the object
     * @link(FacesContext) the context
     * @link(UIComponent) the component
     * @link(Object) the object
     */
    public void validationNewLine(final FacesContext context, final UIComponent component, final Object object) {
        throw new ValidatorException(new FacesMessage("Error"));
    }

    /**
     *
     * @return
     */
    public boolean getIsValidForm() {
        return invalidCells.isEmpty();
    }

    /**
     *
     * @return
     */
    public boolean getIsValidNewForm() {
        if (invalidCells == null || invalidCells.isEmpty()) {
            return true;
        }
        return !invalidCells.containsKey(invalidCells.lastKey());
    }

    /**
     *
     * @return
     */
    public IPolicyManager getPolicyManager() {
        return policyManager;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @return
     */
    public TreeNode getSelectedTreeNode() {
        return uiBeanActionTree.getSelectedTreeNode();
    }

    /**
     *
     * @param selectedTreeNode
     * @throws BusinessException
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    public void setSelectedTreeNode(TreeNode selectedTreeNode) throws BusinessException, PersistenceException {
        if (selectedTreeNode != null && !selectedTreeNode.equals(uiBeanActionTree.getSelectedTreeNode())) {
            uiBeanActionTree.setSelectedTreeNode(selectedTreeNode);
            selectionChanged();
        }
    }

    /**
     * Selection changed.
     *
     * @throws BusinessException the business exception
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     * @link(TreeSelectionChangeEvent) the selection change event
     */
    public void selectionChanged() throws BusinessException, PersistenceException {
        if (uiBeanActionTree.getSelectedTreeNode() == null) {
            return;
        }
        //uiBeanActionTree.getSelectedTreeNode().setExpanded(true);
        if (uiBeanActionTree.getSelectedTreeNode().isLeaf()) {
            setMetadataSelected((ITreeNode<IFlatNode>) uiBeanActionTree.getSelectedTreeNode().getData());
            displayMetadataPanel = true;

        } else {
            displayMetadataPanel = false;
        }
        //uiBeanActionTree.updateColumns();
    }

    /**
     *
     * @param treeNode
     * @return
     */
    public String getLocalDescription(ITreeNode<IFlatNode> treeNode) {
        return getLocalDescriptionForIdNodeable(treeNode.getData().getCode());
    }

    /**
     *
     * @param code
     * @return
     */
    public String getLocalDescriptionForIdNodeable(String code) {
        String language = FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
        return internationalizedRefdataCodes
                .computeIfAbsent(language, k -> new HashMap<String, String>())
                .computeIfAbsent(code, k -> findInternationalizedCode(language, code));

    }

    private String findInternationalizedCode(String language, String code) {
        Optional<INode> nodeByNodeableCode = policyManager.getMgaServiceBuilder().getRecorder().getNodeByNodeableCode(code, WhichTree.TREEREFDATA);
        return nodeByNodeableCode
                .map(node -> node.getNodeable())
                .map(nodeable -> (Refdata) nodeable)
                .map(ref -> ref.getDescription())
                .map(des -> localizationManager.getByNKey(language, Refdata.NAME_ENTITY_JPA, Refdata.NAME_ATTRIBUTS_DESCRIPTION, des))
                .filter(l -> l.isPresent())
                .map(l -> l.get().getLocaleString())
                .orElse("");
    }

    /**
     *
     * @return
     */
    public List<LineModelGridMetadata> getFilteredValues() {
        return filteredValues;
    }

    /**
     *
     * @param filteredValues
     */
    public void setFilteredValues(List<LineModelGridMetadata> filteredValues) {
        this.filteredValues = filteredValues;
    }

    /**
     *
     * @param e
     */
    public void onToggle(ToggleEvent e) {
        visibledColumns.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }

    /**
     *
     * @return
     */
    public List<Boolean> getVisibledColumns() {
        return visibledColumns;
    }

    /**
     *
     * @param visibledColumns
     */
    public void setVisibledColumns(List<Boolean> visibledColumns) {
        this.visibledColumns = visibledColumns;
    }

    /**
     *
     * @param model
     */
    protected void initVisibledColumns(ModelGridMetadata model) {
        this.visibledColumns = (List<Boolean>) modelGridMetadata.getColumns().stream()
                .map(c -> Boolean.TRUE)
                .collect(Collectors.toCollection(() -> new ArrayList()));
    }

    /**
     *
     * @return
     */
    public LineModelGridMetadata getLineModelGridMetadataEdited() {
        return lines.get(NEW_LINE);

    }

    /**
     *
     * @param uiBeanActionTree
     */
    public void setUiBeanActionTree(UIBeanActionTree uiBeanActionTree) {
        this.uiBeanActionTree = uiBeanActionTree;

    }

    /**
     * The Class MetadataVO.
     */
    public static class MetadataVO implements Serializable {

        /**
         * The Constant serialVersionUID @link(long).
         */
        private static final long serialVersionUID = 1L;
        /**
         * The code @link(String).
         */
        private final String code;
        /**
         * The final metadata @link(boolean).
         */
        private boolean finalMetadata = false;
        /**
         * The name @link(String).
         */
        private final String name;
        private final String description;

        /**
         * Instantiates a new metadata vo.
         *
         * @param metadata the metadata
         * @link(Metadata) the metadata
         */
        public MetadataVO(final Metadata metadata) {
            final String localizedName = metadata.getInternationalizedNames().get(getLanguage());
            final String localizedDescription = metadata.getInternationalizedDescriptions().get(getLanguage());
            this.name = localizedName == null ? metadata.getName() : localizedName;
            this.description = localizedDescription == null ? metadata.getDescription() : localizedDescription;
            this.code = Utils.createCodeFromString(metadata.getName());
            this.finalMetadata = metadata.getFinalMetadata();
        }

        /**
         * Gets the code.
         *
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * Gets the final metadata.
         *
         * @return the final metadata
         */
        public boolean getFinalMetadata() {
            return finalMetadata;
        }

        /**
         * Gets the language.
         *
         * @return the language
         */
        protected String getLanguage() {
            return FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
        }

        /**
         * Gets the name.
         *
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the final metadata.
         *
         * @param finalMetadata the new final metadata
         */
        public void setFinalMetadata(final String finalMetadata) {
            this.finalMetadata = Boolean.parseBoolean(finalMetadata);
        }
    }

    /**
     * The Class MetadataSelection.
     */
    public class MetadataSelection {

        /**
         * The delete @link(Boolean).
         */
        private Boolean delete = true;
        /**
         * The download @link(Boolean).
         */
        private Boolean download = true;
        /**
         * The edition @link(Boolean).
         */
        private Boolean edition = true;
        /**
         * The edition @link(Boolean).
         */
        private Boolean publication = true;
        /**
         * The metadata @link(MetadataVO).
         */
        private MetadataVO metadata;

        /**
         * Instantiates a new metadata selection.
         *
         * @param metadataSelected the metadata selected
         * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
         * @link(MetadataVO) the metadata selected
         */
        public MetadataSelection(final MetadataVO metadataSelected) throws PersistenceException {
            metadata = metadataSelected;

            if (!policyManager.isRoot()) {
                Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role = policyManager.getOrLoadActivities(policyManager.getCurrentUser(), WhichTree.TREEREFDATA);

                INode node = policyManager.getNodeByNodeableCode(metadataSelected.getCode(), WhichTree.TREEREFDATA).orElseThrow(PersistenceException::new);
                download = hasRoleForNode(role, node, Activities.telechargement);
                publication = hasRoleForNode(role, node, Activities.publication);
                delete = hasRoleForNode(role, node, Activities.suppression);
                edition = hasRoleForNode(role, node, Activities.edition);
            }

        }

        private boolean hasRoleForNode(Map<Activities, Map<Long, Map<Group, List<LocalDate>>>> role, INode node, Activities activity) {
            return role.containsKey(activity) && role.get(activity).containsKey(node.getId());
        }

        /**
         * Gets the delete.
         *
         * @return the delete
         */
        public Boolean getDelete() {
            return delete;
        }

        /**
         * Gets the download.
         *
         * @return the download
         */
        public Boolean getDownload() {
            return download;
        }

        /**
         * Gets the publication.
         *
         * @return the publication
         */
        public Boolean getPublication() {
            return publication;
        }

        /**
         * Gets the edition.
         *
         * @return the edition
         */
        public Boolean getEdition() {
            return edition;
        }

        /**
         * Gets the final metadata.
         *
         * @return the final metadata
         */
        public boolean getFinalMetadata() {
            return metadata.getFinalMetadata();
        }

        /**
         * Gets the metadata.
         *
         * @return the metadata
         */
        public MetadataVO getMetadata() {
            return metadata;
        }

        /**
         * Sets the delete.
         *
         * @param delete the new delete
         */
        public void setDelete(final Boolean delete) {
            this.delete = delete;
        }

        /**
         * Sets the download.
         *
         * @param download the new download
         */
        public void setDownload(final Boolean download) {
            this.download = download;
        }

        /**
         * Sets the publication.
         *
         * @param publication the new publication
         */
        public void setPublication(final Boolean publication) {
            this.publication = publication;
        }

        /**
         * Sets the edition.
         *
         * @param edition the new edition
         */
        public void setEdition(final Boolean edition) {
            this.edition = edition;
        }

        /**
         * Sets the metadata.
         *
         * @param metadata the new metadata
         */
        public void setMetadata(final MetadataVO metadata) {
            this.metadata = metadata;
        }
    }
}
