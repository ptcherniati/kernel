/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.serialization.impl;

import java.util.HashMap;
import java.util.Map;
import javax.xml.validation.Schema;
import org.inra.ecoinfo.serialization.IModuleSerialization;
import org.inra.ecoinfo.serialization.impl.xsd.UtilisateurSerialization;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class SerializationFactoryTest {

    /**
     *
     */
    public static final String REPOSITORY_URI = "repositoryURI";

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    SerializationFactory instance;
    UtilisateurSerialization module;
    Map<String, IModuleSerialization> modules = new HashMap();
    /**
     *
     */
    public SerializationFactoryTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new SerializationFactory();
        module = new UtilisateurSerialization();
        modules.put(module.getModuleName(), module);
        instance.setModulesSerialization(modules);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getRepositoryURI method, of class SerializationFactory.
     */
    @Test
    public void testGetRepositoryURI() {
        instance.setRepositoryURI(REPOSITORY_URI);
        String result = instance.getRepositoryURI();
        assertEquals(REPOSITORY_URI, result);
    }

    /**
     * Test of createSerialization method, of class SerializationFactory.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateSerialization() throws Exception {
        ModuleSerialization result = instance.createSerialization();
        assertEquals(module, result.getModuleSerialization(module.getModuleName()));
        assertEquals("projet-ORE", ((UtilisateurSerialization) result.getModuleSerialization(module.getModuleName())).getId());
    }

    /**
     * Test of getModulesSerialization method, of class SerializationFactory.
     */
    @Test
    public void testGetModulesSerialization() {
        Map<String, IModuleSerialization> result = instance.getModulesSerialization();
        assertEquals(modules, result);
    }

    /**
     * Test of getSchema method, of class SerializationFactory.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetSchema() throws Exception {
        Schema result = instance.getSchema(module);
        assertNotNull(result);
    }

    /**
     * Test of newIntegerClassArray method, of class SerializationFactory.
     */
    @Test
    public void testNewIntegerClassArray() {
        Class[] result = instance.newIntegerClassArray();
        assertEquals(Integer.class, result[0]);
        assertEquals(StringBuilder.class, result[1]);
    }

    /**
     * Test of newStringClassArray method, of class SerializationFactory.
     */
    @Test
    public void testNewStringClassArray() {
        Class[] result = instance.newStringClassArray();
        assertEquals(String.class, result[0]);
        assertEquals(String.class, result[1]);
    }

}
