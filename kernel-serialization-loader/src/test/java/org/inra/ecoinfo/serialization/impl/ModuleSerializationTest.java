/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.serialization.impl;

import java.util.HashMap;
import java.util.Map;
import org.easymock.Mock;
import org.inra.ecoinfo.serialization.IModuleSerialization;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import org.mockito.MockitoAnnotations;

/**
 * @author tcherniatinsky
 */
public class ModuleSerializationTest {

    /**
     *
     */
    public static final String MODULE_NAME = "moduleName";

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    ModuleSerialization instance;
    @Mock
    IModuleSerialization moduleSerialization;
    Map<String, IModuleSerialization> modules = new HashMap();
    /**
     *
     */
    public ModuleSerializationTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new ModuleSerialization();
        modules.put(MODULE_NAME, moduleSerialization);
        instance.setModulesSerialization(modules);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getModuleSerialization method, of class ModuleSerialization.
     */
    @Test
    public void testGetModuleSerialization() {
        IModuleSerialization result = instance.getModuleSerialization(MODULE_NAME);
        assertEquals(moduleSerialization, result);
    }

    /**
     * Test of getModulesSerialization method, of class ModuleSerialization.
     */
    @Test
    public void testGetModulesSerialization() {
        Map<String, IModuleSerialization> result = instance.getModulesSerialization();
        assertEquals(modules, result);
    }

}
