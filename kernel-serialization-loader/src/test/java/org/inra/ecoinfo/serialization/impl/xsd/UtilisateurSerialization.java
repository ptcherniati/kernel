package org.inra.ecoinfo.serialization.impl.xsd;

import java.util.zip.ZipOutputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.serialization.AbstractSerialization;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MailAdminSerialization.
 */
public class UtilisateurSerialization extends AbstractSerialization {

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(UtilisateurSerialization.class);
    /**
     *
     */
    public static final String HEADER_CSV_FILE = "id;active;email;emploi;isroot;language;login;nom;password;poste;prenom";
    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "utilisateurSerialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "utilisateurSerialization";
    private String id;

    /**
     * @param digester
     */

    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("serialization/module/utilisateurSerialization/id", "setId", 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#createConfig(org.apache.commons.digester.Digester)
     */

    /**
     * @return
     */

    @Override
    public String getModuleName() {
        return UtilisateurSerialization.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#getModuleName()
     */

    /**
     * @return
     */

    @Override
    public String getSchemaPath() {
        return UtilisateurSerialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractSerialization#getSchemaPath()
     */

    /**
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#isAutoLoad()
     */

    /**
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param outZip
     * @throws BusinessException
     */
    @Override
    public void serialize(ZipOutputStream outZip) throws BusinessException {
        outZip.setMethod(2);
    }
}
