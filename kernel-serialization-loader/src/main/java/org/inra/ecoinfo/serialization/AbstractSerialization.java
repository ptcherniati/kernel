package org.inra.ecoinfo.serialization;

import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.AbstractConfiguration;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AbstractSerialization. Can be use to implements
 IModuleSerialization classes. Add some fonction to help building digester
 parsing
 */
public abstract class AbstractSerialization extends AbstractConfiguration  implements IModuleSerialization {

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractSerialization.class);

    /**
     *
     */
    public AbstractSerialization() {
    }

    /**
     *
     * @throws BusinessException
     */

    @Override
    public void postConfig() throws BusinessException {
        LOGGER.info(String.format("no postConfig in %s", getClass()));
    }   

    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod(String.format("serialization/module/%s/skip",getModuleName()), "setSkip", 0);
        //do nothing
    } 
}
