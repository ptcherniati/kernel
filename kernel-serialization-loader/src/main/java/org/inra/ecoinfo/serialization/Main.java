/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.serialization;

import org.inra.ecoinfo.serialization.impl.SerializationFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author tcherniatinsky
 */
public class Main {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Main p = new Main();
        p.start();
    }
    SerializationFactory serializationFactory;

    private void start() {
        ApplicationContext context
                = new ClassPathXmlApplicationContext("META-INF/spring/applicationContextSerialization.xml");
        serializationFactory = (SerializationFactory) context.getBean("serializationFactory");
        serializationFactory.serialize();
    }
}
