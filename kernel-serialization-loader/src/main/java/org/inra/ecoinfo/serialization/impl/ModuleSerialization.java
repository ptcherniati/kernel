package org.inra.ecoinfo.serialization.impl;

import java.util.HashMap;
import java.util.Map;
import org.inra.ecoinfo.serialization.IModuleSerialization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class ModuleSerialization.
 */
public class ModuleSerialization {

    static final Logger LOGGER = LoggerFactory.getLogger(ModuleSerialization.class);
    /**
     * The Map<String,IModuleSerialization> modules configuration.
     */
    private Map<String, IModuleSerialization> modulesSerialization = new HashMap<>();

    /**
     * Gets the module configuration.
     *
     * @param moduleName the module name
     * @return the module configuration
     */
    public IModuleSerialization getModuleSerialization(String moduleName) {
        return modulesSerialization.get(moduleName);
    }

    /**
     * Gets the Map<String,IModuleSerialization> modules configuration.
     *
     * @return the Map<String,IModuleSerialization> modules configuration
     */
    public Map<String, IModuleSerialization> getModulesSerialization() {
        return modulesSerialization;
    }

    /**
     * Sets the modules configuration.
     *
     * @param modulesSerialization the Map<String,IModuleSerialization> modules
     * configuration
     */
    public void setModulesSerialization(Map<String, IModuleSerialization> modulesSerialization) {
        this.modulesSerialization = modulesSerialization;
    }
}
