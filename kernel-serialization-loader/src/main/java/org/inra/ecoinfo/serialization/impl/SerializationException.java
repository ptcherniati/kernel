/**
 * OREILacs project - see LICENCE.txt for use created: 12 févr. 2009 14:19:12
 */
package org.inra.ecoinfo.serialization.impl;

/**
 * The Class SerializationException.
 */
public class SerializationException extends Exception {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new ConfigurationException configuration exception.
     */
    public SerializationException() {
        super();
    }

    /**
     * Instantiates a new ConfigurationException configuration exception.
     *
     * @param string
     * @param arg0 the String arg0
     */
    public SerializationException(final String arg0) {
        super(arg0);
    }

    /**
     * Instantiates a new ConfigurationException configuration exception.
     *
     * @param arg0 the String arg0
     * @param thrwbl
     * @param arg1 the Throwable arg1
     */
    public SerializationException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new ConfigurationException configuration exception.
     *
     * @param thrwbl
     * @param arg0 the Throwable arg0
     */
    public SerializationException(final Throwable arg0) {
        super(arg0);
    }
}
