package org.inra.ecoinfo.serialization.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.zip.ZipOutputStream;
import javax.persistence.Transient;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.serialization.IModuleSerialization;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * A factory for creating Serialization objects.
 */
public class SerializationFactory {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/coreSerialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "coreSerialization";
    private static final String PATH_FOR_SERIALIZATION = "%sserialization/serialization.zip";

    /**
     *
     */
    public static final String CST_DEFAULT_SITE_SEPARATOR_FOR_FILE_NAMES = "-";
    /**
     * The Constant CORRECT_LOADING.
     */
    private static final String CORRECT_LOADING = "Le module %s a été correctement chargé";
    /**
     * The Constant BAD_LOADING.
     */
    private static final String BAD_LOADING = "Le module %s n'a pas été correctement chargé";
    /**
     * The Constant CORRECT_LOADING.
     */
    private static final String CORRECT_CONFIG = "Le module %s a été correctement paramétré";
    /**
     * The Constant BAD_LOADING.
     */
    private static final String BAD_CONFIG = "Le module %s n'a pas été correctement paramétré %n \t %s";
    /**
     * The Constant DYNAMIC_VALIDATION.
     */
    private static final String DYNAMIC_VALIDATION = "http://apache.org/xml/features/validation/dynamic";
    /**
     * The Constant CONFIG_PATH.
     */
    private static final String CONFIG_PATH = "/META-INF/serialization.xml";
    /**
     * The Logger LOGGER.
     */
    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(SerializationFactory.class.getName());
    /**
     * The String repository uri.
     */
    private String repositoryURI;
    ZipOutputStream outZip;
    /**
     * The Map<String,IModuleSerialization> modules configuration.
     */
    private Map<String, IModuleSerialization> modulesSerialization = new HashMap<>();

    /**
     * Instantiates a new SerializationFactory configuration factory.
     *
     */
    public SerializationFactory() {
//        if (new File(SerializationFactory.LOG4J_GEN_PROPERTIES_LOCATION).exists()) {
//            PropertyConfigurator.configure(SerializationFactory.LOG4J_GEN_PROPERTIES_LOCATION);
//        } else if (this.getClass().getResource(SerializationFactory.LOG4J_GEN_PROPERTIES) != null) {
//            try {
//                final Properties p = new Properties();
//                p.load(this.getClass().getResource(SerializationFactory.LOG4J_GEN_PROPERTIES).openStream());
//                PropertyConfigurator.configure(p);
//            } catch (IOException ex) {
//                LoggerFactory.getLogger(SerializationFactory.class.getName()).error(ex.getMessage(), ex);
//            }
//        } else if (new File(SerializationFactory.SRC_MAIN_RESOURCES_LOG4J_PROPERTIES).exists()) {
//            PropertyConfigurator.configure(SerializationFactory.SRC_MAIN_RESOURCES_LOG4J_PROPERTIES);
//        }
    }

    /**
     *
     * @param outZip
     */
    public void setOutZip(ZipOutputStream outZip) {
        this.outZip = outZip;
    }

    /**
     *
     * @return
     */
    public String getRepositoryURI() {
        return repositoryURI;
    }

    /**
     *
     * @param repositoryURI
     */
    public void setRepositoryURI(String repositoryURI) {
        LOGGER.info(String.format("Setting repository URI %s", repositoryURI));
        this.repositoryURI = repositoryURI;
    }

    /**
     * Creates a new Serialization object.
     *
     * @return the module configuration
     * @throws javax.xml.parsers.ParserConfigurationException
     */
    public ModuleSerialization createSerialization() throws ParserConfigurationException {
        final InfosReport infosreport = new InfosReport("Loading and validate serialization configuration success");
        final InfosReport errorsreport = new InfosReport("Loading and validate serialization configuration errors");
        loadGeneralConfiguration(infosreport);
        final ModuleSerialization moduleSerialization = new ModuleSerialization();
        moduleSerialization.setModulesSerialization(modulesSerialization);
        if (modulesSerialization != null) {
            for (final Entry<String, IModuleSerialization> moduleSerializationEntry : modulesSerialization.entrySet()) {
                final Digester digester = new Digester();
                InputStream configurationFile = this.getClass().getResourceAsStream(SerializationFactory.CONFIG_PATH);

                try {
                    digester.setXMLSchema(getSchema(moduleSerializationEntry.getValue()));
                    digester.setValidating(true);
                    digester.setNamespaceAware(true);
                    ErrorHandlerImpl errorHandler = new ErrorHandlerImpl();
                    digester.setErrorHandler(errorHandler);
                    digester.setUseContextClassLoader(true);
                    digester.setFeature(SerializationFactory.DYNAMIC_VALIDATION, true);
                } catch (final SAXException e1) {
                    UncatchedExceptionLogger.log(String.format(SerializationFactory.CORRECT_LOADING, moduleSerializationEntry.getKey()), e1);
                    infosreport.addInfos(moduleSerializationEntry.getKey(), String.format(SerializationFactory.CORRECT_LOADING, moduleSerializationEntry.getKey()));
                }
                if (moduleSerializationEntry.getValue().isAutoLoad()) {
                    digester.push(moduleSerializationEntry.getValue());
                    moduleSerializationEntry.getValue().createConfig(digester);
                }
                try {
                    //digester.parse(configurationFile);
                    digester.parse(configurationFile);
                } catch (final IOException | SAXException e) {
                    UncatchedExceptionLogger.log(String.format(SerializationFactory.BAD_LOADING, moduleSerializationEntry.getKey()), e);
                    errorsreport.addInfos(moduleSerializationEntry.getKey(), String.format(SerializationFactory.BAD_LOADING, moduleSerializationEntry.getKey()));
                }
                infosreport.addInfos(moduleSerializationEntry.getKey(), String.format(SerializationFactory.CORRECT_LOADING, moduleSerializationEntry.getKey()));
            }
            if (!infosreport.getinfoMessages().isEmpty()) {
                LOGGER.error(infosreport.getMessages());
            }
        }
        Optional.ofNullable(modulesSerialization)
                .ifPresent((ms) -> {
                    ms.entrySet().stream().map((entry) -> {
                        String key = entry.getKey();
                        return entry;
                    }).forEach((entry) -> {
                        IModuleSerialization value = entry.getValue();
                            });
                });
        return moduleSerialization;
    }

    /**
     * Gets the Map<String,IModuleSerialization> modules configuration.
     *
     * @return the Map<String,IModuleSerialization> modules configuration
     */
    public Map<String, IModuleSerialization> getModulesSerialization() {
        return modulesSerialization;
    }

    /**
     * Sets the modules configuration.
     *
     * @param modulesSerialization the Map<String,IModuleSerialization> modules
     * configuration
     */
    public void setModulesSerialization(Map<String, IModuleSerialization> modulesSerialization) {
        this.modulesSerialization = modulesSerialization;
    }

    /**
     * Gets the Schema schema.
     *
     * @param moduleSerialization the IModuleSerialization module configuration
     * @return the Schema schema
     * @throws SAXException the sAX exception
     */
    public final Schema getSchema(IModuleSerialization moduleSerialization) throws SAXException {
        final StreamSource schema = new StreamSource(moduleSerialization == null ? getStreamSchema() : moduleSerialization.getStreamSchema());
        final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        return schemaFactory.newSchema(schema);
    }

    /**
     * New integer class array.
     *
     * @return the Class[] class[]
     */
    @SuppressWarnings("rawtypes")
    public Class[] newIntegerClassArray() {
        return new Class[]{Integer.class, StringBuilder.class};
    }

    /**
     * New string class array.
     *
     * @return the Class[] class[]
     */
    @SuppressWarnings("rawtypes")
    public Class[] newStringClassArray() {
        return new Class[]{String.class, String.class};
    }

    /**
     * Post config.
     *
     * @param moduleSerialization the module configuration
     * @return the string
     */
    @Transactional(rollbackFor = Exception.class)
    public String postConfig(IModuleSerialization moduleSerialization) {
        final InfosReport infosreport = new InfosReport("Configuring configuration success");
        final InfosReport errorsreport = new InfosReport("Configuring configuration errors");
        if (modulesSerialization != null) {
            try {
                moduleSerialization.postConfig();
                infosreport.addInfos(moduleSerialization.getModuleName(), String.format(SerializationFactory.CORRECT_CONFIG, moduleSerialization.getModuleName()));
            } catch (final BusinessException e) {
                UncatchedExceptionLogger.log(String.format(SerializationFactory.BAD_CONFIG, moduleSerialization.getModuleName(), e.getMessage()), e);
                errorsreport.addInfos(moduleSerialization.getModuleName(), String.format(SerializationFactory.BAD_CONFIG, moduleSerialization.getModuleName(), e.getMessage()));
            }
            if (!infosreport.getinfoMessages().isEmpty()) {
                LOGGER.error(infosreport.getMessages());
            }
        }
        return infosreport.buildHTMLMessages();
    }

    /**
     *
     */
    @Transactional(rollbackFor = Exception.class)
    public void serialize() {
        try {
            openZip();
            for (Map.Entry<String, IModuleSerialization> moduleEntry : getModulesSerialization().entrySet()) {
                if(moduleEntry.getValue().isSkip()){
                    continue;
                }
                LOGGER.info(String.format("Serialize %s", moduleEntry.getValue().getModuleName()));
                moduleEntry.getValue().serialize(outZip);
                LOGGER.info(String.format("fin de sérializationde  %s", moduleEntry.getValue().getModuleName()));
            }
            closeZip();
        } catch (BusinessException | IOException e) {
            LoggerFactory.getLogger(SerializationFactory.class).error(e.getMessage(), e);
        }

    }

    private void loadGeneralConfiguration(InfosReport infosReport) {
        final Digester digester = new Digester();
        //final File configurationFile = new File(getClass().getResource(SerializationFactory.CONFIG_PATH).getPath());
        InputStream configurationFile = this.getClass().getResourceAsStream(SerializationFactory.CONFIG_PATH);

        try {
            digester.setXMLSchema(getSchema(null));
            digester.setValidating(true);
            digester.setNamespaceAware(true);
            ErrorHandlerImpl errorHandler = new ErrorHandlerImpl();
            digester.setErrorHandler(errorHandler);
            digester.setUseContextClassLoader(true);
            digester.setFeature(SerializationFactory.DYNAMIC_VALIDATION, true);
        } catch (final SAXException e1) {
            UncatchedExceptionLogger.log(String.format(SerializationFactory.CORRECT_LOADING, getModuleName()), e1);
            infosReport.addInfos(getModuleName(), String.format(SerializationFactory.CORRECT_LOADING, getModuleName()));
        } catch (ParserConfigurationException ex) {
            LoggerFactory.getLogger(SerializationFactory.class.getName()).error(ex.getMessage(), ex);
            infosReport.addInfos(getModuleName(), String.format(SerializationFactory.CORRECT_LOADING, getModuleName()));
        }
        if (isAutoLoad()) {
            digester.push(this);
            createConfig(digester);
        }
        try {
            //digester.parse(configurationFile);
            digester.parse(configurationFile);
        } catch (IOException | SAXException e) {
            LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     *
     * @param digester
     */
    public void createConfig(Digester digester) {
        digester.addCallMethod("serialization/repositoryURI", "setRepositoryURI", 0);
    }

    /**
     *
     * @return
     */
    public String getModuleName() {
        return MODULE_NAME;
    }

    /**
     *
     * @return
     */
    public String getSchemaPath() {
        return SCHEMA_PATH;
    }

    /**
     *
     * @return
     */
    public InputStream getStreamSchema() {
        return getClass().getResourceAsStream(getSchemaPath());
    }

    /**
     *
     * @return
     */
    public Boolean isAutoLoad() {
        return true;
    }

    private void openZip() throws FileNotFoundException {
        final String path = String.format(PATH_FOR_SERIALIZATION, repositoryURI);
        FileWithFolderCreator.createFile(path);
        File zipFile = new File(path);
        if (zipFile.exists()) {
            zipFile.delete();

        }
        outZip = new ZipOutputStream(new FileOutputStream(zipFile));
    }

    private void closeZip() throws IOException {
        outZip.close();
    }

    private static class ErrorHandlerImpl implements ErrorHandler {

        @Override
        public void warning(final SAXParseException exception) throws SAXException {
            LOGGER.warn("parse Exception", exception);
        }

        @Override
        public void fatalError(final SAXParseException exception) throws SAXException {
            LOGGER.error("parse Exception", exception);
        }

        @Override
        public void error(final SAXParseException exception) throws SAXException {
            LOGGER.error("parse Exception", exception);
        }
    }
}
