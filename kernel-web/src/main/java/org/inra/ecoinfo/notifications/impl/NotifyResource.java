package org.inra.ecoinfo.notifications.impl;

import org.inra.ecoinfo.notifications.entity.Notification;
import org.primefaces.push.EventBus;
import org.primefaces.push.RemoteEndpoint;
import org.primefaces.push.annotation.*;
import org.primefaces.push.impl.JSONEncoder;

/**
 *
 * @author tcherniatinsky
 */
@PushEndpoint("/notify/{user}")
@Singleton
public class NotifyResource {

    /**
     *
     */
    @OnOpen
    public void onOpen() {
    }

    /**
     *
     * @param r
     */
    @OnOpen
    public void onOpen(RemoteEndpoint r) {
        if (r.isOpen()) {
            return;
        }
    }

    /**
     *
     * @param r
     * @param e
     */
    @OnOpen
    public void onOpen(RemoteEndpoint r, EventBus e) {
    }

    /**
     *
     * @param notification
     * @return
     */
    @OnMessage(encoders = {JSONEncoder.class})
    public Notification onMessage(Notification notification) {
        return notification;
    }

    /**
     *
     */
    @OnClose
    public void onClose() {
    }

    /**
     *
     * @param r
     */
    @OnClose
    public void onClose(RemoteEndpoint r) {
        if (!r.isOpen()) {
            return;
        }
    }

    /**
     *
     * @param r
     * @param e
     */
    @OnClose
    public void onClose(RemoteEndpoint r, EventBus e) {
    }
}
