/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo;

import org.inra.ecoinfo.dataset.IRecorder;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author tcherniatinsky
 */
public class TestRecorder implements IRecorder{

    /**
     *
     * @param versionFile
     * @throws BusinessException
     */
    @Override
    public void deleteRecords(VersionFile versionFile) throws BusinessException {
        //do nothing
        
    }

    /**
     *
     * @param versionFile
     * @param fileEncoding
     * @throws BusinessException
     */
    @Override
    public void record(VersionFile versionFile, String fileEncoding) throws BusinessException {
    }

    /**
     *
     * @param localizationManager
     */
    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
    }

    /**
     *
     * @param versionFile
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void testFormat(VersionFile versionFile, String encoding) throws BusinessException {
    }

    /**
     *
     * @param fileName
     * @throws BusinessException
     */
    @Override
    public void testInDeepFilename(String fileName) throws BusinessException {
    }

    /**
     *
     * @param versionFileDAO
     */
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO){
        
    }
    
}
