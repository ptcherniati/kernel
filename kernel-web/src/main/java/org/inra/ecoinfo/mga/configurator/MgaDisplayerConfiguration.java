package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.theme.Theme;

/**
 * 
 * @author ryahiaoui
 */
public class MgaDisplayerConfiguration extends AbstractMgaDisplayerConfiguration {

    /**
     *
     */
    public MgaDisplayerConfiguration() {

        this.addColumnNamesForInstanceType(Site.class, new String[] {
                "PROPERTY_COLUMN_LOCALISATION_NAME", "PROPERTY_COLUMN_PLATEFORM_NAME",
                "PROPERTY_COLUMN_SITE_DEFAULT" });

        this.addColumnNamesForInstanceType(Theme.class, new String[] {
                "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME",
                "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME",
                "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME" });

        this.addColumnNamesForInstanceType(DataType.class, new String[] {
                "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME",
                "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME",
                "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME" });

        this.addColumnNamesForInstanceType(DatatypeVariableUnite.class, new String[] {
                "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME",
                "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME",
                "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME" });

        this.addColumnNamesForInstanceType(Refdata.class, new String[] {
                "PROPERTY_COLUMN_REFDATA_NAME", "PROPERTY_COLUMN_REFDATA_NAME",
                "PROPERTY_COLUMN_REFDATA_NAME", "PROPERTY_COLUMN_REFDATA_NAME",
                "PROPERTY_COLUMN_REFDATA_NAME", "PROPERTY_COLUMN_REFDATA_NAME" });

    }

}
