package org.inra.ecoinfo.notification;

import org.inra.ecoinfo.AbstractTestFixture;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.runner.RunWith;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

import java.net.URISyntaxException;
import java.util.List;

import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.*;
import org.mockito.MockitoAnnotations;

/**
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {TransactionalTestFixtureExecutionListener.class})
public class CheckNotificationFixture extends AbstractTestFixture {

    String BUNDLE_NAME = "org.inra.ecoinfo.menu.menu";

    /**
     *
     */
    public CheckNotificationFixture() {
        super();
        MockitoAnnotations.openMocks(this);
    }

    /**
     * @return
     */
    public static INotificationsManager getNotificationManager() {
        return applicationContext.getBean(INotificationsManager.class);
    }

    /**
     * @return @throws BusinessException
     */
    public static Notification getLastNotification() throws BusinessException {
        startRequest();
        List<Notification> notifications = getNotificationManager().getAllNotifications();
        endRequest();
        return notifications.get(0);
    }

    /**
     * @return @throws BusinessException
     */
    public String getCountNotifications() throws BusinessException {
        startRequest();
        String count = Integer.toString(getNotificationManager().getAllNotifications().size());
        endRequest();
        return count;
    }

    /**
     * @param message
     * @throws BusinessException
     */
    public void addNotification(String message) throws BusinessException {
        Notification notification = new Notification(Notification.INFO, message, null);
        getNotificationManager().addNotification(notification, getUtilisateurConnected());
    }

    /**
     * @param message
     * @param body
     * @throws BusinessException
     */
    public void addNotification(String message, String body) throws BusinessException {
        Notification notification = new Notification(Notification.INFO, message, body);
        getNotificationManager().addNotification(notification, getUtilisateurConnected());
    }

    /**
     * @param message
     * @param body
     * @param file
     * @throws BusinessException
     * @throws URISyntaxException
     */
    public void addNotification(String message, String body, String file) throws BusinessException, URISyntaxException {
        Notification notification = new Notification(Notification.INFO, message, body);
        notification.setAttachment(file);
        getNotificationManager().addNotification(notification, getUtilisateurConnected());
    }

    /**
     * @throws BusinessException
     * @throws URISyntaxException
     */
    public void readNotification() throws BusinessException, URISyntaxException {
        getNotificationManager().markNotificationAsArchived(getLastNotification().getId());
    }

    /**
     * @return @throws BusinessException
     */
    public String getUnreadNotifications() throws BusinessException {
        return String.valueOf(getNotificationManager().getAllNoArchivedByUserLogin(getUtilisateurConnected()).size());
    }

    /**
     * @param notification
     * @return
     */
    public String getBody(Notification notification) {
        if (notification == null) {
            return "";
        }
        return notification.getBody().replaceAll("<[^>]*>", "");
    }

    /**
     * @throws BusinessException
     * @throws URISyntaxException
     */
    public void deleteNotification() throws BusinessException, URISyntaxException {
        getNotificationManager().removeNotification(getLastNotification().getId());
    }

    /**
     * @throws BusinessException
     * @throws URISyntaxException
     */
    public void deleteAllNotifications() throws BusinessException, URISyntaxException {
        getNotificationManager().removeAllNotification(getPolicyManager().getCurrentUser().getId());
    }

}
