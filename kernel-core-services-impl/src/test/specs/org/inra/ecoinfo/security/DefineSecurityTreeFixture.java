package org.inra.ecoinfo.security;

import org.concordion.api.ExpectedToPass;
import org.inra.ecoinfo.*;
import org.inra.ecoinfo.mga.business.IMgaBuilder;
import org.inra.ecoinfo.mga.business.IMgaServiceBuilder;
import org.inra.ecoinfo.mga.business.MgaBuilder;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.*;

/**
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {TransactionalTestFixtureExecutionListener.class})
@ExpectedToPass
public class DefineSecurityTreeFixture extends AbstractTestFixture {

    /**
     *
     */
    public DefineSecurityTreeFixture() {
        super();
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     * @param path
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    public List<INode> addSecurityNodeToTree(String path) throws PersistenceException {

        //  TransactionStatus beginDefaultTransaction1 = genericDAO.beginDefaultTransaction();
        // boolean actualTransactionActive = TransactionSynchronizationManager.isActualTransactionActive();
        String path1 = getClass().getResource(String.format("../../../../%s", path)).getPath();

        try (Stream<String> stream = Files.lines(Paths.get(path1))) {

            stream.forEach(t -> {
                try {
                    saveNodeables(t);
                } catch (javax.persistence.PersistenceException | PersistenceException ex) {
                    LoggerFactory.getLogger(DefineSecurityTreeFixture.class.getName()).error(null, ex);
                }
            });

        } catch (IOException e) {
        }
        IMgaServiceBuilder mgaServiceBuilder = policyManager.getMgaServiceBuilder();
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
        IMgaRecorder mgaRecorder = mgaServiceBuilder.getRecorder();

        HashMap<String, INodeable> entities = new HashMap();

        mgaRecorder.getNodeables().forEach((n) -> {
            entities.put(MgaBuilder.getUniqueCode(
                    n, PatternConfigurator.ANCESTOR_SEPARATOR), n);
        });

        Stream<String> buildOrderedPaths = mgaBuilder.buildOrderedPaths(
                path1,
                configurator.getConfiguration(0)
                        .map(conf -> conf.getEntryOrder()).orElse(new Integer[0]),
                PatternConfigurator.SPLITER, true
        );
        Stream<INode> listChild = mgaBuilder.buildLeavesForPathes(
                buildOrderedPaths,
                configurator.getConfiguration(0)
                        .map(conf -> conf.getEntryType()).orElse(new Class[0]),
                entities,
                WhichTree.TREEDATASET,
                null);
        policyManager.persistNodes(listChild);
        applicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION);
        applicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS);
        return mgaRecorder.getNodesByTypeResource(WhichTree.TREEDATASET, Variable.class).collect(Collectors.toList());
    }

    private void saveNodeables(String path) throws javax.persistence.PersistenceException, PersistenceException {
        TransactionStatus beginDefaultTransaction = genericDAO.beginDefaultTransaction();
        String[] entities = path.split(";");
        Optional<Site> siteDB = policyManager.getMgaServiceBuilder().getRecorder().getNodeables(Site.class)
                .filter(x -> x.getCode().equals(entities[0]))
                .findFirst();
        Site site = siteDB.isPresent() ? (Site) siteDB.get() : new Site(entities[0]);
        Optional<Datatype> datatypeDB = policyManager.getMgaServiceBuilder().getRecorder().getNodeables(Datatype.class)
                .filter(x -> x.getCode().equals(entities[1]))
                .findFirst();
        Datatype datatype = datatypeDB.isPresent() ? (Datatype) datatypeDB.get() : new Datatype(entities[1]);
        Optional<Variable> variableDB = policyManager.getMgaServiceBuilder().getRecorder().getNodeables(Variable.class)
                .filter(x -> x.getCode().equals(entities[2]))
                .findFirst();
        Variable variable = variableDB.isPresent() ? (Variable) variableDB.get() : new Variable(entities[2]);
        genericDAO.saveOrUpdate(site);
        genericDAO.saveOrUpdate(datatype);
        genericDAO.saveOrUpdate(variable);
        genericDAO.commitTransaction(beginDefaultTransaction);
    }
}
