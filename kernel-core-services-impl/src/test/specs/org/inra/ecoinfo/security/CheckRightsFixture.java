package org.inra.ecoinfo.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.inra.ecoinfo.*;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.policyManager;

/**
 * @author tcherniatinsky
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {TransactionalTestFixtureExecutionListener.class})
public class CheckRightsFixture extends AbstractTestFixture {

    /**
     *
     */
    public static final Class TYPEINSTANCE_VARIABLE = Variable.class;

    /**
     *
     */
    public static final Class<? extends INodeable>[] INSTANCES = new Class[]{Site.class, Datatype.class, Variable.class};

    String currentUserLogin;

    /**
     *
     */
    public CheckRightsFixture() {
        super();
    }

    /**
     * @param currentUserLogin
     */
    public void setCurrentUserLogin(String currentUserLogin) {
        this.currentUserLogin = currentUserLogin;
    }

    /**
     * @param whichTreeCode
     * @param groupName
     * @return
     * @throws PersistenceException
     */
    public Set<Activity> getActivities(String whichTreeCode, String groupName) throws PersistenceException {
        Group group = policyManager.getGroupByGroupNameAndWhichTree(groupName, WhichTree.TREEDATASET)
                .orElseThrow(PersistenceException::new);
        Map<Activities, Map<Long, List<LocalDate>>> activitiesMap = group.getActivitiesMap();
        Set<Activity> activities = new TreeSet();
        activitiesMap.entrySet().stream()
                .forEach((activity) -> {
                    Stream<INode> nodesByIds = policyManager.getMgaServiceBuilder().getRecorder()
                            .getNodesByIds(new LinkedList(activity.getValue().keySet()), WhichTree.TREEDATASET);
                    nodesByIds.forEach((node) -> {
                        activities.add(
                                new Activity(
                                        activity.getKey(),
                                        node,
                                        activity.getValue().get(node.getId())
                                )
                        );
                    });
                });
        return activities;
    }

    /**
     * @param groupName
     * @param whichTreeCode
     * @param activityName
     * @param path
     * @param dateDeDebutString
     * @param dateDeFinString
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     * @throws DateTimeException
     * @throws BadExpectedValueException
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    public boolean addActivity(String groupName, String whichTreeCode, String activityName, String path, String dateDeDebutString, String dateDeFinString) throws BusinessException, PersistenceException, DateTimeException,
            BadExpectedValueException, JsonProcessingException {
        WhichTree whichTree = WhichTree.valueOf(whichTreeCode);
        Group group = policyManager.getGroupByGroupNameAndWhichTree(groupName, whichTree)
                .orElseThrow(PersistenceException::new);
        if (path.isEmpty()) {
            return false;
        }
        LocalDate dateDebut = dateDeDebutString.isEmpty() ? null : DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateDeDebutString).toLocalDate();
        LocalDate dateFin = dateDeFinString.isEmpty() ? null : DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateDeFinString).toLocalDate();
        List<LocalDate> dates = Arrays.asList(new LocalDate[]{dateDebut, dateFin});
        String[] rolesNames = activityName.split(",");
        for (int idx = 0; idx < rolesNames.length; idx++) {
            String r = rolesNames[idx];
            Activities activities = Activities.convertToActivity(r);
            Map<Long, List<LocalDate>> roles = group.getActivitiesMap().get(activities);
            if (roles == null) {
                group.getActivitiesMap().put(activities, new HashMap());
                roles = group.getActivitiesMap().get(activities);

            }
            String base = whichTree == WhichTree.TREEDATASET ? "NodeDataSet" : "NodeRefData";
            List<INode> nodes = policyManager.getMgaServiceBuilder().getRecorder().getEntityManager().
                    createQuery("from  " + base + " n where  n.realNode.path like :like")
                    .setParameter("like", path + "%").getResultList();
            if (nodes.isEmpty()) {
                return false;
            }
            Set<Long> ids = new HashSet();
            nodes.stream().map((node) -> {
                ids.add(node.getId());
                return node;
            }).map((node) -> {
                ArrayList<INode> tmp = new ArrayList();
                node.scanNodes(tmp);
                return tmp;
            }).forEach((tmp) -> {
                tmp.stream().forEach(t -> ids.add(t.getId()));
            });
            for (Long id : ids) {
                roles.put(id, dates);
            }
        }

        policyManager.mergeActivity(group);
        return true;
    }

    /**
     * @param groupName
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    public int persistExtractActivities(String groupName) throws PersistenceException {
        Group group = policyManager.getGroupByGroupNameAndWhichTree(groupName, WhichTree.TREEDATASET)
                .orElseThrow(PersistenceException::new);
        policyManager.persistExtractActivity(groupName, group.getActivitiesMap()
                .get(Activities.extraction));
        return group.getActivitiesMap().size();
    }

    /**
     * @param <T>
     * @return
     */
    protected <T extends INodeable> Class<T> getTypeResourceVariable() {
        return TYPEINSTANCE_VARIABLE;
    }
}

class HasAndMatchActivity {

    private boolean hasActivity;
    private boolean matchActivity;
    private boolean matchActivityWithRoles;

    public boolean ishasActivity() {
        return hasActivity;
    }

    public void sethasActivity(boolean hasActivity) {
        this.hasActivity = hasActivity;
    }

    public boolean isMatchActivity() {
        return matchActivity;
    }

    public void setMatchActivity(boolean matchActivity) {
        this.matchActivity = matchActivity;
    }

    public boolean isMatchActivityWithRoles() {
        return matchActivityWithRoles;
    }

    public void setMatchActivityWithRoles(boolean matchActivityWithRoles) {
        this.matchActivityWithRoles = matchActivityWithRoles;
    }
}
/**
 * @author ptcherniati
 */
