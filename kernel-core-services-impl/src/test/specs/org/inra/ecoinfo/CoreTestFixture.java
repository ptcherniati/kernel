package org.inra.ecoinfo;

import org.concordion.api.ExpectedToPass;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.ActivityCondition;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.*;

/**
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {TransactionalTestFixtureExecutionListener.class})
@ExpectedToPass
public class CoreTestFixture extends
        AbstractTestFixture {

    /**
     *
     */
    public CoreTestFixture() {
        super();
        startSession();
    }

    /**
     * @param codeConfig
     * @param login
     * @param activityCondition
     * @param activities
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    public static void loadtree(String codeConfig,
                                String login,
                                String activityCondition,
                                String activities) throws PersistenceException {
        ActivityCondition condition = getPrivilCondition(activityCondition);
        List<Activities> privs = Activities.convertToListActivities(activities);
        if (policyManager.isRoot()) {
            loadTreeForUser(Integer.parseInt(codeConfig), login);
            return;
        }
        loadTreeOfUserXRestrictedByCurrentUserActivityConditionOnLeafs(Integer.parseInt(codeConfig), login, condition, privs);
    }

    private static void loadTreeOfUserXRestrictedByCurrentUserActivityConditionOnLeafs(
            Integer codeConfiguration,
            String login,
            ActivityCondition conditionPrivil,
            List<Activities> conditions) throws PersistenceException {
        utilisateurDAO.getByLogin(login).ifPresent(user -> policyManager.restrictTreeOnLeafsAccordingActivities(
                policyManager.getOrLoadTree(user, codeConfiguration),
                conditionPrivil,
                conditions
        ));

    }

    private static ActivityCondition getPrivilCondition(String activityCondition) {
        return ActivityCondition.convertToActivityCondition(activityCondition.toUpperCase());
    }

    /**
     * *********************** LOAD Trees
     *
     * @param codeConf
     * @param login    /* Force Extends Query
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    public static ITreeNode<IFlatNode> loadTreeForUser(Integer codeConfiguration, String login) throws PersistenceException {
        return utilisateurDAO.getByLogin(login).map((user) -> policyManager.getOrLoadTree(user, codeConfiguration)).orElse(null);
    }
}
