package org.inra.ecoinfo.identification;

import org.concordion.api.ExpectedToPass;
import org.inra.ecoinfo.AbstractTestFixture;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;

import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.usersManager;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.utilisateurDAO;

/**
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {TransactionalTestFixtureExecutionListener.class})
@ExpectedToPass
public class LoadUsersFixture extends AbstractTestFixture {

    /**
     *
     */
    public LoadUsersFixture() {
        super();
    }

    /**
     * @param login
     * @param nom
     * @param prenom
     * @param email
     * @param password
     * @param isRoot
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     * @throws NoSuchAlgorithmException
     */
    public Utilisateur addUtilisateur(String login, String nom, String prenom, String email, String password, String isRoot) throws BusinessException, PersistenceException, NoSuchAlgorithmException {
        Utilisateur utilisateur = getUtilisateur(login);
        if (utilisateur == null) {
            utilisateur = new Utilisateur();
        }
        utilisateur.setLogin(login);
        utilisateur.setNom(nom);
        utilisateur.setPrenom(prenom);
        utilisateur.setEmail(email);
        utilisateur.setPassword(password);
        utilisateur.setIsRoot(Boolean.parseBoolean(isRoot));
        utilisateur.setActive(true);
        if (utilisateur.getId() == null) {
            usersManager.addUserProfile(utilisateur, "localhost");
        } else {
            utilisateurDAO.saveOrUpdate(utilisateur);
        }
        return utilisateur;
    }

    /**
     * @param login
     * @return
     * @throws BusinessException
     */
    public Utilisateur getUtilisateur(String login) throws BusinessException {
        Utilisateur utilisateur = usersManager.getUtilisateurByLogin(login).orElse(null);
        return utilisateur;
    }

    /**
     * @param utilisateur
     * @return
     */
    public String isSuperAdmin(Utilisateur utilisateur) {
        return utilisateur.getIsRoot() ? "super admin" : "utilisateur simple";
    }

}
