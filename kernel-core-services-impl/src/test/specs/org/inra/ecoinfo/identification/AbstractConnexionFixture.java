package org.inra.ecoinfo.identification;

import org.inra.ecoinfo.AbstractTestFixture;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.*;

/**
 * @author ptcherniati
 */
public abstract class AbstractConnexionFixture extends AbstractTestFixture {

    long debut = System.currentTimeMillis();

    /**
     *
     */
    public AbstractConnexionFixture() {
        super();
    }

    /**
     * @param utilisateur
     * @return
     */
    public String isSuperAdmin(Utilisateur utilisateur) {
        if (utilisateur == null) {
            return "null utilisateur";
        }
        return utilisateur.getIsRoot() ? "super admin" : "utilisateur simple";
    }

    /**
     * @param login
     * @param password
     * @return
     * @throws BusinessException
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    public Utilisateur connect(String login, String password) throws BusinessException, PersistenceException {
        startRequest();
        Utilisateur utilisateur = usersManager.checkPassword(login, password);
        getPolicyManager().setCurrentUser(utilisateur);
        endRequest();
        loadTree(login);
        return (Utilisateur) getPolicyManager().getCurrentUser();
    }

    @Transactional()
    private void loadTree(String login) throws PersistenceException {
        sessionCacheManager.clearFlatTree();
        applicationCacheManager.clearSkeletons();
        Utilisateur user = utilisateurDAO.getByLogin(login).orElseThrow(() -> new PersistenceException("no user"));
        final IMgaIOConfigurator mgaIOConfigurator = policyManager.getMgaServiceBuilder().getMgaIOConfigurator();
        applicationCacheManager.getConfigurationMap().values().stream()
                .flatMap(Collection::stream)
                .distinct()
                .forEach((i) -> {
                    try {
                        policyManager.getOrLoadTree(user, i);
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                });
    }
}
