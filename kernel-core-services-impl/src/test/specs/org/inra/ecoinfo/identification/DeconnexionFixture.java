package org.inra.ecoinfo.identification;

import org.concordion.api.ExpectedToPass;
import org.inra.ecoinfo.AbstractTestFixture;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.*;

/**
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {TransactionalTestFixtureExecutionListener.class})
@ExpectedToPass
public class DeconnexionFixture extends AbstractTestFixture {

    /**
     *
     */
    public DeconnexionFixture() {
        super();
    }

    /**
     * @return
     */
    public String deconnecte() {
        startRequest();
        getPolicyManager().releaseContext();
        Utilisateur utilisateur = (Utilisateur) getPolicyManager().getCurrentUser();
        endRequest();
        return utilisateur == null ? "vide" : utilisateur.getLogin();
    }

    @Override
    protected void finalize() throws Throwable {
        // TODO Auto-generated method stub
        super.finalize();
        endSession();
    }

}
