package org.inra.ecoinfo;

import org.concordion.internal.FixtureRunner;
import org.junit.runner.Description;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author ptcherniati
 */
public class ConcordionSpringJunit4ClassRunner extends SpringJUnit4ClassRunner {

    private final Description fixtureDescription;
    private final FrameworkMethod fakeMethod;

    /**
     * @param fixtureClass
     * @throws InitializationError
     * @throws NoSuchMethodException
     */
    public ConcordionSpringJunit4ClassRunner(Class<?> fixtureClass) throws InitializationError, NoSuchMethodException {
        super(fixtureClass);
        String testDescription = ("[Concordion Specification for '" + fixtureClass.getSimpleName()).replaceAll("Test$", "']");
        fixtureDescription = Description.createTestDescription(fixtureClass, testDescription);
        fakeMethod = new FrameworkMethod(this.getClass().getDeclaredMethod("computeTestMethods"));
    }

    /**
     * @return
     */
    @Override
    protected List<FrameworkMethod> computeTestMethods() {
        return new ArrayList<>(Arrays.asList(fakeMethod));
    }

    /**
     * @param method
     * @param test
     * @return
     */
    @Override
    protected Statement methodInvoker(FrameworkMethod method, final Object test) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                new FixtureRunner().run(test);
            }
        };
    }

    /**
     * @param method
     * @return
     */
    @Override
    protected Description describeChild(FrameworkMethod method) {
        return fixtureDescription;
    }
}
