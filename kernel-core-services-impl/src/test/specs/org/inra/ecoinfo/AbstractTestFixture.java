package org.inra.ecoinfo;

import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.*;

/**
 * @author ptcherniati
 */
public class AbstractTestFixture {

    /**
     *
     */
    public AbstractTestFixture() {
        super();
    }

    /**
     * @return @throws BusinessException
     */
    public String getUtilisateurConnected() throws BusinessException {
        Utilisateur utilisateur;
        utilisateur = (Utilisateur) getPolicyManager().getCurrentUser();
        if (utilisateur == null) {
            return "vide";
        }
        return utilisateur.getLogin();
    }

    /**
     * @return @throws BusinessException
     */
    public String getUtilisateurConnectedInRequest() throws BusinessException {
        startRequest();
        Utilisateur utilisateur;
        utilisateur = (Utilisateur) getPolicyManager().getCurrentUser();
        if (utilisateur == null) {
            return "vide";
        }
        endRequest();
        return utilisateur.getLogin();
    }

    /**
     * @return @throws PersistenceException
     */
    public String getCountUsers() throws PersistenceException {
        startRequest();
        return String.valueOf(utilisateurDAO.getAll(Utilisateur.class).size());
    }
}
