package org.inra.ecoinfo;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.dbunit.dataset.DataSetException;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.config.IModuleConfiguration;
import org.inra.ecoinfo.config.jpa.IGenericDAO;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.menu.IMenuManager;
import org.inra.ecoinfo.mga.caching.ITreeApplicationCacheManager;
import org.inra.ecoinfo.mga.caching.ITreeSessionCacheManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.viewadapter.ITreeBuilder;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.AbstractRequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

/**
 * @author ptcherniati
 */
public class TransactionalTestFixtureExecutionListener extends TransactionalTestExecutionListener {

    /**
     * The Constant LOGGER @link(Logger).
     */
    public static final Logger LOGGER = LoggerFactory.getLogger("tests");
    /**
     * The Constant UNDERSCORE @link(String).
     */
    public static final String UNDERSCORE = "_";
    /**
     * The Constant CSV @link(String).
     */
    public static final String CSV = ".csv";
    /**
     * The Constant LOG4J_PATH @link(String).
     */
    static final String LOG4J_PATH = "src/test/resources/log4jTest.properties";
    /**
     * The Constant CLEAN_ABSTRACT_ @link(String).
     */
    static final String CLEAN_ABSTRACT_ = "cleanAbstract";
    /**
     * The Constant INPUT_ABSTRACT @link(String).
     */
    static final String INPUT_ABSTRACT = "inputAbstract";
    /**
     * The session @link(MockHttpSession).
     */
    public static MockHttpSession session;
    /**
     * The request @link(MockHttpServletRequest).
     */
    public static MockHttpServletRequest request;
    /**
     * The application context @link(ApplicationContext).
     */
    public static ApplicationContext applicationContext;
    /**
     * The users manager @link(IUsersManager).
     */
    public static IUsersManager usersManager;
    /**
     *
     */
    public static IGenericDAO genericDAO;
    /**
     *
     */
    public static IMenuManager menuManager;
    /**
     * The configuration @link(Configuration).
     */
    public static IModuleConfiguration coreConfiguration;
    /**
     * The transaction manager @link(JpaTransactionManager).
     */
    public static JpaTransactionManager transactionManager;
    /**
     *
     */
    public static IUtilisateurDAO utilisateurDAO;
    /**
     *
     */
    public static ITreeBuilder treeBuilder;
    /**
     *
     */
    public static ITreeApplicationCacheManager applicationCacheManager;
    /**
     *
     */
    public static ITreeSessionCacheManager sessionCacheManager;
    /**
     *
     */
    public static IPolicyManager policyManager;
    /**
     * The is context loaded @link(boolean).
     */
    static boolean isContextLoaded = false;
    /**
     * The data source @link(DataSource).
     */
    static DataSource dataSource;
    /**
     * The datasource connection @link(DatabaseDataSourceConnection).
     */
    static DatabaseDataSourceConnection datasourceConnection;
    /**
     * The repository uri test @link(String).
     */
    static String repositoryUriTest;
    private static String testName = "";

    /**
     * End request.
     */
    public static void endRequest() {
        if (request == null) {
            return;
        }
        //getDataSource().getConnection().commit();
        ((AbstractRequestAttributes) RequestContextHolder.getRequestAttributes()).requestCompleted();
        RequestContextHolder.resetRequestAttributes();
        request = null;
    }

    /**
     * End session.
     */
    public static void endSession() {
        if (session == null) {
            return;
        }
        session.clearAttributes();
        session = null;
    }

    /**
     * Start request.
     */
    public static void startRequest() {
        request = new MockHttpServletRequest();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    /**
     * Start session.
     */
    public static void startSession() {
        session = new MockHttpSession();
    }

    /**
     * @return
     */
    public static DataSource getDataSource() {
        return dataSource;
    }

    /**
     * @return
     */
    public static String getRepositoryUriTest() {
        return repositoryUriTest;
    }

    /**
     * @return
     */
    public static DatabaseDataSourceConnection getDatasourceConnection() {
        return datasourceConnection;
    }

    /**
     * @return
     */
    public static IPolicyManager getPolicyManager() {
        return policyManager;
    }

    /**
     * Before test class.
     *
     * @param testContext
     * @throws Exception the exception @see
     *                   org.springframework.test.context.support.AbstractTestExecutionListener
     *                   #beforeTestClass(org.springframework.test.context.TestContext)
     * @link(TestContext) the test context
     */
    @Override
    public void beforeTestClass(final TestContext testContext) throws Exception {
        super.beforeTestClass(testContext);
        if (!isContextLoaded) {
            startSession();
            startRequest();
            // PropertyConfigurator.configure(LOG4J_PATH);
            try {
                applicationContext = testContext.getApplicationContext();
            } catch (Exception e) {
                LOGGER.error("can't load context", e);
                throw e;
            }
            usersManager = (IUsersManager) applicationContext.getBean("usersManager");
            policyManager = (IPolicyManager) applicationContext.getBean("policyManager");
            menuManager = (IMenuManager) applicationContext.getBean("menuManager");
            coreConfiguration = (IModuleConfiguration) applicationContext.getBean("coreConfiguration");
            transactionManager = (JpaTransactionManager) applicationContext.getBean("transactionManager");
            dataSource = (DataSource) applicationContext.getBean("dataSource");
            utilisateurDAO = (IUtilisateurDAO) applicationContext.getBean("utilisateurDAO");
            genericDAO = (IGenericDAO) applicationContext.getBean("genericDAO");
            treeBuilder = (ITreeBuilder) applicationContext.getBean("treeBuilder");
            applicationCacheManager = (ITreeApplicationCacheManager) applicationContext.getBean("treeApplicationCacheManager");
            sessionCacheManager = (ITreeSessionCacheManager) applicationContext.getBean("treeSessionCacheManager");
            repositoryUriTest = ((ICoreConfiguration) coreConfiguration).getRepositoryURI().replaceAll("(-test)?\057$", "-test/");
            ((ICoreConfiguration) coreConfiguration).setRepositoryURI(repositoryUriTest);
            if (testName.isEmpty()) {
                testName = "../target/concordion/" + testContext.getTestClass().getName().replaceAll(".*\\.(.*?)Fixture", "$1");
            }
            System.setProperty("concordion.output.dir", testName);
            final File repositoryTestFile = new File(repositoryUriTest);
            if (repositoryTestFile.exists()) {
                FileWithFolderCreator.recursifDelete(repositoryTestFile);
            }
            repositoryTestFile.mkdirs();
            final File[] contenu = repositoryTestFile.listFiles();
            for (final File file : contenu) {
                file.deleteOnExit();
            }
            new File(repositoryUriTest.concat("metadata")).mkdirs();
            new File(repositoryUriTest.concat("extraction")).mkdirs();
            new File(repositoryUriTest.concat("repository")).mkdirs();
            startSession();
            cleanTables();
            /*
             * createTestConfig(); cleanTables(); loadMetadata(); updateRoles();
             */
            isContextLoaded = true;
            endRequest();
            endSession();
        }
    }

    /**
     * Clean tables.
     *
     * @throws SQLException the sQL exception
     */
    protected void cleanTables() throws SQLException {
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from group_utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from groupe where group_name!='public';")
                .execute();
    }

    /**
     * Gets the datasource connexion.
     *
     * @return the datasource connexion
     * @throws SQLException the sQL exception
     */
    protected DatabaseDataSourceConnection getDatasourceConnexion() throws SQLException {
        datasourceConnection = new DatabaseDataSourceConnection(dataSource, "public");
        datasourceConnection.getConfig().setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);
        return datasourceConnection;
    }

    /**
     * After super class.
     *
     * @param testContext
     * @throws IOException           Signals that an I/O exception has occurred.
     * @throws DataSetException      the data set exception
     * @throws DatabaseUnitException the database unit exception
     * @throws SQLException          the sQL exception
     * @throws Exception             the exception
     * @link(TestContext) the test context
     * @link(TestContext) the test context
     */
    @Transactional(rollbackFor = Exception.class)
    void afterSuperClass(final TestContext testContext) throws IOException, DataSetException, DatabaseUnitException, SQLException, Exception {
        /*
         * final IDataSet dataSet = new FlatXmlDataSetBuilder().build(configurationTest.getFileByCode(CLEAN_ABSTRACT_)); cleanTables(); getDatasourceConnexion().getConnection().commit();
         * DatabaseOperation.CLEAN_INSERT.execute(getDatasourceConnexion(), dataSet); new File(repositoryUriTest).delete();
         */
        super.afterTestClass(testContext);
    }

    /**
     * Compare.
     *
     * @param metadataFile
     * @param metadataFileResult
     * @return true, if successful
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(File) the metadata file
     * @link(File) the metadata file result
     * @link(File) the metadata file
     * @link(File) the metadata file result
     */
    boolean compare(final File metadataFile, final File metadataFileResult) throws IOException {
        final CheckedInputStream resultInputStram;
        final long sourceChecksum;
        final long resultChecksum;
        try (CheckedInputStream originalInputStram = new CheckedInputStream(new FileInputStream(metadataFile.getPath()), new CRC32())) {
            resultInputStram = new CheckedInputStream(new FileInputStream(metadataFileResult.getPath()), new CRC32());
            final byte[] sourceBuf = new byte[128];
            final byte[] resultBuf = new byte[128];
            while (originalInputStram.read(sourceBuf) >= 0) {
            }
            while (resultInputStram.read(resultBuf) >= 0) {
            }
            sourceChecksum = originalInputStram.getChecksum().getValue();
            resultChecksum = resultInputStram.getChecksum().getValue();
        }
        resultInputStram.close();
        return sourceChecksum == resultChecksum;
    }
}
