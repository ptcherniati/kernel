package org.inra.ecoinfo.menu;

import org.inra.ecoinfo.AbstractTestFixture;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.*;

/**
 * @author ptcherniati
 */
public class AbstractMenuFixture extends AbstractTestFixture {

    String BUNDLE_NAME = "org.inra.ecoinfo.menu.menu";

    /**
     *
     */
    public AbstractMenuFixture() {
        super();
    }

    /**
     * @param locale
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public void initLocale(String locale) throws BusinessException {
        startRequest();
        getLocalisationManager().initLocale(new Locale(locale));
        endRequest();
    }

    /**
     * @return @throws CloneNotSupportedException
     */
    @Transactional
    public String getMenus() throws CloneNotSupportedException {
        startRequest();
        Menu menu = menuManager.buildRestrictedMenu();
        StringBuilder menusBuilder = new StringBuilder();
        menu.getMenuItems().stream()
                .filter((menuItem) -> (!menuItem.getDisabled()))
                .map((menuItem) -> {
                    menusBuilder.append("\n");
                    String label = menuItem.getLabel();
                    return label;
                })
                .map((label) -> getLocalisationManager().getMessage(BUNDLE_NAME, label)).map((label) -> {
            menusBuilder.append(label);
            return label;
        })
                .forEach((_item) -> {
                    menusBuilder.append(";");
                });
        endRequest();
        return menusBuilder.toString();
    }

    /**
     * @return
     */
    public String getLocale() {
        return getLocalisationManager().getUserLocale().getLanguage();
    }

    /**
     * @param locale
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    public void changeLocale(String locale) throws BusinessException {
        startRequest();
        getLocalisationManager().changeLocale(new Locale(locale));
        endRequest();
    }

    /**
     * @return
     */
    public ILocalizationManager getLocalisationManager() {
        ILocalizationManager localisationManager = applicationContext.getBean(ILocalizationManager.class);
        return localisationManager;
    }
}
