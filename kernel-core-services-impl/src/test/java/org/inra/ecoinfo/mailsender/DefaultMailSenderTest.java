package org.inra.ecoinfo.mailsender;

import java.util.Properties;
import javax.mail.Session;
import org.inra.ecoinfo.config.impl.CoreConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * @author ptcherniati
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(javax.mail.Session.class)
public class DefaultMailSenderTest {

    DefaultMailSender mailSender;
    @Mock
    CoreConfiguration configuration;

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mailSender = new DefaultMailSender();
        mailSender.setConfiguration(configuration);
    }

    /**
     * @throws Exception
     */
    @Test
    public void testInit() throws Exception {
        PowerMockito.mockStatic(Session.class);
        Mockito.when(configuration.getMailHost()).thenReturn("mailHost");
        new Answer<String>() {

            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                final Session session = (Session) invocation.callRealMethod();
                Assert.assertEquals("mailHost", session.getProperty("mail.smtp.host"));
                Assert.assertEquals("true", session.getProperty("mail.debug"));
                return null;
            }
        };
        Session.class.getMethod("getInstance", Properties.class);
        // PowerMockito.doAnswer(answer).when(Session.class, method);
        mailSender.init();
        // PowerMockito.verifyStatic();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.mailsender.IMailSender#sendMail(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */

    /**
     * @throws Exception
     */
    @Test
    public void testSendMail() throws Exception {
        testInit();
        mailSender.sendMail("from", "to", "subject", "message");
    }
}
