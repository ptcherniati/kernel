package org.inra.ecoinfo.identification;

import java.io.FileInputStream;
import java.util.List;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import junit.framework.TestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.exception.BadPasswordException;
import org.inra.ecoinfo.identification.exception.UnknownUserException;
import org.inra.ecoinfo.identification.impl.DefaultUsersManager;
import org.inra.ecoinfo.mailsender.IMailSender;
import org.inra.ecoinfo.utils.DatabaseTest;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.After;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author tcherniatinsky
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(ConnexionTestTI.USER_CONTEXT)
public class ConnexionTestTI extends TestCase {

    /**
     *
     */
    public static final String USER_CONTEXT = "/org/inra/ecoinfo/identification/user_context.xml";

    /**
     *
     */
    public final static String FILENAME = "user.xml";
    /**
     *
     */
    public static IDataSet dataSet;
    private static IDatabaseTester databaseTester;
    /**
     * @param databaseTester
     * @throws DataSetException
     * @throws Exception
     */
    public static void tearDownUsersTable(IDatabaseTester databaseTester) throws DataSetException, Exception {
        IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(FILENAME));
        DatabaseOperation.DELETE_ALL.execute(databaseTester.getConnection(), dataSet);
    }
    /**
     * @param databaseTester
     * @throws DataSetException
     * @throws Exception
     */
    public static void initUsersTable(IDatabaseTester databaseTester) throws DataSetException, Exception {
        IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(FILENAME));
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
        DatabaseOperation.CLEAN_INSERT.execute(databaseTester.getConnection(), dataSet);
    }
    /**
     * @param databaseTester
     */
    public static void setDatabaseTester(IDatabaseTester databaseTester) {
        databaseTester = databaseTester;
    }
    /**
     *
     */
    @Autowired
    protected IMailSender mailSender;
    /**
     *
     */
    @PersistenceContext(unitName = "JPAPu")
    protected EntityManager entityManager;
    @Autowired
    private IUsersManager service;


    /**
     * @throws Exception
     */
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();

        //Connexion � la base de donn�es
        databaseTester = DatabaseTest.JDBCDatabaseInstance();
        initUsersTable(databaseTester);
    }

    /**
     * @throws Exception
     */
    @After
    @Override
    public void tearDown() throws Exception {
        super.tearDown();

        service = null;
    }

    /**
     * @throws BusinessException
     */
    @Test
    public void testRretrieveAllUsers() throws BusinessException {
        List<Utilisateur> users = service.retrieveAllUsers();
        assertTrue(users.size() == 4);

    }

    /**
     * @throws BusinessException
     */
    @Test
    public void testGetMailAdmin() throws BusinessException {
        String mail = service.getMailAdmin();
        assertEquals("appli@orleans.inra.fr", mail);

    }

    /**
     * @throws BusinessException
     */
    @Test
    public void testResetPassword() throws BusinessException {
        Utilisateur utilisateur = service.getUtilisateurByEmail("testeuractive@moncahier.com");
        assertEquals(utilisateur, service.checkPassword("testeuractive", "testeur"));
        service.resetPassword(utilisateur, "localhost", Locale.FRANCE);
        assertEquals(utilisateur, service.checkPassword("testeuractive", "8815b7196884f2d17ffd001eca8f0d63"));
        try {
            service.checkPassword("testeuractive", "testeur");
            fail("passowrd not changed");
        } catch (BusinessException e) {
            assertEquals(DefaultUsersManager.MSG_BAD_PASSWORD, e.getMessage());
        }
    }

    /**
     * @throws BusinessException
     */
    @Test
    public void testRemoveUserProfil() throws BusinessException {
        Utilisateur utilisateur = service.getUtilisateurByEmail("testeuractive@moncahier.com");
        assertNotNull(utilisateur);
        service.deleteUserProfile(utilisateur);
        utilisateur = service.getUtilisateurByEmail("testeuractive@moncahier.com");
        assertNull(utilisateur);
    }

    /**
     * @throws BusinessException
     */
    @Test
    public void testAddUserProfil() throws BusinessException {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setEmail("mail@adding.com");
        utilisateur.setLogin("addedUser");
        utilisateur.setPassword("addedPassword");
        utilisateur.setNom("added");
        utilisateur.setPrenom("user");
        //nominal
        service.addUserProfile(utilisateur, "localhost");
        assertNotNull(utilisateur);
        assertEquals(utilisateur, service.getUtilisateurByEmail("mail@adding.com"));
        final Utilisateur bdUser = service.getUtilisateurByLogin("addedUser").orElse(null);
        assertEquals(utilisateur, bdUser);
        //new inactiveuser
        try {
            assertEquals(bdUser, service.checkPassword("addedUser", "addedPassword"));
            fail("not inactive user");
        } catch (BusinessException e) {
            assertEquals(DefaultUsersManager.MSG_INACTIVE_USER, e.getMessage());
            assertFalse(bdUser.getActive());
        }
        service.activateUser("addedUser");
        //new activeuser
        assertEquals(bdUser, service.checkPassword("addedUser", "addedPassword"));
        //encryptedPassword
        assertNotEquals("addedPassword", utilisateur.getPassword());
        //tryAdding existing email
        try {
            service.addUserProfile(utilisateur, "localhost");
            fail("duplicate user by mail");
        } catch (BusinessException e) {
            assertEquals("L'adresse mail mail@adding.com existe déjà!", e.getMessage());
        }
        try {
            utilisateur.setEmail("new@email.com");
            service.addUserProfile(utilisateur, "localhost");
            fail("duplicate user by login");
        } catch (BusinessException e) {
            assertEquals("Le login addedUser existe déjà!", e.getMessage());
        }
    }

    /**
     * @throws BusinessException
     */
    @Test
    public void testGetUtilisateurByEmail() throws BusinessException {
        Utilisateur utilisateur = service.getUtilisateurByEmail("testeuractive@moncahier.com");
        assertNotNull(utilisateur);
        assertEquals("testeuractive", utilisateur.getLogin());

    }

    /**
     * @throws BusinessException
     */
    @Test
    public void testGetUtilisateurByLogin() throws BusinessException {
        Utilisateur utilisateur = service.getUtilisateurByLogin("testeuractive").orElse(null);
        assertNotNull(utilisateur);
        assertEquals("testeuractive", utilisateur.getLogin());

    }

    /**
     * @throws BusinessException
     */
    @Test
    public void testCheckPassword() throws BusinessException {
        Utilisateur utilisateur = service.checkPassword("admin", "admin");
        assertNotNull(utilisateur);
        assertEquals("admin", utilisateur.getLogin());
        //unknown user
        try {
            service.checkPassword("admine", "admin");
            fail("user not exists");
        } catch (UnknownUserException e) {
            assertEquals(DefaultUsersManager.MSG_UNKNOWN_USER, e.getMessage());
        }
        //bad password
        try {
            service.checkPassword("admin", "admine");
            fail("user bad password");
        } catch (BadPasswordException e) {
            assertEquals(DefaultUsersManager.MSG_BAD_PASSWORD, e.getMessage());
        }

    }

    /**
     * @return
     */
    public IDatabaseTester getDatabaseTester() {
        return databaseTester;
    }



}
