package org.inra.ecoinfo.identification.impl;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.identification.IRightRequestDAO;
import org.inra.ecoinfo.identification.IUsersDeletion;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.RightRequest;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.mailsender.IMailSender;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.DeleteUserException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.*;
import static org.junit.Assert.*;
import org.mockito.ArgumentCaptor;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.LoggerFactory;

/**
 * @author ptcherniati
 */
public class DefaultUsersManagerTest {

    /**
     *
     */
    protected static final String MAIL_ADMIN = "mailAdmin";
    private static final String LOGIN = "login";
    /**
     * @throws Exception
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
    }
    /**
     * @throws Exception
     */
    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    DefaultUsersManager instance;
    @Mock(name = "admin")
    Utilisateur utilisateur;
    @Mock(name = "oldAdmin")
    Utilisateur oldUtilisateur;
    @Mock
    Utilisateur admin;
    List<Utilisateur> admins = new LinkedList();
    @Mock
    IUtilisateurDAO utilisateurDAO;
    @Mock
    ICoreConfiguration configuration;
    @Mock
    IPolicyManager policyManager;
    @Mock
    INotificationsManager notificationsManager;
    @Mock
    IMailSender mailSender;
    @Mock
    IRightRequestDAO rightRequestDAO;
    @Mock
    RightRequest rightRequest;
    @Mock
    IMgaRecorder recorder;
    @Mock
    Group groupDataset;
    @Mock
    Group groupRefdata;
    @Mock(name = "manager de dataset")
    IUsersDeletion usersDeletion1;
    @Mock(name = "manager de VersionFile")
    IUsersDeletion usersDeletion2;
    @Mock(name = "manager de FileComp")
    IUsersDeletion usersDeletion3;
    @Mock(name = "inforeport du manager de dataset")
    InfosReport infosReport1;
    @Mock(name = "inforeport du manager de VersionFile")
    InfosReport infosReport2;
    @Mock(name = "inforeport du manager de FileComp")
    InfosReport infosReport3;


    /**
     *
     * @throws org.inra.ecoinfo.utils.exceptions.DeleteUserException
     */
    @Before
    public void setUp() throws DeleteUserException {
        MockitoAnnotations.openMocks(this);
        instance = new DefaultUsersManager();
        instance.setUtilisateurDAO(utilisateurDAO);
        instance.setConfiguration(configuration);
        instance.setMailSender(mailSender);
        instance.setRecorder(recorder);
        instance.setPolicyManager(policyManager);
        instance.setNotificationsManager(notificationsManager);
        Mockito.doReturn(WhichTree.TREEDATASET).when(groupDataset).getWhichTree();
        Mockito.doReturn("login").when(groupDataset).getGroupName();
        Mockito.doReturn(WhichTree.TREEREFDATA).when(groupRefdata).getWhichTree();
        Mockito.doReturn("login").when(groupRefdata).getGroupName();
        admins.add(admin);
        Localization.addLanguage("fr");
        Localization.setDefaultLocalisation("fr");

        when(utilisateurDAO.getByEMail(anyString())).thenReturn(Optional.empty());
        when(utilisateurDAO.getByLogin(anyString())).thenReturn(Optional.empty());

    }

    /**
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testAddUserProfile() throws BusinessException, PersistenceException {
        final Answer<Integer> answer = (InvocationOnMock invocation) -> {
            final Object[] args = invocation.getArguments();
            Assert.assertEquals("mailAdmin", args[0]);
            Assert.assertEquals("email", args[1]);
            Assert.assertEquals("Procédure de création de compte.", args[2]);
            Assert.assertEquals("Bonjour prenom nom\nVotre login est : login\n" + "Votre mot de passe est : vous êtes le seul à le savoir !\n" + "Pour activer votre compte, cliquez sur le lien ci dessous\n"
                    + "base/validateAccount.jsf?activate=true&login=login", args[3]);
            return 1;
        };
        doAnswer(answer).when(mailSender).sendMail(anyString(), anyString(), anyString(), anyString());
        when(configuration.getMailAdmin()).thenReturn("mailAdmin");
        when(utilisateur.getEmail()).thenReturn("email");
        when(utilisateur.getLogin()).thenReturn("login");
        when(utilisateur.getNom()).thenReturn("nom");
        when(utilisateur.getPrenom()).thenReturn("prenom");
        when(utilisateurDAO.getByEMail("email")).thenReturn(Optional.empty());
        when(utilisateurDAO.getByLogin("login")).thenReturn(Optional.empty());
        instance.addUserProfile(utilisateur, "base");
        verify(utilisateurDAO).saveOrUpdate(utilisateur);
    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     */
    @Test(expected = BusinessException.class)
    public void testAddUserProfilePersistenceException() throws BusinessException, PersistenceException {
        doThrow(new PersistenceException()).when(utilisateurDAO).saveOrUpdate(utilisateur);
        instance.addUserProfile(utilisateur, "base");
    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testCheckPassword() throws BusinessException, PersistenceException {
        when(utilisateurDAO.getByLogin("admin")).thenReturn(Optional.ofNullable(utilisateur));
        when(utilisateur.getPassword()).thenReturn("21232f297a57a5a743894a0e4a801fc3");
        when(utilisateur.getActive()).thenReturn(true);

        final Utilisateur check = instance.checkPassword("admin", "admin");
        Assert.assertTrue(check.equals(utilisateur));
    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     */
    @Test(expected = BusinessException.class)
    public void testCheckPasswordBadPassword() throws BusinessException, PersistenceException {
        when(utilisateurDAO.getByLogin("admin")).thenReturn(Optional.ofNullable(utilisateur));
        // when(utilisateur.getPassword()).thenReturn("3uRgXQ6mgKAg4675T/tqo7J7/QkoUsw");
        when(utilisateur.getPassword()).thenReturn("21232f297a57a5a743894a0e4a801fc3-RAC");
        instance.checkPassword("admin", "admin");
    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     */
    @Test(expected = BusinessException.class)
    public void testCheckPasswordNullUser() throws BusinessException, PersistenceException {
        when(utilisateurDAO.getByLogin("admin")).thenReturn(Optional.empty());
        instance.checkPassword("admin", "admin");
    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testDeleteUserProfileWithoutDependances() throws BusinessException, PersistenceException {
        when(utilisateur.getLogin()).thenReturn("UserTest");
        when(utilisateur.getIsRoot()).thenReturn(Boolean.FALSE);
        when(policyManager.getCurrentUserLogin()).thenReturn("admin");
        doNothing().when(notificationsManager).addNotification(any(Notification.class), eq("admin"));
        when(utilisateurDAO.merge(utilisateur)).thenReturn(utilisateur);
        when(utilisateurDAO.merge(utilisateur)).thenReturn(utilisateur);
        instance.setLocalizationManager(new SpringLocalizationManager());
        InfosReport resultReport = instance.deleteUserProfile(utilisateur);
        String resultReportMessage = resultReport.getMessages();

        String resultChecker = "Une demande de suppression pour l'utilisateur UserTest a été demandée. Success_User_Deletion\t - L'utilisateur UserTest a bien été supprimé. \n";
        assertEquals(resultChecker, resultReportMessage);
        verify(utilisateurDAO, times(1)).remove(utilisateur);
        ArgumentCaptor<Notification> notif = ArgumentCaptor.forClass(Notification.class);
        verify(notificationsManager).addNotification(notif.capture(), eq("admin"));
        resultChecker = "<p><p style='color: green'>Success_User_Deletion :</p><p style='text-indent: 30px'>- L'utilisateur UserTest a bien été supprimé. </p></p>";
        assertEquals(resultChecker, notif.getValue().getBody());

    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     * @throws org.inra.ecoinfo.utils.exceptions.DeleteUserException
     */
    @Test
    public void testSuccessDeleteDependancesUserProfile() throws BusinessException, PersistenceException, DeleteUserException {
        when(utilisateur.getLogin()).thenReturn("UserTest");
        when(utilisateurDAO.merge(utilisateur)).thenReturn(utilisateur);
        when(utilisateur.getIsRoot()).thenReturn(Boolean.FALSE);
        when(policyManager.getCurrentUserLogin()).thenReturn("admin");
        doNothing().when(notificationsManager).addNotification(any(Notification.class), eq("admin"));

        String messageReport1 = "Hello je suis le messageReport 1";
        String messageReport2 = "Hello je suis le messageReport 2";
        String messageReport3 = "Hello je suis le messageReport 3";

        when(usersDeletion1.deleteDependancesUserProfile(utilisateur)).thenReturn(infosReport1);
        when(usersDeletion2.deleteDependancesUserProfile(utilisateur)).thenReturn(infosReport2);
        when(usersDeletion3.deleteDependancesUserProfile(utilisateur)).thenReturn(infosReport3);
        Map<String, List<String>> infoMessageMap1 = new HashMap<>();
        List<String> infoMessageList1 = new LinkedList<>();
        infoMessageList1.add(messageReport1);
        infoMessageMap1.put("dataSet", infoMessageList1);
        when(infosReport1.getinfoMessages()).thenReturn(infoMessageMap1);
        Map<String, List<String>> infoMessageMap2 = new HashMap<>();
        List<String> infoMessageList2 = new LinkedList<>();
        infoMessageList2.add(messageReport2);
        infoMessageMap2.put("VersionFile", infoMessageList2);
        when(infosReport2.getinfoMessages()).thenReturn(infoMessageMap2);
        Map<String, List<String>> infoMessageMap3 = new HashMap<>();
        List<String> infoMessageList3 = new LinkedList<>();
        infoMessageList3.add(messageReport3);
        infoMessageMap3.put("FilComp", infoMessageList3);
        when(infosReport3.getinfoMessages()).thenReturn(infoMessageMap3);

        instance.setLocalizationManager(new SpringLocalizationManager());
        Map<String, IUsersDeletion> userDeletionMap = new LinkedHashMap<>();
        userDeletionMap.put("dataSet", usersDeletion1);
        userDeletionMap.put("VersionFile", usersDeletion2);
        userDeletionMap.put("FilComp", usersDeletion3);
        instance.setUsersDeletionManagers(userDeletionMap);

        InfosReport reportResult = instance.deleteUserProfile(utilisateur);
        String messageResult = reportResult.buildHTMLMessages();

        verify(usersDeletion1, times(1)).deleteDependancesUserProfile(utilisateur);
        verify(usersDeletion2, times(1)).deleteDependancesUserProfile(utilisateur);
        verify(usersDeletion3, times(1)).deleteDependancesUserProfile(utilisateur);

        String successMessageResultChecker = "<p style='color: green'>dataSet :</p><p style='text-indent: 30px'>- Hello je suis le messageReport 1 </p><p style='color: green'>VersionFile :</p><p style='text-indent: 30px'>- Hello je suis le messageReport 2 </p><p style='color: green'>FilComp :</p><p style='text-indent: 30px'>- Hello je suis le messageReport 3 </p><p style='color: green'>Success_User_Deletion :</p><p style='text-indent: 30px'>- L'utilisateur UserTest a bien été supprimé. </p>";
        assertEquals(successMessageResultChecker, messageResult);
        verify(utilisateurDAO, times(1)).remove(utilisateur);
    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     * @throws org.inra.ecoinfo.utils.exceptions.DeleteUserException
     */
    @Test
    public void testEchecDeleteDependancesUserProfile() throws BusinessException, PersistenceException, DeleteUserException {
        when(utilisateur.getLogin()).thenReturn("UserTest");
        when(utilisateurDAO.merge(utilisateur)).thenReturn(utilisateur);

        when(usersDeletion1.deleteDependancesUserProfile(utilisateur)).thenThrow(new DeleteUserException("Error_Checking"));
        when(usersDeletion2.deleteDependancesUserProfile(utilisateur)).thenReturn(infosReport2);
        when(usersDeletion3.deleteDependancesUserProfile(utilisateur)).thenReturn(infosReport3);

        String messageReport2 = "Hello je suis le messageReport 2";
        String messageReport3 = "Hello je suis le messageReport 3";

        when(infosReport2.getMessages()).thenReturn(messageReport2);
        when(infosReport3.getMessages()).thenReturn(messageReport3);

        instance.setLocalizationManager(new SpringLocalizationManager());

        Map<String, IUsersDeletion> userDeletionMap = new LinkedHashMap<>();
        userDeletionMap.put("dataSet", usersDeletion1);
        userDeletionMap.put("VersionFile", usersDeletion2);
        userDeletionMap.put("FilComp", usersDeletion3);
        when(utilisateur.getIsRoot()).thenReturn(Boolean.FALSE);
        when(policyManager.getCurrentUserLogin()).thenReturn("admin");
        doNothing().when(notificationsManager).addNotification(any(Notification.class), eq("admin"));
        instance.setUsersDeletionManagers(userDeletionMap);

        InfosReport reportResult = instance.deleteUserProfile(utilisateur);
        String messageResult = reportResult.buildHTMLMessages();

        verify(usersDeletion1, times(1)).deleteDependancesUserProfile(utilisateur);
        verify(usersDeletion2, times(1)).deleteDependancesUserProfile(utilisateur);
        verify(usersDeletion3, times(1)).deleteDependancesUserProfile(utilisateur);
        String echecMessageResultChecker = "<p style='color: green'>Error_User_Deletion :</p><p style='text-indent: 30px'>- Une erreur a été détectée lors de la suppression de l'utilisateur UserTest. Voici le rapport d'erreur(s) : DeleteUserException	 - Error_Checking \n </p>";
        assertTrue(messageResult.equals(echecMessageResultChecker));
        verify(utilisateurDAO, never()).remove(utilisateur);
    }

    /**
     * @throws BusinessException
     */
    @SuppressWarnings("unchecked")
    @Test(expected = BusinessException.class)
    public void testDeleteUserProfileThrowDeleteException() throws BusinessException {
        instance.deleteUserProfile(null);
    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testDeleteAdministratorProfile() throws BusinessException, PersistenceException {
        when(utilisateur.getLogin()).thenReturn("AdministratorTest");
        when(utilisateur.getIsRoot()).thenReturn(true);

        instance.setLocalizationManager(new SpringLocalizationManager());

        InfosReport result = instance.deleteUserProfile(utilisateur);
        String reportMessage = result.buildHTMLMessages();

        verify(utilisateurDAO, never()).remove(utilisateur);
        assertEquals(true, reportMessage.equals("<p style='color: green'>Administrator_Deletion :</p><p style='text-indent: 30px'>- Action impossible : l'utilisateur AdministratorTest est un administrateur, il ne peut donc être supprimé. </p>"));
    }

    /**
     * @throws PersistenceException
     * @throws BusinessException
     */
    @Test
    public void testGetUtilisateurByLogin() throws PersistenceException, BusinessException {
        when(utilisateurDAO.getByLogin("admin")).thenReturn(Optional.ofNullable(utilisateur));
        instance.getUtilisateurByLogin("admin");
    }

    /**
     * @throws PersistenceException
     * @throws BusinessException
     */
    @Test
    public void testInitialise() throws BusinessException, PersistenceException {
        when(utilisateurDAO.getById((long) 25)).thenReturn(Optional.ofNullable(utilisateur));
        when(utilisateur.getId()).thenReturn((long) 25);
        // when(utilisateur.getPassword()).thenReturn("password");
        when(utilisateur.getPassword()).thenReturn("21232f297a57a5a743894a0e4a801fc3");
        when(utilisateur.getLogin()).thenReturn("login");
        when(utilisateur.getEmail()).thenReturn("email");
        when(utilisateur.getNom()).thenReturn("nom");
        when(utilisateur.getPrenom()).thenReturn("prenom");
        when(utilisateur.getIsRoot()).thenReturn(true);
        final Answer<String> answer = new Answer<String>() {

            public String password;

            // final BasicPasswordEncryptor basicPasswordEncryptor = new BasicPasswordEncryptor();
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                password = (String) args[0];
                if (password.equals("21232f297a57a5a743894a0e4a801fc3")) {
                    return password;
                }

                MessageDigest md = null;
                BigInteger bI = null;
                try {
                    md = MessageDigest.getInstance("MD5");
                    byte[] thedigest = md.digest("admin".getBytes());
                    bI = new BigInteger(1, thedigest);

                } catch (NoSuchAlgorithmException ex) {
                    LoggerFactory.getLogger(DefaultUsersManager.class).error(ex.getMessage(), ex);
                }

                Assert.assertTrue(bI.toString(16).equals("21232f297a57a5a743894a0e4a801fc3"));
                return password;
            }
        };

        doAnswer(answer).when(utilisateur).setPassword(anyString());
        instance.initialise(utilisateur);
        verify(utilisateur).setEmail("email");
        verify(utilisateur).setNom("nom");
        verify(utilisateur).setPrenom("prenom");
        verify(utilisateur).setIsRoot(true);
        verify(utilisateur).setLogin("login");
        verify(utilisateurDAO).saveOrUpdate(utilisateur);
    }

    /**
     * @throws PersistenceException
     * @throws BusinessException
     */
    @SuppressWarnings("unchecked")
    @Test(expected = BusinessException.class)
    public void testInitialiseWithBusinessException() throws PersistenceException, BusinessException {
        when(utilisateur.getId()).thenReturn((long) 25);
        when(utilisateurDAO.getById((long) 25)).thenReturn(Optional.empty());
        instance.initialise(utilisateur);
    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testResetPassword() throws BusinessException, PersistenceException {
        doNothing().when(spy(instance)).initialise(utilisateur);
        when(utilisateur.getId()).thenReturn((long) 25);
        when(utilisateurDAO.getById((long) 25)).thenReturn(Optional.ofNullable(utilisateur));
        when(configuration.getMailAdmin()).thenReturn("mailAdmin");
        when(utilisateur.getId()).thenReturn((long) 25);
        when(utilisateur.getPassword()).thenReturn("21232f297a57a5a743894a0e4a801fc3");
        when(utilisateur.getLogin()).thenReturn("login");
        when(utilisateur.getEmail()).thenReturn("email");
        when(utilisateur.getNom()).thenReturn("nom");
        when(utilisateur.getPrenom()).thenReturn("prenom");
        when(utilisateur.getIsRoot()).thenReturn(true);
        final Answer<Integer> answer = (InvocationOnMock invocation) -> {
            final Object[] args = invocation.getArguments();
            Assert.assertEquals("mailAdmin", args[0]);
            Assert.assertEquals("email", args[1]);
            Assert.assertEquals("Procédure de réinitialisation de vos mots identifiants.", args[2]);
            Assert.assertEquals("Bonjour prenom nom<br /><br />\n"
                    + "\n"
                    + "Votre login est login<br />\n"
                    + "Votre mot de passe à été réinitialisé<br />\n"
                    + "Pour pouvoir le récupérer, cliquez sur le lien ci dessous<br /><br />\n"
                    + "\n"
                    + "base/retrievePassword.jsf?retrievePassword=true&mail=email&pass=21232f297a57a5a743894a0e4a801fc3", args[3]);
            return 1;
        };
        doAnswer(answer).when(mailSender).sendMail(anyString(), anyString(), anyString(), anyString());
        instance.resetPassword(utilisateur, "base", Locale.FRANCE);
    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Test(expected = BusinessException.class)
    public void testResetPasswordWithUnsupportedEncodingException() throws BusinessException, PersistenceException {
        when(utilisateurDAO.getById(anyLong())).thenReturn(Optional.empty());
        instance.resetPassword(utilisateur, "base", Locale.FRANCE);
    }

    /**
     * @throws BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testRetrieveAllUsers() throws BusinessException, PersistenceException {
        final List<Utilisateur> utilisateurs = new LinkedList<>();
        utilisateurs.add(utilisateur);
        when(configuration.getAnonymousLogin()).thenReturn("anonymous");
        when(utilisateurDAO.getAll()).thenReturn(utilisateurs);
        final List<Utilisateur> returnUtilisateurs = instance.retrieveAllUsers();
        Assert.assertEquals(utilisateurs, returnUtilisateurs);
    }

    /**
     * Test of addUserProfile method, of class DefaultUsersManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testAddUserProfile_Utilisateur_String() throws Exception {
        instance = spy(instance);
        doNothing().when(instance).addUserProfile(utilisateur, "baseUrl", null);
        instance.addUserProfile(utilisateur, "baseUrl");
        verify(instance).addUserProfile(utilisateur, "baseUrl", null);
    }

    /**
     * Test of addUserProfile method, of class DefaultUsersManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testAddUserProfile_3args() throws Exception {
        instance = Mockito.spy(instance);
        final List<Utilisateur> otherRecipients = Arrays.asList(new Utilisateur[]{admin});
        when(configuration.getMailAdmin()).thenReturn(MAIL_ADMIN);
        when(utilisateur.getEmail()).thenReturn("email");
        when(utilisateur.getNom()).thenReturn("nom");
        when(utilisateur.getPrenom()).thenReturn("prénom");
        when(utilisateur.getLogin()).thenReturn("login");
        when(utilisateur.getIsRoot()).thenReturn(true);
        when(utilisateur.getEmploi()).thenReturn("emploi");
        when(utilisateur.getPoste()).thenReturn("poste");
        when(utilisateur.getPassword()).thenReturn("password");
        when(utilisateur.getEmail()).thenReturn("email");
        when(utilisateur.getLanguage()).thenReturn("fr");
        when(utilisateurDAO.getByEMail("email")).thenReturn(Optional.empty());
        when(utilisateurDAO.getByLogin("login")).thenReturn(Optional.empty());
        ArgumentCaptor<IUser> userCapt = ArgumentCaptor.forClass(IUser.class);
        ArgumentCaptor<Group> groupCapt = ArgumentCaptor.forClass(Group.class);
        when(utilisateurDAO.addGroupToUser(utilisateur, groupDataset)).thenReturn(Optional.ofNullable(utilisateur));
        when(utilisateurDAO.addGroupToUser(utilisateur, groupRefdata)).thenReturn(Optional.ofNullable(utilisateur));
        Mockito.doReturn(groupDataset).when(instance).createAndSaveGroup("login", WhichTree.TREEDATASET);
        Mockito.doReturn(groupRefdata).when(instance).createAndSaveGroup("login", WhichTree.TREEREFDATA);
        instance.addUserProfile(utilisateur, "baseUrl", otherRecipients);
        verify(utilisateurDAO).saveOrUpdate(utilisateur);
        verify(utilisateurDAO).addGroupToUser(utilisateur, groupDataset);
        verify(utilisateurDAO).addGroupToUser(utilisateur, groupRefdata);
        verify(mailSender).sendMail("mailAdmin", "email", otherRecipients, "Procédure de création de compte.", "Bonjour prénom nom<br /><br />\n"
                + "\n"
                + "Votre login est : login<br />\n"
                + "Votre mot de passe est : vous êtes le seul à le savoir !<br />\n"
                + "\n"
                + "Vous avez fait une demande d'ouverture de compte sur SOERE MONSOERE<br />\n"
                + "Pour activer votre compte, cliquez sur le lien ci dessous<br /><br />\n"
                + "\n"
                + "baseUrl/validateAccount.jsf?activate=true&login=login");
        //existing user
        when(utilisateurDAO.getByEMail("email")).thenReturn(Optional.ofNullable(utilisateur));
        try {
            instance.addUserProfile(utilisateur, "baseUrl", otherRecipients);
            fail("noexception");
        } catch (BusinessException e) {
            assertEquals("L'adresse mail email existe déjà!", e.getMessage());
        }
        //persistence exception
        when(utilisateurDAO.getByEMail("email")).thenReturn(Optional.empty());
        doThrow(new PersistenceException("erreur")).when(utilisateurDAO).saveOrUpdate(utilisateur);
        try {
            instance.addUserProfile(utilisateur, "baseUrl", otherRecipients);
            fail("noexception");
        } catch (BusinessException e) {
            assertEquals("can't save user", e.getMessage());
        }
    }

    /**
     * Test of sendValidationEmail method, of class DefaultUsersManager.
     */
    @Test
    public void testSendValidationEmail() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(DefaultUsersManager.BUNDLE_NAME, new Locale("fr"));
        when(configuration.getMailAdmin()).thenReturn(MAIL_ADMIN);
        when(utilisateur.getEmail()).thenReturn("email");
        when(utilisateur.getNom()).thenReturn("nom");
        when(utilisateur.getPrenom()).thenReturn("prénom");
        when(utilisateur.getLogin()).thenReturn("login");
        when(utilisateur.getIsRoot()).thenReturn(true);
        when(utilisateur.getEmploi()).thenReturn("emploi");
        when(utilisateur.getPoste()).thenReturn("poste");
        when(utilisateur.getPassword()).thenReturn("password");
        final List<Utilisateur> otherRecipients = Arrays.asList(new Utilisateur[]{admin});
        instance.sendValidationEmail(utilisateur, "baseUrl", resourceBundle, otherRecipients);
        verify(mailSender).sendMail("mailAdmin", "email", otherRecipients, "Procédure de création de compte.", "Bonjour prénom nom<br /><br />\n"
                + "\n"
                + "Votre login est : login<br />\n"
                + "Votre mot de passe est : vous êtes le seul à le savoir !<br />\n"
                + "\n"
                + "Vous avez fait une demande d'ouverture de compte sur SOERE MONSOERE<br />\n"
                + "Pour activer votre compte, cliquez sur le lien ci dessous<br /><br />\n"
                + "\n"
                + "baseUrl/validateAccount.jsf?activate=true&login=login");
    }

    /**
     * Test of getUtilisateurByEmail method, of class DefaultUsersManager.
     */
    @Test
    public void testGetUtilisateurByEmail() {
        when(utilisateurDAO.getByEMail("email")).thenReturn(Optional.ofNullable(utilisateur));
        when(utilisateurDAO.getByEMail("rien")).thenReturn(Optional.empty());
        Utilisateur result = instance.getUtilisateurByEmail("email");
        assertEquals(utilisateur, result);
        result = instance.getUtilisateurByEmail("rien");
        assertEquals(null, result);

    }

    /**
     * Test of updateUserProfile method, of class DefaultUsersManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testUpdateUserProfile() throws Exception {
        Group groupDataset = Mockito.mock(Group.class);
        Group groupRefdata = Mockito.mock(Group.class);
        when(oldUtilisateur.getOwnGroup(WhichTree.TREEDATASET)).thenReturn(groupDataset);
        when(oldUtilisateur.getOwnGroup(WhichTree.TREEREFDATA)).thenReturn(groupRefdata);
        when(utilisateurDAO.getById(0L)).thenReturn(Optional.ofNullable(oldUtilisateur));
        when(oldUtilisateur.getEmail()).thenReturn("oldemail");
        when(oldUtilisateur.getNom()).thenReturn("oldnom");
        when(oldUtilisateur.getPrenom()).thenReturn("oldprénom");
        when(oldUtilisateur.getLogin()).thenReturn("oldlogin");
        when(oldUtilisateur.getIsRoot()).thenReturn(false);
        when(oldUtilisateur.getEmploi()).thenReturn("oldemploi");
        when(oldUtilisateur.getPoste()).thenReturn("oldposte");
        when(oldUtilisateur.getPassword()).thenReturn("oldpassword");
        when(utilisateur.getEmail()).thenReturn("email");
        when(utilisateur.getNom()).thenReturn("nom");
        when(utilisateur.getPrenom()).thenReturn("prénom");
        when(utilisateur.getLogin()).thenReturn("login");
        when(utilisateur.getIsRoot()).thenReturn(true);
        when(utilisateur.getEmploi()).thenReturn("emploi");
        when(utilisateur.getPoste()).thenReturn("poste");
        when(utilisateur.getPassword()).thenReturn("password");
        //nominal
        instance.updateUserProfile(utilisateur);
        verify(oldUtilisateur).setEmail("email");
        verify(oldUtilisateur).setNom("nom");
        verify(oldUtilisateur).setPrenom("prénom");
        verify(oldUtilisateur).setLogin("login");
        verify(oldUtilisateur).setIsRoot(true);
        verify(oldUtilisateur).setEmploi("emploi");
        verify(oldUtilisateur).setPoste("poste");
        verify(utilisateurDAO).saveOrUpdate(oldUtilisateur);
        verify(groupDataset).setGroupName(LOGIN);
        verify(groupRefdata).setGroupName(LOGIN);
        ArgumentCaptor<String> password = ArgumentCaptor.forClass(String.class);
        verify(oldUtilisateur).setPassword(password.capture());
        Assert.assertTrue(Utils.toMD5("password").equals(password.getValue()));
    }

    /**
     * Test of addNewRequest method, of class DefaultUsersManager.
     *
     * @throws java.lang.Exception
     */
    /*@Test
     public void testAddNewRequest() throws Exception {
     instance = spy(instance);
     instance.setLocalizationManager(new SpringLocalizationManager());
     when(utilisateurDAO.getById(0L)).thenReturn(utilisateur);
     when(utilisateurDAO.getAdmins()).thenReturn(admins);
     when(configuration.getMailAdmin()).thenReturn(MAIL_ADMIN);
     when(utilisateur.getLanguage()).thenReturn("fr");
     when(utilisateur.getLogin()).thenReturn(LOGIN);
     when(admin.getPrenom()).thenReturn("prénom");
     when(admin.getNom()).thenReturn("Nom");
     when(admin.getEmail()).thenReturn("otherMailAdmin");
     when(rightRequest.getRequest()).thenReturn("request");
     when(rightRequest.getMotivation()).thenReturn("motivation");
     when(rightRequest.getReference()).thenReturn("référence");
     when(rightRequest.getInfos()).thenReturn("infos");
     when(utilisateur.getEmail()).thenReturn("email");
     ArgumentCaptor<String> c_message = ArgumentCaptor.forClass(String.class);
     ArgumentCaptor<String> c_level = ArgumentCaptor.forClass(String.class);
     ArgumentCaptor<String> c_body = ArgumentCaptor.forClass(String.class);
     ArgumentCaptor<Utilisateur> c_utilisateur = ArgumentCaptor.forClass(Utilisateur.class);
     ArgumentCaptor<String> c_from = ArgumentCaptor.forClass(String.class);
     ArgumentCaptor<String> c_email = ArgumentCaptor.forClass(String.class);
     ArgumentCaptor<String> c_subject = ArgumentCaptor.forClass(String.class);
     ArgumentCaptor<String> c_message2 = ArgumentCaptor.forClass(String.class);
     //nominal
     doReturn(null).when(instance).sendNotification(c_message.capture(), c_level.capture(), c_body.capture(), c_utilisateur.capture());
     instance.addNewRequest(rightRequest, utilisateur);
     verify(rightRequestDAO).saveOrUpdate(rightRequest);
     verify(mailSender).sendMail(c_from.capture(), c_email.capture(), c_subject.capture(), c_message2.capture());
     verify(rightRequest).setUser(utilisateur);
     assertEquals("Vous avez demandé de nouveaux accès.", c_message.getValue());
     assertEquals("info", c_level.getValue());
     assertEquals("La demande a été transmise aux administrateurs et sera traitée dans les plus brefs délais.", c_body.getValue());
     assertEquals(utilisateur, c_utilisateur.getValue());
     assertEquals("mailAdmin", c_from.getValue());
     assertEquals("otherMailAdmin", c_email.getValue());
     assertEquals("Un utilisateur demande de nouveaux droits", c_subject.getValue());
     assertEquals("Bonjour prénom Nom\n"
     + "L'utilisateur login a fait une demande de droits, avec les informations suivantes :\n"
     + "request\n"
     + "Pour l'étude suivante :\n"
     + "motivation\n"
     + "Ses références sont :\n"
     + "référence\n"
     + "Et il a ajouté ces informations :\n"
     + "infos\n"
     + "Vous pouvez lui répondre à cette adresse pour plus de précisions : email ", c_message2.getValue());
     //persistence exception
     when(utilisateurDAO.getById(0L)).thenThrow(new PersistenceException("error"));
     try {
     instance.addNewRequest(rightRequest, utilisateur);
     fail("no exception");
     } catch (BusinessException e) {
     assertEquals("can't save request", e.getMessage());
     }
     }*/
    /**
     * Test of activateUser method, of class DefaultUsersManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testActivateUser() throws Exception {
        //nominal
        when(utilisateur.getLogin()).thenReturn(LOGIN);
        when(utilisateurDAO.getByLogin(LOGIN)).thenReturn(Optional.ofNullable(utilisateur));
        instance.activateUser(LOGIN);
        verify(utilisateurDAO).getByLogin(LOGIN);
        verify(utilisateur).setActive(Boolean.TRUE);
        verify(utilisateurDAO).saveOrUpdate(utilisateur);
        //null utilisateur
        when(utilisateurDAO.getByLogin(null)).thenReturn(Optional.empty());
        try {
            instance.activateUser(null);
            fail("no exception");
        } catch (BusinessException e) {
            assertEquals("PROPERTY_MSG_UNKNOWN_USER", e.getMessage());
        }
    }

    /**
     * Test of getConfiguration method, of class DefaultUsersManager.
     */
    @Test
    public void testGetConfiguration() {
        ICoreConfiguration result = instance.getConfiguration();
        assertEquals(configuration, result);
    }
}
