/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo;

import java.time.LocalDateTime;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationDAO;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.junit.*;
import static org.junit.Assert.*;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 * @author ptcherniati
 */
public class MOTest {
    /**
     *
     */
    protected static final String LOGIN = "login";
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    MOImpl instance = new MOImpl();
    LocalDateTime date = LocalDateTime.of(2_004, 01, 23, 12, 30);
    @Mock
    Utilisateur utilisateur;
    @Mock
    INotificationsManager notificationsManager;
    @Mock
    INotificationDAO notificationDAO;
    @Mock
    IPolicyManager policyManager;
    String level = "level";
    String message = "message";
    String body = "body";
    String htmlBody;
    String attachmentPath = "attachmentPath";
    /**
     *
     */
    public MOTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        htmlBody = String.format("<p>%s</p>", body);
        instance.setNotificationsManager(notificationsManager);
        instance.setNotificationDAO(notificationDAO);
        instance.setPolicyManager(policyManager);
        when(utilisateur.getLogin()).thenReturn(LOGIN);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of buildNotification method, of class MO.
     */
    @Test
    public void testBuildNotification() {
        // nominal
        Notification result = instance.buildNotification(level, message, body, attachmentPath);
        assertNotNull(result);
        assertEquals(level, result.getLevel());
        assertEquals(message, result.getMessage());
        assertEquals(htmlBody, result.getBody());
        assertEquals(attachmentPath, result.getAttachment());
        //very long body
        final String longBody = StringUtils.repeat(body, 5_000);
        result = instance.buildNotification(level, message, longBody, attachmentPath);
        assertNotNull(result);
        assertEquals(level, result.getLevel());
        assertEquals(message, result.getMessage());
        assertTrue(result.getBody().contains(longBody.substring(0, 5_000)));
        assertTrue(result.getBody().startsWith("<p>"));
        assertTrue(result.getBody().endsWith("...</p>"));
        assertTrue(result.getBody().length() == 5_010);
        assertEquals(attachmentPath, result.getAttachment());
    }


    /**
     * Test of sendNotification method, of class MO.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testSendNotification() throws Exception {
        Notification result = instance.sendNotification(message, level, body, utilisateur);
        assertNotNull(result);
        assertEquals(level, result.getLevel());
        assertEquals(message, result.getMessage());
        assertEquals(htmlBody, result.getBody());
        verify(notificationsManager).addNotification(result, LOGIN);
    }

    /**
     * Test of sendNotificationToUser method, of class MO.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testSendNotificationToUser() throws Exception {
        Notification result = instance.sendNotificationToUser(message, level, body, LOGIN);
        assertNotNull(result);
        assertEquals(level, result.getLevel());
        assertEquals(message, result.getMessage());
        assertEquals(htmlBody, result.getBody());
        verify(notificationsManager).addNotification(result, LOGIN);
    }

    /**
     * Test of sendNotificationWithAttachment method, of class MO.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testSendNotificationWithAttachment() throws Exception {
        Notification result = instance.sendNotificationWithAttachment(message, level, body, attachmentPath, utilisateur);
        assertNotNull(result);
        assertEquals(level, result.getLevel());
        assertEquals(message, result.getMessage());
        assertEquals(htmlBody, result.getBody());
        assertEquals(attachmentPath, result.getAttachment());
        verify(notificationsManager).addNotification(result, LOGIN);
    }


    /**
     *
     */
    public class MOImpl extends MO {
    }

}
