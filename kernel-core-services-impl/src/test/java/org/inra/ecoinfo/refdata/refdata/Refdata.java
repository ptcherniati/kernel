package org.inra.ecoinfo.refdata.refdata;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import static org.inra.ecoinfo.mga.business.composite.Nodeable.LOGGER;

/**
 * The Class DataType.
 *
 * @author "Antoine Schellenberger"
 */
@Entity
@Table(name = Refdata.NAME_ENTITY_JPA)
public class Refdata extends Nodeable implements Serializable, Cloneable {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "refdata";

    /**
     *
     */
    public static final String NAME_ATTRIBUTS_NAME = "name";

    /**
     *
     */
    public static final String NAME_ATTRIBUTS_DESCRIPTION = "description";

    @Column(name = Refdata.NAME_ATTRIBUTS_DESCRIPTION, nullable = true, unique = false)
    private String description;

    /**
     *
     */
    public Refdata() {
        super();
    }

    /**
     *
     * @param string
     * @param name
     */
    public Refdata(final String code) {
        super(code);
    }

    /**
     *
     * @param name
     * @param string1
     * @param code
     */
    public Refdata(final String code, final String description) {
        super(code);
        this.description= description;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    @Override
    public Refdata clone() {
        Refdata refdata = null;
        try {
            refdata = (Refdata) super.clone();
            refdata.setId(getId());
            refdata.setCode(getCode());
            refdata.setDescription(description);
            refdata.setOrder(order);
        } catch (CloneNotSupportedException cnse) {
            LOGGER.error(cnse.getMessage(), cnse);
        }
        return refdata;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(final INodeable o) {
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            return 0;
        } else {
            int compareOrder = compare(order, o);
            if (compareOrder != 0) {
                return compareOrder;
            }
        }
        return getCode() == null ? -1 : getCode().compareTo(o.getCode());
    }

    private int compare(Long order, INodeable o) {
        if (order == null) {
            return 0;
        }
        if (o instanceof Refdata) {
            Refdata ref = (Refdata) o;
            return Long.compare(order, ref.order);
        } else {
            return 0;
        }
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        return EqualsBuilder.reflectionEquals(this, obj, new String[]{"id", "refData"});
    }

    /**
     *
     * @return
     */
    @Override
    public Long getId() {
        return super.getId();
    }

    /**
     *
     * @return
     */
    @Override
    public String getName() {
        return getCode();
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (getCode() == null ? 0 : getCode().hashCode());
        return result;
    }

    /**
     *
     * @param id
     */
    @Override
    public void setId(final Long id) {
        super.setId(id);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return this.code;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) Refdata.class;
    }
}
