package org.inra.ecoinfo.config;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.dbunit.DatabaseUnitException;
import org.inra.ecoinfo.config.impl.CoreConfiguration;
import org.inra.ecoinfo.config.impl.InternationalisationConfiguration;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class ConfigurationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
public class ConfigurationTest {

    /**
     * The CoreConfiguration core configuration.
     */
    @Autowired
    CoreConfiguration coreConfiguration;
    /**
     * The InternationalisationConfiguration internationalisation configuration.
     */
    @Autowired
    InternationalisationConfiguration internationalisationConfiguration;

    /**
     * Sets the void core configuration.
     *
     * @param coreConfiguration the CoreConfiguration core configuration
     */
    public void setCoreConfiguration(CoreConfiguration coreConfiguration) {
        this.coreConfiguration = coreConfiguration;
    }

    /**
     * Sets the void internationalisation configuration.
     *
     * @param internationalisationConfiguration the
     *                                          InternationalisationConfiguration internationalisation configuration
     */
    public void setInternationalisationConfiguration(InternationalisationConfiguration internationalisationConfiguration) {
        this.internationalisationConfiguration = internationalisationConfiguration;
    }

    /**
     * Test.
     *
     * @throws URISyntaxException           the uRI syntax exception
     * @throws IOException                  Signals that an I/O exception has occurred.
     * @throws IllegalAccessException       the illegal access exception
     * @throws ConfigurationException       the configuration exception
     * @throws PersistenceException         the persistence exception
     * @throws DatabaseUnitException        the database unit exception
     * @throws SQLException                 the sQL exception
     * @throws ParserConfigurationException the parser configuration exception
     */
    @Test
    public void test() throws URISyntaxException, IOException, IllegalAccessException, ConfigurationException, PersistenceException, DatabaseUnitException, SQLException, ParserConfigurationException {
        Assert.assertEquals("l'id est invalide", "projet-ORE", coreConfiguration.getId());
        Assert.assertEquals("le siteSeparatorForFileNames est invalide", "-", coreConfiguration.getSiteSeparatorForFileNames());
        Assert.assertEquals("le mailAdmin est invalide", "appli@orleans.inra.fr", coreConfiguration.getMailAdmin());
        Assert.assertEquals("le mailHost est invalide", "localhost", coreConfiguration.getMailHost());
        Assert.assertEquals("le nom français est invalide", "ORE", coreConfiguration.getInternationalizedNames().get("fr"));
        Assert.assertEquals("le nom anglais est invalide", "ORE_en", coreConfiguration.getInternationalizedNames().get("en"));
        Assert.assertEquals("la langue par défault est invalide", "fr", internationalisationConfiguration.getDefaultLocalisation());
        Assert.assertTrue("le nom français est invalide", Localization.getLocalisations().contains("fr"));
    }
}
