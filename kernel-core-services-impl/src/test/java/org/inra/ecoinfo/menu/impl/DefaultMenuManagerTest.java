package org.inra.ecoinfo.menu.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.menu.IMenuBuilder;
import org.inra.ecoinfo.menu.Menu;
import org.inra.ecoinfo.menu.MenuItem;
import org.inra.ecoinfo.mga.caching.ITreeApplicationCacheManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.junit.*;
import org.mockito.AdditionalMatchers;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.ArgumentMatchers.matches;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 * @author ptcherniati
 */
public class DefaultMenuManagerTest {
    /**
     * @throws Exception
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
    }
    /**
     * @throws Exception
     */
    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     *
     */
    protected String menuPath;// = DefaultMenuManager.class.getClassLoader().getResource("../../../../META-INF/menu.xml").getPath();
    DefaultMenuManager instance;
    IMenuBuilder menuBuilder = Mockito.spy(new MenuXMLDigesterBuilder());
    @Mock
    IPolicyManager policyManager;
    @Mock
    Utilisateur utilisateur;
    @Mock
    MenuItem menuItem;
    @Mock
    MenuItem menuItem1;
    @Mock
    MenuItem menuItem2;
    @Mock
    ITreeApplicationCacheManager treeApplicationCacheManager;


    /**
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        menuPath = getClass().getClassLoader().getResource("META-INF/menu.xml").getPath();
        instance = new DefaultMenuManager();
        instance.setTreeApplicationCacheManager(treeApplicationCacheManager);
        when(treeApplicationCacheManager.getConfigurationMap()).thenReturn(new HashMap<>());
        instance.setMenuBuilder(menuBuilder);
        instance.setPolicyManager(policyManager);
    }

    /**
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     *
     */
    @Test
    public void testBuildMenu() {
        final Answer<Menu> answer = (InvocationOnMock invocation) -> {
            final Object[] args = invocation.getArguments();
            final Menu menu = (Menu) invocation.callRealMethod();

            /*
             * for (MenuItem menu1 : menu.getMenuItems()) { System.out.println(String.format("\t%s(%s)->%s", menu1.getLabel(), menu1.getLogged() ? "logged" : "non logged", menu1.getDisabled() ? "disabled" : "active")); for (MenuItem menuItem :
             * menu1.getChildren()) { System.out.println(String.format("\t\t%s(%s)->%s", menuItem.getLabel(), menuItem.getLogged() ? "logged" : "non logged", menuItem.getDisabled() ? "disabled" : "active")); } }
             */
            Assert.assertEquals("PROPERTY_LABEL_CONSULTATION_DATA", menu.getMenuItems().get(0).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_REQUEST_RIGHTS", menu.getMenuItems().get(0).getChildren().get(0).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_DATA_ADMIN", menu.getMenuItems().get(1).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_DATA_MANAGEMENT", menu.getMenuItems().get(1).getChildren().get(0).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_REF_DATA_MANAGEMENT", menu.getMenuItems().get(1).getChildren().get(1).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_FILECOMP_MANAGEMENT", menu.getMenuItems().get(1).getChildren().get(2).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_RIGHTS_AND_USERS_MANAGEMENT", menu.getMenuItems().get(2).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_USERS_MANAGEMENT", menu.getMenuItems().get(2).getChildren().get(0).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_DATA_RIGHTS_MANAGEMENT", menu.getMenuItems().get(2).getChildren().get(1).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_REFDATA_RIGHTS_MANAGEMENT", menu.getMenuItems().get(2).getChildren().get(2).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_USER_ACCOUNT", menu.getMenuItems().get(3).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_CHANGE_MY_PROFIL", menu.getMenuItems().get(3).getChildren().get(0).getLabel());
            Assert.assertEquals("PROPERTY_LABEL_MANAGE_RIGHTS_DEMAND", menu.getMenuItems().get(3).getChildren().get(1).getLabel());
            Assert.assertTrue(menu.getMenuItems().size() == 4);
            Assert.assertEquals(menuPath, args[0]);
            return menu;
        };
        Mockito.doAnswer(answer).when(menuBuilder).build(anyString());
        instance.buildMenu();

    }

    /**
     * @throws CloneNotSupportedException
     */
    @Test
    public void testBuildRestrictedMenuForRootUser() throws CloneNotSupportedException {
        instance = spy(instance);
        Mockito.when(policyManager.getCurrentUser()).thenReturn(utilisateur);
        Mockito.when(policyManager.getCurrentUserLogin()).thenReturn("utilisateur");
        Mockito.when(policyManager.isRoot()).thenReturn(true);
        Menu restrictedMenu = instance.buildRestrictedMenu();
        /*
         * System.out.println("\ntestBuildRestrictedMenuForRootUser()\n"); for (MenuItem menu : restrictedMenu.getMenuItems()) { System.out.println(String.format("\t%s(%s)->%s", menu.getLabel(), menu.getLogged() ? "logged" : "non logged",
         * menu.getDisabled() ? "disabled" : "active")); for (MenuItem menuItem : menu.getChildren()) { System.out.println(String.format("\t\t%s(%s)->%s", menuItem.getLabel(), menuItem.getLogged() ? "logged" : "non logged", menuItem.getDisabled() ?
         * "disabled" : "active")); } }
         */
        Assert.assertFalse(restrictedMenu.getMenuItems().get(0).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(0).getChildren().get(0).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(1).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(1).getChildren().get(0).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(1).getChildren().get(1).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(1).getChildren().get(2).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(2).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(2).getChildren().get(0).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(2).getChildren().get(1).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(2).getChildren().get(2).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(3).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(3).getChildren().get(0).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(3).getChildren().get(1).getDisabled());
        verify(instance, never()).restrictedMenuForNonRootUser(any(MenuItem.class), anyString());
        //verify(instance, times(restrictedMenu.getMenuItems().size())).disableTopMenu(any(MenuItem.class));
    }

    /**
     * @throws CloneNotSupportedException
     */
    @Test
    public void testBuildRestrictedMenuWithUserAndNoRights() throws CloneNotSupportedException {
        instance = spy(instance);
        Mockito.when(policyManager.getCurrentUser()).thenReturn(utilisateur);
        Mockito.when(policyManager.getCurrentUserLogin()).thenReturn("utilisateur");
        Mockito.when(policyManager.isRoot()).thenReturn(false);
        //for non root user
        Menu restrictedMenu = instance.buildRestrictedMenu();
        Assert.assertTrue(restrictedMenu.getMenuItems().get(0).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(0).getChildren().get(0).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(1).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(1).getChildren().get(0).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(1).getChildren().get(1).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(1).getChildren().get(2).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(2).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(2).getChildren().get(0).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(2).getChildren().get(1).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(2).getChildren().get(2).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(3).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(3).getChildren().get(0).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(3).getChildren().get(1).getDisabled());
        verify(instance, times(restrictedMenu.getMenuItems().size())).disableTopMenu(any(MenuItem.class));
    }

    /**
     * @throws CloneNotSupportedException
     */
    @Test
    public void testBuildRestrictedMenuWithUserLoggedAndAdministrationRightsOnRefdatas() throws CloneNotSupportedException {
        instance = spy(instance);
        Mockito.when(policyManager.getCurrentUser()).thenReturn(utilisateur);
        Mockito.when(policyManager.getCurrentUserLogin()).thenReturn("utilisateur");
        Mockito.when(policyManager.isRoot()).thenReturn(false);
        //for non root user and rights for refdata administration
        doReturn(true).when(policyManager).checkCurrentUserActivity(eq(1), matches(".*administration.*"), anyString());
        Menu restrictedMenu = instance.buildRestrictedMenu();
        Assert.assertTrue(restrictedMenu.getMenuItems().get(0).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(0).getChildren().get(0).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(1).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(1).getChildren().get(0).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(1).getChildren().get(1).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(1).getChildren().get(2).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(2).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(2).getChildren().get(0).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(2).getChildren().get(1).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(2).getChildren().get(2).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(3).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(3).getChildren().get(0).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(3).getChildren().get(1).getDisabled());
        verify(instance, times(restrictedMenu.getMenuItems().size())).disableTopMenu(any(MenuItem.class));
    }

    /**
     * @throws CloneNotSupportedException
     */
    @Test
    public void testBuildRestrictedMenuWithUserLoggedAndAllRights() throws CloneNotSupportedException {
        instance = spy(instance);
        Mockito.when(policyManager.getCurrentUser()).thenReturn(utilisateur);
        Mockito.when(policyManager.getCurrentUserLogin()).thenReturn("utilisateur");
        Mockito.when(policyManager.isRoot()).thenReturn(false);
        //for non root user and rights for synthese
        doReturn(true).when(policyManager).checkCurrentUserActivity(eq(1), anyString(), anyString());
        doReturn(true).when(policyManager).checkCurrentUserActivity(anyInt(), eq(""), AdditionalMatchers.or(isNull(String.class), eq("*")));
        Menu restrictedMenu = instance.buildRestrictedMenu();
        Assert.assertTrue(restrictedMenu.getMenuItems().get(0).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(0).getChildren().get(0).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(1).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(1).getChildren().get(0).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(1).getChildren().get(1).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(1).getChildren().get(2).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(2).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(2).getChildren().get(0).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(2).getChildren().get(1).getDisabled());
        Assert.assertFalse(restrictedMenu.getMenuItems().get(2).getChildren().get(2).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(3).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(3).getChildren().get(0).getDisabled());
        Assert.assertTrue(restrictedMenu.getMenuItems().get(3).getChildren().get(1).getDisabled());
        verify(instance, times(restrictedMenu.getMenuItems().size())).disableTopMenu(any(MenuItem.class));
    }

    /**
     *
     */
    @Test
    public void testGetMenu() {
        final Menu menu = instance.getMenu();
        Assert.assertNotNull(menu);
        Assert.assertEquals("PROPERTY_LABEL_CONSULTATION_DATA", menu.getMenuItems().get(0).getLabel());
        Assert.assertEquals("PROPERTY_LABEL_REQUEST_RIGHTS", menu.getMenuItems().get(0).getChildren().get(0).getLabel());
        Assert.assertEquals("PROPERTY_LABEL_DATA_MANAGEMENT", menu.getMenuItems().get(1).getChildren().get(0).getLabel());
        Assert.assertEquals("PROPERTY_LABEL_REF_DATA_MANAGEMENT", menu.getMenuItems().get(1).getChildren().get(1).getLabel());
        Assert.assertEquals("PROPERTY_LABEL_FILECOMP_MANAGEMENT", menu.getMenuItems().get(1).getChildren().get(2).getLabel());
        Assert.assertEquals("PROPERTY_LABEL_RIGHTS_AND_USERS_MANAGEMENT", menu.getMenuItems().get(2).getLabel());
        Assert.assertEquals("PROPERTY_LABEL_USERS_MANAGEMENT", menu.getMenuItems().get(2).getChildren().get(0).getLabel());
        Assert.assertEquals("PROPERTY_LABEL_DATA_RIGHTS_MANAGEMENT", menu.getMenuItems().get(2).getChildren().get(1).getLabel());
        Assert.assertEquals("PROPERTY_LABEL_REFDATA_RIGHTS_MANAGEMENT", menu.getMenuItems().get(2).getChildren().get(2).getLabel());
        Assert.assertEquals("PROPERTY_LABEL_USER_ACCOUNT", menu.getMenuItems().get(3).getLabel());
        Assert.assertEquals("PROPERTY_LABEL_CHANGE_MY_PROFIL", menu.getMenuItems().get(3).getChildren().get(0).getLabel());
        Assert.assertEquals("PROPERTY_LABEL_MANAGE_RIGHTS_DEMAND", menu.getMenuItems().get(3).getChildren().get(1).getLabel());
        Assert.assertTrue(menu.getMenuItems().size() == 4);
    }

    /**
     * Test of buildRestrictedMenu method, of class DefaultMenuManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildRestrictedMenu() throws Exception {
        instance = Mockito.spy(instance);
        Menu menu = Mockito.mock(Menu.class);
        doReturn(menu).when(menuBuilder).build(anyString());
        instance.buildMenu();
        List<MenuItem> menuItems = mock(List.class);
        when(menu.getMenuItems()).thenReturn(menuItems);
        doReturn(menuItems).when(instance).buildRestrictedMenuItems(menuItems);
        instance.buildRestrictedMenu();
    }

    /**
     * Test of buildRestrictedMenuItems method, of class DefaultMenuManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildRestrictedMenuItems() throws Exception {
        instance = Mockito.spy(instance);
        Menu menu = Mockito.mock(Menu.class);
        doReturn(menu).when(menuBuilder).build(anyString());
        instance.buildMenu();
        List<MenuItem> menuItems = new LinkedList();
        List<MenuItem> menuItems2 = new LinkedList();
        MenuItem menuItem1 = mock(MenuItem.class);
        MenuItem menuItem2 = mock(MenuItem.class);
        menuItems.add(menuItem1);
        menuItems2.add(menuItem2);
        when(menuItem1.clone()).thenReturn(menuItem1);
        when(menuItem1.buildPermissionsArray()).thenReturn("permission");
        //empty children and nON ROOT USER
        when(menuItem1.getChildren()).thenReturn(new LinkedList());
        List<MenuItem> result = instance.buildRestrictedMenuItems(menuItems);
        Assert.assertTrue(1 == result.size());
        Assert.assertEquals(menuItem1, result.get(0));
        verify(menuItem1).setDisabled(Boolean.TRUE);
        //empty children and ROOT USER
        when(policyManager.getCurrentUserLogin()).thenReturn("LOGIN");
        when(policyManager.isRoot()).thenReturn(Boolean.TRUE);
        result = instance.buildRestrictedMenuItems(menuItems);
        Assert.assertTrue(1 == result.size());
        Assert.assertEquals(menuItem1, result.get(0));
        verify(menuItem1, times(1)).setDisabled(Boolean.FALSE);
        //with non root user
        when(policyManager.isRoot()).thenReturn(Boolean.FALSE);
        result = instance.buildRestrictedMenuItems(menuItems);
        Assert.assertTrue(1 == result.size());
        Assert.assertEquals(menuItem1, result.get(0));
        verify(menuItem1, times(1)).setDisabled(Boolean.FALSE);
        verify(instance).restrictedMenuForNonRootUser(menuItem1, "permission");
        //with not empty list
        when(menuItem1.getChildren()).thenReturn(menuItems2);
        doReturn(menuItems).when(instance).buildRestrictedMenuItems(menuItems2);
        result = instance.buildRestrictedMenuItems(menuItems);
        Assert.assertTrue(1 == result.size());
        Assert.assertEquals(menuItem1, result.get(0));
        verify(instance).buildRestrictedMenuItems(menuItems2);
        verify(instance).disableTopMenu(menuItem1);
    }

    /**
     * Test of restrictedMenuForNonRootUser method, of class DefaultMenuManager.
     */
    @Test
    public void testRestrictedMenuForNonRootUser() {
        List<MenuItem> menuItems = mock(List.class);
        final String permissions = "permission";
        final String ressources = "ressources";
        instance = spy(instance);
        when(menuItem.getResources()).thenReturn(ressources);
        // non null permissions
        doReturn(true).when(policyManager).checkCurrentUserActivity(1, permissions, ressources);
        instance.restrictedMenuForNonRootUser(menuItem, permissions);
        verify(policyManager).checkCurrentUserActivity(0, permissions, ressources);
        verify(menuItem, never()).setDisabled(Boolean.FALSE);
        verify(menuItem, times(1)).setDisabled(Boolean.TRUE);
        doReturn(false).when(policyManager).checkCurrentUserActivity(1, permissions, ressources);
        instance.restrictedMenuForNonRootUser(menuItem, permissions);
        verify(policyManager, times(2)).checkCurrentUserActivity(0, permissions, ressources);
        verify(menuItem, times(2)).setDisabled(Boolean.TRUE);
        verify(menuItem, never()).setDisabled(Boolean.FALSE);
        // with null permissions
        // with not empty children
        when(menuItem.getChildren()).thenReturn(menuItems);
        when(menuItems.isEmpty()).thenReturn(Boolean.FALSE);
        doNothing().when(instance).restrictedMenuWithLocalPermission(menuItem);
        instance.restrictedMenuForNonRootUser(menuItem, null);
        verify(policyManager, times(2)).checkCurrentUserActivity(0, permissions, ressources);
        verify(menuItem, times(2)).setDisabled(Boolean.TRUE);
        verify(menuItem, never()).setDisabled(Boolean.FALSE);
        verify(instance).restrictedMenuWithLocalPermission(menuItem);
        // with empty children
        when(menuItem.getChildren()).thenReturn(menuItems);
        when(menuItems.isEmpty()).thenReturn(Boolean.TRUE);
        doNothing().when(instance).restrictedMenuWithLocalPermission(menuItem);
        instance.restrictedMenuForNonRootUser(menuItem, null);
        verify(policyManager, times(2)).checkCurrentUserActivity(0, permissions, ressources);
        verify(menuItem, times(3)).setDisabled(Boolean.TRUE);
        verify(menuItem, never()).setDisabled(Boolean.FALSE);
        verify(instance).restrictedMenuWithLocalPermission(menuItem);

    }

    /**
     * Test of restrictedMenuWithLocalPermission method, of class
     * DefaultMenuManager.
     */
    @Test
    public void testRestrictedMenuWithLocalPermission() {
        List<MenuItem> menuItems = new LinkedList();
        menuItems.add(menuItem1);
        menuItems.add(menuItem2);
        when(menuItem.getChildren()).thenReturn(menuItems);
        when(menuItem1.getDisabled()).thenReturn(Boolean.FALSE);
        when(menuItem2.getDisabled()).thenReturn(Boolean.TRUE);
        //With at least one item not disabled
        instance.restrictedMenuWithLocalPermission(menuItem);
        verify(menuItem, never()).setDisabled(Boolean.FALSE);
        verify(menuItem, never()).setDisabled(Boolean.TRUE);
        //With all sub menu disabled
        when(menuItem1.getDisabled()).thenReturn(Boolean.TRUE);
        when(menuItem2.getDisabled()).thenReturn(Boolean.TRUE);
        instance.restrictedMenuWithLocalPermission(menuItem);
        verify(menuItem, never()).setDisabled(Boolean.FALSE);
        verify(menuItem, times(1)).setDisabled(Boolean.TRUE);
    }

    /**
     * Test of disableTopMenu method, of class DefaultMenuManager.
     */
    @Test
    public void testDisableTopMenu() {
        List<MenuItem> menuItems = new LinkedList();
        menuItems.add(menuItem1);
        menuItems.add(menuItem2);
        when(menuItem.getChildren()).thenReturn(menuItems);
        when(menuItem1.getDisabled()).thenReturn(Boolean.FALSE);
        when(menuItem2.getDisabled()).thenReturn(Boolean.TRUE);
        //With at least one item not disabled
        instance.disableTopMenu(menuItem);
        verify(menuItem).setDisabled(Boolean.FALSE);
        verify(menuItem, never()).setDisabled(Boolean.TRUE);
        //With all sub menu disabled
        when(menuItem1.getDisabled()).thenReturn(Boolean.TRUE);
        when(menuItem2.getDisabled()).thenReturn(Boolean.TRUE);
        instance.disableTopMenu(menuItem);
        verify(menuItem, times(1)).setDisabled(Boolean.FALSE);
        verify(menuItem, times(1)).setDisabled(Boolean.TRUE);
    }
}
