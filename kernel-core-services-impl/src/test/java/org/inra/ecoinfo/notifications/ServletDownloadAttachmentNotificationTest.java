package org.inra.ecoinfo.notifications;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.DateUtil;
import org.junit.*;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author ptcherniati
 */
public class ServletDownloadAttachmentNotificationTest {
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    @Spy
    ServletDownloadAttachmentNotification instance;
    @Mock
    MockHttpServletRequest request;
    @Mock
    MockHttpServletResponse response;
    @Mock
    WebApplicationContext applicationContext;
    @Mock
    INotificationsManager notificationsManager;
    @Mock
    Notification _notification;
    Optional<Notification> notification;
    @Mock
    ServletOutputStream outputStream;
    String dateString = "01/01/1956";
    LocalDateTime date;
    ServletContext servletContext = new MockServletContext();
    /**
     *
     */
    public ServletDownloadAttachmentNotificationTest() {
    }


    /**
     * @throws DateTimeParseException
     */
    @Before
    public void setUp() throws DateTimeParseException {
        MockitoAnnotations.openMocks(this);
        doReturn(servletContext).when(instance).getServletContext();
        when(request.getParameter("idnotification")).thenReturn("4");
        servletContext.setAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE, applicationContext);
        when(applicationContext.getBean(INotificationsManager.ID_BEAN)).thenReturn(notificationsManager);
        doReturn(outputStream).when(response).getOutputStream();
        date = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateString);
        notification = Optional.ofNullable(_notification);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of doGet method, of class ServletDownloadAttachmentNotification.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDoGet() throws Exception {
        when(notificationsManager.getNotificationById(4L)).thenReturn(notification);
        when(_notification.getAttachment()).thenReturn("org/inra/attachement.txt");
        when(_notification.getDate()).thenReturn(date);
        final byte[] datas = "Un attachement".getBytes();
        when(notificationsManager.getAttachment(4L)).thenReturn(datas);
        instance.doGet(request, response);
        verify(response).setContentType("application/zip");
        verify(response).setContentLength(datas.length);
        String fileName = "extraction_inra_01-01-1956-00-00-00-000.zip";
        verify(response).setHeader("Content-Disposition", String.format("attachment; filename=%s", fileName));
        verify(outputStream).write(datas);
        verify(outputStream).flush();
    }

    /**
     * Test of doPost method, of class ServletDownloadAttachmentNotification.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDoPost() throws Exception {
        doNothing().when(instance).doGet(request, response);
        instance.doPost(request, response);
        verify(instance).doGet(request, response);
    }

}
