package org.inra.ecoinfo.notifications.impl;

import java.io.File;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationDAO;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 * @author ptcherniati
 */
public class LocalNotificationManagerTest {
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    LocalNotificationManager instance;
    @Mock
    ILocalizationManager localizationManager;
    @Mock
    INotificationDAO notificationDAO;
    @Mock
    IPolicyManager policyManager;
    @Mock
    IUtilisateurDAO utilisateurDAO;
    @Mock
    ICoreConfiguration configuration;
    @Mock
    Notification _notification;
    Optional<Notification> notification;
    /**
     *
     */
    public LocalNotificationManagerTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new LocalNotificationManager();
        instance.setConfiguration(configuration);
        instance.setLocalizationManager(localizationManager);
        instance.setNotificationDAO(notificationDAO);
        instance.setPolicyManager(policyManager);
        instance.setUtilisateurDAO(utilisateurDAO);
        notification = Optional.ofNullable(_notification);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addNotification method, of class LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testAddNotification() throws Exception {
        String login = "login";

        Utilisateur utilisateur = mock(Utilisateur.class);
        when(localizationManager.getMessage("org.inra.ecoinfo.notifications.messages", "MSG_LOGIN_NOT_PRESENT_IN_DATABASE")).thenReturn("%s");
        when(utilisateurDAO.getByLogin(login)).thenReturn(Optional.ofNullable(utilisateur));
        when(utilisateurDAO.getByLogin("other")).thenReturn(Optional.empty());
        //null login
        try {
            instance.addNotification(_notification, "other");
            fail();
        } catch (BusinessException e) {
            assertEquals("can't push message", e.getMessage());
        }
        //info notification
        when(_notification.getLevel()).thenReturn(Notification.INFO);
        instance.addNotification(_notification, login);
        verify(_notification, times(1)).setUtilisateur(utilisateur);
        verify(notificationDAO, times(1)).saveOrUpdate(_notification);
        //error notification
        when(_notification.getLevel()).thenReturn(Notification.ERROR);
        instance.addNotification(_notification, login);
        verify(_notification, times(2)).setUtilisateur(utilisateur);
        verify(notificationDAO, times(2)).saveOrUpdate(_notification);
        //warn notification
        when(_notification.getLevel()).thenReturn(Notification.WARN);
        instance.addNotification(_notification, login);
        verify(_notification, times(3)).setUtilisateur(utilisateur);
        verify(notificationDAO, times(3)).saveOrUpdate(_notification);
    }

    /**
     * Test of getAllNoArchivedByUserLogin method, of class
     * LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllNoArchivedByUserLogin() throws Exception {
        String login = "login";
        List<Notification> notifications = new LinkedList();
        notifications.add(_notification);
        notifications.add(_notification);
        doReturn(notifications).when(notificationDAO).getAllNoArchivedByUserLogin(login);
        List<Notification> result = instance.getAllNoArchivedByUserLogin(login);
        verify(notificationDAO).getAllNoArchivedByUserLogin(login);
        assertEquals(notifications, result);
    }

    /**
     * Test of getAllNotifications method, of class LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllNotifications() throws Exception {
        List<Notification> notifications = new LinkedList();
        notifications.add(_notification);
        notifications.add(_notification);
        doReturn(notifications).when(notificationDAO).getAllSortedByDate();
        List<Notification> result = instance.getAllNotifications();
        verify(notificationDAO).getAllSortedByDate();
        assertEquals(notifications, result);
        instance.getAllNotifications();
    }

    /**
     * Test of getAllTransientsNotifications method, of class
     * LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllTransientsNotifications() throws Exception {
        List<Notification> notifications = new LinkedList();
        notifications.add(_notification);
        notifications.add(_notification);
        doReturn(notifications).when(notificationDAO).getAllTransientsSortedByDate(LocalDateTime.MIN);
        List<Notification> result = instance.getAllTransientsNotifications(LocalDateTime.MIN);
        verify(notificationDAO).getAllTransientsSortedByDate(LocalDateTime.MIN);
        assertEquals(notifications, result);
    }

    /**
     * Test of getAllUserNotifications method, of class
     * LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllUserNotifications() throws Exception {
        final String login = "login";
        List<Notification> notifications = new LinkedList();
        notifications.add(_notification);
        notifications.add(_notification);
        doReturn(notifications).when(notificationDAO).getAllByUserLogin(login);
        List<Notification> result = instance.getAllUserNotifications(login);
        verify(notificationDAO).getAllByUserLogin(login);
        assertEquals(notifications, result);
    }

    /**
     * Test of getArchivedNotifications method, of class
     * LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetArchivedNotifications() throws Exception {
        final String login = "login";
        List<Notification> notifications = new LinkedList();
        notifications.add(_notification);
        notifications.add(_notification);
        doReturn(notifications).when(notificationDAO).getAllArchivedNotificationsByUserLogin(login);
        List<Notification> result = instance.getArchivedNotifications(login);
        verify(notificationDAO).getAllArchivedNotificationsByUserLogin(login);
        assertEquals(notifications, result);
    }

    /**
     * Test of getAttachment method, of class LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAttachment() throws Exception {
        instance = spy(instance);
        byte[] body = "body".getBytes();
        ArgumentCaptor<File> filArgumentCaptor = ArgumentCaptor.forClass(File.class);
        doReturn(body).when(instance).buildBytesArrayAttachment(any(File.class));
        when(_notification.getAttachment()).thenReturn("body");
        when(configuration.getRepositoryURI()).thenReturn("uri");
        doReturn(notification).when(notificationDAO).getById(Notification.class, 12L);
        byte[] result = instance.getAttachment(12L);
        verify(notificationDAO).getById(Notification.class, 12L);
        verify(instance).buildBytesArrayAttachment(filArgumentCaptor.capture());
        assertEquals("uri/body", filArgumentCaptor.getValue().getPath());
        Assert.assertArrayEquals(body, result);
    }

    /**
     * Test of getBody method, of class LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetBody() throws Exception {
        byte[] body = "body".getBytes();
        when(_notification.getBody()).thenReturn("body");
        doReturn(notification).when(notificationDAO).getById(Notification.class, 12L);
        byte[] result = instance.getBody(12L);
        verify(notificationDAO).getById(Notification.class, 12L);
        assertEquals("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
                + "<head>\n"
                + "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />\n"
                + "<style type=\"text/css\">\n"
                + "p {font-size:17px;font-family: Arial, Verdana, sans-serif;}\n"
                + "</style>\n"
                + "</head>\n"
                + "<body>\n"
                + "body\n"
                + "</body>\n"
                + "</html>", new String(result));
    }

    /**
     * Test of getNotificationById method, of class LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNotificationById() throws Exception {
        doReturn(notification).when(notificationDAO).getById(Notification.class, 12L);
        assertEquals(notification, instance.getNotificationById(12L));
        verify(notificationDAO).getById(Notification.class, 12L);
    }

    /**
     * Test of getTransientsUserNotifications method, of class
     * LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetTransientsUserNotifications() throws Exception {
        List<Notification> result;
        final String login = "login";
        final LocalDateTime date = LocalDateTime.MAX;
        //null date
        try {
            result = instance.getTransientsUserNotifications(login, null);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("login or date null", e.getMessage());
        }
        //null login
        try {
            result = instance.getTransientsUserNotifications(null, date);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("login or date null", e.getMessage());
        }
        //null login
        List<Notification> notifications = new LinkedList();
        notifications.add(_notification);
        notifications.add(_notification);
        when(notificationDAO.getTransientsByUserLogin(login, date)).thenReturn(notifications);
        when(localizationManager.getMessage("org.inra.ecoinfo.notifications.messages", "MSG_DEBUG_TRANSIENT_NOTIFICATIONS_RETRIEVED")).thenReturn("%s %s %s");
        try {
            result = instance.getTransientsUserNotifications(login, date);
            assertEquals(notifications, result);
        } catch (IllegalArgumentException e) {
            fail();
        }
    }

    /**
     * Test of markAllAsRead method, of class LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testMarkAllAsRead() throws Exception {
        instance.markAllAsRead(12L);
        verify(notificationDAO).markAllAsReadByUtilisateurId(12L);
    }

    /**
     * Test of markNotificationAsArchived method, of class
     * LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testMarkNotificationAsArchived() throws Exception {
        instance.markNotificationAsArchived(12L);
        verify(notificationDAO).archiveNotification(12L);
    }

    /**
     * Test of markNotificationAsNew method, of class LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testMarkNotificationAsNew() throws Exception {
        instance.markNotificationAsNew(12L);
        verify(notificationDAO).markAsNew(12L);
    }

    /**
     * Test of removeAllNotification method, of class LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRemoveAllNotification() throws Exception {
        instance.removeAllNotification(12L);
        verify(notificationDAO).removeAllNotificationsByUtilisateurId(12L);
    }

    /**
     * Test of removeNotification method, of class LocalNotificationManager.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRemoveNotification() throws Exception {
        when(notificationDAO.getById(Notification.class, 25L)).thenReturn(notification);
        when(notificationDAO.getById(Notification.class, 30L)).thenReturn(Optional.empty());
        try {
            instance.removeNotification(25L);
            verify(notificationDAO).remove(_notification);
        } catch (BusinessException e) {
            fail();
        }
        try {
            instance.removeNotification(30L);
            verify(notificationDAO).remove(_notification);
        } catch (BusinessException e) {
            fail();
        }
    }

    /**
     * Test of setConfiguration method, of class LocalNotificationManager.
     */
    @Test
    public void testSetConfiguration() {
        assertEquals(configuration, instance.configuration);
    }

    /**
     * Test of setUtilisateurDAO method, of class LocalNotificationManager.
     */
    @Test
    public void testSetUtilisateurDAO() {
        assertEquals(utilisateurDAO, instance.utilisateurDAO);
    }
}
