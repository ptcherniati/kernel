package org.inra.ecoinfo.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import org.inra.ecoinfo.localization.entity.Localization;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * @author ptcherniati
 */
public class DatasetDescriptorBuilderTest {

    private static final String DATASET_DESCRIPTOR_HEADER_NULL_LINE = "ligne1\nligne2\nligne3\n";
    private static final String DATASET_DESCRIPTOR_HEADER_GOOD_LINE = "ligne1\nligne2\nligne3\nagro_fr;agro_en;nom du site;variable;date";
    private static final String DATASET_DESCRIPTOR_HEADER_BAD_LINE = "ligne1\nligne2\nligne3\n";
    private static final String DATASET_DESCRIPTOR = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<dataset-descriptor xmlns=\"http://orei.inra.fr/dataset-descriptor\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://orei.inra.fr/dataset-descriptor http://orei.inra.fr/dataset-descriptor \">\n"
            + "    <name>test</name>\n"
            + "    <undefined-column>7</undefined-column>\n"
            + "    <column>\n"
            + "        <key>true</key>"
            + "        <name>agro</name>\n"
            + "        <name>agro</name>\n"
            + "        <not-null>true</not-null>\n"
            + "        <localizable fieldName=\"agr_name\" >true</localizable>\n"
            + "    </column>\n"
            + "    <column>\n"
            + "        <name>nom du site</name>\n"
            + "        <not-null>false</not-null>\n"
            + "        <localizable>true</localizable>\n"
            + "        <variable>false</variable>\n"
            + "        <flag>true</flag>\n"
            + "        <flag-type>site</flag-type>\n"
            + "    </column>\n"
            + "    <column>\n"
            + "        <name>variable</name>\n"
            + "        <not-null>true</not-null>\n"
            + "        <localizable>false</localizable>\n"
            + "        <variable>true</variable>\n"
            + "        <flag>true</flag>\n"
            + "        <flag-type>variable</flag-type>\n"
            + "    </column>\n"
            + "    <column>\n"
            + "        <name>date</name>\n"
            + "        <not-null>true</not-null>\n"
            + "        <value-type>date</value-type>\n"
            + "        <format-type>dd/MM/yyyy</format-type>\n"
            + "        <variable>false</variable>\n"
            + "        <flag>true</flag>\n"
            + "        <date-for-interval>true</date-for-interval>\n"
            + "        <flag-type>date</flag-type>\n"
            + "        <ref-variable-name>variable</ref-variable-name>\n"
            + "    </column>\n"
            + "</dataset-descriptor>";
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    InputStream datasetDescriptorStream;
    InputStream datasetDescriptorStreamWithHeader;

    /**
     *
     */
    public DatasetDescriptorBuilderTest() {
    }


    /**
     *
     */
    @Before
    public void setUp() {
        datasetDescriptorStream = new ByteArrayInputStream(DATASET_DESCRIPTOR.getBytes());
        //datasetDescriptorStreamWithHeader = new ByteArrayInputStream((DATASET_DESCRIPTOR_HEADER+DATASET_DESCRIPTOR).getBytes());
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of buildDescriptor method, of class DatasetDescriptorBuilder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildDescriptor() throws Exception {
        Localization.addLanguage("fr");
        Localization.addLanguage("en");
        DatasetDescriptor result = DatasetDescriptorBuilder.buildDescriptor(datasetDescriptorStream);
        assertNotNull(result);
        assertEquals("test", result.getName());
        assertTrue(7 == result.getUndefinedColumn());
        List<Column> columns = result.getColumns();
        assertTrue(7 == columns.size());
        Column column0 = columns.get(0);
        testColumn(column0, false, false, true, false, false, "agr_name", "agro_key", null, null, null, null);
        Column column1 = columns.get(1);
        testColumn(column1, false, false, true, false, false, "agr_name", "agro_fr", null, null, null, null);
        Column column2 = columns.get(2);
        testColumn(column2, false, false, true, false, false, "agr_name", "agro_en", null, null, null, null);
        Column column3 = columns.get(3);
        testColumn(column3, true, true, true, false, false, "nom du site_fr", "nom du site_fr", "site", null, null, null);
        Column column4 = columns.get(4);
        testColumn(column4, true, true, true, false, false, "nom du site_en", "nom du site_en", "site", null, null, null);
        Column column5 = columns.get(5);
        testColumn(column5, true, false, false, false, true, "variable", "variable", "variable", null, null, null);
        Column column6 = columns.get(6);
        testColumn(column6, true, false, false, true, false, "date", "date", "date", "dd/MM/yyyy", "variable", "date");
    }

    /**
     * Test of testInputStream method, of class DatasetDescriptorBuilder.
     */
//    @Test
//    public void testTestInputStream() throws IOException {
//        System.out.println("testInputStream");
//        DatasetDescriptor datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(datasetDescriptorStream);
//        DatasetDescriptorBuilder.testInputStream(datasetDescriptorStreamWithHeader, datasetDescriptor);
//    }
    private void testColumn(Column column, boolean isFlag, boolean isNull, boolean isLocalizable, boolean isDateForInterval, boolean isVariable, String fieldName, Object name, Object flagType, Object formatType, Object refVariableName, Object valuetype) {
        assertTrue(isFlag == column.isFlag());
        assertTrue(isNull == column.isInull());
        assertTrue(isNull == column.isNullable());
        assertTrue(isLocalizable == column.isLocalizable());
        assertTrue(isDateForInterval == column.isDateForInterval());
        assertTrue(isVariable == column.isVariable());
        assertEquals(fieldName, column.getFieldName());
        assertEquals(name, column.getName());
        assertEquals(flagType, column.getFlagType());
        assertEquals(formatType, column.getFormatType());
        assertEquals(refVariableName, column.getRefVariableName());
        assertEquals(valuetype, column.getValueType());
    }

}
