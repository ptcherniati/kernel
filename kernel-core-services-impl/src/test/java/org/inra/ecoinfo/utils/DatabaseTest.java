/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.utils;

import org.dbunit.JdbcDatabaseTester;

/**
 * @author tcherniatinsky
 */
public class DatabaseTest {

    /**
     * @return
     * @throws ClassNotFoundException
     */
    public static JdbcDatabaseTester JDBCDatabaseInstance() throws ClassNotFoundException {
        return new JdbcDatabaseTester(
                "org.postgresql.Driver",
                "jdbc:postgresql://localhost:5432/monsoeretest",
                "monsoereusertest",
                "mst001");
    }

}
