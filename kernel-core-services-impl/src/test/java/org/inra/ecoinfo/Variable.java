package org.inra.ecoinfo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.Utils;

/**
 * Représente un variable scientifique.
 *
 * @author "Antoine Schellenberger"
 */
@Entity
@Table(name = Variable.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = "id")
public class Variable extends Nodeable implements Serializable {

    /**
     * The Constant PERSISTENT_NAME_ID @link(String).
     */
    public static final String PERSISTENT_NAME_ID = "id";
    /**
     * The Constant NAME_ATTRIBUTS_DESCRIPTION @link(String).
     */
    public static final String NAME_ATTRIBUTS_DESCRIPTION = "description";
    /**
     * The Constant NAME_ATTRIBUTS_NAME @link(String).
     */
    public static final String NAME_ATTRIBUTS_NAME = "name";
    /**
     * The Constant NAME_ENTITY_JPA @link(String).
     */
    public static final String NAME_ENTITY_JPA = "variable";
    /**
     * The Constant VARIABLE_NAME_ID @link(String).
     */
    public static final String VARIABLE_NAME_ID = "id";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The description @link(String).
     */
    @Column(columnDefinition = "TEXT", name = Variable.NAME_ATTRIBUTS_DESCRIPTION)
    private String description = "";
    /**
     * The name @link(String).
     */
    @Column(nullable = false, unique = true, name = Variable.NAME_ATTRIBUTS_NAME)
    private String name;

    /**
     * Instantiates a new variable.
     */
    public Variable() {
        super();
    }

    /**
     * Instantiates a new variable.
     *
     * @param string
     * @param name the name
     * @link(String) the name
     */
    public Variable(final String name) {
        super();
        this.setName(name);
    }

    /**
     * @param nom
     * @param string1
     * @param description
     */
    public Variable(final String nom, final String description) {
        setName(nom);
        setDescription(description);
    }

    /**
     * @return
     */
    public Variable cloneOnlyAttributes() {
        final Variable variable = new Variable();
        variable.setName(getName());
        variable.setDescription(getDescription());
        variable.setId(getId());
        return variable;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(final INodeable o) {
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            return 0;
        }
        return getCode() == null ? -1 : getCode().compareTo(o.getCode());
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        return EqualsBuilder.reflectionEquals(this, obj, new String[]{"id", "datasTypes", "sitesVariables"});
    }

    /**
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     */
    public void setDescription(final String description) {
        this.description = description == null ? "" : description;
    }

    /**
     * @return
     */
    @Override
    public Long getId() {
        return super.getId();
    }

    /**
     * @param id
     */
    @Override
    public void setId(final Long id) {
        super.setId(id);
    }

    /**
     * @return
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(final String name) {
        this.name = name;
        setCode(Utils.createCodeFromString(name));
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (getCode() == null ? 0 : getCode().hashCode());
        result = prime * result + (description == null ? 0 : description.hashCode());
        return result;
    }

    /**
     * @param code
     */
    @Override
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return this.name;
    }

    /**
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) Variable.class;
    }

}
