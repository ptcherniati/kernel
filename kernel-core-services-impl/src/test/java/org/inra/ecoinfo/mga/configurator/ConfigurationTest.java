/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;

/**
 * @author tcherniatinsky
 */
public class ConfigurationTest extends AbstractMgaIOConfiguration {

    /**
     * @param codeConfig
     * @param leafType
     * @param entryOrder
     * @param entryTypes
     * @param activities
     * @param sortOrder
     * @param includeAncestor
     * @param whichTree
     * @param stickyLeafType
     * @param bln1
     * @param displayColumnNames
     */
    public ConfigurationTest(Integer codeConfiguration, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
        super(codeConfiguration, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
    }

}
