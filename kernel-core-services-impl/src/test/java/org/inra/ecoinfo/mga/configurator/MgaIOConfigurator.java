package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.Datatype;
import org.inra.ecoinfo.Site;
import org.inra.ecoinfo.Variable;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.refdata.Refdata;

/**
 * @author yahiaoui
 */
public class MgaIOConfigurator extends AbstractMgaIOConfigurator {

    private static final Integer[] NORMAL_ORDER_0_1_2 = new Integer[]{0, 1, 2};
    private static final Integer[] DATASET_ORDER_0 = new Integer[]{0};
    private static final Class<INodeable>[] ENTRY_INSTANCE_SDV = new Class[]{
            Site.class,
            Datatype.class,
            Variable.class
    };
    private static final Activities[] ACTIVITY_SAPDSE
            = new Activities[]{
            Activities.synthese,
            Activities.administration,
            Activities.publication,
            Activities.depot,
            Activities.suppression,
            Activities.extraction,};
    private static final Class<INodeable>[] ENTRY_INSTANCE_REF = new Class[]{Refdata.class};
    private static final Activities[] ACTIVITY_ESTA
            = new Activities[]{
            Activities.edition,
            Activities.suppression,
            Activities.telechargement,
            Activities.administration,};

    /**
     *
     */
    public MgaIOConfigurator() {
        super(new AbstractMgaDisplayerConfiguration() {
        });
    }

    /**
     *
     */
    @Override
    protected void initConfigurations() {

        /**
         * Configuration Zero
         */
        boolean includeAncestorConfigZero = true;

        WhichTree whichTreeDataSetZero = WhichTree.TREEDATASET;

        Class<? extends INodeable> stickyLeafZero = null;//Variable.class;

        boolean displayColumnNamesZero = false;

        ConfigurationTest datasetRightsConfiguration
                = new ConfigurationTest(
                DATASET_CONFIGURATION_RIGHTS,
                Datatype.class,
                NORMAL_ORDER_0_1_2,
                ENTRY_INSTANCE_SDV,
                ACTIVITY_SAPDSE,
                NORMAL_ORDER_0_1_2,
                includeAncestorConfigZero,
                whichTreeDataSetZero,
                stickyLeafZero,
                displayColumnNamesZero);

        /**
         * Configuration One // For RefData Insertion
         */

        boolean includeAncestorConfigOne = false;

        WhichTree whichTreeRefDataOne = WhichTree.TREEREFDATA;

        Class<INodeable> stickyLeafInstanceOne = null;

        boolean displayColumnNamesOne = false;

        ConfigurationTest refdataRightsConfiguration
                = new ConfigurationTest(
                REFDATA_CONFIGURATION_RIGHTS,
                Refdata.class,
                DATASET_ORDER_0,
                ENTRY_INSTANCE_REF,
                ACTIVITY_ESTA,
                DATASET_ORDER_0,
                includeAncestorConfigOne,
                whichTreeRefDataOne,
                stickyLeafInstanceOne,
                displayColumnNamesOne);

        /**
         * ------------------------------------------------------------------------------------
         * *
         */
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION_RIGHTS, k -> datasetRightsConfiguration);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION_RIGHTS, k -> refdataRightsConfiguration);

        /**
         * ------------------------------------------------------------------------------------
         * *
         */
    }
}
