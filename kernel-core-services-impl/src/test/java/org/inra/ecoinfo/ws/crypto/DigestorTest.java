package org.inra.ecoinfo.ws.crypto;

import java.security.NoSuchAlgorithmException;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author yahiaoui
 */
public class DigestorTest {

    /**
     * @param args
     * @throws NoSuchAlgorithmException
     */
    public static void main(String[] args) throws NoSuchAlgorithmException {

        String login = "foretuser";
        String plainPassword = "foret001";
        String timeStamp = "6981698169";
        String md5Password = Digestor.digestMD5(plainPassword);
        String signature = Digestor.digestSha1(login + md5Password + timeStamp);
    }

    /**
     * @throws NoSuchAlgorithmException
     */
    @Test
    public void okDigestSha1() throws NoSuchAlgorithmException {

        String login = "admin";
        String plainPassword = "admin";
        String timeStamp = "123456789";

        String expected = "3043d25553c21bbdf40ddcc811f15cdec02216e2";

        String md5Password = Digestor.digestMD5(plainPassword);

        String signature = Digestor.digestSha1(login + md5Password + timeStamp);

        Assert.assertEquals(expected, signature);
    }

    /**
     * @throws NoSuchAlgorithmException
     */
    @Test
    public void effectiveDigestSha1() throws NoSuchAlgorithmException {

        String login = "admin";
        String password = "password";
        String timeStamp = "123456789";

        String signature = Digestor.digestSha1(login + password + timeStamp);

        Assert.assertTrue(!signature.contains(login));
        Assert.assertTrue(!signature.contains(password));
        Assert.assertTrue(!signature.contains(timeStamp));
    }

    /**
     * @throws NoSuchAlgorithmException
     */
    @Test
    public void badSha1Concordence() throws NoSuchAlgorithmException {

        String login = "admin";
        String timeStamp = "123456789";
        String password_1 = "21232f297a57a5a743894a0e4a801fc3";
        String password_2 = "21232f297a57a5a743894a0e4a801fc4";

        String signature_1 = Digestor.digestSha1(login + password_1 + timeStamp);
        String signature_2 = Digestor.digestSha1(login + password_2 + timeStamp);

        Assert.assertTrue(!signature_1.equals(signature_2));
    }

    /**
     * @throws NoSuchAlgorithmException
     */
    @Test
    public void okDigestMd5() throws NoSuchAlgorithmException {

        String password = "admin";
        String expected = "21232f297a57a5a743894a0e4a801fc3";
        String digested = Digestor.digestMD5(password);

        Assert.assertEquals(expected, digested);
    }

    /**
     * @throws NoSuchAlgorithmException
     */
    @Test
    public void effectiveDigestMd5() throws NoSuchAlgorithmException {

        String password = "password";
        String signature = Digestor.digestMD5(password);

        Assert.assertTrue(!signature.contains(password));
    }

    /**
     * @throws NoSuchAlgorithmException
     */
    @Test
    public void badConcordence() throws NoSuchAlgorithmException {

        String password_1 = "admin";
        String password_2 = "admin.";

        String digestPass_1 = Digestor.digestMD5(password_1);
        String digestPass_2 = Digestor.digestMD5(password_2);

        Assert.assertTrue(!digestPass_1.contains(digestPass_2));
        Assert.assertTrue(!digestPass_2.contains(digestPass_1));
    }
}
