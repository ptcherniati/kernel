package org.inra.ecoinfo.ws.identification;

/**
 * @author yahiaoui
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.inra.ecoinfo.ws.crypto.Digestor;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class WsTEST {

    static final String ROOT_URL = "http://localhost:8080/acbb-web/rest/resources/";

    /* URL exp :
    http://localhost:8081/monsoere-web/rest/resources/admin/45c400197fb9099602b0e32abf758dec52235cd6/1403856528649/2/paramUn
     */

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        byte[] b = {126, 125, 90};
        String s = new String(b);
        BigInteger bb = new BigInteger(s.getBytes());
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void testBadAuthentication() throws Exception {

        String expected = "<status>UNAUTHORIZED RESOURCE</status>";
        String login = "admin";
        String password = "admin_admin";
        long timeSTampe = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        String md5Password = Digestor.digestMD5(password);
        String signature = Digestor.digestSha1(login + md5Password + timeSTampe);

        URL url = new URL(ROOT_URL + login + "/" + signature + "/" + timeSTampe + "/2/serviceGet");

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line = reader.readLine();
        String response = "";
        while (line != null) {
            response += line;
            line = reader.readLine();
        }
        connection.disconnect();
        Assert.assertEquals(expected, response);
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void testOKAuthenticationServiceUN() throws Exception {

        String login = "admin";
        String password = "admin";
        long timeSTampe = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        String md5Password = Digestor.digestMD5(password);
        String signature = Digestor.digestSha1(login + md5Password + timeSTampe);

        String notExpected = "<status>UNAUTHORIZED RESOURCE</status>";

        // System.out.println("URL ==> "+ROOT_URL +login+"/"+signature+"/"+timeSTampe+"/2/serviceGet");
        URL url = new URL(ROOT_URL + login + "/" + signature + "/" + timeSTampe + "/SWC/serviceGet");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line = reader.readLine();
        String response = "";
        while (line != null) {
            response += line;
            line = reader.readLine();
        }
        connection.disconnect();
        Assert.assertTrue(!response.equals(notExpected));
    }
}
