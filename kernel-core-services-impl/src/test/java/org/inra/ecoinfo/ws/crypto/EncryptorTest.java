package org.inra.ecoinfo.ws.crypto;

import com.Ostermiller.util.Base64;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author yahiaoui
 */
public class EncryptorTest {

    /**
     *
     */
    @Test
    public void okAesEncrypt() {

        String password = "21232f297a57a5a743894a0e4a801fc3";

        String plainMessage = "{\n"
                + "   \"DatatypeVariableUnite\" : [ {\n"
                + "      \"dvu_id\" : 1,\n"
                + "      \"datatype_id\" : 1,\n"
                + "      \"datatype_Nom\" : \"Piégeage en montée\",\n"
                + "      \"variable_id\" : 1,\n"
                + "      \"variable_nom\" : \"nom_tmp\",\n"
                + "      \"unite_id\" : 1,\n"
                + "      \"unute_nom\" : \"cels\"\n"
                + "   } ]\n"
                + "}";

        String expectedCryptedMessage
                = "6fEfCKHB5tz1I9klkpxy9M7xfKHTLx4myHzwlCbyyOFDVVkZ9yusgBV"
                + "BxfjuRTLlllUsRfOaJ3KcKFIy1eBbXpvwxZe3r/6NAH/n3i5o/fKYOsx1Uc3O"
                + "r8s5DVPm9aqlZHzz1GxX9OU+6CXl0qBrowjyeb/5GFtKNuOlYEQb/jJK9Cal4"
                + "HRjA20N3j1qARIUZNEfiqIdmlQMABH5x+PEE6MFgB+QiLTR6+tuovWYEnWqn4"
                + "SMUUWdomJDLiuF0N/yJOWe8QWvUPFp3MNgsy0QZg/isfKSmXZqyD+p88QfKxA"
                + "GES12r8XnXA8pJXQI02II5Do7BlWep2Cfu1UjzKEQwg==";

        String cryptedMessage = Encryptor.aes128ECBEncrypt(password, plainMessage);
        Assert.assertEquals(cryptedMessage, expectedCryptedMessage);
        String decryptedMessage = Encryptor.aes128ECBDecrypt(password, cryptedMessage);
        Assert.assertEquals(decryptedMessage, plainMessage);
    }

    /**
     *
     */
    @Test
    public void badAesDecrypt() {

        String password = "21232f297a57a5a743894a0e4a801fc3";

        String plainMessage = "{\n"
                + "   \"DatatypeVariableUnite\" : [ {\n"
                + "      \"dvu_id\" : 1,\n"
                + "      \"datatype_id\" : 1,\n"
                + "      \"datatype_Nom\" : \"Piégeage en montée\",\n"
                + "      \"variable_id\" : 1,\n"
                + "      \"variable_nom\" : \"nom_tmp\",\n"
                + "      \"unite_id\" : 1,\n"
                + "      \"unute_nom\" : \"cels\"\n"
                + "   } ]\n"
                + "}";

        String cryptyedMessage = Encryptor.aes128ECBEncrypt(password, plainMessage);
        password = password + ".";
        String deCryptedMessage = Encryptor.aes128ECBDecrypt(password, cryptyedMessage);
        Assert.assertTrue(deCryptedMessage == null);
    }

    /**
     *
     */
    @Test
    public void okAesCBCPKCS7Encrypt() {

        String password = "21232f297a57a5a743894a0e4a801fc3";

        String plainMessage = "{\n"
                + "   \"DatatypeVariableUnite\" : [ {\n"
                + "      \"dvu_id\" : 1,\n"
                + "      \"datatype_id\" : 1,\n"
                + "      \"datatype_Nom\" : \"Piégeage en montée\",\n"
                + "      \"variable_id\" : 1,\n"
                + "      \"variable_nom\" : \"nom_tmp\",\n"
                + "      \"unite_id\" : 1,\n"
                + "      \"unute_nom\" : \"cels\"\n"
                + "   } ]\n"
                + "}";

        String expectedCryptedMessage = "xGoOLKgtdhHRv2t+nKtsEisZ7znVGxoOuBUojh"
                + "BMS0O/8ww9QRPnohhOLX3XAwsoV9kpFV6mQx7xTo9ShcpqRjx40/ejzCBpID"
                + "CwTQmAL7hKoboCBJ1gWCuiNCovRUZAblPrRZ0KzueSbz/6sKeerxCBXp/T7E"
                + "mjjihlzHv+wJOO6QV0z3XQqtBNnHEcVGX9HkIroNvtWNe3VeI04PqEh5r2EV"
                + "VtPXy0jjXa+u/R36VV2m8fIyjh3amKWrmhL1Tm2+/BeUWm1iE4Q30sWVr29P"
                + "yAYBozNj5Lbt06BNic4Mfhi8Uf1pHPolxN5EAaM7QhT2+8HzBEDAv/XIQivC"
                + "Ewqw==";

        String cryptedMessage = Encryptor.aes128CBC7Encrypt(password, plainMessage);
        Assert.assertEquals(cryptedMessage, expectedCryptedMessage);
        String decryptedMessage = Encryptor.aes128CBC7Decrypt(password, cryptedMessage);
        Assert.assertEquals(decryptedMessage, plainMessage);
    }

    /**
     *
     */
    @Test
    public void badAesCBCPKCS7Decrypt() {

        String password = "21232f297a57a5a743894a0e4a801fc3";

        String plainMessage = "{\n"
                + "   \"DatatypeVariableUnite\" : [ {\n"
                + "      \"dvu_id\" : 1,\n"
                + "      \"datatype_id\" : 1,\n"
                + "      \"datatype_Nom\" : \"Piégeage en montée\",\n"
                + "      \"variable_id\" : 1,\n"
                + "      \"variable_nom\" : \"nom_tmp\",\n"
                + "      \"unite_id\" : 1,\n"
                + "      \"unute_nom\" : \"cels\"\n"
                + "   } ]\n"
                + "}";

        String cryptyedMessage = Encryptor.aes128CBC7Encrypt(password, plainMessage);
        password = password + ".";
        String deCryptedMessage = Encryptor.aes128CBC7Decrypt(password, cryptyedMessage);
        Assert.assertTrue(deCryptedMessage == null);
    }

    /**
     * @throws NoSuchAlgorithmException
     */
    @Test
    public void okPaddingKey() throws NoSuchAlgorithmException {

        String password = "21232f297a57a5a743894a0e4a801fc3";
        String HashedPasswordSha1 = Digestor.digestSha1(password);
        String generatedKey = Encryptor.generateKeyString(password);

        Assert.assertTrue(generatedKey.length() == 16);
        Assert.assertEquals(generatedKey, HashedPasswordSha1.substring(0, 16));
    }

    /**
     * @throws UnsupportedEncodingException
     */
    @Test
    public void okAesCBC7Encrypt() throws UnsupportedEncodingException {

        String texte_1 = "ceci est un test";
        String texte_2 = " de chiffrement par bloc";

        Encryptor encryptor = new Encryptor("21232f297a57a5a743894a0e4a801fc3");
        encryptor.initEncryptMode();
        byte[] encryptedBloc_1 = encryptor.
                aes128CBC7Encrypt(texte_1.getBytes(), CipherOperation.update);
        byte[] encryptedBloc_2 = encryptor.
                aes128CBC7Encrypt(texte_2.getBytes(), CipherOperation.dofinal);

        byte[] both = ArrayUtils.addAll(encryptedBloc_1, encryptedBloc_2);

        String encryptedText = new String(Base64.encode(both));

        encryptor.initDecryptMode();

        byte[] aes128CBC7DecryptedBytes
                = encryptor.aes128CBC7Decrypt(Base64.decode(encryptedText.getBytes()),
                CipherOperation.dofinal);

        Assert.assertEquals(new String(aes128CBC7DecryptedBytes), texte_1 + texte_2);
    }

    /*
     @Test
     public void OkContentDycryption() throws Exception {
           
     String ROOT_URL    = "http://localhost:8081/monsoere-web/rest/resources/";

     String login       = "admin";
     String password    = "admin";
     int numService     =  2 ;
     long timeSTampe = new Date().getTime();
     String md5Password = Digestor.digestMD5(password);
     String signature   = Digestor.digestSha1(login + md5Password +timeSTampe);
         
     URL url = new URL(ROOT_URL 
     +login+"/"+signature+"/"+timeSTampe+"/"+numService+"/serviceGet");
     HttpURLConnection connection = (HttpURLConnection) url.openConnection();
     connection.setRequestMethod("GET");
     connection.setRequestProperty("Accept", "xml/encrypted");
        
     try{
     BufferedReader reader = new BufferedReader(new 
     InputStreamReader(connection.getInputStream()));
     String line = reader.readLine();
     String content = "";
     while (line != null) {
     content += line+"\n";
     line      = reader.readLine();
     }

     String aesCBC7Decrypt = Encryptor.aes128CBC7Decrypt(md5Password, content);
     connection.disconnect();
     Assert.assertNotNull(aesCBC7Decrypt);
     } 
     catch(Exception ex){
     }       
     }
   
     */
    /*
     
     // Test Encryption Decryption Message    
     public static void main(String[] args) throws NoSuchAlgorithmException {
     String login       = "admin";
     String password    = "admin";
     String md5Password = Digestor.digestMD5(password);
     String encryptedMessage = "bzpAfhvzHMgOCqXBxhDBxZiN3ioDr6nmE/QvLbavmgBnbHCH+DXsZ23dKejx3ER2uCo3iW5sfrBqxkjAqGAp8v71aP+EnfeypVjf/3HMYRuv0bDy42q/T9ltODpo1DvDO0QV38nPCDqPel41SSWr+L1homWy6t3t65W2kEnyy4d+/3ULZZPAdB9FKHm1GDMelXIOxQFBzz5g8iuboIvHwOKk2RcB52bS1lbtjL1vmsSdrgFMB+0qM6H4tsVe6Sk7aLci/R6htTXjtdUcMpz/KYUVvNyU4g4Xd2+BdxodiYyKPnLKBaPSamKdtLPK202fNEIi/aX0QC+kDdG355JDzYqOjdbDWu1cp9SJkSWY/A4WMjffZlwUJkXykxgWY20eWiVXcvwWliGGsC3z7ELhvrsIrIaqVRI7emy/Vb5PseuUYT9xIW8lS+MrlLUPSsAoxLYAldJhG/movluu9jOnaVqwVlcgAs2pw2AidmRLQJ16AkmCr5Cv/LTWuWMoNk2AmqsZWBNJ8STtr8Zsxc/4KpNg2vYnGiX6JJZYn6+nR5jrB8+6gZw9qKNe0FrLSTuEcAuP+S/M6J+hs6TfEdNEbLwJNQxxw2kRHWsHM4c1tukaftKU6YPeSPXRN9MJXewJJKLGZtMSQtwJZmLCJv7EPdjdqzzsOStM6Je0/eb0BRI9O/zDqhNG9p/xlEI358ikgPT3YZ9zi48bIVq5N2gR2uLgqO8wW+7ryCiGk0ni6NvS5hTcPt9Z5spRNg5u4B9GHvfInseTTfXjI9GnR3wnZ0uykvyZbkmjr0jB/ufuONkjCcn4Yha44yh802F31c+btOKIBMuehnx97fboPoxwzCV3cXSxMVQmm9Lp3SqXiMH4bBIEV4HgTRcWkOYXkwWD";
     String deCryptyedMessage = Encryptor.aes128CBC7Decrypt(md5Password, encryptedMessage);
     System.out.println(" decrypted Message ==> "+deCryptyedMessage);
     }
     */
    // String encryptedTextBase64 = new String(Base64.encode(Message));
    // byte[] decode = Base64.decode(encryptedTextBase64.getBytes());         
}
