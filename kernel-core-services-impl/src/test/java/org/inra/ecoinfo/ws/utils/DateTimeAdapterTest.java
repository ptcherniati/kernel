package org.inra.ecoinfo.ws.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.inra.ecoinfo.utils.DateUtil;
import org.junit.*;

/**
 * @author ptcherniati
 */
public class DateTimeAdapterTest {


    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    /**
     *
     */
    public DateTimeAdapterTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of marshal method, of class DateTimeAdapter.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testMarshal() throws Exception {
//        LocalDateTime v = null;
//        DateTimeAdapter instance = new DateTimeAdapter();
//        String expResult = "";
//        String result = instance.marshal(v);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");

    }

    /**
     * Test of unmarshal method, of class DateTimeAdapter.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testUnmarshal() throws Exception {
        String date = "24/12/1965";
        String time = "10:30:00";
        String datetime = "24/12/1965 10:30:27";
        String pattern = "(?<date>[0-9]{2}/[0-9]{2}/[0-9]{4})? ?(?<time>[0-9]{2}:[0-9]{2}:[0-9]{2})?";
        Pattern p = Pattern.compile(pattern);
        Matcher matcherDate = p.matcher(date);
        Matcher matcherTime = p.matcher(time);
        Matcher matcherDateTime = p.matcher(datetime);
        LocalDateTime dt = null;
        if (matcherDate.matches()) {
            dt = getDateTimeFromMatcher(matcherDate);
        }
        if (matcherTime.matches()) {
            dt = getDateTimeFromMatcher(matcherTime);
        }
        if (matcherDateTime.matches()) {
            dt = getDateTimeFromMatcher(matcherDateTime);
        }
//        String v = "";
//        DateTimeAdapter instance = new DateTimeAdapter();
//        LocalDateTime expResult = null;
//        LocalDateTime result = instance.unmarshal(v);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    LocalDateTime getDateTimeFromMatcher(Matcher matcher) {
        return Optional.ofNullable(matcher.group("date"))
                .map(s -> DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, s))
                .map(d -> getTime(matcher).map(t -> LocalDateTime.of(d, t)).orElse(d.atStartOfDay()))
                .orElse(getTime(matcher).map(t -> t.atDate(LocalDate.ofEpochDay(0))).orElse((LocalDateTime) null));

    }

    private Optional<LocalTime> getTime(Matcher matcher) {
        return Optional.ofNullable(matcher.group("time"))
                .map(s -> DateUtil.readLocalTimeFromText(DateUtil.HH_MM_SS, s));
    }

}
