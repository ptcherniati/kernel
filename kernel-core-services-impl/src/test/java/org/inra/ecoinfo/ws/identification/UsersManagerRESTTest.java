package org.inra.ecoinfo.ws.identification;

import java.io.IOException;
import java.io.StringReader;
import java.net.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.*;
import javax.persistence.Transient;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UsersManagerRESTTest.
 */
public class UsersManagerRESTTest {

    /**
     * The Logger LOGGER.
     */
    @Transient
    static final private Logger LOGGER = LoggerFactory.getLogger(UsersManagerRESTTest.class);
    /**
     * The Constant ROOT_URL.
     */
    private static final String ROOT_URL = "http://localhost:9090/si_kernel/";
    /**
     * The String session id.
     */
    private static String SESSION_ID;
    /**
     * The Boolean skip tests.
     */
    private static Boolean SKIP_TESTS = false;

    /**
     * Inits the session.
     */
    @BeforeClass
    public static void initSession() {
        try {
            UsersManagerRESTTest.SESSION_ID = UsersManagerRESTTest.retrieveSessionID();
            if (UsersManagerRESTTest.SESSION_ID == null) {
                UsersManagerRESTTest.SKIP_TESTS = true;
            } else {
                UsersManagerRESTTest.LOGGER.info(UsersManagerRESTTest.SESSION_ID);
            }
        } catch (final IOException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched IOException in initSession", e);
            UsersManagerRESTTest.SKIP_TESTS = true;
        } catch (final KeyManagementException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched KeyManagementException in initSession", e);
            UsersManagerRESTTest.SKIP_TESTS = true;
        } catch (final NoSuchAlgorithmException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched NoSuchAlgorithmException in initSession", e);
            UsersManagerRESTTest.SKIP_TESTS = true;
        }
    }

    /**
     * Disable ssl feature.
     *
     * @throws NoSuchAlgorithmException the no such algorithm exception
     * @throws KeyManagementException   the key management exception
     */
    private static void disableSSLFeature() throws NoSuchAlgorithmException, KeyManagementException {
        // Create a trust manager that does not validate certificate chains
        final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

            @Override
            public void checkClientTrusted(final X509Certificate[] certs, final String authType) {
            }

            @Override
            public void checkServerTrusted(final X509Certificate[] certs, final String authType) {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        }};
        // Install the all-trusting trust manager
        final SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // Create all-trusting host name verifier
        final HostnameVerifier allHostsValid = (final String hostname, final SSLSession session) -> true;
        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

    /**
     * Retrieve session id.
     *
     * @return the String string
     * @throws IOException              Signals that an I/O exception has occurred.
     * @throws NoSuchAlgorithmException the no such algorithm exception
     * @throws KeyManagementException   the key management exception
     */
    private static String retrieveSessionID() throws IOException, NoSuchAlgorithmException, KeyManagementException {
        UsersManagerRESTTest.disableSSLFeature();
        final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cookieManager);
        final URL url = new URL(UsersManagerRESTTest.ROOT_URL);
        final URLConnection connection = url.openConnection();
        connection.getContent();
        final CookieStore cookieJar = cookieManager.getCookieStore();
        final List<HttpCookie> cookies = cookieJar.getCookies();
        for (final HttpCookie cookie : cookies) {
            if (cookie != null) {
                return cookie.getValue();
            }
        }
        return null;
    }
    /**
     * The String base rest url.
     */
    private final String BASE_REST_URL = UsersManagerRESTTest.ROOT_URL + "rest/usersManager/";
    /**
     * The HtmlUnitDriver driver.
     */
    HtmlUnitDriver driver = new HtmlUnitDriver(true);

    /**
     * Stop driver.
     */
    @After
    public void stopDriver() {
        driver.close();
    }

    /**
     * Test connection admin.
     *
     * @throws Exception the exception
     */
    //@Test
    @Ignore
    public void testConnectionAdmin() throws Exception {
        if (!UsersManagerRESTTest.SKIP_TESTS) {
            UsersManagerRESTTest.LOGGER.info("session ok");
            final String login = "admin", password = "admin";
            testConnection(login, password);
            testConnectedLogin(login);
        } else {
            UsersManagerRESTTest.LOGGER.info("pas de session");
        }
    }

    /**
     * Test connection jetty admin.
     *
     * @throws Exception the exception
     */
    //@Test
    @Ignore
    public void testConnectionJettyAdmin() throws Exception {
        driver.get(BASE_REST_URL + String.format("connect/%s/%s/", "admin", "admin"));
        String content = driver.getPageSource();
        Digester digester = new Digester();
        digester.addObjectCreate("status", StringBuffer.class);
        digester.addCallMethod("status", "append", 0);
        final StringBuffer status = (StringBuffer) digester.parse(new StringReader(content));
        Assert.assertEquals("OK", status.toString());
        UsersManagerRESTTest.LOGGER.info("connexion de l'utilisateur admin");
        driver.get(BASE_REST_URL + "connectedLogin");
        content = driver.getPageSource();
        digester = new Digester();
        digester.addObjectCreate("utilisateur/login", StringBuffer.class);
        digester.addCallMethod("utilisateur/login", "append", 0);
        final StringBuffer login = (StringBuffer) digester.parse(new StringReader(content));
        Assert.assertEquals("admin", login.toString());
        UsersManagerRESTTest.LOGGER.info("l'utilisateur admin est connecté");
    }

    /**
     * Test connected login.
     *
     * @param login the String login
     * @throws Exception the exception
     */
    private void testConnectedLogin(final String login) throws Exception {
    }

    /**
     * Test connection.
     *
     * @param login    the String login
     * @param password the String password
     * @throws Exception the exception
     */
    private void testConnection(final String login, final String password) throws Exception {
    }
}
