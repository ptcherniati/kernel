package org.inra.ecoinfo.localization.impl;

import java.util.*;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationDAO;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.entity.UserLocale;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 * @author ptcherniati
 */
public class SpringLocalizationManagerTest {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.utils.messages";
    SpringLocalizationManager localizationManager;
    @Mock
    ILocalizationDAO localizationDAO;
    @Mock
    Utilisateur utilisateur;
    @Mock
    IPolicyManager policyManager;
    @Mock
    UserLocale userLocale;
    Localization localization;
    Map<String, ResourceBundle> resourceBundles = new HashMap<>();

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        localizationManager = new SpringLocalizationManager();
        localizationManager.setLocalizationDAO(localizationDAO);
        localizationManager.setUserLocale(userLocale);
        localizationManager.setPolicyManager(policyManager);
        localization = new Localization("localizationString", "entity", "colonne", "defaultString", "fr");
        resourceBundles.put(SpringLocalizationManagerTest.BUNDLE_NAME, ResourceBundle.getBundle(SpringLocalizationManagerTest.BUNDLE_NAME, Locale.FRANCE));
    }

    /**
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testChangeLocale() throws BusinessException, PersistenceException {
        Mockito.when(localizationDAO.getUserLocale(utilisateur)).thenReturn(Optional.ofNullable(userLocale));
        Mockito.when(policyManager.getCurrentUser()).thenReturn(utilisateur);
        final Locale changingLocale = new Locale("fr");
        Mockito.when(userLocale.getLocale()).thenReturn(changingLocale);
        final Locale returnLocale = localizationManager.changeLocale(changingLocale);
        Mockito.verify(localizationDAO).saveUserLocale(userLocale);
        Mockito.verify(userLocale, Mockito.times(2)).setLanguage("fr");
        Assert.assertEquals(changingLocale, returnLocale);
    }

    /**
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testDeleteUnusedProperties() throws BusinessException, PersistenceException {
        localizationManager.deleteUnusedProperties();
        Mockito.verify(localizationDAO).deleteUnusedProperties();
    }

    /**
     *
     */
    @Test
    public void testGetByNKey() {
        Mockito.when(localizationDAO.getByNKey("localizationString", "entity", "colonne", "defaultString")).thenReturn(Optional.ofNullable(localization));
        final Optional<Localization> localization = localizationManager.getByNKey("localizationString", "entity", "colonne", "defaultString");
        Assert.assertEquals(localization, localization);
    }

    /**
     *
     */
    @Test
    public void testGetMessage() {
        Localization.addLanguage("fr");
        Localization.addLanguage("en");
        Mockito.when(Mockito.spy(localizationManager).getUserLocale()).thenReturn(userLocale);
        Mockito.when(userLocale.getLanguage()).thenReturn("fr", "en");
        String message = localizationManager.getMessage(SpringLocalizationManagerTest.BUNDLE_NAME, "PROPERTY_MSG_ERROR");
        Assert.assertEquals("Erreur", message);
        message = localizationManager.getMessage(SpringLocalizationManagerTest.BUNDLE_NAME, "PROPERTY_MSG_ERROR");
        Assert.assertEquals("Error", message);
        message = localizationManager.getMessage(SpringLocalizationManagerTest.BUNDLE_NAME, "UNKNOWN_PROPERTY");
        Assert.assertEquals("UNKNOWN_PROPERTY", message);
    }

    /**
     *
     */
    @Test
    public void testGetUserLocale() {
        final UserLocale returnUserLocale = localizationManager.getUserLocale();
        Assert.assertEquals(userLocale, returnUserLocale);
    }

    /**
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testInitLocale() throws BusinessException, PersistenceException {
        final Answer<String> answerLocalisation = (InvocationOnMock invocation) -> {
            final Object[] args = invocation.getArguments();
            Assert.assertEquals(userLocale, args[0]);
            return null;
        };
        final Answer<String> answerUserLocale = (InvocationOnMock invocation) -> {
            final Object[] args = invocation.getArguments();
            Assert.assertTrue(((String) args[0]).contains("fr"));
            return null;
        };
        Mockito.when(policyManager.getCurrentUser()).thenReturn(utilisateur);
        Mockito.when(localizationDAO.getUserLocale(utilisateur)).thenReturn(Optional.ofNullable(userLocale));
        Mockito.doAnswer(answerLocalisation).when(localizationDAO).saveUserLocale(userLocale);
        Mockito.doAnswer(answerUserLocale).when(userLocale).setLanguage("fr");
        Mockito.when(userLocale.getLocale()).thenReturn(Locale.FRANCE);
        final Locale locale = localizationManager.initLocale(Locale.FRANCE);
        Assert.assertEquals(Locale.FRANCE, locale);
    }

    /**
     *
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testNewPropertiesStringString() {
        Mockito.when(policyManager.getCurrentUser()).thenReturn(utilisateur);
        final Properties newProperties2 = new Properties();
        final Properties newProperties = new Properties();
        newProperties.put("key", "value");
        Mockito.when(localizationDAO.newProperties("entity", "column", utilisateur)).thenReturn(newProperties).thenReturn(newProperties2);
        Properties properties = localizationManager.newProperties("entity", "column");
        Assert.assertEquals(newProperties, properties);
        Assert.assertEquals("value", properties.get("key"));
        Mockito.when(userLocale.getLocale()).thenReturn(Locale.FRANCE);
        newProperties2.put("key2", "value2");
        Mockito.when(localizationDAO.newProperties(eq("entity"), eq("column"), any(Locale.class))).thenReturn(newProperties2);
        properties = localizationManager.newProperties("entity", "column");
        Assert.assertEquals(newProperties2, properties);
        Assert.assertEquals("value2", newProperties2.get("key2"));
    }

    /**
     *
     */
    @Test
    public void testNewPropertiesStringStringLocale() {
        final Properties newProperties = new Properties();
        newProperties.put("key", "value");
        Mockito.when(localizationDAO.newProperties("entity", "column", Locale.FRANCE)).thenReturn(newProperties);
        final Properties properties = localizationManager.newProperties("entity", "column", Locale.FRANCE);
        Assert.assertEquals(newProperties, properties);
        Assert.assertEquals("value", properties.get("key"));
    }

    /**
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testSaveLocalization() throws BusinessException, PersistenceException {
        final Answer<String> answer = (InvocationOnMock invocation) -> {
            final Object[] args = invocation.getArguments();
            Assert.assertEquals(localization, args[0]);
            return null;
        };
        Mockito.doAnswer(answer).when(localizationDAO).saveOrUpdate(localization);
        localizationManager.saveLocalization(localization);
    }

    /**
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    @Test
    public void testSaveUserLocale() throws BusinessException, PersistenceException {
        final Answer<String> answer = (InvocationOnMock invocation) -> {
            final Object[] args = invocation.getArguments();
            Assert.assertEquals(userLocale, args[0]);
            return null;
        };
        Mockito.doAnswer(answer).when(localizationDAO).saveUserLocale(userLocale);
        localizationManager.saveUserLocale(userLocale);
    }

    /**
     *
     */
    @Test
    public void testSetAddLanguage() {
        localizationManager.setAddLanguage("locale");
        Assert.assertTrue(Localization.getLocalisations().contains("locale"));
    }

    /**
     *
     */
    @Test
    public void testSetDefaultLanguage() {
        localizationManager.setAddLanguage("defaultLocalization");
        localizationManager.setDefaultLanguage("defaultLocalization");
        Assert.assertEquals("defaultLocalization", Localization.getDefaultLocalisation());
        localizationManager.setDefaultLanguage(null);
        Assert.assertEquals("fr", Localization.getDefaultLocalisation());
    }
}
