package org.inra.ecoinfo.localization;

import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.entity.UserLocale;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface ILocalizationDAO.
 */
public interface ILocalizationDAO extends IDAO<Localization> {

    /**
     * Delete unused properties.
     *
     * @throws PersistenceException the persistence exception
     */
    void deleteUnusedProperties() throws PersistenceException;

    /**
     * Gets the Localization by n key.
     *
     * @param localizationString the String localization string
     * @param entity the String entity
     * @param colonne the String colonne
     * @param defaultString the String default string
     * @return the Localization by n key
     */
    Optional<Localization> getByNKey(String localizationString, String entity, String colonne, String defaultString);

    /**
     * Gets the UserLocale user locale.
     *
     * @param utilisateur the Utilisateur utilisateur
     * @return the UserLocale user locale
     */
    Optional<UserLocale> getUserLocale(IUser utilisateur);

    /**
     * New properties.
     *
     * @param entity the String entity
     * @param column the String column
     * @param locale the Locale locale
     * @return the Properties properties
     */
    Properties newProperties(String entity, String column, Locale locale);

    /**
     * New properties.
     *
     * @param entity the String entity
     * @param column the String column
     * @param utilisateur the Utilisateur utilisateur
     * @return the Properties properties
     */
    Properties newProperties(String entity, String column, IUser utilisateur);

    /**
     * Save user locale.
     *
     * @param userLocale the UserLocale user locale
     * @throws PersistenceException the persistence exception
     */
    void saveUserLocale(UserLocale userLocale) throws PersistenceException;
}
