package org.inra.ecoinfo.localization.impl;

import com.google.common.base.Strings;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Optional;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import javax.persistence.Transient;
import org.inra.ecoinfo.localization.ILocalizationDAO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.entity.UserLocale;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class SpringLocalizationManager.
 */
public class SpringLocalizationManager implements ILocalizationManager, Serializable {

    /**
     * The Logger LOGGER.
     */
    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(SpringLocalizationManager.class.getName());
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The ILocalizationDAO localization dao.
     */
    private ILocalizationDAO localizationDAO;
    /**
     * The IPolicyManager security context.
     */
    private IPolicyManager policyManager;
    /**
     * The Map<String,Map<String,ResourceBundle>> resource bundles.
     */
    private final Map<String, Map<String, ResourceBundle>> resourceBundles = new HashMap<>();
    /**
     * The UserLocale user locale.
     */
    private UserLocale userLocale;

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#changeLocale(java.util.Locale)
     */
    /**
     *
     * @param newLocale
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Locale changeLocale(final Locale newLocale) throws BusinessException {
        if (newLocale == null) {
            return new Locale(Localization.getDefaultLocalisation());
        }
        UserLocale registeredUserLocale = localizationDAO.getUserLocale(policyManager.getCurrentUser())
                .orElseThrow(() -> new BusinessException("can't change locale"));
        registeredUserLocale.setLanguage(newLocale.getLanguage());
        try {
            localizationDAO.saveUserLocale(registeredUserLocale);
        } catch (PersistenceException ex) {
            throw new BusinessException(ex);
        }
        userLocale.setLanguage(registeredUserLocale.getLocale().getLanguage());
        return registeredUserLocale.getLocale();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#deleteUnusedProperties()
     */
    /**
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Scheduled(cron = "0 00 2 * * MON-FRI")
    @Override
    public void deleteUnusedProperties() throws BusinessException {
        try {
            localizationDAO.deleteUnusedProperties();
        } catch (PersistenceException ex) {
            LOGGER.debug("can't delete unused properties");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#getByNKey(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    /**
     *
     * @param localizationString
     * @param entity
     * @param colonne
     * @param defaultString
     * @return
     */
    @Override
    public Optional<Localization> getByNKey(final String localizationString, final String entity, final String colonne, final String defaultString) {
        return localizationDAO.getByNKey(localizationString, entity, colonne, defaultString);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#getMessage(java.lang.String, java.lang.String)
     */
    /**
     *
     * @param bundleSourcePath
     * @param key
     * @return
     */
    @Override
    public String getMessage(final String bundleSourcePath, final String key) {
        if (resourceBundles.get(bundleSourcePath) == null) {
            initResourcesBundles(bundleSourcePath);
        }
        String message;
        try {
            message = resourceBundles.get(bundleSourcePath).get(getUserLanguage()).getString(key);
        } catch (final Exception e) {
            LOGGER.debug(String.format("no message for bundle %s and ket %s", bundleSourcePath, key));
            message = key;
        }
        return message == null ? key : message;
    }
    
    private String getUserLanguage() {
        return Optional.ofNullable(userLocale)
                .map(ul -> ul.getLanguage())
                .orElse(Optional.ofNullable(policyManager)
                        .map(p -> p.getCurrentUser())
                        .map(u -> u.getLanguage())
                        .orElse("fr"));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#getUserLocale()
     */
    /**
     *
     * @return
     */
    @Override
    public UserLocale getUserLocale() {
        return Optional.ofNullable(userLocale)
                .orElseGet(UserLocale::new);
    }

    /**
     * Sets the void user locale.
     *
     * @param userLocale the UserLocale user locale
     */
    public void setUserLocale(final UserLocale userLocale) {
        this.userLocale = userLocale;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#initLocale(java.util.Locale)
     */
    /**
     *
     * @param defaultSessionLocale
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Locale initLocale(Locale defaultSessionLocale) throws BusinessException {
        Locale localDefaultSessionLocale = defaultSessionLocale;
        if (localDefaultSessionLocale == null) {
            localDefaultSessionLocale = new Locale(Localization.getDefaultLocalisation());
        }
        final UserLocale registeredUserLocale = localizationDAO.getUserLocale(policyManager.getCurrentUser())
                .orElseThrow(() -> new javax.persistence.PersistenceException("can't get userlocale"));
        if (Strings.isNullOrEmpty(registeredUserLocale.getLanguage())) {
            registeredUserLocale.setLanguage(localDefaultSessionLocale.getLanguage());
            try {
                localizationDAO.saveUserLocale(registeredUserLocale);
            } catch (PersistenceException ex) {
                throw new BusinessException(ex);
            }
        }
        userLocale.setLanguage(registeredUserLocale.getLanguage());
        return userLocale.getLocale();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#newProperties(java.lang.String, java.lang.String)
     */
    /**
     *
     * @param entity
     * @param column
     * @return
     */
    @Override
    public Properties newProperties(final String entity, final String column) {
        try {
            return localizationDAO.newProperties(entity, column, policyManager.getCurrentUser());
        } catch (final Exception e) {
            LOGGER.info(String.format("No localisation %s -> %s", entity, column));
            return localizationDAO.newProperties(entity, column, Locale.forLanguageTag(getUserLanguage()));
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#newProperties(java.lang.String, java.lang.String, java.util.Locale)
     */
    /**
     *
     * @param entity
     * @param column
     * @param locale
     * @return
     */
    @Override
    public Properties newProperties(final String entity, final String column, final Locale locale) {
        return localizationDAO.newProperties(entity, column, locale);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#saveLocalization(org.inra.ecoinfo.localization.entity.Localization)
     */
    /**
     *
     * @param localization
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void saveLocalization(final Localization localization) throws BusinessException {
        try {
            localizationDAO.saveOrUpdate(localization);
        } catch (PersistenceException ex) {
            throw new BusinessException(ex);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#saveUserLocale(org.inra.ecoinfo.localization.entity.UserLocale)
     */
    /**
     *
     * @param userLocale
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void saveUserLocale(final UserLocale userLocale) throws BusinessException {
        try {
            localizationDAO.saveUserLocale(userLocale);
        } catch (PersistenceException ex) {
            throw new BusinessException(ex);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#setAddLanguage(java.lang.String)
     */
    /**
     *
     * @param language
     */
    @Override
    public void setAddLanguage(final String language) {
        for (final String localization : Localization.getLocalisations()) {
            if (localization.equals(language)) {
                return;
            }
        }
        Localization.getLocalisations().add(language);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationManager#setDefaultLanguage(java.lang.String)
     */
    /**
     *
     * @param defaultLocalization
     */
    @Override
    public void setDefaultLanguage(String defaultLocalization) {
        String localDefaultLocalization = defaultLocalization;
        if (Strings.isNullOrEmpty(localDefaultLocalization)) {
            localDefaultLocalization = Locale.FRANCE.getLanguage();
        }
        for (final String localization : Localization.getLocalisations()) {
            if (localization.equals(localDefaultLocalization)) {
                Localization.getLocalisations().remove(Localization.getLocalisations().indexOf(localDefaultLocalization));
                break;
            }
        }
        Localization.getLocalisations().add(0, localDefaultLocalization);
        Localization.setDefaultLocalisation(localDefaultLocalization);
    }

    /**
     * Sets the void localization dao.
     *
     * @param localizationDAO the ILocalizationDAO localization dao
     */
    public void setLocalizationDAO(final ILocalizationDAO localizationDAO) {
        this.localizationDAO = localizationDAO;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Inits the resources bundles.
     *
     * @param bundleSourcePath the String bundle source path
     */
    private void initResourcesBundles(final String bundleSourcePath) {
        resourceBundles.put(bundleSourcePath, new HashMap<>());
        Localization.getLocalisations().stream().forEach((localization) -> {
            try {
                ResourceBundle resourceBundle = Optional.ofNullable(bundleSourcePath)
                        .map(bundlePath -> SpringLocalizationManager.class.getResourceAsStream(String.format("../../../../../%s_%s.properties", bundlePath.replaceAll("\\.", "/"), localization)))
                        .map(stream -> getInputStream(stream))
                        .map(reader -> getPropertyResourceBundle(reader))
                        .orElseGet(() -> ResourceBundle.getBundle(bundleSourcePath, new Locale(localization)));
                if (resourceBundle == null) {
                    throw new MissingResourceException(localization, localization, localization);
                }
                resourceBundles.get(bundleSourcePath).put(localization, resourceBundle);
            } catch (MissingResourceException e) {
                LOGGER.debug(String.format("Can't find bundle %s for localisation", bundleSourcePath, localization));
            }
        });
    }

    private Reader getInputStream(InputStream stream) {
        try {
            return new InputStreamReader(stream, "UTF-8");
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            return null;
        }
    }

    private ResourceBundle getPropertyResourceBundle(Reader reader) {
        try {
            return new PropertyResourceBundle(reader);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            return null;
        }
    }

    /**
     *
     * @param nodeable
     * @return
     */
    @Override
    public String getLocalName(INodeable nodeable) {
        if (nodeable == null) {
            return "";
        }
        
        return getByNKey(getUserLanguage(), nodeable.getNodeableType().getSimpleName().toLowerCase(), "name", nodeable.getName())
                .map(localization -> localization.getLocaleString())
                .orElseGet(
                        () -> getByNKey(getUserLanguage(), nodeable.getNodeableType().getName(), "nom", nodeable.getName())
                                .map(localization -> localization.getLocaleString())
                                .orElseGet(() -> nodeable.getName())
                );
    }

    /**
     *
     * @param flatNode
     * @return
     */
    @Override
    public String getLocalName(IFlatNode flatNode) {
        
        if (flatNode == null) {
            return "";
        }
        
        return getByNKey(getUserLanguage(), flatNode.getTypeResource().getSimpleName().toLowerCase(), "name", flatNode.getName())
                .map(localization -> localization.getLocaleString())
                .orElseGet(
                        () -> getByNKey(getUserLanguage(), flatNode.getTypeResource().getName(), "nom", flatNode.getName())
                                .map(localization -> localization.getLocaleString())
                                .orElseGet(() -> flatNode.getName())
                );
    }

    /**
     *
     * @param localizationString
     * @param entity
     * @param colonne
     * @param defaultString
     * @param localeString
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public Localization createorUpdateLocalization(final String localizationString, final String entity, final String colonne, final String defaultString, final String localeString) throws BusinessException {
        Optional<Localization> localizationOpt = getByNKey(localizationString, entity, colonne, defaultString);
        Localization localization;
        if (!localizationOpt.isPresent()) {
            localization = new Localization(
                    localizationString,
                    entity,
                    colonne,
                    defaultString,
                    localeString);
        } else {
            localization = localizationOpt.get();
            localization.setLocaleString(localeString);
        }
        saveLocalization(localization);
        return localization;
    }
}
