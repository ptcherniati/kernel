package org.inra.ecoinfo.localization.jpa;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.localization.ILocalizationDAO;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.entity.Localization_;
import org.inra.ecoinfo.localization.entity.UserLocale;
import org.inra.ecoinfo.localization.entity.UserLocale_;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class JPALocalizationDAO.
 */
public class JPALocalizationDAO extends AbstractJPADAO<Localization> implements ILocalizationDAO, Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The Constant QUERY_LOCALISATION.
     */
    /**
     * Delete unused properties.
     *
     */
    @Override
    @SuppressWarnings("rawtypes")
    public void deleteUnusedProperties() {
        CriteriaQuery<Tuple> criteria = builder.createTupleQuery();
        Root<Localization> localization = criteria.from(Localization.class);
        criteria.distinct(true);
        criteria.multiselect(localization.get(Localization_.entite), localization.get(Localization_.colonne));
        final Iterator<Tuple> results = getResultList(criteria).listIterator();
        while (results.hasNext()) {
            final Tuple tuple = results.next();
            tryDeleteUnusedProperties(tuple);
        }
    }

    /**
     * Delete unused properties.
     *
     * @param entity the String entity
     * @param column the String column
     */
    public void deleteUnusedProperties(final String entity, final String column) {
        Query query = entityManager.createNativeQuery(String.format("select %s from %s ", column.toLowerCase(), entity.toLowerCase()));
        @SuppressWarnings("rawtypes")
        List results = query.getResultList();

        CriteriaDelete<Localization> delete = builder.createCriteriaDelete(Localization.class);
        Root<Localization> localization = delete.from(Localization.class);
        delete.where(
                builder.equal(localization.get(Localization_.entite), entity),
                builder.equal(localization.get(Localization_.colonne), column),
                builder.not(localization.get(Localization_.defaultString).in(results))
        );
        delete(delete);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationDAO#getByNKey(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    /**
     *
     * @param localizationString
     * @param entity
     * @param colonne
     * @param defaultString
     * @return
     */
    @Override
    public Optional<Localization> getByNKey(final String localizationString, final String entity, final String colonne, final String defaultString) {

        CriteriaQuery<Localization> criteria = builder.createQuery(Localization.class);
        Root<Localization> localization = criteria.from(Localization.class);
        criteria.where(
                builder.equal(localization.get(Localization_._localization), localizationString),
                builder.equal(localization.get(Localization_.entite), entity),
                builder.equal(localization.get(Localization_.colonne), colonne),
                builder.equal(localization.get(Localization_.defaultString), defaultString)
        );
        criteria.select(localization);
        return getOptional(criteria);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationDAO#getUserLocale(org.inra.ecoinfo.identification.entity.Utilisateur)
     */
    /**
     *
     * @param utilisateur
     * @return
     */
    @Override
    public Optional<UserLocale> getUserLocale(final IUser utilisateur) {

        CriteriaQuery<UserLocale> query = builder.createQuery(UserLocale.class);
        Root<UserLocale> userLocale = query.from(UserLocale.class);
        query.where(builder.equal(userLocale.get(UserLocale_.id), utilisateur.getId()));
        query.select(userLocale);
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationDAO#newProperties(java.lang.String, java.lang.String, java.util.Locale)
     */
    /**
     *
     * @param entity
     * @param column
     * @param locale
     * @return
     */
    @Override
    public Properties newProperties(final String entity, final String column, final Locale locale) {
        final Properties p = new Properties();
        String localization;
        if (locale == null || !Localization.getLocalisations().contains(locale.getLanguage())) {
            localization = Localization.getDefaultLocalisation();
        } else {
            localization = locale.getLanguage();
        }

        CriteriaQuery<Localization> criteria = builder.createQuery(Localization.class);
        Root<Localization> root = criteria.from(Localization.class);
        criteria.where(
                builder.equal(root.get(Localization_._localization), localization),
                builder.equal(root.get(Localization_.entite), entity),
                builder.equal(root.get(Localization_.colonne), column)
        );
        criteria.select(root);
        final List<Localization> localizations = getResultList(criteria);
        localizations.stream().forEach((localization2) -> {
            p.setProperty(localization2.getDefaultString(), localization2.getLocaleString());
        });
        return p;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationDAO#newProperties(java.lang.String, java.lang.String, org.inra.ecoinfo.identification.entity.Utilisateur)
     */
    /**
     *
     * @param entity
     * @param column
     * @param utilisateur
     * @return
     */
    @Override
    public Properties newProperties(final String entity, final String column, final IUser utilisateur) {
        final Properties p = new Properties();
        final Optional<UserLocale> localization = getUserLocale(utilisateur);
        if (!localization.isPresent()) {
            LOGGER.error("can't find localisation");
            return new Properties();
        }
        CriteriaQuery<Localization> criteria = builder.createQuery(Localization.class);
        Root<Localization> root = criteria.from(Localization.class);
        criteria.where(
                builder.equal(root.get(Localization_._localization), localization.get().getLanguage()),
                builder.equal(root.get(Localization_.entite), entity),
                builder.equal(root.get(Localization_.colonne), column)
        );
        criteria.select(root);
        final List<Localization> localizations = getResultList(criteria);
        localizations.stream().forEach((localization2) -> {
            p.setProperty(localization2.getDefaultString(), localization2.getLocaleString());
        });
        return p;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.ILocalizationDAO#saveUserLocale(org.inra.ecoinfo.localization.entity.UserLocale)
     */
    /**
     *
     * @param userLocale
     * @throws PersistenceException
     */
    @Override
    public void saveUserLocale(final UserLocale userLocale) throws PersistenceException {
        try {
            entityManager.persist(userLocale);
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    private void tryDeleteUnusedProperties(final Tuple tuple) {
        deleteUnusedProperties((String) tuple.get(0), (String) tuple.get(1));
    }
}
