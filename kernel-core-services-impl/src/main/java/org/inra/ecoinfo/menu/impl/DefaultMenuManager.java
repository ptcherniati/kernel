package org.inra.ecoinfo.menu.impl;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.menu.IMenuBuilder;
import org.inra.ecoinfo.menu.IMenuManager;
import org.inra.ecoinfo.menu.Menu;
import org.inra.ecoinfo.menu.MenuItem;
import org.inra.ecoinfo.mga.caching.ITreeApplicationCacheManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class DefaultMenuManager.
 */
public class DefaultMenuManager implements IMenuManager, Serializable {

    /**
     * The Constant serialVersionUID.
     */
    protected static final long serialVersionUID = 1L;
    /**
     * The Menu menu.
     */
    protected Menu menu;
    /**
     * The IMenuBuilder menu builder.
     */
    protected IMenuBuilder menuBuilder;
    /**
     * The String path menu.
     */
    protected String pathMenu = IMenuManager.class.getResource("/META-INF/menu.xml").getPath();

    IPolicyManager policyManager;
    ITreeApplicationCacheManager treeApplicationCacheManager;

    /**
     * Instantiates a new DefaultMenuManager default menu manager.
     */
    public DefaultMenuManager() {
    }

    /**
     * Builds the menu.
     */
    public void buildMenu() {
        menu = menuBuilder.build(pathMenu);
        readConfigurations(menu.getMenuItems());

    }

    protected void readConfigurations(List<MenuItem> menuItems) {
        menuItems.forEach((menuItem) -> {
            if (!Strings.isNullOrEmpty(menuItem.getDirectAction()) && menuItem.getCodeConf()!=null) {
                treeApplicationCacheManager.getConfigurationMap().computeIfAbsent(menuItem.getDirectAction(), k -> new LinkedList<>())
                        .add(menuItem.getCodeConf());
            }
            readConfigurations(menuItem.getChildren());
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.menu.IMenuManager#buildRestrictedMenu(org.inra.ecoinfo.security.IPolicyManager)
     */
    /**
     *
     * @return @throws CloneNotSupportedException
     */
    @Override
    @Transactional
    public Menu buildRestrictedMenu() throws CloneNotSupportedException {
        final Menu restrictedMenu = new Menu();
        if (policyManager.isRoot()) {
            return menu;
        }
        final List<MenuItem> restrictedMenuItems = buildRestrictedMenuItems(menu.getMenuItems());
        if (!restrictedMenuItems.isEmpty()) {
            restrictedMenu.setMenuItems(restrictedMenuItems);
        }
        return restrictedMenu;
    }

    /**
     * Builds the restricted menu items.
     *
     * @param menuItems the List<MenuItem> menu items
     * @return the List<MenuItem> list
     * @throws CloneNotSupportedException the clone not supported exception
     */
    public List<MenuItem> buildRestrictedMenuItems(final List<MenuItem> menuItems) throws CloneNotSupportedException {

        final List<MenuItem> restrictedMenuItems = new LinkedList<>();
        for (final MenuItem menuItem : menuItems) {
            assert menuItem != null : "null menuItem";
            final MenuItem restrictedMenuItem = menuItem.clone();
            final String permissions = menuItem.buildPermissionsArray();
            if (!menuItem.getChildren().isEmpty()) {
                restrictedMenuItem.setChildren(buildRestrictedMenuItems(menuItem.getChildren()));
                disableTopMenu(restrictedMenuItem);
            } else if (policyManager.getCurrentUserLogin() != null && !policyManager.isRoot() && !restrictedMenuItem.getRoot()) {
                restrictedMenuForNonRootUser(restrictedMenuItem, permissions);
            } else {
                restrictedMenuItem.setDisabled(!policyManager.isRoot());
            }
            restrictedMenuItems.add(restrictedMenuItem);
        }
        return restrictedMenuItems;
    }

    /**
     *      * test menuitem </p>
     *      * use policy if menuItem have permissions</p>
     *      * use restrictedMenuWithLocalPermission if menuItem has children</p>
     *      * else set not restricted</p>
     *
     * @param restrictedMenuItem
     * @param permissions
     */
    protected void restrictedMenuForNonRootUser(final MenuItem restrictedMenuItem, final String permissions) {
        if (permissions != null) {
            final Integer codeConfiguration = restrictedMenuItem.getCodeConf();
            final String resources = restrictedMenuItem.getResources();
            final boolean hasRight = policyManager.checkCurrentUserActivity(codeConfiguration, permissions, resources);
            restrictedMenuItem.setDisabled(!hasRight);
        } else if (!restrictedMenuItem.getChildren().isEmpty()) {
            restrictedMenuWithLocalPermission(restrictedMenuItem);
        } else {
            restrictedMenuItem.setDisabled(!policyManager.isRoot() || !Strings.isNullOrEmpty(restrictedMenuItem.getPermissions()));
        }
    }

    /**
     *      * if all child are disabled then turn menuItem to disabled</p>
     *
     * @param restrictedMenuItem
     */
    protected void restrictedMenuWithLocalPermission(final MenuItem restrictedMenuItem) {
        int itemDisabled = 0;
        itemDisabled = restrictedMenuItem.getChildren().stream()
                .filter((menuChild) -> (!menuChild.getDisabled()))
                .map((_item) -> 1)
                .reduce(itemDisabled, Integer::sum);
        if (itemDisabled == 0) {
            restrictedMenuItem.setDisabled(true);
        }
    }

    /**
     *      * If at least one child in menu item is disabled then the item is not
     * disabled</p>
     *
     * @param menuItem
     */
    protected void disableTopMenu(final MenuItem menuItem) {
        Boolean isDisabled = true;
        for (MenuItem child : menuItem.getChildren()) {
            if (!child.getDisabled()) {
                isDisabled = false;
                break;
            }
        }
        menuItem.setDisabled(isDisabled);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.menu.IMenuManager#getMenu()
     */
    /**
     *
     * @return
     */
    @Override
    public Menu getMenu() {
        return menu;
    }

    /**
     * Sets the void menu builder.
     *
     * @param menuBuilder the IMenuBuilder menu builder
     */
    public void setMenuBuilder(final IMenuBuilder menuBuilder) {
        this.menuBuilder = menuBuilder;
        buildMenu();
    }

    /**
     *
     * @return
     */
    public IPolicyManager getPolicyManager() {
        return policyManager;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @return
     */
    public ITreeApplicationCacheManager getTreeApplicationCacheManager() {
        return treeApplicationCacheManager;
    }

    /**
     *
     * @param treeApplicationCacheManager
     */
    public void setTreeApplicationCacheManager(ITreeApplicationCacheManager treeApplicationCacheManager) {
        this.treeApplicationCacheManager = treeApplicationCacheManager;
    }

}
