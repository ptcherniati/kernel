package org.inra.ecoinfo.menu.impl;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.menu.IMenuBuilder;
import org.inra.ecoinfo.menu.Menu;
import org.inra.ecoinfo.menu.MenuItem;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.xml.sax.SAXException;

/**
 * The Class MenuXMLDigesterBuilder.
 */
public class MenuXMLDigesterBuilder implements IMenuBuilder, Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new MenuXMLDigesterBuilder menu xml digester builder.
     */
    public MenuXMLDigesterBuilder() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.menu.IMenuBuilder#build(java.lang.String)
     */

    /**
     *
     * @param menuPath
     * @return
     */
    
    @Override
    public Menu build(final String menuPath) {
        Menu menu = null;
        final Digester digester = new Digester();
        digester.setUseContextClassLoader(true);
        digester.addObjectCreate("menu", Menu.class);
        digester.addObjectCreate("menu/menuEntry", MenuItem.class);
        digester.addSetNext("menu/menuEntry", "addMenuItem");
        digester.addSetProperties("menu/menuEntry");
        digester.addObjectCreate("menu/menuEntry/menuEntry", MenuItem.class);
        digester.addSetNext("menu/menuEntry/menuEntry", "addMenuItem");
        digester.addSetProperties("menu/menuEntry/menuEntry");
        try {
            menu = (Menu) digester.parse(new File(menuPath));
        } catch (final IOException | SAXException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in deleteRequestsFromIds", e);
        }
        return menu;
    }
}
