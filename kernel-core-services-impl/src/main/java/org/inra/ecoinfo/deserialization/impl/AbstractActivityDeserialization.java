package org.inra.ecoinfo.deserialization.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import org.apache.commons.digester.Digester;
import org.assertj.core.util.Strings;
import org.inra.ecoinfo.deserialization.AbstractDeserialization;
import org.inra.ecoinfo.deserialization.IUtilisateurDeserialization;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.mga.business.composite.ICompositeActivity;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;

/**
 * The Class MailAdminSerialization.
 */
public abstract class AbstractActivityDeserialization extends AbstractDeserialization implements IUtilisateurDeserialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/activityDeserialization.xsd";

    /**
     *
     */
    public static final String HEADER_CSV_FILE = "login;resource_path;role;activity_type";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s";

    /**
     *
     */
    public static final String LINE_PROPERTY_CSV_FILE = ";%s;%s";
    /**
     * The Constant MODULE_NAME.
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractActivityDeserialization.class.getName());
    private static Map<Integer, String> getEmptyDates() {
        Map<Integer, String> emptyMap = new HashMap();
        emptyMap.put(0, null);
        emptyMap.put(1, null);
        return emptyMap;
    }
    private String id;
    /**
     * The mail admin.
     */
    private IPolicyManager policyManager;
    private IUtilisateurDAO utilisateurDAO;

    /**
     *
     */
    public AbstractActivityDeserialization() {
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        super.createConfig(digester);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractSerialization#getSchemaPath()
     */
    /**
     *
     * @return
     */
    @Override
    public String getSchemaPath() {
        return AbstractActivityDeserialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#isAutoLoad()
     */
    /**
     *
     * @return
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param inZip
     * @throws BusinessException
     */
    @Override
    public void deSerialize(File inZip) throws BusinessException {
        LOGGER.info("*************************************************************");
        LOGGER.info("*               déserialization privilèges                  *");
        LOGGER.info("*************************************************************");
        LOGGER.info("");
        LOGGER.info(getClass().getName());
        LOGGER.info("");
        TransactionStatus tr = utilisateurDAO.beginNewTransaction();
        try (InputStream inputStream = openEntry(inZip, getActivityEntry())) {
            Scanner sc = new Scanner(inputStream, StandardCharsets.UTF_8.name());
            sc.nextLine();
            boolean debut = true;
            while (sc.hasNextLine()) {
                final String line = sc.nextLine();
                if (debut) {
                    debut = false;
                    continue;
                }
                registerLine(line);
            }
            inputStream.close();
            utilisateurDAO.commitTransaction(tr);
        } catch (IOException ex) {
            LOGGER.error("can't write utilisateur ", ex);
            utilisateurDAO.rollbackTransaction(tr);
        }
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    private String registerLine(String line) {
        LOGGER.info(line);
        try {
            final String[] fields = line.split("[\" ]*;[\" ]*");
            final String login = fields[0];
            final String path = fields[1];
            String activity = fields[2];
            final WhichTree whichTree = WhichTree.convertToWhichTree(fields[3]);
            final Map<Integer, String> dates = getEmptyDates();
            for (int i = 4; i < fields.length - 1; i = i + 2) {
                dates.put(Integer.valueOf(fields[i]), fields[i + 1]);
            }
            TransactionStatus tr = utilisateurDAO.beginDefaultTransaction();
            try {
                addActivity(login, whichTree, activity, path, dates.get(0), dates.get(1));
                utilisateurDAO.commitTransaction(tr);
            } catch (BusinessException | DateTimeParseException | BadExpectedValueException | JsonProcessingException ex) {
                LOGGER.error("can't read activity line ", ex);
                utilisateurDAO.rollbackTransaction(tr);
            }
        } catch (PersistenceException ex) {
            LOGGER.error(String.format("Can't read line %s for activity", line), ex);
            return ":-(";
        }
        return ":-)";
    }


    /**
     *
     * @param groupName
     * @param whichTree
     * @param activityNames
     * @param path
     * @param dateDeDebutString
     * @param dateDeFinString
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     * @throws BadExpectedValueException
     * @throws JsonProcessingException
     */
    public boolean addActivity(String groupName, WhichTree whichTree, String activityNames, String path, String dateDeDebutString, String dateDeFinString) throws BusinessException, PersistenceException, DateTimeParseException,
            BadExpectedValueException, JsonProcessingException {
        if (path.isEmpty()) {
            return false;
        }
        final Optional<Group> groupOpt = policyManager.getGroupByGroupNameAndWhichTree(groupName, whichTree);
        if (groupOpt.isPresent()) {
            ICompositeActivity group = groupOpt.get().getOwnGroup(whichTree);
            LocalDate dateDebut = Strings.isNullOrEmpty(dateDeDebutString) ? null : DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateDeDebutString).toLocalDate();
            LocalDate dateFin = Strings.isNullOrEmpty(dateDeFinString) ? null : DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateDeFinString).toLocalDate();
            List<LocalDate> dates = Arrays.asList(new LocalDate[]{dateDebut, dateFin});
            String[] rolesNames = activityNames.split(",");
            for (int idx = 0; idx < rolesNames.length; idx++) {
                String r = rolesNames[idx];
                Activities activities = Activities.convertToActivity(r);
                Map<Long, List<LocalDate>> roles = group.getActivitiesMap().computeIfAbsent(activities, k -> new HashMap<>());
                String base = whichTree == WhichTree.TREEDATASET ? "NodeDataSet" : "NodeRefData";
                List<INode> nodes = policyManager.getMgaServiceBuilder().getRecorder().getEntityManager().
                        createQuery(String.format("from  %s n where  n.realNode.path like :like", base))
                        .setParameter("like", path + "%").getResultList();
                if (nodes.isEmpty()) {
                    return false;
                }
                Set<Long> ids = new HashSet();
                nodes.stream().map((node) -> {
                    ids.add(node.getId());
                    return node;
                }).map((node) -> {
                    ArrayList<INode> tmp = new ArrayList();
                    node.scanNodes(tmp);
                    return tmp;
                }).forEach((tmp) -> {
                    tmp.stream().forEach(t -> ids.add(t.getId()));
                });
                ids.forEach((id) -> {
                    roles.put(id, dates);
                });
            }

            group.synchronizeActivitiesMap();
            policyManager.mergeActivity(group);
            LOGGER.debug(String.format(
                    "groupName :%s%n"
                    + "whichTree :%s%n"
                    + "activityName :%s%n"
                    + "path :%s%n"
                    + "dateDeDebutString :%s%n"
                    + "dateDeFinString :%s%n"
                    + "role :%s%n",
                    groupName, whichTree.name(), activityNames, path, dateDeDebutString, dateDeFinString, group.getActivitiesMap()
            ));
        } else {
            throw new BusinessException(String.format("can't find group %s", groupName));
        }
        return true;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @return
     */
    public abstract String getActivityEntry();
}
