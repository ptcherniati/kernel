package org.inra.ecoinfo.deserialization;

/**
 * The Interface IIdSerialization.
 */
public interface IUtilisateurDeserialization {

    /**
     *
     * @return
     */
    String getId();
}
