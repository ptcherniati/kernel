package org.inra.ecoinfo.deserialization;

/**
 * The Interface IIdDeserialization.
 */
public interface IActivityDeserialization {

    /**
     *
     * @return
     */
    String getId();
}
