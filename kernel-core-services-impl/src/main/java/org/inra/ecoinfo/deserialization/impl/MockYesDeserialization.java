package org.inra.ecoinfo.deserialization.impl;

import java.io.File;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.deserialization.AbstractDeserialization;
import org.inra.ecoinfo.deserialization.IUtilisateurDeserialization;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class MailAdminDeserialization.
 */
public class MockYesDeserialization extends AbstractDeserialization implements IUtilisateurDeserialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/utilisateurDeserialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private String moduleName = "mockDeserialization";

    /**
     *
     * @param string
     * @param id
     */
    public MockYesDeserialization(String id) {
        moduleName = id;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#createConfig(org.apache.commons.digester.Digester)
     */

    /**
     *
     * @param digester
     */

    @Override
    public void createConfig(Digester digester) {
        //nothing to do
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#getModuleName()
     */

    /**
     *
     * @return
     */

    @Override
    public String getModuleName() {
        return moduleName;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractDeserialization#getSchemaPath()
     */

    /**
     *
     * @return
     */

    @Override
    public String getSchemaPath() {
        return MockYesDeserialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#isAutoLoad()
     */

    /**
     *
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @param inZip
     * @throws BusinessException
     */
    @Override
    public void deSerialize(File inZip) throws BusinessException {
        //do nothing
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return moduleName;
    }
}
