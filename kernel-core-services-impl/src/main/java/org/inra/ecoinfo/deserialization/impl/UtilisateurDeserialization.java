package org.inra.ecoinfo.deserialization.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipInputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.jpa.IGenericDAO;
import org.inra.ecoinfo.deserialization.AbstractDeserialization;
import org.inra.ecoinfo.deserialization.IUtilisateurDeserialization;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;

/**
 * The Class MailAdminDeserialization.
 */
public class UtilisateurDeserialization extends AbstractDeserialization implements IUtilisateurDeserialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/utilisateurDeserialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "utilisateurDeserialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(UtilisateurDeserialization.class.getName());
    private static final String UTILISATEURS_ENTRY = "utilisateur.csv";
    private static final String GROUPS_ENTRY = "group.csv";

    /**
     *
     */
    public static final String HEADER_CSV_FILE = "id;active;email;emploi;isroot;language;login;nom;password;poste;prenom";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
    private String id;
    /**
     * The mail admin.
     */
    private IUtilisateurDAO utilisateurDAO;
    private IMgaRecorder mgaRecorderDAO;
    private IPolicyManager policyManager;

    /**
     *
     */
    protected IGenericDAO genericDAO;
    private boolean firstLine;
    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     *
     * @param genericDAO
     */
    public void setGenericDAO(IGenericDAO genericDAO) {
        this.genericDAO = genericDAO;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#createConfig(org.apache.commons.digester.Digester)
     */

    /**
     *
     * @param digester
     */

    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("repositoryURI", "setRepositoryURI", 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#getModuleName()
     */

    /**
     *
     * @return
     */

    @Override
    public String getModuleName() {
        return UtilisateurDeserialization.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractDeserialization#getSchemaPath()
     */

    /**
     *
     * @return
     */

    @Override
    public String getSchemaPath() {
        return UtilisateurDeserialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#isAutoLoad()
     */

    /**
     *
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param inZip
     * @throws BusinessException
     */
    @Override
    public void deSerialize(File inZip) throws BusinessException {
        LOGGER.info("*************************************************************");
        LOGGER.info("*               déserialization utilisateur                 *");
        LOGGER.info("*************************************************************");
        LOGGER.info("");
        LOGGER.info(getClass().getName());
        LOGGER.info("");
        LOGGER.info("");
        TransactionStatus tr = utilisateurDAO.beginNewTransaction();
        Map<String, List<Utilisateur>> usersByGroup = new HashMap();
        try (ZipInputStream inputStream = openEntry(inZip, UTILISATEURS_ENTRY)) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            reader.lines()
                    .skip(1)
                    .forEach(line -> registerUserLine(line, usersByGroup));
        } catch (IOException ex) {
            LOGGER.error("can't write utilisateur ", ex);
            utilisateurDAO.rollbackTransaction(tr);
            return;
        }
        try (ZipInputStream inputStream = openEntry(inZip, GROUPS_ENTRY)) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            reader.lines()
                    .skip(1)
                    .forEach(line -> registerGroupLine(line, usersByGroup));
        } catch (IOException ex) {
            LOGGER.error("can't write utilisateur ", ex);
            utilisateurDAO.rollbackTransaction(tr);
            return;
        }
        utilisateurDAO.commitTransaction(tr);
    }

    private Utilisateur registerUserLine(String line, Map<String, List<Utilisateur>> usersByGroup) {
        //LOGGER.info(line);
        String[] fields = line.split("[\" ]*;[\" ]*");
        final String login = fields[6];
        Utilisateur utilisateur = null;
        try {
            utilisateur = utilisateurDAO.getByLogin(login).orElse(new Utilisateur());
            utilisateur.setActive(Boolean.valueOf(fields[1].trim()));
            utilisateur.setEmail(fields[2]);
            utilisateur.setEmploi(fields[3]);
            utilisateur.setIsRoot(Boolean.valueOf(fields[4].trim()));
            utilisateur.setLanguage(fields[5]);
            utilisateur.setLogin(login);
            utilisateur.setNom(fields[7]);
            utilisateur.setPassword(fields[8]);
            utilisateur.setPoste(fields[9]);
            utilisateur.setPrenom(fields[10]);
            utilisateurDAO.saveOrUpdate(utilisateur);
            List<WhichTree> collectWhichTree = utilisateur.getAllGroups().stream().map(g -> g.getWhichTree()).collect(Collectors.toList());
            WhichTree[] values = WhichTree.values();
            for (int i = 0; i < values.length; i++) {
                WhichTree value = values[i];
                if (collectWhichTree.contains(value)) {
                    continue;
                }
                Group group = mgaRecorderDAO.createGroup(login, value, GroupType.USER_TYPE);
                mgaRecorderDAO.saveOrUpdate(group);
                utilisateur = utilisateurDAO.addGroupToUser(utilisateur, group).orElse(null);
            }
        } catch (PersistenceException ex) {
            LOGGER.error(String.format("Can't read line %s for utilisateur", line), ex);
        }
        final Utilisateur user = utilisateur;
        Stream.of(fields[11].split(","))
                .filter(groupName->!"public|TREEDATASET".equals(groupName))
                .forEach(groupeName-> usersByGroup
                        .computeIfAbsent(groupeName, k->new LinkedList<>())
                        .add(user));
        return utilisateur;
    }

    private void registerGroupLine(String line, Map<String, List<Utilisateur>> usersByGroup) {
        //LOGGER.info(line);
        String[] fields = line.split("[\" ]*;[\" ]*");
        final String groupName = fields[2];
        final String which_tree = fields[3];
        Utilisateur utilisateur = null;
        Group group = mgaRecorderDAO.getGroupByGroupNameAndWhichTree(groupName, WhichTree.convertToWhichTree(which_tree))
                .orElse(mgaRecorderDAO.createGroup(groupName, WhichTree.convertToWhichTree(which_tree), GroupType.USER_TYPE));
        mgaRecorderDAO.saveOrUpdate(group);
        String code = String.format("%s|%s",group.getGroupName(), group.getWhichTree().name());
        usersByGroup.getOrDefault(code, new LinkedList<>()).stream()
                .forEach(user->addGroup(user, group));
        mgaRecorderDAO.saveOrUpdate(group);
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param mgaRecorderDAO
     */
    public void setMgaRecorderDAO(IMgaRecorder mgaRecorderDAO) {
        this.mgaRecorderDAO = mgaRecorderDAO;
    }

    private void addGroup(Utilisateur user, Group group) {
        try {
            user = utilisateurDAO.merge(user);
            if(!user.getAllGroups().contains(group)){
                policyManager.addGroupToUser(user, group);
            }
        } catch (PersistenceException ex) {
            LOGGER.error(String.format("Can't add group %s to user %s", group.getGroupName(), user.getGroupName()), ex);
        }
    }
}
