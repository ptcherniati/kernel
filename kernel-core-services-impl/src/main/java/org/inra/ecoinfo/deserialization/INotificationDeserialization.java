package org.inra.ecoinfo.deserialization;

/**
 * The Interface IIdSerialization.
 */
public interface INotificationDeserialization {

    /**
     *
     * @return
     */
    String getId();
}
