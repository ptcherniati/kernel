package org.inra.ecoinfo.deserialization.impl;

import com.google.common.base.Strings;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.deserialization.AbstractDeserialization;
import org.inra.ecoinfo.deserialization.INotificationDeserialization;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.INotificationDAO;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.serialization.impl.NotificationSerialization;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;

/**
 * The Class MailAdminDeserialization.
 */
public class NotificationDeserialization extends AbstractDeserialization implements INotificationDeserialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/notificationDeserialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "notificationDeserialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(NotificationDeserialization.class.getName());
    private static final String NOTIFICATIONS_ENTRY = "notification.csv";

    /**
     *
     */
    public static final String HEADER_CSV_FILE = "id;active;email;emploi;isroot;language;login;nom;password;poste;prenom";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    /**
     *
     */
    public static final String PATTERN_CONCAT_FILE = "%s/%s";
    private String id;
    /**
     * The mail admin.
     */
    private INotificationDAO notificationDAO;
    private IUtilisateurDAO utilisateurDAO;

    private ICoreConfiguration configuration;

    /**
     *
     * @param configuration
     */
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param notificationDAO
     */
    public void setNotificationDAO(INotificationDAO notificationDAO) {
        this.notificationDAO = notificationDAO;
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        super.createConfig(digester);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#getModuleName()
     */
    /**
     *
     * @return
     */
    @Override
    public String getModuleName() {
        return NotificationDeserialization.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractDeserialization#getSchemaPath()
     */
    /**
     *
     * @return
     */
    @Override
    public String getSchemaPath() {
        return NotificationDeserialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#isAutoLoad()
     */
    /**
     *
     * @return
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param inZip
     * @throws BusinessException
     */
    @Override
    public void deSerialize(File inZip) throws BusinessException {
        LOGGER.info("*************************************************************");
        LOGGER.info("*              déserialization notifications                *");
        LOGGER.info("*************************************************************");
        LOGGER.info("");
        LOGGER.info(getClass().getName());
        LOGGER.info("");
        LOGGER.info("");
        TransactionStatus tr = utilisateurDAO.beginNewTransaction();
        try (InputStreamReader inputStreamReader = new InputStreamReader(openEntry(inZip, NOTIFICATIONS_ENTRY), StandardCharsets.UTF_8)) {
            CSVReader reader = new CSVReaderBuilder(inputStreamReader)
                    .withCSVParser(new CSVParserBuilder().withSeparator(';').build())
                    .build();
            List<String> attachments = new LinkedList();
            reader.skip(1);
            String[] line;
            while ((line = reader.readNext()) != null) {
                registerLine(line, attachments);

            }
            attachments.stream()
                    .filter(a -> !Strings.isNullOrEmpty(a) || "null".equals(a))
                    .forEach(a -> writeFile(inZip, a));
            utilisateurDAO.commitTransaction(tr);
        } catch (IOException ex) {
            LOGGER.error("can't write notification ", ex);
            utilisateurDAO.rollbackTransaction(tr);
        } catch (CsvValidationException ex) {
            LOGGER.error("can't write notification ", ex);
            utilisateurDAO.rollbackTransaction(tr);
        }
    }

    private void registerLine(String[] fields, List<String> attachments) {
        //LOGGER.info(line);
        Notification notification = null;
        try {
            final String login = fields[1];
            final LocalDateTime date = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM_SS, fields[2]);
            final String message = fields[3];
            Utilisateur utilisateur = null;
            notification = new Notification();
            utilisateur = utilisateurDAO.getByLogin(login).orElseThrow(() -> new PersistenceException("no user"));
            notification.setUtilisateur(utilisateur);
            notification.setDate(date);
            notification.setMessage(message.replaceAll(NotificationSerialization.BACK_CARRIAGE_REPLACE, NotificationSerialization.BACK_CARRIAGE));
            notification.setLevel(fields[4]);
            notification.setBody(fields[5].replaceAll(NotificationSerialization.BACK_CARRIAGE_REPLACE, NotificationSerialization.BACK_CARRIAGE));
            notification.setAttachment(fields[6]);
            notification.setArchived(Boolean.valueOf(fields[7]));
            notificationDAO.saveOrUpdate(notification);
            notificationDAO.flush();
            final String attachment = notification.getAttachment();
            if (!Strings.isNullOrEmpty(attachment) && !"null".equals(attachment)) {
                attachments.add(attachment);
            }
        } catch (DateTimeParseException | PersistenceException | ArrayIndexOutOfBoundsException ex) {
            LOGGER.error(String.format("Can't read line %s for notification", fields), ex);
        }
    }

    private void writeFile(File inZip, String attachment) {
        LOGGER.info("Start write attachment {}", attachment);
        String outFileName = String.format(PATTERN_CONCAT_FILE, configuration.getRepositoryURI(), attachment);
        try (FileOutputStream outStream = FileWithFolderCreator.createOutputStream(outFileName);
                ZipInputStream zis = new ZipInputStream(new FileInputStream(inZip));) {
            ZipEntry ze = zis.getNextEntry();
            byte[] buffer = new byte[1_024];
            while (ze != null) {
                String fileName = ze.getName();
                if (fileName.equals(attachment)) {
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        outStream.write(buffer, 0, len);
                    }
                    outStream.close();
                    break;
                }
                ze = zis.getNextEntry();
            }
            LOGGER.info("End write attachment {}", attachment);
        } catch (IOException ex) {
            LOGGER.error("file not found {}", ex.getMessage(), ex);
        }
    }
}
