package org.inra.ecoinfo;

import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.IInternationalizable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationDAO;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class MO.
 */
public abstract class MO implements IInternationalizable {

    /**
     * The Constant CSV_SEPARATOR.
     */
    protected static final String CSV_SEPARATOR = ";";
    /**
     * The ILocalizationManager localization manager.
     */
    protected ILocalizationManager localizationManager;
    /**
     * The INotificationDAO notification dao.
     */
    protected INotificationDAO notificationDAO;
    /**
     * The INotificationsManager notifications manager.
     */
    protected INotificationsManager notificationsManager;
    /**
     * The IPolicyManager security context.
     */
    protected IPolicyManager policyManager;

    /**
     * Send notification.
     *
     * @param message the String message
     * @param logLevel the String log level
     * @param bodyMessage the String body message
     * @param utilisateur the Utilisateur utilisateur
     * @return the Notification notification
     * @throws BusinessException the business exception
     */
    public Notification sendNotification(final String message, final String logLevel, final String bodyMessage, final Utilisateur utilisateur) throws BusinessException {
        final Notification notificationStub = buildNotification(logLevel, message, bodyMessage, null);
        notificationsManager.addNotification(notificationStub, utilisateur.getLogin());
        return notificationStub;
    }

    /**
     * Send notification to user.
     *
     * @param message the String message
     * @param logLevel the String log level
     * @param bodyMessage the String body message
     * @param loginUtilisateur the String login utilisateur
     * @return the Notification notification
     * @throws BusinessException the business exception
     */
    public Notification sendNotificationToUser(final String message, final String logLevel, final String bodyMessage, final String loginUtilisateur) throws BusinessException {
        final Notification notificationStub = buildNotification(logLevel, message, bodyMessage, null);
        notificationsManager.addNotification(notificationStub, loginUtilisateur);
        return notificationStub;
    }

    /**
     * Send notification with attachment.
     *
     * @param message the String message
     * @param logLevel the String log level
     * @param bodyMessage the String body message
     * @param attachmentPath the String attachment path
     * @param utilisateur the Utilisateur utilisateur
     * @return the Notification notification
     * @throws BusinessException the business exception
     */
    public Notification sendNotificationWithAttachment(final String message, final String logLevel, final String bodyMessage, final String attachmentPath, final Utilisateur utilisateur) throws BusinessException {
        final Notification notificationStub = buildNotification(logLevel, message, bodyMessage, attachmentPath);
        notificationsManager.addNotification(notificationStub, utilisateur.getLogin());
        return notificationStub;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.localization.IInternationalizable#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    /**
     *
     * @param localizationManager
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the void notification dao.
     *
     * @param notificationDAO the INotificationDAO notification dao
     */
    public void setNotificationDAO(final INotificationDAO notificationDAO) {
        this.notificationDAO = notificationDAO;
    }

    /**
     * Sets the void notifications manager.
     *
     * @param notificationsManager the INotificationsManager notifications
     * manager
     */
    public void setNotificationsManager(final INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Builds the notification.
     *
     * @param level the String level
     * @param message the String message
     * @param body the String body
     * @param attachmentPath the String attachment path
     * @return the Notification notification
     */
    protected Notification buildNotification(final String level, final String message, final String body, final String attachmentPath) {
        final Notification notification = new Notification();
        notification.setLevel(level);
        notification.setMessage(message);
        String bodyMessage = null;
        if (body != null && body.length() > 5_000) {
            bodyMessage = body.substring(0, 5_000).concat("...");
        } else {
            bodyMessage = body;
        }
        notification.setBody(bodyMessage);
        notification.setAttachment(attachmentPath);
        return notification;
    }
}
