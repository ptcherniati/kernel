package org.inra.ecoinfo.mailsender;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DefaultMailSender.
 */
public class DefaultMailSender implements IMailSender {

    /**
     * The Constant PROPERTY_CST_MAIL_DEBUG.
     */
    private static final String PROPERTY_CST_MAIL_DEBUG = "mail.debug";
    /**
     * The Constant PROPERTY_CST_MAIL_SMTP_HOST.
     */
    private static final String PROPERTY_CST_MAIL_SMTP_HOST = "mail.smtp.host";
    private static final String TOMCAT_EMAIL = "tomcat@orleans.inra.fr";
    /**
     * The ICoreConfiguration configuration.
     */
    private ICoreConfiguration configuration;
    /**
     * The Properties props.
     */
    private final Properties props = new Properties();
    /**
     * The Session session.
     */
    Session session = Session.getInstance(props);
    Logger LOGGER = LoggerFactory.getLogger(DefaultMailSender.class);

    /**
     * Instantiates a new DefaultMailSender default mail sender.
     */
    public DefaultMailSender() {
        super();
    }

    /**
     * Inits the.
     */
    public void init() {
        props.put(DefaultMailSender.PROPERTY_CST_MAIL_SMTP_HOST, configuration.getMailHost());
        props.put(DefaultMailSender.PROPERTY_CST_MAIL_DEBUG, true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.mailsender.IMailSender#sendMail(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    /**
     *
     * @param from
     * @param to
     * @param subject
     * @param message
     * @return
     */
    @Override
    public int sendMail(final String from, final String to, final String subject, final String message) {
        final Message msg = new MimeMessage(session);
        try {
            msg.setReplyTo(new Address[]{new InternetAddress(from)});
            msg.setFrom(new InternetAddress(TOMCAT_EMAIL));
            final InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            msg.setSubject(subject);
            msg.setSentDate(Date.from(Instant.now()));
            msg.setContent(message, "text/html; charset=utf-8");
            Transport.send(msg);
        } catch (final MessagingException mex) {
            LOGGER.error(String.format("%s %s->%s", mex.getMessage(), from, to), mex);
            return 1;
        }
        return 0;
    }

    /**
     *
     * @param from
     * @param to
     * @param otherRecipients
     * @param subject
     * @param message
     * @return
     */
    @Override
    public int sendMail(final String from, final String to, final List<Utilisateur> otherRecipients, final String subject, final String message) {
        final Message msg = new MimeMessage(session);
        try {
            msg.setReplyTo(new Address[]{new InternetAddress(from)});
            msg.setFrom(new InternetAddress(TOMCAT_EMAIL));
            final InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            if (otherRecipients != null && !otherRecipients.isEmpty()) {
                final InternetAddress[] addressesCC = new InternetAddress[otherRecipients.size()];
                for (int i = 0; i < otherRecipients.size(); i++) {
                    String cc = otherRecipients.get(i).getEmail();
                    addressesCC[i] = new InternetAddress(cc);
                }
                msg.addRecipients(Message.RecipientType.CC, addressesCC);
            }
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setContent(message, "text/html; charset=utf-8");
            Transport.send(msg);
        } catch (final MessagingException mex) {
            LOGGER.error(String.format("%s %s->%s", mex.getMessage(), from, to), mex);
            return 1;
        }
        return 0;
    }

    /**
     * Sets the void configuration.
     *
     * @param configuration the ICoreConfiguration configuration
     */
    public void setConfiguration(final ICoreConfiguration configuration) {
        this.configuration = configuration;
    }
}
