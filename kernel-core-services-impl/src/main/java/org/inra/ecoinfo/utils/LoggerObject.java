package org.inra.ecoinfo.utils;

import java.time.LocalDateTime;
import org.inra.ecoinfo.notifications.entity.Notification;

/**
 * The Class LoggerObject.
 */
public class LoggerObject {

    /**
     * The Constant MSG_OUTPUT_PATTERN.
     */
    private static final String MSG_OUTPUT_PATTERN = "%s;\"%s\";\"%s\"";
    /**
     * The Constant MSG_OUTPUT_PATTERN2.
     */
    private static final String MSG_OUTPUT_PATTERN2 = "Notification level : %s; message : %s; body : %s; attachement : %s";
    /**
     * The Date date.
     */
    private final LocalDateTime date;
    /**
     * The String login.
     */
    private final String login;
    /**
     * The String message.
     */
    private final String message;

    /**
     * Instantiates a new LoggerObject LOGGER object.
     *
     * @param ntfctn
     * @param notification the Notification notification
     */
    public LoggerObject(final Notification notification) {
        this.login = notification.getUtilisateur().getLogin();
        this.date = notification.getDate();
        this.message = String.format(LoggerObject.MSG_OUTPUT_PATTERN2, notification.getLevel(), notification.getMessage(), notification.getBodyForAffichage(), notification.getAttachment());
    }

    /**
     * Instantiates a new LoggerObject LOGGER object.
     *
     * @param login the String login
     * @param date the Date date
     * @param string1
     * @param message the String message
     */
    public LoggerObject(final String login, final LocalDateTime date, final String message) {
        super();
        this.login = login;
        this.date = date;
        this.message = message;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */

    /**
     *
     * @return
     */

    @Override
    public String toString() {
        String utcDateTextFromLocalDate = DateUtil.getUTCDateTextFromLocalDateTime((date == null ? LocalDateTime.now(): date), DateUtil.DD_MM_YYYY_HH_MM);
        return String.format(LoggerObject.MSG_OUTPUT_PATTERN, utcDateTextFromLocalDate, login, message);
    }
}
