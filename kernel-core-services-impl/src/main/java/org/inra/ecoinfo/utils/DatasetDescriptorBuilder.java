/**
 * OREILacs project - see LICENCE.txt for use created: 23 févr. 2009 16:11:37
 */
package org.inra.ecoinfo.utils;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.digester.Digester;
import org.xml.sax.SAXException;

/**
 * The Class DatasetDescriptorBuilder.
 */
public class DatasetDescriptorBuilder {

    /**
     * Builds the descriptor.
     *
     * @param datasetDescriptorStream the InputStream dataset descriptor stream
     * @return the DatasetDescriptor dataset descriptor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final DatasetDescriptor buildDescriptor(final InputStream datasetDescriptorStream) throws IOException {
        try {
            DatasetDescriptor datasetDescriptor = null;
            final Digester digester = new Digester();
            digester.setNamespaceAware(true);
            digester.setUseContextClassLoader(true);
            digester.addObjectCreate("dataset-descriptor", DatasetDescriptor.class);
            digester.addCallMethod("dataset-descriptor/name", "setName", 0);
            digester.addCallMethod("dataset-descriptor/undefined-column", "setUndefinedColumn", 0);
            digester.addObjectCreate("dataset-descriptor/column", Column.class);
            digester.addSetNext("dataset-descriptor/column", "addColumn");
            digester.addCallMethod("dataset-descriptor/column/name", "setName", 0);
            digester.addSetProperties("dataset-descriptor/column/name");
            digester.addCallMethod("dataset-descriptor/column/localizable", "setLocalizable", 0);            
            digester.addSetProperties("dataset-descriptor/column/localizable");
            digester.addCallMethod("dataset-descriptor/column/key", "setKey", 0);
            digester.addCallMethod("dataset-descriptor/column/variable", "setVariable", 0);
            digester.addCallMethod("dataset-descriptor/column/flag", "setFlag", 0);
            digester.addCallMethod("dataset-descriptor/column/date-for-interval", "setDateForInterval",
                                                                                  0);
            digester.addCallMethod("dataset-descriptor/column/value-type", "setValueType", 0);
            digester.addCallMethod("dataset-descriptor/column/format-type", "setFormatType", 0);
            digester.addCallMethod("dataset-descriptor/column/not-null", "setNullable", 0);
            digester.addCallMethod("dataset-descriptor/column/flag-type", "setFlagType", 0);
            digester.addCallMethod("dataset-descriptor/column/ref-variable-name", "setRefVariableName",
                                                                                  0);
            datasetDescriptor = (DatasetDescriptor) digester.parse(datasetDescriptorStream);
            StreamUtils.closeStream(datasetDescriptorStream);
            return datasetDescriptor;
        } catch (SAXException ex) {
            throw new IOException(ex);
        }
    }
    /**
     * Skip header.
     *
     * @param bufferedReader the BufferedReader buffered reader
     * @throws IOException Signals that an I/O exception has occurred.
     */
//    private static void skipHeader(final BufferedReader bufferedReader) throws IOException {
//        for (int i = 0; i < 3; i++) {
//            bufferedReader.readLine();
//        }
//    }
    /**
     * Test input stream.
     *
     * @param is the InputStream is
     * @param datasetDescriptor the DatasetDescriptor dataset descriptor
     */
//    public static final void testInputStream(final InputStream is,
//            final DatasetDescriptor datasetDescriptor) {
//        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
//        try {
//            DatasetDescriptorBuilder.skipHeader(bufferedReader);
//            final String line = bufferedReader.readLine();
//            if (line == null) {
//                return;
//            }
//            final String[] columns = line.split(";");
//            for (int i = 0; i < columns.length; i++) {
//                datasetDescriptor.getColumns().get(i).getName();
//            }
//        } catch (final IOException e) {
//            LoggerFactory.getLogger(DatasetDescriptor.class.getName()).error(e.getMessage(), e);
//        } catch (final Exception e) {
//            LoggerFactory.getLogger(DatasetDescriptor.class.getName()).error(e.getMessage(), e);
//        }
//    }
    
    /**
     *
     */
    public DatasetDescriptorBuilder() {
    }

}
