/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.security;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author tcherniatinsky
 */
public class Activity implements Comparable<Activity> {

    Activities activities;
    INode node;
    LocalDate startDate;
    LocalDate endDate;

    /**
     *
     * @param activities
     * @param node
     * @param dates
     */
    public Activity(Activities activities, INode node, List<LocalDate> dates) {
        this.activities = activities;
        this.node = node;
        this.startDate = dates.get(0);
        this.endDate = dates.get(1);
    }

    /**
     *
     * @return
     */
    public String getStartDate() {
        return startDate == null ? "" : DateUtil.getUTCDateTextFromLocalDateTime(startDate.atStartOfDay(), DateUtil.DD_MM_YYYY);
    }

    /**
     *
     * @return
     */
    public String getEndDate() {
        return endDate == null ? "" : DateUtil.getUTCDateTextFromLocalDateTime(endDate.atStartOfDay(), DateUtil.DD_MM_YYYY);
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.activities);
        hash = 19 * hash + Objects.hashCode(this.node.getPath());
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Activity other = (Activity) obj;
        if (this.activities != other.activities) {
            return false;
        }
        return Objects.equals(this.node.getPath(), other.node.getPath());
    }

    /**
     *
     * @return
     */
    public Activities getActivities() {
        return activities;
    }

    /**
     *
     * @return
     */
    public INode getNode() {
        return node;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Activity o) {
        if (equals(o)) {
            return 0;
        }
        if (activities.compareTo(o.activities) == 0) {
            return node.getPath().compareTo(o.node.getPath());
        } else {
            return activities.compareTo(o.activities);
        }
    }

}
