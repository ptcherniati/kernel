package org.inra.ecoinfo;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;

/**
 * The Class Messages.
 */
public final class Messages {

    /**
     * The Constant BUNDLE_NAME.
     */
    private static final String BUNDLE_NAME = "org.inra.ecoinfo.messages";
    /**
     * The Constant RESOURCE_BUNDLE.
     */
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
            .getBundle(Messages.BUNDLE_NAME);

    /**
     * Gets the String string.
     *
     * @param key the String key
     * @return the String string
     */
    public static String getString(final String key) {
        try {
            return Messages.RESOURCE_BUNDLE.getString(key);
        } catch (final MissingResourceException e) {
            UncatchedExceptionLogger.log("no bundle for key", e);
            return '!' + key + '!';
        }
    }

    /**
     * Instantiates a new Messages messages.
     */
    private Messages() {
    }
}
