package org.inra.ecoinfo.serialization;

/**
 * The Interface IIdSerialization.
 */
public interface INotificationSerialization {

    /**
     *
     * @return
     */
    String getId();
}
