package org.inra.ecoinfo.serialization.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.serialization.AbstractSerialization;
import org.inra.ecoinfo.serialization.IUtilisateurSerialization;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MailAdminSerialization.
 */
public class UtilisateurSerialization extends AbstractSerialization implements IUtilisateurSerialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/utilisateurSerialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "utilisateurSerialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(UtilisateurSerialization.class.getName());
    private static final String UTILISATEURS_ENTRY = "utilisateur.csv";
    private static final String GROUP_ENTRY = "group.csv";

    /**
     *
     */
    public static final String HEADER_USERS_CSV_FILE = "id;active;email;emploi;isroot;language;login;nom;password;poste;prenom";

    /**
     *
     */
    public static final String HEADER_GROUPS_CSV_FILE = "id;activityasstream;group_name;group_which_tree";

    /**
     *
     */
    public static final String LINE_USER_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    /**
     *
     */
    public static final String LINE_GROUP_CSV_FILE = "%n%s;%s;%s;%s";
    private String id;
    /**
     * The mail admin.
     */
    private IPolicyManager policyManager;

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#createConfig(org.apache.commons.digester.Digester)
     */
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("repositoryURI", "setRepositoryURI", 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#getModuleName()
     */
    /**
     *
     * @return
     */
    @Override
    public String getModuleName() {
        return UtilisateurSerialization.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.AbstractSerialization#getSchemaPath()
     */
    /**
     *
     * @return
     */
    @Override
    public String getSchemaPath() {
        return UtilisateurSerialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#isAutoLoad()
     */
    /**
     *
     * @return
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param outZip
     * @throws BusinessException
     */
    @Override
    public void serialize(ZipOutputStream outZip) throws BusinessException {
        List<Utilisateur> utilisateurs = null;
        try {
            List<ICompositeGroup> userOrGroups = policyManager.retrieveAllUserOrGroups(AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS);
            userOrGroups.addAll(policyManager.retrieveAllUserOrGroups(AbstractMgaIOConfigurator.REFDATA_CONFIGURATION_RIGHTS));
            writeLineEntry(userOrGroups, outZip);
        } catch (IOException ex) {
            LOGGER.error("can't write utilisateur ", ex);
        }
    }

    /**
     *
     * @param usersOrGroups
     * @param outZip
     * @throws IOException
     */
    public void writeLineEntry(List<ICompositeGroup> usersOrGroups, ZipOutputStream outZip) throws IOException {
        Map<String, Utilisateur> users = new HashMap();
        Map<String, Group> groups = new HashMap();
        usersOrGroups.stream().forEach(userOrGroup -> {
            if (userOrGroup instanceof Utilisateur) {
                Utilisateur user = (Utilisateur) userOrGroup;
                users.put(user.getLogin(), user);
            } else if (userOrGroup instanceof Group) {
                Group group = (Group) userOrGroup;
                groups.put(group.getGroupName(), group);
            }
        });
        openEntry(outZip, UTILISATEURS_ENTRY, HEADER_USERS_CSV_FILE);
        users.values().stream()
                .forEach(user -> writeUserLineEntry(user, outZip));
        outZip.closeEntry();
        openEntry(outZip, GROUP_ENTRY, HEADER_GROUPS_CSV_FILE);
        groups.values().stream()
                .forEach(group -> writeGroupLineEntry(group, outZip));
        outZip.closeEntry();
    }

    /**
     *
     * @param utilisateur
     * @param outZip
     */
    public void writeUserLineEntry(Utilisateur utilisateur, ZipOutputStream outZip) {
        String line = String.format(LINE_USER_CSV_FILE,
                utilisateur.getId(),
                utilisateur.getActive(),
                utilisateur.getEmail(),
                utilisateur.getEmploi(),
                utilisateur.getIsRoot(),
                utilisateur.getLanguage(),
                utilisateur.getLogin(),
                utilisateur.getNom(),
                utilisateur.getPassword(),
                utilisateur.getPoste(),
                utilisateur.getPrenom(),
                utilisateur.getAllGroups().stream()
                        .map(g -> String.format("%s|%s",g.getGroupName(), g.getWhichTree().name()))
                        .distinct()
                        .collect(Collectors.joining(","))
        );
        try {
            outZip.write(line.getBytes(StandardCharsets.UTF_8));
        } catch (IOException ex) {
            LOGGER.error(String.format("can't write line for user %", utilisateur.getLogin()));
        }
    }

    /**
     *
     * @param group
     * @param outZip
     */
    public void writeGroupLineEntry(Group group, ZipOutputStream outZip) {
        String line = String.format(LINE_GROUP_CSV_FILE,
                group.getId(),
                group.getActivityAsString(),
                group.getGroupName(),
                group.getWhichTree()
        );
        try {
            outZip.write(line.getBytes(StandardCharsets.UTF_8));
        } catch (IOException ex) {
            LOGGER.error(String.format("can't write line for group %", group.getGroupName()));
        }
    }

    /**
     *
     * @param outZip
     * @param header
     * @throws IOException
     */
    public void openEntry(ZipOutputStream outZip, String entryName, String header) throws IOException {
        ZipEntry utilisateursFile = new ZipEntry(entryName);
        outZip.putNextEntry(utilisateursFile);
        outZip.write(header.getBytes(StandardCharsets.UTF_8));
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }
}
