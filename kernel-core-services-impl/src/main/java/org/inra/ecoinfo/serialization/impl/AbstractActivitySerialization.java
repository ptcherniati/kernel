package org.inra.ecoinfo.serialization.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.serialization.AbstractSerialization;
import org.inra.ecoinfo.serialization.IUtilisateurSerialization;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MailAdminSerialization.
 */
public abstract class AbstractActivitySerialization extends AbstractSerialization implements IUtilisateurSerialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/activitySerialization.xsd";

    /**
     *
     */
    public static final String HEADER_CSV_FILE = "login;resource_path;role;activity_type";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s";

    /**
     *
     */
    public static final String LINE_PROPERTY_CSV_FILE = ";%s;%s";

    /**
     * The Constant MODULE_NAME.
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractActivitySerialization.class.getName());
    private String id;
    /**
     * The mail admin.
     */
    private IPolicyManager policyManager;

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param policyManager
     */
    public void setActivityDAO(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#createConfig(org.apache.commons.digester.Digester)
     */
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        super.createConfig(digester);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.AbstractSerialization#getSchemaPath()
     */
    /**
     *
     * @return
     */
    @Override
    public String getSchemaPath() {
        return AbstractActivitySerialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#isAutoLoad()
     */
    /**
     *
     * @return
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param outZip
     * @throws BusinessException
     */
    @Override
    public void serialize(ZipOutputStream outZip) throws BusinessException {
        try {
            List<ICompositeGroup> usersOrGroups = policyManager.retrieveAllUserOrGroups(getConfigurationNumber());
            openEntry(outZip);
            usersOrGroups.forEach(userOrGroup -> writeLinesEntry(userOrGroup, outZip));
            outZip.closeEntry();
        } catch (IOException ex) {
            LOGGER.error("cant'write activities", ex);
        }
    }

    /**
     *
     * @param userOrGroup
     * @param user
     * @param outZip
     */
    public void writeLinesEntry(ICompositeGroup userOrGroup, ZipOutputStream outZip) {
        policyManager.getMgaServiceBuilder().getMgaIOConfigurator().getConfiguration(getConfigurationNumber())
                .map(conf -> Arrays.asList(conf.getActivities()))
                .orElseGet(LinkedList::new).stream()
                .forEach(
                        activity -> policyManager.getOrLoadTree(userOrGroup, getConfigurationNumber()).getChildren()
                                .forEach(
                                        node -> writeLinesEntry(
                                                userOrGroup,
                                                getWhichTree(),
                                                node,
                                                activity,
                                                outZip
                                        )
                                )
                );
    }

    /**
     *
     * @return
     */
    public abstract int getConfigurationNumber();

    /**
     *
     * @return
     */
    public abstract WhichTree getWhichTree();

    private void writeLinesEntry(ICompositeGroup userOrGroup, WhichTree role, ITreeNode<IFlatNode> tree, Activities activity, ZipOutputStream outZip) {
        IFlatNode flatNode = tree.getData();
        if (flatNode != null) {
            Activities activityToRemove = null;
            switch (flatNode.getRealState(activity)) {
                case 0:
                    return;
                case 1:
                    try {
                        writeLineEntry(userOrGroup, role, flatNode, activity, outZip);
                        return;
                    } catch (IOException ex) {
                        LOGGER.error("can't write activity line", new BusinessException(ex));
                    }
                    break;
                case 2:
                    tree.getChildren().forEach(child -> writeLinesEntry(userOrGroup, role, child, activity, outZip));
                    return;
                default:
            }
        }
    }

    /**
     *
     * @param userOrGroup
     * @param role
     * @param flatNode
     * @param activity
     * @param outZip
     * @throws IOException
     */
    public void writeLineEntry(ICompositeGroup userOrGroup, WhichTree role, IFlatNode flatNode, Activities activity, ZipOutputStream outZip) throws IOException {
        //"login;resource_path;role;activity_type;debut;fin"
        String line = String.format(LINE_CSV_FILE,
                userOrGroup.getGroupName(),
                flatNode.getPath(),
                activity.name(),
                role.name()
        );
        outZip.write(line.getBytes(StandardCharsets.UTF_8));
        final String startDate = flatNode.dateAsString(activity, 0);
        if (startDate != null) {
            outZip.write(String.format(LINE_PROPERTY_CSV_FILE, 0, startDate).getBytes(StandardCharsets.UTF_8));
        }
        final String endDate = flatNode.dateAsString(activity, 1);
        if (endDate != null) {
            outZip.write(String.format(LINE_PROPERTY_CSV_FILE, 1, endDate).getBytes(StandardCharsets.UTF_8));
        }
    }

    /**
     *
     * @param outZip
     * @throws IOException
     */
    public void openEntry(ZipOutputStream outZip) throws IOException {
        ZipEntry utilisateursFile = new ZipEntry(getActivityEntry());
        outZip.putNextEntry(utilisateursFile);
        outZip.write(HEADER_CSV_FILE.getBytes(StandardCharsets.UTF_8));
    }

    /**
     *
     * @return
     */
    public abstract String getActivityEntry();
}
