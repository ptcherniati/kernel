package org.inra.ecoinfo.serialization;

/**
 * The Interface IIdSerialization.
 */
public interface IUtilisateurSerialization {

    /**
     *
     * @return
     */
    String getId();
}
