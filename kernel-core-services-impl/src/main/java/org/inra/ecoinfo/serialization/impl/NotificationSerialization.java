package org.inra.ecoinfo.serialization.impl;

import com.google.common.base.Strings;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.digester.Digester;
import org.apache.commons.lang.StringEscapeUtils;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.notifications.INotificationDAO;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.serialization.AbstractSerialization;
import org.inra.ecoinfo.serialization.INotificationSerialization;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MailAdminSerialization.
 */
public class NotificationSerialization extends AbstractSerialization implements INotificationSerialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/notificationSerialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "notificationSerialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(NotificationSerialization.class.getName());
    private static final String NOTIFICATIONS_ENTRY = "notification.csv";

    /**
     *
     */
    public static final String HEADER_CSV_FILE = "id;login;date;message;level;body;attachment;isarchived";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s";

    /**
     *
     */
    public static final String ATTACHMENT_PATTERN = "attachments/%s";

    /**
     *
     */
    public static final String ATTACHMENT_PATTERN_IN_TMP = "%s/%s";

    /**
     *
     */
    public static final String BACK_CARRIAGE_REPLACE = "@@@n@@@";

    /**
     *
     */
    public static final String BACK_CARRIAGE = "\n";
    private String id;
    /**
     * The mail admin.
     */
    private INotificationDAO notificationDAO;
    private ICoreConfiguration configuration;

    /**
     *
     * @param configuration
     */
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param notificationDAO
     */
    public void setNotificationDAO(INotificationDAO notificationDAO) {
        this.notificationDAO = notificationDAO;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#createConfig(org.apache.commons.digester.Digester)
     */
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("repositoryURI", "setRepositoryURI", 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#getModuleName()
     */
    /**
     *
     * @return
     */
    @Override
    public String getModuleName() {
        return NotificationSerialization.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.AbstractSerialization#getSchemaPath()
     */
    /**
     *
     * @return
     */
    @Override
    public String getSchemaPath() {
        return NotificationSerialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleSerialization#isAutoLoad()
     */
    /**
     *
     * @return
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param outZip
     * @throws BusinessException
     */
    @Override
    public void serialize(ZipOutputStream outZip) throws BusinessException {
        List<Notification> notifications = null;
        List<String> lines = new LinkedList();
        try {
            notifications = notificationDAO.getAll(Notification.class);
            for (Notification notification : notifications) {
                lines.add(readLine(outZip, notification));
            }
            writeLines(outZip, lines);
        } catch (IOException ex) {
            LOGGER.error("can't write notification ", ex);
        } catch (DateTimeParseException ex) {
            LOGGER.error("can't parse date ", ex);
        }
    }

    /**
     *
     * @param outZip
     * @param lines
     * @throws IOException
     */
    public void writeLines(ZipOutputStream outZip, List<String> lines) throws IOException {
        openEntry(outZip);
        for (String line : lines) {
            outZip.write(line.getBytes(StandardCharsets.UTF_8));
        }
        outZip.closeEntry();
    }

    /**
     *
     * @param outZip
     * @param notification
     * @return
     * @throws IOException
     */
    public String readLine(ZipOutputStream outZip, Notification notification) throws IOException, DateTimeParseException {
        final LocalDateTime date = notification.getDate();
        String formatDate = DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY_HH_MM_SS);
        String line = String.format(LINE_CSV_FILE,
                notification.getId(),
                notification.getUtilisateur().getLogin(),
                formatDate,
                StringEscapeUtils.escapeCsv(notification.getMessage()),
                notification.getLevel(),
                StringEscapeUtils.escapeCsv(notification.getBody()),
                notification.getAttachment(),
                notification.isArchived());
        if (!Strings.isNullOrEmpty(notification.getAttachment())) {
            File file = new File(String.format(ATTACHMENT_PATTERN_IN_TMP, configuration.getRepositoryURI(), notification.getAttachment()));
            if (file.exists()) {
                registerAttachment(outZip, notification.getAttachment(), file);
            } else {
                LOGGER.error(String.format("missing attachment file %s", file.getAbsolutePath()));
            }
        }
        return line;
    }

    /**
     *
     * @param outZip
     * @throws IOException
     */
    public void openEntry(ZipOutputStream outZip) throws IOException {
        ZipEntry notificationsFile = new ZipEntry(NOTIFICATIONS_ENTRY);
        outZip.putNextEntry(notificationsFile);
        outZip.write(HEADER_CSV_FILE.getBytes(StandardCharsets.UTF_8));
    }

    private void registerAttachment(ZipOutputStream outZip, String attachment, File file) throws IOException {
        ZipEntry attachmentFile = new ZipEntry(String.format(ATTACHMENT_PATTERN, attachment));
        outZip.putNextEntry(attachmentFile);
        try (FileInputStream fis = new FileInputStream(file);) {
            byte[] bytes = new byte[1_024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                outZip.write(bytes, 0, length);
            }
        }

        outZip.closeEntry();
    }
}
