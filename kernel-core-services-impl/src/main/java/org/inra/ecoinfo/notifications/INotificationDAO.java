package org.inra.ecoinfo.notifications;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.notifications.entity.Notification;

/**
 * The Interface INotificationDAO.
 */
public interface INotificationDAO extends IDAO<Notification> {

    /**
     * Archive notification.
     *
     * @param id the long id
     */
    void archiveNotification(long id);

    /**
     * Gets the List<Notification> all archived notifications by user login.
     *
     * @param login the String login
     * @return the List<Notification> all archived notifications by user login
     */
    List<Notification> getAllArchivedNotificationsByUserLogin(String login);

    /**
     * Gets the List<Notification> all by user login.
     *
     * @param userLogin the String user login
     * @return the List<Notification> all by user login
     */
    List<Notification> getAllByUserLogin(String userLogin);

    /**
     * Gets the List<Notification> all no archived by user login.
     *
     * @param userLogin the String user login
     * @return the List<Notification> all no archived by user login
     */
    List<Notification> getAllNoArchivedByUserLogin(String userLogin);

    /**
     * Gets the List<Notification> all sorted by date.
     *
     * @return the List<Notification> all sorted by date
     */
    List<Notification> getAllSortedByDate();

    /**
     * Gets the List<Notification> all transients sorted by date.
     *
     * @param date the Date date
     * @return the List<Notification> all transients sorted by date
     */
    List<Notification> getAllTransientsSortedByDate(LocalDateTime date);

    /**
     * Gets the Notification by n key.
     *
     * @param login the String login
     * @param date the Date date
     * @param message the String message
     * @return the Notification by n key
     */
    Optional<Notification> getByNKey(String login, LocalDateTime date, String message);

    /**
     * Gets the List<Notification> transients by user login.
     *
     * @param login the String login
     * @param date the Date date
     * @return the List<Notification> transients by user login
     */
    List<Notification> getTransientsByUserLogin(String login, LocalDateTime date);

    /**
     * Mark all as read by utilisateur id.
     *
     * @param utilisateurId the Long utilisateur id
     */
    void markAllAsReadByUtilisateurId(Long utilisateurId);

    /**
     * Mark as new.
     *
     * @param id the Long id
     */
    void markAsNew(Long id);

    /**
     * Removes the all notifications by utilisateur id.
     *
     * @param utilisateurId the Long utilisateur id
     */
    void removeAllNotificationsByUtilisateurId(Long utilisateurId);

}
