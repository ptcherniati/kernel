package org.inra.ecoinfo.notifications.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import javax.persistence.Transient;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.identification.IUsersDeletion;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.INotificationDAO;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.DeleteUserException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class LocalNotificationManager.
 */
public class LocalNotificationManager extends MO implements IUsersDeletion, INotificationsManager {

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.notifications.messages";
    
    /**
     * The Constant PROPERTY_MSG_FOR_SUCCESS_NOTIFICATION_DELETION @link(String).
     */
    protected static final String PROPERTY_MSG_FOR_SUCCESS_NOTIFICATION_DELETION = "PROPERTY_MSG_FOR_SUCCESS_NOTIFICATION_DELETION";
    
    /**
     * The Constant PROPERTY_MSG_FOR_ECHEC_NOTIFICATION_DELETION @link(String).
     */
    protected static final String PROPERTY_MSG_FOR_ECHEC_NOTIFICATION_DELETION = "PROPERTY_MSG_FOR_ECHEC_NOTIFICATION_DELETION";
    
    /**
     * The Constant FOOT.
     */
    private static final String FOOT = "\n</body>\n" + "</html>";
    /**
     * The Constant HEADER.
     */
    private static final String HEADER = "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" + "<head>\n" + "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />\n" + "<style type=\"text/css\">\n"
            + "p {font-size:17px;font-family: Arial, Verdana, sans-serif;}\n" + "</style>\n" + "</head>\n" + "" + "<body>\n";
    /**
     * The Constant MSG_DEBUG_TRANSIENT_NOTIFICATIONS_RETRIEVED.
     */
    private static final String MSG_DEBUG_TRANSIENT_NOTIFICATIONS_RETRIEVED = "MSG_DEBUG_TRANSIENT_NOTIFICATIONS_RETRIEVED";
    /**
     * The Constant MSG_LOGIN_NOT_PRESENT_IN_DATABASE.
     */
    private static final String MSG_LOGIN_NOT_PRESENT_IN_DATABASE = "MSG_LOGIN_NOT_PRESENT_IN_DATABASE";
    /**
     * The Logger LOGGER.
     */
    @Transient
    protected static final Logger LOGGER = LoggerFactory.getLogger(LocalNotificationManager.class.getName());
    /**
     * The ICoreConfiguration configuration.
     */
    protected ICoreConfiguration configuration;
    /**
     * The IUtilisateurDAO utilisateur dao.
     */
    protected IUtilisateurDAO utilisateurDAO;

    /*
     * A ACTIVER POUR LA MISE EN PLACE DE JMS
     * 
     * private ActiveMQTopic topicMessage; private ConnectionFactory connectionFactoryMessage;
     */
 /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#addNotification(org.inra.ecoinfo.notifications.entity.Notification, java.lang.String, boolean)
     */
    /**
     *
     * @param notification
     * @param userLogin
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    @Override
    public void addNotification(final Notification notification, final String userLogin) throws BusinessException {
        try {
            final Utilisateur utilisateur = utilisateurDAO.getByLogin(userLogin).orElseThrow(() -> new PersistenceException("no user"));
            if (utilisateur == null) {
                final String message = String.format(localizationManager.getMessage(LocalNotificationManager.BUNDLE_SOURCE_PATH, LocalNotificationManager.MSG_LOGIN_NOT_PRESENT_IN_DATABASE), userLogin);
                LOGGER.error(message);
                throw new BusinessException(message);
            }
            switch (notification.getLevel()) {
                case Notification.ERROR:
                    LOGGER.error(notification.getMessage());
                    break;
                case Notification.WARN:
                    LOGGER.warn(notification.getMessage());
                    break;
                default:
                    LOGGER.info(notification.getMessage());
                    break;
            }
            notification.setUtilisateur(utilisateur);
            notificationDAO.saveOrUpdate(notification);
            /*
             * A ACTIVER POUR LA MISE EN PLACE DE JMS
             * 
             * Connection connection = connectionFactoryMessage.createConnection(); Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); MessageProducer producer = session.createProducer(topicMessage); ObjectMessage
             * objectMessage = session.createObjectMessage(); NotificationVO notificationVO = new NotificationVO(notification); objectMessage.setObject(notificationVO); producer.send(objectMessage, Message.DEFAULT_DELIVERY_MODE,Message
             * .DEFAULT_PRIORITY,5*60*1000);
             */
        } catch (final PersistenceException e) {
            throw new BusinessException("can't push message", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#getAllNoArchivedByUserLogin(java.lang.String)
     */
    /**
     *
     * @param userLogin
     * @return
     */
    @Override
    public List<Notification> getAllNoArchivedByUserLogin(final String userLogin) {
        return notificationDAO.getAllNoArchivedByUserLogin(userLogin);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#getAllNotifications()
     */
    /**
     *
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<Notification> getAllNotifications() {
        return notificationDAO.getAllSortedByDate();
    }

    /*
    * (non-Javadoc)
    * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#getAllTransientsNotifications(java.time.LocalDateTime)
     */
    /**
     *
     * @param date
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<Notification> getAllTransientsNotifications(final LocalDateTime date) {
        return Optional.ofNullable(date)
                .map(d -> notificationDAO.getAllTransientsSortedByDate(d))
                .orElseThrow(() -> new IllegalArgumentException("missing date"));
    }

    /*
    * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#getAllUserNotifications(java.lang.String)
     */
    /**
     *
     * @param userLogin
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<Notification> getAllUserNotifications(final String userLogin) {
        return notificationDAO.getAllByUserLogin(userLogin);
    }

    /*
    * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#getArchivedNotifications(java.lang.String)
     */
    /**
     *
     * @param login
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<Notification> getArchivedNotifications(final String login) {
        return notificationDAO.getAllArchivedNotificationsByUserLogin(login);
    }

    /*
    * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#getAttachment(java.lang.Long)
     */
    /**
     *
     * @param notificationId
     * @return
     */
    @Override
    public byte[] getAttachment(final Long notificationId) {
        return notificationDAO.getById(Notification.class, notificationId)
                .map(n -> n.getAttachment())
                .map(attachment -> configuration.getRepositoryURI().concat(Utils.SEPARATOR_URL).concat(attachment))
                .map(filePath -> new File(filePath))
                .map(file->buildBytesArrayAttachment(file))
                .orElseGet(() -> new byte[0]);
    }

    /*
    * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#getBody(java.lang.Long)
     */
    /**
     *
     * @param notificationId
     * @return
     */
    @Override
    public byte[] getBody(final Long notificationId) {
        return notificationDAO.getById(Notification.class, notificationId)
                .map(notification -> String.format("%s%s%s", LocalNotificationManager.HEADER, notification.getBody(), LocalNotificationManager.FOOT))
                .map(body -> body.getBytes(StandardCharsets.UTF_8))
                .orElseGet(() -> new byte[0]);
    }

    /*
    * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#getNotificationById(java.lang.Long)
     */
    /**
     *
     * @param id
     * @return
     */
    @Override
    public Optional<Notification> getNotificationById(final Long id) {
        return notificationDAO.getById(Notification.class, id);
    }

    /*
    * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#getTransientsUserNotifications(java.lang.String, java.time.LocalDateTime)
     */
    /**
     *
     * @param login
     * @param date
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<Notification> getTransientsUserNotifications(final String login, final LocalDateTime date) {
        if (login == null || date == null) {
            throw new IllegalArgumentException("login or date null");
        }
        List<Notification> notifications = notificationDAO.getTransientsByUserLogin(login, date);
        if (!notifications.isEmpty()) {
            LOGGER.debug(String.format(localizationManager.getMessage(LocalNotificationManager.BUNDLE_SOURCE_PATH, LocalNotificationManager.MSG_DEBUG_TRANSIENT_NOTIFICATIONS_RETRIEVED), notifications.size(), login, date.toString()));
        }
        return notifications;
    }

    /*
    * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#markAllAsRead(java.lang.Long)
     */
    /**
     *
     * @param utilisateurId
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void markAllAsRead(final Long utilisateurId) {
        notificationDAO.markAllAsReadByUtilisateurId(utilisateurId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#markNotificationAsArchived(long)
     */
    /**
     *
     * @param id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void markNotificationAsArchived(final long id) {
        notificationDAO.archiveNotification(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#markNotificationAsNew(java.lang.Long)
     */
    /**
     *
     * @param id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void markNotificationAsNew(final Long id) {
        notificationDAO.markAsNew(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#removeAllNotification(java.lang.Long)
     */
    /**
     *
     * @param utilisateurId
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeAllNotification(final Long utilisateurId) {
        notificationDAO.removeAllNotificationsByUtilisateurId(utilisateurId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationsManager#removeNotification(long)
     */
    /**
     *
     * @param id
     * @throws BusinessException
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeNotification(final long id) throws BusinessException {
        try {
            Optional<Notification> notificationById = getNotificationById(id);
            if (!notificationById.isPresent()) {
                LOGGER.info("no ");
                return; 
            }
            notificationDAO.remove(notificationById.get());
        } catch (final PersistenceException e) {
            throw new BusinessException("can't remove notification", e);
        }
    }

    /**
     * Sets the void configuration.
     *
     * @param configuration the ICoreConfiguration configuration
     */
    public void setConfiguration(final ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.MO#setNotificationDAO(org.inra.ecoinfo.notifications.INotificationDAO)
     */
    /**
     *
     * @param notificationDAO
     */
    @Override
    public void setNotificationDAO(final INotificationDAO notificationDAO) {
        this.notificationDAO = notificationDAO;
    }

    /**
     * Sets the void utilisateur dao.
     *
     * @param utilisateurDAO the IUtilisateurDAO utilisateur dao
     */
    public void setUtilisateurDAO(final IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     * Builds the bytes array attachment.
     *
     * @param file the File file
     * @return the byte[] byte[]
     */
    protected byte[] buildBytesArrayAttachment(final File file) {
        final byte[] datas = new byte[(int) file.length()];
        try (FileInputStream fis = new FileInputStream(file)) {
            fis.read(datas, 0, (int) file.length());
        } catch (final IOException e) {
            LOGGER.error("can't read attachement", e);
            return new byte[0];
        }
        return datas;
    }

    /**
     *
     * @param utilisateur
     * @return
     * @throws DeleteUserException
     */
    @Override
    public InfosReport deleteDependancesUserProfile(Utilisateur utilisateur) throws DeleteUserException {
        InfosReport notificationsReport = new InfosReport("Notifications_deletion");
        String reportMessage;
        notificationDAO.removeAllNotificationsByUtilisateurId(utilisateur.getId());
        
        if(notificationDAO.getAllByUserLogin(utilisateur.getLogin()).isEmpty()){
            reportMessage = String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_FOR_SUCCESS_NOTIFICATION_DELETION), utilisateur.getLogin());
            notificationsReport.addInfos("Notifications_deletion", reportMessage);
        }
        else{
            reportMessage = String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_FOR_ECHEC_NOTIFICATION_DELETION), utilisateur.getLogin());
            throw new DeleteUserException(reportMessage);
        }
        
        return notificationsReport;
    }
}
