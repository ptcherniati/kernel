package org.inra.ecoinfo.notifications.jpa;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.identification.entity.Utilisateur_;
import org.inra.ecoinfo.notifications.INotificationDAO;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.notifications.entity.Notification_;

/**
 * The Class JPANotificationDAO.
 */
public class JPANotificationDAO extends AbstractJPADAO<Notification> implements INotificationDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationDAO#archiveNotification(long)
     */
    /**
     *
     * @param id
     */
    @Override
    public void archiveNotification(final long id) {
        CriteriaUpdate<Notification> update = builder.createCriteriaUpdate(Notification.class);
        Root<Notification> from = update.from(Notification.class);
        update.set(from.get(Notification_.archived), true);
        update.where(builder.equal(from.get(Notification_.id), id));
        update(update);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationDAO#getAllArchivedNotificationsByUserLogin(java.lang.String)
     */
    /**
     *
     * @param login
     * @return
     */
    @Override
    public List<Notification> getAllArchivedNotificationsByUserLogin(final String login) {
        CriteriaQuery<Notification> query = builder.createQuery(Notification.class);
        Root<Notification> from = query.from(Notification.class);
        List<Predicate> and = new LinkedList();
        and.add(builder.equal(from.join(Notification_.utilisateur).get(Utilisateur_.login), login));
        and.add(builder.equal(from.get(Notification_.archived), true));
        query.where(and.toArray(new Predicate[and.size()]));
        query.orderBy(builder.desc(from.get(Notification_.date)));
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationDAO#getAllByUserLogin(java.lang.String)
     */
    /**
     *
     * @param userLogin
     * @return
     */
    @Override
    public List<Notification> getAllByUserLogin(final String userLogin) {
        CriteriaQuery<Notification> query = builder.createQuery(Notification.class);
        Root<Notification> notification = query.from(Notification.class);
        List<Predicate> and = new LinkedList();
        and.add(builder.equal(notification.join(Notification_.utilisateur).get(Utilisateur_.login), userLogin));
        query
                .select(notification)
                .where(and.toArray(new Predicate[and.size()]))
                .orderBy(builder.desc(notification.get(Notification_.date)));
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationDAO#getAllNoArchivedByUserLogin(java.lang.String)
     */
    /**
     *
     * @param userLogin
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Notification> getAllNoArchivedByUserLogin(final String userLogin) {
        CriteriaQuery<Notification> query = builder.createQuery(Notification.class);
        Root<Notification> notification = query.from(Notification.class);
        List<Predicate> and = new LinkedList();
        and.add(builder.equal(notification.join(Notification_.utilisateur).get(Utilisateur_.login), userLogin));
        and.add(builder.equal(notification.get(Notification_.archived), false));
        query
                .select(notification)
                .where(and.toArray(new Predicate[and.size()]))
                .orderBy(builder.desc(notification.get(Notification_.date)));
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationDAO#getAllSortedByDate()
     */
    /**
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Notification> getAllSortedByDate() {
        CriteriaQuery<Notification> query = builder.createQuery(Notification.class);
        Root<Notification> notification = query.from(Notification.class);
        List<Predicate> and = new LinkedList();
        and.add(builder.equal(notification.get(Notification_.archived), false));
        query
                .select(notification)
                .where(and.toArray(new Predicate[and.size()]))
                .orderBy(builder.desc(notification.get(Notification_.date)));
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationDAO#getAllTransientsSortedByDate(java.time.LocalDateTime)
     */
    /**
     *
     * @param date
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Notification> getAllTransientsSortedByDate(final LocalDateTime date) {
        CriteriaQuery<Notification> query = builder.createQuery(Notification.class);
        Root<Notification> notification = query.from(Notification.class);
        List<Predicate> and = new LinkedList();
        and.add(builder.greaterThan(notification.<LocalDateTime>get(Notification_.date), date));
        and.add(builder.equal(notification.get(Notification_.archived), false));
        query
                .select(notification)
                .where(and.toArray(new Predicate[and.size()]))
                .orderBy(builder.desc(notification.get(Notification_.date)));
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationDAO#getByNKey(java.lang.String, java.time.LocalDateTime, java.lang.String)
     */
    /**
     *
     * @param login
     * @param date
     * @param message
     * @return
     */
    @Override
    public Optional<Notification> getByNKey(final String login, final LocalDateTime date, final String message) {
        CriteriaQuery<Notification> query = builder.createQuery(Notification.class);
        Root<Notification> notification = query.from(Notification.class);
        List<Predicate> and = new LinkedList();
        and.add(builder.equal(notification.join(Notification_.utilisateur).get(Utilisateur_.login), login));
        and.add(builder.equal(notification.<LocalDateTime>get(Notification_.date), date));
        and.add(builder.equal(notification.get(Notification_.message), message));
        query
                .select(notification)
                .where(and.toArray(new Predicate[and.size()]));
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationDAO#getTransientsByUserLogin(java.lang.String, java.time.LocalDateTime)
     */
    /**
     *
     * @param login
     * @param date
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Notification> getTransientsByUserLogin(final String login, final LocalDateTime date) {
        CriteriaQuery<Notification> query = builder.createQuery(Notification.class);
        Root<Notification> from = query.from(Notification.class);
        List<Predicate> and = new LinkedList();
        and.add(builder.equal(from.join(Notification_.utilisateur).get(Utilisateur_.login), login));
        and.add(builder.greaterThan(from.<LocalDateTime>get(Notification_.date), date));
        and.add(builder.equal(from.get(Notification_.archived), false));
        query.where(and.toArray(new Predicate[and.size()]));
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationDAO#markAllAsReadByUtilisateurId(java.lang.Long)
     */
    /**
     *
     * @param utilisateurId
     */
    @Override
    public void markAllAsReadByUtilisateurId(final Long utilisateurId) {
        CriteriaUpdate<Notification> update = builder.createCriteriaUpdate(Notification.class);
        Root<Notification> from = update.from(Notification.class);
        update.set(from.get(Notification_.archived), true);
        update.where(builder.equal(from.get(Notification_.utilisateur).get(Utilisateur_.id), utilisateurId));
        update(update);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationDAO#markAsNew(java.lang.Long)
     */
    /**
     *
     * @param id
     */
    @Override
    public void markAsNew(final Long id) {
        CriteriaUpdate<Notification> update = builder.createCriteriaUpdate(Notification.class);
        Root<Notification> from = update.from(Notification.class);
        update.set(from.get(Notification_.archived), false);
        update.where(builder.equal(from.get(Notification_.id), id));
        update(update);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.notifications.INotificationDAO#removeAllNotificationsByUtilisateurId(java.lang.Long)
     */
    /**
     *
     * @param utilisateurId
     */
    @Override
    public void removeAllNotificationsByUtilisateurId(final Long utilisateurId) {
        CriteriaDelete<Notification> delete = builder.createCriteriaDelete(Notification.class);
        Root<Notification> from = delete.from(Notification.class);
        delete.where(builder.equal(from.get(Notification_.utilisateur).get(Utilisateur_.id), utilisateurId));
        delete(delete);
    }
}
