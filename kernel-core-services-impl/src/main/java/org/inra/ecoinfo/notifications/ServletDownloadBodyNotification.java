package org.inra.ecoinfo.notifications;

import com.google.common.base.Strings;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.inra.ecoinfo.utils.StreamUtils;
import org.inra.ecoinfo.utils.Utils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * The Class ServletDownloadBodyNotification.
 */
@WebServlet(value = "/downloadBodyNotification", name = "Servlet download body notification")
public class ServletDownloadBodyNotification extends HttpServlet {

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.notifications.messages";
    /**
     * The Constant PARAMETER_REQUEST_ID_NOTIFICATION.
     */
    private static final String PARAMETER_REQUEST_ID_NOTIFICATION = "idnotification";
    /**
     * The Constant PARAMETER_REQUEST_LANGUAGE.
     */
    private static final String PARAMETER_REQUEST_LANGUAGE = "language";
    /**
     * The Constant PROPERTY_MSG_NOTIFICATION_DETAIL.
     */
    private static final String PROPERTY_MSG_NOTIFICATION_DETAIL = "PROPERTY_MSG_NOTIFICATION_DETAIL";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new ServletDownloadBodyNotification servlet download body
     * notification.
     */
    public ServletDownloadBodyNotification() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final ServletContext servletContext = this.getServletContext();
        final WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        final INotificationsManager notificationsManager = (INotificationsManager) applicationContext.getBean(INotificationsManager.ID_BEAN);
        final Long notificationId = Long.parseLong(request.getParameter(ServletDownloadBodyNotification.PARAMETER_REQUEST_ID_NOTIFICATION));
        final String language = request.getParameter(ServletDownloadBodyNotification.PARAMETER_REQUEST_LANGUAGE);
        final ResourceBundle resourceBundle = ResourceBundle.getBundle(ServletDownloadBodyNotification.BUNDLE_SOURCE_PATH, Strings.isNullOrEmpty(language) ? Locale.getDefault() : new Locale(language));
        final byte[] datas = notificationsManager.getBody(notificationId);
        final String nameFileDownload = Utils.createCodeFromString(String.format(resourceBundle.getString(ServletDownloadBodyNotification.PROPERTY_MSG_NOTIFICATION_DETAIL), notificationId));
        response.setContentType("application/xhtml+xml");
        response.setContentLength(datas.length);
        response.setHeader("Content-Disposition", String.format("attachment; filename=%s", nameFileDownload.replaceAll(",", "_")));
        response.getOutputStream().write(datas);
        response.getOutputStream().flush();
        StreamUtils.closeStream(response.getOutputStream());
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
