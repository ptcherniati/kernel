package org.inra.ecoinfo.notifications;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.StreamUtils;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * The Class ServletDownloadAttachmentNotification.
 */
@WebServlet(value = "/downloadAttachmentNotification", name = "Servlet download attachment notification")
public class ServletDownloadAttachmentNotification extends HttpServlet {

    /**
     * The Constant DD_MM_YYYY_HH_MM_SS_SSS.
     */
    private static final String DD_MM_YYYY_HH_MM_SS_SSS = "dd-MM-yyyy-HH-mm-ss-SSS";
    /**
     * The Constant PARAMETER_REQUEST_ID_NOTIFICATION.
     */
    private static final String PARAMETER_REQUEST_ID_NOTIFICATION = "idnotification";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new ServletDownloadAttachmentNotification servlet download
     * attachment notification.
     */
    public ServletDownloadAttachmentNotification() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final ServletContext servletContext = this.getServletContext();
        final WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        final INotificationsManager notificationsManager = (INotificationsManager) applicationContext.getBean(INotificationsManager.ID_BEAN);
        final Long notificationId = Long.parseLong(request.getParameter(ServletDownloadAttachmentNotification.PARAMETER_REQUEST_ID_NOTIFICATION));
        try {
            Optional<Notification> notificationById = notificationsManager.getNotificationById(notificationId);
            if(notificationById.isPresent()){
                Notification notification = notificationById.get();
            final String attachment = notification.getAttachment();
            final byte[] datas = notificationsManager.getAttachment(notificationId);
            final LocalDateTime date = notification.getDate();
            final String dateString = DateUtil.getUTCDateTextFromLocalDateTime(date, ServletDownloadAttachmentNotification.DD_MM_YYYY_HH_MM_SS_SSS);
            final String[] arrayUrl = attachment.split(Utils.SEPARATOR_URL);
            final String nameFileDownload = String.format("extraction_%s_%s", arrayUrl[arrayUrl.length - 2], dateString);
            response.setContentType("application/zip");
            response.setContentLength(datas.length);
            response.setHeader("Content-Disposition", String.format("attachment; filename=%s", nameFileDownload.replaceAll(",", "_").concat(Utils.EXTENSION_FILE_ZIP)));
            response.getOutputStream().write(datas);
            response.getOutputStream().flush();
            StreamUtils.closeStream(response.getOutputStream());
            }
        } catch (final BusinessException e) {
            UncatchedExceptionLogger.logUncatchedException("uncatched exception in ServletDownloadAttachmentNotification", e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
