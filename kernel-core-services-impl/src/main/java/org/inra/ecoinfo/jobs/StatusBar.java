package org.inra.ecoinfo.jobs;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import org.inra.ecoinfo.mga.business.IUser;

/**
 *
 * @author ptcherniati
 */
public class StatusBar implements IStatusBar {

    /**
     *
     */
    public static Map<LocalDateTime, StatusBar> statusBars = new HashMap();

    /**
     *
     * @return
     */
    public static Map<LocalDateTime, StatusBar> getStatusBars() {
        return statusBars;
    }

    private Integer progress = -1;
    private LocalDateTime date;
    private String groupName;
    private IUser user;

    /**
     *
     * @param user
     * @param groupName
     */
    public StatusBar(IUser user, String groupName) {
        super();
        this.date = LocalDateTime.now();
        this.groupName = groupName;
        this.user = user;
        getStatusBars().put(date, this);
    }

    /**
     *
     * @return
     */
    public Integer getProgress() {
        return progress;
    }

    /**
     *
     * @param progress
     */
    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     *
     * @param groupName
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     *
     * @return
     */
    public IUser getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(IUser user) {
        this.user = user;
    }
}
