package org.inra.ecoinfo.config.impl;

import java.util.List;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.AbstractConfiguration;
import org.inra.ecoinfo.config.ILocalisationConfiguration;
import org.inra.ecoinfo.config.IModuleConfiguration;
import org.inra.ecoinfo.localization.entity.Localization;

/**
 * The Class InternationalisationConfiguration.
 */
public class InternationalisationConfiguration extends AbstractConfiguration implements IModuleConfiguration, ILocalisationConfiguration {

    /**
     * The Constant MODULE_NAME.
     */
    public static final String MODULE_NAME = "localisation";
    /**
     * The Constant SCHEMA_PATH.
     */
    public static final String SCHEMA_PATH = "xsd/internationalisationConfiguration.xsd";
    /**
     * The Constant DEFAULT_LANGUAGE_FR.
     */
    public static final String DEFAULT_LANGUAGE_FR = "fr";

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.ILocalisationConfiguration#addLanguage(java.lang.String)
     */

    /**
     *
     * @param language
     */
    
    @Override
    public void addLanguage(String language) {
        Localization.addLanguage(language);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#createConfig(org.apache.commons.digester.Digester)
     */

    /**
     *
     * @param digester
     */

    @Override
    public void createConfig(Digester digester) {
        digester.addSetProperties("configuration/module/localizationConfiguration/localizations", "defaultLanguage", "defaultLocalisation");
        digester.addCallMethod("configuration/module/localizationConfiguration/localizations/localization", "addLanguage", 1);
        digester.addCallParam("configuration/module/localizationConfiguration/localizations/localization", 0, "language");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.ILocalisationConfiguration#getDefaultLocalisation()
     */

    /**
     *
     * @return
     */
    
    @Override
    public String getDefaultLocalisation() {
        return Localization.getDefaultLocalisation();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ILocalisationConfiguration#setDefaultLanguage(java.lang.String)
     */

    /**
     *
     * @param defaultLanguage
     */
    
    @Override
    public void setDefaultLocalisation(String defaultLanguage) {
        Localization.setDefaultLocalisation(defaultLanguage);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ILocalisationConfiguration#getLocalisations()
     */
    /**
     *
     * @return
     */
    @Override
    public List<String> getLocalisations() {
        return Localization.getLocalisations();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getModuleName()
     */

    /**
     *
     * @return
     */

    @Override
    public String getModuleName() {
        return InternationalisationConfiguration.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getSchemaPath()
     */

    /**
     *
     * @return
     */

    @Override
    public String getSchemaPath() {
        return InternationalisationConfiguration.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#isAutoLoad()
     */

    /**
     *
     * @return
     */

    
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }
}
