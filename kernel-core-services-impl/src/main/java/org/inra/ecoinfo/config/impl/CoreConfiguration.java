package org.inra.ecoinfo.config.impl;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.config.AbstractConfiguration;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.config.jpa.IGenericDAO;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.LoggerFactory;

/**
 * The Class CoreConfiguration.
 */
public class CoreConfiguration extends AbstractConfiguration implements ICoreConfiguration {

    private static final long serialVersionUID = 1;

    /**
     * The Constant CST_DEFAULT_SITE_SEPARATOR_FOR_FILE_NAMES.
     */
    public static final String CST_DEFAULT_SITE_SEPARATOR_FOR_FILE_NAMES = "-";
    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/coreConfiguration.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "coreConfiguration";

    /**
     *
     */
    protected static final String PATH_NAME = "configuration/module/coreConfiguration/name";

    /**
     *
     */
    protected static final String ADMIN = "admin";
    protected static final String ANONYMOUS = "anonymous";

    /**
     *
     */
    protected static final String ADMIN_DEFAULT_PASSWORD = "21232f297a57a5a743894a0e4a801fc3";
    protected static final String ANONYMOUS_DEFAULT_PASSWORD = "a743894a0e4a801fc321232f297a57a5";

    /**
     *
     */
    protected static final String MAIL_ADMIN = "admin.admin@inra.fr";
    protected static final String MAIL_ANONYMOUS = "anonymous.anonymous@inra.fr";

    /**
     * Gets the String cst default site separator for file names.
     *
     * @return the String cst default site separator for file names
     */
    public static String getCstDefaultSiteSeparatorForFileNames() {
        return CoreConfiguration.CST_DEFAULT_SITE_SEPARATOR_FOR_FILE_NAMES;
    }

    /**
     * The String id.
     */
    private String id;
    /**
     * The Map<String,String> internationalized names.
     */
    private Map<String, String> internationalizedNames = new HashMap<>();
    /**
     * The String mail admin.
     */
    private String mailAdmin;
    /**
     * The String mail host.
     */
    private String mailHost="";
    /**
     * The String repository uri.
     */
    private String repositoryURI;
    /**
     * The String site separator for file names.
     */
    private String siteSeparatorForFileNames = CoreConfiguration.CST_DEFAULT_SITE_SEPARATOR_FOR_FILE_NAMES;
    /**
     * The utilisateur dao.
     */
    private IUtilisateurDAO utilisateurDAO;
    private IMgaRecorder mgaRecorderDAO;

    private ILocalizationManager localizationManager;
    private IGenericDAO generiqueDAO;

    /**
     *
     */
    public CoreConfiguration() {
        super();
    }

    /**
     *
     * @param recorder
     */
    public void setRecorder(IMgaRecorder recorder) {
        this.mgaRecorderDAO = recorder;
    }

    /**
     *
     * @param generiqueDAO
     */
    public void setGeneriqueDAO(IGenericDAO generiqueDAO) {
        this.generiqueDAO = generiqueDAO;
    }

    /**
     * Adds the internationalized name.
     *
     * @param language the String language
     * @param value the String value
     */
    public void addInternationalizedName(final String language, final String value) {
        internationalizedNames.put(language == null ? Localization.getDefaultLocalisation() : language, value);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#createConfig(org.apache.commons.digester.Digester)
     */

    /**
     *
     * @param digester
     */

    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod(PATH_NAME, "addInternationalizedName", 2, newStringClassArray());
        digester.addCallParam(PATH_NAME, 0, "language");
        digester.addObjectCreate(PATH_NAME, String.class);
        digester.addCallMethod(PATH_NAME, "concat", 0);
        digester.addCallParam(PATH_NAME, 1, false);
        digester.addCallMethod("configuration/module/coreConfiguration/mailAdmin", "setMailAdmin", 0);
        digester.addCallMethod("configuration/module/coreConfiguration/mailHost", "setMailHost", 0);
        digester.addCallMethod("configuration/module/coreConfiguration/id", "setId", 0);
        digester.addCallMethod("configuration/module/coreConfiguration/repositoryURI", "setRepositoryURI", 0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.ICoreConfiguration#getId()
     */
    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.ICoreConfiguration#setId(java.lang.String)
     */
    /**
     *
     * @param id
     */
    @Override
    public void setId(String id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ICoreConfiguration#getInternationalizedNames()
     */
    /**
     *
     * @return
     */
    @Override
    public Map<String, String> getInternationalizedNames() {
        return internationalizedNames;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.ICoreConfiguration#setInternationalizedNames(java.util.Map)
     */
    /**
     *
     * @param internationalizedNames
     */
    @Override
    public void setInternationalizedNames(Map<String, String> internationalizedNames) {
        this.internationalizedNames = internationalizedNames;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ICoreConfiguration#getMailAdmin()
     */
    /**
     *
     * @return
     */
    @Override
    public String getMailAdmin() {
        return mailAdmin;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.ICoreConfiguration#setMailAdmin(java.lang.String)
     */
    /**
     *
     * @param mailAdmin
     */
    @Override
    public void setMailAdmin(String mailAdmin) {
        this.mailAdmin = mailAdmin;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ICoreConfiguration#getMailHost()
     */
    /**
     *
     * @return
     */
    @Override
    public String getMailHost() {
        return mailHost;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.ICoreConfiguration#setMailHost(java.lang.String)
     */
    /**
     *
     * @param mailHost
     */
    @Override
    public void setMailHost(String mailHost) {
        this.mailHost = mailHost;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getModuleName()
     */

    /**
     *
     * @return
     */

    @Override
    public String getModuleName() {
        return CoreConfiguration.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.ICoreConfiguration#getRepositoryURI()
     */
    /**
     *
     * @return
     */
    @Override
    public String getRepositoryURI() {
        return repositoryURI;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ICoreConfiguration#setRepositoryURI(java.lang.String)
     */
    /**
     *
     * @param repositoryURI
     */
    @Override
    public void setRepositoryURI(String repositoryURI) {
        this.repositoryURI = repositoryURI;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getSchemaPath()
     */

    /**
     *
     * @return
     */

    @Override
    public String getSchemaPath() {
        return CoreConfiguration.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.ICoreConfiguration#getSiteSeparatorForFileNames()
     */
    /**
     *
     * @return
     */
    @Override
    public String getSiteSeparatorForFileNames() {
        return siteSeparatorForFileNames;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.ICoreConfiguration#setSiteSeparatorForFileNames(java.lang.String)
     */
    /**
     *
     * @param siteSeparatorForFileNames
     */
    @Override
    public void setSiteSeparatorForFileNames(String siteSeparatorForFileNames) {
        this.siteSeparatorForFileNames = siteSeparatorForFileNames;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#isAutoLoad()
     */

    /**
     *
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractConfiguration#postConfig()
     */

    /**
     *
     * @throws BusinessException
     */

    @Override
    public void postConfig() throws BusinessException {
        try {
            IntervalDate.setLocalizationManager(localizationManager);
            updateDatabase();
            createRepositoryIfNeeded();
            addPostSQLScript();
            mgaRecorderDAO.setUserDao(utilisateurDAO, AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS);
            mgaRecorderDAO.setUserDao(utilisateurDAO, AbstractMgaIOConfigurator.REFDATA_CONFIGURATION_RIGHTS);
        } catch (final PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the utilisateur dao.
     *
     * @param utilisateurDAO the new utilisateur dao
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param generiqueDAO
     */
    public void setGenericDAO(IGenericDAO generiqueDAO) {
        this.generiqueDAO = generiqueDAO;
    }

    private void addPostSQLScript() {
        URL resource = getClass().getResource("/META-INF/script.sql");
        if (resource == null) {
            return;
        }
        File sqlFile = new File(resource.getPath());
        if (!sqlFile.exists() && !sqlFile.canRead()) {
            return;
        }
        try (Stream<String> lines = Files.lines(sqlFile.toPath(), StandardCharsets.ISO_8859_1)) {
            lines.forEach(line->generiqueDAO.execute(line));
        } catch (IOException ex) {
            LoggerFactory.getLogger(getClass()).error("Can't read file {}", sqlFile.getAbsolutePath(), ex);
        }
    }

    /**
     * Update utilisateur.
     *
     * @throws PersistenceException the persistence exception
     */
    private void createUser(String nom, String prenom, String login, Boolean isRoot, String email, String password, Boolean active) throws PersistenceException {
        Utilisateur utilisateur = new Utilisateur();
        WhichTree[] values = WhichTree.values();
        utilisateur.setNom(nom);
        utilisateur.setPrenom(prenom);
        utilisateur.setLogin(login);
        utilisateur.setIsRoot(isRoot);
        utilisateur.setEmail(email);
        utilisateur.setPassword(password);
        utilisateur.setActive(active);
        utilisateurDAO.saveOrUpdate(utilisateur);
        for (int i = 0; i < values.length; i++) {
            WhichTree value = values[i];
            Group group = mgaRecorderDAO.createGroup(login, value, GroupType.USER_TYPE);
            utilisateur = utilisateurDAO.addGroupToUser(utilisateur, group).orElse(null);
        }
    }

    private void createRepositoryIfNeeded() throws IOException {
        String repositoryFiles = getRepositoryURI();
        FileWithFolderCreator.createFile(repositoryFiles + "/.");
    }

    /**
     * Update database.
     *      * add the neccessaruies tables and rows in the database
     *
     * @throws PersistenceException the persistence exception
     */
    protected void updateDatabase() throws PersistenceException {
        ((AbstractJPADAO) utilisateurDAO).initCriteriaBuilder();
        mgaRecorderDAO.createGroup("public", WhichTree.TREEDATASET, GroupType.USER_TYPE);
        final boolean hasNoUtilisateurs = utilisateurDAO.getAdmins().isEmpty();
        if (hasNoUtilisateurs) {
            createUser(ADMIN, ADMIN, ADMIN, true, MAIL_ADMIN, ADMIN_DEFAULT_PASSWORD, true);
        }
        final boolean hasNoAnonymous = !utilisateurDAO.getByLogin(ANONYMOUS).isPresent();
        if (hasNoAnonymous) {
            createUser(ANONYMOUS, ANONYMOUS, ANONYMOUS, true, MAIL_ANONYMOUS, ANONYMOUS_DEFAULT_PASSWORD, true);
        }
    }
    
    public String getAnonymousLogin(){
        return ANONYMOUS;
    }
}
