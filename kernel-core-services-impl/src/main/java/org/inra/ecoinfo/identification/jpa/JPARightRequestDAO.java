package org.inra.ecoinfo.identification.jpa;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.identification.IRightRequestDAO;
import org.inra.ecoinfo.identification.entity.RightRequest;
import org.inra.ecoinfo.identification.entity.Utilisateur;

/**
 *
 * @author ptcherniati
 */
public class JPARightRequestDAO extends AbstractJPADAO<RightRequest> implements IRightRequestDAO<RightRequest>, Serializable {

    /**
     *
     * @param utilisateur
     * @return
     */
    @Override
    public List<RightRequest> getByUser(Utilisateur utilisateur) {
        return new LinkedList();
    }

    /**
     *
     * @return
     */
    @Override
    public List<RightRequest> getAll() {
        return getAll(RightRequest.class);
    }

}
