package org.inra.ecoinfo.identification.jpa;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.GroupUtilisateur;
import org.inra.ecoinfo.identification.entity.GroupUtilisateur_;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.entity.Utilisateur_;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.business.composite.groups.Group_;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class JPAUtilisateurDAO.
 */
public class JPAUtilisateurDAO extends AbstractJPADAO<Utilisateur> implements IUtilisateurDAO, Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    private static final String PUBLIC = "public";

    private static boolean isAssociate(Tuple tuple) {
        final IUser user = (IUser) tuple.get(0);
        final Group group = (Group) tuple.get(1);
        final GroupUtilisateur groupUtilisateur = (GroupUtilisateur) tuple.get(2);
        final boolean isAssociate = (groupUtilisateur).getGroup().equals(group)
                && groupUtilisateur.getUser().equals(user);
        final boolean isPublic = group.getGroupName().equals("public")
                && group.getWhichTree().equals(WhichTree.TREEDATASET);
        return isAssociate || isPublic;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.identification.IUtilisateurDAO#getByLogin(java.lang.String)
     */
    /**
     *
     * @param login
     * @return
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Optional<Utilisateur> getByLogin(final String login) {
        CriteriaQuery<Utilisateur> query = builder.createQuery(Utilisateur.class);
        Root<Utilisateur> users = query.from(Utilisateur.class);
        query.where(builder.equal(users.get(Utilisateur_.login), login));
        query.select(users);
        return getFirstOrNullAndAddGroups(query);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.identification.IUtilisateurDAO#getByLoginAndPassword(java.lang.String, java.lang.String)
     */
    /**
     *
     * @param login
     * @param password
     * @return
     */
    @Override
    public Optional<Utilisateur> getByLoginAndPassword(final String login, final String password) {
        CriteriaQuery<Utilisateur> query = builder.createQuery(Utilisateur.class);
        Root<Utilisateur> users = query.from(Utilisateur.class);
        query.where(builder.equal(users.get(Utilisateur_.login), login));
        query.where(builder.equal(users.get(Utilisateur_.password), password));
        query.select(users);
        return getFirstOrNullAndAddGroups(query);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.identification.IUtilisateurDAO#hasUser()
     */
    /**
     *
     * @return
     */
    @Override
    public boolean hasUser() {
        try {
            final CriteriaQuery<Boolean> cq = builder.createQuery(Boolean.class);
            Root<Utilisateur> user = cq.from(Utilisateur.class);
            cq.select(builder.greaterThan(builder.count(user), 0L));
            return getFirstOrNull(cq);
        } catch (final Exception e) {
            LOGGER.info("no user", e);
            return false;
        }
    }

    /**
     *
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public List<Utilisateur> getAdmins() {
        Group publicGroup = getPublicGroup();
        CriteriaQuery<Utilisateur> query = builder.createQuery(Utilisateur.class);
        Root<Utilisateur> users = query.from(Utilisateur.class);
        query.where(builder.equal(users.get(Utilisateur_.isRoot), true));
        query.select(users);
        return getResultListAndAddGroups(query, publicGroup);
    }

    private List<Utilisateur> getResultListAndAddGroups(CriteriaQuery<Utilisateur> query, Group publicGroup) {
        return getResultListToStream(query)
                .map((t) -> addAllGroups(t, publicGroup))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param email
     * @return
     */
    @Override
    public Optional<Utilisateur> getByEMail(String email) {
        try {

            CriteriaQuery<Utilisateur> query = builder.createQuery(Utilisateur.class);
            Root<Utilisateur> users = query.from(Utilisateur.class);
            query.where(builder.equal(users.get(Utilisateur_.email), email));
            query.select(users);
            return getFirstOrNullAndAddGroups(query);
        } catch (final Exception e) {
            LOGGER.info("no user", e);
            return Optional.empty();
        }
    }

    private Optional<Utilisateur> getFirstOrNullAndAddGroups(CriteriaQuery<Utilisateur> query) {
        return getOptional(query)
                .map(user -> addAllGroups(user, getPublicGroup()));
    }

    /**
     *
     * @return
     */
    @Override
    public List<Utilisateur> getAll() {
        Group publicGroup = getPublicGroup();
        CriteriaQuery<Utilisateur> query = builder.createQuery(Utilisateur.class);
        Root<Utilisateur> all = query.from(Utilisateur.class);
        query.select(all);
        return getResultListAndAddGroups(query, publicGroup);
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public Optional<Utilisateur> getById(Long id) {
        CriteriaQuery<Utilisateur> query = builder.createQuery(Utilisateur.class);
        Root<Utilisateur> u = query.from(Utilisateur.class);
        query.where(builder.equal(u.get(Utilisateur_.id), id));
        query.select(u);
        return getFirstOrNullAndAddGroups(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Utilisateur> retrieveAllUsers() {
        return getAll();
    }

    /**
     *
     * @param whichTree
     * @return
     */
    @Override
    public List<ICompositeGroup> retrieveAllUsersOrGroups(WhichTree whichTree) {
        List<ICompositeGroup> compositeGroups = getAll().stream().collect(Collectors.toList());
        List<String> groupNames = getAll().stream().map(gr -> gr.getGroupName())
                .collect(Collectors.toList());
        CriteriaQuery<ICompositeGroup> query2 = builder.createQuery(ICompositeGroup.class);
        Root<Group> g = query2.from(Group.class);
        query2.where(
                builder.equal(g.get(Group_.whichTree), whichTree),
                g.get(Group_.GROUP_TYPE).in(GroupType.SIMPLE_GROUP)
        );
        query2.select(g);
        compositeGroups.addAll(getResultList(query2));
        return compositeGroups;
    }

    /**
     *
     * @param <T>
     * @param user
     * @param group
     * @return
     */
    @Override
    public <T extends ICompositeGroup> Optional<T> addGroupToUser(T user, Group group) {
        Utilisateur utilisateur = (Utilisateur) user;
        if (utilisateur.testIsMutableGroup(group)) {
            try {
                utilisateur = merge(
                        utilisateur.getGroupsUsers()
                                .stream()
                                .filter(gu -> gu.getUser().equals(user) && gu.getGroup().equals(group))
                                .findFirst().orElse(_addGroup(utilisateur, group))
                                .getUser());
            } catch (PersistenceException ex) {
                LOGGER.error("can't merge user", ex);
            }
        }
        return Optional.ofNullable((T) utilisateur);
    }

    private GroupUtilisateur _addGroup(Utilisateur user, Group group) {
        CriteriaQuery<GroupUtilisateur> query = builder.createQuery(GroupUtilisateur.class);
        Root<GroupUtilisateur> guFrom = query.from(GroupUtilisateur.class);
        query.where(builder.and(
                builder.equal(guFrom.join(GroupUtilisateur_.group), group),
                builder.equal(guFrom.join(GroupUtilisateur_.user), user)
        ));
        query.select(guFrom);
        return getOptional(query).orElseGet(() -> {
            GroupUtilisateur gu = new GroupUtilisateur(user, group);
            entityManager.persist(gu);
            return gu;
        });
    }

    /**
     *
     * @param <T>
     * @param user
     * @param group
     * @return
     */
    @Override
    public <T extends ICompositeGroup> Optional<T> removeGroupOfUser(T user, Group group) {
        Utilisateur utilisateur = (Utilisateur) user;
        if (utilisateur.testIsMutableGroup(group)) {
            getGroupUtilisateur(utilisateur, group).ifPresent(gu -> entityManager.remove(gu));
            utilisateur.getAllGroups().remove(group);
            utilisateur = getByLogin(utilisateur.getLogin()).orElse(utilisateur);
        }
        return Optional.ofNullable((T) utilisateur);
    }

    Optional<GroupUtilisateur> getGroupUtilisateur(Utilisateur user, Group group) {
        CriteriaQuery<GroupUtilisateur> query = builder.createQuery(GroupUtilisateur.class);
        Root<GroupUtilisateur> gu = query.from(GroupUtilisateur.class);
        query.where(
                builder.equal(gu.join(GroupUtilisateur_.group), group),
                builder.equal(gu.join(GroupUtilisateur_.user), user)
        );
        query.select(gu);
        return getOptional(query);
    }
    
    /**
     * @param utilisateur
     * @throws PersistenceException
     */
    @Override
    public void remove(final Utilisateur utilisateur) throws PersistenceException {
        Utilisateur dbUtilisateur = merge(utilisateur);
        dbUtilisateur.getGroupsUsers().stream()
                .forEach((gu) -> {
                    if (gu.getUser().equals(utilisateur)) {
                        remove(gu.getGroup());
                    } else {
                        entityManager.remove(gu);
                    }
                });
        flush();
        super.remove(getById(utilisateur.getId()).orElse(null));
    }



    /**
     *
     * @param group
     */
    @Override
    public void saveOrUpdate(Group group) {
        try {
            saveOrUpdateGeneric(group);
        } catch (org.inra.ecoinfo.utils.exceptions.PersistenceException ex) {
            LOGGER.debug(String.format("can't perist group %s %s", group.getGroupName(), group.getWhichTree().name()), ex);
        }
    }

    private Group getPublicGroup() {
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> group = query.from(Group.class);
        query.where(
                builder.equal(group.get(Group_.whichTree), WhichTree.TREEDATASET),
                builder.equal(group.get(Group_.groupName), PUBLIC)
        );
        query.select(group);
        return getFirstOrNull(query);
    }

    private Utilisateur addAllGroups(Utilisateur user, Group publicGroup) {
        Optional.ofNullable(user).ifPresent(u -> u.getAllGroups().add(publicGroup));
        return user;
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Group, Boolean> getAllPossiblesGroupUser(IUser user) {
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> g = query.from(Group.class);
        query
                .where(
                        builder.or(
                                builder.equal(g.get(Group_.groupType), GroupType.SIMPLE_GROUP),
                                builder.equal(g.get(Group_.groupName), user.getLogin())
                        )
                )
                .select(g);
        return getResultList(query).stream()
                .collect(Collectors.toMap(gr -> gr, gr -> user.getAllGroups().contains(gr)));
    }

    /**
     *
     * @param group
     */
    @Override
    public void remove(ICompositeGroup group) {
        CriteriaDelete<GroupUtilisateur> delete = builder.createCriteriaDelete(GroupUtilisateur.class);
        Root<GroupUtilisateur> gu = delete.from(GroupUtilisateur.class);
        delete.where(builder.equal(gu.get(GroupUtilisateur_.group), group));
        entityManager.createQuery(delete).executeUpdate();
        entityManager.remove(group);
    }

}
