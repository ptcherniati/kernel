/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.identification.impl;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import org.inra.ecoinfo.identification.entity.RightRequest;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.entity.Localization;

/**
 *
 * @author ptcherniati
 */
public class DefaultRequestManager extends AbstractRequestManager<RightRequest> {

    /**
     *
     * @param rightRequest
     * @return
     */
    @Override
    protected Optional<Utilisateur> getUtilisateurForRequest(RightRequest rightRequest) {
        Utilisateur utilisateur = rightRequest.getUser();
        if(utilisateur==null){
            utilisateur=(Utilisateur) policyManager.getCurrentUser();
        }
        return utilisateurDAO.getById(utilisateur.getId());
    }

    /**
     *
     * @param admin
     * @param utilisateur
     * @param rightRequest
     * @return
     */
    @Override
    public String getMailMessageForRequest(Utilisateur admin, final Utilisateur utilisateur, RightRequest rightRequest) {
        final ResourceBundle resourceBundle = ResourceBundle.getBundle(DefaultUsersManager.BUNDLE_NAME, utilisateur.getLanguage() == null ? new Locale(Localization.getDefaultLocalisation()) : new Locale(utilisateur.getLanguage()));
        String message = String.format(resourceBundle.getString(DefaultUsersManager.PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT), admin.getPrenom(), admin.getNom(), utilisateur.getLogin(), rightRequest.getRequest(), rightRequest.getMotivation(), rightRequest.getReference(), rightRequest.getInfos(), utilisateur.getEmail());
        return message;
    }

    /**
     *
     * @param rightRequest
     * @param utilisateur
     */
    @Override
    protected void setRightRequestUser(RightRequest rightRequest, Utilisateur utilisateur) {
        rightRequest.setUser(utilisateur);
    }
}
