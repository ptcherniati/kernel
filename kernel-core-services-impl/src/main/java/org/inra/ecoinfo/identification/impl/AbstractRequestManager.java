/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.identification.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.identification.IRequestManager;
import org.inra.ecoinfo.identification.IRightRequestDAO;
import org.inra.ecoinfo.identification.IUsersDeletion;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.IRightRequest;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mailsender.IMailSender;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.DeleteUserException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 * @param <R>
 */
public abstract class AbstractRequestManager<R extends IRightRequest> extends MO implements IUsersDeletion, IRequestManager<R> {
    
    /**
     * The Constant BUNDLE_NAME.
     */
    public static final String BUNDLE_NAME = "org.inra.ecoinfo.identification.impl.messages";
    
    /**
     * The Constant PROPERTY_MSG_FOR_SUCCESS_RIGHTREQUEST_DELETION.
     */
    public static final String PROPERTY_MSG_FOR_SUCCESS_RIGHTREQUEST_DELETION = "PROPERTY_MSG_FOR_SUCCESS_RIGHTREQUEST_DELETION";
    
    /**
     * The Constant PROPERTY_MSG_FOR_ECHEC_RIGHTREQUEST_DELETION.
     */
    public static final String PROPERTY_MSG_FOR_ECHEC_RIGHTREQUEST_DELETION="PROPERTY_MSG_FOR_ECHEC_RIGHTREQUEST_DELETION";
    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(DefaultRequestManager.class);

    /**
     *
     */
    protected IUtilisateurDAO utilisateurDAO;

    /**
     *
     */
    protected IRightRequestDAO<R> rightRequestDAO;

    /**
     *
     */
    protected ICoreConfiguration configuration;

    /**
     *
     */
    protected IMailSender mailSender;
    

    /**
     *
     * @param policyManager
     */
    @Override
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     *
     * @param rightRequestDAO
     */
    public void setRightRequestDAO(IRightRequestDAO<R> rightRequestDAO) {
        this.rightRequestDAO = rightRequestDAO;
    }

    /**
     *
     * @param configuration
     */
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param mailSender
     */
    public void setMailSender(IMailSender mailSender) {
        this.mailSender = mailSender;
    }

    /**
     *
     * @param request
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveRequest(R request) {
        try {
            rightRequestDAO.saveOrUpdate(rightRequestDAO.merge(request));
        } catch (PersistenceException ex) {
            LOGGER.error("can't update request", ex);
        }
    }

    /**
     *
     * @param request
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteRequest(R request) {
        try {
            rightRequestDAO.remove(rightRequestDAO.merge(request));
        } catch (PersistenceException ex) {
            LOGGER.error("can't selete request", ex);
        }
    }
    
    /**   
     * @param utilisateur
     * @return rightRequestReport
     * @throws DeleteUserException the DeleteUserException exception
     */
    @Override
    public InfosReport deleteDependancesUserProfile(Utilisateur utilisateur) throws DeleteUserException {
        InfosReport rightRequestReport = new InfosReport("RightRequests Deletion");
        String rightRequestMessage;
                
        List<R> userRequests = getRestrictedRequestsForDeletion(utilisateur);
        for (R request : userRequests) {
            deleteRequest(request);
        }
        userRequests = getRestrictedRequestsForDeletion(utilisateur);
        if (userRequests.isEmpty()) {
            
            rightRequestMessage = String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_FOR_SUCCESS_RIGHTREQUEST_DELETION), utilisateur.getLogin());
            rightRequestReport.addInfos("Right_Request_deletion", rightRequestMessage);
        }
        else{
            rightRequestMessage = String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_FOR_ECHEC_RIGHTREQUEST_DELETION), utilisateur.getLogin());
            throw new DeleteUserException(rightRequestMessage);
        }
        
        return rightRequestReport;
    }

    /**
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<R> getRestrictedRequests() {
        final IUser utilisateur = policyManager.getCurrentUser();
        List<R> requests = rightRequestDAO.getAll();
        if (utilisateur.getIsRoot()) {
            return requests;
        } else {
            List<R> restrictedRequests = new LinkedList();
            requests.stream().forEach((request) -> {
                try {
                    if (getUtilisateurForRequest(request)
                            .orElseThrow(() -> new BusinessException("Can't find user"))
                            .getLogin().equals(utilisateur.getLogin())) {
                        restrictedRequests.add(request);
                    }
                } catch (BusinessException ex) {
                    LOGGER.error("can't load requests", ex);
                }
            });
            return restrictedRequests;
        }
    }

    /**
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    private List<R> getRestrictedRequestsForDeletion(Utilisateur utilisateur) {
        List<R> requests = rightRequestDAO.getAll();
        if (utilisateur.getIsRoot()) {
            return requests;
        } else {
            List<R> restrictedRequests = new LinkedList();
            requests.stream().forEach((request) -> {
                try {
                    if (getUtilisateurForRequest(request)
                            .orElseThrow(() -> new BusinessException("Can't find user"))
                            .getLogin().equals(utilisateur.getLogin())) {
                        restrictedRequests.add(request);
                    }
                } catch (BusinessException ex) {
                    LOGGER.error("can't load requests", ex);
                }
            });
            return restrictedRequests;
        }
    }

    /**
     *
     * @param rightRequest
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addNewRequest(R rightRequest) throws BusinessException {
        try {
            Utilisateur utilisateur = utilisateurDAO.merge((rightRequest.getUser()==null?(Utilisateur)policyManager.getCurrentUser():rightRequest.getUser()));
            setRightRequestUser(rightRequest, utilisateur);
            rightRequestDAO.saveOrUpdate(rightRequestDAO.merge(rightRequest));
            rightRequestDAO.flush();
            final ResourceBundle resourceBundle = ResourceBundle.getBundle(DefaultUsersManager.BUNDLE_NAME, utilisateur.getLanguage() == null ? new Locale(Localization.getDefaultLocalisation()) : new Locale(utilisateur.getLanguage()));
            final String from = configuration.getMailAdmin();
            final String subject = resourceBundle.getString(DefaultUsersManager.PROPERTY_MSG_RIGHT_DEMAND);
            List<Utilisateur> administrators = utilisateurDAO.getAdmins();
            for (Utilisateur admin : administrators) {
                String message = getMailMessageForRequest(admin, utilisateur, rightRequest);
                mailSender.sendMail(from, admin.getEmail(), subject, message);
            }
            sendNotification(String.format(localizationManager.getMessage(DefaultUsersManager.BUNDLE_NAME, DefaultUsersManager.PROPERTY_MSG_NEW_RIGHT_REQUEST)), Notification.INFO, localizationManager.getMessage(DefaultUsersManager.BUNDLE_NAME, DefaultUsersManager.PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY), utilisateur);
        } catch (final PersistenceException e) {
            throw new BusinessException("can't save request", e);
        }
    }

    /**
     *
     * @param rightRequest
     * @param utilisateur
     */
    abstract protected void setRightRequestUser(R rightRequest, Utilisateur utilisateur);

    /**
     *
     * @param rightRequest
     * @return
     */
    abstract protected Optional<Utilisateur> getUtilisateurForRequest(R rightRequest);

}
