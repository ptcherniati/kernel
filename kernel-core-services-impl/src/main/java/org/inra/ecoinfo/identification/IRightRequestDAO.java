package org.inra.ecoinfo.identification;

import java.util.List;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;

/**
 *
 * @author ptcherniati
 * @param <R>
 */
public interface IRightRequestDAO<R> extends IDAO<R> {

    /**
     *
     * @param utilisateur
     * @return
     */
    List<R> getByUser(Utilisateur utilisateur);

    /**
     *
     * @return
     */
    List<R> getAll();

}
