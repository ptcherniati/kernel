package org.inra.ecoinfo.identification.impl;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.identification.IUsersDeletion;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.exception.BadPasswordException;
import org.inra.ecoinfo.identification.exception.InactiveUserException;
import org.inra.ecoinfo.identification.exception.UnknownUserException;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mailsender.IMailSender;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.GroupType;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import static org.inra.ecoinfo.utils.Utils.toMD5;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.DeleteUserException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class DefaultUsersManager.
 */
public class DefaultUsersManager extends MO implements IUsersManager {

    /**
     *
     */
    public static final Logger USER_DELETION_LOGGER = LoggerFactory.getLogger(IUsersDeletion.class);

    /**
     * The Constant BUNDLE_NAME.
     */
    public static final String BUNDLE_NAME = "org.inra.ecoinfo.identification.impl.messages";

    /**
     *
     */
    public static final String SITE_BUNDLE_NAME = "org.inra.ecoinfo.jsf.site";
    /**
     * The Constant MSG_BAD_PASSWORD.
     */
    public static final String MSG_BAD_PASSWORD = "PROPERTY_MSG_BAD_PASSWORD";

    /**
     *
     */
    public static final String MSG_EXISTING_EMAIL = "PROPERTY_MSG_EXISTING_EMAIL";

    /**
     *
     */
    public static final String MSG_EXISTING_LOGIN = "PROPERTY_MSG_EXISTING_LOGIN";
    /**
     * The Constant MSG_UNKNOWN_USER.
     */
    public static final String MSG_UNKNOWN_USER = "PROPERTY_MSG_UNKNOWN_USER";

    /**
     *
     */
    public static final String MSG_INACTIVE_USER = "PROPERTY_MSG_INACTIVE_USER";
    /**
     * The Constant PROPERTY_MSG_INITIALISATION_TITLE.
     */
    public static final String PROPERTY_MSG_INITIALISATION_TITLE = "PROPERTY_MSG_INITIALISATION_TITLE";
    /**
     * The Constant PROPERTY_MSG_NEW_PASSWORD_MAIL_CONTENT.
     */
    public static final String PROPERTY_MSG_NEW_PASSWORD_MAIL_CONTENT = "PROPERTY_MSG_NEW_PASSWORD_MAIL_CONTENT";
    /**
     * The Constant PROPERTY_MSG_CREATION_TITLE.
     */
    public static final String PROPERTY_MSG_CREATION_TITLE = "PROPERTY_MSG_CREATION_TITLE";
    /**
     * The Constant PROPERTY_MSG_NEW_USER_MAIL_CONTENT.
     */
    public static final String PROPERTY_MSG_NEW_USER_MAIL_CONTENT = "PROPERTY_MSG_NEW_USER_MAIL_CONTENT";

    /**
     *
     */
    public static final String PARAM_MSG_ORE_FULL_NAME = "PARAM_MSG_ORE_FULL_NAME";
    /**
     * The Constant PROPERTY_MSG_RIGHT_DEMAND.
     */
    public static final String PROPERTY_MSG_RIGHT_DEMAND = "PROPERTY_MSG_RIGHT_DEMAND";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT.
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT = "PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST = "PROPERTY_MSG_NEW_RIGHT_REQUEST";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY = "PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY";

    /**
     * The Constant PROPERTY_MSG_DEFAULT_MESSAGE_FOR_USER_DELETE
     */
    public static final String PROPERTY_MSG_INITIAL_MESSAGE_FOR_USER_DELETE = "PROPERTY_MSG_INITIAL_MESSAGE_FOR_USER_DELETE";

    /**
     * The Constant PROPERTY_MSG_DEFAULT_MESSAGE_FOR_USER_DELETE
     */
    public static final String PROPERTY_MSG_INITIAL_ERROR_MESSAGE_FOR_USER_DELETE = "PROPERTY_MSG_INITIAL_ERROR_MESSAGE_FOR_USER_DELETE";

    /**
     * The Constant PROPERTY_MSG_FOR_ADMINISTRATOR_DELETE
     */
    public static final String PROPERTY_MSG_FOR_ADMINISTRATOR_DELETE = "PROPERTY_MSG_FOR_ADMINISTRATOR_DELETE";

    /**
     * The Constant PROPERTY_MSG_FOR_ADMINISTRATOR_DELETE
     */
    public static final String PROPERTY_ERROR_MSG_FOR_USER_DELETION = "PROPERTY_ERROR_MSG_FOR_USER_DELETION";

    /**
     * The Constant PROPERTY_MSG_FOR_ADMINISTRATOR_DELETE
     */
    public static final String PROPERTY_SUCCESS_MSG_FOR_USER_DELETION = "PROPERTY_SUCCESS_MSG_FOR_USER_DELETION";

    /**
     *
     */
    public static final String PROPERTY_SUCCESS_MSG_MAIL_SUBJECT_FOR_USER_DELETION = "PROPERTY_SUCCESS_MSG_MAIL_SUBJECT_FOR_USER_DELETION";
    protected static final String ERROR_MSG_CANT_GET_HOST_ADRESS = "can't get host adress";

    /**
     * The ICoreConfiguration configuration.
     */
    private ICoreConfiguration configuration;
    /**
     * The IMailSender mail sender.
     */
    protected IMailSender mailSender;
    /**
     * The IUtilisateurDAO utilisateur dao.
     */
    protected IUtilisateurDAO utilisateurDAO;
    private IMgaRecorder mgaRecorderDAO;
    private Map<String, IUsersDeletion> usersDeletionManagers = new LinkedHashMap<>();

    /**
     *
     * @param recorder
     */
    public void setRecorder(IMgaRecorder recorder) {
        this.mgaRecorderDAO = recorder;
    }

    /**
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.identification.IUsersManager#addUserProfile(org.inra.ecoinfo.identification.entity.Utilisateur)
     *
     * @param utilisateur
     * @param baseUrl
     * @throws BusinessException
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addUserProfile(final Utilisateur utilisateur, final String baseUrl) throws BusinessException {
        addUserProfile(utilisateur, baseUrl, null);
    }

    /**
     *
     * @param utilisateur
     * @param baseUrl
     * @param otherRecipients
     * @throws BusinessException
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void addUserProfile(final Utilisateur utilisateur, final String baseUrl, final List<Utilisateur> otherRecipients) throws BusinessException {
        try {
            if (utilisateur.getPassword() != null) {
                utilisateur.setPassword(toMD5(utilisateur.getPassword()));
            }
        } catch (NoSuchAlgorithmException ex) {
            LoggerFactory.getLogger(DefaultUsersManager.class.getName()).error(null, ex);
            throw new BusinessException("erreur Hash", ex);
        }

        try {
            utilisateur.setId(null);
            final ResourceBundle resourceBundle = ResourceBundle.getBundle(DefaultUsersManager.BUNDLE_NAME, utilisateur.getLanguage() == null ? new Locale(Localization.getDefaultLocalisation()) : new Locale(utilisateur.getLanguage()));
            if (utilisateurDAO.getByEMail(utilisateur.getEmail()).isPresent()) {
                throw new BusinessException(String.format(resourceBundle.getString(MSG_EXISTING_EMAIL), utilisateur.getEmail()));
            }
            if (utilisateurDAO.getByLogin(utilisateur.getLogin()).isPresent()) {
                throw new BusinessException(String.format(resourceBundle.getString(MSG_EXISTING_LOGIN), utilisateur.getLogin()));
            }
            final WhichTree[] whichtrees = WhichTree.values();
            utilisateurDAO.saveOrUpdate(utilisateur);
            for (int i = 0; i < whichtrees.length; i++) {
                WhichTree whichTree = whichtrees[i];
                Group group = createAndSaveGroup(utilisateur.getLogin(), whichTree);
                utilisateurDAO.addGroupToUser(utilisateur, group);
            }
            sendValidationEmail(utilisateur, baseUrl, resourceBundle, otherRecipients);

        } catch (final PersistenceException e) {
            throw new BusinessException("can't save user", e);
        }
    }

    Group createAndSaveGroup(String groupName, WhichTree whichTree) {
        Group pg = mgaRecorderDAO.createGroup(groupName, whichTree, GroupType.USER_TYPE);
        return pg;
    }

    /**
     *
     * @param utilisateur
     * @param baseUrl
     * @param resourceBundle
     * @param otherRecipients
     */
    @Override
    public void sendValidationEmail(final Utilisateur utilisateur, final String baseUrl, final ResourceBundle resourceBundle, List<Utilisateur> otherRecipients) {
        final String from = getMailAdmin();
        final String subject = resourceBundle.getString(DefaultUsersManager.PROPERTY_MSG_CREATION_TITLE);
        ResourceBundle siteResource = ResourceBundle.getBundle(SITE_BUNDLE_NAME);
        String soere = siteResource.getString(PARAM_MSG_ORE_FULL_NAME);
        String message = String.format(resourceBundle.getString(DefaultUsersManager.PROPERTY_MSG_NEW_USER_MAIL_CONTENT),
                utilisateur.getPrenom(),
                utilisateur.getNom(),
                utilisateur.getLogin(),
                soere,
                baseUrl,
                Boolean.TRUE.toString(),
                utilisateur.getLogin()
        );
        mailSender.sendMail(from, utilisateur.getEmail(), otherRecipients, subject, message);
    }

    /**
     *
     * @return
     */
    @Override
    public String getMailAdmin() {
        return configuration.getMailAdmin();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.identification.IUsersManager#checkPassword(java.lang.String, java.lang.String)
     */
    /**
     *
     * @param login
     * @param password
     * @return
     * @throws BusinessException
     */
    @Override
    @Transactional(readOnly = true)
    public Utilisateur checkPassword(final String login, final String password) throws BusinessException {
        Utilisateur utilisateur = null;

        try {
            utilisateur = utilisateurDAO.getByLogin(login).orElseThrow(() -> new UnknownUserException(DefaultUsersManager.MSG_UNKNOWN_USER));
            if (!toMD5(password).equals(utilisateur.getPassword())) {
                throw new BadPasswordException(DefaultUsersManager.MSG_BAD_PASSWORD);
            } else if (!utilisateur.getActive()) {
                throw new InactiveUserException(DefaultUsersManager.MSG_INACTIVE_USER, utilisateur);
            }

        } catch (NoSuchAlgorithmException ex) {
            LoggerFactory.getLogger(DefaultUsersManager.class.getName()).error(ex.getMessage(), ex);
        }
        return utilisateur;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.identification.IUsersManager#deleteUserProfile(utilisateur)
     */
    /**
     * *      * Delete user profile.
     *      * If user is bind to other object <ol><li>eitheir these objects are deleted
     * </li><li>either their user is replaced by an anonymous user.</li></ol>
     *
     * @param utilisateur
     * @throws BusinessException
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public InfosReport deleteUserProfile(Utilisateur utilisateur) throws BusinessException {
        if (utilisateur == null) {
            throw new BusinessException("User can't be null");
        }

        String initialReport = String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_INITIAL_MESSAGE_FOR_USER_DELETE), utilisateur.getLogin());
        final InfosReport infosReport = new InfosReport(initialReport);
        String initialErrorReport = String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_INITIAL_ERROR_MESSAGE_FOR_USER_DELETE), utilisateur.getLogin());
        ErrorsReport errorsReport = new ErrorsReport(initialErrorReport);

        if (utilisateur.getIsRoot() == true) {

            String adminMessage = "";
            adminMessage = String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_FOR_ADMINISTRATOR_DELETE), utilisateur.getLogin());
            infosReport.addInfos("Administrator_Deletion", adminMessage);
            return infosReport;

        }

        try {

            usersDeletionManagers.entrySet().stream()
                    .forEach((e) -> {
                        try {
                            String newInfo = e.getValue().deleteDependancesUserProfile(utilisateur).getinfoMessages()
                                    .values().stream()
                                    .filter(l -> !l.isEmpty())
                                    .map(l -> l.get(0))
                                    .findFirst()
                                    .orElse("");
                            infosReport.addInfos(e.getKey(), newInfo);
                        } catch (DeleteUserException due) {
                            errorsReport.addException(due);
                        }
                    });

            if (!errorsReport.hasErrors()) {
                String successUserDeletionMessage = removeUserAndAssociatedGroups(utilisateur);
                infosReport.addInfos("Success_User_Deletion", successUserDeletionMessage);
            } else {
                infosReport.getinfoMessages().clear();
                infosReport.addInfos("Error_User_Deletion", errorsReport.getMessages());
            }

        } catch (DeleteUserException due) {
            infosReport.getinfoMessages().clear();
            infosReport.addInfos("Error_User_Deletion", due.getMessage());

            Notification notification = new Notification(
                    Notification.INFO,
                    String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_ERROR_MSG_FOR_USER_DELETION), utilisateur.getLogin()),
                    infosReport.buildHTMLMessages());
            notificationsManager.addNotification(notification, policyManager.getCurrentUserLogin());
            return infosReport;
        }
        notifyUserDeletion(utilisateur, infosReport);
        return infosReport;
    }

    protected void notifyUserDeletion(Utilisateur utilisateur, final InfosReport infosReport) throws BusinessException {
        if (!policyManager.getCurrentUserLogin().equals(utilisateur.getLogin())) {
            Notification notification = new Notification(
                    Notification.INFO,
                    String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_SUCCESS_MSG_FOR_USER_DELETION), utilisateur.getLogin()),
                    infosReport.buildHTMLMessages());
            notificationsManager.addNotification(notification, policyManager.getCurrentUserLogin());
        
        } else {
            String hostName = "Ecoinformatique-ORE";
            try {
                hostName = InetAddress.getLocalHost().getCanonicalHostName();
            } catch (UnknownHostException ex) {
                LoggerFactory.getLogger(this.getClass()).error(ERROR_MSG_CANT_GET_HOST_ADRESS);
            }
            String subject = String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_SUCCESS_MSG_MAIL_SUBJECT_FOR_USER_DELETION), hostName);
            mailSender.sendMail(policyManager.getCurrentUser().getEmail(), utilisateur.getEmail(), subject, infosReport.buildHTMLMessages());
            policyManager.releaseContext();
            ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true)).invalidate();
        }
        USER_DELETION_LOGGER.info(infosReport.buildHTMLMessages());
    }

    /**
     *
     * @param utilisateur
     * @return successUserDeletionMessage
     * @throws DeleteUserException
     */
    private String removeUserAndAssociatedGroups(Utilisateur utilisateur) throws DeleteUserException {
        try {
            utilisateurDAO.remove(utilisateur);

        } catch (PersistenceException ex) {
            throw new DeleteUserException(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_ERROR_MSG_FOR_USER_DELETION), utilisateur.getLogin()));
        }
        String successUserDeletionMessage = String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_SUCCESS_MSG_FOR_USER_DELETION), utilisateur.getLogin());
        return successUserDeletionMessage;
    }

    /**
     *
     * @param usersDeletionManagers
     *
     */
    public void setUsersDeletionManagers(Map<String, IUsersDeletion> usersDeletionManagers) {
        this.usersDeletionManagers = usersDeletionManagers;
    }


    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.identification.IUsersManager#getUtilisateurByLogin(java.lang.String)
     */
    /**
     *
     * @param login
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Utilisateur> getUtilisateurByLogin(final String login) {
        return utilisateurDAO.getByLogin(login);
    }

    /**
     *
     * @param email
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public Utilisateur getUtilisateurByEmail(final String email) {
        Utilisateur utilisateur = null;
        utilisateur = utilisateurDAO.getByEMail(email).orElse(null);
        return utilisateur;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.identification.IUsersManager#initialise(org.inra.ecoinfo.identification.entity.Utilisateur)
     */
    /**
     *
     * @param utilisateur
     * @throws BusinessException
     */
    @Override
    public void initialise(Utilisateur utilisateur) throws BusinessException {
        final Utilisateur dbUtilisateur = utilisateur;
        if (dbUtilisateur == null) {
            return;
        }
        dbUtilisateur.setPassword(dbUtilisateur.getPassword());
        updateUserProfile(dbUtilisateur);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.identification.IUsersManager#resetPassword(org.inra.ecoinfo.identification.entity.Utilisateur, java.lang.String, java.util.Locale)
     */
    /**
     *
     * @param utilisateur
     * @param baseUrl
     * @param locale
     * @throws BusinessException
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void resetPassword(final Utilisateur utilisateur, final String baseUrl, Locale locale) throws BusinessException {
        Utilisateur returnUtilisateur = utilisateur;
        final ResourceBundle resourceBundle = ResourceBundle.getBundle(DefaultUsersManager.BUNDLE_NAME, locale == null ? new Locale(Localization.getDefaultLocalisation()) : locale);
        initialise(returnUtilisateur);
        try {
            returnUtilisateur = utilisateurDAO.getById(returnUtilisateur.getId()).orElseThrow(() -> new PersistenceException("no user"));
        } catch (final PersistenceException e1) {
            UncatchedExceptionLogger.log("no user", e1);
        }
        final String from = getMailAdmin();
        final String subject = resourceBundle.getString(DefaultUsersManager.PROPERTY_MSG_INITIALISATION_TITLE);
        String message;
        try {
            message = String.format(resourceBundle.getString(DefaultUsersManager.PROPERTY_MSG_NEW_PASSWORD_MAIL_CONTENT), 
                    returnUtilisateur.getPrenom(), returnUtilisateur.getNom(), 
                    returnUtilisateur.getLogin(), baseUrl, Boolean.TRUE.toString(),
                    returnUtilisateur.getEmail(), URLEncoder.encode(returnUtilisateur.getPassword(), "UTF-8"));
        } catch (final UnsupportedEncodingException e) {
            throw new BusinessException("Erreur d'encodage du mail", e);
        }
        mailSender.sendMail(from, returnUtilisateur.getEmail(), subject, message);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.identification.IUsersManager#retrieveAllUsers()
     */
    /**
     *
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<Utilisateur> retrieveAllUsers() {
        return utilisateurDAO.getAll()
                .stream()
                .filter(u -> !"public".equals(u.getLogin()) && !configuration.getAnonymousLogin().equals(u.getLogin()))
                .collect(Collectors.toList());
    }

    /**
     * Sets the void configuration.
     *
     * @param configuration the ICoreConfiguration configuration
     */
    public void setConfiguration(final ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @return
     */
    public ICoreConfiguration getConfiguration() {
        return configuration;
    }

    /**
     * Sets the void mail sender.
     *
     * @param mailSender the IMailSender mail sender
     */
    public void setMailSender(final IMailSender mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * Sets the void utilisateur dao.
     *
     * @param utilisateurDAO the IUtilisateurDAO utilisateur dao
     */
    public void setUtilisateurDAO(final IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.identification.IUsersManager#updateUserProfile(org.inra.ecoinfo.identification.entity.Utilisateur)
     */
    /**
     *
     * @param utilisateur
     * @throws BusinessException
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateUserProfile(Utilisateur utilisateur) throws BusinessException {
        try {
            final Utilisateur utilisateurDB = utilisateurDAO.getById(utilisateur.getId()).orElseThrow(() -> new PersistenceException("no user"));
            utilisateurDB.setEmail(utilisateur.getEmail());
            utilisateurDB.setNom(utilisateur.getNom());
            if(!utilisateurDB.getLogin().equals(utilisateur.getLogin())) {
                Stream.of(WhichTree.values())
                        .map(wt -> utilisateurDB.getOwnGroup(wt))
                        .forEach(g -> g.setGroupName(utilisateur.getLogin()));
            }
            utilisateurDB.setLogin(utilisateur.getLogin());
            utilisateurDB.setPrenom(utilisateur.getPrenom());
            utilisateurDB.setIsRoot(utilisateur.getIsRoot());
            utilisateurDB.setEmploi(utilisateur.getEmploi());
            utilisateurDB.setPoste(utilisateur.getPoste());
            if (StringUtils.isNotBlank(utilisateur.getPassword())) {
                if (utilisateur.getPassword() != null) {
                    utilisateurDB.setPassword(toMD5(utilisateur.getPassword()));
                } else {
                    utilisateurDB.setPassword(utilisateur.getPassword());
                }
            }
            utilisateurDAO.saveOrUpdate(utilisateurDB);
        } catch (final PersistenceException e) {
            throw new BusinessException("can't save user", e);
        } catch (NoSuchAlgorithmException ex) {
            LoggerFactory.getLogger(DefaultUsersManager.class.getName()).error(ex.getMessage(), ex);
        }
    }

    /**
     *
     * @param login
     * @throws BusinessException
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = BusinessException.class)
    public void activateUser(String login) throws BusinessException {
        Utilisateur utilisateur = null;
        try {
            utilisateur = utilisateurDAO.getByLogin(login).orElseThrow(() -> new PersistenceException(DefaultUsersManager.MSG_UNKNOWN_USER));
            if (utilisateur == null) {
                throw new BusinessException(new UnknownUserException(DefaultUsersManager.MSG_UNKNOWN_USER));
            }
            utilisateur.setActive(true);
            utilisateurDAO.saveOrUpdate(utilisateur);
        } catch (final PersistenceException e) {
            throw new BusinessException(MSG_UNKNOWN_USER, e);
        }

    }

}
