package org.inra.ecoinfo.identification;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.middleware.ICompositeGroupDAO;

/**
 * The Interface IUtilisateurDAO.
 */
public interface IUtilisateurDAO extends IDAO<Utilisateur>, ICompositeGroupDAO {

    /**
     * Gets the Utilisateur by id.
     *
     * @param id the Long id
     * @return the Utilisateur by id
     */
    Optional<Utilisateur> getById(Long id);

    /**
     * Gets the Utilisateur by login.
     *
     * @param login the String login
     * @return the Utilisateur by login
     */
    Optional<Utilisateur> getByLogin(String login);

    /**
     *
     * @param email
     * @return
     */
    Optional<Utilisateur> getByEMail(String email);

    /**
     * Gets the Utilisateur by login and password.
     *
     * @param login the String login
     * @param password the String password
     * @return the Utilisateur by login and password
     */
    Optional<Utilisateur> getByLoginAndPassword(String login, String password);

    /**
     * Gets the Administrator list
     *
     * @return the List of Utilisateur
     */
    List<Utilisateur> getAdmins();

    /**
     * return all users
     *
     * @return the List of Utilisateur
     */
    List<Utilisateur> getAll();

    /**
     * Checks for user.
     *
     * @return true, if successful
     */
    boolean hasUser();
    
    /**
     *
     * @param group
     */
    void saveOrUpdate(Group group);
}
