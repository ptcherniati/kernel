package org.inra.ecoinfo.ws.utils;

/**
 *
 * @author yahiaoui
 */
import java.time.LocalDateTime;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author ptcherniati
 */
public class HourAdapter extends XmlAdapter<String, LocalDateTime> {

    /**
     *
     * @param v
     * @return
     * @throws Exception
     */
    @Override
    public String marshal(LocalDateTime v) throws Exception {
        return DateUtil.getUTCDateTextFromLocalDateTime(v, DateUtil.HH_MM_SS);
    }

    /**
     *
     * @param v
     * @return
     * @throws Exception
     */
    @Override
    public LocalDateTime unmarshal(String v) throws Exception {
        return DateUtil.readLocalDateTimeFromText(v, DateUtil.HH_MM_SS);
    }

}
