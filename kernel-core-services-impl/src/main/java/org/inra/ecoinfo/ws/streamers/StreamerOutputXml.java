package org.inra.ecoinfo.ws.streamers;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.inra.ecoinfo.ws.exceptions.BusinessException;
import org.inra.ecoinfo.ws.manager.EmptyPojo;
import org.inra.ecoinfo.ws.manager.IResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yahiaoui
 */
public class StreamerOutputXml extends Streamer implements StreamingOutput, IStreamer {

    private static final Logger LOGGER = LoggerFactory.getLogger(StreamerOutputXml.class.getName());

    /**
     *
     */
    public StreamerOutputXml() {
    }

    /**
     *
     * @param output
     * @throws IOException
     */
    @Override
    public void write(OutputStream output) throws IOException {

        Writer writer = new BufferedWriter(
                new OutputStreamWriter(output, "UTF8"));

        /* Executino des Threads Producer */
        producerScheduler();

        JAXBContext jc = null;
        Marshaller marshaller = null;

        try {
            jc = JAXBContext.newInstance(dto, EmptyPojo.class);
            marshaller = jc.createMarshaller();
            marshaller.setProperty("com.sun.xml.bind.xmlHeaders", "");
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        } catch (JAXBException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        ByteArrayOutputStream baoStream = new ByteArrayOutputStream();

        try {
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            writer.write("\n<Data>");

            writer.flush();

            while (!isFinishedProcess || !dtos.isEmpty()) {
                int iteration = 0;
                while (!(marshaller==null || dtos.isEmpty())) {
                    marshaller.marshal(dtos.poll(), baoStream);
                    writer.write(baoStream.toString("UTF8"));
                    baoStream.reset();
                    iteration++;
                    if (iteration % LOOPFLUSH == 0) {
                        writer.flush();
                        iteration = 0;
                    }
                }
                writer.flush();
            }

            writer.write("\n</Data>");
            writer.write("\n");
            writer.close();
            baoStream.close();
        } catch (IOException | JAXBException ex) {
            if (ex.getClass().getName().endsWith(".ClientAbortException")) {
                try {
                    writer.close();
                    baoStream.close();
                    throw new BusinessException("ClientAbortException !! " + ex.getMessage(), ex);
                } catch (IOException | BusinessException ex1) {
                    LOGGER.error(ex1.getMessage(), ex1);
                }
            } else {
                try {
                    writer.close();
                    baoStream.close();
                    throw new BusinessException(" Exception : " + ex.getMessage());
                } catch (IOException | BusinessException ex1) {
                    LOGGER.error(ex1.getMessage(), ex1);
                }
            }
            isFinishedProcess = true;
        }
    }

    /**
     *
     * @return
     */
    public IResource getResource() {
        return resource;
    }

    /**
     *
     * @param resource
     */
    public void setResource(IResource resource) {
        this.resource = resource;
    }

    /**
     *
     * @param dto
     */
    public void setDto(Class dto) {
        this.dto = dto;
    }

    /**
     *
     * @return
     */
    public long getMemoryUsage() {

        int mb = 1_024 * 1_024;

        Runtime runtime = Runtime.getRuntime();

        return (runtime.totalMemory() - runtime.freeMemory()) / mb;
    }

    /**
     *
     * @param iStreamerConfigurator
     */
    @Override
    public void setStreamerConfigurator(IStreamerConfigurator iStreamerConfigurator) {
        streamerConfigurator = iStreamerConfigurator;
    }
}
