package org.inra.ecoinfo.ws.manager;

import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author yahiaoui
 * @param <T>
 */
public interface IResource<T extends IDto> {

    /**
     *
     * @param session
     * @param indexRequest
     * @param limit
     * @return
     */
    List<T> getDtoIterable(Session session, int indexRequest, int limit);

    /**
     *
     * @param limit
     */
    void initResource(int limit);

    /**
     *
     * @return
     */
    boolean isFinished();

    /**
     *
     * @param offset
     */
    void setOffset(int offset);

}
