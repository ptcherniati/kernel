package org.inra.ecoinfo.ws.streamers;

/**
 *
 * @author yahiaoui
 */
public class DefaultStreamerConfigurator implements IStreamerConfigurator {

    /**
     * Taille de l'extraction *
     */
    private int recorderLenght = 16_000;
    /**
     * Ratio de l'extraction *
     */
    private int ratio = 4;
    /**
     * Nbr Threads *
     */
    private int nbrCores = 2;

    /**
     *
     * @param nbrCores
     * @param recorderLenght
     * @param ratio
     */
    public DefaultStreamerConfigurator(int nbrCores, int recorderLenght,
            int ratio) {
        this.nbrCores = nbrCores;
        this.recorderLenght = recorderLenght;
        this.ratio = ratio;
    }

    /**
     *
     */
    public DefaultStreamerConfigurator() {
    }

    /**
     *
     * @return
     */
    @Override
    public int getRecorderLenght() {
        return recorderLenght;
    }

    /**
     *
     * @param recorderLenght
     */
    @Override
    public void setRecorderLenght(int recorderLenght) {
        this.recorderLenght = recorderLenght;
    }

    /**
     *
     * @return
     */
    @Override
    public int getRatio() {
        return ratio;
    }

    /**
     *
     * @param ratio
     */
    @Override
    public void setRatio(int ratio) {
        this.ratio = ratio;
    }

    /**
     *
     * @return
     */
    @Override
    public int getNbrCores() {
        return nbrCores;
    }

    /**
     *
     * @param nbrCores
     */
    @Override
    public void setNbrCores(int nbrCores) {
        this.nbrCores = nbrCores;
    }

}
