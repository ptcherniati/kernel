package org.inra.ecoinfo.ws.inspect;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.inra.ecoinfo.identification.ws.subservice.ISubService;
import org.inra.ecoinfo.ws.exceptions.BusinessException;

/**
 *
 * @author yahiaoui
 */
public interface IService extends ISubService {

    /**
     *
     * @param value
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    Response inspectAllServicesXML(UriInfo value) throws BusinessException;

    /**
     *
     * @param value
     * @param serviceName
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    Response inspectSpecificServiceXML(UriInfo value, String serviceName)
            throws BusinessException;

    /**
     *
     * @param value
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    Response inspectAllServicesJSON(UriInfo value) throws BusinessException;

    /**
     *
     * @param value
     * @param serviceName
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    Response inspectSpecificServiceJSON(UriInfo value, String serviceName)
            throws BusinessException;

    /**
     *
     * @param value
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    Response inspectAllServicesEncryptedXML(UriInfo value) throws BusinessException;

    /**
     *
     * @param value
     * @param serviceName
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    Response inspectSpecificServiceEncryptedXML(UriInfo value, String serviceName)
            throws BusinessException;

    /**
     *
     * @param value
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    Response inspectAllServicesEncryptedJSON(UriInfo value) throws BusinessException;

    /**
     *
     * @param value
     * @param serviceName
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    Response inspectSpecificServiceEncryptedJSON(UriInfo value, String serviceName)
            throws BusinessException;
}
