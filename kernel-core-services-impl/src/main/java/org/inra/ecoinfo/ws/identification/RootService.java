package org.inra.ecoinfo.ws.identification;

import java.security.NoSuchAlgorithmException;
import java.util.Map;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.identification.ws.subservice.ISubService;
import org.inra.ecoinfo.ws.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST Web Service
 *
 * @author yahiaoui
 */
@Path(RootService.PATH_RESOURCE)
public class RootService extends MO implements IRootService {


    /**
     *
     */
    public static final String LOGIN = "{login}";

    /**
     *
     */
    public static final String PASSWORD = "{password}";

    /**
     *
     */
    public static final String SIGNATURE = "{signature}";

    /**
     *
     */
    public static final String TIMESTAMP = "{timeStamp}";

    /**
     *
     */
    public static final String SERVICENAME = "{_service_Name_}";

    /**
     *
     */
    public static final String SERVICENAME_P = "_service_Name_";

    /**
     *
     */
    public static final String PATH_RESOURCE = "/resources";

    /**
     *
     */
    public static final String separator = "/";
    Logger LOGGER = LoggerFactory.getLogger(RootService.class);
    private Map<String, ISubService> mapSubServices;
    /**
     * The EndPoint Checker Signature.
     */
    private EndPointChecker endPointChecker;

    /**
     *
     */
    public RootService() {
    }

    /**
     *
     * @param login
     * @param password
     * @param uri
     * @return
     * @throws BusinessException
     */
    @Path(LOGIN + separator + PASSWORD)
    @Produces(MediaType.TEXT_PLAIN)
    public Response getSignature(
            @PathParam("login") final String login,
            @PathParam("password") final String password,
            @Context UriInfo uri)
            throws BusinessException {
        final Response response;
        try {
            throw new BusinessException(endPointChecker.getSignatureFromLoginAndPassword(uri, login, password));
        } catch (org.inra.ecoinfo.utils.exceptions.BusinessException | NoSuchAlgorithmException ex) {
            throw new BusinessException(ex.getMessage(), ex);
        }
    }

    /**
     *
     * @param login
     * @param signature
     * @param timeStamp
     * @param codeService
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    @Path(LOGIN + separator + SIGNATURE + separator + TIMESTAMP + separator + SERVICENAME)
    public Object subResourceLocators(
            @PathParam("login") final String login,
            @PathParam("signature") final String signature,
            @PathParam("timeStamp") final String timeStamp,
            @PathParam(SERVICENAME_P) final String codeService)
            throws BusinessException {

        LOGGER.debug(String.format("login:%s; service %s", login, codeService));
        if (endPointChecker.checkIntegrity(login, timeStamp, signature)) {

            if (mapSubServices.get(codeService) != null) {
                return mapSubServices.get(codeService);
            } else {
                throw new BusinessException("Unavailable Service");
            }
        } else {
            throw new BusinessException("Unauthorized Resource");
        }
    }

    /**
     *
     * @param login
     * @param signature
     * @param timeStamp
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    @Override
    @Path(LOGIN + separator + SIGNATURE + separator + TIMESTAMP)
    public Object authenticationCheck(
            @PathParam("login") final String login,
            @PathParam("signature") final String signature,
            @PathParam("timeStamp") final String timeStamp)
            throws BusinessException {
        LOGGER.debug(String.format("login:%s; no service", login));

        if (endPointChecker.checkIntegrity(login, timeStamp, signature)) {
            throw new BusinessException("OK_Authorization");
        }
        throw new BusinessException("KO_Authorization");
    }

    /**
     *
     * @param endPointChecker
     */
    public void setEndPointChecker(EndPointChecker endPointChecker) {
        this.endPointChecker = endPointChecker;
    }

    /**
     *
     * @return
     */
    public EndPointChecker getEndPointChecker() {
        return endPointChecker;
    }

    /**
     *
     * @return
     */
    public Map<String, ISubService> getMapSubServices() {
        return mapSubServices;
    }

    /**
     *
     * @param mapSubServices
     */
    public void setMapSubServices(Map<String, ISubService> mapSubServices) {
        this.mapSubServices = mapSubServices;
    }

}
