package org.inra.ecoinfo.ws.crypto;

/**
 *
 * @author yahiaoui
 */
import com.Ostermiller.util.Base64;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class Encryptor {

    private static final String CRYPTO_MODE = "AES/CBC/PKCS5Padding";
    private static final Logger ROOT_LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    /**
     *
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String generateKeyString(String password) throws NoSuchAlgorithmException {
        return Digestor.digestSha1(password).substring(0, 16);
    }

    /**
     * key 128 Bits
     *
     * @param password
     * @param message
     * @return
     */
    public static String aes128ECBEncrypt(String password, String message) {
        try {
            String key = generateKeyString(password);
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);

            return new String(Base64.encode(cipher.doFinal((message).getBytes(StandardCharsets.UTF_8))));

        } catch (InvalidKeyException | NoSuchAlgorithmException
                | BadPaddingException | IllegalBlockSizeException
                | NoSuchPaddingException ex) {
            ROOT_LOGGER.error(ex.getMessage(), ex);
            return null;
        }
    }

    /**
     * Key 128 Bits
     *
     * @param password
     * @param encryptedMessage
     * @return
     */
    public static String aes128ECBDecrypt(String password, String encryptedMessage) {
        try {
            String key = generateKeyString(password);
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);

            return new String(cipher.doFinal(Base64.decode(
                    encryptedMessage.getBytes(StandardCharsets.UTF_8))));

        } catch (InvalidKeyException | NoSuchAlgorithmException
                | BadPaddingException | IllegalBlockSizeException
                | NoSuchPaddingException ex) {
            ROOT_LOGGER.error(ex.getMessage(), ex);
            return null;
        }
    }

    /**
     * Key 128 Bits
     *
     * @param password
     * @param message
     * @return
     */
    public static String aes128CBC7Encrypt(String password, String message) {
        try {
            String key = generateKeyString(password);
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");

            byte[] ivBytes = new byte[]{
                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};

            IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivSpec);

            return new String(Base64.encode(cipher.doFinal((message).getBytes(StandardCharsets.UTF_8))));

        } catch (NoSuchAlgorithmException | NoSuchPaddingException
                | InvalidKeyException | InvalidAlgorithmParameterException
                | IllegalBlockSizeException | BadPaddingException ex) {
            ROOT_LOGGER.error(ex.getMessage(), ex);
            return null;
        }
    }

    /**
     * Key 128 Bits
     *
     * @param password
     * @param encryptedMessage
     * @return
     */
    public static String aes128CBC7Decrypt(String password, String encryptedMessage) {
        try {
            String key = generateKeyString(password);
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
            byte[] ivBytes = new byte[]{
                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};

            IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivSpec);

            return new String(cipher.doFinal(Base64.decode(
                    encryptedMessage.getBytes(StandardCharsets.UTF_8))));

        } catch (NoSuchAlgorithmException | NoSuchPaddingException
                | InvalidKeyException | InvalidAlgorithmParameterException
                | IllegalBlockSizeException | BadPaddingException ex) {
            ROOT_LOGGER.error(ex.getMessage(), ex);
            return null;
        }
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        String md5DigestedPassAdmin = "21232f297a57a5a743894a0e4a801fc3";
        String md5DigestedPasspfSol = "1df35065040391bd18b53efd7e9da2d1";
        StringBuilder stringBuilder;
        try (BufferedReader br = new BufferedReader(new FileReader("/home/yahiaoui/Bureau/toto.txt"))) {
            String line;
            stringBuilder = new StringBuilder();
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
            }
            String aes128Decrypt = aes128CBC7Decrypt(md5DigestedPassAdmin, stringBuilder.toString());
            ROOT_LOGGER.info(aes128Decrypt);
        } catch (IOException ex) {
            ROOT_LOGGER.error(ex.getMessage(), ex);
        }
    }

    private Cipher cipher;
    private IvParameterSpec ivSpec;
    private byte[] ivBytes;
    private SecretKeySpec secretKeySpec;

    /**
     *
     * @param string
     * @param password
     */
    public Encryptor(String password) {
        try {
            ivBytes = new byte[]{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
            secretKeySpec = new SecretKeySpec(generateKeyString(password).getBytes(StandardCharsets.UTF_8), "AES");
            ivSpec = new IvParameterSpec(ivBytes);
        } catch (NoSuchAlgorithmException ex) {
            LoggerFactory.getLogger(Encryptor.class.getName()).error(ex.getMessage(), ex);
        }
    }

    /**
     *
     */
    public void initEncryptMode() {
        try {
            cipher = Cipher.getInstance(CRYPTO_MODE);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivSpec);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException
                | NoSuchAlgorithmException | NoSuchPaddingException ex) {
            LoggerFactory.getLogger(Encryptor.class.getName()).error(ex.getMessage(), ex);
        }
    }

    /**
     *
     */
    public void initDecryptMode() {
        try {
            cipher = Cipher.getInstance(CRYPTO_MODE);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivSpec);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException
                | NoSuchAlgorithmException | NoSuchPaddingException ex) {
            LoggerFactory.getLogger(Encryptor.class.getName()).error(ex.getMessage(), ex);
        }
    }

    /**
     *
     * @param toEncrypt
     * @param co
     * @return
     */
    public byte[] aes128CBC7Encrypt(byte[] toEncrypt, CipherOperation co) {
        try {
            if (co.equals(CipherOperation.dofinal)) {
                return cipher.doFinal(toEncrypt);
            } else {
                return cipher.update(toEncrypt);
            }
        } catch (BadPaddingException | IllegalBlockSizeException ex) {
            ROOT_LOGGER.error(ex.getMessage(), ex);
            return null;
        }
    }

    /**
     *
     * @param encryptedMessage
     * @param co
     * @return
     */
    public byte[] aes128CBC7Decrypt(byte[] encryptedMessage, CipherOperation co) {
        try {
            if (co.equals(CipherOperation.dofinal)) {
                return cipher.doFinal(encryptedMessage);
            } else {
                return cipher.update(encryptedMessage);
            }
        } catch (BadPaddingException | IllegalBlockSizeException ex) {
            ROOT_LOGGER.error(ex.getMessage(), ex);
            return null;
        }
    }

    /**
     *
     * @return
     */
    public Cipher getCipher() {
        return cipher;
    }

    /**
     *
     * @param cipher
     */
    public void setCipher(Cipher cipher) {
        this.cipher = cipher;
    }

    /**
     *
     * @return
     */
    public IvParameterSpec getIvSpec() {
        return ivSpec;
    }

    /**
     *
     * @param ivSpec
     */
    public void setIvSpec(IvParameterSpec ivSpec) {
        this.ivSpec = ivSpec;
    }

}
