package org.inra.ecoinfo.ws.crypto;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yahiaoui
 */
public class Digestor {
    /**
     *
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String digestMD5(final String password) throws NoSuchAlgorithmException{
        
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] thedigest = md.digest(password.getBytes(StandardCharsets.UTF_8));
        BigInteger bI = new BigInteger(1, thedigest);
        return bI.toString(16);
    }

    /**
     *
     * @param message
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String digestSha1(String message) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] hash = md.digest(message.getBytes(StandardCharsets.UTF_8));
        BigInteger bI = new BigInteger(1, hash);

        return bI.toString(16);
    }

    /**
     *
     * @param login
     * @param password
     * @param timeStamp
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String generateSignature(String login, String password, String timeStamp) throws NoSuchAlgorithmException {
        return digestSha1(login + password + timeStamp);
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        String ss = "pfsol001!";
        try {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).info("{}", digestMD5(ss));
        } catch (NoSuchAlgorithmException ex) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error(ex.getMessage(), ex);
        }
    }

    private Digestor() {
        
    }
}
