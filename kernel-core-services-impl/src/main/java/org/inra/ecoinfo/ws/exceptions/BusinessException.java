package org.inra.ecoinfo.ws.exceptions;

/**
 *
 * @author yahiaoui
 */
public class BusinessException extends Exception {

    /**
     *
     * @param string
     * @param message
     */
    public BusinessException(String message) {
        super(message);
    }

    /**
     *
     * @param message
     * @param thrwbl
     * @param cause
     */
    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

}
