package org.inra.ecoinfo.ws.streamers;

/**
 *
 * @author yahiaoui
 */
public interface IStreamerConfigurator {

    /**
     *
     * @return
     */
    int getRecorderLenght();

    /**
     *
     * @return
     */
    int getRatio();

    /**
     *
     * @return
     */
    int getNbrCores();

    /**
     *
     * @param recorderLenght
     */
    void setRecorderLenght(int recorderLenght);

    /**
     *
     * @param ratio
     */
    void setRatio(int ratio);

    /**
     *
     * @param cores
     */
    void setNbrCores(int cores);

}
