package org.inra.ecoinfo.ws.streamers;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.inra.ecoinfo.ws.exceptions.BusinessException;
import org.inra.ecoinfo.ws.manager.EmptyPojo;
import org.inra.ecoinfo.ws.manager.IResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yahiaoui
 */
public class StreamerOutputJson extends Streamer implements StreamingOutput, IStreamer {

    private static final Logger LOGGER = LoggerFactory.getLogger(StreamerOutputJson.class.getName());

    /**
     *
     */
    public StreamerOutputJson() {
    }

    /**
     *
     * @param output
     * @throws IOException
     */
    @Override
public void write(OutputStream output) throws IOException {
    Writer writer = new BufferedWriter(
            new OutputStreamWriter(output, "UTF8"));
    /* Executino des Threads Producer */
    producerScheduler();
    Marshaller marshaller = null;
    ByteArrayOutputStream baoStream = new ByteArrayOutputStream();
    try {
        marshaller = trySetProperties(marshaller);
        
        while (!isFinishedProcess || !dtos.isEmpty()) {
            int iteration = 0;
            
            while (!dtos.isEmpty()) {
                
                marshaller.marshal(dtos.poll(), baoStream);
                writer.write(baoStream.toString("UTF8"));
                baoStream.reset();
                iteration++;
                if (iteration % LOOPFLUSH == 0) {
                    writer.flush();
                }
            }
            writer.flush();
        }
        writer.close();
        baoStream.close();
    } catch (JAXBException | IOException ex) {
        if (ex.getClass().getName().endsWith(".ClientAbortException")) {
            try {
                writer.close();
                baoStream.close();
                throw new BusinessException("ClientAbortException !! " + ex.getMessage(), ex);
            } catch (IOException | BusinessException ex1) {
                LOGGER.info(ex1.getMessage(), ex1);
            }
        } else {
            try {
                writer.close();
                baoStream.close();
                throw new BusinessException("Exception : " + ex.getMessage(), ex);
            } catch (IOException | BusinessException ex1) {
                LOGGER.info(ex1.getMessage(), ex1);
            }
        }
        isFinishedProcess = true;
    }
}

/**
 *
 * @return
 */
public IResource getResource() {
        return resource;
    }

    /**
     *
     * @param resource
     */
    public void setResource(IResource resource) {
        this.resource = resource;
    }

    /**
     *
     * @param dto
     */
    public void setDto(Class dto) {
        this.dto = dto;
    }

    /**
     *
     * @param iStreamerConfigurator
     */
    @Override
    public void setStreamerConfigurator(IStreamerConfigurator iStreamerConfigurator) {
        streamerConfigurator = iStreamerConfigurator;
    }

    private Marshaller trySetProperties(Marshaller marshaller) {
        Marshaller localMarshaller= marshaller;
        JAXBContext jc;
        try {
            jc = JAXBContext.newInstance(dto, EmptyPojo.class);
            localMarshaller = jc.createMarshaller();
            localMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            localMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, Boolean.FALSE);
            localMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        } catch (JAXBException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return localMarshaller;
    }
}
