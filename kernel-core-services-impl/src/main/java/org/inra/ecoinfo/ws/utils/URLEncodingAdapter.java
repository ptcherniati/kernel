package org.inra.ecoinfo.ws.utils;

/**
 *
 * @author yahiaoui
 */
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author ptcherniati
 */
public class URLEncodingAdapter extends XmlAdapter<String, String> {

    /**
     *
     * @param url
     * @return
     * @throws Exception
     */
    @Override
    public String unmarshal(String url) throws Exception {
        return url;
    }

    /**
     *
     * @param url
     * @return
     * @throws Exception
     */
    @Override
    public String marshal(String url) throws Exception {
        return url.replace("&amp;", "&");
    }

}
