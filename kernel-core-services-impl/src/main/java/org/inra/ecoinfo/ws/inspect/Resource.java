package org.inra.ecoinfo.ws.inspect;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.ws.inspector.core.Inspector;
import org.inra.ecoinfo.ws.manager.IDto;
import org.inra.ecoinfo.ws.manager.IResource;
import org.inra.ecoinfo.ws.manager.Manager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Antoine Schellenberger
 */
public class Resource extends Manager<IDto>
        implements IResource {

    private String uri;
    private String whichService;

    /**
     *
     */
    public Resource() {
        super();
    }

    /**
     *
     * @param session
     * @param indexRequest
     * @param limit
     * @return
     */
    @Override
    public List<IDto> getDtoIterable(Session session, int indexRequest, int limit) {
        finish = true;

        if (StringUtils.isEmpty(whichService)) {
            whichService = null;
        }

        List<IDto> inspect = new ArrayList();
        try {
            inspect = Inspector.inspect(whichService, uri);
        } catch (BusinessException ex) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).info("Exception in Inspect", ex);
        }
        return inspect;
    }

    /**
     * Initialize First Call
     *
     * @param limit
     */
    @Override
    public void initResource(int limit) {
        //not implemented
    }

    /**
     *
     * @param offset
     */
    @Override
    public synchronized void setOffset(int offset) {
        //not implemented
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isFinished() {
        return finish;
    }

    /**
     *
     * @return
     */
    public String getURI() {
        return uri;
    }

    /**
     *
     * @param uri
     */
    public void setURI(String uri) {
        this.uri = uri;
    }

    /**
     *
     * @return
     */
    public String getWhichService() {
        return whichService;
    }

    /**
     *
     * @param whichService
     */
    public void setWhichService(String whichService) {
        this.whichService = whichService;
    }

}
