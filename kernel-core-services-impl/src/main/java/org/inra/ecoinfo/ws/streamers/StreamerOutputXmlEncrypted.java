package org.inra.ecoinfo.ws.streamers;

import com.Ostermiller.util.Base64;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.apache.commons.lang.ArrayUtils;
import org.inra.ecoinfo.ws.crypto.CipherOperation;
import org.inra.ecoinfo.ws.crypto.Encryptor;
import org.inra.ecoinfo.ws.exceptions.BusinessException;
import org.inra.ecoinfo.ws.identification.EndPointChecker;
import org.inra.ecoinfo.ws.manager.EmptyPojo;
import org.inra.ecoinfo.ws.manager.IResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yahiaoui
 */
public class StreamerOutputXmlEncrypted extends Streamer implements StreamingOutput, IStreamer {

    private static final Logger LOGGER = LoggerFactory.getLogger(StreamerOutputXml.class.getName());

    /**
     *
     */
    public StreamerOutputXmlEncrypted() {
    }

    /**
     *
     * @param output
     * @throws IOException
     */
    @Override
    public void write(OutputStream output) throws IOException {

        Encryptor encryptor = new Encryptor(EndPointChecker.th.get());

        encryptor.initEncryptMode();
        Marshaller marshaller = null;
        Writer writer = new BufferedWriter(
                new OutputStreamWriter(output, "UTF8"));
        ByteArrayOutputStream baoStream = new ByteArrayOutputStream();
        Queue<Byte> qeueBytes = new LinkedList<>();
        StringBuilder plainTextBuilder = new StringBuilder();
        ByteArrayOutputStream outString = new ByteArrayOutputStream();
        int nbrBlocks = 0;

        /* Execution des Threads Producer */
        producerScheduler();

        try {
            marshaller = trySetProperties(marshaller);

            baoStream.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>".getBytes(StandardCharsets.UTF_8));
            baoStream.write("\n<Data>".getBytes(StandardCharsets.UTF_8));

            while (!isFinishedProcess || !dtos.isEmpty()) {
                int iteration = 0;

                while (!dtos.isEmpty()) {

                    marshaller.marshal(dtos.poll(), baoStream);
                    plainTextBuilder.append(baoStream.toString());

                    baoStream.reset();
                    iteration++;
                    if (iteration % LOOPFLUSH == 0) {
                        nbrBlocks = (plainTextBuilder.length() / blockSize);

                        if ((plainTextBuilder.length() % blockSize > 0) && (nbrBlocks >= 1)) {

                            qeueBytes.addAll(Arrays.asList(
                                    ArrayUtils.toObject(encryptor.aes128CBC7Encrypt(
                                                    plainTextBuilder.substring(0, nbrBlocks * blockSize).getBytes(StandardCharsets.UTF_8),
                                                    CipherOperation.update)
                                    )));
                            plainTextBuilder.delete(0, nbrBlocks * blockSize);
                        } else if (nbrBlocks > 1) {
                            qeueBytes.addAll(Arrays.asList(
                                    ArrayUtils.toObject(
                                            encryptor.aes128CBC7Encrypt(
                                                    plainTextBuilder.substring(0, (nbrBlocks - 1) * blockSize).
                                                    getBytes(StandardCharsets.UTF_8), CipherOperation.update))));

                            plainTextBuilder.delete(0, (nbrBlocks - 1) * blockSize);
                        }

                        while ((qeueBytes.size() / 3) >= 1) {
                            outString.write(qeueBytes.poll());
                            outString.write(qeueBytes.poll());
                            outString.write(qeueBytes.poll());
                        }
                        writer.write(new String(Base64.encode(outString.toByteArray())));
                        writer.flush();
                        baoStream.reset();
                        outString.reset();
                        iteration = 0;
                    }
                }
            }

            plainTextBuilder.append(baoStream.toString());
            qeueBytes.addAll(Arrays.asList(
                    ArrayUtils.toObject(encryptor.aes128CBC7Encrypt(
                                    (plainTextBuilder.append("\n</Data>").append("\n")).
                                    toString().getBytes(StandardCharsets.UTF_8), CipherOperation.dofinal))));

            while (!qeueBytes.isEmpty()) {
                outString.write(qeueBytes.poll());
            }

            writer.write(new String(Base64.encode(outString.toByteArray())));

            writer.flush();
            writer.close();

            baoStream.close();
            outString.close();

        } catch (IOException | JAXBException ex) {
            if (ex.getClass().getName().endsWith(".ClientAbortException")) {
                try {
                    writer.close();
                    baoStream.close();
                    throw new BusinessException("ClientAbortException !! " + ex.getMessage(), ex);
                } catch (IOException | BusinessException ex1) {
                    LOGGER.error(ex1.getMessage(), ex1);
                }
            } else {
                try {
                    writer.close();
                    baoStream.close();
                    throw new BusinessException("Exception : " + ex.getMessage(), ex);
                } catch (IOException | BusinessException ex1) {
                    LOGGER.error(ex1.getMessage(), ex1);
                }
            }
            isFinishedProcess = true;
        }
    }

    /**
     *
     * @return
     */
    public IResource getResource() {
        return resource;
    }

    /**
     *
     * @param resource
     */
    public void setResource(IResource resource) {
        this.resource = resource;
    }

    /**
     *
     * @param dto
     */
    public void setDto(Class dto) {
        this.dto = dto;
    }

    /**
     *
     * @param iStreamerConfigurator
     */
    @Override
    public void setStreamerConfigurator(IStreamerConfigurator iStreamerConfigurator) {
        streamerConfigurator = iStreamerConfigurator;
    }

    private Marshaller trySetProperties(Marshaller marshaller) {
        Marshaller localMarshaller= marshaller;
        JAXBContext jc;
        try {
            jc = JAXBContext.newInstance(dto, EmptyPojo.class);
            localMarshaller = jc.createMarshaller();
            localMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            localMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", "");
            localMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
        } catch (JAXBException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return localMarshaller;
    }
}
