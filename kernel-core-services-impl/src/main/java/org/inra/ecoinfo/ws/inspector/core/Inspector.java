package org.inra.ecoinfo.ws.inspector.core;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.ws.identification.RootService;
import org.inra.ecoinfo.ws.inspect.DiscoveredService;
import org.inra.ecoinfo.ws.manager.IDto;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

/**
 *
 * @author yahiaoui
 */
public class Inspector {

    private static String URI;
    private static final String wsRegex = ".ws.";
    private static final String serviceNameRegex = RootService.SERVICENAME;
    private static final String NEXT_PARAMETER_PATTERN = "%s&%s=VALUE";
    private static final String FIRST_PARAMETER_PATTERN = "%s?%s=VALUE";

    /**
     *
     * @param serviceName
     * @param uri
     * @return
     * @throws BusinessException
     */
    public static List< IDto> inspect(String serviceName, String uri)
            throws BusinessException {

        try {
            Inspector.URI = uri;

            List<IDto> discoveredServices = new ArrayList<>();
            List< Method> allMethodsEndPointService = getAllMethods(RootService.class);

            Map< String, String> parameterOfEntryWebServiceMethod = null;
            Map< String, String> annotationsOfEntryWebServiceMethod = null;

            for (Method method : allMethodsEndPointService) {
                annotationsOfEntryWebServiceMethod = getAnnotationsOfMethod(method);
                if (isAvailablePath(annotationsOfEntryWebServiceMethod)) {
                    parameterOfEntryWebServiceMethod = getParametersOfMethod(method);
                    break;
                }
            }
            String endPoint = Optional.ofNullable(annotationsOfEntryWebServiceMethod)
                    .map(aewsm->aewsm.get("Path"))
                    .orElseGet(String::new);
            if (!endPoint.endsWith("/")) {
                endPoint += "/";
            }

            List< Package> webServicesPackages = scanAllPackagesWebService(serviceName);

            parsePackages(webServicesPackages, endPoint, parameterOfEntryWebServiceMethod, discoveredServices);
            return discoveredServices;
        } catch (IOException | ClassNotFoundException ex) {
            throw new BusinessException(ex);
        }
    }

    private static void parsePackages(List<Package> webServicesPackages, String endPoint, Map<String, String> parameterOfEntryWebServiceMethod, List<IDto> discoveredServices) throws IOException, ClassNotFoundException {
        for (Package pack : webServicesPackages) {

            String discoveredServiceName = pack.getName().
                    split("\\.")[pack.getName().split("\\.").length - 1];

            List<Class> classes = getClasses(pack.getName());

            classes.stream().forEach((clazz) -> {
                List< Method> allMethods = getAllMethods(clazz);
                if (clazz.getSimpleName().equals("Service")) {
                    parseMethods(allMethods, discoveredServiceName, endPoint, parameterOfEntryWebServiceMethod, discoveredServices);
                }
            });
        }
    }

    private static void parseMethods(List<Method> allMethods, String discoveredServiceName, String endPoint, Map<String, String> parameterOfEntryWebServiceMethod, List<IDto> discoveredServices) {
        String localEndPoint = endPoint;
        for (Method method : allMethods) {
            Map< String, String> annotationsOfMethod = getAnnotationsOfMethod(method);
            if (isAvailableService(annotationsOfMethod)) {
                Map< String, String> parameterOfMethod = getParametersOfMethod(method);

                String buildURI = buildURI(annotationsOfMethod.get("Path"),
                        parameterOfMethod);
                if (buildURI == null) {
                    buildURI = "";
                }

                DiscoveredService ds = new DiscoveredService();
                ds.setServiceName(discoveredServiceName);

                localEndPoint = parseEndPoint(localEndPoint, buildURI);

                ds.setUrl(URI + (localEndPoint + buildURI).replace(serviceNameRegex, discoveredServiceName));

                parseParameterOfMethod(parameterOfEntryWebServiceMethod, ds);
                parseParameterOfMethod(parameterOfMethod, ds);

                addRequestsServices(annotationsOfMethod, ds);

                ds.setProduces(annotationsOfMethod.get("Produces"));

                discoveredServices.add(ds);
            }
        }
    }

    private static void addRequestsServices(Map<String, String> annotationsOfMethod, DiscoveredService ds) {
        if (annotationsOfMethod.containsKey(RequestService.GET.toString())) {
            ds.getRequestService().add(RequestService.GET.toString());
        }

        if (annotationsOfMethod.containsKey(RequestService.PUT.toString())) {
            ds.getRequestService().add(RequestService.PUT.toString());
        }

        if (annotationsOfMethod.containsKey(RequestService.POST.toString())) {
            ds.getRequestService().add(RequestService.POST.toString());
        }

        if (annotationsOfMethod.containsKey(RequestService.DELETE.toString())) {
            ds.getRequestService().add(RequestService.DELETE.toString());
        }
    }

    private static void parseParameterOfMethod(Map<String, String> parameterOfMethod, DiscoveredService ds) {
        parameterOfMethod.entrySet().stream().forEach((entry) -> {
            String key = entry.getKey().replace("-PathParam", "").replace("-QueryParam", "");
            String value = entry.getValue();
            if (value != null && !key.equals(RootService.SERVICENAME_P)) {
                ds.getParameters().add(key + ":" + value);
            }
        });
    }

    private static String parseEndPoint(String endPoint, String buildURI) {
        String localEndPoint = endPoint;
        if (localEndPoint.endsWith("/") && buildURI.startsWith("?")) {
            localEndPoint = localEndPoint.substring(0, localEndPoint.length() - 1);
        } else if (localEndPoint.endsWith("/") && buildURI.startsWith("/")) {
            localEndPoint = localEndPoint.substring(0, localEndPoint.length() - 1);
        } else {
            if (!localEndPoint.endsWith("/") && !buildURI.startsWith("?") && !buildURI.startsWith("/")) {
                localEndPoint += "/";
            }
        }
        return localEndPoint;
    }

    private static List< Package> scanAllPackagesWebService(String serviceName) throws IOException {

        List< Package> list = new ArrayList();
        Package[] pa = Package.getPackages();

        if (serviceName == null) {
            for (Package p : pa) {
                if (p.getName().contains(wsRegex)) {
                    list.add(p);
                }
            }
        } else {
            for (Package p : pa) {
                if (p.getName().contains(wsRegex)
                        && p.getName().toLowerCase().endsWith(serviceName.toLowerCase())) {
                    list.add(p);
                }
            }
        }

        return list;
    }

    private static List<Class> getClasses(String basePackage)
            throws IOException, ClassNotFoundException {

        ResourcePatternResolver resourcePatternResolver
                = new PathMatchingResourcePatternResolver();
        MetadataReaderFactory metadataReaderFactory
                = new CachingMetadataReaderFactory(resourcePatternResolver);

        List<Class> candidates = new ArrayList();
        String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX
                + resolveBasePackage(basePackage) + "/" + "**/*.class";
        Resource[] resources = resourcePatternResolver.getResources(packageSearchPath);
        for (Resource resource : resources) {
            if (resource.isReadable()) {
                MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource);
                candidates.add(Class.forName(metadataReader.getClassMetadata().getClassName()));
            }
        }
        return candidates;
    }

    private static String resolveBasePackage(String basePackage) {
        return ClassUtils.convertClassNameToResourcePath(
                SystemPropertyUtils.resolvePlaceholders(basePackage));
    }

    private static List< Method> getAllMethods(Class clazz) {
        return Arrays.asList(clazz.getMethods());
    }

    private static Map< String, String> getAnnotationsOfMethod(Method method) {

        Map< String, String> map = new HashMap();

        List< Annotation> annotationsOfMethod = Arrays.asList(method.getAnnotations());

        annotationsOfMethod.stream().forEach((annot) -> {
            if (annot.toString().contains("(value=")) {

                String iKey = annot.annotationType().toString();
                String rKey = iKey.replace("interface ", "");
                String value = annot.toString().replace("@" + rKey, "").
                        replace("value=", "").replace("(", "").replace(")", "")
                        .replace("[", "").replace("]", "");

                map.put(annot.annotationType().getSimpleName(), value);
            } else {
                map.put(annot.annotationType().getSimpleName(), "");
            }
        });
        return map;
    }

    private static Map< String, String> getParametersOfMethod(Method method) {

        Map< String, String> map = new HashMap();
        Class<?>[] parameterTypes = method.getParameterTypes();
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        for (int i = 0; i < parameterAnnotations.length; i++) {
            for (Annotation parameterAnnotation : parameterAnnotations[i]) {
                if (parameterAnnotation.toString().contains("PathParam") || parameterAnnotation.toString().contains("QueryParam")) {
                    String paramType = parameterAnnotation.toString().contains("QueryParam") ? "QueryParam" : "PathParam";
                    map.put(parameterAnnotation.toString().split("value=")[1].replace(")", "") + "-" + paramType, parameterTypes[i].getSimpleName());
                }
            }
        }
        return map;

        /*
        Code for  JDK 1.8
        
        List< Parameter> parameterOfMethod = Arrays.asList(method.getParameters());
        for (Parameter param : parameterOfMethod) {
        
        for (Annotation annot : param.getAnnotations()) {
        if (annot.annotationType().getSimpleName().equals("PathParam") ||
        annot.annotationType().getSimpleName().equals("QueryParam")) {
        String key = annot.toString().split("value=")[1].replace(")", "");
        String value = param.getType().getSimpleName();
        map.put(key + "-" + annot.annotationType().getSimpleName(), value);
        }
         }
         }

         return map;
            
        */
    }

    private static boolean isAvailableService(Map< String, String> map) {
        return map.containsKey(RequestService.GET.toString())
                || map.containsKey(RequestService.PUT.toString())
                || map.containsKey(RequestService.POST.toString())
                || map.containsKey(RequestService.DELETE.toString());
    }

    private static boolean isAvailablePath(Map< String, String> map) {
        return map.containsKey("Path");
    }

    private static String buildURI(String path, Map< String, String> map) {

        boolean existingQueryParam = false;

        String uri = lookOverMapForUri(existingQueryParam, map);
        return getReturnpath(existingQueryParam, path, uri);
    }

    private static String lookOverMapForUri(boolean existingQueryParam, Map< String, String> map) {
        boolean localExistingQueryParam = existingQueryParam;
        String uri = "";
        for (String key : map.keySet()) {
            if (key.toLowerCase().contains("queryparam")) {
                if (!localExistingQueryParam) {
                    uri = String.format(FIRST_PARAMETER_PATTERN, uri,key.split("-")[0]);
                    localExistingQueryParam = true;
                } else {
                    uri = String.format(NEXT_PARAMETER_PATTERN, uri,key.split("-")[0]);
                }
            }
        }
        return uri;
    }

    private static String getReturnpath(boolean existingQueryParam, String path, String uri) {
        if (existingQueryParam) {
            if (path == null) {
                return uri;
            }
            if (path.endsWith("/")) {
                return path.substring(0, path.length() - 1) + uri;
            } else {
                return path + uri;
            }
        } else {
            return path;
        }
    }

    /**
     *
     */
    private Inspector() {
    }

}
