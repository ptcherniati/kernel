package org.inra.ecoinfo.ws.identification;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import javax.persistence.Transient;
import javax.ws.rs.core.UriInfo;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.exception.UnknownUserException;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.ws.crypto.Digestor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yahiaoui
 */
public class EndPointChecker {
    /**
     *
     */
    public static final ThreadLocal<String> th = new ThreadLocal();

    /**
     * The Logger LOGGER.
     */
    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(EndPointChecker.class.getName());
    /**
     * The IUsersManager users manager.
     */
    private IUsersManager usersManager;
    /**
     * The IPolicyManager PolicyManager.
     */
    private IPolicyManager policyManager;


    /**
     *
     */
    public EndPointChecker() {
    }

    /**
     *
     * @param uri
     * @param login
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     * @throws BusinessException
     */
    public String getSignatureFromLoginAndPassword(UriInfo uri, String login, String password) throws  NoSuchAlgorithmException, BusinessException{
        final Utilisateur utilisateur;
        final String timeStamp = Integer.toString(LocalDateTime.now().toInstant(ZoneOffset.UTC).getNano());
        utilisateur = usersManager.checkPassword(login, password);
        String signature = Digestor.digestSha1(login + utilisateur.getPassword()+ timeStamp);
        return String.format("%sresources/%s/%s/%s", uri.getBaseUri(),login,signature,timeStamp);
    }

    /**
     *
     * @param login
     * @param timeStamp
     * @param signature
     * @return
     */
    public boolean checkIntegrity(String login, String timeStamp, String signature) {

        final Utilisateur utilisateur;

        try {
            utilisateur = usersManager.getUtilisateurByLogin(login).orElseThrow(UnknownUserException::new );
            policyManager.setCurrentUser(utilisateur);
        } catch (final UnknownUserException e) {
            LOGGER.debug("unknown user", e);
            return false;
        }

        try {
            if (utilisateur != null && Digestor.digestSha1(login + utilisateur.getPassword()
                    + timeStamp).equalsIgnoreCase(signature)) {
                th.set(utilisateur.getPassword());
                return true;
            }
        } catch (NoSuchAlgorithmException ex) {
            LOGGER.error(EndPointChecker.class.getName(), ex);
        }

        return false;

    }

    /**
     *
     * @param usersManager
     */
    public void setUsersManager(IUsersManager usersManager) {
        this.usersManager = usersManager;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

}
