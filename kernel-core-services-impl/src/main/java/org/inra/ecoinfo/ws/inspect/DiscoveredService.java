package org.inra.ecoinfo.ws.inspect;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.inra.ecoinfo.ws.manager.IDto;

/**
 *
 * @author yahiaoui
 */
@XmlRootElement(name = "Service")
@XmlType(propOrder = {"serviceName", "url", "requestService",
    "produces", "parameters"})
@XmlAccessorType(XmlAccessType.FIELD)

public class DiscoveredService implements IDto {

    @XmlElement(name = "serviceName")
    private String serviceName;

    @XmlElement(name = "url")
    private String url;

    @XmlElementWrapper(name = "reqServices")
    @XmlElement(name = "reqService")
    private List<String> requestService;

    @XmlElementWrapper(name = "parameters")
    @XmlElement(name = "parameter")
    private List<String> parameters;

    @XmlElement(name = "produces")
    private String produces;

    /**
     *
     */
    public DiscoveredService() {
        this.requestService = new ArrayList();
        this.parameters = new ArrayList();
    }

    /**
     *
     * @return
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     *
     * @param serviceName
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     *
     * @return
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     */
    public List<String> getRequestService() {
        return requestService;
    }

    /**
     *
     * @param requestService
     */
    public void setRequestService(List<String> requestService) {
        this.requestService = requestService;
    }

    /**
     *
     * @return
     */
    public List<String> getParameters() {
        return parameters;
    }

    /**
     *
     * @param parameters
     */
    public void setParameters(List<String> parameters) {
        this.parameters = parameters;
    }

    /**
     *
     * @return
     */
    public String getProduces() {
        return produces;
    }

    /**
     *
     * @param produces
     */
    public void setProduces(String produces) {
        this.produces = produces;
    }

}
