package org.inra.ecoinfo.ws.utils;

/**
 *
 * @author yahiaoui
 */
import java.time.LocalDateTime;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author ptcherniati
 */
public class DateTimeAdapter extends XmlAdapter<String, LocalDateTime> {

    /**
     *
     * @param v
     * @return
     * @throws Exception
     */
    @Override
    public String marshal(LocalDateTime v) throws Exception {
        return DateUtil.getUTCDateTextFromLocalDateTime(v, DateUtil.DD_MM_YYYY_HH_MM_SS);
    }

    /**
     *
     * @param v
     * @return
     * @throws Exception
     */
    @Override
    public LocalDateTime unmarshal(String v) throws Exception {
        boolean  isDate, isTime;
        return DateUtil.readLocalDateTimeFromText(v, DateUtil.DD_MM_YYYY_HH_MM_SS);
    }

}
