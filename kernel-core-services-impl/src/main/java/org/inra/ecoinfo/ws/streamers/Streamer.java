package org.inra.ecoinfo.ws.streamers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.inra.ecoinfo.ws.manager.IDto;
import org.inra.ecoinfo.ws.manager.IResource;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author yahiaoui
 */
public class Streamer {

    /**
     *
     */
    public static final int LOOPFLUSH = 100;
    /**
     *
     */
    protected static final int blockSize = 16;

    private SessionFactory sessionFactory;

    private int indexRequest;
    private CountDownLatch latch;
    private final Object lock = new Object();
    private final Set<Integer> setProducerIteration = new HashSet();
    /**
     *
     */
    @PersistenceUnit(unitName = "JPAPu")
    protected EntityManagerFactory entityManagerFactory;

    /**
     *
     */
    protected IStreamerConfigurator streamerConfigurator;

    /**
     * Runtime.getRuntime().availableProcessors() *
     */
    protected int cores;

    /**
     *
     */
    protected int recorderLenght;

    /**
     *
     */
    protected int ratio;


    /**
     *
     */
    protected boolean isFinishedProcess = false;


    /**
     *
     */
    protected volatile IResource resource;

    /**
     *
     */
    protected Class dto;

    /**
     *
     */
    protected BlockingQueue<IDto> dtos;

    /**
     *
     */
    public Streamer() {
    }

    /**
     *
     */
    public void producerScheduler() {
        sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);

        configureStreamer();

        ExecutorService poolProducer = Executors.newFixedThreadPool(cores);
        latch = new CountDownLatch(cores);

        List<Producer> producers = new ArrayList();

        resource.initResource(recorderLenght * ratio);
        for (int i = 0; i < cores; i++) {
            producers.add(new Producer());
        }
        try {
            producers.stream().forEach((producer) -> poolProducer.execute(producer));
        } catch (Exception ex) {
            LoggerFactory.getLogger(StreamerOutputXml.class).error(ex.getMessage(), ex);
        }
    }


    /**
     *
     * @param index
     * @param sizeResult
     */
    public synchronized void getNextIndexRequest(int index, int sizeResult) {
        synchronized (lock) {
            if (!setProducerIteration.contains(index) && sizeResult == 0) {
                setProducerIteration.add(index);
                indexRequest++;
                resource.setOffset(-recorderLenght * ratio);
            }
        }
    }

    /**
     *
     * @return
     */
    public synchronized int getIndexRequest() {
        synchronized (lock) {
            return indexRequest;
        }
    }

    private void configureStreamer() {

        if (streamerConfigurator == null) {
            this.streamerConfigurator = new DefaultStreamerConfigurator();
        }
        this.cores = streamerConfigurator.getNbrCores();
        this.ratio = streamerConfigurator.getRatio();
        this.recorderLenght = streamerConfigurator.getRecorderLenght();

        dtos = new ArrayBlockingQueue<>(this.recorderLenght);
    }

    /**
     *
     */
    protected class Producer implements Runnable {

        Session session = null;

        @Override
        @Transactional(readOnly = true)
        public void run() {
            session = sessionFactory.openSession();
            int localIndexRequest;

            while (!resource.isFinished()) {
                localIndexRequest = getIndexRequest();
                List<IDto> dtoIterable
                        = resource.getDtoIterable(session, localIndexRequest, recorderLenght * ratio);

                if (dtoIterable == null) {
                    session.close();
                    sessionFactory.close();
                    latch.countDown();
                    isFinishedProcess = true;
                } else {
                    dtoIterable.stream().forEach((localDto) -> {
                        try {
                            dtos.put(localDto);
                        } catch (InterruptedException ex) {
                            LoggerFactory.getLogger(StreamerOutputXml.class).error(ex.getMessage(), ex);
                        }
                    });
                }
                getNextIndexRequest(localIndexRequest, dtoIterable==null?0:dtoIterable.size());
            }
            if(session.isOpen()) {
                session.close();
            }
            if(sessionFactory.isClosed()) {
                sessionFactory.close();
            }
            latch.countDown();
            try {
                latch.await();
            } catch (InterruptedException ex) {
                LoggerFactory.getLogger(Streamer.class).error(ex.getMessage(), ex);
            }
            isFinishedProcess = true;
        }
    }

}
