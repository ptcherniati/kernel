package org.inra.ecoinfo.ws.manager;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yahiaoui
 *
 * Enable Moxy ( JAX-B Implementation )
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class EmptyPojo implements Serializable {

    @XmlElement
    private String name;

    /**
     *
     */
    public EmptyPojo() {
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
}
