package org.inra.ecoinfo.ws.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author yahiaoui
 */
public class UtilsQueryBuilder {

    private static final String AND = " AND ";
    private static final String WHERE = " WHERE ";

    /**
     *
     * @param subQuery
     * @param listVariables
     * @param filterVariables
     * @param dates
     * @param listDates
     * @param valueDates
     * @param conditionQueryLimitOffset
     * @param queryListParameter
     * @return
     */
    public static String buildQuery(
            final String subQuery,
            final Map<String, Object> listVariables,
            final Map<String, String> filterVariables,
            final Map<String, LocalDateTime> dates,
            final Map<String, List<String>> listDates,
            final Map<String, List<String>> valueDates,
            final String conditionQueryLimitOffset,
            Map<String, Object> queryListParameter) {

        StringBuilder query = new StringBuilder(subQuery);

        if (!filterVariables.isEmpty()) {
            query.append(WHERE);

            filterVariables.entrySet().stream().forEach((entry) -> {
                String variable = entry.getKey();
                String value = entry.getValue();
                Object get = listVariables.get(variable);
                if (get != null) {
                    query.append(value).append(AND);
                    queryListParameter.put(variable, listVariables.get(variable));
                }
            });

            if (query.toString().endsWith(AND)) {
                query.replace(query.lastIndexOf(AND),
                        query.lastIndexOf(AND) + AND.length(), " ");
            }

            if (query.toString().endsWith(WHERE)) {
                query.replace(query.lastIndexOf(WHERE),
                        query.lastIndexOf(WHERE) + WHERE.length(), " ");
            }
        }

        if (!listDates.isEmpty()) {

            listDates.entrySet().stream().forEach((entry) -> {
                String groupDate = entry.getKey();
                List<String> valueList = entry.getValue();
                if (valueList != null) {
                    String sDateMin = valueList.get(0);
                    String sDateMax = valueList.get(1);
                    LocalDateTime dateMin = dates.get(sDateMin);
                    LocalDateTime dateMax = dates.get(sDateMax);
                    List<String> valuesDate = valueDates.get(groupDate);
                    if (valuesDate != null) {
                        if (query.toString().contains(WHERE)) {

                            if (dateMin != null && dateMax == null) {
                                query.append(AND).append(valuesDate.get(0));
                                queryListParameter.put(sDateMin, dateMin);
                            }

                            if (dateMin == null && dateMax != null) {
                                query.append(AND).append(valuesDate.get(1));
                                queryListParameter.put(sDateMax, dateMax);
                            }

                            if (dateMin != null && dateMax != null) {
                                query.append(AND).append(valuesDate.get(2));
                                queryListParameter.put(sDateMin, dateMin);
                                queryListParameter.put(sDateMax, dateMax);
                            }

                        } else {
                            if (dateMin != null && dateMax == null) {
                                query.append(WHERE).append(valuesDate.get(0));
                                queryListParameter.put(sDateMin, dateMin);
                            }

                            if (dateMin == null && dateMax != null) {
                                query.append(WHERE).append(valuesDate.get(1));
                                queryListParameter.put(sDateMax, dateMax);
                            }

                            if (dateMin != null && dateMax != null) {
                                query.append(WHERE).append(valuesDate.get(2));
                                queryListParameter.put(sDateMin, dateMin);
                                queryListParameter.put(sDateMax, dateMax);
                            }
                        }
                    }
                }
            });

        }

        return query.append(conditionQueryLimitOffset).toString();

    }

    /**
     *
     * @param queries
     * @param variables
     * @return
     */
    public static List<String> selectQueries(Map<String, String> queries, List<String> variables) {

        List queriesResult = new ArrayList();
        if (variables.isEmpty()) {
            queries.entrySet().stream().forEach((entry) -> {
                queriesResult.add(entry.getValue());
            });
        } else {
            queries.entrySet().stream().forEach((entry) -> {
                String variable = entry.getKey();
                String query = entry.getValue();
                if (variables.contains(variable)) {
                    queriesResult.add(query);
                }
            });
        }

        return queriesResult;
    }

    private UtilsQueryBuilder() {
    }
}
