package org.inra.ecoinfo.ws.manager;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 *
 * .....
 *
 * @author schellenberger
 */
@XmlRootElement(name = "data")
@XmlType(propOrder = {"variable", "unite", "fullLocalDateTime", "timeDay", "profondeur", "nomParcelle", "nomTraitement", "numRepetition", "valeur"})
@XmlAccessorType(XmlAccessType.FIELD)

public class Dto implements IDto, Serializable {

    @XmlElement(name = "unite")
    private String unite;

    @XmlTransient
    private LocalDateTime dayLocalDateTime;

    @XmlElement(name = "numRepetition")
    private Integer numRepetition;

    @XmlElement(name = "variable")
    private String variable;

    @XmlElement(name = "valeur")
    private Float valeur;

    @XmlElement(name = "profondeur")
    private Integer profondeur;

    @XmlElement(name = "heure")
    private String timeDay;

    @XmlElement(name = "date")
    private String fullLocalDateTime;

    @XmlTransient
    private LocalDateTime date;

    @XmlElement(name = "nomParcelle")
    private String nomParcelle;

    @XmlElement(name = "nomTraitement")
    private String nomTraitement;

    /**
     *
     */
    public Dto() {
    }

    /**
     *
     * @param variable
     * @param unite
     * @param date
     * @param profondeur
     * @param nomParcelle
     * @param nomTraitement
     * @param numRepetition
     * @param f
     * @param valeur
     */
    public Dto(String variable, String unite, LocalDateTime date, Integer profondeur, String nomParcelle, String nomTraitement, Integer numRepetition, Float valeur) {
        this.unite = unite;
        this.variable = variable;
        this.valeur = valeur;
        this.date = date;
        this.profondeur = profondeur;
        this.nomParcelle = nomParcelle;
        this.nomTraitement = nomTraitement;
        this.numRepetition = numRepetition;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getDayLocalDateTime() {
        return dayLocalDateTime;
    }

    /**
     *
     * @param dayLocalDateTime
     */
    public void setDayLocalDateTime(LocalDateTime dayLocalDateTime) {
        this.dayLocalDateTime = dayLocalDateTime;
        fullLocalDateTime = DateUtil.getUTCDateTextFromLocalDateTime(dayLocalDateTime, DateUtil.DD_MM_YYYY_FILE);
    }

    /**
     *
     * @return
     */
    public String getFullLocalDateTime() {
        return fullLocalDateTime;
    }

    /**
     *
     * @return
     */
    public Integer getNumRepetition() {
        return numRepetition;
    }

    /**
     *
     * @param numRepetition
     */
    public void setNumRepetition(Integer numRepetition) {
        this.numRepetition = numRepetition;
    }

    /**
     *
     * @return
     */
    public String getNomParcelle() {
        return nomParcelle;
    }

    /**
     *
     * @param nomParcelle
     */
    public void setNomParcelle(String nomParcelle) {
        this.nomParcelle = nomParcelle;
    }

    /**
     *
     * @return
     */
    public String getNomTraitement() {
        return nomTraitement;
    }

    /**
     *
     * @param nomTraitement
     */
    public void setNomTraitement(String nomTraitement) {
        this.nomTraitement = nomTraitement;
    }

    /**
     *
     * @return
     */
    public String getUnite() {
        return unite;
    }

    /**
     *
     * @param unite
     */
    public void setUnite(String unite) {
        this.unite = unite;
    }

    /**
     *
     * @return
     */
    public String getVariable() {
        return variable;
    }

    /**
     *
     * @param variable
     */
    public void setVariable(String variable) {
        this.variable = variable;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public Integer getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(Integer profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getLocalDateTime() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setLocalDateTime(LocalDateTime date) {
        this.date = date;
        timeDay = DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.HH_MM_SS);
    }

    /**
     *
     * @return
     */
    public String getTimeDay() {
        return timeDay;
    }

}
