package org.inra.ecoinfo.ws.utils;

/**
 *
 * @author yahiaoui
 */
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import org.eclipse.persistence.internal.oxm.CharacterEscapeHandler;

/**
 *
 * @author ptcherniati
 */
public class XmlCharacterHandler implements CharacterEscapeHandler {

    /**
     *
     * @param buf
     * @param start
     * @param len
     * @param isAttValue
     * @param out
     * @throws IOException
     */
    @Override
    public void escape(char[] buf, int start, int len, boolean isAttValue,
            Writer out) throws IOException {
        StringWriter buffer = new StringWriter();

        for (int i = start; i < start + len; i++) {
            buffer.write(buf[i]);
        }

        String st = buffer.toString();

        st = buffer.toString().replace("&amp;", "&").replace("&lt;", "<")
                .replace("&gt;", ">").replace("&apos;", "'")
                .replace("&quot;", "\"");

        out.write(st);
    }
}
