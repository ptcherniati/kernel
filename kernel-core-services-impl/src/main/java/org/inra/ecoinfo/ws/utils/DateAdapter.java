package org.inra.ecoinfo.ws.utils;

/**
 *
 * @author yahiaoui
 */
import java.time.LocalDate;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author ptcherniati
 */
public class DateAdapter extends XmlAdapter<String, LocalDate> {

    /**
     *
     * @param v
     * @return
     * @throws Exception
     */
    @Override
    public String marshal(LocalDate v) throws Exception {
        return DateUtil.getUTCDateTextFromLocalDateTime(v, DateUtil.DD_MM_YYYY);
    }

    /**
     *
     * @param v
     * @return
     * @throws Exception
     */
    @Override
    public LocalDate unmarshal(String v) throws Exception {
        return DateUtil.readLocalDateFromText(v, DateUtil.DD_MM_YYYY);
    }

}
