package org.inra.ecoinfo.ws.streamers;

/**
 *
 * @author yahiaoui
 */
public interface IStreamer {

    /**
     *
     * @param iStreamerConfigurator
     */
    void setStreamerConfigurator(IStreamerConfigurator iStreamerConfigurator);
}
