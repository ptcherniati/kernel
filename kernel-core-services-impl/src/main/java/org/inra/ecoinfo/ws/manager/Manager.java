package org.inra.ecoinfo.ws.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.inra.ecoinfo.AbstractJPADAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yahiaoui
 * @param <T>
 */
public abstract  class Manager<T extends IDto> extends AbstractJPADAO<T> implements IManager<T>{

    private static final Logger LOGGER = LoggerFactory.getLogger(Manager.class);
    private final Object lockOffset = new Object();

    /**
     *
     */
    protected List<String> queryList;

    /**
     *
     */
    protected int offset = 0;

    /**
     *
     */
    protected volatile boolean finish = false;


    /**
     *
     */
    protected Map<String, Object> queryListParameter;

    /**
     *
     */
    protected Map<String, Class> addEntityParameter;

    /**
     *
     */
    protected Map<String, String> addJoinParamater;

    /**
     *
     */
    public Manager() {
        this.queryList = new ArrayList();
        this.queryListParameter = new HashMap();
        this.addEntityParameter = new HashMap();
        this.addJoinParamater = new HashMap();

    }

    protected synchronized int incOffset(int limit) {
        synchronized (lockOffset) {
            this.offset += limit;
            return this.offset;
        }
    }

    protected void setLimitOffsetSQLParameter(NativeQuery query, int limit, int offset) {
        query.setParameter(QueryParam.limit.name(), limit);
        query.setParameter(QueryParam.offset.name(), offset);
    }

    protected void setLimitOffsetJPAParameter(NativeQuery query, int limit, int offset) {
        query.setFirstResult(offset);
        query.setMaxResults(limit);
    }

    protected void setGenericParameters(NativeQuery query, Map<String, Object> generiquesParams) {
        
        generiquesParams.keySet().stream().forEach((param) -> {
            query.setParameter(param, generiquesParams.get(param));
        });
    }
    
    protected void setEntityParametersForSQLQuery(NativeQuery query) {
        if (!addEntityParameter.isEmpty()) {
            addEntityParameter.entrySet().stream().forEach((entry) -> {
                query.addEntity(entry.getKey(), entry.getValue());
            });
        }
    }
    
    protected void setJoinParametersForSQLQuery(NativeQuery query) {
        if (!addJoinParamater.isEmpty()) {
            addJoinParamater.entrySet().stream().forEach((entry) -> {
                query.addJoin(entry.getKey(), entry.getValue());
            });
        }
    }

    /**
     *
     * @param session
     * @param indexRequest
     * @param limit
     * @param dtoClass
     * @return
     */
    protected List<T> executeSQLQuery(Session session, int indexRequest, int limit, Class dtoClass) {
        List<T> list = new ArrayList();

        if (indexRequest > (queryList.size() - 1)) {
            finish = true;
            return list;
        }

        try {
            NativeQuery createSQLQuery = session.createNativeQuery(
                    queryList.get(indexRequest));
            setGenericParameters(createSQLQuery, queryListParameter);
            setEntityParametersForSQLQuery(createSQLQuery);
            setJoinParametersForSQLQuery(createSQLQuery);

            int incOffset = incOffset(limit);
            setLimitOffsetSQLParameter(createSQLQuery, limit, incOffset);
            list = createSQLQuery.setResultTransformer(
                    Transformers.aliasToBean(dtoClass)).list();
        } catch (HibernateException ex) {
            LOGGER.error(String.format("error in executeSQLQuery indexRequest %d, limit %d, dtoClass %s", indexRequest, limit, dtoClass), ex);
        }

        return list;
    }

    /**
     *
     * @param session
     * @param indexRequest
     * @param limit
     * @param queryListParameter
     * @param dtoClass
     * @return
     */
    protected List<IDto> executeCustomJPAQuery(Session session, int indexRequest, int limit, Map<String, Object> queryListParameter, Class dtoClass) {
        
        if (indexRequest > (queryList.size() - 1)) {
            finish = true;
            return new ArrayList();
        }
        try {
            NativeQuery jpaQyery = session.createNativeQuery(
                    queryList.get(indexRequest));
            setGenericParameters(jpaQyery, queryListParameter);

            int incOffset = incOffset(limit);
            setLimitOffsetJPAParameter(jpaQyery, limit, incOffset);
            List list = jpaQyery.list();
            return list;
        } catch (HibernateException ex) {
            LOGGER.debug(ex.getMessage(), ex);
        }

        return null;
    }

    /**
     *
     * @param session
     * @param query
     * @param queryListParameter
     * @param dtoClass
     * @return
     */
    protected List<IDto> executeJPAQuery(Session session, String query, Map<String, Object> queryListParameter, Class dtoClass) {
        
        try {
            NativeQuery jpaQyery = session.createNativeQuery(query);
            if (queryListParameter != null) {
                setGenericParameters(jpaQyery, queryListParameter);
            }

            List list = jpaQyery.list();
            return list;
        } catch (HibernateException ex) {
            LOGGER.debug(ex.getMessage(), ex);
        }

        return null;
    }

    /**
     *
     * @param limit
     */
    protected synchronized void decOffset(int limit) {
        synchronized (lockOffset) {
            this.offset -= limit;
        }
    }

}
