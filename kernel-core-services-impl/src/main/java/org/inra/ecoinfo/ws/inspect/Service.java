package org.inra.ecoinfo.ws.inspect;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.inra.ecoinfo.ws.exceptions.BusinessException;
import org.inra.ecoinfo.ws.identification.RootService;
import org.inra.ecoinfo.ws.streamers.AbstractService;
import org.inra.ecoinfo.ws.streamers.DefaultStreamerConfigurator;
import org.inra.ecoinfo.ws.streamers.StreamerOutputJson;
import org.inra.ecoinfo.ws.streamers.StreamerOutputJsonEncrypted;
import org.inra.ecoinfo.ws.streamers.StreamerOutputXml;
import org.inra.ecoinfo.ws.streamers.StreamerOutputXmlEncrypted;

/**
 *
 * @author ptcherniati
 */
public class Service extends AbstractService implements IService {

    private Resource resource;

    /**
     *
     * @param uriInfo
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    @GET
    @Produces("xml/plain")
    @Override
    public Response inspectAllServicesXML(@Context UriInfo uriInfo)
            throws BusinessException {

        String uri = uriInfo.getBaseUri()
                + RootService.PATH_RESOURCE.replaceAll("/", "") + "/";

        resource.setURI(uri);
        StreamerOutputXml streamerOutputXml = getStreamerOutputXml();

        streamerOutputXml.setResource(resource);
        streamerOutputXml.setDto(DiscoveredService.class);

        streamerOutputXml.setStreamerConfigurator(getDefaultStreamerConfigurator());

        return Response.status(Response.Status.OK).
                entity(streamerOutputXml).build();
    }

    /**
     *
     * @param uriInfo
     * @param serviceName
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    @GET
    @Produces("xml/plain")
    @Path("{whichServiceName}")
    @Override
    public Response inspectSpecificServiceXML(@Context UriInfo uriInfo,
            @PathParam("whichServiceName") String serviceName)
            throws BusinessException {

        String uri = uriInfo.getBaseUri()
                + RootService.PATH_RESOURCE.replaceAll("/", "") + "/";

        resource.setURI(uri);
        resource.setWhichService(serviceName);
        StreamerOutputXml streamerOutputXml = getStreamerOutputXml();

        streamerOutputXml.setResource(resource);
        streamerOutputXml.setDto(DiscoveredService.class);

        streamerOutputXml.setStreamerConfigurator(getDefaultStreamerConfigurator());

        return Response.status(Response.Status.OK).
                entity(streamerOutputXml).build();
    }

    /**
     *
     * @param uriInfo
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    @GET
    @Produces("json/plain")
    @Override
    public Response inspectAllServicesJSON(@Context UriInfo uriInfo)
            throws BusinessException {

        String uri = uriInfo.getBaseUri()
                + RootService.PATH_RESOURCE.replaceAll("/", "") + "/";

        resource.setURI(uri);
        StreamerOutputJson streamerOutputJson = getStreamerOutputJson();
        streamerOutputJson.setResource(resource);
        streamerOutputJson.setDto(DiscoveredService.class);

        streamerOutputJson.setStreamerConfigurator(getDefaultStreamerConfigurator());

        return Response.status(Response.Status.OK).
                entity(streamerOutputJson).build();
    }

    /**
     *
     * @param uriInfo
     * @param serviceName
     * @return
     */
    @GET
    @Produces("json/plain")
    @Path("{whichServiceName}")
    @Override
    public Response inspectSpecificServiceJSON(@Context UriInfo uriInfo,
            @PathParam("whichServiceName") String serviceName)
            throws BusinessException {

        String uri = uriInfo.getBaseUri()
                + RootService.PATH_RESOURCE.replaceAll("/", "") + "/";

        resource.setURI(uri);
        resource.setWhichService(serviceName);
        StreamerOutputJson streamerOutputJson = getStreamerOutputJson();

        streamerOutputJson.setResource(resource);
        streamerOutputJson.setDto(DiscoveredService.class);

        streamerOutputJson.setStreamerConfigurator(getDefaultStreamerConfigurator());

        return Response.status(Response.Status.OK).
                entity(streamerOutputJson).build();
    }

    /**
     *
     * @param uriInfo
     * @return
     */
    @GET
    @Produces("xml/encrypted")
    @Override
    public Response inspectAllServicesEncryptedXML(@Context UriInfo uriInfo)
            throws BusinessException {

        String uri = uriInfo.getBaseUri()
                + RootService.PATH_RESOURCE.replaceAll("/", "") + "/";

        resource.setURI(uri);
        StreamerOutputXmlEncrypted streamerOutputXmlEncrypted
                = getStreamerOutputXmlEncrypted();

        streamerOutputXmlEncrypted.setResource(resource);
        streamerOutputXmlEncrypted.setDto(DiscoveredService.class);

        streamerOutputXmlEncrypted.
                setStreamerConfigurator(getDefaultStreamerConfigurator());

        return Response.status(Response.Status.OK).
                entity(streamerOutputXmlEncrypted).build();
    }

    /**
     *
     * @param uriInfo
     * @param serviceName
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    @GET
    @Produces("xml/encrypted")
    @Path("{whichServiceName}")
    @Override
    public Response inspectSpecificServiceEncryptedXML(@Context UriInfo uriInfo,
            @PathParam("whichServiceName") String serviceName)
            throws BusinessException {

        String uri = uriInfo.getBaseUri()
                + RootService.PATH_RESOURCE.replaceAll("/", "") + "/";

        resource.setURI(uri);
        resource.setWhichService(serviceName);
        StreamerOutputXmlEncrypted streamerOutputXmlEncrypted
                = getStreamerOutputXmlEncrypted();

        streamerOutputXmlEncrypted.setResource(resource);
        streamerOutputXmlEncrypted.setDto(DiscoveredService.class);

        streamerOutputXmlEncrypted.
                setStreamerConfigurator(getDefaultStreamerConfigurator());

        return Response.status(Response.Status.OK).
                entity(streamerOutputXmlEncrypted).build();
    }

    /**
     *
     * @param uriInfo
     * @return
     */
    @GET
    @Produces("json/encrypted")
    @Override
    public Response inspectAllServicesEncryptedJSON(@Context UriInfo uriInfo)
            throws BusinessException {

        String uri = uriInfo.getBaseUri()
                + RootService.PATH_RESOURCE.replaceAll("/", "") + "/";

        resource.setURI(uri);
        StreamerOutputJsonEncrypted streamerOutputJsonEncrypted
                = getStreamerOutputJsonEncrypted();

        streamerOutputJsonEncrypted.setResource(resource);
        streamerOutputJsonEncrypted.setDto(DiscoveredService.class);

        streamerOutputJsonEncrypted.
                setStreamerConfigurator(getDefaultStreamerConfigurator());

        return Response.status(Response.Status.OK).
                entity(streamerOutputJsonEncrypted).build();
    }

    /**
     *
     * @param uriInfo
     * @param serviceName
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    @GET
    @Produces("json/encrypted")
    @Path("{whichServiceName}")
    @Override
    public Response inspectSpecificServiceEncryptedJSON(@Context UriInfo uriInfo,
            @PathParam("whichServiceName") String serviceName)
            throws BusinessException {

        String uri = uriInfo.getBaseUri()
                + RootService.PATH_RESOURCE.replaceAll("/", "") + "/";

        resource.setURI(uri);
        resource.setWhichService(serviceName);
        StreamerOutputJsonEncrypted streamerOutputJsonEncrypted
                = getStreamerOutputJsonEncrypted();

        streamerOutputJsonEncrypted.setResource(resource);
        streamerOutputJsonEncrypted.setDto(DiscoveredService.class);

        streamerOutputJsonEncrypted.
                setStreamerConfigurator(getDefaultStreamerConfigurator());

        return Response.status(Response.Status.OK).
                entity(streamerOutputJsonEncrypted).build();

    }

    /**
     *
     * @return
     */
    public Resource getResource() {
        return resource;
    }

    /**
     *
     * @param resource
     */
    public void setResource(Resource resource) {
        this.resource = resource;
    }

    private DefaultStreamerConfigurator getDefaultStreamerConfigurator() {
        DefaultStreamerConfigurator defaultStreamerConfigurator
                = new DefaultStreamerConfigurator();
        defaultStreamerConfigurator.setNbrCores(1);
        defaultStreamerConfigurator.setRatio(1);
        defaultStreamerConfigurator.setRecorderLenght(100);
        return defaultStreamerConfigurator;
    }

}
