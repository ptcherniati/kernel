package org.inra.ecoinfo.ws.identification;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.inra.ecoinfo.ws.exceptions.BusinessException;

/**
 * The Interface IUsersManagerREST.
 */
public interface IRootService {

    /**
     *
     * @param login
     * @param signature
     * @param timeStamp
     * @param codeService
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    Object subResourceLocators(
            @PathParam("login") String login,
            @PathParam("signature") String signature,
            @PathParam("timeStamp") String timeStamp,
            @PathParam("codeService") String codeService) throws BusinessException;

    /**
     *
     * @param login
     * @param signature
     * @param timeStamp
     * @return
     * @throws org.inra.ecoinfo.ws.exceptions.BusinessException
     */
    Object authenticationCheck(
            @PathParam("login") String login,
            @PathParam("signature") String signature,
            @PathParam("timeStamp") String timeStamp) throws BusinessException;

    /**
     *
     * @param login
     * @param password
     * @param uri
     * @return
     * @throws BusinessException
     */
    Response getSignature(@PathParam(value = "login") final String login, @PathParam(value = "password") final String password, @Context UriInfo uri) throws BusinessException;

}
