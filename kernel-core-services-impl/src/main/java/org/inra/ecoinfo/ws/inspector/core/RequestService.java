package org.inra.ecoinfo.ws.inspector.core;

/**
 *
 * @author yahiaoui
 */
public enum RequestService {

    /**
     *
     */
    GET,
    /**
     *
     */
    PUT,
    /**
     *
     */
    POST,
    /**
     *
     */
    DELETE,

}
