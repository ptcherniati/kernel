package org.inra.ecoinfo.ws.streamers;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author yahiaoui
 */
public abstract class AbstractService implements ApplicationContextAware {

    ApplicationContext applicationContext;

    /**
     *
     * @return
     */
    public StreamerOutputXml getStreamerOutputXml() {
        return (StreamerOutputXml) applicationContext.getBean("streamerOutputXml");
    }

    /**
     *
     * @return
     */
    public StreamerOutputXmlEncrypted getStreamerOutputXmlEncrypted() {
        return (StreamerOutputXmlEncrypted) applicationContext.getBean("streamerOutputXmlEncrypted");
    }

    /**
     *
     * @return
     */
    public StreamerOutputJson getStreamerOutputJson() {
        return (StreamerOutputJson) applicationContext.getBean("streamerOutputJson");
    }

    /**
     *
     * @return
     */
    public StreamerOutputJsonEncrypted getStreamerOutputJsonEncrypted() {
        return (StreamerOutputJsonEncrypted) applicationContext.getBean("streamerOutputJsonEncrypted");
    }

    /**
     *
     * @param appContext
     */
    public void setAppContext(ApplicationContext appContext) {
        this.applicationContext = appContext;
    }

    /**
     *
     * @param applicationContext
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

}
