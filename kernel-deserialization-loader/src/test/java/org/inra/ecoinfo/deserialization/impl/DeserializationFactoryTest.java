/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.deserialization.impl;

import java.util.HashMap;
import java.util.Map;
import javax.xml.validation.Schema;
import org.inra.ecoinfo.deserialization.IModuleDeserialization;
import org.inra.ecoinfo.deserialization.impl.xsd.UtilisateurDeserialization;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.MockitoAnnotations;


/**
 *
 * @author tcherniatinsky
 */
public class DeserializationFactoryTest {

    /**
     *
     */
    public static final String REPOSITORY_URI = "repositoryURI";

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    DeserializationFactory instance;
    UtilisateurDeserialization module;
    Map<String, IModuleDeserialization> modules = new HashMap();

    /**
     *
     */
    public DeserializationFactoryTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new DeserializationFactory();
        module = new UtilisateurDeserialization();
        modules.put(module.getModuleName(), module);
        instance.setModulesDeserialization(modules);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getRepositoryURI method, of class DeserializationFactory.
     */
    @Test
    public void testGetRepositoryURI() {
        instance.setRepositoryURI(REPOSITORY_URI);
        String result = instance.getRepositoryURI();
        assertEquals(REPOSITORY_URI, result);
    }

    /**
     * Test of createDeserialization method, of class DeserializationFactory.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testCreateDeserialization() throws Exception {
        ModuleDeserialization result = instance.createDeserialization();
        assertEquals(module, result.getModuleDeserialization(module.getModuleName()));
        assertEquals("projet-ORE", ((UtilisateurDeserialization)result.getModuleDeserialization(module.getModuleName())).getId());
    }

    /**
     * Test of getModulesDeserialization method, of class DeserializationFactory.
     */
    @Test
    public void testGetModulesDeserialization() {
        Map<String, IModuleDeserialization> result = instance.getModulesDeserialization();
        assertEquals(modules, result);
    }

    /**
     * Test of getSchema method, of class DeserializationFactory.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetSchema() throws Exception {
        Schema result = instance.getSchema(module);
        assertNotNull(result);
    }

    /**
     * Test of newIntegerClassArray method, of class DeserializationFactory.
     */
    @Test
    public void testNewIntegerClassArray() {
        Class[] result = instance.newIntegerClassArray();
        assertEquals(Integer.class, result[0]);
        assertEquals(StringBuilder.class, result[1]);
    }

    /**
     * Test of newStringClassArray method, of class DeserializationFactory.
     */
    @Test
    public void testNewStringClassArray() {
        Class[] result = instance.newStringClassArray();
        assertEquals(String.class, result[0]);
        assertEquals(String.class, result[1]);
    }

}
