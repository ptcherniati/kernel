package org.inra.ecoinfo.deserialization.impl.xsd;

import java.io.File;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.deserialization.AbstractDeserialization;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MailAdminDeserialization.
 */
public class UtilisateurDeserialization extends AbstractDeserialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "utilisateurDeserialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "utilisateurDeserialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(UtilisateurDeserialization.class.getName());
    private String id;

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }
    
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        super.createConfig(digester);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#getModuleName()
     */

    /**
     *
     * @return
     */

    @Override
    public String getModuleName() {
        return UtilisateurDeserialization.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.AbstractDeserialization#getSchemaPath()
     */

    /**
     *
     * @return
     */

    @Override
    public String getSchemaPath() {
        return UtilisateurDeserialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.config.IModuleDeserialization#isAutoLoad()
     */

    /**
     *
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }
    
    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }
    
    /**
     *
     * @param inZip
     * @throws BusinessException
     */
    @Override
    public void deSerialize(File inZip) throws BusinessException {
       LOGGER.info(String.format("loading %s ", inZip.getName()));
    }
}
