/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.deserialization.impl;

import java.util.HashMap;
import java.util.Map;
import org.easymock.Mock;
import org.inra.ecoinfo.deserialization.IModuleDeserialization;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.MockitoAnnotations;


/**
 *
 * @author tcherniatinsky
 */
public class ModuleDeserializationTest {

    /**
     *
     */
    public static final String MODULE_NAME = "moduleName";

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    ModuleDeserialization instance;
    @Mock
    IModuleDeserialization moduleDeserialization;
    Map<String, IModuleDeserialization> modules = new HashMap();

    /**
     *
     */
    public ModuleDeserializationTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        instance = new ModuleDeserialization();
        modules.put(MODULE_NAME, moduleDeserialization);
        instance.setModulesDeserialization(modules);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getModuleDeserialization method, of class ModuleDeserialization.
     */
    @Test
    public void testGetModuleDeserialization() {
        IModuleDeserialization result = instance.getModuleDeserialization(MODULE_NAME);
        assertEquals(moduleDeserialization, result);
    }

    /**
     * Test of getModulesDeserialization method, of class ModuleDeserialization.
     */
    @Test
    public void testGetModulesDeserialization() {
        Map<String, IModuleDeserialization> result = instance.getModulesDeserialization();
        assertEquals(modules, result);
    }

}
