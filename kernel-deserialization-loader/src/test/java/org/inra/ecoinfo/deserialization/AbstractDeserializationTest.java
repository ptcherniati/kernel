/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.deserialization;

import java.io.InputStream;
import org.inra.ecoinfo.deserialization.impl.xsd.UtilisateurDeserialization;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tcherniatinsky
 */
public class AbstractDeserializationTest {

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractDeserializationTest.class.getName());

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    AbstractDeserialization instance;

    /**
     *
     */
    public AbstractDeserializationTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        instance = new UtilisateurDeserialization();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getStreamSchema method, of class AbstractDeserialization.
     */
    @Test
    public void testGetStreamSchema() {
        InputStream expResult = null;
        InputStream result = instance.getStreamSchema();
        assertNotNull(result);
    }

    /**
     * Test of newIntegerClassArray method, of class AbstractDeserialization.
     */
    @Test
    public void testNewIntegerClassArray() {
        Class[] result = instance.newIntegerClassArray();
        assertEquals(Integer.class, result[0]);
        assertEquals(StringBuilder.class, result[1]);
    }

    /**
     * Test of newStringClassArray method, of class AbstractDeserialization.
     */
    @Test
    public void testNewStringClassArray() {
        Class[] result = instance.newStringClassArray();
        assertEquals(String.class, result[0]);
        assertEquals(String.class, result[1]);
    }


}
