package org.inra.ecoinfo.deserialization;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.AbstractConfiguration;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AbstractDeserialization. Can be use to implements
 * IModuleDeserialization classes. Add some fonction to help building digester
 * parsing
 */
public abstract class AbstractDeserialization extends AbstractConfiguration implements IModuleDeserialization {

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractDeserialization.class);

    /**
     *
     */
    public AbstractDeserialization() {
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#postConfig()
     */
    @Override
    public void postConfig() throws BusinessException {
        LOGGER.info(String.format("no postConfig in %s", getModuleName()));
    }

    /**
     *
     * @param inZip
     * @param entryName
     * @return
     * @throws IOException
     */
    protected ZipInputStream openEntry(File inZip, String entryName) throws IOException {
                FileInputStream fis;
                ZipInputStream zipInputStream;
        try  {
                fis = new FileInputStream(inZip);
                zipInputStream = new ZipInputStream(fis, StandardCharsets.UTF_8);
            for (ZipEntry e; (e = zipInputStream.getNextEntry()) != null;) {
                if (entryName.equals(e.getName())) {
                    return zipInputStream;
                }
            }
        } catch (Exception e) {
            throw new IOException(String.format("No entry for %s in %s", entryName, inZip.getName()), e);
        }
        throw new IOException(String.format("No entry for %s in %s", entryName, inZip.getName()));
    }

    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod(String.format("deserialization/module/%s/skip",getModuleName()), "setSkip", 0);
        //do nothing
    }
}
