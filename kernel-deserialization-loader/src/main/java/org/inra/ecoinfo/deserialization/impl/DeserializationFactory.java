package org.inra.ecoinfo.deserialization.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipInputStream;
import javax.persistence.Transient;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.deserialization.IModuleDeserialization;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * A factory for creating Deserialization objects.
 */
public class DeserializationFactory {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/coreDeserialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "coreDeserialization";
    private static final String PATH_FOR_DESERIALIZATION = "%sserialization/serialization.zip";

    /**
     *
     */
    public static final String CST_DEFAULT_SITE_SEPARATOR_FOR_FILE_NAMES = "-";
    /**
     * The Constant CORRECT_LOADING.
     */
    private static final String CORRECT_LOADING = "Le module %s a été correctement chargé";
    /**
     * The Constant BAD_LOADING.
     */
    private static final String BAD_LOADING = "Le module %s n'a pas été correctement chargé";
    /**
     * The Constant CORRECT_LOADING.
     */
    private static final String CORRECT_CONFIG = "Le module %s a été correctement paramétré";
    /**
     * The Constant BAD_LOADING.
     */
    private static final String BAD_CONFIG = "Le module %s n'a pas été correctement paramétré %n \t %s";
    /**
     * The Constant DYNAMIC_VALIDATION.
     */
    private static final String DYNAMIC_VALIDATION = "http://apache.org/xml/features/validation/dynamic";
    /**
     * The Constant CONFIG_PATH.
     */
    private static final String CONFIG_PATH = "/META-INF/deserialization.xml";
    /**
     * The Logger LOGGER.
     */
    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(DeserializationFactory.class.getName());
    /**
     * The String repository uri.
     */
    private String repositoryURI;
    /**
     * The Map<String,IModuleDeserialization> modules configuration.
     */
    private Map<String, IModuleDeserialization> modulesDeserialization = new HashMap<>();
    ZipInputStream outZip;

    /**
     * Instantiates a new DeserializationFactory configuration factory.
     *
     */
    public DeserializationFactory() {
//        if (new File(DeserializationFactory.LOG4J_GEN_PROPERTIES_LOCATION).exists()) {
//            PropertyConfigurator.configure(DeserializationFactory.LOG4J_GEN_PROPERTIES_LOCATION);
//        } else if (this.getClass().getResource(DeserializationFactory.LOG4J_GEN_PROPERTIES) != null) {
//            try {
//                final Properties p = new Properties();
//                p.load(this.getClass().getResource(DeserializationFactory.LOG4J_GEN_PROPERTIES).openStream());
//                PropertyConfigurator.configure(p);
//            } catch (IOException ex) {
//                LoggerFactory.getLogger(DeserializationFactory.class.getName()).error(ex.getMessage(), ex);
//            }
//        } else if (new File(DeserializationFactory.SRC_MAIN_RESOURCES_LOG4J_PROPERTIES).exists()) {
//            PropertyConfigurator.configure(DeserializationFactory.SRC_MAIN_RESOURCES_LOG4J_PROPERTIES);
//        }
    }

    /**
     *
     * @return
     */
    public String getRepositoryURI() {
        return repositoryURI;
    }

    /**
     *
     * @param repositoryURI
     */
    public void setRepositoryURI(String repositoryURI) {
        this.repositoryURI = repositoryURI;
    }

    /**
     * Creates a new Deserialization object.
     *
     * @return the module configuration
     * @throws javax.xml.parsers.ParserConfigurationException
     */
    public ModuleDeserialization createDeserialization() throws ParserConfigurationException {
        final InfosReport infosreport = new InfosReport("Loading and validate deserialization configuration success");
        final InfosReport errorsreport = new InfosReport("Loading and validate deserialization configuration errors");
        loadGeneralConfiguration(infosreport);
        final ModuleDeserialization moduleDeserialization = new ModuleDeserialization();
        moduleDeserialization.setModulesDeserialization(modulesDeserialization);
        moduleDeserialization.setRepositoryURI(repositoryURI);
        if (modulesDeserialization != null) {
            for (final Entry<String, IModuleDeserialization> moduleDeserializationEntry : modulesDeserialization.entrySet()) {
                final Digester digester = new Digester();
                InputStream configurationFile = this.getClass().getResourceAsStream(DeserializationFactory.CONFIG_PATH);

                try {
                    digester.setXMLSchema(getSchema(moduleDeserializationEntry.getValue()));
                    digester.setValidating(true);
                    digester.setNamespaceAware(true);
                    ErrorHandlerImpl errorHandler = new ErrorHandlerImpl();
                    digester.setErrorHandler(errorHandler);
                    digester.setUseContextClassLoader(true);
                    digester.setFeature(DeserializationFactory.DYNAMIC_VALIDATION, true);
                } catch (final SAXException e1) {
                    LOGGER.error(String.format(DeserializationFactory.BAD_LOADING, moduleDeserializationEntry.getKey()), e1);
                    infosreport.addInfos(moduleDeserializationEntry.getKey(), String.format(DeserializationFactory.CORRECT_LOADING, moduleDeserializationEntry.getKey()));
                }
                if (moduleDeserializationEntry.getValue().isAutoLoad()) {
                    digester.push(moduleDeserializationEntry.getValue());
                    moduleDeserializationEntry.getValue().createConfig(digester);
                }
                try {
                    //digester.parse(configurationFile);
                    digester.parse(configurationFile);
                } catch (final IOException | SAXException e) {
                    UncatchedExceptionLogger.log(String.format(DeserializationFactory.BAD_LOADING, moduleDeserializationEntry.getKey()), e);
                    errorsreport.addInfos(moduleDeserializationEntry.getKey(), String.format(DeserializationFactory.BAD_LOADING, moduleDeserializationEntry.getKey()));
                }
                infosreport.addInfos(moduleDeserializationEntry.getKey(), String.format(DeserializationFactory.CORRECT_LOADING, moduleDeserializationEntry.getKey()));
            }
            if (!infosreport.getinfoMessages().isEmpty()) {
                LOGGER.info(infosreport.getMessages());
            }
        }
        return moduleDeserialization;
    }

    /**
     * Gets the Map<String,IModuleDeserialization> modules configuration.
     *
     * @return the Map<String,IModuleDeserialization> modules configuration
     */
    public Map<String, IModuleDeserialization> getModulesDeserialization() {
        return modulesDeserialization;
    }

    /**
     * Sets the modules configuration.
     *
     * @param modulesDeserialization the Map<String,IModuleDeserialization>
     * modules configuration
     */
    public void setModulesDeserialization(Map<String, IModuleDeserialization> modulesDeserialization) {
        this.modulesDeserialization = modulesDeserialization;
    }

    /**
     * Gets the Schema schema.
     *
     * @param moduleDeserialization the IModuleDeserialization module
     * configuration
     * @return the Schema schema
     * @throws SAXException the sAX exception
     */
    public final Schema getSchema(IModuleDeserialization moduleDeserialization) throws SAXException {
        final StreamSource schema = new StreamSource(moduleDeserialization == null ? getStreamSchema() : moduleDeserialization.getStreamSchema());
        final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        return schemaFactory.newSchema(schema);
    }

    /**
     * New integer class array.
     *
     * @return the Class[] class[]
     */
    @SuppressWarnings("rawtypes")
    public Class[] newIntegerClassArray() {
        return new Class[]{Integer.class, StringBuilder.class};
    }

    /**
     * New string class array.
     *
     * @return the Class[] class[]
     */
    @SuppressWarnings("rawtypes")
    public Class[] newStringClassArray() {
        return new Class[]{String.class, String.class};
    }

    /**
     * Post config.
     *
     * @param moduleDeserialization the module configuration
     * @return the string
     */
    @Transactional(rollbackFor = Exception.class)
    public String postConfig(IModuleDeserialization moduleDeserialization) {
        final InfosReport infosreport = new InfosReport("Configuring configuration success");
        final InfosReport errorsreport = new InfosReport("Configuring configuration errors");
        if (modulesDeserialization != null) {
            try {
                moduleDeserialization.postConfig();
                infosreport.addInfos(moduleDeserialization.getModuleName(), String.format(DeserializationFactory.CORRECT_CONFIG, moduleDeserialization.getModuleName()));
            } catch (final BusinessException e) {
                UncatchedExceptionLogger.log(String.format(DeserializationFactory.BAD_CONFIG, moduleDeserialization.getModuleName(), e.getMessage()), e);
                errorsreport.addInfos(moduleDeserialization.getModuleName(), String.format(DeserializationFactory.BAD_CONFIG, moduleDeserialization.getModuleName(), e.getMessage()));
            }
            if (!infosreport.getinfoMessages().isEmpty()) {
                LOGGER.error(infosreport.getMessages());
            }
        }
        return infosreport.buildHTMLMessages();
    }

    private void loadGeneralConfiguration(InfosReport infosReport) {
        final Digester digester = new Digester();
        //final File configurationFile = new File(getClass().getResource(DeserializationFactory.CONFIG_PATH).getPath());
        InputStream configurationFile = this.getClass().getResourceAsStream(DeserializationFactory.CONFIG_PATH);

        try {
            digester.setNamespaceAware(true);
            ErrorHandlerImpl errorHandler = new ErrorHandlerImpl();
            digester.setErrorHandler(errorHandler);
            digester.setUseContextClassLoader(true);
            digester.setFeature(DeserializationFactory.DYNAMIC_VALIDATION, true);
        } catch (final SAXException e1) {
            UncatchedExceptionLogger.log(String.format(DeserializationFactory.CORRECT_LOADING, getModuleName()), e1);
            infosReport.addInfos(getModuleName(), String.format(DeserializationFactory.CORRECT_LOADING, getModuleName()));
        } catch (ParserConfigurationException ex) {
            LoggerFactory.getLogger(DeserializationFactory.class.getName()).error(ex.getMessage(), ex);
            infosReport.addInfos(getModuleName(), String.format(DeserializationFactory.CORRECT_LOADING, getModuleName()));
        }
        if (isAutoLoad()) {
            digester.push(this);
            createConfig(digester);
        }
        try {
            //digester.parse(configurationFile);
            digester.parse(configurationFile);
        } catch (IOException | SAXException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     *
     * @param digester
     */
    public void createConfig(Digester digester) {
        digester.addCallMethod("deserialization/repositoryURI", "setRepositoryURI", 0);
    }

    /**
     *
     * @return
     */
    public String getModuleName() {
        return MODULE_NAME;
    }

    /**
     *
     * @return
     */
    public String getSchemaPath() {
        return SCHEMA_PATH;
    }

    /**
     *
     * @return
     */
    public InputStream getStreamSchema() {
        return getClass().getResourceAsStream(getSchemaPath());
    }

    /**
     *
     * @return
     */
    public Boolean isAutoLoad() {
        return true;
    }


    /**
     *
     */
    @Transactional(rollbackFor = Exception.class)
    public void deserialize() {
        try {
            File zipFile = getzipFile();
            for (Map.Entry<String, IModuleDeserialization> moduleEntry : getModulesDeserialization().entrySet()) {
                if(moduleEntry.getValue().isSkip()){
                LOGGER.info(String.format("skipping sérializationde  %s", moduleEntry.getValue().getModuleName()));
                    continue;
                }
                LOGGER.info(String.format("Serialize %s", moduleEntry.getValue().getModuleName()));
                moduleEntry.getValue().deSerialize(zipFile);
                LOGGER.info(String.format("fin de sérializationde  %s", moduleEntry.getValue().getModuleName()));
            }
        } catch (IOException | BusinessException e) {
            LoggerFactory.getLogger(DeserializationFactory.class).error(e.getMessage(), e);
        }

    }

    private File getzipFile() throws FileNotFoundException {
        final String path = String.format(PATH_FOR_DESERIALIZATION, repositoryURI);
        return new File(path);
    }
    private static class ErrorHandlerImpl implements ErrorHandler {
        
        @Override
        public void warning(final SAXParseException exception) throws SAXException {
            LOGGER.warn("parse Exception", exception);
        }
        
        @Override
        public void fatalError(final SAXParseException exception) throws SAXException {
            LOGGER.error("parse Exception", exception);
        }
        
        @Override
        public void error(final SAXParseException exception) throws SAXException {
            LOGGER.error("parse Exception", exception);
        }
    }
}
