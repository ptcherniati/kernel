package org.inra.ecoinfo.deserialization.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import org.inra.ecoinfo.deserialization.IModuleDeserialization;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class ModuleDeserialization.
 */
public class ModuleDeserialization {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModuleDeserialization.class.getName());
    private static final String PATH_FOR_SERIALIZATION = "%sserialization/serialization.zip";
    /**
     * The String repository uri.
     */
    private String repositoryURI;
    /**
     * The Map<String,IModuleDeserialization> modules configuration.
     */
    private Map<String, IModuleDeserialization> modulesDeserialization = new HashMap<>();

    /**
     * Gets the module configuration.
     *
     * @param moduleName the module name
     * @return the module configuration
     */
    public IModuleDeserialization getModuleDeserialization(String moduleName) {
        return modulesDeserialization.get(moduleName);
    }

    /**
     * Gets the Map<String,IModuleDeserialization> modules configuration.
     *
     * @return the Map<String,IModuleDeserialization> modules configuration
     */
    public Map<String, IModuleDeserialization> getModulesDeserialization() {
        return modulesDeserialization;
    }

    /**
     * Sets the modules configuration.
     *
     * @param modulesDeserialization the Map<String,IModuleDeserialization>
     * modules configuration
     */
    public void setModulesDeserialization(Map<String, IModuleDeserialization> modulesDeserialization) {
        this.modulesDeserialization = modulesDeserialization;
    }

    /**
     *
     */
    public void deSerialize() {
        try {
            File zipFile = getZipFile();
            for (Map.Entry<String, IModuleDeserialization> moduleEntry : getModulesDeserialization().entrySet()) {
                LOGGER.info("*************************************************************************");
                LOGGER.info("start module {}", moduleEntry.getKey());
                deserializeModule(moduleEntry, zipFile);
                LOGGER.info("end module {}", moduleEntry.getKey());
                LOGGER.info("*************************************************************************");
            }
        } catch (BusinessException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (FileNotFoundException e) {
            LOGGER.error(String.format("can't find file %s", String.format(PATH_FOR_SERIALIZATION, repositoryURI)), e);
        }

    }

    /**
     *
     * @param moduleEntry
     * @param zipFile
     * @throws BusinessException
     */
    public void deserializeModule(Map.Entry<String, IModuleDeserialization> moduleEntry, File zipFile) throws BusinessException {
        try {
            moduleEntry.getValue().deSerialize(zipFile);
        } catch (Exception e) {
            LOGGER.error("unknown erro", e);
        }
    }

    private File getZipFile() throws FileNotFoundException {
        final String path = String.format(PATH_FOR_SERIALIZATION, repositoryURI);
        File zipFile = new File(path);
        if (!zipFile.exists()) {
            LOGGER.error(String.format("no file %s", path));
            throw new FileNotFoundException(path);
        }
        return zipFile;
    }

    /**
     *
     * @param repositoryURI
     */
    public void setRepositoryURI(String repositoryURI) {
        this.repositoryURI = repositoryURI;
    }
}
