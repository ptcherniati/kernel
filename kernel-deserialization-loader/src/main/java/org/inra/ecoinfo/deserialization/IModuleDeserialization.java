package org.inra.ecoinfo.deserialization;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IModuleDeserialization.
 */
public interface IModuleDeserialization extends Serializable {

    /**
     * Creates the config.
     *
     * @param digester the Digester digester
     */
    void createConfig(Digester digester);

    /**
     * Gets the String module name.
     *
     * @return the String module name
     */
    String getModuleName();

    /**
     * Gets the String schema path.
     *
     * @return the String schema path
     */
    String getSchemaPath();

    /**
     * Gets the stream schema.
     *
     * @return The Stream resource of the schema
     */
    InputStream getStreamSchema();

    /**
     * Checks if is auto load.
     *
     * @return the Boolean boolean
     */
    Boolean isAutoLoad();

    /**
     * Post config.
     *
     * @throws BusinessException the business exception
     */
    void postConfig() throws BusinessException;

    /**
     * Post config.
     *
     * @param inZip
     * @throws BusinessException the business exception
     */
    void deSerialize(File inZip) throws BusinessException;

    /**
     *
     * @return
     */
    boolean isSkip();
}
