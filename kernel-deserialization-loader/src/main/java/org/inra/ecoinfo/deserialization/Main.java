/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.deserialization;

import org.inra.ecoinfo.deserialization.impl.ModuleDeserialization;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tcherniatinsky
 */
public class Main {

    /**
     *
     * @param args
     */
    @Transactional(rollbackFor = Exception.class)
    public static void main(String[] args) {
        Main p = new Main();
        p.start();
    }
    ModuleDeserialization moduleDeserialization;

    private void start() {
        ApplicationContext context
                = new ClassPathXmlApplicationContext("META-INF/spring/applicationContextDeserialization.xml");
        moduleDeserialization = (ModuleDeserialization) context.getBean("moduleDeserialization");
        moduleDeserialization.deSerialize();
    }
}
