package org.inra.ecoinfo.synthesis.jsf;

import java.io.Serializable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 * The Class ItemDatatableVariableSynthesis.
 */
public class ItemDatatableVariableSynthesis implements Serializable {

    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    /**
     * The debut @link(String).
     */
    private String debut;
    /**
     * The fin @link(String).
     */
    private String fin;
    /**
     * The variable @link(Variable).
     */
    private NodeDataSet variableNode;
    private INodeable variable;
    /**
     * The localization manager @link(ILocalizationManager).
     */

    private Boolean accessChart;
    private Boolean accessChartDetail;
    private String localizedName;
    private String localizedDescription;
    private String localizedUniteNameGetAxisName;

    /**
     * Instantiates a new item datatable variable synthesis.
     *
     * @param variableNodeContainer
     * @param debut the debut
     * @param fin the fin
     * @param accessChartDetail
     * @param accessChart
     * @param localizedName
     * @param localizedUniteNameGetAxisName
     * @param string4
     * @param localizedDescription
     * @link(ILocalizationManager) the localization manager
     * @link(Variable) the variable
     * @link(String) the debut
     * @link(String) the fin
     */
    public ItemDatatableVariableSynthesis(
            final NodeDataSet variableNodeContainer,
            final String debut, final String fin,
            final Boolean accessChart,
            final Boolean accessChartDetail,
            final String localizedName,
            final String localizedDescription,
            final String localizedUniteNameGetAxisName) {
        setAccessChart(accessChart);
        setAccessChartDetail(accessChartDetail);
        setDebut(debut);
        setFin(fin);
        setLocalizedName(localizedName);
        setLocalizedDescription(localizedDescription);
        setLocalizedUniteNameGetAxisName(localizedUniteNameGetAxisName);
        setVariableNode(variableNodeContainer);
    }

    /**
     *
     * @return
     */
    public String getLocalizedUniteNameGetAxisName() {
        return localizedUniteNameGetAxisName;
    }

    /**
     *
     * @param localizedUniteNameGetAxisName
     */
    public void setLocalizedUniteNameGetAxisName(String localizedUniteNameGetAxisName) {
        this.localizedUniteNameGetAxisName = localizedUniteNameGetAxisName;
    }

    /**
     *
     * @return
     */
    public String getLocalizedName() {
        return localizedName;
    }

    /**
     *
     * @return
     */
    public String getLocalizedDescription() {
        return localizedDescription;
    }

    /**
     * Gets the debut.
     *
     * @return the debut
     */
    public String getDebut() {
        return debut;
    }

    /**
     * Sets the debut.
     *
     * @param debut the new debut
     */
    public void setDebut(final String debut) {
        this.debut = debut;
    }

    /**
     * Gets the fin.
     *
     * @return the fin
     */
    public String getFin() {
        return fin;
    }

    /**
     * Sets the fin.
     *
     * @param fin the new fin
     */
    public void setFin(final String fin) {
        this.fin = fin;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public INodeable getVariable() {
        return variable;
    }

    /**
     * Sets the variable.
     *
     * @param variableNodeableContainer the new variable
     */
    public void setVariable(final INodeable variableNodeableContainer) {
        this.variable = variableNodeableContainer;
    }

    /**
     *
     * @param localizedName
     */
    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    /**
     *
     * @param localizedDescription
     */
    public void setLocalizedDescription(String localizedDescription) {
        this.localizedDescription = localizedDescription;
    }

    /**
     *
     * @return
     */
    public Boolean getAccessChart() {
        return accessChart;
    }

    /**
     *
     * @param accessChart
     */
    public void setAccessChart(Boolean accessChart) {
        this.accessChart = accessChart;
    }

    /**
     *
     * @return
     */
    public Boolean getAccessChartDetail() {
        return accessChartDetail;
    }

    /**
     *
     * @param accessChartDetail
     */
    public void setAccessChartDetail(Boolean accessChartDetail) {
        this.accessChartDetail = accessChartDetail;
    }

    /**
     *
     * @return
     */
    public NodeDataSet getVariableNode() {
        return variableNode;
    }

    /**
     *
     * @param variableNode
     */
    public void setVariableNode(NodeDataSet variableNode) {
        this.variableNode = variableNode;
        this.variable = this.variableNode.getRealNode().getNodeable();
    }
}
