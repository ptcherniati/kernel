package org.inra.ecoinfo.synthesis.jsf;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.synthesis.ISynthesisManager;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartModel;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 * The Class UIBeanSynthesis.
 */
@ManagedBean(name = "uiSynthesisChartRepartitionBuilder")
@ViewScoped
public class UIBeanSynthesisChartRepartitionBuilder implements Serializable {

    @ManagedProperty(value = "#{synthesisManager}")
    ISynthesisManager synthesisManager;
    @ManagedProperty(value = "#{policyManager}")
    IPolicyManager policyManager;

    Map<String, Map<String, ChartModel>> model=new HashMap<>();

    /**
     * build chart model repartition
     *
     * @param synthesisValueClass
     * @param item
     * @param language
     * @return
     * @throws ClassNotFoundException
     * @throws PersistenceException
     * @throws BusinessException
     */
    public ChartModel getChartValuesVariable(Class<GenericSynthesisValue> synthesisValueClass, ItemDatatableVariableSynthesis item, String language) throws ClassNotFoundException, PersistenceException, BusinessException {
        String context = Optional.ofNullable(item)
                .map(i->i.getVariableNode())
                .map(vn->vn.getParent())
                .map(cn->cn.getRealNode().getPath())
                .orElse(null);
        final Optional<ItemDatatableVariableSynthesis> itemOpt = Optional.ofNullable(item);
        if (!itemOpt.isPresent()) {
            return null;
        }
        if (!model.computeIfAbsent(context, k -> new HashMap<>()).containsKey(item.getVariable().getCode())) {
            LineChartModel dateModel = new LineChartModel();
            LineChartSeries series = new LineChartSeries();
            final String variable = item.getVariable().getCode();
            final LocalDateTime dateDebut = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, item.getDebut());
            final LocalDateTime dateFin = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, item.getFin());
            TreeSet values = (TreeSet) synthesisManager.getSynthesisValuesByVariableNodeableAndSiteByDatesInterval(dateDebut, dateFin, item.getVariableNode(), synthesisValueClass, Stream.of(Activities.synthese, Activities.extraction).collect(Collectors.toList()))
                    .map(sv -> sv.getDate())
                    .collect(Collectors.toCollection(() -> new TreeSet()));

            Stream.iterate(
                    dateDebut,
                    d -> d.plus(1, ChronoUnit.MONTHS)
            )
                    .limit(ChronoUnit.MONTHS.between(dateDebut, dateFin) + 1)
                    .forEach((date) -> {
                        if (values.subSet(date, date.with(TemporalAdjusters.lastDayOfMonth())).isEmpty()) {
                            series.set(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(date, "yyyy-MM-dd", Locale.forLanguageTag(language)), 0);
                            series.set(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(date.with(TemporalAdjusters.lastDayOfMonth()), "yyyy-MM-dd", Locale.forLanguageTag(language)), 0);
                        } else {
                            series.set(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(date, "yyyy-MM-dd", Locale.forLanguageTag(language)), 1);
                            series.set(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(date.with(TemporalAdjusters.lastDayOfMonth()), "yyyy-MM-dd", Locale.forLanguageTag(language)), 1);
                        }
                    });

            dateModel.addSeries(series);
            DateAxis axis = new DateAxis();

            axis.setMin(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(dateDebut, "yyyy-MM-dd", Locale.forLanguageTag(language)));
            axis.setMax(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(dateFin, "yyyy-MM-dd", Locale.forLanguageTag(language)));
            axis.setTickFormat(
                    "%b-%y");
            dateModel.getAxes()
                    .put(AxisType.X, axis);
            dateModel.setExtender(
                    "chartValuesVariableExtender");
            model.get(context).put(item.getVariable().getCode(),dateModel);
        }
        return model.get(context).get(item.getVariable().getCode());
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param synthesisManager
     */
    public void setSynthesisManager(ISynthesisManager synthesisManager) {
        this.synthesisManager = synthesisManager;
    }
}
