/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis.jsf;

import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.model.chart.ChartModel;

/**
 *
 * @author ptchernia
 */
@FunctionalInterface
public interface ISynthesisChartTrenderVariableBuilder {

    /**
     *
     * @param <SV>
     * @param item
     * @param synthesisValueClass
     * @param language
     * @return
     * @throws BusinessException
     */
    public <SV extends GenericSynthesisValue> ChartModel buildModel(ItemDatatableVariableSynthesis item, Class<SV> synthesisValueClass, String language) throws BusinessException;
}
