package org.inra.ecoinfo.synthesis.jsf;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.synthesis.ISynthesisManager;
import org.inra.ecoinfo.synthesis.ISynthesisRegister;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.primefaces.model.chart.ChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * The Class UIBeanSynthesis.
 */
@ManagedBean(name = "uiSynthesisChartTrendVariableBuilder")
@ViewScoped
public class UIBeanSynthesisChartTrendVariableBuilder implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(UIBeanSynthesisChartTrendVariableBuilder.class);

    private static ISynthesisChartTrenderVariableBuilder defaultSynthesisChartTrenderVariableBuilder;

    @ManagedProperty(value = "#{synthesisManager}")
    ISynthesisManager synthesisManager;

    @ManagedProperty(value = "#{synthesisRegister}")
    ISynthesisRegister synthesisRegister;

    Map<String, Map<String, ChartModel>> model=new HashMap<>();

    /**
     *
     * @param synthesisManager
     */
    public void setSynthesisManager(ISynthesisManager synthesisManager) {
        this.synthesisManager = synthesisManager;
    }

    /**
     *
     * @param synthesisValueClass
     * @param context
     * @param synthesisValueCla
     * @param item
     * @param language
     * @return
     * @throws ClassNotFoundException
     * @throws PersistenceException
     * @throws BusinessException
     */
    public <SV extends GenericSynthesisValue> ChartModel getGraphicVariable(Class<SV> synthesisValueClass, ItemDatatableVariableSynthesis item, String language) throws ClassNotFoundException, PersistenceException, BusinessException {
        String context = Optional.ofNullable(item)
                .map(i->i.getVariableNode())
                .map(vn->vn.getParent())
                .map(cn->cn.getPath())
                .orElse(null);
        final Optional<ItemDatatableVariableSynthesis> itemOpt = Optional.ofNullable(item);
        if (!itemOpt.isPresent()) {
            return null;
        }
        if (!model.computeIfAbsent(context, k -> new HashMap<>()).containsKey(item.getVariable().getCode())) {
            ISynthesisChartTrenderVariableBuilder synthesisChartTrenderVariableBuilder = itemOpt
                    .map(
                            i -> i.getVariableNode()
                    )
                    .map(
                            nds -> nds.getNodeByNodeableTypeResource(DataType.class)
                    )
                    .map(
                            nds -> nds.getNodeable()
                    )
                    .map(
                            ndb -> ndb.getCode()
                    )
                    .map(
                            datatypeCode -> synthesisRegister.retrieveMetadataSynthesisDatatype(datatypeCode)
                    )
                    .map(synthesisDatatype -> synthesisDatatype.getSynthesisChartTrenderVariableBuilder())
                    .map(synthesisChartTrenderVariableBuilderBeanName -> getSynthesisChartTrenderVariableBuilder(synthesisChartTrenderVariableBuilderBeanName))
                    .orElseGet(() -> getDefaultSynthesisChartTrenderVariableBuilder());
            model.get(context).put(item.getVariable().getCode(), synthesisChartTrenderVariableBuilder.buildModel(item, synthesisValueClass, language));
        }
        return model.get(context).get(item.getVariable().getCode());
    }

    protected ISynthesisChartTrenderVariableBuilder getSynthesisChartTrenderVariableBuilder(String synthesisChartTrenderVariableBuilderBeanName) throws BeansException {
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            ISynthesisChartTrenderVariableBuilder synthesisChartTrenderVariableBuilder = (ISynthesisChartTrenderVariableBuilder) webApplicationContext.getBean(synthesisChartTrenderVariableBuilderBeanName);
            return synthesisChartTrenderVariableBuilder == null ? getDefaultSynthesisChartTrenderVariableBuilder() : synthesisChartTrenderVariableBuilder;
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            return getDefaultSynthesisChartTrenderVariableBuilder();
        }
    }

    /**
     *
     * @param synthesisRegister
     */
    public void setSynthesisRegister(ISynthesisRegister synthesisRegister) {
        this.synthesisRegister = synthesisRegister;
    }

    protected ISynthesisChartTrenderVariableBuilder getDefaultSynthesisChartTrenderVariableBuilder() {
        if (defaultSynthesisChartTrenderVariableBuilder == null) {
            defaultSynthesisChartTrenderVariableBuilder = new DefaultSynthesisChartTrendVariableBuilder(synthesisManager);
        }
        return defaultSynthesisChartTrenderVariableBuilder;
    }

}
