/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.synthesis.jsf;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang.StringEscapeUtils;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.synthesis.ISynthesisManager;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartModel;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author ptchernia
 */
public class DefaultSynthesisChartTrendVariableBuilder implements ISynthesisChartTrenderVariableBuilder{
    ISynthesisManager synthesisManager;

    /**
     *
     * @param ism
     */
    public DefaultSynthesisChartTrendVariableBuilder(ISynthesisManager synthesisManager) {
        this.synthesisManager = synthesisManager;
    }

    /**
     *
     */
    public DefaultSynthesisChartTrendVariableBuilder() {
    }

    /**
     *
     * @param <SV>
     * @param item
     * @param synthesisValueClass
     * @param language
     * @return
     * @throws BusinessException
     */
    public <SV extends GenericSynthesisValue> ChartModel buildModel(ItemDatatableVariableSynthesis item, Class<SV> synthesisValueClass, String language) throws BusinessException {
        String context = Optional.ofNullable(item)
                .map(i->i.getVariableNode())
                .map(vn->vn.getParent())
                .map(cn->cn.getPath())
                .orElse(null);
        LineChartModel dateModel = new LineChartModel();
        Map<String, LineChartSeries> seriesMap = new HashMap();
        Map<String, Float> seriesMapNumber = new HashMap();
        LineChartSeries series = new LineChartSeries();
        final String variable = item.getVariable().getCode();
        final LocalDateTime dateDebut = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, item.getDebut());
        final LocalDateTime dateFin = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, item.getFin());

        synthesisManager.getSynthesisValuesByVariableNodeableAndSiteByDatesInterval(dateDebut, dateFin, item.getVariableNode(), (Class<GenericSynthesisValue>) synthesisValueClass, Stream.of(Activities.extraction).collect(Collectors.toList()))
                .forEach(sv -> addValue(sv, seriesMap, series, seriesMapNumber, dateModel, Locale.forLanguageTag(language)));

        DateAxis axis = new DateAxis();
        Duration duration = Duration.between(dateDebut, dateFin);
        duration = duration.dividedBy(20);
        axis.setMin(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(dateDebut.minus(duration), "yyyy-MM-dd", Locale.forLanguageTag(language)));
        axis.setMax(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(dateFin.plus(duration), "yyyy-MM-dd", Locale.forLanguageTag(language)));
        dateModel.getAxes().put(AxisType.X, axis);
        dateModel.setZoom(true);
        if (seriesMapNumber.size() > 0) {
            dateModel.getAxis(AxisType.Y).setMin(0);
            final Float max = seriesMapNumber.values().stream().max(Float::compare).map(f -> f + 0.1F).orElse(0.1F);
            dateModel.getAxis(AxisType.Y).setMax(max);
            dateModel.setLegendPosition("e");
            dateModel.setExtender("graphicMeanVariableChartExtender");
        } else {
            dateModel.setExtender("graphicVariableChartExtender");
        }
        return dateModel;
    }

    protected void addValue(GenericSynthesisValue sv, Map<String, LineChartSeries> seriesMap, LineChartSeries serieSimple, Map<String, Float> seriesMapNumber, LineChartModel dateModel, Locale locale) {
        if (sv.getIsMean()) {
            String valueString = sv.getValueString();
            LineChartSeries serie = seriesMap.get(valueString);
            if (serie == null) {
                serie = new LineChartSeries(StringEscapeUtils.escapeJavaScript(valueString));
                dateModel.addSeries(serie);
                serie.setShowLine(false);
                seriesMap.put(valueString, serie);
                seriesMapNumber.put(valueString, (seriesMapNumber.size() + 1) / 10F);
            }
            serie.set(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(sv.getDate(), "yyyy-MM-dd", locale), seriesMapNumber.get(valueString));
        } else {
            if (!dateModel.getSeries().contains(serieSimple)) {
                serieSimple.setShowLine(false);
                dateModel.addSeries(serieSimple);
            }
            serieSimple.set(DateUtil.getUTCDateTextWithLocaleFromLocalDateTime(sv.getDate(), "yyyy-MM-dd", locale), sv.getValueFloat());
        }
    }

    /**
     *
     * @param synthesisManager
     */
    public void setSynthesisManager(ISynthesisManager synthesisManager) {
        this.synthesisManager = synthesisManager;
    }
}
