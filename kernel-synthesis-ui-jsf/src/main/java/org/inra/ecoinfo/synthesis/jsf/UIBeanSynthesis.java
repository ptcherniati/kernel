package org.inra.ecoinfo.synthesis.jsf;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Transient;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.menu.jsf.UIBeanMenu;
import org.inra.ecoinfo.mga.business.composite.FlatNode;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.caching.ITreeApplicationCacheManager;
import org.inra.ecoinfo.mga.caching.ITreeSessionCacheManager;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.ActivityCondition;
import org.inra.ecoinfo.mga.enums.WhichUser;
import org.inra.ecoinfo.mga.managedbean.IBeanTreeNodeBuilder;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.managedbean.UIBeanActionTree;
import org.inra.ecoinfo.mga.managedbean.UITreeNode;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.synthesis.ILocalizedFormatter;
import org.inra.ecoinfo.synthesis.ISynthesisDAO;
import org.inra.ecoinfo.synthesis.ISynthesisManager;
import org.inra.ecoinfo.synthesis.ISynthesisRegister;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.component.api.UITree;
import org.primefaces.component.tree.Tree;
import org.primefaces.component.tree.TreeRenderer;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * The Class UIBeanSynthesis.
 */
@ManagedBean(name = "uiSynthesis")
@ViewScoped
public class UIBeanSynthesis implements Serializable, IBeanTreeNodeBuilder {

    /**
     * The Constant NAVIGATION_SYNTHESIS_HTML @link(String).
     */
    public static final String NAVIGATION_SYNTHESIS_HTML = "synthesisManagement.jsf";
    /**
     * The Constant ACTION_NAVIGATE @link(String).
     */
    private static final String ACTION_NAVIGATE = "synthesis";
    /**
     * The Constant serialVersionUID @link(long).
     */
    private static final long serialVersionUID = 1L;
    private static final String ACTION_NAVIGATE_PUBLIC = "synthesisPublic";
    private final Map<Long, TreeNode> cashMap = new HashMap();

    @ManagedProperty(value = "#{uiBeanActionTree}")
    private UIBeanActionTree uiBeanActionTree;
    @ManagedProperty(value = "#{uiMenu}")
    private UIBeanMenu uiMenu;
    @ManagedProperty(value = "#{localizedFormatterForVariableNameGetDisplay}")
    private ILocalizedFormatter<INodeable> localizedFormatterForVariableNameGetDisplay;
    @ManagedProperty(value = "#{localizedFormatterForVariableNameGetDescription}")
    private ILocalizedFormatter<INodeable> localizedFormatterForVariableNameGetDescription;
    @ManagedProperty(value = "#{localizedFormatterForUniteNameGetAxisName}")
    private ILocalizedFormatter<INodeable> localizedFormatterForUniteNameGetAxisName;

    @ManagedProperty(value = "#{synthesisDAO}")
    ISynthesisDAO synthesisDAO;
    @ManagedProperty(value = "#{synthesisRegister}")
    ISynthesisRegister synthesisRegister;
    @ManagedProperty(value = "#{treeSessionCacheManager}")
    ITreeSessionCacheManager sessionCacheManager;

    /**
     * The current items datatable variables synthesis.
     * @link(List<ItemDatatableVariableSynthesis>).
     */
    private List<ItemDatatableVariableSynthesis> currentItemsDatatableVariablesSynthesis;
    /**
     * The current selection @link(TreeNodeSynthesis).
     */
    private IFlatNode currentSelection;
    /**
     * The item datatable variable selected.
     * @link(ItemDatatableVariableSynthesis).
     */
    private ItemDatatableVariableSynthesis itemDatatableVariableSelected;
    /**
     * The localization manager @link(ILocalizationManager).
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;
    @ManagedProperty(value = "#{treeApplicationCacheManager}")
    private ITreeApplicationCacheManager applicationCacheManager;
    private int lastTreeHash = -1;

    /**
     * The tree node location @link(UITreeNode).
     */
    @Transient
    private TreeNode treeNodeLocation;
    /**
     * The tree nodes synthesis @link(List<TreeNodeSynthesis>).
     */
    @Transient
    private TreeNode treeNodesSynthesis;
    @Transient
    private TreeNode defaultTreeNodesSynthesis;
    /**
     * The tree selection @link(UITree).
     */
    @Transient
    private UITree treeSelection;
    /**
     * The security context @link(IPolicyManager).
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;
    /**
     * The synthesis manager @link(ISynthesisManager).
     */
    @ManagedProperty(value = "#{synthesisManager}")
    ISynthesisManager synthesisManager;
    String currentDatatypeName = "";
    private boolean isGenericDatatypeCurrentNode;

    /**
     *
     * @param localizedFormatterForUniteNameGetAxisName
     */
    public void setLocalizedFormatterForUniteNameGetAxisName(ILocalizedFormatter<INodeable> localizedFormatterForUniteNameGetAxisName) {
        this.localizedFormatterForUniteNameGetAxisName = localizedFormatterForUniteNameGetAxisName;
    }

    /**
     *
     * @param synthesisRegister
     */
    public void setSynthesisRegister(ISynthesisRegister synthesisRegister) {
        this.synthesisRegister = synthesisRegister;
    }

    /**
     *
     * @param synthesisDAO
     */
    public void setSynthesisDAO(ISynthesisDAO synthesisDAO) {
        this.synthesisDAO = synthesisDAO;
    }

    /**
     *
     * @param localizedFormatterForVariableNameGetDisplay
     */
    public void setLocalizedFormatterForVariableNameGetDisplay(ILocalizedFormatter<INodeable> localizedFormatterForVariableNameGetDisplay) {
        this.localizedFormatterForVariableNameGetDisplay = localizedFormatterForVariableNameGetDisplay;
    }

    /**
     *
     * @param localizedFormatterForVariableNameGetDescription
     */
    public void setLocalizedFormatterForVariableNameGetDescription(ILocalizedFormatter<INodeable> localizedFormatterForVariableNameGetDescription) {
        this.localizedFormatterForVariableNameGetDescription = localizedFormatterForVariableNameGetDescription;
    }

    /**
     *
     * @param applicationCacheManager
     */
    public void setApplicationCacheManager(ITreeApplicationCacheManager applicationCacheManager) {
        this.applicationCacheManager = applicationCacheManager;
    }

    /**
     *
     * @param uiBeanActionTree
     */
    public void setUiBeanActionTree(UIBeanActionTree uiBeanActionTree) {
        this.uiBeanActionTree = uiBeanActionTree;
    }

    /**
     * Builds the current items datatable variables synthesis.
     *
     * @param datasetNodeContextId
     */
    public void buildCurrentItemsDatatableVariablesSynthesis(Long datasetNodeContextId) {
        currentItemsDatatableVariablesSynthesis = new LinkedList<>();
        if (!Strings.isNullOrEmpty(currentDatatypeName)) {
            final List<NodeDataSet> variablesNodes = synthesisManager.getAllRestrictedAvailablesVariablesSynthesisNodes(currentDatatypeName, datasetNodeContextId);
            buildItemsDatatableVariablesSynthesis(currentItemsDatatableVariablesSynthesis, variablesNodes);
        }
    }

    /**
     *
     * @param url
     * @return
     */
    public String encodeUrl(String url) {
        try {
            return URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            LoggerFactory.logger(UIBeanSynthesis.class).error(String.format("can't encode url %s", url), ex);
            return url;
        }
    }

    /**
     *
     * @param node
     * @param i
     * @return
     */
    public String getDatatypeMessage(ITreeNode<IFlatNode> node, int i) throws ClassNotFoundException {
        String datatypeNameFromNode = getDatatypeNameFromNode(node);
        return synthesisManager.getSynthesisDatatypeForDatatypeAndNode(datatypeNameFromNode, node.getData().getLeafId())
                .map(std
                        -> DateUtil.getUTCDateTextFromLocalDateTime(i == 0 ? std.getMinDate() : std.getMaxDate(), DateUtil.DD_MM_YYYY))
                .orElse("");
    }

    /**
     *
     * @param n
     * @param option
     */
    public void collapsingORexpanding(TreeNode n, boolean option) {
        n.setExpanded(option);
        n.setSelected(false);
    }

    /**
     * Gets the can synchronize.
     *
     * @return the can synchronize
     */
    public Boolean getCanSynchronize() {
        return policyManager.isRoot();
    }

    /**
     * Gets the current items datatable variables synthesis.
     *
     * @return the current items datatable variables synthesis
     */
    public List<ItemDatatableVariableSynthesis> getCurrentItemsDatatableVariablesSynthesis() {
        return currentItemsDatatableVariablesSynthesis;
    }

    /**
     * Gets the current selection.
     *
     * @return the current selection
     */
    public IFlatNode getCurrentSelection() {
        return currentSelection;
    }

    /**
     * Sets the current selection.
     *
     * @param currentSelection the new current selection
     */
    public void setCurrentSelection(final IFlatNode currentSelection) {
        this.currentSelection = currentSelection;
    }

    /**
     * Gets the item datatable variable selected.
     *
     * @return the item datatable variable selected
     */
    public ItemDatatableVariableSynthesis getItemDatatableVariableSelected() {
        return itemDatatableVariableSelected;
    }

    /**
     * Sets the item datatable variable selected.
     *
     * @param itemDatatableVariableSelected the new item datatable variable
     * selected
     */
    public void setItemDatatableVariableSelected(final ItemDatatableVariableSynthesis itemDatatableVariableSelected) {
        this.itemDatatableVariableSelected = itemDatatableVariableSelected;
    }

    /**
     * Gets the tree node location.
     *
     * @return the tree node location
     */
    public TreeNode getTreeNodeLocation() {
        return treeNodeLocation;
    }

    /**
     * Sets the tree node location.
     *
     * @param treeNodeLocation the new tree node location
     */
    public void setTreeNodeLocation(final TreeNode treeNodeLocation) {
        this.treeNodeLocation = treeNodeLocation;
    }

    /**
     *
     * @return
     */
    public TreeNode getSelectedTreeNode() {
        return uiBeanActionTree.getSelectedTreeNode();
    }

    /**
     *
     * @param selectedTreeNode
     * @throws BusinessException
     */
    public void setSelectedTreeNode(TreeNode selectedTreeNode) throws BusinessException {
        if (selectedTreeNode != null && !selectedTreeNode.getRowKey().equals(uiBeanActionTree.getSelectedTreeNode().getRowKey())) {
            uiBeanActionTree.setSelectedTreeNode(selectedTreeNode);
            selectionChanged((ITreeNode<IFlatNode>) selectedTreeNode.getData());
        }
    }

    /**
     *
     * @param selectedTreeNode
     * @throws BusinessException
     */
    public void selectedTreeNode(Object selectedTreeNode) throws BusinessException {
        if (selectedTreeNode != null && !((TreeNode) selectedTreeNode).getRowKey().equals(uiBeanActionTree.getSelectedTreeNode().getRowKey())) {
            uiBeanActionTree.setSelectedTreeNode(((TreeNode) selectedTreeNode));
            selectionChanged((ITreeNode<IFlatNode>) ((TreeNode) selectedTreeNode).getData());
        }
    }

    /**
     * Gets the tree nodes synthesis. the tree node synthesis is build again
     * when buildSynthesis has been done from last gettting tree. The skeleton
     * construction of tree id done in buildSynthesis function
     *
     * @return the tree nodes synthesis
     */
    public TreeNode treeNodesSynthesis() {
        if (treeNodesSynthesis == null) {
            final Integer codeConfiguration = uiMenu.getConfigurationNumber() == null ? AbstractMgaIOConfigurator.SYNTHESIS_CONFIGURATION : uiMenu.getConfigurationNumber();
            String activities = Stream.of(Activities.synthese, Activities.extraction)
                    .map(a -> a.name())
                    .collect(Collectors.joining(","));
            uiBeanActionTree.loadTreeForUserAccordingLeafsRestrictions(codeConfiguration, WhichUser.CURRENTUSER.name(), ActivityCondition.ATLEAST.name(), activities);
            defaultTreeNodesSynthesis = uiBeanActionTree.getCurrentTreeNode();
            uiBeanActionTree.setSelectedTreeNode(new DefaultTreeNode());
            uiBeanActionTree.updateColumns();
            treeNodesSynthesis = uiBeanActionTree.getCurrentTreeNode();
        }
        return treeNodesSynthesis;
    }

    protected void filterTreeNodeSynthesis(Set<String> validPathes, List<TreeNode> children) {
        for (Iterator<TreeNode> iterator = children.iterator(); iterator.hasNext();) {
            TreeNode node = iterator.next();
            final FlatNode flatNode = (FlatNode) ((ITreeNode) node.getData()).getData();
            if (node.isLeaf()) {
                if (validPathes.contains(flatNode.getLeafNodePath())) {
                    node.setSelected(node.isSelected());
                } else {
                    iterator.remove();
                }
            } else {
                filterTreeNodeSynthesis(validPathes, node.getChildren());
                if (node.getChildren().isEmpty()) {
                    iterator.remove();
                }
            }
        }
    }

    protected void getTreeIds(Set<String> validLeafIds, TreeNode node) {
        final FlatNode flatNode = (FlatNode) ((ITreeNode) node.getData()).getData();
        if (node.isLeaf() && flatNode != null) {
            validLeafIds.add(flatNode.getLeafNodePath());
        }
        node.getChildren().forEach(child -> getTreeIds(validLeafIds, child));
    }

    /**
     *
     * @return
     */
    public TreeNode getTreeNodesSynthesis() {
        return treeNodesSynthesis != null ? treeNodesSynthesis : treeNodesSynthesis();
    }

    /**
     * Gets the tree selection.
     *
     * @return the tree selection
     */
    public UITree getTreeSelection() {
        return treeSelection;
    }

    /**
     * Sets the tree selection.
     *
     * @param treeSelection the new tree selection
     */
    public void setTreeSelection(final UITree treeSelection) {
        this.treeSelection = treeSelection;
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public String navigate() {
        return UIBeanSynthesis.ACTION_NAVIGATE;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the security context.
     *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Sets the synthesis manager.
     *
     * @param synthesisManager the new synthesis manager
     */
    public void setSynthesisManager(final ISynthesisManager synthesisManager) {
        this.synthesisManager = synthesisManager;
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public String navigatePublic() {
        return UIBeanSynthesis.ACTION_NAVIGATE_PUBLIC;
    }

    /**
     * Synchronize.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public String synchronize() throws BusinessException {
        synthesisManager.buildSynthesis();
        currentSelection = null;
        treeNodesSynthesis = null;
        final FacesContext context = FacesContext.getCurrentInstance();
        currentItemsDatatableVariablesSynthesis = null;
        treeSelection = (UITree) context.getApplication().createComponent(context, Tree.COMPONENT_TYPE, TreeRenderer.class.getName());
        treeNodesSynthesis = null;
        return null;
    }

    /**
     * Builds the items datatable variables synthesis.
     *
     * @param currentItemsDatatableVariablesSynthesis the current items
     * datatable variables synthesis
     * @param variableNodes the variables
     * @param siteName the site name
     * @link(List<ItemDatatableVariableSynthesis>) the current items datatable
     * variables synthesis
     * @link(List<Variable>) the variables
     * @link(String) the site name
     */
    private <T extends INodeable> void buildItemsDatatableVariablesSynthesis(final List<ItemDatatableVariableSynthesis> currentItemsDatatableVariablesSynthesis, final List<NodeDataSet> variableNodes) {
        variableNodes.stream().forEach((variableNodeContainer) -> {
            try {
                RealNode variableRealNodeContainer = variableNodeContainer.getRealNode();
                INodeable variableNodeableContainer = variableRealNodeContainer.getNodeable();
                IntervalDate dates = synthesisManager.getDateByContextAndVariable(variableNodeContainer.getId(), currentDatatypeName);
                Boolean accessChart = policyManager.isRoot() || synthesisManager.hasRoleForNodeIdAndActivity(variableNodeContainer.getId(), Activities.synthese);
                Boolean accessChartDetail = policyManager.isRoot() || synthesisManager.hasRoleForNodeIdAndActivity(variableNodeContainer.getId(), Activities.extraction);
                if (accessChart || accessChartDetail) {
                    Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
                    String localizedName = localizedFormatterForVariableNameGetDisplay.format(variableNodeableContainer, locale);
                    String localizedDescription = localizedFormatterForVariableNameGetDescription.format(variableNodeableContainer, locale);
                    String localizedUniteNameGetAxisName = localizedFormatterForUniteNameGetAxisName.format(variableNodeableContainer, locale);
                    final ItemDatatableVariableSynthesis itemDatatableVariableSynthesis = new ItemDatatableVariableSynthesis(
                            variableNodeContainer,
                            DateUtil.getUTCDateTextFromLocalDateTime(dates.getBeginDate(), DateUtil.DD_MM_YYYY),
                            DateUtil.getUTCDateTextFromLocalDateTime(dates.getEndDate(), DateUtil.DD_MM_YYYY),
                            accessChart,
                            accessChartDetail,
                            localizedName, localizedDescription, localizedUniteNameGetAxisName);
                    currentItemsDatatableVariablesSynthesis.add(itemDatatableVariableSynthesis);
                }
            } catch (final BusinessException e) {
                UncatchedExceptionLogger.logUncatchedException("uncatched exception in buildItemsDatatableVariablesSynthesis", e);
            }
        });
    }

    protected String getDatatypeNameFromNode(ITreeNode<IFlatNode> selectedTreeNode) {
        IFlatNode flatNode = Optional.ofNullable(selectedTreeNode)
                .map(n -> n.getData())
                .orElse((IFlatNode) null);
        if (flatNode == null) {
            return "";
        }
        return DataType.class.equals(flatNode.getTypeResource()) ? flatNode.getCode() : getDatatypeNameFromNode(selectedTreeNode.getParent());
    }

    private void selectionChanged(ITreeNode<IFlatNode> selectedTreeNode) {
        currentSelection = selectedTreeNode.getData();
        if (selectedTreeNode.getChildren().isEmpty() && policyManager.getCurrentUser() != null) {
            currentDatatypeName = getDatatypeNameFromNode(selectedTreeNode);
            if (synthesisManager.isGenericdatatype(currentDatatypeName)) {
                isGenericDatatypeCurrentNode = true;
                return;
            }
            isGenericDatatypeCurrentNode = false;
            buildCurrentItemsDatatableVariablesSynthesis(selectedTreeNode.getData().getLeafId());
        }
    }

    /**
     *
     * @return
     */
    public String getCurrentDatatypeName() {
        return currentDatatypeName;
    }

    /**
     *
     * @param <T>
     * @param flatNode
     * @return
     */
    @Override
    public <T extends IFlatNode> TreeNode getOrCreateTreeNode(ITreeNode<T> flatNode) {
        if (flatNode == null || flatNode.getData() == null) {
            return cashMap.computeIfAbsent(Long.MIN_VALUE, k -> new UITreeNode(flatNode, this, cashMap));
        }
        return cashMap.computeIfAbsent(flatNode.getData().getId(), k -> new UITreeNode(flatNode, this, cashMap));
    }

    /**
     *
     * @param datatypeName
     * @return
     * @throws ClassNotFoundException
     */
    public Class<GenericSynthesisValue> getSynthesisValueClass(String datatypeName) throws ClassNotFoundException {
        final String prefix = synthesisRegister.retrieveMetadataSynthesisDatatype(datatypeName).getPrefix();
        return (Class<GenericSynthesisValue>) Class.forName(String.format("%s.%s.SynthesisValue", synthesisRegister.getBasePackage(), prefix));
    }

    public void setUiMenu(UIBeanMenu uiMenu) {
        this.uiMenu = uiMenu;
    }

    public void setSessionCacheManager(ITreeSessionCacheManager sessionCacheManager) {
        this.sessionCacheManager = sessionCacheManager;
    }

    public boolean isGenericDatatypeCurrentNode() {
        return isGenericDatatypeCurrentNode;
    }
}
