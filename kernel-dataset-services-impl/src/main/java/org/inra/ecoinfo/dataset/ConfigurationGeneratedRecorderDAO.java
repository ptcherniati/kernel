/**
 * OREILacs project - see LICENCE.txt for use created: 12 août 2009 11:23:00
 */
package org.inra.ecoinfo.dataset;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.config.impl.DataTypeDescription;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.localization.IInternationalizable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The Class ConfigurationGeneratedRecorderDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class ConfigurationGeneratedRecorderDAO implements IRecorderDAO, ApplicationContextAware, IInternationalizable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationGeneratedRecorderDAO.class);

    /**
     *
     */
    public static final String LOGGER_ERROR = "Method %s -> %s";

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.messages";

    /**
     * The Constant CST_VERSION_FILE_SETTER @link(String).
     */
    private static final String CST_VERSION_FILE_SETTER = "setVersionFileDAO";
    /**
     * The Constant PROPERTY_MSG_NO_SETTER @link(String).
     */
    private static final String PROPERTY_MSG_NO_SETTER = "PROPERTY_MSG_NO_SETTER";
    /**
     * The Constant PROPERTY_MSG_UNAVALAIBLE_RECORDER @link(String).
     */
    private static final String PROPERTY_MSG_UNAVALAIBLE_RECORDER = "PROPERTY_MSG_UNAVALAIBLE_RECORDER";
    /**
     * The application context @link(ApplicationContext).
     */
    private ApplicationContext applicationContext;
    /**
     * The localization manager @link(ILocalizationManager).
     */
    private ILocalizationManager localizationManager;
    /**
     * The configuration @link(Configuration).
     */
    protected IDatasetConfiguration configuration;

    /**
     *
     */
    protected IRecorder defaultRecorder;
    /**
     * The datatypes recorders map @link(Map<String,IRecorder>).
     */
    protected Map<String, IRecorder> datatypesRecordersMap = new HashMap<>();

    /**
     * Gets the recorder by data type code.
     *
     * @param datatypeCode the datatype code
     * @return the recorder by data type code
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.dataset.IRecorderDAO#getRecorderByDataTypeCode(java.lang.String)
     */
    @Override
    public IRecorder getRecorderByDataTypeCode(final String datatypeCode) throws PersistenceException {
        final IRecorder datatypeRecorder = datatypesRecordersMap.getOrDefault(datatypeCode, defaultRecorder);
        if (datatypeRecorder == null) {
            LOGGER.error(String.format(localizationManager.getMessage(ConfigurationGeneratedRecorderDAO.BUNDLE_SOURCE_PATH, ConfigurationGeneratedRecorderDAO.PROPERTY_MSG_UNAVALAIBLE_RECORDER), datatypeCode));
            throw new PersistenceException(String.format(localizationManager.getMessage(ConfigurationGeneratedRecorderDAO.BUNDLE_SOURCE_PATH, ConfigurationGeneratedRecorderDAO.PROPERTY_MSG_UNAVALAIBLE_RECORDER), datatypeCode));
        } else {
            return datatypeRecorder;
        }
    }

    /**
     * Inits the.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @SuppressWarnings("rawtypes")
    public void init() throws BusinessException {
        for (final DataTypeDescription datatype : configuration.getDataTypes()) {
            Object datatypeRecorderInstance;
            Class datatypeRecorderClass = null;
            try {
                datatypeRecorderClass = Class.forName(datatype.getRecorder());
                datatypeRecorderInstance = datatypeRecorderClass.newInstance();
            } catch (final ClassNotFoundException e) {
                LOGGER.warn(String.format(LOGGER_ERROR, "init", e.getMessage()));
                datatypeRecorderInstance = applicationContext.getBean(datatype.getRecorder());
                datatypeRecorderClass = datatypeRecorderInstance.getClass();
            } catch (InstantiationException | IllegalAccessException ex) {
                LOGGER.warn(String.format("class or recorder not found %s", datatype.getRecorder()));
                throw new BusinessException(ex);
            }
            final Method versionFileSetterMethod = Optional.ofNullable(retrieveMethodByName(ConfigurationGeneratedRecorderDAO.CST_VERSION_FILE_SETTER, datatypeRecorderClass))
                    .orElseThrow(() -> new BusinessException(String.format("no setter for %s on %s ", ConfigurationGeneratedRecorderDAO.CST_VERSION_FILE_SETTER, datatype.getRecorder())));
            final Object versionFileDAO = applicationContext.getBean(IVersionFileDAO.ID_BEAN);
            try {
                versionFileSetterMethod.invoke(datatypeRecorderInstance, versionFileDAO);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                throw new BusinessException(ex);
            }
            for (final String daoReference : datatype.getDaos()) {
                try {
                    final Object springDAO = applicationContext.getBean(daoReference);
                    final StringBuilder stringBuilder = new StringBuilder(daoReference);
                    final String daoName = stringBuilder.replace(0, 1, stringBuilder.substring(0, 1).toUpperCase()).toString();
                    final Method daoSetterMethod = Optional.ofNullable(retrieveMethodByName("set" + daoName, datatypeRecorderClass))
                            .orElseThrow(() -> throwExceptionIfNull(daoName, datatype));
                    daoSetterMethod.invoke(datatypeRecorderInstance, springDAO);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    throw new BusinessException(ex);
                }
            }
            final IRecorder datatypeRecorder = (IRecorder) datatypeRecorderInstance;
            datatypeRecorder.setLocalizationManager((ILocalizationManager) applicationContext.getBean("localizationManager"));
            datatypesRecordersMap.put(datatype.getCode(), datatypeRecorder);
        }
    }

    /**
     * Sets the application context.
     *
     * @param applicationContext the new application context
     * @see
     * org.inra.ecoinfo.dataset.ore.data.dao.IRecorderDAO#getRecorderByDataTypeName(java.lang.String)
     */
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        setLocalizationManager((ILocalizationManager) applicationContext.getBean("localizationManager"));
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final IDatasetConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param defaultRecorder
     */
    public void setDefaultRecorder(IRecorder defaultRecorder) {
        this.defaultRecorder = defaultRecorder;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see
     * org.inra.ecoinfo.localization.IInternationalizable#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    private BusinessException throwExceptionIfNull(final String daoName, final DataTypeDescription datatype) {
        return new BusinessException(new NoSuchMethodException(String.format(localizationManager.getMessage(ConfigurationGeneratedRecorderDAO.BUNDLE_SOURCE_PATH, ConfigurationGeneratedRecorderDAO.PROPERTY_MSG_NO_SETTER), daoName, datatype.getRecorder())));

    }

    /**
     * Retrieve method by name.
     *
     * @param name the name
     * @param clazz the clazz
     * @return the method
     * @link(String) the name
     * @link(Class) the clazz
     */
    @SuppressWarnings("rawtypes")
    private Method retrieveMethodByName(final String name, final Class clazz) {
        for (final Method method : clazz.getMethods()) {
            if (method.getName().equals(name)) {
                return method;
            }
        }
        return null;
    }
}
