/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.dataset.versioning.impl;

import com.Ostermiller.util.CSVParser;
import java.util.Optional;
import org.inra.ecoinfo.dataset.*;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptchernia
 */
public class DefaultRecorder extends AbstractRecorder {

    /**
     *
     */
    protected IVersionFileDAO versionFileDAO;
    ITestFormat testFormat;
    IProcessRecord processRecord;
    IDeleteRecord deleteRecord;

    @Override
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    @Override
    public void testFormat(VersionFile versionFile, String encoding) throws BusinessException {
        Optional<ITestFormat> opt = Optional.ofNullable(testFormat);
        if (opt.isPresent()) {
            try {
                opt.get().testFormat(versionFile, encoding);
            } catch (BadFormatException ex) {
                throw new BusinessException(ex);
            }
        }
    }

    @Override
    protected void processRecord(CSVParser parser, VersionFile versionFile,
            String fileEncoding) throws BusinessException {
        Optional<IProcessRecord> opt = Optional.ofNullable(processRecord);
        if (opt.isPresent()) {
            opt.get().processRecord(parser, versionFile, fileEncoding);
        }
    }

    @Override
    public void deleteRecords(VersionFile versionFile) throws BusinessException {
        Optional<IDeleteRecord> opt = Optional.ofNullable(deleteRecord);
        if (opt.isPresent()) {
            opt.get().deleteRecord(versionFile);
        }
    }

    /**
     *
     * @param testFormat
     */
    public void setTestFormat(ITestFormat testFormat) {
        this.testFormat = testFormat;
    }

    /**
     *
     * @param processRecord
     */
    public void setProcessRecord(IProcessRecord processRecord) {
        this.processRecord = processRecord;
    }

    /**
     *
     * @param deleteRecord
     */
    public void setDeleteRecord(IDeleteRecord deleteRecord) {
        this.deleteRecord = deleteRecord;
    }

}
