/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.dataset.versioning.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.dataset.IProcessRecord;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IGenericDatatypeDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.GenericDatatype;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptchernia
 */
public class GenericDatatypeProcessRecord implements IProcessRecord {

    /**
     *
     */
    public static final String BUNDLE_PATH = "org.inra.ecoinfo.dataset.versioning.impl.messages";

    /**
     *
     */
    public static final String MSG_MISSING_VERSION_TITLE = "MSG_MISSING_VERSION_TITLE";

    /**
     *
     */
    public static final String MSG_MISSING_VERSION_BODY = "MSG_MISSING_VERSION_BODY";

    /**
     *
     */
    public static final String MSG_CAN_T_SAVE_VERSION_TITLE = "MSG_CAN_T_SAVE_VERSION_TITLE";

    /**
     *
     */
    public static final String MSG_CAN_T_SAVE_VERSION_BODY = "MSG_CAN_T_SAVE_VERSION_BODY";
    IGenericDatatypeDAO genericDatatypeDAO;
    IDatasetDAO datasetDAO;
    INotificationsManager notificationsManager;
    IPolicyManager policyManager;
    ILocalizationManager localizationManager;

    /**
     *
     * @param parser
     * @param versionFile
     * @param fileEncoding
     * @throws BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding) throws BusinessException {
        if (versionFile == null) {
            String messageTitle = localizationManager.getMessage(BUNDLE_PATH, MSG_MISSING_VERSION_TITLE);
            String messageBody = localizationManager.getMessage(BUNDLE_PATH, MSG_MISSING_VERSION_BODY);
            Notification notification = new Notification(Notification.ERROR, messageTitle, messageBody);
            notificationsManager.addNotification(notification, policyManager.getCurrentUserLogin());
            throw new BusinessException(messageBody);
        }
        try {
            Dataset dataset = datasetDAO.getById(Dataset.class, versionFile.getDataset().getId()).orElseThrow(() -> new PersistenceException("can't get dataset"));
            GenericDatatype genericDatatype = new GenericDatatype(dataset);
            genericDatatypeDAO.saveOrUpdate(genericDatatype);
        } catch (PersistenceException ex) {
            String messageTitle = localizationManager.getMessage(BUNDLE_PATH, MSG_CAN_T_SAVE_VERSION_TITLE);
            String messageBody = localizationManager.getMessage(BUNDLE_PATH, MSG_CAN_T_SAVE_VERSION_BODY);
            Notification notification = new Notification(Notification.ERROR, messageTitle, messageBody);
            notificationsManager.addNotification(notification, policyManager.getCurrentUserLogin());
            throw new BusinessException(messageBody, ex);
        }
    }

    /**
     *
     * @param genericDatatypeDAO
     */
    public void setGenericDatatypeDAO(IGenericDatatypeDAO genericDatatypeDAO) {
        this.genericDatatypeDAO = genericDatatypeDAO;
    }

    /**
     *
     * @param datasetDAO
     */
    public void setDatasetDAO(IDatasetDAO datasetDAO) {
        this.datasetDAO = datasetDAO;
    }

    /**
     *
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

}
