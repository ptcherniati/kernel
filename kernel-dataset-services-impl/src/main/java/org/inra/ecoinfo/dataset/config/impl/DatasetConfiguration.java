package org.inra.ecoinfo.dataset.config.impl;

import com.google.common.base.Strings;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.AbstractConfiguration;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.localization.ILocalizationDAO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DatasetConfiguration.
 */
public class DatasetConfiguration extends AbstractConfiguration implements IDatasetConfiguration {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/datasetConfiguration.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "datasetConfiguration";
    private static final String REPOSITORY_PATTERN = "%s/repository/.";
    private static final String KEY = "key";
    private static final Logger LOGGER = LoggerFactory.getLogger(DatasetConfiguration.class);
    /**
     * The data types.
     */
    private List<DataTypeDescription> dataTypes = new LinkedList<>();
    /**
     * The datatype dao.
     */
    private IDatatypeDAO datatypeDAO;
    /**
     * The localization dao.
     */
    protected ILocalizationDAO localizationDAO;
    private ICoreConfiguration coreConfiguration;
    private ILocalizationManager localizationManager;


    /**
     * Instantiates a new dataset configuration.
     */
    public DatasetConfiguration() {
        super();
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.dataset.config.IDatasetConfiguration#addDataType(org.inra.ecoinfo.dataset.config.impl.DataTypeDescription)
     */
    /**
     *
     * @param dataType
     */
    @Override
    public void addDataType(DataTypeDescription dataType) {
        if (!dataTypes.contains(dataType)) {
            dataTypes.add(dataType);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#createConfig(org.apache.commons.digester.Digester)
     */

    /**
     *
     * @param digester
     */

    @Override
    public void createConfig(Digester digester) {
        digester.addObjectCreate("configuration/module/datatypeConfiguration/dataType", DataTypeDescription.class);
        digester.addSetNext("configuration/module/datatypeConfiguration/dataType", "addDataType");
        digester.addCallMethod("configuration/module/datatypeConfiguration/dataType/name", "addInternationalizedName", 2, newStringClassArray());
        digester.addCallParam("configuration/module/datatypeConfiguration/dataType/name", 0, "language");
        digester.addObjectCreate("configuration/module/datatypeConfiguration/dataType/name", String.class);
        digester.addCallMethod("configuration/module/datatypeConfiguration/dataType/name", "concat", 0);
        digester.addCallParam("configuration/module/datatypeConfiguration/dataType/name", 1, false);
        digester.addBeanPropertySetter("configuration/module/datatypeConfiguration/dataType/fileNameDateFormat", "fileNameDateFormat");
        digester.addCallMethod("configuration/module/datatypeConfiguration/dataType/description", "addInternationalizedDescription", 2, newStringClassArray());
        digester.addCallParam("configuration/module/datatypeConfiguration/dataType/description", 0, "language");
        digester.addObjectCreate("configuration/module/datatypeConfiguration/dataType/description", String.class);
        digester.addCallMethod("configuration/module/datatypeConfiguration/dataType/description", "concat", 0);
        digester.addCallParam("configuration/module/datatypeConfiguration/dataType/description", 1, false);
        digester.addCallMethod("configuration/module/datatypeConfiguration/dataType/overlapDates", "setCanOverlapDates", 0);
        digester.addCallMethod("configuration/module/datatypeConfiguration/dataType/recorder", "setRecorder", 0);
        digester.addObjectCreate("configuration/module/datatypeConfiguration/dataType/spring-dao", StringBuilder.class);
        digester.addCallMethod("configuration/module/datatypeConfiguration/dataType/spring-dao/class", "append", 0);
        digester.addSetNext("configuration/module/datatypeConfiguration/dataType/spring-dao", "addDao");
        digester.addObjectCreate("configuration/module/datatypeConfiguration/dataType/name-file-checkers", StringBuilder.class);
        digester.addCallMethod("configuration/module/datatypeConfiguration/dataType/name-file-checkers/class", "append", 0);
        digester.addSetNext("configuration/module/datatypeConfiguration/dataType/name-file-checkers", "addNameFileChecker");
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.dataset.config.IDatasetConfiguration#getDataTypes()
     */
    /**
     *
     * @return
     */
    @Override
    public List<DataTypeDescription> getDataTypes() {
        return dataTypes;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.dataset.config.IDatasetConfiguration#setDataTypes(java.util.List)
     */
    /**
     *
     * @param dataTypes
     */
    @Override
    public void setDataTypes(List<DataTypeDescription> dataTypes) {
        this.dataTypes = dataTypes;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getModuleName()
     */

    /**
     *
     * @return
     */

    @Override
    public String getModuleName() {
        return DatasetConfiguration.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#getSchemaPath()
     */

    /**
     *
     * @return
     */

    @Override
    public String getSchemaPath() {
        return DatasetConfiguration.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleConfiguration#isAutoLoad()
     */

    /**
     *
     * @return
     */

    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     * Post config.
     *
     * @throws BusinessException the business exception
     */
    @Override
    public void postConfig() throws BusinessException {
        try {
            createRepositoryIfNeeded();
            updateDatatypes();
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the datatype dao.
     *
     * @param datatypeDAO the new datatype dao
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Sets the localization dao.
     *
     * @param localizationDAO the new localization dao
     */
    public void setLocalizationDAO(ILocalizationDAO localizationDAO) {
        this.localizationDAO = localizationDAO;
    }

    /**
     *
     * @param coreConfiguration
     */
    public void setCoreConfiguration(ICoreConfiguration coreConfiguration) {
        this.coreConfiguration = coreConfiguration;
    }

    /**
     *
     * @param datatypeCode
     * @return
     */
    @Override
    public String getFileNameDateFormat(String datatypeCode) {
        String fileNameDateFormat = DateUtil.DD_MM_YYYY;
        for (DataTypeDescription dataTypeDescription : dataTypes) {
            if (dataTypeDescription.getCode().equals(datatypeCode)) {
                fileNameDateFormat = dataTypeDescription.getFileNameDateFormat();
                break;
            }
        }
        return fileNameDateFormat;
    }

    private void createRepositoryIfNeeded() throws IOException {
        String repositoryFiles = String.format(REPOSITORY_PATTERN, coreConfiguration.getRepositoryURI());
        FileWithFolderCreator.createFile(repositoryFiles + "/.");
    }

    /**
     * Update datatypes.
     *
     * @throws PersistenceException the persistence exception
     */
    private void updateDatatypes() throws PersistenceException {
        for (final DataTypeDescription datatypeDescription : getDataTypes()) {
            DataType datatype = datatypeDAO.getByCode(datatypeDescription.getCode()).orElse(null);
            if (datatype == null) {
                datatype = new DataType(datatypeDescription.getName());
                saveInternationalizedNames(datatypeDescription.getInternationalizedNames(), datatype.getName());
                final Map<String, String> internationalizedDescriptions = datatypeDescription.getInternationalizedDescriptions();
                if (!internationalizedDescriptions.isEmpty()) {
                    final String description = internationalizedDescriptions.getOrDefault(KEY, datatypeDescription.getInternationalizedDescriptions().get(Localization.getDefaultLocalisation()));
                    datatype.setDescription(description);
                    saveDescription(internationalizedDescriptions, description);
                }
                datatypeDAO.saveOrUpdate(datatype);
            } else {
                saveInternationalizedNames(datatypeDescription.getInternationalizedNames(), datatype.getName());
                final Map<String, String> internationalizedDescriptions = datatypeDescription.getInternationalizedDescriptions();
                if (Strings.isNullOrEmpty(datatype.getDescription()) && !internationalizedDescriptions.isEmpty()) {
                    final String description = internationalizedDescriptions.getOrDefault(KEY, datatypeDescription.getInternationalizedDescriptions().get(Localization.getDefaultLocalisation()));
                    datatype.setDescription(description);
                    saveDescription(internationalizedDescriptions, description);
                }
            }
        }
    }

    private void saveDescription(Map<String, String> internationalizedDescriptions, String defaultDescription) throws PersistenceException {
        if (defaultDescription != null) {
            saveInternationalizedDescriptions(internationalizedDescriptions, defaultDescription);
        }
    }

    private void saveInternationalizedDescriptions(Map<String, String> internationalizedDescriptions, String code) {
        String defaultKey = internationalizedDescriptions.containsKey(KEY) ? KEY : Localization.getDefaultLocalisation();
        for (Map.Entry<String, String> entry : internationalizedDescriptions.entrySet()) {
            try {
                String language = entry.getKey();
                if (defaultKey.equals(language)) {
                    continue;
                }
                String internationalizedString = entry.getValue();
                localizationManager.createorUpdateLocalization(language, DataType.NAME_ENTITY_JPA, "description", code, internationalizedString);
            } catch (BusinessException ex) {
                LOGGER.error("can't register internationalized description", ex);
            }
        }
    }

    private void saveInternationalizedNames(Map<String, String> internationalizedNames, String code) {
        String defaultKey = internationalizedNames.containsKey(KEY) ? KEY : Localization.getDefaultLocalisation();
        for (Map.Entry<String, String> entry : internationalizedNames.entrySet()) {
            try {
                String language = entry.getKey();
                if (defaultKey.equals(language)) {
                    continue;
                }
                String internationalizedString = entry.getValue();
                localizationManager.createorUpdateLocalization(language, DataType.NAME_ENTITY_JPA, "name", code, internationalizedString);
            } catch (BusinessException ex) {
                LOGGER.error("can't register internationalized description", ex);
            }
        }
    }
}
