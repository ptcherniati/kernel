package org.inra.ecoinfo.dataset.versioning;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface IVersionFileDAO.
 */
public interface IVersionFileDAO extends IDAO<VersionFile> {

    /**
     * The id bean @link(String).
     */
    String ID_BEAN = "versionFileDAO";

    /**
     * Gets the by id.
     *
     * @param versionFileId the version file id
     * @return the by id
     * @link(ILocalizationManager) the localization manager
     * @link(Long) the version file id
     */
    Optional<VersionFile> getById(Long versionFileId);
    
    /**
     *
     * @param utilisateur
     * @param anonymousUser
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    void anonymiseUserVersionsFile(Utilisateur utilisateur, Utilisateur anonymousUser) throws PersistenceException;
}
