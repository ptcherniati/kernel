package org.inra.ecoinfo.dataset.impl.filenamecheckers;

import com.google.common.base.Strings;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.Transient;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.dataset.IFileNameChecker;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AbstractFileNameChecker.
 *
 * @author ptcherniati Cette classe définie comment sont valider les noms dans
 * une implémentation site thème datatype
 */
public abstract class AbstractFileNameChecker implements IFileNameChecker {

    /**
     * The Constant PATTERN_COMMENT @link(String).
     */
    private static final String PATTERN_COMMENT = "#.*?#";
    /**
     * The Constant FILE_FORMAT @link(String).
     */
    protected static final String FILE_FORMAT = "csv";
    /**
     * The Constant INVALID_FILE_NAME @link(String).
     */
    protected static final String INVALID_FILE_NAME = "%s_%s_%s_%s.csv";
    /**
     * The Constant PATTERN @link(String).
     */
    protected static final String PATTERN = "^(%s|.*?)_(%s|.*?)_(.*)_(.*)\\.csv$";
    /**
     * The Constant PATTERN_DATE @link(String).
     */
    protected static final String PATTERN_DATE = "^_(.*)_(.*)\\.csv$";
    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    static final Logger LOGGER = LoggerFactory.getLogger(AbstractFileNameChecker.class.getName());
    /**
     * The configuration @link(Configuration).
     */
    protected ICoreConfiguration configuration;
    /**
     * The datatype dao @link(IDatatypeDAO).
     */
    protected IDatatypeDAO datatypeDAO;
    /**
     * The site dao @link(ISiteDAO).
     */
    protected ISiteDAO siteDAO;

    /**
     * Instantiates a new abstract file name checker.
     */
    protected AbstractFileNameChecker() {
        super();
    }

    /**
     * Checks if is valid file name.
     *
     * @param fileName the file name
     * @param version the version
     * @return true, if is valid file name
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#isValidFileName(java.lang.String,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public boolean isValidFileName(String fileName, final VersionFile version) throws InvalidFileNameException {
        final String localFileName = cleanFileName(fileName);
        String currentSite = version.getDataset().getRealNode().getNodeByNodeableTypeResource(Site.class).getCode();
        final String currentDatatype = version.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode();
        final Matcher splitFilename = Pattern.compile(String.format(AbstractFileNameChecker.PATTERN, currentSite, currentDatatype)).matcher(localFileName);
        testPath(currentSite, currentDatatype, splitFilename);
        final String siteName = reWriteSitePath(Utils.createCodeFromString(splitFilename.group(1)));
        testSite(version, currentSite, currentDatatype, siteName);
        final String datatypeName = Utils.createCodeFromString(splitFilename.group(2));
        testDatatype(currentSite, currentDatatype, datatypeName);
        testDates(version, currentSite, currentDatatype, splitFilename);
        return true;
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the datatype dao.
     *
     * @param datatypeDAO the new datatype dao
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     * Clean file name.
     *
     * @param fileName the file name
     * @return the string
     * @link(String) the file name
     */
    protected String cleanFileName(final String fileName) {
        return fileName.replaceAll(AbstractFileNameChecker.PATTERN_COMMENT, "");
    }

    /**
     * Gets the date pattern.
     *
     * @return the date pattern
     */
    protected abstract String getDatePattern();

    /**
     * Checks if is configuration separator defined.
     *
     * @return true, if is configuration separator defined
     */
    protected boolean isConfigurationSeparatorDefined() {
        return !Strings.isNullOrEmpty(configuration.getSiteSeparatorForFileNames());
    }

    /**
     * Re write current site path.
     *
     * @param currentSite the current site
     * @return the string
     * @link(String) the current site
     */
    protected String reWriteCurrentSitePath(String currentSite) {
        String returnCurrentSite = currentSite;
        if (isConfigurationSeparatorDefined()) {
            returnCurrentSite = currentSite.replaceAll(PatternConfigurator.UNDERSCORE, configuration.getSiteSeparatorForFileNames());
        }
        return returnCurrentSite;
    }

    /**
     * Re write site path.
     *
     * @param siteName the site name
     * @return the string
     * @link(String) the site name
     */
    protected String reWriteSitePath(String siteName) {
        String localSiteName = siteName;
        if (isConfigurationSeparatorDefined()) {
            localSiteName = localSiteName.replaceAll(configuration.getSiteSeparatorForFileNames(), PatternConfigurator.UNDERSCORE);
        }
        return localSiteName;
    }

    /**
     * Test datatype.
     *
     * @param currentSite the current site
     * @param currentDatatype the current datatype
     * @param datatypeName the datatype name
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @link(String) the current site
     * @link(String) the current datatype
     * @link(String) the datatype name
     */
    protected void testDatatype(final String currentSite, final String currentDatatype, final String datatypeName) throws InvalidFileNameException {
        if (!datatypeDAO.getByCode(Utils.createCodeFromString(datatypeName)).isPresent()) {
            throw new InvalidFileNameException(String.format(AbstractFileNameChecker.INVALID_FILE_NAME, currentSite, currentDatatype, getDatePattern(), getDatePattern()));
        }
    }

    /**
     * Test dates.
     *
     * @param version the version
     * @param currentSite the current site
     * @param currentDatatype the current datatype
     * @param splitFilename the split filename
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @link(VersionFile) the version
     * @link(String) the current site
     * @link(String) the current datatype
     * @link(Matcher) the split filename
     */
    protected abstract void testDates(VersionFile version, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException;

    /**
     * Test path.
     *
     * @param currentSite the current site
     * @param currentDatatype the current datatype
     * @param splitFilename the split filename
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @link(String) the current site
     * @link(String) the current datatype
     * @link(Matcher) the split filename
     */
    protected void testPath(final String currentSite, final String currentDatatype, final Matcher splitFilename) throws InvalidFileNameException {
        if (!splitFilename.find()) {
            throw new InvalidFileNameException(String.format(AbstractFileNameChecker.INVALID_FILE_NAME, currentSite, currentDatatype, getDatePattern(), getDatePattern()));
        }
    }

    /**
     * Test site.
     *
     * @param version the version
     * @param currentSite the current site
     * @param currentDatatype the current datatype
     * @param siteName the site name
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @link(VersionFile) the version
     * @link(String) the current site
     * @link(String) the current datatype
     * @link(String) the site name
     */
    protected void testSite(final VersionFile version, final String currentSite, final String currentDatatype, final String siteName) throws InvalidFileNameException {
        if (!siteDAO.getByPath(Utils.createCodeFromString(siteName)).isPresent() || !version.getDataset().getRealNode().getNodeByNodeableTypeResource(Site.class).getCode().equals(siteName)) {
            throw new InvalidFileNameException(String.format(AbstractFileNameChecker.INVALID_FILE_NAME, currentSite, currentDatatype, getDatePattern(), getDatePattern()));
        }
    }
}
