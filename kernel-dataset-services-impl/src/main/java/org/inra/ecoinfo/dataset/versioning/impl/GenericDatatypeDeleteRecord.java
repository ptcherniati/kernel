/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.dataset.versioning.impl;

import org.inra.ecoinfo.dataset.IDeleteRecord;
import org.inra.ecoinfo.dataset.versioning.IGenericDatatypeDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptchernia
 */
public class GenericDatatypeDeleteRecord implements IDeleteRecord {
    IGenericDatatypeDAO genericDatatypeDAO;

    /**
     *
     * @param genericDatatypeDAO
     */
    public void setGenericDatatypeDAO(IGenericDatatypeDAO genericDatatypeDAO) {
        this.genericDatatypeDAO = genericDatatypeDAO;
    }
    
    @Override
    public void deleteRecord(VersionFile versionFile) throws BusinessException {
        genericDatatypeDAO.deleteGenericDatatypeFromVersionFile(versionFile);
    }
    
}
