package org.inra.ecoinfo.dataset.deserialization;

/**
 * The Interface IIdSerialization.
 */
public interface IDatasetDeserialization {

    /**
     *
     * @return
     */
    String getId();
}
