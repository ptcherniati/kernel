/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.dataset.versioning.jpa;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.IGenericDatatypeDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.GenericDatatype;
import org.inra.ecoinfo.dataset.versioning.entity.GenericDatatype_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptchernia
 */
public class JPADatatypeGenericDAO extends AbstractJPADAO<GenericDatatype> implements IGenericDatatypeDAO {

    /**
     *
     * @param versionFile
     */
    @Override
    public void deleteGenericDatatypeFromVersionFile(VersionFile versionFile) {
        CriteriaDelete<GenericDatatype> delete = builder.createCriteriaDelete(GenericDatatype.class);
        Root<GenericDatatype> genericDatatype = delete.from(GenericDatatype.class);
        delete.where(builder.equal(genericDatatype.get(GenericDatatype_.dataset), versionFile.getDataset()));
        delete(delete);
    }

    /**
     *
     * @param periods
     * @return
     */
    @Override
    public List<Tuple> getDatatype(List<IntervalDate> periods) {
        CriteriaQuery<Tuple> query = builder.createQuery(Tuple.class);
        Root<GenericDatatype> genericDatatype = query.from(GenericDatatype.class);
        Join<GenericDatatype, Dataset> dataset = genericDatatype.join(GenericDatatype_.dataset);
        Join<Dataset, RealNode> datatypeRealNode1 = dataset.join(Dataset_.realNode);
        Root<DataType> datatype = query.from(DataType.class);
        Join<RealNode, Nodeable> datatypeNodeable = datatypeRealNode1.join(RealNode_.nodeable);
        Root<NodeDataSet> nodeDataset = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> datatypeRealNode2 = nodeDataset.join(NodeDataSet_.realNode);
        List<Predicate> and = new LinkedList();
        and.add(builder.equal(datatype, datatypeNodeable));
        and.add(builder.equal(datatypeRealNode1, datatypeRealNode2));
        testPeriods(and, dataset, periods);
        final Predicate[] andArray = and.toArray(new Predicate[0]);
        query
                .distinct(true)
                .multiselect(nodeDataset, dataset.<LocalDateTime>get(Dataset_.dateDebutPeriode), dataset.<LocalDateTime>get(Dataset_.dateFinPeriode), datatype)
                .where(builder.and(andArray));
        return getResultList(query);
    }

    private void testPeriods(List<Predicate> and, Join<GenericDatatype, Dataset> dataset, List<IntervalDate> periods) {
        List<Predicate> or = new LinkedList();
        for (IntervalDate period : periods) {
            or.add(builder.and(
                    builder.lessThanOrEqualTo(dataset.<LocalDateTime>get(Dataset_.dateDebutPeriode), period.getEndDate()),
                    builder.greaterThanOrEqualTo(dataset.<LocalDateTime>get(Dataset_.dateFinPeriode), period.getBeginDate())
            ));
        }
        final Predicate[] orArray = or.toArray(new Predicate[0]);
        and.add(builder.or(orArray));
    }

    /**
     *
     * @param nodeDataSets
     * @param intervalDates
     * @return
     */
    @Override
    public List<Tuple> getNodeDataSetFromNodesAndPeriods(Set<NodeDataSet> nodeDataSets, List<IntervalDate> intervalDates) {
        CriteriaQuery<Tuple> query = builder.createQuery(Tuple.class);
        Root<NodeDataSet> nodeDataset = query.from(NodeDataSet.class);
        Root<GenericDatatype> genericDatatype = query.from(GenericDatatype.class);
        Join<GenericDatatype, Dataset> dataset = genericDatatype.join(GenericDatatype_.dataset);
        dataset.fetch(Dataset_.publishVersion);
        List<Predicate> and = new LinkedList();
        and.add(builder.equal(nodeDataset.join(NodeDataSet_.realNode), dataset.join(Dataset_.realNode)));
        and.add(nodeDataset.get(NodeDataSet_.id).in(nodeDataSets));
        testPeriods(and, dataset, intervalDates);
        nodeDataset.join(NodeDataSet_.realNode);
        final Predicate[] andArray = and.toArray(new Predicate[0]);
        query
                .multiselect(nodeDataset, dataset)
                .where(builder.and(andArray));
        return entityManager.createQuery(query)
                .getResultList();
    }

}
