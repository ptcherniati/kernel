package org.inra.ecoinfo.dataset.versioning.impl;

import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.dataset.versioning.IRepositoryManagerVersions;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver;

/**
 * The Class DefaultRepositoryManagerVersions.
 */
public class DefaultRepositoryManagerVersions extends MO implements IRepositoryManagerVersions {

    /**
     * The Constant PATH_SUFFIX_REPOSITORY_VERSION @link(String).
     */
    public static final String PATH_SUFFIX_REPOSITORY_VERSION = "repository";
    /**
     * The configuration @link(Configuration).
     */
    protected ICoreConfiguration configuration;
    /**
     * The version file helper resolver @link(IVersionFileHelperResolver).
     */
    protected IVersionFileHelperResolver versionFileHelperResolver;

    /**
     * Instantiates a new default repository manager versions.
     */
    public DefaultRepositoryManagerVersions() {
        super();
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the version file helper resolver.
     *
     * @param versionFileHelperResolver the new version file helper resolver
     */
    public void setVersionFileHelperResolver(final IVersionFileHelperResolver versionFileHelperResolver) {
        this.versionFileHelperResolver = versionFileHelperResolver;
    }
}
