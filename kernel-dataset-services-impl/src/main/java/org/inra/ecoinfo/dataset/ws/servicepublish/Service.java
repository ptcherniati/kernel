package org.inra.ecoinfo.dataset.ws.servicepublish;

//~--- non-JDK imports --------------------------------------------------------
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.apache.commons.io.IOUtils;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.inra.ecoinfo.ws.exceptions.BusinessException;
import org.inra.ecoinfo.ws.streamers.AbstractService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Antoine Schellenberger
 */
public class Service extends AbstractService implements IService {
    
    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(Service.class);

    /**
     *
     */
    public static final String LOGGER_ERROR = "Method %s -> %s";
    private IDatasetManager datasetManager;
    private IPolicyManager policyManager;
    private IPublishWSDAO publishWSDAO;
    private INotificationsManager notificationsManager;
    
    /**
     *
     * @param request
     * @param requestBody
     * @return
     * @throws BusinessException
     */
    @POST
    @Produces("xml/plain")
    public Response publishFromContext(
            @Context HttpServletRequest request,
            InputStream requestBody) throws BusinessException {
        String fileName = null, context = request.getParameter("context");
        fileName = getParameter(request, "fileName");
        if (context == null) {
            context = getContextFromParameters(request, requestBody);
        }
        Utilisateur currentUser;
        
        try {
            currentUser = (Utilisateur) policyManager.getCurrentUser();
            byte[] data = IOUtils.toByteArray(requestBody);
            final String localContext = context;
            VersionFile versionFile = getNewVersion(context, data, fileName).orElseThrow(() -> new BusinessException(String.format("no version for %s", localContext)));
            Dataset dataset = datasetManager.uploadVersion(versionFile);
            if (dataset == null || dataset.getLastVersion() == null) {
                Optional<Notification> lastNotification = getLastNotification(currentUser);
                if (lastNotification.isPresent()) {
                    throw new BusinessException(String.format("%s%n%s", lastNotification.get().getMessage(), lastNotification.get().getBody()));
                } else {
                    throw new BusinessException(String.format("can't upload version for context", context));
                }
            }
            VersionFile lastVersion = dataset.getLastVersion();
            datasetManager.publishVersion(lastVersion);
            String notification = getLastNotification((Utilisateur) policyManager.getCurrentUser())
                    .map(n -> n.getMessage().concat("\n").concat(n.getBody()))
                    .orElseGet(() -> "{\"Submit\": \"Success\"}");
            return Response.ok().entity(notification).build();
        } catch (org.inra.ecoinfo.utils.exceptions.BusinessException ex) {
            String notification = getLastNotification((Utilisateur) policyManager.getCurrentUser())
                    .map(n -> n.getMessage().concat("\n").concat(n.getBody()))
                    .orElseGet(() -> ex.getMessage());
            throw new BusinessException(notification, ex);
        } catch (BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            String notification = getLastNotification((Utilisateur) policyManager.getCurrentUser())
                    .map(n -> n.getMessage().concat("\n").concat(n.getBody()))
                    .orElseGet(() -> ex.getMessage());
            throw new BusinessException(notification, ex);
        }
        
    }
    
    /**
     *
     * @param request
     * @param requestBody
     * @return
     * @throws BusinessException
     */
    public String getContextFromParameters(
            HttpServletRequest request,
            InputStream requestBody) throws BusinessException {
        Class<INodeable>[] entryType = getEntries();
        List<String> elements = new LinkedList();
        for (int i = 0; i < entryType.length; i++) {
            Class<INodeable> clazz = entryType[i];
            elements.add(getParameter(request, clazz.getSimpleName()));
        }
        return elements.stream().collect(Collectors.joining(","));
    }
    
    private Class<INodeable>[] getEntries() {
        return policyManager.getMgaServiceBuilder().
                getMgaIOConfigurator().
                getConfiguration(AbstractMgaIOConfigurator.DATASET_CONFIGURATION)
                .map(conf -> conf.getEntryType())
                .orElse(new Class[0]);
    }
    
    private String getParameter(HttpServletRequest request, String type) throws BusinessException {
        String parameter = request.getParameter(type);
        if (parameter == null) {
            parameter = request.getParameter(type.toLowerCase());
        }
        if (parameter == null) {
            String parametersList = Stream.of(getEntries())
                    .map(e -> e.getSimpleName().toLowerCase())
                    .collect(Collectors.joining("\",\"", "\"", "\""));
            String message = String.format("Parameters must be \"filename\" and both \"context\" or %s", parametersList);
            throw new BusinessException(message);
        }
        
        return parameter;
    }
    
    private Optional<Notification> getLastNotification(Utilisateur user) {
        return notificationsManager.getAllNotifications()
                .stream()
                .filter(n -> n.getUtilisateur().equals(user))
                .findFirst();
    }

    /**
     *
     * @param nodePath
     * @param file
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     * @throws FileNotFoundException
     * @throws IOException
     */
    protected Optional<VersionFile> getNewVersion(String context, byte[] data, String fileName) throws BusinessException {
        
        try {
            final Optional<Long> leafrealNodeIdOpt = publishWSDAO.getLeafrealNodeId(context);
            Long leafrealNodeId = leafrealNodeIdOpt.orElseThrow(() -> new BusinessException(String.format("no node for context %s", context)));
            VersionFile versionFromFileName = datasetManager.getVersionFromFileName(fileName, leafrealNodeId);
            versionFromFileName.setData(data);
            return Optional.ofNullable(versionFromFileName);
        } catch (org.inra.ecoinfo.utils.exceptions.BusinessException ex) {
            throw new BusinessException(String.format("can't find node for  context : %s", context), ex);
        }
    }
    
    /**
     *
     * @param datasetManager
     */
    public void setDatasetManager(IDatasetManager datasetManager) {
        this.datasetManager = datasetManager;
    }
    
    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }
    
    /**
     *
     * @param publishWSDAO
     */
    public void setPublishWSDAO(IPublishWSDAO publishWSDAO) {
        this.publishWSDAO = publishWSDAO;
    }
    
    /**
     *
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
