package org.inra.ecoinfo.dataset.impl.filenamecheckers;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 * The Class FileNameCheckerddmmyyyy.
 */
public class PermissiveFileNameMMYYYYChecker extends AbstractFileNameChecker {

    /**
     * The Constant DATE_PATTERN @link(String).
     */
    private static final String DATE_PATTERN = DateUtil.MM_YYYY_FILE;

    /**
     * Checks if is valid file name.
     *
     * @param fileName the file name
     * @param version the version
     * @return true, if is valid file name
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#isValidFileName(java.lang.String,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public boolean isValidFileName(String fileName, final VersionFile version) throws InvalidFileNameException {
        final String localFileName = cleanFileName(fileName);
        final Matcher splitFilename = Pattern.compile("^(.+)_(.*?)_(.*?)\\.(.+)$").matcher(localFileName);
        testPath(splitFilename, fileName);
        testDates(version, fileName, splitFilename);
        version.setFileName(splitFilename.group(1));
        version.setExtention(splitFilename.group(4));
        version.getDataset().setSanitize(false);
        return true;
    }

    /**
     * Test path.
     *
     * @param splitFilename the split filename
     * @param fileName
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @link(String) the current site
     * @link(String) the current datatype
     * @link(Matcher) the split filename
     */
    protected void testPath(final Matcher splitFilename, String fileName) throws InvalidFileNameException {
        if (!splitFilename.find()) {
            throw new InvalidFileNameException(fileName);
        }
    }

    /**
     *
     * @param version
     * @param fileName
     * @param splitFilename
     * @throws InvalidFileNameException
     */
    protected void testDates(final VersionFile version, String fileName, final Matcher splitFilename) throws InvalidFileNameException {
        IntervalDate intervalDate;
        try {
            intervalDate = IntervalDate.getIntervalDateddMMyyyy(splitFilename.group(2), splitFilename.group(3));
        } catch (final BadExpectedValueException e1) {
            throw new InvalidFileNameException(String.format("%s_%s_%s.%s", splitFilename.group(1), getDatePattern(), getDatePattern(), splitFilename.group(4)), e1);
        }
        if (version != null) {
            version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
            version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
        }
    }

    /**
     * Gets the file path.
     *
     * @param version the version
     * @return the file path
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String getFilePath(final VersionFile version) {
        return String.format("%s_%s_%s#%s#.%s",
                version.getFileName(),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateDebutPeriode(), DATE_PATTERN),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateFinPeriode(),DATE_PATTERN),
                version.getVersionNumber(),
                version.getExtention());
    }

    /**
     * Gets the file path.
     *
     * @param dataset
     * @return the file path
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String getFilePath(final Dataset dataset) {
        Optional<Dataset> datasetOpt = Optional.ofNullable(dataset);
        Optional<VersionFile> versionOpt = datasetOpt
                .map(d->d.getVersions().values().stream().findFirst().orElse(null));
        return String.format("%s_%s_%s#%s#.%s",
                versionOpt.map(v->v.getFileName()).orElse("no version fileName"),
                datasetOpt.map(d->DateUtil.getUTCDateTextFromLocalDateTime(d.getDateDebutPeriode(), DATE_PATTERN)).orElse("no dataset start date"),
                datasetOpt.map(d->DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), DATE_PATTERN)).orElse("no dataset end date"),
                dataset.getVersions().size(),
                versionOpt.map(v->v.getExtention()).orElse("no version extention")
        );
    }

    /**
     * Gets the date pattern.
     *
     * @return the date pattern
     * @see
     * org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker#getDatePattern()
     */
    @Override
    protected String getDatePattern() {
        return PermissiveFileNameMMYYYYChecker.DATE_PATTERN;
    }

    @Override
    protected void testDates(VersionFile version, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
