package org.inra.ecoinfo.dataset.versioning.impl;

import com.google.common.collect.Lists;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import javax.persistence.Transient;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.dataset.IRecorder;
import org.inra.ecoinfo.dataset.IRecorderDAO;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.config.impl.DataTypeDescription;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver;
import org.inra.ecoinfo.dataset.versioning.IVersioningManager;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import static org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator.*;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.theme.IThemeDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class DefaultVersioningManager.
 */
public class DefaultVersioningManager extends AbstractVersioningManager implements IVersioningManager {

    /**
     * The Constant MSG_BAD_FILE_FORMAT @link(String).
     */
    public static final String MSG_BAD_FILE_FORMAT = "PROPERTY_MSG_BAD_FILE_FORMAT";
    /**
     * The Constant MSG_GOOD_FILE_FORMAT @link(String).
     */
    public static final String MSG_GOOD_FILE_FORMAT = "PROPERTY_MSG_GOOD_FILE_FORMAT";

    /**
     * The Constant UNCATCHED_EXEPTION_IN_CHECK_VERSION @link(String).
     */
    protected static final String UNCATCHED_EXEPTION_IN_CHECK_VERSION = "Uncatched exeption in check Version";
    /**
     * The Constant UNCATCHED_EXEPTION_IN_PUBLISH_VERSION @link(String).
     */
    protected static final String UNCATCHED_EXEPTION_IN_PUBLISH_VERSION = "Uncatched exeption in publish Version";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.versioning.impl.messages";
    /**
     * The Constant MSG_ERREUR_PERSISTENCE2 @link(String).
     */
    protected static final String MSG_ERREUR_PERSISTENCE2 = "PROPERTY_MSG_ERREUR_PERSISTENCE2";
    /**
     * The Constant MSG_ERROR_DURING_CHECKING @link(String).
     */
    protected static final String MSG_ERROR_DURING_CHECKING = "PROPERTY_MSG_ERROR_DURING_CHECKING";
    /**
     * The Constant MSG_ERROR_DURING_PUBLICATION @link(String).
     */
    protected static final String MSG_ERROR_DURING_PUBLICATION = "PROPERTY_MSG_ERROR_DURING_PUBLICATION";
    /**
     * The Constant MSG_PUBLISH_NOT_AVAILABLE @link(String).
     */
    protected static final String MSG_PUBLISH_NOT_AVAILABLE = "PROPERTY_MSG_PUBLISH_NOT_AVAILABLE";

    /**
     *
     */
    protected static final String MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_BEFORE = "PROPERTY_MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_BEFORE";

    /**
     *
     */
    protected static final String MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_AFTER = "PROPERTY_MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_AFTER";
    /**
     * The Constant MSG_PUBLISH_NOT_AVAILABLE_FOR_OVERLAPING @link(String).
     */
    protected static final String MSG_PUBLISH_NOT_AVAILABLE_FOR_OVERLAPING = "PROPERTY_MSG_PUBLISH_NOT_AVAILABLE_FOR_OVERLAPING";
    /**
     * The Constant MSG_PUBLISH_START @link(String).
     */
    protected static final String MSG_PUBLISH_START = "PROPERTY_MSG_PUBLISH_START";
    /**
     * The Constant MSG_PUBLISH_SUCCESS @link(String).
     */
    protected static final String MSG_PUBLISH_SUCCESS = "PROPERTY_MSG_PUBLISH_SUCCESS";
    /**
     * The Constant MSG_UNPUBLISH_FAILURE @link(String).
     */
    protected static final String MSG_UNPUBLISH_FAILURE = "PROPERTY_MSG_UNPUBLISH_FAILURE";
    /**
     * The Constant MSG_UNPUBLISH_START @link(String).
     */
    protected static final String MSG_UNPUBLISH_START = "PROPERTY_MSG_UNPUBLISH_START";
    /**
     * The Constant MSG_UNPUBLISH_SUCCESS @link(String).
     */
    protected static final String MSG_UNPUBLISH_SUCCESS = "PROPERTY_MSG_UNPUBLISH_SUCCESS";
    /**
     * The Constant MSG_UPLOAD_SUCCESS @link(String).
     */
    protected static final String MSG_UPLOAD_SUCCESS = "PROPERTY_MSG_UPLOAD_SUCCESS";
    /**
     * The Constant MSG_DELETION_SUCCESS @link(String).
     */
    protected static final String MSG_DELETION_SUCCESS = "PROPERTY_MSG_DELETION_SUCCESS";
    
    public static Logger DATASET_LOGGER = LoggerFactory.getLogger("dataset.logger");

    /**
     *
     */
    protected static final String MSG_NO_RIGHTS_FOR_UPLOAD = "PROPERTY_MSG_NO_RIGHTS_FOR_UPLOAD";

    /**
     *
     */
    protected static final String MSG_NO_RIGHTS_FOR_PUBLISH = "PROPERTY_MSG_NO_RIGHTS_FOR_PUBLISH";

    /**
     *
     */
    protected static final String MSG_NO_RIGHTS_FOR_DELETE = "PROPERTY_MSG_NO_RIGHTS_FOR_DELETE";

    /**
     *
     */
    protected static final String MSG_NULL_VERSION = "PROPERTY_MSG_NULL_VERSION";
    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    protected static final Logger LOGGER = LoggerFactory.getLogger(DefaultVersioningManager.class.getName());
    /**
     * The dataset dao @link(IDatasetDAO).
     */
    protected IDatasetDAO datasetDAO;
    /**
     * The datatype dao @link(IDatatypeDAO).
     */
    protected IDatatypeDAO datatypeDAO;
    /**
     * The publishing version files @link(List<VersionFile>).
     */
    protected List<VersionFile> publishingVersionFiles = Lists.newArrayList();
    /**
     * The recorder dao @link(IRecorderDAO).
     */
    protected IRecorderDAO recorderDAO;
    /**
     * The repository manager versions @link(IRepositoryManagerVersions).
     */
//    protected IRepositoryManagerVersions repositoryManagerVersions;
    /**
     * The site dao @link(ISiteDAO).
     */
    protected ISiteDAO siteDAO;
    /**
     * The theme dao @link(IThemeDAO).
     */
    protected IThemeDAO themeDAO;
    /**
     * The utilisateur dao @link(IUtilisateurDAO).
     */
    protected IUtilisateurDAO utilisateurDAO;
    /**
     * The coreConfiguration @link(ICoreConfiguration).
     */
    protected ICoreConfiguration coreConfiguration;
    /**
     * The version file dao @link(IVersionFileDAO).
     */
    protected IVersionFileDAO versionFileDAO;
    /**
     * The version file helper resolver @link(IVersionFileHelperResolver).
     */
    protected IVersionFileHelperResolver versionFileHelperResolver;

    /**
     *
     */
    protected IDatasetConfiguration datasetConfiguration;

    /**
     *
     * @param datasetConfiguration
     */
    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }

    /**
     *
     * @param coreConfiguration
     */
    public void setCoreConfiguration(ICoreConfiguration coreConfiguration) {
        this.coreConfiguration = coreConfiguration;
    }

    /**
     * anonymiseUserVersionsFile.
     *
     * @param utilisateur
     * @throws PersistenceException
     */
    @Override
    public void anonymiseUserVersionsFile(Utilisateur utilisateur) throws PersistenceException {
        Utilisateur anonymousUser = utilisateurDAO.merge((Utilisateur) utilisateurDAO.getByLogin(coreConfiguration.getAnonymousLogin()).get());
        versionFileDAO.anonymiseUserVersionsFile(utilisateur, anonymousUser);
    }

    /**
     * Check version.
     *
     * @param versionFile the version
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#checkVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    @Transactional
    public void checkVersion(VersionFile versionFile) throws BusinessException {
        testIsNullVersion(versionFile);
        versionFile = versionFileDAO.getById(versionFile.getId()).orElse(versionFile);
        checkVersion(versionFile, true);
    }

    /**
     * Delete version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#deleteVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void deleteVersion(VersionFile versionFile) throws BusinessException {
        testIsNullVersion(versionFile);
        testHasRights(versionFile, Activities.suppression, DefaultVersioningManager.MSG_NO_RIGHTS_FOR_DELETE, DATASET_CONFIGURATION);
        VersionFile localVersionFile = versionFileDAO.getById(versionFile.getId()).orElse(null);
        if (localVersionFile == null) {
            return;
        }
        try {

            final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(localVersionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode());
            final String filename = versionFileHelper.buildDownloadFilename(localVersionFile);
            final String message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_DELETION_SUCCESS), filename);
            if (localVersionFile.getDataset().getPublishVersion() != null && localVersionFile.getDataset().getPublishVersion().getId() == localVersionFile.getId()) {
                if (unPublish(versionFile) < 1) {
                    return;
                };
                deleteVersion(versionFile);
                return;
            }
            final Dataset datasetDB = datasetDAO.merge(localVersionFile.getDataset());
            if (datasetDB.getVersions().size() == 1) {
                datasetDAO.remove(datasetDB);
            } else {
                localVersionFile = datasetDB.getVersions().remove(localVersionFile.getVersionNumber());
                versionFileDAO.saveOrUpdate(localVersionFile);
                versionFileDAO.flush();
                versionFileDAO.remove(versionFileDAO.merge(localVersionFile));
            }
            notificationsManager.addNotification(buildNotification(Notification.INFO, message, null, null), policyManager.getCurrentUserLogin());
        } catch (final PersistenceException e) {
            throw new BusinessException("can't remove version", e);
        }
    }

    /**
     * Download version by id.
     *
     * @param versionFileId the version file id
     * @return the byte[]
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#downloadVersionById(java.lang.Long)
     */
    @Override
    @Transactional(readOnly = true)
    public byte[] downloadVersionById(final Long versionFileId) {
        final Optional<VersionFile> versionFile = versionFileDAO.getById(versionFileId);
        return versionFile.map(t -> t.getData()).orElse(new byte[0]);
    }

    /**
     * Gets the version file by id.
     *
     * @param versionFileId the version file id
     * @return the version file by id
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#getVersionFileById(java.lang.Long)
     */
    @Override
    public VersionFile getVersionFileById(final Long versionFileId) throws BusinessException {
        VersionFile versionFile = null;
        try {
            versionFile = versionFileDAO.getById(versionFileId).orElseThrow(PersistenceException::new);
        } catch (final PersistenceException e) {
            throw new BusinessException("can't get version", e);
        }
        return versionFile;
    }

    /**
     * Gets the version file helper resolver.
     *
     * @return the version file helper resolver
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#getVersionFileHelperResolver()
     */
    @Override
    public IVersionFileHelperResolver getVersionFileHelperResolver() {
        return versionFileHelperResolver;
    }

    /**
     * Sets the version file helper resolver.
     *
     * @param versionFileHelperResolver the new version file helper resolver
     */
    public void setVersionFileHelperResolver(final IVersionFileHelperResolver versionFileHelperResolver) {
        this.versionFileHelperResolver = versionFileHelperResolver;
    }

    /**
     * Publish version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#publishVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void publishVersion(VersionFile versionFile) throws BusinessException {
        Optional.ofNullable(policyManager.getCurrentUser())
                .ifPresent((user) -> {
                    MDC.put("publication.user.login", user.getLogin());
                    MDC.put("publication.user.email", user.getEmail());
                    MDC.put("publication.user.name", user.getNom());
                    MDC.put("publication.user.surname", user.getPrenom());
                });
        testIsNullVersion(versionFile);
        testHasRights(versionFile, Activities.publication, DefaultVersioningManager.MSG_NO_RIGHTS_FOR_PUBLISH, DATASET_CONFIGURATION);
        versionFile = versionFileDAO.getById(versionFile.getId()).orElse(versionFile);
        testOverlapThenRecord(versionFile);
    }

    /**
     * Retrieve datasets.
     *
     * @param realNodeLeafId
     * @return the list
     * @throws BusinessException the business exception
     * @see
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public List<Dataset> retrieveDatasets(final Long realNodeLeafId) throws BusinessException {
        List<Dataset> datasets = null;
        datasets = datasetDAO.retrieveDatasets(realNodeLeafId);
        return datasets;
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(final IDatasetConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the dataset dao.
     *
     * @param datasetDAO the new dataset dao
     */
    public void setDatasetDAO(final IDatasetDAO datasetDAO) {
        this.datasetDAO = datasetDAO;
    }

    /**
     * Sets the datatype dao.
     *
     * @param datatypeDAO the new datatype dao
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     * Sets the recorder dao.
     *
     * @param recorderDAO the new recorder dao
     */
    public void setRecorderDAO(final IRecorderDAO recorderDAO) {
        this.recorderDAO = recorderDAO;
    }

    /**
     * Sets the repository manager versions.
     *
     * @param repositoryManagerVersions the new repository manager versions
     */
//    public void setRepositoryManagerVersions(final IRepositoryManagerVersions repositoryManagerVersions) {
//        this.repositoryManagerVersions = repositoryManagerVersions;
//    }
    /**
     * Sets the site dao.
     *
     * @param siteDAO the new site dao
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     * Sets the theme dao.
     *
     * @param themeDAO the new theme dao
     */
    public void setThemeDAO(final IThemeDAO themeDAO) {
        this.themeDAO = themeDAO;
    }

    /**
     * Sets the utilisateur dao.
     *
     * @param utilisateurDAO the new utilisateur dao
     */
    public void setUtilisateurDAO(final IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     * Sets the version file dao.
     *
     * @param versionFileDAO the new version file dao
     */
    public void setVersionFileDAO(final IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    /**
     * Un publish.
     *
     * @param versionFile the version file
     * @return
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#unPublish(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public int unPublish(VersionFile versionFile) throws BusinessException {
        Optional.ofNullable(policyManager.getCurrentUser())
                .ifPresent((user) -> {
                    MDC.put("publication.user.login", user.getLogin());
                    MDC.put("publication.user.email", user.getEmail());
                    MDC.put("publication.user.name", user.getNom());
                    MDC.put("publication.user.surname", user.getPrenom());
                });
        MDC.put("publication.type", "unpublish");
        testIsNullVersion(versionFile);
        testHasRights(versionFile, Activities.publication, DefaultVersioningManager.MSG_NO_RIGHTS_FOR_PUBLISH, DATASET_CONFIGURATION);
        VersionFile localVersionFile = versionFile;
        IRecorder recorder = null;
        String message;
        final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(localVersionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode());
        final String filename = versionFileHelper.buildDownloadFilename(localVersionFile);
        notificationsManager.addNotification(buildNotification(Notification.INFO, String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_UNPUBLISH_START), versionFile.getVersionNumber(), filename),
                null, null), policyManager.getCurrentUserLogin());
        try {
            final Utilisateur utilisateur = utilisateurDAO.merge((Utilisateur) policyManager.getCurrentUser());
            if (datasetDAO.getById(Dataset.class, localVersionFile.getDataset().getId()).orElseThrow(PersistenceException::new) != null) {;
                recorder = recorderDAO.getRecorderByDataTypeCode(localVersionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode());
                recorder.deleteRecords(localVersionFile);
                MDC.put("publication.unpublishDate", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.DD_MM_YYYY));
                MDC.put("publication.version", Long.toString(localVersionFile.getVersionNumber()));
                MDC.put("publication.type", "unpublish");
                MDC.put("publication.comment", versionFile.getDataset().getPublishComment());
                MDC.put("publication.fileName", filename);
                MDC.put("publication.startDate", DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateDebutPeriode(), DateUtil.DD_MM_YYYY));
                MDC.put("publication.endDate", DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateFinPeriode(), DateUtil.DD_MM_YYYY));

                message = String.format(
                        localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_UNPUBLISH_SUCCESS),
                        versionFile.getVersionNumber(),
                        filename);
                notificationsManager.addNotification(buildNotification(Notification.INFO, message, null, null), utilisateur.getLogin());
                final int unpublishVersion = datasetDAO.unpublishVersion(localVersionFile);
                versionFileDAO.flush();
                return unpublishVersion;
            }
        } catch (final PersistenceException | BusinessException e) {
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_UNPUBLISH_FAILURE), filename);
            MDC.put("publication.error", message);
            notificationsManager.addNotification(buildNotification(Notification.ERROR, message, e.getMessage(), null), policyManager.getCurrentUserLogin());
            throw new BusinessException(message, e);
        }finally {
            publishingVersionFiles.remove(versionFile);
            DATASET_LOGGER.info(String.format("%s a déposé  le type de donnée %s", MDC.get("publication.user.login"), MDC.get("publication.fileName")));
        }
        return 0;
    }

    /**
     * Upload version.
     *
     * @param version the version
     * @return the dataset
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersioningManager#uploadVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public Dataset uploadVersion(final VersionFile version) throws BusinessException {
        testIsNullVersion(version);
        testHasRights(version, Activities.depot, DefaultVersioningManager.MSG_NO_RIGHTS_FOR_UPLOAD, DATASET_CONFIGURATION);
        checkVersion(version, false);
        final ErrorsReport errorsReport = new ErrorsReport(localizationManager.getMessage(ErrorsReport.BUNDLE_NAME, ErrorsReport.PROPERTY_MSG_ERROR));
        Dataset dataset = null;
        try {
            final Utilisateur utilisateur = utilisateurDAO.merge((Utilisateur) policyManager.getCurrentUser());
            final LocalDateTime uploadDate = LocalDateTime.ofInstant(Instant.now(), DateUtil.getLocaleZoneId());
            version.setUploadDate(uploadDate);
            version.setUploadUser(utilisateur);
            version.setFileSize(version.getData().length);
            final RealNode node = version.getDataset().getRealNode();
            if (errorsReport.hasErrors()) {
                LOGGER.error("{}", errorsReport.getErrorsMessages());
                notificationsManager.addNotification(buildNotification(Notification.INFO, errorsReport.buildHTMLMessages(), null, null), utilisateur.getLogin());
                throw new BusinessException(errorsReport.getMessages());
            }
            Optional<Dataset> datasetOpt = datasetDAO.getDatasetByNaturalKey(node, version.getDataset().getDateDebutPeriode(), version.getDataset().getDateFinPeriode());
            if (datasetOpt.isPresent()) {
                dataset = datasetOpt.get();
            } else {
                dataset = version.getDataset();
                dataset.setRealNode(node);
                dataset.setCreateDate(uploadDate);
                dataset.setCreateUser(utilisateur);
                datasetDAO.saveOrUpdate(dataset);
            }
            dataset.addVersion(version);
            version.getData();
            final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(version.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode());
            final String filename = versionFileHelper.buildDownloadFilename(version);
            MDC.put("publication.unpublishDate", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.DD_MM_YYYY));
            MDC.put("publication.version", Long.toString(version.getVersionNumber()));
            MDC.put("publication.type", "upload");
            MDC.put("publication.comment", version.getDataset().getPublishComment());
            MDC.put("publication.fileName", filename);
            MDC.put("publication.startDate", DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateDebutPeriode(), DateUtil.DD_MM_YYYY));
            MDC.put("publication.endDate", DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateFinPeriode(), DateUtil.DD_MM_YYYY));

            notificationsManager
                    .addNotification(buildNotification(Notification.INFO, String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_UPLOAD_SUCCESS), filename),
                            null, null), utilisateur.getLogin());
        } catch (final PersistenceException e) {
            throw new BusinessException("can't upload version", e);
        }
        return dataset;
    }

    /**
     * Check version.
     *
     * @param version the version
     * @param enableNotifications the enable notifications
     * @return the i recorder
     * @throws BusinessException the business exception
     * @link(VersionFile) the version
     * @link(boolean) the enable notifications
     */
    protected IRecorder checkVersion(final VersionFile version, final boolean enableNotifications) throws BusinessException {
        IRecorder recorder = null;
        final Utilisateur utilisateur = (Utilisateur) policyManager.getCurrentUser();
        try {
            recorder = recorderDAO.getRecorderByDataTypeCode(version.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode());
            final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(version.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode());
            final String filename = versionFileHelper.buildDownloadFilename(version);
            trySanitize(version, utilisateur, filename);
            tryTestFormat(version, enableNotifications, recorder, utilisateur, filename);
        } catch (final PersistenceException e) {
            LOGGER.error("persistence exception", e);
            notificationsManager.addNotification(buildNotification(Notification.ERROR, localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_ERREUR_PERSISTENCE2), null, null),
                    utilisateur.getLogin());
            throw new BusinessException("can't check version", e);
        }
        return recorder;
    }

    /**
     *
     * @param versionFile
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     */
    protected Overlap getInstance(VersionFile versionFile) throws BusinessException, PersistenceException {
        final Overlap overlap = new Overlap(versionFile);
        overlap.initInstance();
        return overlap;
    }

    /**
     * Test right for publishing
     *
     * @param versionFile
     * @param activity
     * @param configurationNumber
     * @param keyMessage
     * @throws BusinessException
     */
    protected void testHasRights(VersionFile versionFile, Activities activity, String keyMessage, Integer codeConfiguration) throws BusinessException {
        if (policyManager.isRoot()) {
            return;
        }
        ITreeNode<IFlatNode> tree = policyManager.getOrLoadTree(policyManager.getCurrentUser(), codeConfiguration);
        Optional<ITreeNode<IFlatNode>> rightNode = policyManager.getTreeNodeByFilterPredicate(tree,
                n -> nodeMatchesVersion(n, activity, versionFile)
        );
        if (!rightNode.isPresent()) {
            String message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, keyMessage), versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getPath());
            notificationsManager.addNotification(buildNotification(Notification.ERROR, message, null, null), policyManager.getCurrentUserLogin());
            throw new BusinessException(message);
        }
    }

    private boolean nodeMatchesVersion(ITreeNode<IFlatNode> n, Activities activity, VersionFile versionFile) {
        return Optional.ofNullable(n)
                .map(node -> node.getData())
                .map(flatNode -> flatNode.getRealNodeLeafId())
                .filter(id -> id.equals(versionFile.getDataset().getRealNode().getId()) && n.getData().hasActivity(activity))
                .isPresent();
    }

    /**
     *
     * @param versionFile
     * @throws BusinessException
     */
    protected void testOverlapThenRecord(VersionFile versionFile) throws BusinessException {
        String message;
        Overlap overlap;
        try {
            overlap = getInstance(versionFile);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't read overlaping versions", ex);
        }
        if (overlap.canRecord()) {
            final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode());
            final String filename = versionFileHelper.buildDownloadFilename(versionFile);
            Notification notification = buildNotification(Notification.INFO, String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_PUBLISH_START), versionFile.getUploadDate(), filename), null, null);
            notificationsManager.addNotification(notification, policyManager.getCurrentUserLogin());
            tryRecord(versionFile, filename);
        } else if (overlap.overlapDB()) {
            final IVersionFileHelper versionFileHelper = versionFileHelperResolver.resolveByDatatype(versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode());
            final String filename = versionFileHelper.buildDownloadFilename(versionFile);
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_ERROR_DURING_PUBLICATION), filename);
            final String body = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_PUBLISH_NOT_AVAILABLE_FOR_OVERLAPING), versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getPath());
            notificationsManager.addNotification(buildNotification(Notification.ERROR, message, body, null), policyManager.getCurrentUserLogin());
        } else if (overlap.notOverlappingWithSameDate != 0) {
            String bundleMessage = overlap.notOverlappingWithSameDate == Overlap.NOT_OVERLAPING_BUT_SAME_AT_BEGIN ? DefaultVersioningManager.MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_BEFORE : DefaultVersioningManager.MSG_PUBLISH_NOT_AVAILABLE_FOR_SAME_DATE_AFTER;
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, bundleMessage), versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getPath(),
                    versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getPath());
            notificationsManager.addNotification(buildNotification(Notification.ERROR, message, null, null), policyManager.getCurrentUserLogin());
        } else {
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_PUBLISH_NOT_AVAILABLE), versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getPath());
            notificationsManager.addNotification(buildNotification(Notification.ERROR, message, null, null), policyManager.getCurrentUserLogin());
        }
    }

    /**
     *
     * @param versionFile
     * @param filename
     * @throws BusinessException
     */
    protected void tryRecord(VersionFile versionFile, final String filename) throws BusinessException {
        IRecorder recorder;
        String message;
        try {
            VersionFile localVersionFile = versionFileDAO.getById(versionFile.getId()).orElseThrow(PersistenceException::new);

            localVersionFile.getDataset().setRealNode(versionFile.getDataset().getRealNode());

            publishingVersionFiles.add(localVersionFile);
            if (localVersionFile.getDataset().getPublishVersion() != null) {
                unPublish(localVersionFile.getDataset().getPublishVersion());
                publishingVersionFiles.remove(localVersionFile);
                versionFileDAO.flush();
                publishVersion(localVersionFile);
                return;
            }
            final Utilisateur utilisateur = utilisateurDAO.merge((Utilisateur) policyManager.getCurrentUser());
            recorder = recorderDAO.getRecorderByDataTypeCode(localVersionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode());
            final Instant startTime = Instant.now();
            final Dataset dataset = localVersionFile.getDataset();
            final LocalDateTime publishdate = LocalDateTime.ofInstant(Instant.now(), DateUtil.getLocaleZoneId());
            dataset.setPublishDate(publishdate);
            dataset.setPublishUser(utilisateur);
            dataset.setPublishVersion(localVersionFile);
            String encoding;
            encoding = Utils.detectStreamEncoding(localVersionFile.getData());
            MDC.put("publication.publishDate", DateUtil.getUTCDateTextFromLocalDateTime(publishdate, DateUtil.DD_MM_YYYY));
            MDC.put("publication.version", Long.toString(localVersionFile.getVersionNumber()));
            MDC.put("publication.type", "publish");
            MDC.put("publication.comment", versionFile.getDataset().getPublishComment());
            MDC.put("publication.fileName",filename);
            MDC.put("publication.startDate", DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateDebutPeriode(), DateUtil.DD_MM_YYYY));
            MDC.put("publication.endDate", DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateFinPeriode(), DateUtil.DD_MM_YYYY));
            recorder.record(localVersionFile, encoding);
            final Instant endTime = Instant.now();
            Duration duration = Duration.between(startTime, endTime);
            String timePublish = DurationFormatUtils.formatDuration(duration.toMillis(), "mm:ss");
            LOGGER.debug(String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_PUBLISH_SUCCESS), localVersionFile.getUploadDate(), filename, timePublish));
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_PUBLISH_SUCCESS), localVersionFile.getUploadDate(), filename, timePublish);
            notificationsManager.addNotification(buildNotification(Notification.INFO, message, null, null), utilisateur.getLogin());
        } catch (final BusinessException e) {
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_ERROR_DURING_PUBLICATION), filename);
            MDC.put("publication.error", message);
            notificationsManager.addNotification(buildNotification(Notification.ERROR, message, e.getMessage(), null), policyManager.getCurrentUserLogin());
            publishingVersionFiles.remove(versionFile);
            throw new BusinessException("business exception", e);
        } catch (final IOException | PersistenceException e) {
            UncatchedExceptionLogger.logUncatchedException(DefaultVersioningManager.UNCATCHED_EXEPTION_IN_PUBLISH_VERSION, e);
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_ERROR_DURING_PUBLICATION), filename);
            MDC.put("publication.error", message);
            notificationsManager.addNotification(buildNotification(Notification.ERROR, message, e.getMessage(), null), policyManager.getCurrentUserLogin());
            publishingVersionFiles.remove(versionFile);
            throw new BusinessException("can't publish version", e);
        } finally {
            publishingVersionFiles.remove(versionFile);
            DATASET_LOGGER.info(String.format("%s a déposé  le type de donnée %s", MDC.get("publication.user.login"), MDC.get("publication.fileName")));
        }
    }

    /**
     *
     * @param versionFile
     * @return
     * @throws BusinessException
     */
    protected DataTypeDescription findDatatype(VersionFile versionFile) throws BusinessException {
        org.inra.ecoinfo.dataset.config.impl.DataTypeDescription datatype = DataTypeDescription.genericDatatypeDescription;
        try {
            for (final org.inra.ecoinfo.dataset.config.impl.DataTypeDescription dt : configuration.getDataTypes()) {
                if (dt.getCode().equals(versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode())) {
                    datatype = dt;
                    break;
                }
            }
            if (datatype == null) {
                throw new PersistenceException("null datatype");
            }
        } catch (final PersistenceException e1) {
            LOGGER.info("no datatype", e1);
            notificationsManager.addNotification(buildNotification(Notification.ERROR, "erreur d'accès à la base", "Une erreur s'est produite lors de l'accès à la base.", null), policyManager.getCurrentUserLogin());
        }
        return datatype;
    }

    /**
     *
     * @param version
     * @param utilisateur
     * @param filename
     * @throws BusinessException
     */
    protected void trySanitize(final VersionFile version, final Utilisateur utilisateur, final String filename) throws BusinessException {
        String message;
        try {
            if (version.getDataset().isSanitize()) {
                version.setData(Utils.sanitizeData(version.getData(), localizationManager));
            }
        } catch (final BusinessException e) {
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_BAD_FILE_FORMAT), filename);
            notificationsManager.addNotification(buildNotification(Notification.ERROR, message, e.getMessage(), null), utilisateur.getLogin());
            throw new BusinessException("message", e);
        }
    }

    /**
     *
     * @param version
     * @param enableNotifications
     * @param recorder
     * @param utilisateur
     * @param filename
     * @throws BusinessException
     */
    protected void tryTestFormat(final VersionFile version, final boolean enableNotifications, IRecorder recorder, final Utilisateur utilisateur, final String filename) throws BusinessException {
        String message;
        try {
            final String encoding = Utils.detectStreamEncoding(version.getData());
            recorder.testFormat(version, encoding);
            recorder.testInDeepFilename(filename);
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_GOOD_FILE_FORMAT), filename);
            if (enableNotifications) {
                notificationsManager.addNotification(buildNotification(Notification.INFO, message, null, null), utilisateur.getLogin());
            }
        } catch (final BusinessException e) {
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_BAD_FILE_FORMAT), filename);
            notificationsManager.addNotification(buildNotification(Notification.ERROR, message, e.getMessage(), null), utilisateur.getLogin());
            throw new BusinessException(message, e);
        } catch (final IOException e) {
            UncatchedExceptionLogger.logUncatchedException(DefaultVersioningManager.UNCATCHED_EXEPTION_IN_CHECK_VERSION, e);
            message = String.format(localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_ERROR_DURING_CHECKING), filename);
            notificationsManager.addNotification(buildNotification(Notification.ERROR, message, e.getMessage(), null), policyManager.getCurrentUserLogin());
            throw new BusinessException("can't test format", e);
        }
    }

    /**
     * test the nullability of versionFile
     *
     * @param version
     * @throws BusinessException
     */
    protected void testIsNullVersion(final VersionFile version) throws BusinessException {
        if (version == null) {
            String message = localizationManager.getMessage(DefaultVersioningManager.BUNDLE_SOURCE_PATH, DefaultVersioningManager.MSG_NULL_VERSION);
            notificationsManager.addNotification(buildNotification(Notification.WARN, message, null, null), policyManager.getCurrentUserLogin());
            throw new BusinessException(message);
        }
    }

    class Overlap {

        protected static final int NOT_OVERLAPING = 0;
        protected static final int NOT_OVERLAPING_BUT_SAME_AT_END = -1;
        protected static final int NOT_OVERLAPING_BUT_SAME_AT_BEGIN = 1;

        /**
         * The version to publish
         */
        final VersionFile versionFile;
        /**
         * true if this version overlap another version in DB
         */
        boolean hasOverlapingDatesInDB = false;
        /**
         * sometimes version not overlap any version but the start date is the
         * same of the end date than an other publishing version or th end date
         * is the sams than the begin date of an other publishing version in
         * that case, wehave to wait theend of the publishing
         */
        int notOverlappingWithSameDate = NOT_OVERLAPING;
        /**
         * an other version from same dates and leafnode is trying publish
         */
        Boolean inList = false;
        /**
         * start date
         */
        final LocalDateTime dateFinPeriode;
        /*
         end date
         */
        final LocalDateTime dateDebutPeriode;
        String dateformat = DateUtil.DD_MM_YYYY;
        /*
        the associate data type
         */
        org.inra.ecoinfo.dataset.config.impl.DataTypeDescription datatype;

        public Overlap(VersionFile versionFile) throws BusinessException {
            this.versionFile = versionFile;
            dateFinPeriode = versionFile.getDataset().getDateFinPeriode();
            dateDebutPeriode = versionFile.getDataset().getDateDebutPeriode();
            dateformat = DateUtil.DD_MM_YYYY;
            datatype = findDatatype(versionFile);
            hasOverlapingDatesInDB();
        }

        /**
         * find if the version - has eoverlaping version in db - has other
         * version in publishing continueous
         *
         * @throws PersistenceException
         */
        protected void initInstance() throws PersistenceException {
            final LocalDateTime dateFinPeriode = versionFile.getDataset().getDateFinPeriode();
            final LocalDateTime dateDebutPeriode = versionFile.getDataset().getDateDebutPeriode();
            for (final VersionFile publishingVersionFile : publishingVersionFiles) {
                final LocalDateTime publishingDateDebutPeriode = publishingVersionFile.getDataset().getDateDebutPeriode();
                final LocalDateTime publishingDateFinPeriode = publishingVersionFile.getDataset().getDateFinPeriode();
                final String publishingPath = publishingVersionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getPath();
                final String path = versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getPath();
                if (publishingPath.equals(path)
                        && publishingDateDebutPeriode.compareTo(dateFinPeriode) <= 0
                        && publishingDateFinPeriode.compareTo(dateDebutPeriode) >= 0) {
                    inList = true;
                    break;
                }
                if (publishingPath.equals(path)
                        && publishingDateFinPeriode.equals(dateDebutPeriode)) {
                    notOverlappingWithSameDate = NOT_OVERLAPING_BUT_SAME_AT_BEGIN;
                } else if (publishingPath.equals(path)
                        && publishingDateDebutPeriode.equals(dateFinPeriode)) {
                    notOverlappingWithSameDate = NOT_OVERLAPING_BUT_SAME_AT_END;
                }
            }
        }

        protected void hasOverlapingDatesInDB() throws BusinessException {
            hasOverlapingDatesInDB = datasetDAO.hasOverlapingDates(versionFile);
        }

        /**
         * can record if there is not an other version with same dates and same
         * datatype trying publishing
         *
         * @return
         */
        protected boolean canRecord() {
            return !inList && !overlapDB();
        }

        protected boolean overlapDB() {
            return hasOverlapingDatesInDB && !datatype.canOverlapDates();
        }

        public String getDateformat() {
            return dateformat;
        }

    }
}
