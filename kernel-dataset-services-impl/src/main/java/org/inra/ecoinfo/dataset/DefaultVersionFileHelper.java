package org.inra.ecoinfo.dataset;

import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.exception.MalformedFilenameException;
import org.inra.ecoinfo.localization.IInternationalizable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class DefaultVersionFileHelper.
 */
public class DefaultVersionFileHelper implements IVersionFileHelper, IInternationalizable {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.messages";
    /**
     * The Constant PROPERTY_MSG_BAD_EXTENSION_FILENAME @link(String).
     */
    protected static final String PROPERTY_MSG_BAD_EXTENSION_FILENAME = "PROPERTY_MSG_BAD_EXTENSION_FILENAME";
    /**
     * The Constant PROPERTY_MSG_CHECK_FILENAME_FAILURE @link(String).
     */
    protected static final String PROPERTY_MSG_CHECK_FILENAME_FAILURE = "PROPERTY_MSG_CHECK_FILENAME_FAILURE";
    /**
     * The Constant PROPERTY_MSG_ERROR @link(String).
     */
    protected static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";
    /**
     * The Constant PROPERTY_MSG_MALFORMED_FILENAME @link(String).
     */
    protected static final String PROPERTY_MSG_MALFORMED_FILENAME = "PROPERTY_MSG_MALFORMED_FILENAME";
    /**
     * The Constant PROPERTY_MSG_MISMATCH_DATATYPE_SELECTION @link(String).
     */
    protected static final String PROPERTY_MSG_MISMATCH_DATATYPE_SELECTION = "PROPERTY_MSG_MISMATCH_DATATYPE_SELECTION";
    /**
     * The Constant PROPERTY_MSG_MISMATCH_SITE_PATH_SELECTION @link(String).
     */
    protected static final String PROPERTY_MSG_MISMATCH_SITE_PATH_SELECTION = "PROPERTY_MSG_MISMATCH_SITE_PATH_SELECTION";
    /**
     * The Constant UNDERSCORE @link(String).
     */
    protected static final String UNDERSCORE = "_";
    /**
     * The localization manager @link(ILocalizationManager).
     */
    protected ILocalizationManager localizationManager;
    /**
     * The notifications manager @link(INotificationsManager).
     */
    protected INotificationsManager notificationsManager;
    /**
     * The security context @link(IPolicyManager).
     */
    protected IPolicyManager policyManager;

    /**
     *
     */
    protected IDatasetConfiguration configuration;

    DefaultVersionFileHelper(IDatasetConfiguration datasetConfiguration) {
        configuration = datasetConfiguration;
    }

    /**
     *
     */
    public DefaultVersionFileHelper() {
    }

    /**
     * Builds the download filename.
     *
     * @param versionFile the version file
     * @return the string
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersionFileHelper#buildDownloadFilename(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String buildDownloadFilename(final VersionFile versionFile) {
        String downloadFileName = "";
        try {
            downloadFileName = versionFile.getDataset().buildDownloadFilename(configuration);
        } catch (final Exception e) {
            UncatchedExceptionLogger.log("can't build filename", e);
            return downloadFileName;
        }
        return downloadFileName;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see
     * org.inra.ecoinfo.localization.IInternationalizable#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the notifications manager.
     *
     * @param notificationsManager the new notifications manager
     */
    public void setNotificationsManager(final INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * Sets the security context.
     *
     * @param policyManager
     */
    public void setPolicyManager(final IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Test basic filename.
     *
     * @param uploadedFilename the uploaded filename
     * @param sitePath the site path
     * @param datatypeCode the datatype code
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersionFileHelper#testBasicFilename(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public void testBasicFilename(final String uploadedFilename, String sitePath, String datatypeCode) throws BusinessException {
        String returnSitePath = sitePath;
        String returnDatatypeCode = datatypeCode;
        final String[] splitFilename = uploadedFilename.split(DefaultVersionFileHelper.UNDERSCORE);
        returnSitePath = Utils.createCodeFromString(returnSitePath);
        returnDatatypeCode = Utils.createCodeFromString(returnDatatypeCode);
        final BadsFormatsReport badFormatReport = new BadsFormatsReport(localizationManager.getMessage(DefaultVersionFileHelper.BUNDLE_SOURCE_PATH, DefaultVersionFileHelper.PROPERTY_MSG_ERROR));
        checkMalformedFilename(uploadedFilename, splitFilename, badFormatReport);
        checkExtensionFile(uploadedFilename, badFormatReport);
        if (splitFilename.length == 4) {
            final String fileSitePath = Utils.createCodeFromString(splitFilename[0]);
            final String fileDatatype = Utils.createCodeFromString(splitFilename[1]);
            if (!returnSitePath.equalsIgnoreCase(fileSitePath)) {
                badFormatReport.addException(new MalformedFilenameException(String.format(localizationManager.getMessage(DefaultVersionFileHelper.BUNDLE_SOURCE_PATH, DefaultVersionFileHelper.PROPERTY_MSG_MISMATCH_SITE_PATH_SELECTION), fileSitePath,
                        returnSitePath)));
            }
            if (!returnDatatypeCode.equalsIgnoreCase(fileDatatype)) {
                badFormatReport.addException(new MalformedFilenameException(String.format(localizationManager.getMessage(DefaultVersionFileHelper.BUNDLE_SOURCE_PATH, DefaultVersionFileHelper.PROPERTY_MSG_MISMATCH_DATATYPE_SELECTION), fileDatatype,
                        returnDatatypeCode)));
            }
        }
        if (badFormatReport.hasErrors()) {
            notificationsManager.addNotification(new Notification(Notification.ERROR, String.format(localizationManager.getMessage(DefaultVersionFileHelper.BUNDLE_SOURCE_PATH, DefaultVersionFileHelper.PROPERTY_MSG_CHECK_FILENAME_FAILURE),
                    uploadedFilename), badFormatReport.getHTMLMessages()), policyManager.getCurrentUserLogin());
            throw new MalformedFilenameException(badFormatReport.getMessages());
        }
    }

    /**
     *
     * @param configuration
     */
    public void setConfiguration(IDatasetConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Check extension file.
     *
     * @param filename the filename
     * @param badFormatReport the bad format report
     * @link(String) the filename
     * @link(BadsFormatsReport) the bad format report
     */
    private void checkExtensionFile(final String filename, final BadsFormatsReport badFormatReport) {
        if (!filename.endsWith(IVersionFileHelper.EXTENSION_FILE_CSV)) {
            badFormatReport.addException(new MalformedFilenameException(String.format(localizationManager.getMessage(DefaultVersionFileHelper.BUNDLE_SOURCE_PATH, DefaultVersionFileHelper.PROPERTY_MSG_BAD_EXTENSION_FILENAME), filename,
                    IVersionFileHelper.EXTENSION_FILE_CSV)));
        }
    }

    /**
     * Check malformed filename.
     *
     * @param filename the filename
     * @param splitFilename the split filename
     * @param badFormatReport the bad format report
     * @link(String) the filename
     * @link(String[]) the split filename
     * @link(BadsFormatsReport) the bad format report
     */
    private void checkMalformedFilename(final String filename, final String[] splitFilename, final BadsFormatsReport badFormatReport) {
        if (splitFilename.length != 4) {
            badFormatReport.addException(new MalformedFilenameException(String.format(localizationManager.getMessage(DefaultVersionFileHelper.BUNDLE_SOURCE_PATH, DefaultVersionFileHelper.PROPERTY_MSG_MALFORMED_FILENAME), filename)));
        }
    }
}
