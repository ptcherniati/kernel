/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.dataset.ws.servicepublish;

import java.util.Optional;

/**
 *
 * @author ptcherniati
 */
interface IPublishWSDAO<T> {
    Optional<Long> getLeafrealNodeId(String context);
}
