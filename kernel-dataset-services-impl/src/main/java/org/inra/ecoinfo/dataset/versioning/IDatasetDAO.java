package org.inra.ecoinfo.dataset.versioning;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface IDatasetDAO.
 */
public interface IDatasetDAO extends IDAO<Dataset> {

    /**
     * Adds the.
     *
     * @param version the version
     * @return the dataset
     * @link(VersionFile) the version
     */
    Optional<Dataset> add(VersionFile version);

    /**
     * Gets the dataset by natural key.
     *
     * @param node
     * @param startDate the start date
     * @param endDate the end date
     * @return the dataset by natural key
     * @link(LeafNode) the leaf node
     * @link(Date) the start date
     * @link(Date) the end date
     */
    Optional<Dataset> getDatasetByNaturalKey(RealNode node, LocalDateTime startDate, LocalDateTime endDate);

    /**
     * Checks for overlaping dates.
     *
     * @param version the version
     * @return true, if successful
     * @link(VersionFile) the version
     */
    boolean hasOverlapingDates(VersionFile version);

    /**
     * Retrieve datasets.
     *
     * @param realNodeLeafId
     * @return the list
     * @link(LeafNode) the leaf node
     */
    List<Dataset> retrieveDatasets(Long realNodeLeafId);

    /**
     *
     * @param versionFile
     * @return
     */
    int unpublishVersion(VersionFile versionFile);
    
    /**
     *
     * @param utilisateur
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    void anonymiseUserDatasets(Utilisateur utilisateur, Utilisateur anonymousUser) throws PersistenceException;
}
