package org.inra.ecoinfo.dataset.ws.servicepublish;

//~--- non-JDK imports --------------------------------------------------------
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;

/**
 *
 * @author Antoine Schellenberger
 */
public class JPAPublishWSDAO extends AbstractJPADAO<Object> implements IPublishWSDAO<Object> {

    private String parcelle;
    protected List<CriteriaQuery<Tuple>> queryTupleList = new LinkedList();

    /**
     *
     */
    public JPAPublishWSDAO() {
    }

    @Override
    public Optional<Long> getLeafrealNodeId(String context) {
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<NodeDataSet> nds = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rn = nds.join(NodeDataSet_.realNode);
        query.where(builder.equal(rn.get(RealNode_.path), String.format(context)));
        query.select(rn.get(RealNode_.id));

        return getOptional(query);
    }
}
