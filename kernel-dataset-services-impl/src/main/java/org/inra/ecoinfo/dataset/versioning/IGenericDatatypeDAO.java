/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.dataset.versioning;

import java.util.List;
import java.util.Set;
import javax.persistence.Tuple;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.dataset.versioning.entity.GenericDatatype;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptchernia
 */
public interface IGenericDatatypeDAO extends IDAO<GenericDatatype>{

    /**
     *
     * @param versionFile
     */
    void deleteGenericDatatypeFromVersionFile(VersionFile versionFile);

    /**
     *
     * @param periods
     * @return
     */
    List<Tuple> getDatatype(List<IntervalDate> periods);

    /**
     *
     * @param nodeDataSets
     * @param intervalDates
     * @return
     */
    List<Tuple> getNodeDataSetFromNodesAndPeriods(Set<NodeDataSet> nodeDataSets, List<IntervalDate> intervalDates);
    
}
