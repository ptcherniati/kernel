package org.inra.ecoinfo.dataset.deserialization.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.IRecorderDAO;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.deserialization.IDatasetDeserialization;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.deserialization.AbstractDeserialization;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.TransactionStatus;

/**
 * The Class MailAdminDeserialization.
 */
public class DatasetDeserialization extends AbstractDeserialization implements IDatasetDeserialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/datasetDeserialization.xsd";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "datasetDeserialization";

    /**
     *
     */
    public static final String DATASET_ENTRY = "dataset";

    /**
     *
     */
    public static final String HEADER_CSV_FILE = "id;active;email;emploi;isroot;language;login;nom;password;poste;prenom";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    /**
     *
     */
    public static final String PATTERN_CONCAT_WORDS = "%s%s";

    /**
     *
     */
    public static final String START_FILE_NAME_PATTERN = String.format(PATTERN_CONCAT_WORDS, DATASET_ENTRY, File.separator);

    /**
     *
     */
    public static final String DATASET_CSV = "dataset/dataset.csv";
    private String id;
    /**
     * The mail admin.
     */
    private IUtilisateurDAO utilisateurDAO;

    /**
     *
     */
    protected IDatasetConfiguration datasetConfiguration;

    /**
     *
     */
    protected IRecorderDAO recorderDAO;
    private ICoreConfiguration configuration;
    private IDatasetManager datasetManager;
    private IPolicyManager policyManager;
    private IDatasetDAO datasetDAO;

    /**
     *
     */
    protected IVersionFileDAO versionFileDAO;

    /**
     *
     * @param versionFileDAO
     */
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    /**
     *
     * @param datasetDAO
     */
    public void setDatasetDAO(IDatasetDAO datasetDAO) {
        this.datasetDAO = datasetDAO;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param datasetManager
     */
    public void setDatasetManager(IDatasetManager datasetManager) {
        this.datasetManager = datasetManager;
    }

    /**
     *
     * @param configuration
     */
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     *
     * @param datasetConfiguration
     */
    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }

    /**
     *
     * @param recorderDAO
     */
    public void setRecorderDAO(IRecorderDAO recorderDAO) {
        this.recorderDAO = recorderDAO;
    }
    
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        super.createConfig(digester);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#getModuleName()
     */
    /**
     *
     * @return
     */
    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractDeserialization#getSchemaPath()
     */
    /**
     *
     * @return
     */
    @Override
    public String getSchemaPath() {
        return SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleDeserialization#isAutoLoad()
     */
    /**
     *
     * @return
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param inZip
     * @throws BusinessException
     */
    @Override
    public void deSerialize(File inZip) throws BusinessException {
        LOGGER.info("*************************************************************");
        LOGGER.info("*                 déserialization dataset                   *");
        LOGGER.info("*************************************************************");
        LOGGER.info("");
        LOGGER.info(getClass().getName());
        LOGGER.info("");
        LOGGER.info("");
        TransactionStatus tr = utilisateurDAO.beginDefaultTransaction();
        try {
            init(policyManager);
            List<String> lines = readCSV(inZip);
            for (String line : lines) {
                readFile(line, inZip);
            }
            utilisateurDAO.commitTransaction(tr);
        } catch (PersistenceException ex) {
            utilisateurDAO.rollbackTransaction(tr);
            throw new BusinessException("no root user", ex);
        }

    }

    /**
     *
     * @param line
     * @param inZip
     * @throws BusinessException
     */
    public void readFile(String line, File inZip) throws BusinessException {
        LOGGER.info("\n{}\n", line);
        String[] cells = line.split(";");
        String fileName = cells[1];
        String filePath = String.format(PATTERN_CONCAT_WORDS, START_FILE_NAME_PATTERN, fileName);
        LOGGER.info(String.format("reading file %s", filePath));
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(inZip));) {
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                String entryName = zipEntry.getName();
                if (entryName.equals(filePath)) {
                    readFile(cells, zis, zipEntry);
                }
                zipEntry = zis.getNextEntry();
            }
        } catch (IOException ex) {
            throw new BusinessException(String.format("can't write file %s", inZip.getAbsolutePath()), ex);
        }
    }

    /**
     *
     * @param inZip
     * @return
     * @throws BusinessException
     */
    public List<String> readCSV(File inZip) throws BusinessException {
        List<String> lines = new LinkedList();
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(inZip), StandardCharsets.UTF_8);) {
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                String entryName = zipEntry.getName();
                if (entryName.equals(DATASET_CSV)) {
                    Scanner sc = new Scanner(zis, StandardCharsets.UTF_8.name());
                    String line;
                    int i = 0;
                    while (sc.hasNextLine()) {
                        i++;
                        line = sc.nextLine();
                        if (i == 1) {
                            continue;
                        }
                        lines.add(line);
                    }
                    zis.close();
                    break;
                }
                zipEntry = zis.getNextEntry();
            }
            return lines;
        } catch (IOException ex) {
            throw new BusinessException(String.format("can't write file %s", inZip.getAbsolutePath()), ex);
        }
    }

    private void readFile(String[] cells, InputStream zis, ZipEntry entry) throws BusinessException {

        byte[] buffer = new byte[1_024];
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            long count = 0;
            int n = 0;
            while (-1 != (n = zis.read(buffer))) {
                out.write(buffer, 0, n);
                count += n;
            }
            byte[] data = out.toByteArray();
            uploadFile(cells, data);
        } catch (IOException ex) {
            LOGGER.error("can't read entry", ex);
        }
    }

    private void init(IPolicyManager policyManager) throws BusinessException, PersistenceException {
        IUser rootUtilisateur = utilisateurDAO.getAdmins().get(0);
        policyManager.setCurrentUser(rootUtilisateur);
    }

    private void uploadFile(String[] cells, byte[] data) {
        try {
            Optional<IMgaIOConfiguration> configuration = policyManager.getMgaServiceBuilder().getMgaIOConfigurator().getConfiguration(AbstractMgaIOConfigurator.DATASET_CONFIGURATION);
            if (!configuration.isPresent()) {
                return;
            }
            IMgaIOConfiguration conf = configuration.get();
            VersionFile versionFile = null;
            String nodePath = cells[0];
            final String fileName = cells[1];
            Optional<INode> optDatatypeNode = policyManager.getMgaServiceBuilder().loadNodes(conf, true, false)
                    .filter(datatypeNode -> datatypeNode.getRealNode().getPath().equals(nodePath))
                    .findAny();
            if (optDatatypeNode.isPresent()) {
                INode datatypeNode = optDatatypeNode.get();
                LOGGER.info(String.format("reading file -- %s -- %s --  %s", nodePath, datatypeNode.getPath(), fileName));
                versionFile = datasetManager.getVersionFromFileName(fileName, datatypeNode.getRealNode().getId());
            }
            if (versionFile == null) {
                LOGGER.error(String.format("No version for file name %s", fileName));
                return;
            }
            boolean isPublish = Boolean.valueOf(cells[2]);
            LocalDateTime uploadDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, cells[3]);
            LocalDateTime createDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, cells[4]);
            Utilisateur uploadUser = utilisateurDAO.getByLogin(cells[5]).orElseThrow(PersistenceException::new);
            Utilisateur createUser = utilisateurDAO.getByLogin(cells[6]).orElseThrow(PersistenceException::new);
            versionFile.setData(Utils.convertEncoding(data, Utils.ENCODING_UTF8));
            policyManager.setCurrentUser(uploadUser);
            Dataset dataset = datasetManager.uploadVersion(versionFile);
            dataset = datasetDAO.merge(dataset);
            dataset.getLastVersion().setUploadDate(uploadDate);
            dataset.setCreateDate(createDate);
            dataset.setCreateUser(createUser);
            if (isPublish) {
                publish(dataset, cells);
            }
        } catch (BusinessException | PersistenceException | DateTimeException | IOException ex) {
            LOGGER.error(String.format("Can't get version %s", cells[0]), ex);
        }
    }

    private void publish(Dataset dataset, String[] cells) {
        try {
            final VersionFile lastVersion = dataset.getLastVersion();
            Utilisateur publishUser = utilisateurDAO.getByLogin(cells[9]).orElseThrow(PersistenceException::new);
            policyManager.setCurrentUser(publishUser);
            datasetManager.publishVersion(lastVersion);
            dataset.setPublishComment(cells[7]);
            LocalDateTime publishDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, cells[8]);
            dataset.setPublishDate(publishDate);
            dataset.setPublishUser(publishUser);
        } catch (BusinessException | DateTimeException | PersistenceException ex) {
            LOGGER.error(String.format("cant' publish versions %s version %s", dataset.getRealNode().getPath(), dataset.getLastVersionNumber()), ex);
        }
    }
}
