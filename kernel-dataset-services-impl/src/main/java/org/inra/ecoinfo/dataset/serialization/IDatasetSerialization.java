package org.inra.ecoinfo.dataset.serialization;

/**
 * The Interface IIdSerialization.
 */
public interface IDatasetSerialization {

    /**
     *
     * @return
     */
    String getId();
}
