/**
 * OREILacs project - see LICENCE.txt for use created: 25 févr. 2009 19:28:07
 */
package org.inra.ecoinfo.dataset;

import com.Ostermiller.util.BadDelimiterException;
import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.time.DateTimeException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Transient;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.IInternationalizable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadIntervalDateException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NullValueException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AbstractRecorder.
 *
 * @author "Antoine Schellenberger"
 */
public abstract class AbstractRecorder implements IRecorder, IInternationalizable {
    /**
     * The Constant KERNEL_BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String KERNEL_BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.impl.messages";
    /**
     * The LOGGER @link(Logger).
     */
    @Transient
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractRecorder.class.getName());

    /**
     * The Constant DATASET_DESCRIPTOR_XML @link(String).
     */
    protected static final String DATASET_DESCRIPTOR_XML = "dataset-descriptor.xml";
    /**
     * The Constant DATE_TYPE @link(String).
     */
    protected static final String DATE_TYPE = "date";
    /**
     * The Constant FLOAT_TYPE @link(String).
     */
    protected static final String FLOAT_TYPE = "float";
    /**
     * The Constant INTEGER_TYPE @link(String).
     */
    protected static final String INTEGER_TYPE = "integer";
    /**
     * The Constant PROPERTY_MSG_BAD_INTERVAL_DATE_VALUE @link(String).
     */
    protected static final String PROPERTY_MSG_BAD_INTERVAL_DATE_VALUE = "PROPERTY_MSG_BAD_INTERVAL_DATE_VALUE";
    /**
     * The Constant PROPERTY_MSG_DATE_VALUE_EXPECTED @link(String).
     */
    protected static final String PROPERTY_MSG_DATE_VALUE_EXPECTED = "PROPERTY_MSG_DATE_VALUE_EXPECTED";
    /**
     * The Constant PROPERTY_MSG_FLOAT_VALUE_EXPECTED @link(String).
     */
    protected static final String PROPERTY_MSG_FLOAT_VALUE_EXPECTED = "PROPERTY_MSG_FLOAT_VALUE_EXPECTED";
    /**
     * The Constant PROPERTY_MSG_INTEGER_VALUE_EXPECTED @link(String).
     */
    protected static final String PROPERTY_MSG_INTEGER_VALUE_EXPECTED = "PROPERTY_MSG_INTEGER_VALUE_EXPECTED";
    /**
     * The Constant PROPERTY_MSG_MISMATCH_COLUMN_HEADER @link(String).
     */
    protected static final String PROPERTY_MSG_MISMATCH_COLUMN_HEADER = "PROPERTY_MSG_MISMATCH_COLUMN_HEADER";
    /**
     * The Constant PROPERTY_MSG_TIME_VALUE_EXPECTED @link(String).
     */
    protected static final String PROPERTY_MSG_TIME_VALUE_EXPECTED = "PROPERTY_MSG_TIME_VALUE_EXPECTED";
    /**
     * The Constant PROPERTY_MSG_VALUE_EXPECTED @link(String).
     */
    protected static final String PROPERTY_MSG_VALUE_EXPECTED = "PROPERTY_MSG_VALUE_EXPECTED";
    /**
     * The Constant SEPARATOR @link(char).
     */
    protected static final char SEPARATOR = ';';
    /**
     * The Constant serialVersionUID @link(long).
     */
    protected static final long serialVersionUID = 1L;
    /**
     * The Constant TIME_TYPE @link(String).
     */
    protected static final String TIME_TYPE = "time";
    /**
     * The localization manager @link(ILocalizationManager).
     */
    private ILocalizationManager localizationManager;
    /**
     * The dataset descriptor @link(DatasetDescriptor).
     */
    protected DatasetDescriptor datasetDescriptor;

    /*
     * *
     *
     * @see org.inra.ore.business.rmi.IRMIRecorder#testFormatOnly(java.util .List)
     */
    /**
     * Gets the localization manager.
     *
     * @return the localization manager
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see
     * org.inra.ecoinfo.dataset.IRecorder#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Record.
     *
     * @param versionFile the version file
     * @param fileEncoding the file encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.IRecorder#record(org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * java.lang.String)
     */
    @Override
    public void record(final VersionFile versionFile, final String fileEncoding) throws BusinessException {
        try {
            testFormat( versionFile, fileEncoding);
            final CSVParser parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(versionFile.getData()), fileEncoding), AbstractRecorder.SEPARATOR);
            processRecord(parser, versionFile, fileEncoding);
        } catch (final BadDelimiterException | UnsupportedEncodingException e) {
            LOGGER.error(e.getMessage());
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the dataset descriptor.
     *
     * @param datasetDescriptor the new dataset descriptor
     */
    public void setDatasetDescriptor(final DatasetDescriptor datasetDescriptor) {
        this.datasetDescriptor = datasetDescriptor;
    }

    /**
     * Sets the version file dao.
     *
     * @param versionFileDAO the new version file dao {@link IVersionFileDAO}
     * the new version file dao
     */
    public abstract void setVersionFileDAO(final IVersionFileDAO versionFileDAO);

    /**
     * Test format.
     *
     * @param versionFile the version file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.IRecorder#testFormat(org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * java.lang.String)
     */
    @Override
    public abstract void testFormat(VersionFile versionFile, String encoding) throws BusinessException;

    /**
     * Test in deep filename.
     *
     * @param fileName the file name
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.IRecorder#testInDeepFilename(java.lang.String)
     */
    @Override
    public void testInDeepFilename(final String fileName) throws BusinessException {
        // TODO A implémenter
    }

    /**
     * Check date value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkDateValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        if (AbstractRecorder.DATE_TYPE.equalsIgnoreCase(column.getValueType().trim()) && value.length() > 0) {
            try {
                DateUtil.readLocalDateTimeFromText(column.getFormatType(), value);
            } catch (final DateTimeException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(AbstractRecorder.KERNEL_BUNDLE_SOURCE_PATH, AbstractRecorder.PROPERTY_MSG_DATE_VALUE_EXPECTED), column.getFormatType(), lineNumber,
                                                                                                                                                                                                                                              i + 1, column.getName(), value), e));
            }
        }
    }

    /**
     * Check floatvalue.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @return the string
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private String checkFloatvalue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, String value, final Column column) {
        String localValue = value;
        if (AbstractRecorder.FLOAT_TYPE.equalsIgnoreCase(column.getValueType().trim()) && localValue.length() > 0) {
            try {
                localValue = localValue.replaceAll(",", ".");
                localValue = localValue.replaceAll(" ", "");
                Float.parseFloat(localValue);
            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(AbstractRecorder.KERNEL_BUNDLE_SOURCE_PATH, AbstractRecorder.PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineNumber, i + 1,
                        column.getName(), localValue), e));
            }
        }
        return localValue;
    }

    /**
     * Check integer value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkIntegerValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, String value, final Column column) {
        String localValue = value;
        if (AbstractRecorder.INTEGER_TYPE.equalsIgnoreCase(column.getValueType().trim()) && localValue.length() > 0) {
            try {
                localValue = localValue.replaceAll(" ", "");
                Integer.parseInt(localValue);
            } catch (final NumberFormatException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(AbstractRecorder.KERNEL_BUNDLE_SOURCE_PATH, AbstractRecorder.PROPERTY_MSG_INTEGER_VALUE_EXPECTED), lineNumber, i + 1,
                        column.getName(), localValue), e));
            }
        }
    }

    /**
     * Check null value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkNullValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        try {
            if (!column.isNullable() && StringUtils.isEmpty(value)) {
                throw new NullValueException(String.format(localizationManager.getMessage(AbstractRecorder.KERNEL_BUNDLE_SOURCE_PATH, AbstractRecorder.PROPERTY_MSG_VALUE_EXPECTED), lineNumber, i + 1, column.getName()));
            }
        }catch (final NullValueException e) {
            LOGGER.error("bad format", e);
            badsFormatsReport.addException(e);
        }
    }

    /**
     * Check time value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkTimeValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        if (AbstractRecorder.TIME_TYPE.equalsIgnoreCase(column.getValueType().trim()) && value.length() > 0) {
            try {
                DateUtil.readLocalDateTimeFromText(column.getFormatType(), value);
            } catch (final DateTimeException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(AbstractRecorder.KERNEL_BUNDLE_SOURCE_PATH, AbstractRecorder.PROPERTY_MSG_TIME_VALUE_EXPECTED), column.getFormatType(), lineNumber,
                        i + 1, column.getName(), value), e));
            }
        }
    }

    /**
     * Check value by type.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    private void checkValueByType(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        if (column.getValueType() != null) {
            checkDateValue(badsFormatsReport, lineNumber, i, value, column);
            checkTimeValue(badsFormatsReport, lineNumber, i, value, column);
            checkFloatvalue(badsFormatsReport, lineNumber, i, value, column);
            checkIntegerValue(badsFormatsReport, lineNumber, i, value, column);
        }
    }
    
    private void testLines(final BadsFormatsReport badsFormatsReport, final String[] values) {
        for (int index = 0; index < values.length; index++) {
            if (index > datasetDescriptor.getColumns().size() - 1) {
                break;
            }
            final String value = values[index].trim();
            final Column column = datasetDescriptor.getColumns().get(index);
            final String codifiedConfigColumnNameHeader = Utils.createCodeFromString(column.getName());
            final String codifiedFileColumnNameHeader = Utils.createCodeFromString(value);
            if (!codifiedConfigColumnNameHeader.equals(codifiedFileColumnNameHeader)) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(localizationManager.getMessage(AbstractRecorder.KERNEL_BUNDLE_SOURCE_PATH, AbstractRecorder.PROPERTY_MSG_MISMATCH_COLUMN_HEADER), index + 1, value,
                        column.getName())));
            }
        }
    }

    /**
     * Check value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     */
    protected void checkValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column) {
        checkNullValue(badsFormatsReport, lineNumber, i, value, column);
        checkValueByType(badsFormatsReport, lineNumber, i, value, column);
    }

    /**
     * Check value.
     *
     * @param badsFormatsReport the bads formats report
     * @param lineNumber the line number
     * @param i the i
     * @param value the value
     * @param column the column
     * @param interval the interval
     * @link(BadsFormatsReport) the bads formats report
     * @link(int) the line number
     * @link(int) the i
     * @link(String) the value
     * @link(Column) the column
     * @link(IntervalDate) the interval
     */
    protected void checkValue(final BadsFormatsReport badsFormatsReport, final int lineNumber, final int i, final String value, final Column column, final IntervalDate interval) {
        checkValue(badsFormatsReport, lineNumber, i, value, column);
        if (column.getValueType() != null && AbstractRecorder.DATE_TYPE.equalsIgnoreCase(column.getValueType().trim()) && column.isDateForInterval() && value.length() > 0) {
            try {
                if (!interval.isInto(DateUtil.readLocalDateTimeFromText(value, column.getFormatType()))) {
                    badsFormatsReport.addException(new BadIntervalDateException(String.format(localizationManager.getMessage(AbstractRecorder.KERNEL_BUNDLE_SOURCE_PATH, AbstractRecorder.PROPERTY_MSG_BAD_INTERVAL_DATE_VALUE), value, lineNumber,
                                                                                                                                                                                                                                        i + 1, interval.getBeginDateToString(), interval.getEndDateToString())));
                }
            } catch (final DateTimeException e) {
                badsFormatsReport.addException(new BadValueTypeException(String.format(localizationManager.getMessage(AbstractRecorder.KERNEL_BUNDLE_SOURCE_PATH, AbstractRecorder.PROPERTY_MSG_DATE_VALUE_EXPECTED), column.getFormatType(), lineNumber,
                        i + 1, column.getName(), value), e));
            }
        }
    }

    /**
     * Cette méthode est appelée depuis testFormat() de CSVMetadataRecorder.
     *
     * @param badsFormatsReport the bads formats report
     * @param parser the parser
     * @return le nombre de ligne analysée
     * @throws BusinessException the business exception
     * @link(BadsFormatsReport) the bads formats report
     * @link(CSVParser) the parser
     */
    protected int genericTestHeader(final BadsFormatsReport badsFormatsReport, final CSVParser parser) throws BusinessException {

        try {
            int lineNumber = 0;
            final String[] values = parser.getLine();
            lineNumber++;
            testLines(badsFormatsReport, values);
            return lineNumber;
        } catch (IOException ex) {
            throw new BusinessException(ex);
        }
    }

    /**
     * Process record.
     *
     * @param parser the parser
     * @param versionFile the version file
     * @param fileEncoding the file encoding
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(String) the file encoding
     */
    protected abstract void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding) throws BusinessException;

    /**
     * Skip header and retrieve variable name.
     *
     * @param variableColumnIndex the variable column index
     * @param parser the parser
     * @return the list
     * @throws IOException Signals that an I/O exception has occurred.
     * @link(int) the variable column index
     * @link(CSVParser) the parser
     */
    protected List<String> skipHeaderAndRetrieveVariableName(final int variableColumnIndex, final CSVParser parser) throws IOException {
        final String[] values = parser.getLine();
        final List<String> variablesNames = new LinkedList<>();
        for (int vci = variableColumnIndex; vci <= values.length; vci++) {
            variablesNames.add(Utils.createCodeFromString(values[vci - 1]));
        }
        return variablesNames;
        // TODO A implémenter
    }
    
    /**
     * The Class CleanerValues.
     */
    protected static class CleanerValues {
        
        /**
         * The value index @link(int).
         */
        private int valueIndex = 0;
        /**
         * The values @link(String[]).
         */
        private final String[] values;
        
        /**
         * Instantiates a new cleaner values.
         *
         * @param val the val
         * @link(String[]) the val
         */
        public CleanerValues(final String[] val) {
            if (val == null) {
                this.values = new String[0];
            } else {
                this.values = Arrays.copyOf(val, val.length);
            }
        }
        
        /**
         * Current token.
         *
         * @return the string
         */
        public String currentToken() {
            return values[valueIndex].toLowerCase().trim();
        }

        /**
         * Current token index.
         *
         * @return the int
         */
        public int currentTokenIndex() {
            return valueIndex;
        }

        /**
         * Next token.
         *
         * @return the string
         */
        public String nextToken() {
            return values[valueIndex++].toLowerCase().trim();
        }
    }
}
