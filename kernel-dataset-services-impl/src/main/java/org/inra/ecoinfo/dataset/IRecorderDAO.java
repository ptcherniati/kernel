/**
 * OREILacs project - see LICENCE.txt for use created: 20 févr. 2009 18:46:05
 */
package org.inra.ecoinfo.dataset;

import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface IRecorderDAO.
 *
 * @author "Antoine Schellenberger"
 */
public interface IRecorderDAO {

    /**
     * Gets the recorder by data type code.
     *
     * @param dataTypeCode the data type code
     * @return the recorder by data type code
     * @throws PersistenceException the persistence exception
     * @link(String) the data type code
     */
    IRecorder getRecorderByDataTypeCode(String dataTypeCode) throws PersistenceException;
}
