package org.inra.ecoinfo.dataset.impl.filenamecheckers;

import com.google.common.base.Strings;
import java.util.regex.Matcher;
import org.inra.ecoinfo.dataset.IFileNameChecker;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 * The Class FileNameCheckermmyyyy.
 */
public class FileNameCheckermmyyyy extends AbstractFileNameChecker {
    private static final String DATE_PATTERN = DateUtil.MM_YYYY_FILE;

    /**
     * Gets the file path.
     *
     * @param dataset
     * @return the file path
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String getFilePath(final Dataset dataset) {
        String currentSite = dataset.getRealNode().getNodeByNodeableTypeResource(Site.class).getCode();
        final String currentDatatype = dataset.getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode();
        if (!Strings.isNullOrEmpty(configuration.getSiteSeparatorForFileNames())) {
            currentSite = currentSite.replaceAll(PatternConfigurator.UNDERSCORE, configuration.getSiteSeparatorForFileNames());
        }
        return String.format(IFileNameChecker.PATTERN_FILE_NAME_PATH, 
                currentSite, 
                currentDatatype,
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(), DATE_PATTERN),
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), DATE_PATTERN),
                dataset.getVersions().size());
    }

    /**
     * Gets the file path.
     *
     * @param version the version
     * @return the file path
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String getFilePath(final VersionFile version) {
        String currentSite = version.getDataset().getRealNode().getNodeByNodeableTypeResource(Site.class).getCode();
        final String currentDatatype = version.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode();
        if (!Strings.isNullOrEmpty(configuration.getSiteSeparatorForFileNames())) {
            currentSite = currentSite.replaceAll(PatternConfigurator.UNDERSCORE, configuration.getSiteSeparatorForFileNames());
        }
        return String.format(IFileNameChecker.PATTERN_FILE_NAME_PATH, 
                currentSite, 
                currentDatatype,
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateDebutPeriode(), DATE_PATTERN),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateFinPeriode(), DATE_PATTERN),
                version.getVersionNumber());
    }

    /**
     * Test dates.
     *
     * @param currentSite the current site
     * @param currentDatatype the current datatype
     * @param splitFilename the split filename
     * @return the interval date
     * @throws InvalidFileNameException the invalid file name exception
     * @link(String) the current site
     * @link(String) the current datatype
     * @link(Matcher) the split filename
     */
    private IntervalDate testDates(final String currentSite, final String currentDatatype, final Matcher splitFilename) throws InvalidFileNameException {
        IntervalDate intervalDate;
        try {
            intervalDate = IntervalDate.getIntervalDateMMyyyy(splitFilename.group(3), splitFilename.group(4));
        } catch (final BadExpectedValueException e1) {
            throw new InvalidFileNameException(
                    String.format(AbstractFileNameChecker.INVALID_FILE_NAME, 
                            currentSite, 
                            currentDatatype, 
                            DateUtil.MM_YYYY_FILE, 
                            DateUtil.MM_YYYY_FILE
                    ), 
                    e1);
        }
        return intervalDate;
    }

    /**
     * Gets the date pattern.
     *
     * @return the date pattern
     * @see
     * org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker#getDatePattern()
     */
    @Override
    protected String getDatePattern() {
        return DateUtil.MM_YYYY_FILE;
    }

    /**
     * Test dates.
     *
     * @param version the version
     * @param currentSite the current site
     * @param currentDatatype the current datatype
     * @param splitFilename the split filename
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @see
     * org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker#testDates(org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * java.lang.String, java.lang.String, java.util.regex.Matcher)
     */
    @Override
    protected void testDates(final VersionFile version, final String currentSite, final String currentDatatype, final Matcher splitFilename) throws InvalidFileNameException {
        final IntervalDate intervalDate = testDates(currentSite, currentDatatype, splitFilename);

        if (version != null) {
            version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
            version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
        }
    }
}
