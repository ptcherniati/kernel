package org.inra.ecoinfo.dataset.versioning.impl;

import com.google.common.base.Strings;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.dataset.IFileNameChecker;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.config.impl.DataTypeDescription;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.ICurrentSelection;
import org.inra.ecoinfo.dataset.versioning.IVersionManager;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The Class AbstractVersioningManager.
 */
public abstract class AbstractVersioningManager extends MO implements IVersionManager, ApplicationContextAware {

    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.versioning.impl.messages";
    /**
     * The Constant PROPERTY_MSG_FILE_FORMAT_BASE @link(String).
     */
    private static final String PROPERTY_MSG_FILE_FORMAT_BASE = "PROPERTY_MSG_FILE_FORMAT_BASE";
    /**
     * The application context @link(ApplicationContext).
     */
    private ApplicationContext applicationContext;
    /**
     * The default file name checkers @link(List<IFileNameChecker>).
     */
    private List<IFileNameChecker> defaultFileNameCheckers;
    private List<IFileNameChecker> permissiveFileNameCheckers;
    /**
     * The configuration @link(Configuration).
     */
    protected IDatasetConfiguration configuration;
    /**
     * The name file checkers map @link(Map<String,List<IFileNameChecker>>).
     */
    protected Map<String, List<IFileNameChecker>> nameFileCheckersMap = new HashMap<>();

    /**
     * Gets the download file name.
     *
     * @param versionFile the version file
     * @return the download file name
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.impl.IVersionManager#getDownloadFileName(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String getDownloadFileName(final VersionFile versionFile) throws BusinessException {
        if (!Strings.isNullOrEmpty(versionFile.getFileName())) {
            return String.format("%s_%s_%s.%s",
                    versionFile.getFileName(),
                    DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateDebutPeriode(), DateUtil.DD_MM_YYYY_FILE),
                    DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateFinPeriode(), DateUtil.DD_MM_YYYY_FILE),
                    versionFile.getExtention());
        }
        return versionFile.getDataset().buildDownloadFilename(configuration);
    }

    /**
     * Gets the version.
     *
     * @param currentSelection the current selection
     * @return the version
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.impl.IVersionManager#getVersion(org.inra.ecoinfo.dataset.versioning.ICurrentSelection)
     */
    @Override
    public VersionFile getVersion(final ICurrentSelection currentSelection) throws BusinessException {
        return null;
    }

    /**
     * Gets the version.
     *
     * @param fileName the file name
     * @return the version
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.impl.IVersionManager#getVersion(java.lang.String)
     */
    @Override
    public VersionFile getVersion(final String fileName) throws BusinessException {
        return null;
    }

    /**
     * Inits the.
     */
    public void init() {
        for (final DataTypeDescription dataType : configuration.getDataTypes()) {
            for (final String namefileChecker : dataType.getNameFilesCheckers()) {
                if (!nameFileCheckersMap.containsKey(dataType.getCode())) {
                    nameFileCheckersMap.put(dataType.getCode(), new LinkedList<>());
                }
                final IFileNameChecker fileNameChecker = (IFileNameChecker) applicationContext.getBean(namefileChecker);
                nameFileCheckersMap.get(dataType.getCode()).add(fileNameChecker);
                dataType.getFileCheckers().add(fileNameChecker);
            }
        }
    }

    /**
     * Checks if is valid file name.
     *
     * @param fileName the file name
     * @param realNodeLeafId
     * @return true, if successful
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.versioning.impl.IVersionManager#IsValidFileName(java.lang.String,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public Optional<VersionFile> testIsValidFileNameAndGetNewVersion(String fileName, Long realNodeLeafId) throws BusinessException {
        RealNode realNode = policyManager.getMgaServiceBuilder().getRecorder().getRealNodeById(realNodeLeafId).orElseThrow(BusinessException::new);
        VersionFile version = new VersionFile();
        version.getDataset().setRealNode(realNode);
        String datatypeCode = realNode.getNodeByNodeableTypeResource(DataType.class).getCode();
        List<IFileNameChecker> fileNameCheckers = nameFileCheckersMap.get(datatypeCode);
        final ErrorsReport errorsReport = new ErrorsReport(localizationManager.getMessage(ErrorsReport.BUNDLE_NAME, ErrorsReport.PROPERTY_MSG_ERROR));
        if (fileNameCheckers == null || fileNameCheckers.isEmpty()) {
            if (configuration.getDataTypes().stream()
                    .anyMatch(d -> d.getCode().equals(datatypeCode))) {
                fileNameCheckers = defaultFileNameCheckers;
            } else {
                fileNameCheckers=permissiveFileNameCheckers;
            }
        }
        for (final IFileNameChecker fileNameChecker : fileNameCheckers) {
            try {
                if (fileNameChecker.isValidFileName(fileName, version)) {
                    return Optional.ofNullable(version);
                }
            } catch (final InvalidFileNameException e) {
                UncatchedExceptionLogger.log("invalid file name", e);
                errorsReport.addException(e);
            } catch (final Exception e) {
                UncatchedExceptionLogger.log("invalid file name", e);
                errorsReport.addException(new BusinessException("invalid file name"));
            }
        }
        if (errorsReport.hasErrors()) {
            String message = localizationManager.getMessage(AbstractVersioningManager.BUNDLE_SOURCE_PATH, AbstractVersioningManager.PROPERTY_MSG_FILE_FORMAT_BASE);
            for (final String errorMessage : errorsReport.getErrorsMessages()) {
                message = message.concat("\n").concat(errorMessage);
            }
            throw new BusinessException(message);
        }
        return Optional.empty();
    }

    /**
     * Sets the application context.
     *
     * @param arg0 the new application context
     * @see
     * org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    @Override
    public void setApplicationContext(final ApplicationContext arg0) {
        this.applicationContext = arg0;
    }

    /**
     * Sets the default file name checkers.
     *
     * @param defaultFileNameCheckers the new default file name checkers
     */
    public void setDefaultFileNameCheckers(final List<IFileNameChecker> defaultFileNameCheckers) {
        this.defaultFileNameCheckers = defaultFileNameCheckers;
    }

    /**
     *
     * @param permissiveFileNameCheckers
     */
    public void setPermissiveFileNameCheckers(List<IFileNameChecker> permissiveFileNameCheckers) {
        this.permissiveFileNameCheckers = permissiveFileNameCheckers;
    }
}
