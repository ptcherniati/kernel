/**
 *
 */
package org.inra.ecoinfo.dataset.impl;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.IRecorderDAO;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver;
import org.inra.ecoinfo.dataset.versioning.IVersioningManager;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.IUsersDeletion;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.middleware.MgaRecorder;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.InfosReport;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.DeleteUserException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class DefaultDatasetManager.
 *
 * @author "Antoine Schellenberger"
 */
public class DefaultDatasetManager extends MO implements IUsersDeletion, IDatasetManager {
    
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.impl.messages";
    
    /**
     * The Constant BUNDLE_SOURCE_PATH_VERSIONING @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH_VERSIONING = "org.inra.ecoinfo.dataset.versioning.impl.messages";

    /**
     * The Constant PROPERTY_MSG_BAD_FILE_FORMAT @link(String).
     */
    public static final String PROPERTY_MSG_BAD_FILE_FORMAT = "PROPERTY_MSG_BAD_FILE_FORMAT";
    
    /**
     * The Constant PROPERTY_MSG_FOR_SUCCESS_DATASET_DELETION @link(String).
     */
    public static final String PROPERTY_MSG_FOR_SUCCESS_DATASET_ANONYMISATION = "PROPERTY_MSG_FOR_SUCCESS_DATASET_ANONYMISATION";
    
    /**
     * The Constant PROPERTY_MSG_FOR_ECHEC_DATASET_DELETION @link(String).
     */
    public static final String PROPERTY_MSG_FOR_ECHEC_DATASET_ANONYMISATION = "PROPERTY_MSG_FOR_ECHEC_DATASET_ANONYMISATION";
    
    /**
     * The Constant PROPERTY_MSG_FOR_SUCCESS_VERSIONFILE_DELETION @link(String).
     */
    public static final String PROPERTY_MSG_FOR_SUCCESS_VERSIONFILE_ANONYMISATION = "PROPERTY_MSG_FOR_SUCCESS_VERSIONFILE_ANONYMISATION";
    
    /**
     * The Constant PROPERTY_MSG_FOR_ECHEC_VERSIONFILE_DELETION @link(String).
     */
    public static final String PROPERTY_MSG_FOR_ECHEC_VERSIONFILE_ANONYMISATION = "PROPERTY_MSG_FOR_ECHEC_VERSIONFILE_ANONYMISATION";
    
    private static Logger DATASET_LOGGER = LoggerFactory.getLogger("dataset.logger");

    /**
     * The recorder dao @link(IRecorderDAO).
     */
    protected IRecorderDAO recorderDAO;
    /**
     * The dataset dao @link(IDatasetDAO).
     */
    protected IDatasetDAO datasetDAO;
    /**
     * The coreConfiguration @link(ICoreConfiguration).
     */
    protected ICoreConfiguration coreConfiguration;
    /**
     * The utilisateur dao @link(IUtilisateurDAO).
     */
    protected IUtilisateurDAO utilisateurDAO;
    /**
     * The versioning manager @link(IVersioningManager).
     */
    protected IVersioningManager versioningManager;

    /**
     *
     */
    protected MgaRecorder mgaRecorderNode;
    /**
     *
     * @param coreConfiguration
     */
    public void setCoreConfiguration(ICoreConfiguration coreConfiguration) {
        this.coreConfiguration = coreConfiguration;
    }
    /**
     * Sets the dataset dao.
     *
     * @param datasetDAO the new dataset dao
     */
    public void setDatasetDAO(final IDatasetDAO datasetDAO) {
        this.datasetDAO = datasetDAO;
    }
    
    /**   
     * @param utilisateur
     * @return successDatasetReport
     * @throws DeleteUserException the DeleteUserException exception
     */
    @Override
    public InfosReport deleteDependancesUserProfile(Utilisateur utilisateur) throws DeleteUserException {
        InfosReport successDatasetReport = new InfosReport("Dataset_VersionFile");
        String reportMessage;
        try{
            Utilisateur anonymousUser = utilisateurDAO.merge((Utilisateur) utilisateurDAO.getByLogin(coreConfiguration.getAnonymousLogin()).get());
            datasetDAO.anonymiseUserDatasets(utilisateur, anonymousUser);
            reportMessage = String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_FOR_SUCCESS_DATASET_ANONYMISATION), utilisateur.getLogin());
            successDatasetReport.addInfos("Dataset Deletion", reportMessage);
        }
        catch(PersistenceException e){
            reportMessage = String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_FOR_ECHEC_DATASET_ANONYMISATION), utilisateur.getLogin());
            throw new DeleteUserException(reportMessage);
        }
        
        try{
            versioningManager.anonymiseUserVersionsFile(utilisateur);
            reportMessage = String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH_VERSIONING, PROPERTY_MSG_FOR_SUCCESS_VERSIONFILE_ANONYMISATION), utilisateur.getLogin());
            successDatasetReport.addInfos("VersionFile Deletion", reportMessage);
        }
        catch(PersistenceException e){
            reportMessage = String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH_VERSIONING, PROPERTY_MSG_FOR_ECHEC_VERSIONFILE_ANONYMISATION), utilisateur.getLogin());
            throw new DeleteUserException(reportMessage);
        }
              
        return successDatasetReport;
    }
    
    /**
     * Check version.
     *
     * @param version the version
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.IDatasetManager#checkVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    @Transactional
    public void checkVersion(final VersionFile version) throws BusinessException {
        versioningManager.checkVersion(version);
    }

    /**
     * Delete version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.IDatasetManager#deleteVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void deleteVersion(final VersionFile versionFile) throws BusinessException {
        versioningManager.deleteVersion(versionFile);
    }

    /**
     * Download version by id.
     *
     * @param versionFileId the version file id
     * @return the byte[]
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.IDatasetManager#downloadVersionById(java.lang.Long)
     */
    @Override
    public byte[] downloadVersionById(final Long versionFileId) throws BusinessException {
        return versioningManager.downloadVersionById(versionFileId);
    }

    /**
     * Gets the version file by id.
     *
     * @param versionFileId the version file id
     * @return the version file by id
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.IDatasetManager#getVersionFileById(java.lang.Long)
     */
    @Override
    public VersionFile getVersionFileById(final Long versionFileId) throws BusinessException {
        return versioningManager.getVersionFileById(versionFileId);
    }

    /**
     * Gets the version file helper resolver.
     *
     * @return the version file helper resolver
     * @see
     * org.inra.ecoinfo.dataset.IDatasetManager#getVersionFileHelperResolver()
     */
    @Override
    public IVersionFileHelperResolver getVersionFileHelperResolver() {
        return versioningManager.getVersionFileHelperResolver();
    }

    /**
     * Gets the version from file name.
     *
     * @param fileName the file name
     * @param realNodeLeafId
     * @return the version from file name
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.IDatasetManager#getVersionFromFileName(java.lang.String,
     */
    @Override
    public VersionFile getVersionFromFileName(final String fileName, final Long realNodeLeafId) throws BusinessException {
        try {
            final Optional<VersionFile> version = versioningManager.testIsValidFileNameAndGetNewVersion(fileName, realNodeLeafId);
            return version.orElseThrow(() -> new BusinessException(String.format("no realNode for id %d", realNodeLeafId)));
        } catch (final BusinessException e) {
            final Utilisateur utilisateur = (Utilisateur) policyManager.getCurrentUser();
            UncatchedExceptionLogger.log(String.format(localizationManager.getMessage(DefaultDatasetManager.BUNDLE_SOURCE_PATH, DefaultDatasetManager.PROPERTY_MSG_BAD_FILE_FORMAT), fileName), e);
            sendNotification(String.format(localizationManager.getMessage(DefaultDatasetManager.BUNDLE_SOURCE_PATH, DefaultDatasetManager.PROPERTY_MSG_BAD_FILE_FORMAT), fileName), Notification.ERROR, e.getMessage(), utilisateur);
        }
        return null;
    }

    /**
     * Publish version.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.IDatasetManager#publishVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void publishVersion(final VersionFile versionFile) throws BusinessException {
        try {
            versioningManager.publishVersion(versionFile);
            DATASET_LOGGER.info(String.format("%s a publié le fichier %s", MDC.get("publication.user.login"), MDC.get("publication.fileName")));
        } catch (final BusinessException e) {
            throw new BusinessException("can't publish version", e);
        }
    }

    /**
     * Retrieve datasets.
     *
     * @param leafNodeId
     * @return the list
     * @throws BusinessException the business exception
     * @see
     */
    @Override
    public List<Dataset> retrieveDatasets(final Long leafNodeId) throws BusinessException {
        return versioningManager.retrieveDatasets(leafNodeId);
    }

    /**
     * Sets the recorder dao.
     *
     * @param recorderDAO the new recorder dao
     */
    public void setRecorderDAO(final IRecorderDAO recorderDAO) {
        this.recorderDAO = recorderDAO;
    }

    /**
     * Sets the utilisateur dao.
     *
     * @param utilisateurDAO the new utilisateur dao
     */
    public void setUtilisateurDAO(final IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     * Sets the versioning manager.
     *
     * @param versioningManager the new versioning manager
     */
    public void setVersioningManager(final IVersioningManager versioningManager) {
        this.versioningManager = versioningManager;
    }

    /**
     * Un publish version.
     *
     * @param versionFile the version file
     * @return 
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.IDatasetManager#unPublishVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public int unPublishVersion(final VersionFile versionFile) throws BusinessException {
        return versioningManager.unPublish(versionFile);
    }

    /**
     * Upload version.
     *
     * @param version the version
     * @return the dataset
     * @throws BusinessException the business exception
     * @see
     * org.inra.ecoinfo.dataset.IDatasetManager#uploadVersion(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public Dataset uploadVersion(final VersionFile version) throws BusinessException {
        final Dataset uploadVersion = versioningManager.uploadVersion(version);    
        DATASET_LOGGER.info(String.format("%s a déposé le fichier %s", MDC.get("publication.user.login"), MDC.get("publication.fileName")));
        return uploadVersion;    
    }

    /**
     *
     * @param mgaRecorderNode
     */
    public void setMgaRecorderNode(MgaRecorder mgaRecorderNode) {
        this.mgaRecorderNode = mgaRecorderNode;
    }

}
