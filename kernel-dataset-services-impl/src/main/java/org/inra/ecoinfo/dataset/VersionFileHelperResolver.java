package org.inra.ecoinfo.dataset;

import java.util.HashMap;
import java.util.Map;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver;
import org.inra.ecoinfo.dataset.versioning.exception.NoVersionFileHelperResolvedException;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class VersionFileHelperResolver.
 */
public class VersionFileHelperResolver implements IVersionFileHelperResolver {

    /**
     * The datatypes version file helpers map.
     * @link(Map<String,IVersionFileHelper>).
     */
    private static Map<String, IVersionFileHelper> datatypesVersionFileHelpersMap =new HashMap();
    /**
     * Sets the datatypes version file helpers map.
     *
     * @param datatypesVersionFileHelpersMap the datatypes version file helpers
     * map
     * @link(Map<String,IVersionFileHelper>) the datatypes version file helpers
     * map
     */
    public static void setDatatypesVersionFileHelpersMap(final Map<String, IVersionFileHelper> datatypesVersionFileHelpersMap) {
        VersionFileHelperResolver.datatypesVersionFileHelpersMap = datatypesVersionFileHelpersMap;
    }
    private IDatasetConfiguration datasetConfiguration;

    /**
     *
     * @param datasetConfiguration
     */
    public void setConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }


    /**
     * Gets the datatypes version file helpers map.
     *
     * @return the datatypes version file helpers map
     */
    public Map<String, IVersionFileHelper> getDatatypesVersionFileHelpersMap() {
        return VersionFileHelperResolver.datatypesVersionFileHelpersMap;
    }

    /**
     * Resolve by datatype.
     *
     * @param datatypeName the datatype name
     * @return the i version file helper
     * @throws org.inra.ecoinfo.dataset.versioning.exception.NoVersionFileHelperResolvedException
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver#resolveByDatatype(java.lang.String)
     */
    @Override
    public IVersionFileHelper resolveByDatatype(final String datatypeName) throws NoVersionFileHelperResolvedException {
        final String datatypeCode = Utils.createCodeFromString(datatypeName);
        if (!VersionFileHelperResolver.datatypesVersionFileHelpersMap.containsKey(datatypeCode)) {
            VersionFileHelperResolver.datatypesVersionFileHelpersMap.put(datatypeCode, new DefaultVersionFileHelper(datasetConfiguration));
            // throw new NoVersionFileHelperResolvedException(datatypeCode);
        }
        return VersionFileHelperResolver.datatypesVersionFileHelpersMap.get(datatypeCode);
    }
}
