package org.inra.ecoinfo.dataset.impl.filenamecheckers;

import java.util.regex.Matcher;
import org.inra.ecoinfo.dataset.IFileNameChecker;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 * The Class FileNameCheckerddmmyyyy.
 */
public class FileNameCheckerddmmyyyy extends AbstractFileNameChecker {

    /**
     * The Constant DATE_PATTERN @link(String).
     */
    private static final String DATE_PATTERN = DateUtil.DD_MM_YYYY_FILE;

    /**
     * Gets the file path.
     *
     * @param version the version
     * @return the file path
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String getFilePath(final VersionFile version) {
        String currentSite = version.getDataset().getRealNode().getNodeByNodeableTypeResource(Site.class).getCode();
        currentSite = reWriteSitePath(currentSite);
        final String currentDatatype = version.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode();
        return String.format(IFileNameChecker.PATTERN_FILE_NAME_PATH, 
                currentSite, 
                currentDatatype,
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateDebutPeriode(), DATE_PATTERN),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateFinPeriode(), DATE_PATTERN),
                version.getVersionNumber());
    }

    /**
     * Gets the file path.
     *
     * @param dataset
     * @return the file path
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String getFilePath(final Dataset dataset) {
        String currentSite = dataset.getRealNode().getNodeByNodeableTypeResource(Site.class).getCode();
        currentSite = reWriteSitePath(currentSite);
        final String currentDatatype = dataset.getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode();
        return String.format(IFileNameChecker.PATTERN_FILE_NAME_PATH, 
                currentSite, 
                currentDatatype,
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(),DATE_PATTERN),
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), DATE_PATTERN),
                dataset.getVersions().size());
    }

    /**
     * Gets the date pattern.
     *
     * @return the date pattern
     * @see
     * org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker#getDatePattern()
     */
    @Override
    protected String getDatePattern() {
        return FileNameCheckerddmmyyyy.DATE_PATTERN;
    }

    /**
     * Test dates.
     *
     * @param version the version
     * @param currentSite the current site
     * @param currentDatatype the current datatype
     * @param splitFilename the split filename
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @see
     * org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker#testDates(org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * java.lang.String, java.lang.String, java.util.regex.Matcher)
     */
    @Override
    protected void testDates(final VersionFile version, final String currentSite, final String currentDatatype, final Matcher splitFilename) throws InvalidFileNameException {
        IntervalDate intervalDate;
        try {
            intervalDate = IntervalDate.getIntervalDateddMMyyyy(splitFilename.group(3), splitFilename.group(4));
        } catch (final BadExpectedValueException e1) {
            throw new InvalidFileNameException(String.format(AbstractFileNameChecker.INVALID_FILE_NAME, currentSite, currentDatatype, getDatePattern(), getDatePattern()), e1);
        }
        if (version != null) {
            version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
            version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
        }
    }
}
