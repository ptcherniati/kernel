package org.inra.ecoinfo.dataset.impl.filenamecheckers;

import com.google.common.base.Strings;
import java.time.temporal.TemporalAdjusters;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.inra.ecoinfo.dataset.IFileNameChecker;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 * The Class FileNameCheckeryyyy.
 */
public class FileNameCheckersSimpleyyyy extends AbstractFileNameChecker {
    protected static final String PATTERN = "^(%s|.*?)_(%s|.*?)_(.*)\\.csv$";

    /**
     * The Constant DATE_PATTERN @link(String).
     */
    private static final String DATE_PATTERN = DateUtil.YYYY;
    String PATTERN_FILE_NAME_PATH = "%s_%s_%s#V%d#.csv";
    @Override
    public boolean isValidFileName(String fileName, final VersionFile version) throws InvalidFileNameException {
        final String localFileName = cleanFileName(fileName);
        String currentSite = version.getDataset().getRealNode().getNodeByNodeableTypeResource(Site.class).getCode();
        final String currentDatatype = version.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode();
        final Matcher splitFilename = Pattern.compile(String.format(PATTERN, currentSite, currentDatatype)).matcher(localFileName);
        testPath(currentSite, currentDatatype, splitFilename);
        final String siteName = reWriteSitePath(Utils.createCodeFromString(splitFilename.group(1)));
        testSite(version, currentSite, currentDatatype, siteName);
        final String datatypeName = Utils.createCodeFromString(splitFilename.group(2));
        testDatatype(currentSite, currentDatatype, datatypeName);
        testDates(version, currentSite, currentDatatype, splitFilename);
        return true;
    }

    /**
     * Gets the file path.
     *
     * @param version the version
     * @return the file path
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String getFilePath(final VersionFile version) {
        String currentSite = version.getDataset().getRealNode().getNodeByNodeableTypeResource(Site.class).getCode();
        final String currentDatatype = version.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode();
        if (!Strings.isNullOrEmpty(configuration.getSiteSeparatorForFileNames())) {
            currentSite = currentSite.replaceAll(PatternConfigurator.UNDERSCORE, configuration.getSiteSeparatorForFileNames());
        }
        return String.format(IFileNameChecker.PATTERN_FILE_NAME_PATH, 
                currentSite, 
                currentDatatype,
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateDebutPeriode(), DATE_PATTERN),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateFinPeriode(), DATE_PATTERN),
                version.getVersionNumber());
    }

    /**
     * Gets the file path.
     *
     * @param dataset
     * @return the file path
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public String getFilePath(final Dataset dataset) {
        String currentSite = dataset.getRealNode().getNodeByNodeableTypeResource(Site.class).getCode();
        final String currentDatatype = dataset.getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode();
        if (!Strings.isNullOrEmpty(configuration.getSiteSeparatorForFileNames())) {
            currentSite = currentSite.replaceAll(PatternConfigurator.UNDERSCORE, configuration.getSiteSeparatorForFileNames());
        }
        return String.format(IFileNameChecker.PATTERN_FILE_NAME_PATH, 
                currentSite, 
                currentDatatype,
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(), DATE_PATTERN),
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), DATE_PATTERN),
                dataset.getVersions().size());
    }

    /**
     * Gets the date pattern.
     *
     * @return the date pattern
     * @see
     * org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker#getDatePattern()
     */
    @Override
    protected String getDatePattern() {
        return FileNameCheckersSimpleyyyy.DATE_PATTERN;
    }

    /**
     * Test dates.
     *
     * @param version the version
     * @param currentSite the current site
     * @param currentDatatype the current datatype
     * @param splitFilename the split filename
     * @throws org.inra.ecoinfo.dataset.exception.InvalidFileNameException
     * @see
     * org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker#testDates(org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * java.lang.String, java.lang.String, java.util.regex.Matcher)
     */
    @Override
    protected void testDates(final VersionFile version, final String currentSite, final String currentDatatype, final Matcher splitFilename) throws InvalidFileNameException {
        IntervalDate intervalDate;
        try {
            intervalDate = IntervalDate.getIntervalDateyyyy(splitFilename.group(3), splitFilename.group(3));
        } catch (final BadExpectedValueException e1) {
            throw new InvalidFileNameException(String.format(AbstractFileNameChecker.INVALID_FILE_NAME, currentSite, currentDatatype, FileNameCheckersSimpleyyyy.DATE_PATTERN, FileNameCheckersSimpleyyyy.DATE_PATTERN), e1);
        }

        if (version != null) {
            version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate().with(TemporalAdjusters.firstDayOfYear()));
            version.getDataset().setDateFinPeriode(intervalDate.getEndDate().with(TemporalAdjusters.lastDayOfYear()));
        }
    }
}
