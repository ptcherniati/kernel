package org.inra.ecoinfo.dataset.serialization.impl;

import java.io.IOException;
import java.time.DateTimeException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.dataset.IRecorderDAO;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.serialization.IDatasetSerialization;
import org.inra.ecoinfo.dataset.versioning.IRepositoryManagerVersions;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.serialization.AbstractSerialization;
import org.inra.ecoinfo.serialization.impl.AbstractActivitySerialization;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MailAdminSerialization.
 */
public class DatasetSerialization extends AbstractSerialization implements IDatasetSerialization {

    /**
     * The Constant SCHEMA_PATH.
     */
    private static final String SCHEMA_PATH = "xsd/datasetSerialization.xsd";

    /**
     *
     */
    public static final String DATASET_ENTRY = "dataset";

    /**
     *
     */
    public static final String HEADER_CSV_FILE = "path;file name;is publish version;upload date;create date;upload user;create users;publish comment;publish date;publish user;version number";

    /**
     *
     */
    public static final String LINE_CSV_FILE = "%n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    /**
     *
     */
    public static final String LINE_PROPERTY_CSV_FILE = ";%s;%s";

    /**
     *
     */
    public static final String FILE_NAME_PATTERN = "%s.csv";

    /**
     *
     */
    public static final String DATASET_ZIP_ENTRY_PATTERN = "%s/%s";
    /**
     * The Constant MODULE_NAME.
     */
    private static final String MODULE_NAME = "datasetSerialization";

    /**
     *
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractActivitySerialization.class.getName());
    private String id;
    /**
     * The mail admin.
     */
    protected IUtilisateurDAO utilisateurDAO;
    private IDatasetConfiguration datasetConfiguration;

    /**
     *
     */
    protected IRecorderDAO recorderDAO;
    private IVersionFileDAO versionFileDAO;

    /**
     *
     */
    protected IRepositoryManagerVersions repositoryManagerVersions;

    /**
     *
     * @param repositoryManagerVersions
     */
    public void setRepositoryManagerVersions(IRepositoryManagerVersions repositoryManagerVersions) {
        this.repositoryManagerVersions = repositoryManagerVersions;
    }

    /**
     *
     * @param versionFileDAO
     */
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     *
     * @param datasetConfiguration
     */
    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }

    /**
     *
     * @param recorderDAO
     */
    public void setRecorderDAO(IRecorderDAO recorderDAO) {
        this.recorderDAO = recorderDAO;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#createConfig(org.apache.commons.digester.Digester)
     */
    /**
     *
     * @param digester
     */
    @Override
    public void createConfig(Digester digester) {
        digester.addCallMethod("serialization/module/datasetSerialization/id", "setId", 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#getModuleName()
     */
    /**
     *
     * @return
     */
    @Override
    public String getModuleName() {
        return DatasetSerialization.MODULE_NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.AbstractSerialization#getSchemaPath()
     */
    /**
     *
     * @return
     */
    @Override
    public String getSchemaPath() {
        return DatasetSerialization.SCHEMA_PATH;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.config.IModuleSerialization#isAutoLoad()
     */
    /**
     *
     * @return
     */
    @Override
    public Boolean isAutoLoad() {
        return Boolean.TRUE;
    }

    /**
     *
     * @return
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     *
     * @param outZip
     * @throws BusinessException
     */
    @Override
    public void serialize(ZipOutputStream outZip) throws BusinessException {
        try {
            List<String> lines = new LinkedList<>();
            lines = versionFileDAO.getAll(VersionFile.class).stream()
                    .map(versionFile -> serializeVersion(versionFile, outZip))
                    .collect(Collectors.toList());
            registerLine(lines, outZip);
        } catch (IOException ex) {
            LOGGER.error("cant'write activities", ex);
        }
    }

    /**
     *
     * @param outZip
     * @throws IOException
     */
    public void openEntry(ZipOutputStream outZip) throws IOException {
        ZipEntry datasetsFile = new ZipEntry(DATASET_ENTRY);
        outZip.putNextEntry(datasetsFile);
    }

    /**
     *
     * @param outZip
     * @param versionFile
     * @param datasetCode
     * @param fileContent
     */
    public void writeEntry(ZipOutputStream outZip, VersionFile versionFile, String datasetCode, byte[] fileContent) {
        String fileName = null;
        try {
            fileName = versionFile.getDataset().buildDownloadFilename(datasetConfiguration);
            fileName = fileName.replaceAll("/", "-");
            ZipEntry datasetFile = new ZipEntry(String.format(DATASET_ZIP_ENTRY_PATTERN, DATASET_ENTRY, fileName));
            outZip.putNextEntry(datasetFile);
            outZip.write(fileContent);
            outZip.closeEntry();
        } catch (IOException ex) {
            LOGGER.error(String.format("can't zip dataset file ", fileName));
        }
    }

    private String serializeVersion(VersionFile versionFile, ZipOutputStream outZip) {
        String line = addLine(versionFile);
        zipVersion(versionFile, outZip);
        return line;
    }

    private void zipVersion(VersionFile versionFile, ZipOutputStream outZip) {
        writeEntry(outZip, versionFile, versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode(), versionFile.getData());
    }

    private void registerLine(List<String> lines, ZipOutputStream outZip) throws IOException {
        String fileName = String.format(FILE_NAME_PATTERN, DATASET_ENTRY);
        ZipEntry datasetFile = new ZipEntry(String.format(DATASET_ZIP_ENTRY_PATTERN, DATASET_ENTRY, fileName));
        outZip.putNextEntry(datasetFile);
        lines.stream()
                .filter(line -> line != null)
                .forEach(line -> {
                    try {
                        outZip.write(line.getBytes());
                    } catch (IOException ex) {
                        LOGGER.error(String.format("can't write dataset line information %>", line));
                    }
                });
        outZip.closeEntry();
    }

    private String addLine(VersionFile versionFile) {
        String line = "";
        try {
            boolean isPublishVersion = versionFile.equals(versionFile.getDataset().getPublishVersion());
            String publishComment = versionFile.getDataset().getPublishComment();
            publishComment = publishComment == null ? "" : publishComment;
            String fileName = versionFile.getDataset().buildDownloadFilename(datasetConfiguration);
            fileName = fileName.replaceAll("/", "-");
            line = String.format(LINE_CSV_FILE,
                    versionFile.getDataset().getRealNode().getPath(),
                    fileName,
                    isPublishVersion,
                    DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getUploadDate(), DateUtil.DD_MM_YYYY_HH_MM),
                    DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getCreateDate(), DateUtil.DD_MM_YYYY_HH_MM),
                    versionFile.getUploadUser().getLogin(),
                    versionFile.getDataset().getCreateUser().getLogin(),
                    isPublishVersion ? publishComment : "",
                    isPublishVersion ? DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getPublishDate(), DateUtil.DD_MM_YYYY_HH_MM) : "",
                    isPublishVersion ? versionFile.getDataset().getPublishUser().getLogin() : "",
                    isPublishVersion ? versionFile.getVersionNumber() : "");
        } catch (DateTimeException ex) {
            LOGGER.error("can't register version", ex);
        }
        return line;
    }
}
