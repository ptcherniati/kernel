package org.inra.ecoinfo.dataset.versioning.jpa;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class JPADatasetDAO.
 */
public class JPADatasetDAO extends AbstractJPADAO<Dataset> implements IDatasetDAO {
    
    /**
     * Adds the.
     *
     * @param version the version
     * @return the dataset
     * @see
     * org.inra.ecoinfo.dataset.versioning.IDatasetDAO#add(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public Optional<Dataset> add(final VersionFile version) {
        return Optional.empty();
    }

    /**
     * Gets the dataset by natural key.
     *
     * @param node
     * @param startDate the start date
     * @param endDate the end date
     * @return the dataset by natural key
     * @see
     * org.inra.ecoinfo.dataset.versioning.IDatasetDAO#getDatasetByNaturalKey(org.inra.ecoinfo.refdata.node.entity.LeafNode,
     * java.time.LocalDateTime, java.time.LocalDateTime)
     */
    @Override
    public Optional<Dataset> getDatasetByNaturalKey(final RealNode node, final LocalDateTime startDate, final LocalDateTime endDate) {
        CriteriaQuery<Dataset> query = builder.createQuery(Dataset.class);
        Root<Dataset> dsf = query.from(Dataset.class);
        dsf.fetch(Dataset_.versions, JoinType.LEFT);
        query.select(dsf)
                .where(
                        builder.equal(dsf.join(Dataset_.realNode), node),
                        builder.equal(dsf.get(Dataset_.dateDebutPeriode), startDate),
                        builder.equal(dsf.get(Dataset_.dateFinPeriode), endDate));
        return getOptional(query);
    }

    /**
     * Checks for overlaping dates.
     *      * Search for a published version of a different data set that overlaps
     * version period</p>
     *
     * @param version the version
     * @return true, if successful
     * @see
     * org.inra.ecoinfo.dataset.versioning.IDatasetDAO#hasOverlapingDates(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean hasOverlapingDates(final VersionFile version) {
        CriteriaQuery<Dataset> query = builder.createQuery(Dataset.class);
        Root<Dataset> dsf = query.from(Dataset.class);
        Join<Dataset, VersionFile> publishVersion = dsf.join(Dataset_.publishVersion);
        Join<Dataset, RealNode> rn = dsf.join(Dataset_.realNode);
        Predicate and = builder.and(
                builder.isNotNull(publishVersion),
                builder.equal(rn, version.getDataset().getRealNode()),
                builder.equal(dsf, version.getDataset()).not(),
                builder.greaterThan(dsf.<LocalDateTime>get(Dataset_.dateFinPeriode), version.getDataset().getDateDebutPeriode()),
                builder.lessThan(dsf.<LocalDateTime>get(Dataset_.dateDebutPeriode), version.getDataset().getDateFinPeriode())
        );
        query.where(and);
        query.select(dsf);
        return getOptional(query).map(d -> true).orElse(false);
    }

    /**
     * Retrieve datasets.
     *
     * @param realNodeLeafId
     * @return the list
     * @see
     * org.inra.ecoinfo.dataset.versioning.IDatasetDAO#retrieveDatasets(org.inra.ecoinfo.refdata.node.entity.LeafNode)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Dataset> retrieveDatasets(final Long realNodeLeafId) {
        CriteriaQuery<Dataset> query = builder.createQuery(Dataset.class);
        Root<Dataset> dsf = query.from(Dataset.class);
        query.where(builder.equal(dsf.join(Dataset_.realNode).get(RealNode_.id), realNodeLeafId));
        query.select(dsf);
        return getResultList(query);
    }

    /**
     *
     * @param versionFile
     * @return
     */
    @Override
    public int unpublishVersion(VersionFile versionFile) {
        CriteriaUpdate<Dataset> update = builder.createCriteriaUpdate(Dataset.class);
        Root<Dataset> dataset = update.from(Dataset.class);
        update
                .where(builder.equal(dataset.get(Dataset_.publishVersion), versionFile))
                .set(dataset.get(Dataset_.publishComment), builder.nullLiteral(String.class))
                .set(dataset.get(Dataset_.publishDate), builder.nullLiteral(LocalDateTime.class))
                .set(dataset.get(Dataset_.publishUser), builder.nullLiteral(Utilisateur.class))
                .set(dataset.get(Dataset_.publishVersion), builder.nullLiteral(VersionFile.class));
        final int executedRows = entityManager.createQuery(update).executeUpdate();
        return executedRows;
    }
    
    /**
     *
     * @param utilisateur
     * @param anonymousUser
     * @throws PersistenceException
     */
    @Override
    public void anonymiseUserDatasets(Utilisateur utilisateur, Utilisateur anonymousUser) throws PersistenceException{
        CriteriaUpdate<Dataset> updateCreator = builder.createCriteriaUpdate(Dataset.class);
        Root<Dataset> datasetCreator = updateCreator.from(Dataset.class);
        updateCreator.set(datasetCreator.get(Dataset_.createUser), anonymousUser);
        updateCreator.where(builder.equal(datasetCreator.get(Dataset_.createUser), utilisateur));
        update(updateCreator);
        
        CriteriaUpdate<Dataset> updatePublisher = builder.createCriteriaUpdate(Dataset.class);
        Root<Dataset> datasetPublish = updatePublisher.from(Dataset.class);
        updatePublisher.set(datasetPublish.get(Dataset_.publishUser), anonymousUser);
        updatePublisher.where(builder.equal(datasetPublish.get(Dataset_.publishUser), utilisateur));
        update(updatePublisher);
    }
}
