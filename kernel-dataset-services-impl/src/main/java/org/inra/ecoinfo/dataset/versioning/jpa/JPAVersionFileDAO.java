package org.inra.ecoinfo.dataset.versioning.jpa;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.persistence.EntityGraph;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class JPAVersionFileDAO.
 */
public class JPAVersionFileDAO extends AbstractJPADAO<VersionFile> implements IVersionFileDAO {
    
    /**
     * Gets the by id.
     *
     * @param versionFileId the version file id
     * @return the by id
     * @see
     * org.inra.ecoinfo.dataset.versioning.IVersionFileDAO#getById(org.inra.ecoinfo.localization.ILocalizationManager,
     * java.lang.Long)
     */
    @Override
    public Optional<VersionFile> getById(final Long versionFileId) {
        if (versionFileId == null) {
            return Optional.empty();
        }
        EntityGraph<VersionFile> versionFileGraph = (EntityGraph<VersionFile>) entityManager.getEntityGraph(VersionFile.COMPLETE_GRAPH);
        Map hints = new HashMap();
        hints.put("javax.persistence.fetchgraph", versionFileGraph);
        return Optional.ofNullable(entityManager.find(VersionFile.class, versionFileId, hints));
    }
    
    @Override
    public void anonymiseUserVersionsFile(Utilisateur utilisateur, Utilisateur anonymousUser) throws PersistenceException{
        CriteriaUpdate<VersionFile> update = builder.createCriteriaUpdate(VersionFile.class);
        Root<VersionFile> datasetCreator = update.from(VersionFile.class);
        update.set(datasetCreator.get(VersionFile_.uploadUser), anonymousUser);
        update.where(builder.equal(datasetCreator.get(VersionFile_.uploadUser), utilisateur));
        
        update(update);
    }
}
