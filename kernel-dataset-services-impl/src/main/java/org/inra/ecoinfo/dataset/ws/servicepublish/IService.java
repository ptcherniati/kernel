/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.dataset.ws.servicepublish;

import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.inra.ecoinfo.identification.ws.subservice.ISubService;
import org.inra.ecoinfo.ws.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IService extends ISubService {

    Response publishFromContext(
            @Context HttpServletRequest request,
            InputStream requestBody) throws BusinessException;

}
