package org.inra.ecoinfo.dataset;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.endRequest;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.startRequest;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.mockito.MockitoAnnotations;

import org.springframework.transaction.TransactionStatus;

/**
 *
 * @author ptcherniati
 */
public abstract class UnLoadDatasetFixture extends AbstractDatasetFixture {

    /**
     *
     */
    public UnLoadDatasetFixture() {
        super();
        MockitoAnnotations.openMocks(this);
    }

    /**
     *
     * @param nodePath
     * @param path
     * @return
     * @throws BusinessException
     */
    public String unloadVersion(String nodePath, String path) throws BusinessException {
        TransactionStatus tr = getDatasetDAO().beginDefaultTransaction();
        File fileToUpload;
        fileToUpload = new File(path);
        if (!fileToUpload.exists()) {
            throw new BusinessException("Le fichier " + path + " n'existe pas.");
        }
        VersionFile versionFile;
        try {
            VersionFile versionFileNew = getNewVersionTest(nodePath, fileToUpload);
            versionFile = getDatasetDAO().getDatasetByNaturalKey(versionFileNew.getDataset().getRealNode(), versionFileNew.getDataset().getDateDebutPeriode(), versionFileNew.getDataset().getDateFinPeriode())
                    .orElseThrow(() -> new BusinessException("can't get last version"))
                    .getLastVersion();
            //versionFile.getDataset().getNodeDataset().setActivities(versionFileNew.getDataset().getNodeDataset().getActivities());
        } catch (BusinessException | PersistenceException | IOException e) {
            throw new BusinessException("La version de fichier n'existe pas " + e.getMessage());
        }
        startRequest();
        getdatDatasetManager().deleteVersion(versionFile);
        getDatasetDAO().commitTransaction(tr);

        Optional<VersionFile> versionFileOpt = getVersionFileDAO().getById(VersionFile.class, versionFile.getId());
        endRequest();
        if (!versionFileOpt.isPresent()) {
            return "true";
        }
        return "Le fichier n'a pas été supprimé";
    }
}
