package org.inra.ecoinfo.dataset;

import java.io.File;
import java.io.IOException;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.endRequest;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.startRequest;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.notification.CheckNotificationFixture;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.mockito.MockitoAnnotations;


/**
 *
 * @author ptcherniati
 */
public abstract class LoadDatasetFixture extends AbstractDatasetFixture {

    /**
     *
     */
    public LoadDatasetFixture() {
        super();
        MockitoAnnotations.openMocks(this);
    }

    /**
     *
     * @param nodePath
     * @param path
     * @return
     * @throws BusinessException
     */
    public VersionFile uploadDataset(String nodePath, String path) throws BusinessException {
        startRequest();
        File fileToUpload;
        fileToUpload = new File(path);
        if (!fileToUpload.exists()) {
            endRequest();
            throw new BusinessException("Le fichier " + path + " n'existe pas.");
        }
        VersionFile versionFile;
        try {
            versionFile = getNewVersionTest(nodePath, fileToUpload);
        } catch (BusinessException | PersistenceException | IOException e) {
            endRequest();
            Notification lastNotification = CheckNotificationFixture.getLastNotification();
            throw new BusinessException(String.format("%s%n%s", lastNotification.getMessage(), lastNotification.getBody()), e);
        }
        Dataset dataset = getdatDatasetManager().uploadVersion(versionFile);
        if (dataset == null || dataset.getLastVersion() == null) {
            endRequest();
            return null;
        }
        endRequest();
        return dataset.getLastVersion();
    }

    /**
     *
     * @param versionFile
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     */
    public String publishVersion(VersionFile versionFile) throws BusinessException, org.inra.ecoinfo.utils.exceptions.PersistenceException {
        startRequest();
        try {
            getdatDatasetManager().publishVersion(versionFile);
        } catch (BusinessException e) {
            endRequest();
            Notification lastNotification = CheckNotificationFixture.getLastNotification();
            return String.format("%s%n%s", lastNotification.getMessage(), lastNotification.getBody());
        }
        Notification notification = getINotificationsManager().getAllUserNotifications(getUtilisateurConnected()).get(0);
        String message;
        if (notification != null && notification.getLevel().equals(Notification.INFO) && notification.getMessage().contains("a été publiée")) {
            message = "true";
        } else if (notification != null) {
            message = String.format("La version n'a pas été publiée %s %s", notification.getMessage(), notification.getBody());
        } else {
            message = "La version n'a pas été publiée ";
        }
        endRequest();
        return message;
    }
}
