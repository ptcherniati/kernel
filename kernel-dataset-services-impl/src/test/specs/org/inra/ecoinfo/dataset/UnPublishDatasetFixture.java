package org.inra.ecoinfo.dataset;

import java.io.File;
import java.io.IOException;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.endRequest;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.startRequest;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.notification.CheckNotificationFixture;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.mockito.MockitoAnnotations;

import org.springframework.transaction.TransactionStatus;

/**
 *
 * @author ptcherniati
 */
public abstract class UnPublishDatasetFixture extends AbstractDatasetFixture {

    /**
     *
     */
    public UnPublishDatasetFixture() {
        super();
        MockitoAnnotations.openMocks(this);
    }

    /**
     *
     * @param nodePath
     * @param path
     * @return
     * @throws BusinessException
     */
    public String unPublishVersion(String nodePath, String path) throws BusinessException {
        TransactionStatus tr = getDatasetDAO().beginDefaultTransaction();
        File fileToUpload;
        fileToUpload = new File(path);
        startRequest();
        if (!fileToUpload.exists()) {
            endRequest();
            throw new BusinessException("Le fichier " + path + " n'existe pas.");
        }
        VersionFile versionFile;
        try {
            VersionFile versionFileNew = getNewVersionTest(nodePath, fileToUpload);
            versionFile = getDatasetDAO()
                    .getDatasetByNaturalKey(versionFileNew.getDataset().getRealNode(), versionFileNew.getDataset().getDateDebutPeriode(), versionFileNew.getDataset().getDateFinPeriode())
                    .orElseThrow(() -> new BusinessException("can't get dataset"))
                    .getPublishVersion();
            //versionFile.getDataset().getNodeDataset().setActivities(versionFileNew.getDataset().getNodeDataset().getActivities());
        } catch (BusinessException | PersistenceException | IOException e) {
            endRequest();
            throw new BusinessException("aucune version publiée" + e.getMessage());
        }
        int unpublish = getdatDatasetManager().unPublishVersion(versionFile); 
        endRequest();
        getDatasetDAO().commitTransaction(tr);
        startRequest();
        endRequest();
        if (unpublish==1) {
            return "true";
        }
        Notification lastNotification = CheckNotificationFixture.getLastNotification();
        getDatasetDAO().rollbackTransaction(tr);
        return String.format("%s%n%s", lastNotification.getMessage(), lastNotification.getBody());
    }
}
