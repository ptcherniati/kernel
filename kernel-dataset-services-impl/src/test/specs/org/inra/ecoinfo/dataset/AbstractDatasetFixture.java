package org.inra.ecoinfo.dataset;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.apache.commons.io.IOUtils;
import org.inra.ecoinfo.AbstractTestFixture;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.*;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractDatasetFixture extends AbstractTestFixture {

    /**
     *
     */
    public AbstractDatasetFixture() {
        super();
    }

    /**
     *
     * @param path
     * @return
     * @throws PersistenceException
     */
    public Long getLeafrealNodeId(String path) throws PersistenceException {
//        policyManager.getMgaServiceBuilder().getRecorder().getEntityManager()
//                .createNativeQuery("select path from RealNode where path like 'suivi_des_lacs,grand_lac,lac3,compartiments_biologiques%'")
//                .getResultList()
        final EntityManager em = policyManager.getMgaServiceBuilder().getRecorder().getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<NodeDataSet> nds = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rn = nds.join(NodeDataSet_.realNode);
        query.where(builder.equal(rn.get(RealNode_.path), path));
        query.select(rn.get(RealNode_.id));
        List<Long> resultList = em.createQuery(query).setMaxResults(1).getResultList();
        if (resultList.isEmpty()) {
            return null;
        }

        return resultList.get(0);
    }

    /**
     *
     * @return
     */
    public IDatasetManager getdatDatasetManager() {
        return (IDatasetManager) applicationContext.getBean("datasetManager");
    }

    /**
     *
     * @return
     */
    public INotificationsManager getINotificationsManager() {
        return (INotificationsManager) applicationContext.getBean("notificationsManager");
    }

    /**
     *
     * @return
     */
    public IDatasetDAO getDatasetDAO() {
        return (IDatasetDAO) applicationContext.getBean("datasetDAO");
    }

    /**
     *
     * @return
     */
    public IVersionFileDAO getVersionFileDAO() {
        return (IVersionFileDAO) applicationContext.getBean("versionFileDAO");
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return (ILocalizationManager) applicationContext.getBean("localizationManager");
    }

    /**
     *
     * @param nodePath
     * @param file
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     * @throws FileNotFoundException
     * @throws IOException
     */
    protected VersionFile getNewVersionTest(String nodePath, File file) throws BusinessException, PersistenceException, FileNotFoundException, IOException {
        VersionFile versionFromFileName = getdatDatasetManager().getVersionFromFileName(file.getName(), getLeafrealNodeId(nodePath));
        final byte[] data = Utils.sanitizeData(
                IOUtils.toByteArray(new FileInputStream(file)), getLocalizationManager());
        versionFromFileName.setData(data);
        return versionFromFileName;
    }
}
