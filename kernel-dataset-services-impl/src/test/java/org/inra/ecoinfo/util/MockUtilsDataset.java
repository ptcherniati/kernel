package org.inra.ecoinfo.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationDAO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.entity.UserLocale;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationDAO;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.theme.IThemeDAO;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.refdata.unite.IUniteDAO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *
 * @author ptcherniati
 */
public class MockUtilsDataset {


    /**
     *
     */
    public static String DATE_DEBUT = "01/01/2012";

    /**
     *
     */
    public static String DATE_FIN = "31/12/2012";

    /**
     *
     */
    public static String DATE_DEBUT_COMPLETE = "01/01/2012 00:00";

    /**
     *
     */
    public static String DATE_FIN_COMPLETE = "31/12/2012 00:00";

    /**
     *
     */
    public static String TYPE_SITE = "typesite";

    /**
     *
     */
    public static String SITE_CODE = "site";

    /**
     *
     */
    public static String SITE_NOM = "Site";

    /**
     *
     */
    public static Long SITE_ID = 1L;

    /**
     *
     */
    public static String ESPECE = "espece";

    /**
     *
     */
    public static String PROJET = "projet";

    /**
     *
     */
    public static String THEME = "theme";

    /**
     *
     */
    public static String DATATYPE = "datatype";

    /**
     *
     */
    public static String DATATYPE_NOM = "Datatype";

    /**
     *
     */
    public static String VARIABLE_CODE = "variablenom";

    /**
     *
     */
    public static String VARIABLE_NOM = "variableNom";

    /**
     *
     */
    public static String VARIABLE_AFFICHAGE = "variableAffichage";

    /**
     *
     */
    public static String UNITE = "unite";

    /**
     *
     */
    public static String TRAITEMENT = "traitement";

    /**
     *
     */
    public static String TRAITEMENT_AFFICHAGE = "Traitement";

    /**
     *
     */
    public static String FILE_NAME = "site_datatype_01-01-2012_31-12-2012.csv";

    /**
     *
     */
    public static String PATH = "site/theme/datatype";
    /**
     *
     */
    public static String SITE_PATH = String.format("%s/%s",
            MockUtilsDataset.TYPE_SITE,
            MockUtilsDataset.SITE_CODE);
    /**
     *
     */
    public static ILocalizationManager localizationManager;

    /**
     *
     */
    public static final String UTILISATEUR = "utilisateur";

    private static final Logger LOGGER = LoggerFactory.getLogger(MockUtilsDataset.class
            .getName());

    /**
     *
     * @return
     */
    public static MockUtilsDataset getInstance() {
        MockUtilsDataset instance = new MockUtilsDataset();
        MockitoAnnotations.openMocks(instance);
        
        try {
            
            instance.initMocks();
            
        } catch (DateTimeParseException ex) {
            
            return null;
            
        }
        
        MockUtilsDataset.initLocalization();
        
        return instance;
    }

    /**
     *
     */
    public static void initLocalization() {
        Localization.setDefaultLocalisation("fr");
        
        Localization.setLocalisations(Arrays.asList(new String[]{"fr", "en"}));
        
        MockUtilsDataset.localizationManager = new SpringLocalizationManager();
        ((SpringLocalizationManager) MockUtilsDataset.localizationManager).setUserLocale(new UserLocale());
    }

    /**
     *
     */
    @Mock
    public IPolicyManager policyManager; 

    /**
     *
     */
    @Mock
    public IDatasetDAO datasetDAO; 

    /**
     *
     */
    @Mock
    public IUtilisateurDAO utilisateurDAO; 

    /**
     *
     */
    @Mock
    public VersionFile versionFile;

    /**
     *
     */
    @Mock
    public IVersionFileDAO versionFileDAO;

    /**
     *
     */
    @Mock
    public Dataset dataset;

    /**
     *
     */
    @Mock
    public Site site;

    /**
     *
     */
    @Mock
    public ISiteDAO siteDAO;

    /**
     *
     */
    @Mock
    public Theme theme;

    /**
     *
     */
    @Mock
    public IThemeDAO themeDAO;

    /**
     *
     */
    @Mock
    public DataType datatype;

    /**
     *
     */
    @Mock
    public IDatatypeDAO datatypeDAO;

    /**
     *
     */
    @Mock
    public Utilisateur utilisateur;

    /**
     *
     */
    @Mock
    public Variable variable;

    /**
     *
     */
    @Mock
    public IVariableDAO variableDAO;

    /**
     *
     */
    @Mock
    public Unite unite;

    /**
     *
     */
    @Mock
    public IUniteDAO uniteDAO;

    /**
     *
     */
    @Mock
    public DatatypeVariableUnite datatypeVariableUnite;

    /**
     *
     */
    @Mock
    public List<DatatypeVariableUnite> datatypeVariableUnites;

    /**
     *
     */
    @Mock
    public IDatatypeVariableUniteDAO datatypeVariableUniteDAO;

    /**
     *
     */
    @Mock
    public DatasetDescriptor datasetDescriptor;

    /**
     *
     */
    @Mock
    public Column column;

    /**
     *
     */
    @Mock
    public List<Column> columns;
    /**
     *
     */
    public LocalDateTime dateFinLocale, dateDebutLocale,dateDebutCompleteLocal, dateFinCompleteLocal;
    /**
     *
     */
    @Mock
    public IDatasetConfiguration datasetConfiguration;

    /**
     *
     */
    @Mock
    public ILocalizationDAO localizationDAO;

    /**
     *
     */
    @Mock
    public NodeDataSet nodeDadaset;

    /**
     *
     */
    @Mock
    public RealNode realNodeDataset;

    /**
     *
     */
    @Mock
    public INotificationDAO notificationDAO;

    /**
     *
     */
    @Mock
    public INotificationsManager notificationsManager;
    @Mock NodeDataSet nodeDatatype;
    @Mock NodeDataSet nodeTheme;

    /**
     *
     */
    @Mock
    public RealNode realNodeTheme;

    @Mock NodeDataSet nodeSite;

    /**
     *
     */
    @Mock
    public RealNode realNodeSite;
    @Mock NodeDataSet nodeVariable;

    /**
     *
     */
    @Mock
    public RealNode realNodeVariable;

    /**
     *
     */
    public MockUtilsDataset() {
        MockitoAnnotations.openMocks(this);
    }

    /**
     *
     * @throws DateTimeParseException
     */
    public void initMocks() throws DateTimeParseException {
        
        try {
            
            initDates();
            
            initEntities();
            
            initDAOs();
            
            initDatasetDescriptor();
            
            initDataset();
            
        } catch (PersistenceException ex) {
            
            LOGGER.error(ex.getMessage(), ex);
            
        }
        
    }
    
    /**
     *
     * @throws DateTimeParseException
     */
    @Test
    public void test() throws DateTimeParseException {
        
        MockUtilsDataset mockUtils = MockUtilsDataset.getInstance();
        
        VersionFile v = mockUtils.versionFile;
        
        Assert.assertEquals(MockUtilsDataset.UTILISATEUR, v.getUploadUser().getLogin());
        
        Assert.assertTrue(1L == v.getVersionNumber());
        
        Assert.assertEquals(MockUtilsDataset.DATE_DEBUT,
                            DateUtil.getUTCDateTextFromLocalDateTime(v.getDataset().getDateDebutPeriode(), DateUtil.DD_MM_YYYY));
        
        Assert.assertEquals(MockUtilsDataset.DATE_FIN,
                            DateUtil.getUTCDateTextFromLocalDateTime(v.getDataset().getDateFinPeriode(), DateUtil.DD_MM_YYYY));
        
        Assert.assertEquals(v, v.getDataset().getVersions().get(1L));
        
        Assert.assertEquals(MockUtilsDataset.DATATYPE, v.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode());
        
        Assert.assertEquals(MockUtilsDataset.DATATYPE_NOM, v.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getName());
        
        Assert.assertEquals(MockUtilsDataset.SITE_CODE, v.getDataset().getRealNode().getNodeByNodeableTypeResource(Site.class).getCode());
        
        Assert.assertEquals(MockUtilsDataset.SITE_PATH, v.getDataset().getRealNode().getNodeByNodeableTypeResource(Site.class).getPath());
        
        Assert.assertEquals(MockUtilsDataset.THEME, v.getDataset().getRealNode().getNodeByNodeableTypeResource(Theme.class).getCode());
        
    }
    
    private Map<Long, VersionFile> getVersions() {

        Map<Long, VersionFile> versions = new HashMap();

        versions.put(1L, versionFile);

        return versions;

    }

    private void initDAOs() throws PersistenceException {

        // daos
        Mockito.when(siteDAO.getByPath(MockUtilsDataset.SITE_PATH)).thenReturn(Optional.ofNullable(site));

        Mockito.when(siteDAO.getByPath(MockUtilsDataset.SITE_PATH)).thenReturn(Optional.ofNullable(site));

        Mockito.when(siteDAO.getByCodeAndParent(MockUtilsDataset.SITE_CODE, null)).thenReturn(Optional.ofNullable(site));

        Mockito.when(datatypeDAO.getByCode(MockUtilsDataset.DATATYPE)).thenReturn(Optional.ofNullable(datatype));

        Mockito.when(variableDAO.getByCode(MockUtilsDataset.VARIABLE_CODE)).thenReturn(Optional.ofNullable(variable));

        Mockito.when(datatypeVariableUniteDAO.getByDatatype(MockUtilsDataset.DATATYPE)).thenReturn(
                datatypeVariableUnites);

        Mockito.when(datatypeVariableUniteDAO.getByNKey(MockUtilsDataset.DATATYPE, MockUtilsDataset.VARIABLE_CODE,
                                                                                   MockUtilsDataset.UNITE))
                .thenReturn(Optional.ofNullable(datatypeVariableUnite));
        
        Mockito.when(uniteDAO.getByCode(MockUtilsDataset.UNITE)).thenReturn(Optional.ofNullable(unite));
        Mockito.when(variableDAO.getByCode(MockUtilsDataset.VARIABLE_CODE)).thenReturn(Optional.ofNullable(variable));

    }

    private void initDataset() throws DateTimeParseException {
        when(nodeDadaset.getCode()).thenReturn(DATATYPE);
        when(nodeDadaset.getName()).thenReturn(DATATYPE_NOM);
        when(nodeDadaset.getNodeable()).thenReturn(datatype);
        when(realNodeDataset.getCode()).thenReturn(DATATYPE);
        when(realNodeDataset.getName()).thenReturn(DATATYPE_NOM);
        when(realNodeDataset.getNodeable()).thenReturn(datatype);
        when(nodeTheme.getName()).thenReturn(THEME);
        when(nodeTheme.getCode()).thenReturn(THEME);
        when(nodeTheme.getNodeable()).thenReturn(theme);
        when(realNodeTheme.getName()).thenReturn(THEME);
        when(realNodeTheme.getCode()).thenReturn(THEME);
        when(realNodeTheme.getNodeable()).thenReturn(theme);
        when(nodeSite.getName()).thenReturn(SITE_NOM);
        when(nodeSite.getCode()).thenReturn(PATH);
        when(nodeSite.getNodeable()).thenReturn(site);
        when(realNodeSite.getName()).thenReturn(SITE_NOM);
        when(realNodeSite.getCode()).thenReturn(PATH);
        when(realNodeSite.getNodeable()).thenReturn(site);
        when(realNodeSite.getNodeable()).thenReturn(site);
        when(nodeVariable.getName()).thenReturn(VARIABLE_NOM);
        when(nodeVariable.getCode()).thenReturn(VARIABLE_CODE);
        when(nodeVariable.getName()).thenReturn(VARIABLE_NOM);
        when(nodeVariable.getNodeable()).thenReturn(variable);
        when(realNodeVariable.getCode()).thenReturn(VARIABLE_CODE);
        when(realNodeVariable.getNodeable()).thenReturn(variable);

        LocalDateTime dateDebut =  DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, MockUtilsDataset.DATE_DEBUT);

        LocalDateTime dateFin = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, MockUtilsDataset.DATE_FIN);

        // init user
        Mockito.when(utilisateur.getLogin()).thenReturn(MockUtilsDataset.UTILISATEUR);

        // init dataset
        Mockito.when(dataset.getDateDebutPeriode()).thenReturn(dateDebutLocale);

        Mockito.when(dataset.getDateFinPeriode()).thenReturn(dateFinLocale);

        Mockito.when(dataset.getVersions()).thenReturn(getVersions());

        Mockito.when(dataset.getLastVersion()).thenReturn(versionFile);

        Mockito.when(dataset.getVersions()).thenReturn(getVersions());
        Mockito.when(nodeDadaset.getRealNode()).thenReturn(realNodeDataset);
        Mockito.when(nodeTheme.getRealNode()).thenReturn(realNodeTheme);
        Mockito.when(nodeSite.getRealNode()).thenReturn(realNodeSite);
        Mockito.when(nodeVariable.getRealNode()).thenReturn(realNodeVariable);
        Mockito.when(realNodeVariable.getParent()).thenReturn(realNodeDataset);
        Mockito.when(realNodeDataset.getParent()).thenReturn(realNodeTheme);
        Mockito.when(realNodeTheme.getParent()).thenReturn(realNodeSite);

        Mockito.when(dataset.getRealNode()).thenReturn(realNodeDataset);
        Mockito.when(realNodeDataset.getNodeByNodeableTypeResource(DataType.class)).thenReturn(realNodeDataset);
        Mockito.when(realNodeDataset.getNodeByNodeableTypeResource(Theme.class)).thenReturn(realNodeTheme);
        Mockito.when(realNodeDataset.getNodeByNodeableTypeResource(Site.class)).thenReturn(realNodeSite);
        Mockito.when(realNodeDataset.getNodeable()).thenReturn(datatype);
        Mockito.when(nodeDadaset.getNodeByNodeableTypeResource(DataType.class)).thenReturn(nodeDadaset);
        Mockito.when(nodeDadaset.getNodeByNodeableTypeResource(Theme.class)).thenReturn(nodeTheme);
        Mockito.when(nodeDadaset.getNodeByNodeableTypeResource(Site.class)).thenReturn(nodeSite);
        Mockito.when(nodeDadaset.getNodeable()).thenReturn(datatype);
        Mockito.when(nodeDadaset.getPath()).thenReturn(PATH);
        Mockito.when(realNodeDataset.getPath()).thenReturn(PATH);

        // init version
        Mockito.when(versionFile.getDataset()).thenReturn(dataset);

        Mockito.when(versionFile.getVersionNumber()).thenReturn(1L);

        Mockito.when(versionFile.getUploadUser()).thenReturn(utilisateur);

        Mockito.when(dataset.buildDownloadFilename(datasetConfiguration)).thenReturn(String.format("%s_%s_%s_%s.csv", MockUtilsDataset.SITE_PATH.replaceAll("/", "_"),
                                                                                                                      MockUtilsDataset.DATATYPE,
                                                                                                                      DateUtil.getUTCDateTextFromLocalDateTime(dateDebut, DateUtil.DD_MM_YYYY_FILE),
                                                                                                                      DateUtil.getUTCDateTextFromLocalDateTime(dateFin, DateUtil.DD_MM_YYYY_FILE)
        ));
        Mockito.when(versionFile.getData()).thenReturn("data".getBytes());
        Mockito.when(policyManager.getCurrentUser()).thenReturn(utilisateur);

    }

    private void initDatasetDescriptor() {

        Mockito.when(datasetDescriptor.getColumns()).thenReturn(columns);

        Mockito.when(column.getName()).thenReturn("colonne1", "colonne2", "colonne3", "colonne4",
                                                                                      "colonne5", "colonne6", "colonne7", "colonne8", "colonne9", "colonne10", null);
        
        Mockito.when(columns.size()).thenReturn(10);

        Mockito.when(datasetDescriptor.getUndefinedColumn()).thenReturn(2);

        for (int i = 0; i < 10; i++) {

            Mockito.when(columns.get(i)).thenReturn(column);

        }

    }

    private void initDates() throws DateTimeParseException {

        dateDebutLocale = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, MockUtilsDataset.DATE_DEBUT);

        dateFinLocale = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, MockUtilsDataset.DATE_FIN);

        dateDebutCompleteLocal = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, MockUtilsDataset.DATE_DEBUT_COMPLETE);

        dateFinCompleteLocal = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, MockUtilsDataset.DATE_FIN_COMPLETE);

    }

    private void initEntities() {

        // init site
        Mockito.when(site.getPath()).thenReturn(MockUtilsDataset.SITE_PATH);

        Mockito.when(site.getCode()).thenReturn(MockUtilsDataset.SITE_CODE);

        Mockito.when(site.getName()).thenReturn(MockUtilsDataset.SITE_NOM);

        Mockito.when(site.getId()).thenReturn(MockUtilsDataset.SITE_ID);

        // init theme
        Mockito.when(theme.getCode()).thenReturn(MockUtilsDataset.THEME);

        // init datatype
        Mockito.when(datatype.getCode()).thenReturn(MockUtilsDataset.DATATYPE);

        // init variable
        Mockito.when(variable.getCode()).thenReturn(MockUtilsDataset.VARIABLE_CODE);

        Mockito.when(variable.getName()).thenReturn(MockUtilsDataset.VARIABLE_NOM);

        Mockito.when(variable.getAffichage()).thenReturn(MockUtilsDataset.VARIABLE_AFFICHAGE);
        Mockito.when(policyManager.getCurrentUser()).thenReturn(utilisateur);

    }

}
