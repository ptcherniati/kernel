package org.inra.ecoinfo.dataset.config.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.dbunit.DatabaseUnitException;
import org.inra.ecoinfo.config.ConfigurationException;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class DatasetConfigurationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
public class DatasetConfigurationTest {

    /**
     * The dataset configuration.
     */
    @Autowired
    DatasetConfiguration datasetConfiguration;

    /**
     * Sets the dataset configuration.
     *
     * @param datasetConfiguration the new dataset configuration
     */
    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = (DatasetConfiguration) datasetConfiguration;
    }

    /**
     * Test.
     *
     * @throws URISyntaxException the uRI syntax exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws IllegalAccessException the illegal access exception
     * @throws org.inra.ecoinfo.config.ConfigurationException
     * @throws PersistenceException the persistence exception
     * @throws DatabaseUnitException the database unit exception
     * @throws SQLException the sQL exception
     * @throws ParserConfigurationException the parser configuration exception
     */
    @Test
    public void test() throws URISyntaxException, IOException, IllegalAccessException, ConfigurationException, PersistenceException, DatabaseUnitException, SQLException, ParserConfigurationException {
        Assert.assertTrue("la liste des types de données est vide", datasetConfiguration.getDataTypes().size() > 0);
        DataTypeDescription dataTypeDescription = datasetConfiguration.getDataTypes().get(0);
        Assert.assertFalse("la liste des types de données est vide", dataTypeDescription.canOverlapDates());
        Assert.assertEquals("la liste des types de données est vide", "dd/MM/yyyy", dataTypeDescription.getFileNameDateFormat());
        Assert.assertEquals("la liste des types de données est vide", "piegeage_en_montee", dataTypeDescription.getCode());
        Assert.assertEquals("la liste des types de données est vide", "piegeage_en_montee_description", dataTypeDescription.getDescription());
    }
}
